<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import="java.util.*"%>
<%Random generator = new Random();
int r = generator.nextInt();
%>
<%@page import="com.sch.delegates.EduStudentManager"%>
<!--begin custom header content for this example-->
<style type="text/css">
/* custom styles for this example */
#yui-history-iframe {
  position:absolute;
  top:0; left:0;
  width:1px; height:1px; /* avoid scrollbars */
  visibility:hidden;
}
</style>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentManager.js"></script>
<!--end custom header content for this example-->

<!--BEGIN SOURCE CODE FOR EXAMPLE =============================== -->
<table align="center" width="97%" cellpadding="0" cellspacing="0" >
<tr class="mainhead" height="25px"><td>
		&nbsp;&nbsp;<b><bean:message key="app.sch.studentProfileList.listformtitle"/></b>
		&nbsp;					
		</td>
		<td align="right" valign="middle" class="mainhead">
					<input type="text" onkeypress="if(event.keyCode==13) searchByattribute()" name="searchbyname" id="searchbyname" size="20" maxlength="25"  class="srchbox" />&nbsp;&nbsp;
					<a href="#" onclick="searchByattribute()">
						<img src="./images/searcharrow.jpg" align="top">
					</a>&nbsp;&nbsp;
        </td>
		</tr>
	
<tr><td >
&nbsp;</td></tr>
<tr><td colspan="2">
	<div id="bhmintegration" style="width:100%;"></div>
	<div id="dt-pag-nav" align ="center"></div>
</td></tr></table>

<script type="text/javascript">
//Table cell formating for Buttons
 function searchByattribute() {
    // Column definitions
   YAHOO.example.CustomFormatting = new function() {
		this.myCustomFormatterviewStudentProfile = function(elCell, oRecord, oColumn,
				oData) {			
			if(true){
				elCell.innerHTML = '<a href="/schmangmt/EduStudentReportAction.do?operation=viewStudentProfile&User_id='
					+ oRecord.getData("student_id")+'&Stud_status=A'
					+ '" onMouseOut=over("viewStudentProfile'
					+ oRecord.getData("student_id")
					+ '","profile.gif") onMouseOver=over("viewStudentProfile'
					+ oRecord.getData("student_id")
					+ '","profile.gif")><img border=0 src="./images/profile.gif" id="viewStudentProfile'
					+ oRecord.getData("student_id")
					+ '" title="Click Icon to View Student Profile"></a>';
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};
		
		YAHOO.widget.DataTable.Formatter.viewStudentProfile = this.myCustomFormatterviewStudentProfile;

    };
    
    var searchKey = trim(document.getElementById("searchbyname").value);
    var myColumnDefs = [ // sortable:true enables sorting
                         {key:"user_id", label:"User Id", sortable:false,width: 100},
                         {key:"student_name", label:"Student Name", sortable:true,width: 220},
                         {key:"Email", label:"Email", sortable:true,width: 220},
                         {key:"Phone", label:"Contact No.", sortable:true, width: 100},
                         {key:"admissiondate", label:"Admission Date", sortable:false,width: 100},
                         {key:"dateofbirth", label:"Date of Birth", sortable:false,width: 100},
                         {key:"StudentProfile", label:"StudentProfile",formatter: "viewStudentProfile",sortable :false,resizeable :false}       
                     ];

    // Custom parser
    var stringToDate = function(sData) {
        var array = sData.split("-");
        return new Date(array[1] + " " + array[0] + ", " + array[2]);
    };
    
    // DataSource instance
    var myDataSource = new YAHOO.util.DataSource("/schmangmt/jsp/studentProfileFile_json.jsp?ran=<%=r%>&searchKey="+searchKey+"&");
    myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSON;
    myDataSource.responseSchema = {
        resultsList: "records",
        fields: ["student_name","Phone","user_id","Email","student_id","admissiondate","dateofbirth"],
        
        metaFields: {
            totalRecords: "totalRecords" // Access to value in the server response
        }
    };
   
    // DataTable configuration
    var myConfigs = {
		paginator: new YAHOO.widget.Paginator({rowsPerPage:10, containers : ["dt-pag-nav"], template : "{PageLinks} {RowsPerPageDropdown}", rowsPerPageOptions : [10,25,50,100] }), // Enables pagination 
    	initialRequest: "sort=student_name&dir=asc&startIndex=0&results=10", // Initial request for first page of data
        dynamicData: true, // Enables dynamic server-driven data
        sortedBy : {key:"student_name", dir:YAHOO.widget.DataTable.CLASS_ASC} // Sets UI initial sort arrow
    };

     
    
    // DataTable instance
    var myDataTable = new YAHOO.widget.DataTable("bhmintegration", myColumnDefs, myDataSource, myConfigs);
    // Update totalRecords on the fly with value from server
    myDataTable.handleDataReturnPayload = function(oRequest, oResponse, oPayload) {
        oPayload.totalRecords = oResponse.meta.totalRecords;
        return oPayload;
    }
    
    return {
        ds: myDataSource,
        dt: myDataTable
    };
        
}
 searchByattribute();
 loadClass();
</script>

<!--END SOURCE CODE FOR EXAMPLE =============================== -->









