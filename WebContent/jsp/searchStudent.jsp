<%@ page import ="com.sch.to.FeesPaymentTO,java.util.*" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<title>Select Student</title>
	<link href="/styles/default.css" rel="stylesheet" type="text/css" />
<script type='text/javascript'>
function showStudent(studentId,first_name,last_name,father_name,dateofbirth,address,sessionId,classId,class_name,section,monthly_fee)
{
	window.opener.document.getElementById("admission_no").innerHTML = studentId;
	window.opener.document.getElementById("first_name").innerHTML = first_name+" "+last_name;
	window.opener.document.getElementById("father_name").innerHTML = father_name;
	window.opener.document.getElementById("dateofbirth").innerHTML = dateofbirth;
	window.opener.document.getElementById("address").innerHTML  = address;
	window.opener.document.getElementById("class_name").innerHTML  = class_name+"&nbsp&nbsp"+section+"&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Monthly Fee&nbsp"+monthly_fee;
	window.opener.document.getElementById("sessionid").value  = sessionId;
	window.opener.document.getElementById("studentid").value=studentId;
	window.opener.document.getElementById("monthly_fee").value=monthly_fee;
	window.opener.document.getElementById("monthlyfee").value=monthly_fee;
	window.opener.document.getElementById("classid").value=classId;
	
	window.close();
}
</script>
<body >
<html:form  action="/FeesPaymentAction.do?operation=searchStudent" >
    <%
      
      ArrayList<FeesPaymentTO> objstudentlist = (ArrayList<FeesPaymentTO>)request.getAttribute("studentList");
    %>
<% 
if(objstudentlist.size()==0)
			{%>
			<DIV STYLE="font-size:large; ;color: RED">	NO DISTRIBUTOR AVAILABLE.ADD DISTRIBUTOR.</DIV>
			<% }else{%>
       <table cellspacing="0" cellpadding="5" width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr style="background-image: url('../images/ltbrgrey1.gif');">
        <td  align="left"  width="28%"><b>Find Student:</b>
       <input type="text" name="keyword" id="keyword" style="border:1px solid #000000" value="" />
       <input type ="submit" name ="Search" value="Search"/></td>
        </table>
<br>
   <table cellspacing="1" cellpadding="8" width="100%" border="0" align="center" > 
           <tr class="mainhead">          
            <td align="left"   width="10%" >Admission No</td>
			<td align="left"   width="25%" >Name</td>
			<td  align="left"  width="25%" >Father Name</td>
			 <td  align="left"   width="10%" >Class</td>
            <td  align="left"   width="10%" >Date of Birth</td>  
           </tr>
  
  <% 
			
  for(int zz = 0;zz<objstudentlist.size();zz++)
  {
 %>   
         <tr height="10"  style="background-image: url('../images/ltbrgrey1.gif');"> 
          <td  align="left"  width="5%" class="mail"> 
          <INPUT TYPE="radio" NAME="storecodeno" VALUE="<%=objstudentlist.get(zz).getStudentid()%>" onclick="javascript:showStudent('<%=objstudentlist.get(zz).getStudentid() %>','<%=objstudentlist.get(zz).getFirstname()%>','<%=objstudentlist.get(zz).getLastname()%>','<%=objstudentlist.get(zz).getFathername()%>','<%=objstudentlist.get(zz).getDateofbirth()%>','<%=objstudentlist.get(zz).getAddress()%>','<%=objstudentlist.get(zz).getSessionid()%>','<%=objstudentlist.get(zz).getClassid()%>','<%=objstudentlist.get(zz).getClassname()%>','<%=objstudentlist.get(zz).getSection()%>','<%=objstudentlist.get(zz).getMonthly_fee()%>');"/> 
          <span class="text"><%=objstudentlist.get(zz).getStudentid()%></span> </td>
          <td  align="left"  width="25%" class="mail"><span class="text"><%=objstudentlist.get(zz).getFirstname()+" "+objstudentlist.get(zz).getLastname()%></span> </td>
          <td  align="left"  width="25%" class="mail"><span class="text"><%=objstudentlist.get(zz).getFathername()%></span> </td>
          <td  align="left"  width="25%" class="mail"><span class="text"><%=objstudentlist.get(zz).getClassname()%></span> </td>
          <td  align="left"  width="25%" class="mail"><span class="text"><%=objstudentlist.get(zz).getDateofbirth()%></span> </td>
       
          </tr>
 <%} }%>  
     </table>
   
 </html:form>  
</body>