<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import="java.util.*"%>
<%Random generator = new Random();
int r = generator.nextInt();
%>
<!--begin custom header content for this example-->
<style type="text/css">
/* custom styles for this example */
#yui-history-iframe {
  position:absolute;
  top:0; left:0;
  width:1px; height:1px; /* avoid scrollbars */
  visibility:hidden;
}
</style>
<script type='text/javascript' src="/schmangmt/dwr/interface/ConcernTrackingManager.js"></script>

<!--end custom header content for this example-->

<!--BEGIN SOURCE CODE FOR EXAMPLE =============================== -->
<table align="center" width="97%" cellpadding="0" cellspacing="0" >
<tr class="mainhead" height="25px"><td>
		&nbsp;&nbsp;<bean:message key="app.sch.addconcerntracking.concernList"/>
		&nbsp;<a href="/schmangmt/ConcernTrackingAction.do?operation=createSetup" ><img border="0" src="./images/add2.jpg" id="addSchl" width="7%" height="60%" align="top"></a>					
		</td>
		<td align="right" valign="middle" class="mainhead">
					<input type="text" onkeypress="if(event.keyCode==13) searchByattribute()" name="searchbyname" id="searchbyname" size="20" maxlength="25"  class="srchbox" />&nbsp;&nbsp;
					<a href="#" onclick="searchByattribute()">
						<img src="./images/searcharrow.jpg" align="top">
					</a>&nbsp;&nbsp;
        </td>
		</tr>
	  
<tr align="left" style="background-image: url('./images/ltbrgrey1.gif')">

	<td colspan="2">    
					<Label for ="name"><b>&nbsp;&nbsp;Status &nbsp;&nbsp; </b></Label>
			        <select name ="status" id= "status" onchange="searchByattribute();">
					<option value="">Select Status</option>
					<option value ="1">Pending</option>
                    <option value="2">Resolved</option>
					<option value ="3">Unresolved</option>
			        </select>
					<Label for ="name"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Priority &nbsp;&nbsp; </b></Label>
			        <select name ="prioritykey" id= "prioritykey" onchange="searchByattribute();">
					<option value="">Select Priority</option>
                    <option value="Low">Low</option>
					<option value ="High">High</option>
					<option value ="Medium">Medium</option>
			        </select>
					<Label for ="name"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Category &nbsp;&nbsp; </b></Label>
			        <select name ="category" id= "category" onchange="searchByattribute();">
					</select>
</td>

	</tr>	
<tr><td >
&nbsp;</td></tr>
<tr><td colspan="2">
	<div id="bhmintegration" style="width:100%;"></div>
	<div id="dt-pag-nav" align ="center"></div>
</td></tr></table>

<script type="text/javascript">
function loadProperty()
{
	var categoryoption = document.getElementById("category");
	ConcernTrackingManager.getAllCategory(function(category){
			for(var xx=0;xx<category.length;xx++)
			{
				var catidvalue = category[xx].split(",");
				categoryoption[xx]= new Option(catidvalue[1],catidvalue[0]); 						
			}
			 
    	});
   
}
//Table cell formating for Buttons
 function searchByattribute() {
	 
     searchStatusKey = trim(document.getElementById("status").value);
     searchcategory = trim(document.getElementById("category").value);
   	 searchpriority = trim(document.getElementById("prioritykey").value);
   // Column definitions
   YAHOO.example.CustomFormatting = new function() {
    	this.myCustomFormatterEditStudent = function(elCell, oRecord, oColumn,
				oData) {
			
			if(true){

				elCell.innerHTML = '<a href="/schmangmt/ConcernTrackingAction.do?operation=updateSetup&Issue_id='
					+ oRecord.getData("issue_id")
					+ '" onMouseOut=over("EditStudent'
					+ oRecord.getData("user_id")
					+ '","edit.gif") onMouseOver=over("EditStudent'
					+ oRecord.getData("user_id")
					+ '","edit.gif")><img border=0 src="./images/edit.gif" id="EditStudent'
					+ oRecord.getData("user_id")
					+ '" title="Click Edit Icon to update the concern"></a>';
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};
		
		this.myCustomFormatterDeleteStudent = function(elCell, oRecord, oColumn,
				oData) {
			
			if(true){

				elCell.innerHTML = '<a href="/schmangmt/ConcernTrackingAction.do?operation=delete&Issue_id='
					+ oRecord.getData("issue_id")
					+ '" onMouseOut=over("DeleteStudent'
					+ oRecord.getData("user_id")
					+ '","close1.gif") onMouseOver=over("DeleteStudent'
					+ oRecord.getData("user_id")
					+ '","close1.gif")><img border=0 src="./images/close1.gif" id="DeleteStudent'
					+ oRecord.getData("user_id")
					+ '" title="Click Delete Icon to delete concern"></a>';
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};

		this.myCustomFormatterviewDetails = function(elCell, oRecord, oColumn,
				oData) {			
			if(true){
				elCell.innerHTML = '<a href="/schmangmt/ConcernTrackingAction.do?operation=viewSetup&Issue_id='
					+ oRecord.getData("issue_id")
					+ '" onMouseOut=over("viewDetails'
					+ oRecord.getData("user_id")
					+ '","view.gif") onMouseOver=over("viewDetails'
					+ oRecord.getData("user_id")
					+ '","view.gif")><img border=0 src="./images/view.gif" id="viewDetails'
					+ oRecord.getData("user_id")
					+ '" title="Click Icon to view concern detail"></a>';
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};
		
		this.myCustomFormatterConcernReply = function(elCell, oRecord, oColumn,
				oData) {
			
			if(true){

				elCell.innerHTML = '<a href="/schmangmt/ConcernTrackingAction.do?operation=replySetup&Issue_id='
					+ oRecord.getData("issue_id")
					+ '" ><img border=0 src="./images/Reply Icon.jpg" id="ConcernReply'
					+ oRecord.getData("IssueId")
					+ '" title="Click Reply Icon to reply on concern"></a>';
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};

		YAHOO.widget.DataTable.Formatter.ConcernReply = this.myCustomFormatterConcernReply;		
		YAHOO.widget.DataTable.Formatter.EditStudent = this.myCustomFormatterEditStudent;
		YAHOO.widget.DataTable.Formatter.DeleteStudent = this.myCustomFormatterDeleteStudent;
		YAHOO.widget.DataTable.Formatter.viewDetails = this.myCustomFormatterviewDetails;

    };
    
    
    var searchKey = trim(document.getElementById("searchbyname").value);
    var myColumnDefs = [ // sortable:true enables sorting
                         {key:"issue_title", label:"Issue Title", sortable:true,width: 200},
                         {key:"issue_submited_date", label:"Submited Date", sortable:true, width: 200},
                         {key:"priority", label:"Priority", sortable:false,width: 50},
                         {key:"current_status", label:"Current Status", sortable:false,width: 100},
                         {key:"category", label:"Category", sortable:false,width: 100},
                         {key:"viewDetails", label:"View",formatter: "viewDetails",sortable :false,resizeable :false},
                         {key:"ConcernReply", label:"Reply",formatter: "ConcernReply",sortable :false,resizeable :false},
                         {key:"EditStudent", label:"Edit",formatter: "EditStudent",sortable :false,resizeable :false},
                         {key:"DeleteStudent", label:"Delete",formatter: "DeleteStudent",sortable :false,resizeable :false} 
                     ];

    // Custom parser
    var stringToDate = function(sData) {
        var array = sData.split("-");
        return new Date(array[1] + " " + array[0] + ", " + array[2]);
    };
    searchpriority
    // DataSource instance
    var myDataSource = new YAHOO.util.DataSource("/schmangmt/jsp/concerntracker_json.jsp?ran=<%=r%>&searchKey="+searchKey+"&searchcategory="+searchcategory+"&searchpriority="+searchpriority+"&searchStatusKey="+searchStatusKey+"&");
    myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSON;
    myDataSource.responseSchema = {
        resultsList: "records",
        fields: ["issue_id","issue_title","issue_submited_date","priority","current_status","category"],
        
        metaFields: {
            totalRecords: "totalRecords" // Access to value in the server response
        }
    };
   
    // DataTable configuration
    var myConfigs = {
		paginator: new YAHOO.widget.Paginator({rowsPerPage:10, containers : ["dt-pag-nav"], template : "{PageLinks} {RowsPerPageDropdown}", rowsPerPageOptions : [10,25,50,100] }), // Enables pagination 
    	initialRequest: "sort=issue_title&dir=asc&startIndex=0&results=10", // Initial request for first page of data
        dynamicData: true, // Enables dynamic server-driven data
        sortedBy : {key:"issue_title", dir:YAHOO.widget.DataTable.CLASS_ASC} // Sets UI initial sort arrow
    };

     
    
    // DataTable instance
    var myDataTable = new YAHOO.widget.DataTable("bhmintegration", myColumnDefs, myDataSource, myConfigs);
    // Update totalRecords on the fly with value from server
    myDataTable.handleDataReturnPayload = function(oRequest, oResponse, oPayload) {
        oPayload.totalRecords = oResponse.meta.totalRecords;
        return oPayload;
    }
    
    return {
        ds: myDataSource,
        dt: myDataTable
    };
        
}
 searchByattribute();
 loadProperty();
</script>

<!--END SOURCE CODE FOR EXAMPLE =============================== -->









