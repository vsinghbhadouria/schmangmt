<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page import="java.util.*" %>
<%@ page import="java.text.*"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<html>
<head>
<title><tiles:getAsString name="title" ignore="true"/></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />     
	<script type='text/javascript' src='/schmangmt/dwr/engine.js'></script>
	<script type='text/javascript' src='/schmangmt/dwr/util.js'></script> 
	<link href="styles/default.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="./yui/build/fonts/fonts-min.css" />
	<link rel="stylesheet" type="text/css" href="./yui/build/container/assets/skins/sam/container.css" />
	<link rel="stylesheet" type="text/css" href="./yui/build/datatable/assets/skins/sam/datatable.css" />
	<link rel="stylesheet" type="text/css" href="./styles/tabview.css" />
	<link rel="stylesheet" type="text/css" href="./yui/build/paginator/assets/skins/sam/paginator.css" />
<script type="text/javascript">
function return2br(dataStr) {
    return dataStr.replace(/(\r\n|[\r\n])/g, "<br />");
}

function br2return(dataStr) {
	var myExp = new RegExp("<br />",'g');
    return dataStr.replace(myExp,"\r\n");
}

function removeOptions(allclasses)
{
	var i;
	for(i=allclasses.options.length-1;i>=0;i--)
	{
		allclasses.remove(i);
	}
}

	function trim(stringToTrim) {
		return stringToTrim.replace(/^\s+|\s+$/g, "");
	}

	function over(but, url) {
		document.getElementById(but).src = "./images/" + url;
	}

	function popUp(URL) {
		day = new Date();
		id = day.getTime();
		eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=450,height=355');");
	}
	
</script>

<!-- Style for tab view -->	
	<style type="text/css">
	.srchbox {
margin-top: -2px;	
border : 1px solid #CCCCCC;
background-color : #FFFFFF;
color : #0E1930;
font-size : 10px;
font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
font-weight : normal;
height:14px;
}		

		/* .yui-navset defaults to .yui-navset-top */
		.yui-skin-sam .yui-navset .yui-nav,
		.yui-skin-sam .yui-navset .yui-navset-top .yui-nav { /* protect nested tabviews from other orientations */
		    border:solid #2647a0; /* color between tab list and content */
		    border-width:0 0 0px;
		    Xposition:relative;
		    zoom:1;
		    
		    
		}
		
		.yui-skin-sam .yui-navset .yui-nav li,
		.yui-skin-sam .yui-navset .yui-navset-top .yui-nav li {
		    margin:0 0.16em 0 0; /* space between tabs */
		    padding:1px 0 0; /* gecko: make room for overflow */
		    zoom:1;
		}
		
		.yui-skin-sam .yui-navset .yui-nav .selected,
		.yui-skin-sam .yui-navset .yui-navset-top .yui-nav .selected { 
		    margin:0 0.16em -1px 0; /* for overlap */
		}
		
		.yui-skin-sam .yui-navset .yui-nav a,
		.yui-skin-sam .yui-navset .yui-navset-top .yui-nav a {
		    background:#d8d8d8 url(./images/navgradient.gif) repeat-x; /* tab background */
		    border:solid #a3a3a3;
		    border-width:0 1px;
		    color:#000;
		    text-decoration:none;
		}
		
		.yui-skin-sam .yui-navset .yui-nav a em,
		.yui-skin-sam .yui-navset .yui-navset-top .yui-nav a em {
		    border:solid #a3a3a3;
		    border-width:1px 0 0;
		    cursor:hand;
		    padding:0.25em .75em;
		    left:0; right: 0; bottom: 0; /* protect from other orientations */
		    top:-1px; /* for 1px rounded corners */
		    position:relative;
		}
		
		.yui-skin-sam .yui-navset .yui-nav .selected a,
		.yui-skin-sam .yui-navset .yui-nav .selected a:focus, /* no focus effect for selected */
		.yui-skin-sam .yui-navset .yui-nav .selected a:hover { /* no hover effect for selected */
		    background:#7c755f url(./images/drkgreyhead.gif) repeat-x left ; /* selected tab background */
		    color:#fff;
		}
		
		.yui-skin-sam .yui-navset .yui-nav a:hover,
		.yui-skin-sam .yui-navset .yui-nav a:focus {
		    background:#CCCCCC url(../../../../assets/skins/sam/sprite.png) repeat-x left -1300px; /* selected tab background */
		    outline:0;
		}
		
		.yui-skin-sam .yui-navset .yui-nav .selected a em {
		    padding:0.35em 0.75em; /* raise selected tab */
		}
		
		.yui-skin-sam .yui-navset .yui-nav .selected a,
		.yui-skin-sam .yui-navset .yui-nav .selected a em {
		    border-color:#243356; /* selected tab border color */
		}
		
		.yui-skin-sam .yui-navset .yui-content {
		    background:#FFFFFF; /* content background color */
		    
		}
		
		.yui-skin-sam .yui-navset .yui-content,
		.yui-skin-sam .yui-navset .yui-navset-top .yui-content {
		    border:1px solid #808080; /* content border */
		    border-top-color:#243356; /* different border color */
		    padding:0.25em 0.5em; /* content padding */
		    height: 150px;
		}

</style>
<!-- Listing Table-->
<style type="text/css">
/* custom styles for this example */
	.yui-skin-sam .yui-dt-liner {
	
	}

	.yui-skin-sam .yui-dt-hd table {
			border-left: 1px solid #7F7F7F;
			border-top: 1px solid #7F7F7F;
			border-right: 1px solid #7F7F7F;
			width: 100%;
	}

	.yui-skin-sam .yui-dt-bd table {
			border-left: 1px solid #7F7F7F;
			border-bottom: 1px solid #7F7F7F;
			border-right: 1px solid #7F7F7F;
			width: 100%;
	}

	.yui-skin-sam .yui-dt th {
			width: 90%;
	}


	.yui-skin-sam tr.yui-dt-odd{
			background-color:#ece9d8;
	}
	.yui-skin-sam tr.yui-dt-even td.yui-dt-asc,
	.yui-skin-sam tr.yui-dt-even td.yui-dt-desc{
			background-color:#ece9d8;
	}
	.yui-skin-sam tr.yui-dt-odd td.yui-dt-asc,
	.yui-skin-sam tr.yui-dt-odd td.yui-dt-desc{
			background-color:#d8d1b0;
	}
	.yui-skin-sam .yui-dt-list tr.yui-dt-even{
			background-color:#FFF;
	}
	.yui-skin-sam .yui-dt-list tr.yui-dt-odd{
			background-color:#FFF;
	}
	.yui-skin-sam .yui-dt-list tr.yui-dt-even td.yui-dt-asc,
	.yui-skin-sam .yui-dt-list tr.yui-dt-even td.yui-dt-desc{
			background-color:#ece9d8;
	}
	.yui-skin-sam .yui-dt-list tr.yui-dt-odd td.yui-dt-asc,
	.yui-skin-sam .yui-dt-list tr.yui-dt-odd td.yui-dt-desc{
			background-color:#ece9d8;
	}
	.yui-skin-sam th.yui-dt-highlighted,
	.yui-skin-sam th.yui-dt-highlighted a{
			background-color:#B2D2FF;
	}
	.yui-skin-sam tr.yui-dt-highlighted,.yui-skin-sam tr.yui-dt-highlighted td.yui-dt-asc,
	.yui-skin-sam tr.yui-dt-highlighted td.yui-dt-desc,.yui-skin-sam tr.yui-dt-even td.yui-dt-highlighted,
	.yui-skin-sam tr.yui-dt-odd td.yui-dt-highlighted{
				cursor:pointer;background-color:#B2D2FF;}
			.yui-skin-sam .yui-dt-list th.yui-dt-highlighted,.yui-skin-sam .yui-dt-list th.yui-dt-highlighted a{
				background-color:#B2D2FF;}
			.yui-skin-sam .yui-dt-list tr.yui-dt-highlighted,
			.yui-skin-sam .yui-dt-list tr.yui-dt-highlighted td.yui-dt-asc,
			.yui-skin-sam .yui-dt-list tr.yui-dt-highlighted td.yui-dt-desc,
			.yui-skin-sam .yui-dt-list tr.yui-dt-even td.yui-dt-highlighted,
			.yui-skin-sam .yui-dt-list tr.yui-dt-odd td.yui-dt-highlighted{
				cursor:pointer;background-color:#B2D2FF;}
			.yui-skin-sam th.yui-dt-selected,.yui-skin-sam th.yui-dt-selected a{
				background-color:#d8d1b0;}
			.yui-skin-sam tr.yui-dt-selected td,.yui-skin-sam tr.yui-dt-selected td.yui-dt-asc,
			.yui-skin-sam tr.yui-dt-selected td.yui-dt-desc{
				background-color:#d8d1b0;
				color:#FFF;
	}
	.yui-skin-sam tr.yui-dt-even td.yui-dt-selected,
	.yui-skin-sam tr.yui-dt-odd td.yui-dt-selected{
			background-color:#d8d1b0;
			color:#FFF;
	}
	.yui-skin-sam .yui-dt-list th.yui-dt-selected,
	.yui-skin-sam .yui-dt-list th.yui-dt-selected a{
			background-color:#d8d1b0;
	}
	.yui-skin-sam .yui-dt-list tr.yui-dt-selected td,
	.yui-skin-sam .yui-dt-list tr.yui-dt-selected td.yui-dt-asc,
	.yui-skin-sam .yui-dt-list tr.yui-dt-selected td.yui-dt-desc{
			background-color:#d8d1b0;
			color:#FFF;
	}
	.yui-skin-sam .yui-dt-list tr.yui-dt-even td.yui-dt-selected,
	.yui-skin-sam .yui-dt-list tr.yui-dt-odd td.yui-dt-selected{
			background-color:#d8d1b0;
			color:#FFF;
	}

</style>
	</head>
  <body style="margin-left: 0px; margin-top: 0px;"  class="yui-skin-sam">

  <table  style="border:1px dotted #2B1B17; width: 100%;">
  <tr>
             <td colspan="2" >
			  <tiles:insert attribute="userlogininfo"/>
			</td>
  </tr>
   <tr>
			<td style="width:10em;">
			<img src="./images/imagesCA4CN5KS.jpg" width="150%" height="120" alt="What We Do" align="left" >
			</td>
			<td style="width:90em;">
			<img src="./images/schoolname.gif" width="95%" height="120" alt="What We Do" align="right" >
			</td>			
	</tr>
  
 <tr>
  <% DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); %>
  <td align="left" colspan="2" >
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr><td ><tiles:insert attribute="topnav"/></td>
		</tr>
				
</table>
	</td>
</tr> 

 	<tr>
		<td colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  			<tr><td align="center">&nbsp;
<div id="loading" style="display: none; " ><img src="./images/loadingbar.gif"></div>

				</td></tr>
				<tr>
	    				<td valign="top" width="100%"><div id="paginated">
	<script type="text/javascript" src="./yui/build/connection/connection-min.js"></script>
	<script type="text/javascript" src="./yui/build/element/element-min.js"></script>
	<script type="text/javascript" src="./yui/build/paginator/paginator-min.js"></script>
	<script type="text/javascript" src="./yui/build/datasource/datasource-min.js"></script>
	<script type="text/javascript" src="./yui/build/datatable/datatable-min.js"></script>
	<script type="text/javascript" src="./yui/build/yahoo/yahoo-min.js"></script>
	<script type="text/javascript" src="./yui/build/yahoo/yahoo.js"></script>
	<script type="text/javascript" src="./yui/build/yahoo/yahoo-debug.js"></script>
	<script type="text/javascript" src="./yui/build/json/json-min.js"></script>
	<script type="text/javascript" src="./yui/build/container/container-min.js"></script>
	<script type="text/javascript" src="./yui/build/tabview/tabview-min.js"></script>
								<tiles:insert attribute="body"/>
						</div>
						</td>
						
				</tr>
			</table>
			
		</td>
	  </tr>
	

<tr>
	<td colspan="2">
		<tiles:insert attribute="footer"/>
	</td>
  </tr>
</table>

  </body>

</html>