<%@ page language="java" %>
<%@page import="com.sch.to.EduStaffTO"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
  <head>
 
  <title>Add Employees</title>  
  </head>
<script type='text/javascript' src="/schmangmt/dwr/interface/LibraryManagementManager.js"></script>
<script type='text/javascript' src='./scripts/schmanagmt.js'></script>
<script>
function getRackIds(){
	
	document.getElementById('rackdisplay').style.display='';
	var bookTypeId = document.getElementById('booktypeid').value;
	//alert(bookTypeId);
	var bookName = document.getElementById('tags').value;
	var numberofcopy = document.getElementById('numberofcopy').value;
	var rackid = document.getElementById('rackid');
	LibraryManagementManager.getRackIds(bookTypeId,function(rackdetail){
		for(var i=0;i<rackdetail.length;i++){
				var userOptions = rackdetail[i];
				userOptions = userOptions.split(',');
				rackid[i] = new Option(userOptions[1],userOptions[0]);
			}
	});
	
	LibraryManagementManager.genrateBookIds(bookName,numberofcopy,bookTypeId,function(bookIds){
		
		//alert(bookIds);
		document.getElementById('bookid').value=bookIds;
	});
	
}

function isValid(parm,val) {
	 for (var i=0; i<parm.length; i++) {
	 if (val.indexOf(parm.charAt(i),0) == -1) return false;
	 }
	return true;
	}
	
function checkValidation(){
	var numb = '0123456789';
	var bookName= document.getElementById('tags').value;
	var numberofcopy=document.getElementById('numberofcopy').value;
	var booktype = document.getElementById('booktypeid').value;
	//var rackname = document.getElementById('rackid').value;
	   var booknameflg    = true;
	   var noofcopyflg   = true;
	   var booktypeflg     = true;
	   if(bookName!=''){
		document.getElementById('bookNameError').style.display='none';
	    document.getElementById('bookNameMsg').style.display = 'none';
		booknameflg = true;
	  }
      else{
       over("bookNameError","error_bang.gif");
       document.getElementById('bookNameError').style.display='inline';
       document.getElementById('bookNameMsg').innerHTML ='Book Name should not be blank';
       document.getElementById('bookNameMsg').style.display = 'inline';
       booknameflg = false;
 	  }
	
	  if(numberofcopy!=''){
		  document.getElementById('copyError').style.display='none';
	      document.getElementById('copyMsg').style.display = 'none';
	      if(isValid(numberofcopy,numb)){
	    	noofcopyflg = true ;
		  }
	      else{	
	       over("copy	Error","error_bang.gif");
	       document.getElementById('copyError').style.display='inline';
	       document.getElementById('copyMsg').innerHTML =' Number of copy should be numeric';
	       document.getElementById('copyMsg').style.display = 'inline';
	       noofcopyflg = false;
	 	  }
	 }
	  else{ 
		over("copyError","error_bang.gif");
      	document.getElementById('copyError').style.display='inline';
      	document.getElementById('copyMsg').innerHTML =' Number of copy should not be blank';
     	document.getElementById('copyMsg').style.display = 'inline';
      	noofcopyflg = false;
	  }
	  
	  
		if(booktype!=-1){
			document.getElementById('booktypeError').style.display='none';
		    document.getElementById('booktypeMsg').style.display = 'none';
		    booktypeflg = true;
		  }
	      else{
	       over("booktypeError","error_bang.gif");
	       document.getElementById('booktypeError').style.display='inline';
	       document.getElementById('booktypeMsg').innerHTML ='Book type should not be none';
	       document.getElementById('booktypeMsg').style.display = 'inline';
	       booktypeflg = false;
	 	  }

		if(booknameflg && noofcopyflg && booktypeflg)
			return true;
		else
			return false;
}


</script>
  <body>
    
    <html:errors />
      <html:form  action="/LibraryManagementAction.do?operation=save" method ="post">
        <%

                         String struserid      = request.getParameter("userid");
                                               
     %>
<div style="border: 1px solid;width:97%"  >
 <table align="center" cellspacing="2" cellpadding="5" width=100% order="1">
	<tr><td class="mainhead" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="app.sch.addbook.addformtitle"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   </td></tr>
         <tr>
          <td align=right width="3%" ><img src="./images/blank.png" alt="" id="bookNameError"/></td><td align=left  width=25%><b><bean:message key="app.sch.addbook.bookname"/></b></td>
          <td align="left">
			<jsp:include page="autocomplete.jsp"/>
         </td> 

         </tr>
         
<tr>
 <td align=right width="3%" ><img src="./images/blank.png" alt="" id="copyError"/></td><td align=left  width=25%><b><bean:message key="app.sch.addbook.numberofcopy"/></b></td>
          <td><html:text property="numberofcopy" size="28" styleClass="srchbox" styleId="numberofcopy" style="border:1px solid #000000" value="" />&nbsp;<div style ='display:none; color: red;' id="copyMsg"></div></td>
          
        </tr>
        <tr>
                       
 		<tr>
          <td align=right width="3%" ><img src="./images/blank.png" alt="" id="booktypeError"/></td><td align=left  width=25%><b><bean:message key="app.sch.addbook.booktype"/></b></td>
		  <td align=left>
			<html:select property="booktypename" style="border:1px solid #000000" styleId = "booktypeid" value="-1" onchange="getRackIds()" >
				 <option value="-1">none</option>  
				<html:options collection="bookTypes"  property="booktypeid"  labelProperty="booktypename" />
			</html:select>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="booktypeMsg"></div>
		 </td>
         </tr>
         <tr>
          <td align=right width="3%" ><img src="./images/blank.png" alt="" id="previousexpError"/></td><td align=left  width=25%><b><bean:message key="app.sch.addbook.bookid"/></b></td>
          <td align="left"><html:textarea property="bookid" styleId="bookid" style="border:1px solid #000000" value="" rows="5" cols="25"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="previousexpdMsg"></div></td>
         </tr>
         
          <tr id="rackdisplay" style="display:none">
          <td align=right width="3%"><img src="./images/blank.png" alt="" id="highereduError"/></td><td align=left  width=25%><b><bean:message key="app.sch.addbook.rackid"/></b></td>
		  <td align=left>
			<html:select property="rackid" style="border:1px solid #000000" styleId = "rackid" value="-1" onchange="displayCountry()" >
			</html:select>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="highereduMsg"></div></td>
         </tr>
			
		
</table>


  <table align="center" cellspacing=2 cellpadding=5 width=97%> 
   <tr>   <td colspan="2" align="center" >
            <input type="submit" value ="Submit" onclick="return checkValidation();"/>
            <input type="button" value ="Cancel"  onclick="javascript:history.back();" />
            </td>
        
   </tr>
  </table>
  </div> 
<%--  <html:hidden property="password" styleId="password" styleClass="srchbox" style="border:1px solid #000000"  />
 --%>    </html:form>


</body>
</html>