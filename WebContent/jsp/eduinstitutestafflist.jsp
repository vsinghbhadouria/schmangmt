<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import="java.util.*"%>
<%Random generator = new Random();
int r = generator.nextInt();
%>
<%@page import="com.sch.delegates.EduStaffManager"%>
<!--begin custom header content for this example-->
<style type="text/css">
/* custom styles for this example */
#yui-history-iframe {
  position:absolute;
  top:0; left:0;
  width:1px; height:1px; /* avoid scrollbars */
  visibility:hidden;
}
</style>

<!--end custom header content for this example-->

<!--BEGIN SOURCE CODE FOR EXAMPLE =============================== -->
<table align="center" width="97%" cellpadding="0" cellspacing="0">
<tr class="mainhead" height="25px"><td>
&nbsp;&nbsp;<bean:message key="app.sch.eduinstitutestafflist.addformtitle"/>&nbsp;
<a href="/schmangmt/EduStaffAction.do?operation=createSetup" ><img border="0" src="./images/add2.jpg" id="addSchl" width="7%" height="60%" align="top"></a>
</td>
<td align="right" valign="middle">
				
					<input type="text" onkeypress="if(event.keyCode==13) searchShop()" name="shopbyseach" id="shopbyseach" size="20" maxlength="25"  class="srchbox" />&nbsp;&nbsp;
					<a href="#" onclick="searchShop()">
						<img src="./images/searcharrow.jpg" align="top">
					</a>&nbsp;&nbsp;
				
				
</td></tr>
<tr><td colspan="2">
	<div id="bhmintegration" style="width:100%;"></div>
	<div id="dt-pag-nav" align ="center"></div>
</td></tr></table>

<script type="text/javascript">
//Table cell formating for Buttons
 function searchShop() {
    // Column definitions
   YAHOO.example.CustomFormatting = new function() {
    	this.myCustomFormatterEditInstituteStaff = function(elCell, oRecord, oColumn,
				oData) {
			
			if(true){

				elCell.innerHTML = '<a href="/schmangmt/EduStaffAction.do?operation=updateSetup&User_id='
					+ oRecord.getData("user_id")+'&Emp_status=A'
					+ '" onMouseOut=over("EditInstituteStaff'
					+ oRecord.getData("user_id")
					+ '","edit.gif") onMouseOver=over("EditInstituteStaff'
					+ oRecord.getData("user_id")
					+ '","edit.gif")><img border=0 src="./images/edit.gif" id="EditInstituteStaff'
					+ oRecord.getData("user_id")
					+ '" title="Click Icon to edit staff details"></a>';
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};
		
		this.myCustomFormatterDeleteInstituteStaff = function(elCell, oRecord, oColumn,
				oData) {
			
			if(true){

				elCell.innerHTML = '<a href="/schmangmt/EduStaffAction.do?operation=delete&User_id='
					+ oRecord.getData("user_id")
					+ '" onMouseOut=over("DeleteInstituteStaff'
					+ oRecord.getData("user_id")
					+ '","close1.gif") onMouseOver=over("DeleteInstituteStaff'
					+ oRecord.getData("user_id")
					+ '","close1.gif")><img border=0 src="./images/close1.gif" id="DeleteInstituteStaff'
					+ oRecord.getData("user_id")
					+ '" title="Click Icon to Delete Institute Staff"></a>';
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};
		YAHOO.widget.DataTable.Formatter.EditInstituteStaff = this.myCustomFormatterEditInstituteStaff;
		YAHOO.widget.DataTable.Formatter.DeleteInstituteStaff = this.myCustomFormatterDeleteInstituteStaff;

    };
    
    var searchKey = trim(document.getElementById("shopbyseach").value);
    var myColumnDefs = [ // sortable:true enables sorting
                         {key:"first_name", label:"Name", sortable:true},
                         {key:"Email", label:"Email", sortable:true},
                         {key:"emp_title", label:"Level", sortable:true, width: 150},
                        // {key:"City", label:"City", sortable:true},
                         //{key:"District", label:"District", sortable:true},
                         //{key:"State", label:"State", sortable:true},
                         //{key:"Country", label:"Country", sortable:true},
                        // {key:"Zone", label:"Zone", sortable:true},
                         {key:"Phone", label:"Phone", sortable:false},
                        //{key:"Zipcode", label:"Zipcode", sortable:true},
                         {key:"EditInstituteStaff", label:"Edit",formatter: "EditInstituteStaff",sortable :false,resizeable :false},
                         {key:"DeleteInstituteStaff", label:"Delete",formatter: "DeleteInstituteStaff",sortable :false,resizeable :false} 
                     ];

    // Custom parser
    var stringToDate = function(sData) {
        var array = sData.split("-");
        return new Date(array[1] + " " + array[0] + ", " + array[2]);
    };
    
    // DataSource instance
    var myDataSource = new YAHOO.util.DataSource("/schmangmt/jsp/eduinstitutestaff_json.jsp?ran=<%=r%>&searchKey="+searchKey+"&");
    myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSON;
    myDataSource.responseSchema = {
        resultsList: "records",
        fields: ["first_name","emp_title","Phone","user_id","Email"],
        
        metaFields: {
            totalRecords: "totalRecords" // Access to value in the server response
        }
    };
   
    // DataTable configuration
    var myConfigs = {
		paginator: new YAHOO.widget.Paginator({rowsPerPage:10, containers : ["dt-pag-nav"], template : "{PageLinks} {RowsPerPageDropdown}", rowsPerPageOptions : [10,25,50,100] }), // Enables pagination 
    	initialRequest: "sort=first_name&dir=asc&startIndex=0&results=10", // Initial request for first page of data
        dynamicData: true, // Enables dynamic server-driven data
        sortedBy : {key:"first_name", dir:YAHOO.widget.DataTable.CLASS_ASC} // Sets UI initial sort arrow
    };

     
    
    // DataTable instance
    var myDataTable = new YAHOO.widget.DataTable("bhmintegration", myColumnDefs, myDataSource, myConfigs);
    // Update totalRecords on the fly with value from server
    myDataTable.handleDataReturnPayload = function(oRequest, oResponse, oPayload) {
        oPayload.totalRecords = oResponse.meta.totalRecords;
        return oPayload;
    }
    
    return {
        ds: myDataSource,
        dt: myDataTable
    };
        
}
 searchShop();
</script>

<!--END SOURCE CODE FOR EXAMPLE =============================== -->









