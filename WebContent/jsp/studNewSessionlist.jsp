<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import="java.util.*"%>
<%Random generator = new Random();
int r = generator.nextInt();
%>
<%@page import="com.sch.delegates.EduStudentManager"%>
<!--begin custom header content for this example-->
<style type="text/css">
/* custom styles for this example */
#yui-history-iframe {
  position:absolute;
  top:0; left:0;
  width:1px; height:1px; /* avoid scrollbars */
  visibility:hidden;
}
</style>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentManager.js"></script>
<!--end custom header content for this example-->

<!--BEGIN SOURCE CODE FOR EXAMPLE =============================== -->
<table align="center" width="97%" cellpadding="0" cellspacing="0" >
<tr class="mainhead" height="25px"><td>
		&nbsp;&nbsp;<b><bean:message key="app.sch.studNewSessionlist.listformtitle"/></b>
		&nbsp;					
		</td>
		<td align="right" valign="middle" class="mainhead">
					<input type="text" onkeypress="if(event.keyCode==13) searchByattribute()" name="searchbyname" id="searchbyname" size="20" maxlength="25"  class="srchbox" />&nbsp;&nbsp;
					<a href="#" onclick="searchByattribute()">
						<img src="./images/searcharrow.jpg" align="top">
					</a>&nbsp;&nbsp;
        </td>
		</tr>
	  
<tr align="left" style="background-image: url('./images/ltbrgrey1.gif')">
<td colspan="2">    
					<Label for ="name"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Class &nbsp;&nbsp; </b></Label>
			        <select name ="class" id= "class" onchange="searchByattribute();">
					</select>
</td>
	</tr>	
<tr><td >
&nbsp;</td></tr>
<tr><td colspan="2">
	<div id="bhmintegration" style="width:100%;"></div>
	<div id="dt-pag-nav" align ="center"></div>
</td></tr></table>

<script type="text/javascript">
var searchBysessionkey = "";
var searchByclasskey = "";
 	function loadClass()
    {
    	var classoption = document.getElementById("class");
    	EduStudentManager.getClasses(function(classinfo){
				for(var xx=0;xx<classinfo.length;xx++)
				{
					var catidvalue = classinfo[xx].split(",");
					classoption[xx]= new Option(catidvalue[1],catidvalue[0]); 						
				}
				 
        	});
       
    }
//Table cell formating for Buttons
 function searchByattribute() {
	 
	 searchBysessionkey = '';
	 searchByclasskey = trim(document.getElementById("class").value);
    // Column definitions
   YAHOO.example.CustomFormatting = new function() {
    	this.myCustomFormatterNewSession = function(elCell, oRecord, oColumn,
				oData) {
			
			if(true){
				elCell.innerHTML = '<a href="/schmangmt/EduStudentReportAction.do?operation=createNewStudentSetup&User_id='
					+ oRecord.getData("student_id")+'&session_id='
					+ oRecord.getData("session_id")+'&class_id='
					+ oRecord.getData("class_id")
					+ '" onMouseOut=over("NewSession'
					+ oRecord.getData("student_id")
					+ '","AdmissionIcon.jpg") onMouseOver=over("NewSession'
					+ oRecord.getData("student_id")
					+ '","AdmissionIcon.jpg")><img border=0 src="./images/AdmissionIcon.jpg" id="NewSession'
					+ oRecord.getData("student_id")
					+ '" title="Click Icon to Assign New Session to Student"></a>';
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};
		YAHOO.widget.DataTable.Formatter.NewSession = this.myCustomFormatterNewSession;
    };
    
    var searchKey = trim(document.getElementById("searchbyname").value);
    var myColumnDefs = [ // sortable:true enables sorting
                         {key:"student_name", label:"Student Name", sortable:true,width: 200},
                         {key:"dept_name", label:"Department Name", sortable:true,width: 200},
                         {key:"class", label:"Class", sortable:true, width: 100},
                         {key:"status", label:"Result", sortable:false,width: 50},
                         {key:"division", label:"division", sortable:false,width: 100},
                         {key:"NewSession", label:"New Session",formatter: "NewSession",sortable :false,resizeable :false}
                     ];

    // Custom parser
    var stringToDate = function(sData) {
        var array = sData.split("-");
        return new Date(array[1] + " " + array[0] + ", " + array[2]);
    };
    
    // DataSource instance
    var myDataSource = new YAHOO.util.DataSource("/schmangmt/jsp/studNewSessionlist_json.jsp?ran=<%=r%>&searchBysessionkey="+searchBysessionkey+"&searchByclasskey="+searchByclasskey+"&searchKey="+searchKey+"&");
    myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSON;
    myDataSource.responseSchema = {
        resultsList: "records",
        fields: ['student_id', 'student_name','dept_name','class','status','division','class_id','dept_id','session_id'],
        
        metaFields: {
            totalRecords: "totalRecords" // Access to value in the server response
        }
    };
   
    // DataTable configuration
    var myConfigs = {
		paginator: new YAHOO.widget.Paginator({rowsPerPage:10, containers : ["dt-pag-nav"], template : "{PageLinks} {RowsPerPageDropdown}", rowsPerPageOptions : [10,25,50,100] }), // Enables pagination 
    	initialRequest: "sort=student_name&dir=asc&startIndex=0&results=10", // Initial request for first page of data
        dynamicData: true, // Enables dynamic server-driven data
        sortedBy : {key:"student_name", dir:YAHOO.widget.DataTable.CLASS_ASC} // Sets UI initial sort arrow
    };

     
    
    // DataTable instance
    var myDataTable = new YAHOO.widget.DataTable("bhmintegration", myColumnDefs, myDataSource, myConfigs);
    // Update totalRecords on the fly with value from server
    myDataTable.handleDataReturnPayload = function(oRequest, oResponse, oPayload) {
        oPayload.totalRecords = oResponse.meta.totalRecords;
        return oPayload;
    }
    
    return {
        ds: myDataSource,
        dt: myDataTable
    };
        
}
 searchByattribute();
 loadClass();
</script>

<!--END SOURCE CODE FOR EXAMPLE =============================== -->









