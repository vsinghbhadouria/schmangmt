<%@ page language="java" %>
<%@page import="com.sch.to.EduStaffTO"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
  <head>
 
  <title>Add Employees</title>  
  </head>
<script type='text/javascript' src="/schmangmt/dwr/interface/LibraryManagementManager.js"></script>
<script type='text/javascript' src='./scripts/schmanagmt.js'></script>
<script>
/* function getRackIds(){
	
	document.getElementById('rackdisplay').style.display='';
	var bookTypeId = document.getElementById('booktypeid').value;
	//alert(bookTypeId);
	var bookName = document.getElementById('tags').value;
	var numberofcopy = document.getElementById('numberofcopy').value;
	var rackid = document.getElementById('rackid');
	LibraryManagementManager.getRackIds(bookTypeId,function(rackdetail){
		for(var i=0;i<rackdetail.length;i++){
				var userOptions = rackdetail[i];
				userOptions = userOptions.split(',');
				rackid[i] = new Option(userOptions[1],userOptions[0]);
			}
	}); */
	
	function getBookIds(){

		var bookName = document.getElementById('tags').value;
		var bookid = document.getElementById('bookid');
		LibraryManagementManager.getAvailableBookIds(bookName,function(bookIds){
			document.getElementById('bookdisplay').style.display='none';
			document.getElementById('booktype').style.display='none';
			if(bookIds.length>0){
            	removeOptions(bookid);
            	document.getElementById('bookdisplay').style.display='';
				for(var i=0;i<bookIds.length;i++){
					var userOptions = bookIds[i];
					userOptions = userOptions.split(',');
					bookid[i] = new Option(userOptions[1],userOptions[1]);
				}
		LibraryManagementManager.getAvailableBookcountandtype(bookName,function(bookdetail){
			if(bookdetail.length>0){
				//document.getElementById('availablebookcount').style.display='';
				document.getElementById('booktype').style.display='';
				//document.getElementById('rackname').style.display='';
				breakDetails = bookdetail.split(',');
				document.getElementById('availability_count').innerHTML=breakDetails[0];
				document.getElementById('rackid').innerHTML=breakDetails[1];
				document.getElementById('booktypename').innerHTML=breakDetails[2];
				document.getElementById('bookmasterid').value=breakDetails[3];
			}
		
		});	

				
            }
            else{
            	//document.getElementById('rackdisplay').style.display='none';
            	//removeOptions(rackid);
            }
		});
		
		document.getElementById('bookname').value=bookName;
	}
	
	function getStudentData(){
		
		var userId = document.getElementById('user_id').value;
		LibraryManagementManager.getStudentDataByUserId(userId,function(studentData){
			if(studentData.length>0){
				document.getElementById('studentclassname').style.display='';
				document.getElementById('emailstatus').style.display='';
				//document.getElementById('rackname').style.display='';
				document.getElementById('Studentprofile').style.display='';
				student = studentData.split(',');
				document.getElementById('studentname').innerHTML=student[0];
				document.getElementById('classname').innerHTML=student[1];
				document.getElementById('s_emailid').innerHTML=student[2];
				document.getElementById('status').innerHTML=student[3];
				document.getElementById('username').value=student[0];
				document.getElementById('emailid').value=student[2];
				
				var x = document.createElement("IMG");
			    x.setAttribute("src", "profile-pics/"+userId +".JPG");
			    x.setAttribute("width", "200");
			    x.setAttribute("width", "130");
			    x.setAttribute("alt", "Student's profile photo is missing");
			    x.setAttribute("align", "right");
			    x.setAttribute("style","border:1px dotted #2B1B17;")
			    var pp=document.getElementById('Studentprofile').innerHTML;
			    if(pp.trim()===''){
			    document.getElementById('Studentprofile').appendChild(x);
			    }
			    else{
			    	document.getElementById('Studentprofile').innerHTML='';
			        document.getElementById('Studentprofile').appendChild(x);
			    }
			}
		else{
			document.getElementById('studentclassname').style.display='none';
			document.getElementById('emailstatus').style.display='none';
			document.getElementById('Studentprofile').style.display='none';
		}
	});
		
	}
	
	function getTeacherData(){
		
		var userId = document.getElementById('empId').value;
		LibraryManagementManager.getTeacherDataByUserId(userId,function(teacherData){
			if(teacherData.length>0){
				document.getElementById('teacherqualification').style.display='';
				document.getElementById('teacheremailphone').style.display='';
				document.getElementById('Teacherprofile').style.display='';
				//document.getElementById('rackname').style.display='';
				teacher = teacherData.split(',');
				document.getElementById('employeename').innerHTML=teacher[0];
				document.getElementById('qualification_title').innerHTML=teacher[1];
				document.getElementById('t_emailid').innerHTML=teacher[2];
				document.getElementById('t_phoneno').innerHTML=teacher[3];
				document.getElementById('username').value=teacher[0];
				document.getElementById('emailid').value=teacher[2];
				var x = document.createElement("IMG");
				    x.setAttribute("src", "profile-pics/"+userId +".JPG");
				    x.setAttribute("width", "200");
				    x.setAttribute("width", "130");
				    x.setAttribute("alt", "Teacher's profile photo is missing");
				    x.setAttribute("align", "right");
				    x.setAttribute("style","border:1px dotted #2B1B17;")
				    var pp=document.getElementById('Teacherprofile').innerHTML;
				    if(pp.trim()===''){
				    document.getElementById('Teacherprofile').appendChild(x);
				    } else{
				    	document.getElementById('Teacherprofile').innerHTML='';
				        document.getElementById('Teacherprofile').appendChild(x);
				    }
				}
			else{
				document.getElementById('teacherqualification').style.display='none';
				document.getElementById('teacheremailphone').style.display='none';
				document.getElementById('Teacherprofile').style.display='none';
			}
			
			});
		}
	
	
	function selectOption(value) {
	   	
		var valueOption = document.getElementById(value).value;
		
		if(valueOption=='teacher'){
			document.getElementById('teacherData').style.display='';
			document.getElementById('studentData').style.display='none';
		}
		else{
			document.getElementById('studentData').style.display='';
			document.getElementById('teacherData').style.display='none';
		}
	}
</script>
  <body>
    
    <html:errors />
      <html:form  action="/LibraryManagementAction.do?operation=assignBooks" method ="post">
        <%

                         String struserid      = request.getParameter("userid");
                                               
     %>

  <div  align=left width="70">
      <fieldset id="optionsContainer" width="70">
		  <legend><b><bean:message key="app.sch.issuebook.checkavailability"/></b></legend>
		<table align=left border=0 width=100%>
		<tr>
		<td width=50%>	
		<table align=left border=0 width=100%>
		
		 <tr>
          <td align=left width="3%" ><img src="./images/blank.png" alt="" id="bookNameError"/></td>
          <td align=left  width=10%><b><bean:message key="app.sch.addbook.bookname"/></b></td>
          <td align="left" width=15%><jsp:include page="autocomplete.jsp"/></td> 
          <td align=left  width=15%></td>
          <td align="left" width=25%></td>
         </tr>
		
		         
         <tr id="bookdisplay" style="display:none">
          <td align=right width="3%"><img src="./images/blank.png" alt="" id="highereduError"/></td>
          <td align=left  width=10%><b><bean:message key="app.sch.addbook.bookid"/></b></td>
		  <td align=left><html:select property="bookid" style="border:1px solid #000000" styleId = "bookid" value="" ></html:select></td>
 		  <td align=left  width=15%><b><bean:message key="app.sch.addbook.numberofcopy"/></b></td>
          <td id="availability_count" align="left" width=25%></td>
        </tr>
		
	    <tr id="booktype" style="display:none">
	 		<td align=right width="3%" ><img src="./images/blank.png" alt="" id="copyError"/></td>
	 		<td align=left  width=10%><b><bean:message key="app.sch.addbook.booktype"/></b></td>
	 		<td id="booktypename" align="left" width=25%></td>
	        <td align=left  width=15%><b><bean:message key="app.sch.addbook.rackid"/></b></td>
	        <td id="rackid" align="left" width=25%></td>
        </tr>
		        
         		
		</table>
		</td>
		
		<td width=30%>
		<table><tr><td>	
	<!-- 	<img src="images/DSCN1420.JPG"
	width="200" height="130" alt="What We Do" align="right"/> -->
		</td></tr></table>
		</td>
		</tr>
		</table>	
		</fieldset>
		</div>
	
	<div align=left width="50">
		<fieldset id="optionsContainer" width="50">
			<legend>
				<b>User Type</b>
			</legend>

			<table align=center>

				<tr id="meetingOption">
				
				<td align="left" width="15%"><INPUT TYPE="radio"
						name="meeting" styleClass="srchbox" Id="student"
						style="border: 1px solid #000000" value="student"
						onchange="selectOption(this.value);" checked /><b>Students</b></td>
				<td align="left" width="15%"><INPUT TYPE="radio"
						name="meeting" styleClass="srchbox" Id="teacher"
						style="border: 1px solid #000000" value="teacher"
						onchange="selectOption(this.value);"  /><b>Teachers</b></td>
				</tr>

			</table>



 <div  align=left width="70" style="display:inline" id="studentData">
      <fieldset id="optionsContainer" width="70">
		  <legend><b><bean:message key="app.sch.returnissuebook.userprofile"/></b></legend>
		<table align=left border=0 width=100%>
		<tr>
		<td width=50%>	
		<table align=left border=0 width=100%>

	 	<tr>
          <td align=left width="3%"><img src="./images/blank.png" alt="" id="bookNameError"/></td>
          <td align=left  width=10%><b><bean:message key="app.sch.studentprofile.user_id"/></b></td>
          <td align="left" width=15%><input type="text" id="user_id" name="user_id"style="border:1px solid #000000" size=18 name="user_id"  onkeyup="getStudentData();" onmouseover="getStudentData();"/></td>
          <td align="left" width=15%></td>
		  <td align="left" width=25%></td>
      	</tr>
      	
         
		<tr id="studentclassname"  style="display:none">
				<td align="right" width="3%"></td>
				<td align="left" width=10%><b><bean:message key="app.sch.prepareReportCard.studentname"/></b></td>
                <td id="studentname" align="left" width=25%>
				<td align="left" width=15%><b><bean:message key="app.sch.issuebook.class"/></b></td>
				<td id="classname" align="left" width=25%></td>
        </tr>

        <tr id="emailstatus" style="display:none">
				<td align="left" width="3%"></td>
				<td align="left" width=10%><b><bean:message key="app.sch.studentprofile.emailid"/></b></td>
                <td id="s_emailid" align="left" width=25%>
				<td align="left" width=15%><b><bean:message key="app.sch.issuebook.status"/></b></td>
				<td id="status" align="left" width=25%></td>
        </tr>
        	
       		
		</table>
		</td>
		
		<td width=30%>
		<table><tr><td id="Studentprofile">	
		</td></tr></table>
		</td>
		</tr>
		</table>	
		</fieldset>
		</div>
  
  <div  align=left width="70" style="display:none" id="teacherData">
      <fieldset id="optionsContainer" width="70">
		  <legend><b><bean:message key="app.sch.returnissuebook.userprofile"/></b></legend>
		<table align=left border=0 width=100%>
		<tr>
		<td width=50%>	
		<table align=left border=0 width=100%>

		<tr id="departmentnametr" >
				<td align="left" width="2%"></td>
				<td align="left" width=15% rowspan="1" ><b><bean:message key="app.sch.employeeprofile.employeeno"/></b></td>
	      		<td align="left"><input type="text" id="empId" style="border:1px solid #000000" size=18 name="empId"  onkeyup="getTeacherData();" onmouseover="getTeacherData();"/></td>
   
		</tr>	

		
		<tr id="teacherqualification" style="display:none;">
				<td align="left" width="3%"></td>
				<td align="left" width=10%><bean:message key="app.sch.employeeprofile.employeename"/></td>
                <td id="employeename" align="left" width=25%>
				<td align="left" width=15%><bean:message key="app.sch.addeduinstituestaff.higherqualification"/></td>
				<td id="qualification_title" align="left" width=25%></td>
        </tr>
        <tr id="teacheremailphone" style="display:none;">
				<td align="left" width="3%"></td>
				<td align="left" width=10%><bean:message key="app.sch.studentprofile.emailid"/></td>
                <td id="t_emailid" align="left" width=25%>
				<td align="left" width=15%><bean:message key="app.sch.studentprofile.phoneno"/></td>
				<td id="t_phoneno" align="left" width=25%></td>
        </tr>

		</table>
		</td>
		<td width=30%>
		<table><tr><td id="Teacherprofile">	

		</td></tr></table>
		</td>
</tr>
</table>
</fieldset>
</div>

		<table style="" align="center" border=0 cellspacing=2 cellpadding=3 width=100%  > 
				<tr> 
					<td colspan="2" align="center" >
					<input type="submit" value ="Submit" onclick="return checkValidation();"/>
					<input type="button" value ="Cancel"  onclick="javascript:history.back();"/>
				</td>
			   </tr>
			</table>
		</fieldset>
		
	</div>
	
		
   
  
  
<html:hidden property="bookmasterid" styleId="bookmasterid" styleClass="srchbox" style="border:1px solid #000000" value="" />
<html:hidden property="bookname" styleId="bookname" styleClass="srchbox" style="border:1px solid #000000" value="" />
<html:hidden property="username" styleId="username" styleClass="srchbox" style="border:1px solid #000000" value="" />
<html:hidden property="emailid" styleId="emailid" styleClass="srchbox" style="border:1px solid #000000" value="" />
    </html:form>


</body>
</html>