<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import ="java.text.*"%>
<%@ page import ="java.util.*"%>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentReportManager.js"></script>
<script type='text/javascript'>

function isRecordExist(){

  var student_id = document.getElementById("studentid").value;
  var class_id = document.getElementById("classid").value;
  var session_id = document.getElementById("sessionid").value;
  
  document.getElementById('isrecordMsg').style.display = 'none';
  document.getElementById('isobtainedMsg').style.display = 'none';
  document.getElementById('isoutofMsg').style.display = 'none';
  document.getElementById('iscalMsg').style.display = 'none';
  document.getElementById('isoutofobtainedMsg').style.display = 'none';
  var reqCome="prepareReportCard";
  EduStudentReportManager.isRecordExist(student_id,class_id,session_id,reqCome,{
  async:false,
  callback: function(recordFlag){

  if(recordFlag!=false){
  //alert("ififififif");
  document.getElementById('isrecordExistval').value='';	  
  document.getElementById('isrecordMsg').innerHTML ='Record already inserted for Student ID '+student_id;
  document.getElementById('isrecordMsg').style.display = 'inline';
  document.getElementById('isrecordExistval').value='No';
  }
  else{
  //alert("esle");
  document.getElementById('isrecordExistval').value='';
  document.getElementById('isrecordExistval').value='Yes';
  }
}
 });
  
  var obtainedMsg =true;
  var outofMsg =true;
  var calMsg =true;
  var outofobtainedMsg =true;
  var flag =true;   
  var totalRowId=document.getElementById('subject_count').value;
  		   for(var xx=1;xx<=totalRowId;xx++){
		   if(document.getElementById('markObtained'+xx).value==''){
			document.getElementById('isobtainedMsg').innerHTML='Mark obtained field should not be blank';
			document.getElementById('isobtainedMsg').style.display = 'inline';
			obtainedMsg=false;
		     break;
		   }
		}
		  for(var xx=1;xx<=totalRowId;xx++){
		   if(document.getElementById('outof'+xx).value==''){
			document.getElementById('isoutofMsg').innerHTML='Out of mark field should not be blank';
			document.getElementById('isoutofMsg').style.display = 'inline';
			outofMsg=false;
		     break;
		   }
		}
		
		 for(var xx=1;xx<=totalRowId;xx++){
		   if(document.getElementById('outof'+xx).value!='' && document.getElementById('markObtained'+xx).value!=''){
		   var markObtained = document.getElementById('markObtained'+xx).value;
		   var outof = document.getElementById('outof'+xx).value;
		   if(parseInt(markObtained)>parseInt(outof)){
			document.getElementById('isoutofobtainedMsg').innerHTML='Out of mark not more then obtained mark';
			document.getElementById('isoutofobtainedMsg').style.display = 'inline';
			outofobtainedMsg=false;
		     break;
				}
			}
		}
		
	
	var percentage=document.getElementById('percentage').value;
			
	if(percentage==''){
	 document.getElementById('iscalMsg').innerHTML='Please calculate the report before submit';
	 document.getElementById('iscalMsg').style.display = 'inline';
	 calMsg=false;
	}	

	var strFlag = document.getElementById('isrecordExistval').value;  
   //alert("obtainedMsg "+obtainedMsg+"outofMsg "+outofMsg+"calMsg "+calMsg+"strFlag "+strFlag+"outofobtainedMsg "+outofobtainedMsg);
  if(strFlag.toString()=='Yes' && obtainedMsg && outofMsg && calMsg && outofobtainedMsg && outofobtainedMsg){
     flag = true;
  }
  else{
  flag = false;
  }
  return flag;
 } 
</script>

<%
Integer userid= Integer.valueOf((String)request.getSession().getAttribute("user_id"));
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");	
%>
<html:form action="/EduStudentReportAction.do?operation=saveResult" method="Post">
<div  align=left width="50">
      <fieldset id="optionsContainer" width="50">
		  <legend><b>Student Report Card</b></legend>
	
		<table align=center >
		
		<tr id="departmentnametr" >
				<td align="left" width="2%"></td>
				<td align="left" width=15% rowspan="1" ><bean:message key="app.sch.prepareReportCard.rollnumber"/></td>
				<td id="student_id" align="left" width=25%></td>
				<td align="left" width=15%><bean:message key="app.sch.prepareReportCard.departmentname"/></td>
				<td id="departmentname" align="left" width=25%></td>
		</tr>	

		
		<tr id="studentnametr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.studentname"/></td>
                <td id="studentname" align="left" width=10%>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.fathername"/></td>
				<td id="fathername" align="left" width=10%></td>
        </tr>
		<tr id="classtr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.class"/></td>
                <td id="classname" align="left" width=10%>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.dateofbirth"/></td>
				<td id="dateofbirth" align="left" width=10%></td>
        </tr>
        		
		</table>
		
		 <div  align=left>
        <fieldset id="optionsContainer11">
		  <legend ><b><bean:message key="app.sch.prepareReportCard.fillobtained"/></b></legend>
		  <table border=0 align=center id="errortab">
		  <tr id="errortr" >
				
				<td align="left" width=300><div style ='display:none; color: red;' id="isrecordMsg"></div></td>
				
		  </tr>
		  <tr id="errorobtainedtr" >
				
				<td align="left" width=300><div style ='display:none; color: red;' id="isobtainedMsg"></div></td>
				
		  </tr>
		   <tr id="erroroutoftr" >
				
				<td align="left" width=300><div style ='display:none; color: red;' id="isoutofMsg"></div></td>
				
		  </tr>
		  
		  <tr id="errorcaltr" >
				
				<td align="left" width=300><div style ='display:none; color: red;' id="iscalMsg"></div></td>
				
		  </tr>
		  
		  <tr id="erroroutofobtainedtr" >
				
				<td align="left" width=300><div style ='display:none; color: red;' id="isoutofobtainedMsg"></div></td>
				
		  </tr>
		  
		  </table>
		 <table border=0 align=center id="marktr">
		  <tr id="subjecttr" >
				<td align="center" width=630><bean:message key="app.sch.prepareReportCard.subject"/></td>
				<td align="center" width=300><bean:message key="app.sch.prepareReportCard.outof"/></td>
				<td align="center" width=300><bean:message key="app.sch.prepareReportCard.obtained"/></td>
				
		  </tr>		
		  </table>
	   </fieldset>
	   <div>
	   <table style="width:100em;margin-top:1em;">
	    <tr id="actiontr" >
				<td align="left" width="2%"></td>
				<td align="left" width="15%"><Label for ="transaction"></Label></td>
				<td align="left" width="15%">
				<input type="submit" value ="Submit" onclick="return isRecordExist();" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" value ="Cancel"  onclick="javascript:history.back();" /></td>
                <td align="left" width="15%"></td>    
           </tr>
       </table>		   
	   </div>
	   </div>
	  </fieldset>
	  
	 
</div>
<html:hidden property="studentid" styleId="studentid" styleClass="srchbox" style="border:1px solid #000000" />
<html:hidden property="classid" styleId="classid" styleClass="srchbox" style="border:1px solid #000000" />
<html:hidden property="sessionid" styleId="sessionid" styleClass="srchbox" style="border:1px solid #000000" />
<html:hidden property="session_year" styleId="session_year" styleClass="srchbox" style="border:1px solid #000000" />
<html:hidden property="subject_count" styleId="subject_count" styleClass="srchbox" style="border:1px solid #000000" />
<html:hidden property="isrecordExist" styleId="isrecordExistval" styleClass="srchbox" style="border:1px solid #000000" />
<html:hidden property="dept_id" styleId="dept_id" styleClass="srchbox" style="border:1px solid #000000" />

</html:form>      
<script language="javascript">
  function showData()
    {
     	document.getElementById("studentname").innerHTML='<%=request.getAttribute("studentname")%>';
    	document.getElementById("student_id").innerHTML='<%=request.getAttribute("studentid")%>';
    	document.getElementById("dateofbirth").innerHTML='<%=request.getAttribute("dateofbirth")%>';
		document.getElementById("fathername").innerHTML='<%=request.getAttribute("fathername")%>';
		document.getElementById("classname").innerHTML='<%=request.getAttribute("classname")%>';
		document.getElementById("departmentname").innerHTML='<%=request.getAttribute("departmentname")%>';
		document.getElementById("studentid").value='<%=request.getAttribute("studentid")%>';
		document.getElementById("dept_id").value='<%=request.getAttribute("dept_id")%>';
		getSelectedSubject(<%=request.getAttribute("studentid")%>,<%=request.getAttribute("classid")%>,'');
    }
	showData();
	var idd=0;
	var totalRowId=0;
   function getSelectedSubject(userId,classId,sessionId){
	EduStudentReportManager.getSelectedSubject(userId,classId,sessionId,function(associatedsubject){
		    document.getElementById("subject_count").value=(associatedsubject.length);
			totalRowId=associatedsubject.length+1;
			var defvalue=100;
			for(var i=1;i<=(associatedsubject.length);i++)
			{
			    var selectedSub=associatedsubject[i-1].split(",");
			    var newRow = document.getElementById("marktr").insertRow(i);
				var oCell = newRow.insertCell(0);
				oCell.align="center"; 
				oCell.width=700;
				oCell.innerHTML = selectedSub[1];
				oCell.name='sub'+i;
				oCell.value=selectedSub[0];
				oCell = newRow.insertCell(1);
			    oCell.align="center"; 
				oCell.width=300;
				oCell.innerHTML = "<input type='text' name='outof"+i+"' onchange=findTotalOutOfByDefault(totalRowId) id='outof"+i+"' value='"+defvalue+"'/>";
				oCell = newRow.insertCell(2);
				oCell.align="center"; 
				oCell.width=300;
				oCell.innerHTML="<input type='text' name='markObtained"+i+"' onchange=findTotalMarkObtained(totalRowId) id='markObtained"+i+"'/>";
				oCell = newRow.insertCell(3);
				oCell.align="center"; 
				oCell.width=300;
				oCell.innerHTML="<input type='hidden' name='sub"+i+"' id='sub"+i+"' value='"+selectedSub[0]+"'/>";
			}			    
			    
			    var totalRow = document.getElementById("marktr").insertRow(totalRowId);
				var oCell = totalRow.insertCell(0);
				oCell.align="center"; 
				oCell.width=700;
				oCell.innerHTML = "Total";
				oCell = totalRow.insertCell(1);
			    oCell.align="center"; 
				oCell.width=300;
				oCell.innerHTML = "<input type='text' name='totaloutof' id='totaloutof'/>";
				oCell = totalRow.insertCell(2);
				oCell.align="center"; 
				oCell.width=300;
				oCell.innerHTML="<input type='text' name='markObtained' id='totalmarkObtained'/>";
				
				var calRow = document.getElementById("marktr").insertRow(totalRowId+1);
				var oCell = calRow.insertCell(0);
				oCell.align="center"; 
				oCell.width=700;
				oCell.innerHTML = "<button type='button' name='Calculate' id='Calculate' onclick=find()>Calculate</button>";
				oCell = calRow.insertCell(1);
			    oCell.align="center"; 
				oCell.width=300;
				oCell.innerHTML = "%"+"<input type='text' name='percentage' id='percentage' ReadOnly='true'/>"+"%";
				oCell = calRow.insertCell(2);
				oCell.align="center"; 
				oCell.width=300;
				oCell.innerHTML="<input type='text' name='division' id='division' ReadOnly='true' />";
				oCell = calRow.insertCell(3);
				oCell.align="center"; 
				oCell.width=300;
				oCell.innerHTML="<input type='text' name='result' id='result' ReadOnly='true' />";
			    findTotalOutOfByDefault(totalRowId);
 		   });
		   
		   }
		   var totalObtainedMark=0;
		   function findTotalMarkObtainedtest(id){
		   if(document.getElementById(id).value!=''){
		   totalObtainedMark=(totalObtainedMark)+parseFloat(document.getElementById(id).value);
		   }
		   document.getElementById('totalmarkObtained').value=totalObtainedMark;
		   //alert("Check"+document.getElementById("outof2").value);
		   }
		   
		   
		   function findTotalMarkObtained(totalRowId){
		   var totalObtainedMark=0;
		   for(var xx=1;xx<totalRowId;xx++){
		   if(document.getElementById('markObtained'+xx).value!=''){
		   totalObtainedMark=(totalObtainedMark)+parseFloat(document.getElementById('markObtained'+xx).value);
		   }
		   }
		   document.getElementById('totalmarkObtained').value=totalObtainedMark;
		   //alert("Check"+document.getElementById("outof2").value);
		   }
		   
		   function findTotalOutOfByDefault(totalRowId){
		   var totalmarkdef=0;
		   for(var xx=1;xx<totalRowId;xx++){
		   if(document.getElementById('outof'+xx).value!=''){
		   totalmarkdef=(totalmarkdef)+parseFloat(document.getElementById('outof'+xx).value);
		   }
		   }
		   document.getElementById('totaloutof').value=totalmarkdef;
		   //alert("Check"+document.getElementById("outof2").value);
		   }
		   function find(){
		    var totalOutOfMark=document.getElementById('totaloutof').value;
			var toatlMarkObtained=document.getElementById('totalmarkObtained').value;
			var status='';
			var percentage= parseFloat((toatlMarkObtained*100)/totalOutOfMark);
			document.getElementById('percentage').value=percentage;
			if(percentage>=33 && percentage<45){
			document.getElementById('division').value='Third';
			}
			else if(percentage>=45 && percentage<60){
			document.getElementById('division').value='Second';
			}
			else if(percentage>=60){
			document.getElementById('division').value='First';
			}
			else if(percentage!=0 && percentage<33){
			document.getElementById('division').value='Failed';
			}
			
			 for(var xx=1;xx<totalRowId;xx++){
			 var outofmark=document.getElementById('outof'+xx).value;
			 var markInEachSub=document.getElementById('markObtained'+xx).value;
		   if(outofmark!='' && markInEachSub!=''){
				var PercentageInEachSub=(markInEachSub*100)/outofmark;
				 if(PercentageInEachSub<33){
						status='Fail';
						break;
						}
				else{
					status='Pass';
				}	
		   }
		}
			document.getElementById('result').value=status;
	}
</script>