<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import="java.util.*"%>
<%
	Random generator = new Random();
int r = generator.nextInt();
%>
<%@page import="com.sch.delegates.ConcernTrackingManager"%>
<script type='text/javascript' src='/dwr/interface/IssueTrackingManager.js'></script>

<!--end custom header content for this example-->

<!--BEGIN SOURCE CODE FOR EXAMPLE =============================== -->
<table align="center" width="97%" cellpadding="5" cellspacing="0" >
<tr><td class="mainhead">
		<div class="barleft-element">
		<div class="barright-elementl"><b>&nbsp;Issue Tracking</b>&nbsp;&nbsp;&nbsp;<a href="/IssueTrackingAction.do?operation=createSetup" onmouseover="over('addSchl','add2over.gif')" onmouseout="over('addSchl','add2.gif')"><img border="0" src="./images/add2.gif" id="addSchl"></a></div>						
        </div>
		</td></tr>
	  
<tr align="left" style="background-image: url('./images/ltbrgrey1.gif')">
<td>    
					<Label for ="name"><b>&nbsp;&nbsp;Status &nbsp;&nbsp; </b></Label>
			        <select name ="status" id= "status" onchange="sortingBystatus();">
					<option value="">Select Status</option>
					<option value ="Pending">Pending</option>
                    <option value="Resolved">Resolved</option>
					<option value ="Unresolved">Unresolved</option>
			        </select>
					<Label for ="name"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Priority &nbsp;&nbsp; </b></Label>
			        <select name ="prioritykey" id= "prioritykey" onchange="sortingBystatus();">
					<option value="">Select Priority</option>
                    <option value="Low">Low</option>
					<option value ="High">High</option>
					<option value ="Medium">Medium</option>
			        </select>
					<Label for ="name"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Category &nbsp;&nbsp; </b></Label>
			        <select name ="category" id= "category" onchange="sortingBystatus();">
					</select>
</td>
	</tr>	
<tr><td >
&nbsp;</td></tr>
<tr><td>
	<div id="bhmintegration" style="width:100%;"></div>
	<div id="dt-pag-nav" align ="center"></div>
</td></tr></table>

<%
	String userValue="";
	String userId = request.getSession().getAttribute("user_id").toString();
    String checkuser = new ConcernTrackingManager().checkForExecutive(userId);
%>
<script type="text/javascript">
//var url="/IssueTrackingAction.do?operation=updateSetup&IssueId=";
var userId = '<%=userValue%>';
var searchKey="";
var searchcategory="";
var searchpriority="";
    function loadProperty()
    {
    	var categoryoption = document.getElementById("category");
    	IssueTrackingManager.getAllCategory(function(category){
				for(var xx=0;xx<category.length;xx++)
				{
					var catidvalue = category[xx].split(",");
					categoryoption[xx]= new Option(catidvalue[1],catidvalue[0]); 						
				}
				 
        	});
       
    }
	 function sortingBystatus() {
		                                     
	    // Column definitions
	     searchKey = trim(document.getElementById("status").value);
	     searchcategory = trim(document.getElementById("category").value);
	   	 searchpriority = trim(document.getElementById("prioritykey").value);
	    YAHOO.example.CustomFormatting = new function() {
	    	this.myCustomFormatterEditIssue = function(elCell, oRecord, oColumn,
					oData) {
	    		if((oRecord.getData("user_id")==<%=session.getAttribute("user_id").toString()%>) && (oRecord.getData("reply")<=0)){

					elCell.innerHTML = '<a href="/IssueTrackingAction.do?operation=updateSetup&IssueId='
						+ oRecord.getData("IssueId")
						+ '" ><img border=0 src="./images/edit.gif" id="EditIssue'
						+ oRecord.getData("IssueId")
						+ '" title="Click Pencil Icon to edit dates, messages and survey title"></a>';
				}else{
					elCell.innerHTML = '&nbsp;';
				}
			};
		this.myCustomFormatterDeleteIssue = function(elCell, oRecord, oColumn,
				oData) {
			
			if((oRecord.getData("user_id")==<%=session.getAttribute("user_id").toString()%>) && (oRecord.getData("reply")<=0)){

				elCell.innerHTML = '<a href="/IssueTrackingAction.do?operation=delete&IssueId='
					+ oRecord.getData("IssueId")
					+ '" ><img border=0 src="./images/close1.gif" id="DeleteIssue'
					+ oRecord.getData("IssueId")
					+ '" title="Click Pencil Icon to edit dates, messages and survey title"></a>';
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};
		this.myCustomFormatterIssueReply = function(elCell, oRecord, oColumn,
				oData) {
			
			if(true){

				elCell.innerHTML = '<a href="/IssueTrackingAction.do?operation=replySetup&IssueId='
					+ oRecord.getData("IssueId")
					+ '" ><img border=0 src="./images/Reply Icon.jpg" id="IssueReply'
					+ oRecord.getData("IssueId")
					+ '" title="Click Pencil Icon to edit dates, messages and survey title"></a>';
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};
		
		YAHOO.widget.DataTable.Formatter.IssueReply = this.myCustomFormatterIssueReply;
		YAHOO.widget.DataTable.Formatter.EditIssue = this.myCustomFormatterEditIssue;
		YAHOO.widget.DataTable.Formatter.DeleteIssue = this.myCustomFormatterDeleteIssue;

    };
	    var myColumnDefs = [ // sortable:true enables sorting
	                         
	                         {key:"Issue_Title", label:"Issue Title", sortable:true},
	                         {key:"Issue_Desc", label:"Issue Desc", sortable:true, width : 180},
	                         {key:"user_first_name", label:"Name", sortable:true},
	                         {key:"ug_title", label:"Designation", sortable:true},
	                         {key:"Priority", label:"Priority", sortable:true},
	                         {key:"Category", label:"Category", sortable:true},
	                         {key:"Current_Status", label:"Status", sortable:true},
	                         {key:"issue_submited_date", label:"Issue Date", sortable:true},
	                         {key:"IssueReply", label:"Reply",formatter: "IssueReply",sortable :false,resizeable :false},
	                         {key:"EditIssue", label:"Edit",formatter: "EditIssue",sortable :false,resizeable :false},
	                         {key:"DeleteIssue", label:"Delete",formatter: "DeleteIssue",sortable :false,resizeable :false}
	                     ];

	    // Custom parser
	    // DataSource instance
	    var myDataSource = new YAHOO.util.DataSource("/jsp/issuetrackingproxy.jsp?ran=<%=r%>&searchKey="+searchKey+"&searchcategory="+searchcategory+"&searchpriority="+searchpriority+"&userId="+userId+"&");
	    myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSON;
	    myDataSource.responseSchema = {
	        resultsList: "records",
	        fields: ["IssueId","user_first_name","Issue_Title","Issue_Desc","ug_title","Priority","Category","Current_Status","user_id","reply","issue_submited_date"],

	        metaFields: {
	            totalRecords: "totalRecords" // Access to value in the server response
	        }
	    };
	   
	    // DataTable configuration
	    var myConfigs = {
			paginator: new YAHOO.widget.Paginator({rowsPerPage:10, containers : ["dt-pag-nav"], template : "{PageLinks} {RowsPerPageDropdown}", rowsPerPageOptions : [10,25,50,100] }), // Enables pagination 
	    	initialRequest: "sort=Current_Status&dir=asc&startIndex=0&results=10", // Initial request for first page of data
	        dynamicData: true, // Enables dynamic server-driven data
	        sortedBy : {key:"Current_Status", dir:YAHOO.widget.DataTable.CLASS_ASC} // Sets UI initial sort arrow
	    };
    // DataTable instance
	    var myDataTable = new YAHOO.widget.DataTable("bhmintegration", myColumnDefs, myDataSource, myConfigs);
	    // Update totalRecords on the fly with value from server
	    myDataTable.handleDataReturnPayload = function(oRequest, oResponse, oPayload) {
	        oPayload.totalRecords = oResponse.meta.totalRecords;
	        return oPayload;
	    }
	    
	    return {
	        ds: myDataSource,
	        dt: myDataTable
	    };
	        
	}
	 sortingBystatus();
	 loadProperty();
		    // Column definiti
</script>
<!--END SOURCE CODE FOR EXAMPLE =============================== -->
</div>