<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
          <script type="text/javascript">
            /*
                 Initialize and render the MenuBar when its elements are ready 
                 to be scripted.
            */
            YAHOO.util.Event.onContentReady("productsandservices", function () {

                /*
					Instantiate a MenuBar:  The first argument passed to the constructor
					is the id for the Menu element to be created, the second is an 
					object literal of configuration properties.
                */

                var oMenuBar = new YAHOO.widget.MenuBar("productsandservices", { 
                                                            autosubmenudisplay: true, 
                                                            hidedelay: 750, 
                                                            lazyload: true });

                /*
                     Call the "render" method with no arguments since the 
                     markup for this MenuBar instance is already exists in 
                     the page.
                */

                oMenuBar.render();
            });
        </script>

    <body class="yui3-skin-sam">

		
					   <div id="productsandservices" class="yuimenubar yuimenubarnav">
							<div class="bd">
								<ul class="first-of-type">
								   
									<li class="yuimenubaritem"><a class="yui3-menuitem-content" href="/schmangmt/EduHomeAction.do?operation=home"> Home </a></li>
									<li class="yuimenubaritem"><a class="yui3-menuitem-content" href="/schmangmt/EduHomeAction.do?operation=aboutUs"> About Us</a></li>
									
									<li class="yuimenuitem"><a class="yui3-menuitem-content" href="/schmangmt/EduRegistrationAction.do?operation=registrationSetup"> Registration </a></li> 

									<li class="yui3-menuitem"><a class="yui3-menuitem-content" href="/schmangmt/EduHomeAction.do?operation=lifeStyles">Lifestyles</a></li>

									<li class="yui3-menuitem"><a class="yui3-menuitem-content" href="/schmangmt/EduHomeAction.do?operation=events">Events</a></li>

									<li class="yui3-menuitem"><a class="yui3-menuitem-content" href="/schmangmt/EduHomeAction.do?operation=contactUs" align="left"> Contact Us </a></li>
                                    <li align="right"><a class="yui3-menuitem-content" id="show"> Login </a></li>  
								</ul>            
							</div>
						</div>
				     
        
    </body>
</html>
