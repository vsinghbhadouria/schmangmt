<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<script type='text/javascript' src='/schmangmt/dwr/interface/LoginManager.js'></script>
<script type='text/javascript' src='./scripts/schmanagmt.js'></script>
<html:form action="/LoginAction.do?operation=savePassword" method="post">
	<table height="200" width="70%" style="border: 1px solid" align="center" /> 
	<tr class="mainhead">
		<td colspan="3">&nbsp;&nbsp;Change Password</td>
	</tr> 
	 <tr>
		<td align=right width="5%"><img src="./images/blank.png" alt="" id="oldpasswordError"/></td>
		<td align="left" width="20%"><b>Password</b></td>
		<td ><html:password property="password" styleId="password" style="border:1px solid #000000" value=""/>&nbsp;&nbsp;&nbsp;<div  style ='display:none; color: red;' id="oldpasswordMsg"></div></td>                                     
	</tr>
	<tr>
 		<td align=right width="5%"><img src="./images/blank.png" alt="" id="newpasswordError"/></td>
		<td align="left" width="20%"><b>New Password:</b></td>
    	<td ><html:password property="newpassword" styleId="newpassword" style="border:1px solid #000000" value=""/>&nbsp;&nbsp;&nbsp;<div  style ='display:none; color: red;' id="newpasswordMsg"></div></td>	
 	</tr>
	<tr>
		<td align=right width="5%"><img src="./images/blank.png" alt="" id="confirmpasswordError"/></td>
		<td align="left" width="20%"><b>Confirm Password:</b></td>
        <td><html:password property="confirmpassword" styleId="confirmpassword" style="border:1px solid #000000" value=""/>&nbsp;&nbsp;&nbsp;<div  style ='display:none; color: red;' id="confirmpasswordMsg"></div></td>	
	</tr>
	<tr>
		<td align="center" colspan="3"><input type="submit" name="Change Password" value="Change Password" onclick="return validate();"/>
					&nbsp;<input type="button" name="Back" value="Back" onclick="javascript:history.back()"/>	
</table>
<br>
</html:form>

<script>
function validate()
{
	var oldpassword = document.getElementById("password").value;
	var newpassword = document.getElementById("newpassword").value;
	var confirmpassword = document.getElementById("confirmpassword").value;

	var oldflg = false;
	var newflg = false;
	var confirmflg = false;
	var flag = false;
	var check=false;
	var returnFlag = false; 
	  if(newpassword!='')
	    {
		  document.getElementById('newpasswordError').style.display='none';
	      document.getElementById('newpasswordMsg').style.display = 'none';
	      newflg = true;
	      if(newpassword.length >=6){
		   		newflg=true;
	 	  		  	
	 	  	}
	 	  	else{
	       		over("newpasswordError","error_bang.gif");
	       		document.getElementById('newpasswordError').style.display='inline';
	       		document.getElementById('newpasswordMsg').innerHTML ='Password length should be 6 atleast';
	       		document.getElementById('newpasswordMsg').style.display = 'inline';
	       		newflg = false;
	 	  	}  	
		     
	     }
	  else
	  { 
		over("newpasswordError","error_bang.gif");
        document.getElementById('newpasswordError').style.display='inline';
    	document.getElementById('newpasswordMsg').innerHTML ='Please Enter the New Password';
    	document.getElementById('newpasswordMsg').style.display = 'inline';
    	newflg = false;
	  }
	  if(confirmpassword!='')
	    {
		  document.getElementById('confirmpasswordError').style.display='none';
	      document.getElementById('confirmpasswordMsg').style.display = 'none';
		  confirmflg = true;
	      if(newpassword == confirmpassword)
		      {			
		   		  confirmflg=true;  	
	 	  	 }
	 	  	else{
	       		over("confirmpasswordError","error_bang.gif");
	       		document.getElementById('confirmpasswordError').style.display='inline';
	       		document.getElementById('confirmpasswordMsg').innerHTML ='New & Confirm Password MisMatch:Please re-enter Password';
	       		document.getElementById('confirmpasswordMsg').style.display = 'inline';
	       		confirmflg = false;
	 	  	}
	    }
	  else
	  { 
		over("confirmpasswordError","error_bang.gif");
        document.getElementById('confirmpasswordError').style.display='inline';
        document.getElementById('confirmpasswordMsg').innerHTML ='Please Confirm the Password';
        document.getElementById('confirmpasswordMsg').style.display = 'inline';
        confirmflg = false;
	  }

	  if(oldpassword!='')
	    {
		  document.getElementById('oldpasswordError').style.display='none';
	      document.getElementById('oldpasswordMsg').style.display = 'none';
	      var encoldpassword = MD5(document.getElementById("password").value);
	  	  var userid ='<%=session.getAttribute("user_id")%>';
	  	  var ug_id ='<%=session.getAttribute("ug_id")%>';
	  	  LoginManager.checkPassword(encoldpassword,userid,ug_id,{
			async:false,
			callback:function(chk)
	  			{	
	  			check = chk;
				if(check==false)
				{
				
					over("oldpasswordError","error_bang.gif");
			      	document.getElementById('oldpasswordError').style.display='inline';
			      	document.getElementById('oldpasswordMsg').innerHTML ='Password MisMatch:Please re-enter Password';
			      	document.getElementById('oldpasswordMsg').style.display = 'inline';
			      	oldflg = false;
				}
				else
				{
					oldflg = true;
				}
			}
			});

	    }
	  else
	  { 
		  
		over("oldpasswordError","error_bang.gif");
    	document.getElementById('oldpasswordError').style.display='inline';
    	document.getElementById('oldpasswordMsg').innerHTML ='Please Enter the Password You Wish to Change';
    	document.getElementById('oldpasswordMsg').style.display = 'inline';
    	oldflg = false;
     }
	  //alert("oldflg "+oldflg+" newflg "+newflg+" confirmflg "+confirmflg);
		  if(oldflg && newflg && confirmflg) {
	  			if(confirm("Are You Sure You Want To Change Password")){
	  				var encnewpassword=MD5(document.getElementById("newpassword").value);
	  				document.getElementById('newpassword').value = encnewpassword;
	  				var encpassword=MD5(document.getElementById("password").value);
	  				document.getElementById('password').value = encpassword;
	  				returnFlag = true;
	  				}
	  			else{
	  				returnFlag = false;
	  			}
	  			
		  	 }
		  else{
			  returnFlag = false;
		  }
	return returnFlag;
}
</script>