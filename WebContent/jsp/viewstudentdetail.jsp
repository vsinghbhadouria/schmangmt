<%@ page language="java" %>
<%@page import="com.sch.to.EduStudentTO"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
  <head>
  <title>Institute Student</title> 
  </head>
  <body>
   
  <html:form action="/EduStudentAction.do?operation=update" method ="post">

   <div style="border: 1px solid;width:97%"  >
 <table style="" align="center" cellspacing="2" cellpadding="5" width=100% border="0">
	<tr><td class="mainhead" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="app.sch.viewedustudent.viewformtitle"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   </td></tr>
        <tr>
          <td align=left width="1%" ><img src="./images/blank.png" alt="" id="firstNameError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.firstname"/></b></td>
          <td align="left"><label for="firstname" id="firstname"></label></td>
		  
         </tr>
<tr>
 <td align=left ><img src="./images/blank.png" alt="" id="middleNameError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.middlename"/></b></td>
          <td><label for="middlename" id="middlename"></label></td>
        </tr>
        <tr>
<tr>
 <td align=left ><img src="./images/blank.png" alt="" id="lastNameError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.lastname"/></b></td>
          <td><label for="lastname" id="lastname"></label></td>
        </tr>
        
         <tr>
          <td align=left><img src="./images/blank.png" alt="" id="genderError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.sex"/></b></td>
		  <td align=left>
			<label for="gender" id="gender"></label></td>
         </tr>
               
        <tr>
          <td align=left ><img src="./images/blank.png" alt="" id="phoneError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.phoneno"/></b></td>
          <td><label for="phoneno" id="phoneno"></label></td>
    </tr>
    <tr>      <td align=left><img src="./images/blank.png" alt="" id="addressError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.address"/></b></td>
          <td><label for="address" id="address"></label></td>
        </tr>
		<tr>
          <td align=left ><img src="./images/blank.png" alt="" id="emailidError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.emailid"/></b></td>
          <td align="left"><label for="emailid" id="emailid"></label></td>
         </tr>

		 <tr>
          <td align=left><img src="./images/blank.png" alt="" id="fatherNameError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.fathername"/></b></td>
          <td align="left"><label for="fathername" id="fathername"></label></td>
         </tr>
<tr> <td align=left><img src="./images/blank.png" alt="" id="mothernameError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.mothername"/></b></td>
		  <td><label for="mothername" id="mothername"></label></td>
           </tr>
<tr>
          <td align=left ><img src="./images/blank.png" alt="" id="parentemailidError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.parentemailid"/></b></td>
          <td align="left"><label for="parentemailid" id="parentemailid"></label></td>
         </tr>       
		 <tr>
          <td align=left><img src="./images/blank.png" alt="" id="previousinstituteError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.studentpreviousinstitute"/></b></td>
          <td align="left"><label for="previousinstitute" id="previousinstitute"></label></td>
         </tr>
<tr>
          <td align=left><img src="./images/blank.png" alt="" id="parenthighereduError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.parenthigherqualification"/></b></td>
		  <td align=left><label for="parentqualification" id="parentqualification"></label></td>
         </tr>		 
		 <tr>
          <td align=left><img src="./images/blank.png" alt="" id="studentpreviousqualiError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.studentpreviousqualification"/></b></td>
		  <td align=left>
		  <label for="studprequalification" id="studprequalification"></label></td>
         </tr>
<tr>
          <td align=left><img src="./images/blank.png" alt="" id="studclassError"/></td>
		  <td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.class"/></b></td>
		  <td align=left>
		  <label for="classname" id="classname"></label></td>
         </tr>
		 <tr >
          <td align=left ><img src="./images/blank.png" alt="" id="admissionfeeError"/></td>
		  <td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.admissionfee"/></b></td>
          <td align=left><label for="admissionfee" id="admissionfee"></label></td>
         </tr>
		 <tr >
          <td align=left  ><img src="./images/blank.png" alt="" id="admissionfeeError"/></td>
		  <td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.concessionamount"/></b></td>
          <td align=left><label for="concessionamt" id="concessionamt"></label></td>
         </tr>
		  <tr ><td align=left><img src="./images/blank.png" alt="" id="commentError"/></td>
		  <td align=left width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.comments"/></b></td>
          <td><label for="comment" id="comment"></label></td>
         </tr>
		 <tr>
          <td align="left"><img src="./images/blank.png" alt="" id="birthyearError"/></td><td align=left  width=25%>&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.dateofbirth"/></b></td>
		  <td><label for="birthdate" id="birthdate"></label></td>
         </tr>    
          <tr>
          <td align="left"><img src="./images/blank.png" alt="" id="admissiondateError"/></td><td align=left  width=25%>&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.admissiondate"/></b></td>
		  <td><label for="admissiondate" id="admissiondate"></label></td>
         </tr>

      
</table>

  <table style="" align="center" cellspacing=2 cellpadding=5 width=97%  > 
   <tr>   <td colspan="2" align="center" >
            <input type="button" value ="Cancel"  onclick="javascript:history.back();" />
            </td>
        
        </tr>
      </table>

</div>
</html:form>
<script type="text/javascript">
    function showData()
    {
        document.getElementById("firstname").innerHTML='<%=request.getAttribute("firstname")%>';
    	document.getElementById("middlename").innerHTML='<%=request.getAttribute("middlename")%>';
    	document.getElementById("lastname").innerHTML='<%=request.getAttribute("lastname")%>';
		document.getElementById("gender").innerHTML='<%=request.getAttribute("gender")%>';
    	document.getElementById("phoneno").innerHTML='<%=request.getAttribute("phoneno")%>';
    	document.getElementById("address").innerHTML='<%=request.getAttribute("address")%>';
		document.getElementById("emailid").innerHTML='<%=request.getAttribute("emailid")%>';
		document.getElementById("fathername").innerHTML='<%=request.getAttribute("fathername")%>';
    	document.getElementById("mothername").innerHTML='<%=request.getAttribute("mothername")%>';
		document.getElementById("parentemailid").innerHTML='<%=request.getAttribute("parentemailid")%>';
		document.getElementById("previousinstitute").innerHTML='<%=request.getAttribute("previous_edu_center")%>';
		document.getElementById("parentqualification").innerHTML='<%=request.getAttribute("parent_higher_qualification")%>';
		document.getElementById("studprequalification").innerHTML='<%=request.getAttribute("student_prev_qualification")%>';
    	document.getElementById("classname").innerHTML='<%=request.getAttribute("classname")%>';
    	document.getElementById("admissionfee").innerHTML='<%=request.getAttribute("admissionfee")%>';
    	document.getElementById("concessionamt").innerHTML='<%=request.getAttribute("concessionamt")%>';
    	document.getElementById("comment").innerHTML='<%=request.getAttribute("comment")%>';
    	document.getElementById("birthdate").innerHTML='<%=request.getAttribute("birthofdate")%>';
    	document.getElementById("admissiondate").innerHTML='<%=request.getAttribute("admissionofdate")%>';
    	if('<%=request.getAttribute("firstname")%>'!=''){
			'<%
				request.removeAttribute("firstname");
			%>'
	    }
		if('<%=request.getAttribute("middlename")%>'!=''){
			'<%
				request.removeAttribute("middlename");
			%>'
	    }
		if('<%=request.getAttribute("lastname")%>'!=''){
			'<%
				request.removeAttribute("lastname");
			%>'
	    }
		if('<%=request.getAttribute("gender")%>'!=''){
			'<%
				request.removeAttribute("gender");
			%>'
	    }
		if('<%=request.getAttribute("phoneno")%>'!=''){
			'<%
				request.removeAttribute("phoneno");
			%>'
	    }
		if('<%=request.getAttribute("address")%>'!=''){
			'<%
				request.removeAttribute("address");
			%>'
	    }
		if('<%=request.getAttribute("emailid")%>'!=''){
			'<%
				request.removeAttribute("emailid");
			%>'
	    }
		if('<%=request.getAttribute("fathername")%>'!=''){
			'<%
				request.removeAttribute("fatheranme");
			%>'
	    }
		if('<%=request.getAttribute("mothername")%>'!=''){
			'<%
				request.removeAttribute("mothername");
			%>'
	    }
		if('<%=request.getAttribute("parentemailid")%>'!=''){
			'<%
				request.removeAttribute("parentemailid");
			%>'
	    }
		if('<%=request.getAttribute("previous_edu_center")%>'!=''){
			'<%
				request.removeAttribute("previous_edu_center");
			%>'
	    }
		
		if('<%=request.getAttribute("parent_higher_qualification")%>'!=''){
			'<%
				request.removeAttribute("parent_higher_qualification");
			%>'
	    }
		if('<%=request.getAttribute("student_prev_qualification")%>'!=''){
			'<%
				request.removeAttribute("student_prev_qualification");
			%>'
	    }
		if('<%=request.getAttribute("classname")%>'!=''){
			'<%
				request.removeAttribute("classname");
			%>'
	    }
		if('<%=request.getAttribute("admissionfee")%>'!=''){
			'<%
				request.removeAttribute("admissionfee");
			%>'
	    }
		if('<%=request.getAttribute("concessionamt")%>'!=''){
			'<%
				request.removeAttribute("concessionamt");
			%>'
	    }
		if('<%=request.getAttribute("comment")%>'!=''){
			'<%
				request.removeAttribute("comment");
			%>'
	    }
		if('<%=request.getAttribute("birthofdate")%>'!=''){
			'<%
				request.removeAttribute("birthofdate");
			%>'
	    }if('<%=request.getAttribute("admissionofdate")%>'!=''){
			'<%
				request.removeAttribute("admissionofdate");
			%>'
			//alert("remove all");
	    }
    }
    showData();

</script>
