<%@ page language="java" %>
<%@page import="com.sch.to.EduStaffTO"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
  <head>
 
  <title>Add Employees</title>  
  </head>
<script type='text/javascript' src="/schmangmt/dwr/interface/LibraryManagementManager.js"></script>
<script type='text/javascript' src='./scripts/schmanagmt.js'></script>
<script>
/* function getRackIds(){
	
	document.getElementById('rackdisplay').style.display='';
	var bookTypeId = document.getElementById('booktypeid').value;
	//alert(bookTypeId);
	var bookName = document.getElementById('tags').value;
	var numberofcopy = document.getElementById('numberofcopy').value;
	var rackid = document.getElementById('rackid');
	LibraryManagementManager.getRackIds(bookTypeId,function(rackdetail){
		for(var i=0;i<rackdetail.length;i++){
				var userOptions = rackdetail[i];
				userOptions = userOptions.split(',');
				rackid[i] = new Option(userOptions[1],userOptions[0]);
			}
	}); */
	
	function getBookIds(){

		var bookName = document.getElementById('tags').value;
		var bookid = document.getElementById('bookid');
		LibraryManagementManager.getAvailableBookIds(bookName,function(bookIds){
			document.getElementById('bookdisplay').style.display='none';
			document.getElementById('booktype').style.display='none';
			if(bookIds.length>0){
            	removeOptions(bookid);
            	document.getElementById('bookdisplay').style.display='';
				for(var i=0;i<bookIds.length;i++){
					var userOptions = bookIds[i];
					userOptions = userOptions.split(',');
					bookid[i] = new Option(userOptions[1],userOptions[1]);
				}
		LibraryManagementManager.getAvailableBookcountandtype(bookName,function(bookdetail){
			if(bookdetail.length>0){
				//document.getElementById('availablebookcount').style.display='';
				document.getElementById('booktype').style.display='';
				//document.getElementById('rackname').style.display='';
				breakDetails = bookdetail.split(',');
				document.getElementById('availability_count').innerHTML=breakDetails[0];
				document.getElementById('rackid').innerHTML=breakDetails[1];
				document.getElementById('booktypename').innerHTML=breakDetails[2];
				document.getElementById('bookmasterid').value=breakDetails[3];
			}
		
		});	

				
            }
            else{
            	//document.getElementById('rackdisplay').style.display='none';
            	//removeOptions(rackid);
            }
		});
	}
	
	function getStudentData(){
		
		var userId = document.getElementById('user_id').value;
		LibraryManagementManager.getStudentDataByUserId(userId,function(studentData){
			if(studentData.length>0){
				document.getElementById('studentclassname').style.display='';
				document.getElementById('emailstatus').style.display='';
				//document.getElementById('rackname').style.display='';
				student = studentData.split(',');
				document.getElementById('studentname').innerHTML=student[0];
				document.getElementById('classname').innerHTML=student[1];
				document.getElementById('emailid').innerHTML=student[2];
				document.getElementById('status').innerHTML=student[3];
				}
			});
	}
	
	function getTeacherData(){
		
		var userId = document.getElementById('empId').value;
		LibraryManagementManager.getTeacherDataByUserId(userId,function(teacherData){
			if(teacherData.length>0){
				document.getElementById('teacherqualification').style.display='';
				document.getElementById('teacheremailphone').style.display='';
				//document.getElementById('rackname').style.display='';
				teacher = teacherData.split(',');
				document.getElementById('employeename').innerHTML=teacher[0];
				document.getElementById('qualification_title').innerHTML=teacher[1];
				document.getElementById('t_emailid').innerHTML=teacher[2];
				document.getElementById('t_phoneno').innerHTML=teacher[3];
				}
			});
	}
	
	function bookStatus(){
		
		var bookId = document.getElementById('bookId').value;
		LibraryManagementManager.getassignedBookByBookId(bookId,function(assignedbookInfo){
			bookInfo = assignedbookInfo.split(',');
			
			document.getElementById('bookdisplay').style.display='';
			document.getElementById('bookname').innerHTML=bookInfo[6];
			document.getElementById('booktypename').innerHTML=bookInfo[7];
			
			document.getElementById('bookdate').style.display='';
			document.getElementById('issuedate').innerHTML=bookInfo[10];
			document.getElementById('duedate').innerHTML=bookInfo[11];
			
			document.getElementById('rackinfo').style.display='';
			document.getElementById('rackname').innerHTML=bookInfo[9];
			document.getElementById('booktypename').innerHTML=bookInfo[7];
			
			
			
			document.getElementById('studentData').style.display='';
			document.getElementById('userinfo').style.display='';
			document.getElementById('username').innerHTML=bookInfo[0];
			document.getElementById('user_id').innerHTML=bookInfo[1];
			document.getElementById('userId').value=bookInfo[1];
			
			document.getElementById('emailstatus').style.display='';
			document.getElementById('emailid').innerHTML=bookInfo[2];
			document.getElementById('phoneno').innerHTML=bookInfo[3];
			
			document.getElementById('userstatus').style.display='';
			document.getElementById('usertype').innerHTML=bookInfo[5];
			document.getElementById('status').innerHTML=bookInfo[4];
			document.getElementById('profileImg').src="profile-pics/"+bookInfo[1]+".JPG";
			
		});
		
		
	}
</script>
 
    
    
 <html:form  action="/LibraryManagementAction.do?operation=returnBook" method ="post">
        <%

                         String struserid      = request.getParameter("userid");
                                               
     %>
 <body>
 <html:errors />
  <div  align=left width="70">
      <fieldset id="optionsContainer" width="70">
		  <legend><b><bean:message key="app.sch.returnissuebook.bookstatus"/></b></legend>
		<table align=left border=0 width=100%>
		<tr>
		<td width=50%>	
		<table align=left border=0 width=100%>
		
		 <tr>
          <td align=left width="3%" colspan="1"><img src="./images/blank.png" alt="" id="bookNameError"/></td>
          <td align=left  width=10%><b><bean:message key="app.sch.addbook.bookid"/></b></td>
          <td align="left" width=25%><input type="text" id="bookId" name="bookId" style="border:1px solid #000000" size=18"/></td>
          <td align="left" width=10%> <input type="button" value ="Book Status"  onclick="javascript:bookStatus();"/></td>
          <td align="left" width=25%></td>
         </tr>
		
		         
         <tr id="bookdisplay" style="display:none">
          <td align=right width="3%"><img src="./images/blank.png" alt="" id="highereduError"/></td>
          <td align=left  width=10%><b><bean:message key="app.sch.addbook.bookname"/></b></td>
          <td id="bookname" align="left" width=25%></td>
 		  <td align=left  width=10%><b><bean:message key="app.sch.addbook.booktype"/></b></td>
	 	  <td id="booktypename" align="left" width=25%></td>
         </tr>
		
	    <tr id="bookdate" style="display:none">
	 		<td align=right width="3%" ><img src="./images/blank.png" alt="" id="copyError"/></td>
	 		<td align=left  width=10%><b><bean:message key="app.sch.returnissuebook.issuedate"/></b></td>
	 		<td id="issuedate" align="left" width=25%></td>
	        <td align=left  width=15%><b><bean:message key="app.sch.returnissuebook.duedate"/></b></td>
	        <td id="duedate" align="left" width=25%></td>
        </tr>
	    <tr id="rackinfo" style="display:none">
	 		<td align=right width="3%" ><img src="./images/blank.png" alt="" id="copyError"/></td>
	 		<td align=left  width=10%><b><bean:message key="app.sch.returnissuebook.rackname"/></b></td>
	 		<td id="rackname" align="left" width=25%></td>
	        <td align=left  width=15%><b><bean:message key="app.sch.returnissuebook.duedate"/></b></td>
	        <td id="duedate" align="left" width=25%></td>
        </tr>
         		
		</table>
		</td>
		
		<td width=30%>
		<table><tr><td>	
	<!-- 	<img src="images/DSCN1420.JPG"
	width="200" height="130" alt="What We Do" align="right"/> -->
		</td></tr></table>
		</td>
		</tr>
		</table>	
		</fieldset>
		</div>
	
	<div align=left width="50">
		<fieldset id="optionsContainer" width="50">
 <div  align=left width="70" style="display:none" id="studentData">
      <fieldset id="optionsContainer" width="70">
		  <legend><b><bean:message key="app.sch.returnissuebook.userprofile"/></b></legend>
		<table align=left border=0 width=100%>
		<tr>
		<td width=50%>	
		<table align=left border=0 width=100%>

         
		<tr id="userinfo"  style="display:none">
				<td align="right" width="3%"></td>
				<td align="left" width=10%><b><bean:message key="app.sch.returnissuebook.username"/></b></td>
                <td id="username" align="left" width=25%>
                <td align=left  width=15%><b><bean:message key="app.sch.studentprofile.user_id"/></b></td>
			    <td id="user_id" align="left" width=25%>
        </tr>

        <tr id="emailstatus" style="display:none">
				<td align="left" width="3%"></td>
				<td align="left" width=10%><b><bean:message key="app.sch.studentprofile.emailid"/></b></td>
                <td id="emailid" align="left" width=25%>
                <td align="left" width=15%><b><bean:message key="app.sch.studentprofile.phoneno"/></b></td>
				<td id="phoneno" align="left" width=25%></td>
	    </tr>
    
        <tr id="userstatus" style="display:none">
				<td align="left" width="3%"></td>
                <td align="left" width=15%><b><bean:message key="app.sch.returnissuebook.usertype"/></b></td>
				<td id="usertype" align="left" width=25%></td>
				<td align="left" width=15%><b><bean:message key="app.sch.issuebook.status"/></b></td>
				<td id="status" align="left" width=25%></td>
        </tr>
        	
       		
		</table>
		</td>
		
		<td width=30%>
		<table><tr><td>	
		<img id="profileImg" width="200" height="130" alt="What We Do" align="right"/>
		</td></tr></table>
		</td>
		</tr>
		</table>	
		</fieldset>
		</div>
  
  
		<table style="" align="center" border=0 cellspacing=2 cellpadding=3 width=100%  > 
				<tr> 
					<td colspan="2" align="center" >
					<input type="submit" value ="Submit" onclick="return checkValidation();"/>
					<input type="button" value ="Cancel"  onclick="javascript:history.back();"/>
				</td>
			   </tr>
			</table>
		</fieldset>
		
	</div>
	
		
   
  
  
<html:hidden property="bookmasterid" styleId="bookmasterid" styleClass="srchbox" style="border:1px solid #000000" value="" />
<html:hidden property="userId" styleId="userId" styleClass="srchbox" style="border:1px solid #000000" value="" />
</body>
</html:form>