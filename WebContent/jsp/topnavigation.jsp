<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
 <!-- Standard reset, fonts and grids -->
        <!-- <link rel="stylesheet" type="text/css" href="./yui/build/reset-fonts-grids/reset-fonts-grids.css">-->
        <!-- CSS for Menu -->
        <link rel="stylesheet" type="text/css" href="./yui/build/menu/assets/skins/sam/menu.css"> 
        <!-- Page-specific styles -->
        <style type="text/css">
            div.yui-b p {
                margin: 0 0 .5em 0;
                color: #999;
            }
            div.yui-b p strong {
                font-weight: bold;
                color: #000;
            
            }
            div.yui-b p em {
                color: #000;
            }            
            h1 {

                font-weight: bold;
                margin: 0 0 1em 0;
                padding: .25em .5em;
                background-color: #ccc;
            }
            #productsandservices {
                margin: 0 0 10px 0;
            }
        </style>
        <!-- Dependency source files -->
        <script type="text/javascript" src="./yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
        <script type="text/javascript" src="./yui/build/container/container_core.js"></script>
        <!-- Menu source file -->
        <script type="text/javascript" src="./yui/build/menu/menu.js"></script>
        <!-- Page-specific script -->
        <script type="text/javascript">
            /*
                 Initialize and render the MenuBar when its elements are ready 
                 to be scripted.
            */
            YAHOO.util.Event.onContentReady("productsandservices", function () {

                /*
					Instantiate a MenuBar:  The first argument passed to the constructor
					is the id for the Menu element to be created, the second is an 
					object literal of configuration properties.
                */

                var oMenuBar = new YAHOO.widget.MenuBar("productsandservices", { 
                                                            autosubmenudisplay: true, 
                                                            hidedelay: 750, 
                                                            lazyload: true });

                /*
                     Call the "render" method with no arguments since the 
                     markup for this MenuBar instance is already exists in 
                     the page.
                */

                oMenuBar.render();
            });
        </script>
    <div class="yui-skin-sam" id="yahoo-com">

        <div id="doc" class="yui-t1">
            <div id="hd">
                <!-- start: your content here -->
                <!-- end: your content here -->
            </div>

            <div id="bd">

                <!-- start: primary column from outer template -->
                <div id="yui-main">
                    <div class="yui-b">
                        <!-- start: stack grids here -->

                       <div id="productsandservices" class="yuimenubar yuimenubarnav">
                            <div class="bd">
                                <ul class="first-of-type">
								<% 
									int fMenu = 0;
									int fSubMenu = 0;
								%> 
									<logic:iterate name="TopNavigationForm" property="listfullMenu" id="listfullMenu">
									<bean:define name="listfullMenu" property="funcID" id="funcID" type="java.lang.Integer"></bean:define>
									<bean:define name="listfullMenu" property="pfuncID" id="pfuncID" type="java.lang.Integer"></bean:define>
								    <%if (pfuncID==0){ %>
                                        <%if (fSubMenu==1){
                                        	fSubMenu=0;
                                        	%>
                                </ul>
                           </div>
                       
                       </div>   
                                        <% }
                                         else{
                                        	 fSubMenu=0; 
                                         }
                                         %>
                                        <li class="yuimenubaritem"><a class="yuimenubaritemlabel" href="<bean:write name="listfullMenu" property="funcURL"/>"><bean:write name="listfullMenu" property="funcTITLE"/></a>
                                        <div id="<bean:write name="listfullMenu" property="funcTITLE"/>" class="yuimenu">
                                        <div class="bd">                    
                                        <ul>
 						             <%}else{%>
										<li class="yuimenuitem"><a class="yuimenuitemlabel" href="<bean:write name="listfullMenu" property="funcURL"/>"><bean:write name="listfullMenu" property="funcTITLE"/></a></li>
                                       	<%
                                        fSubMenu=1;
                                        }%>
                                       
										</logic:iterate>
										 				
                                    </li>
                               
                                </ul>   
     							
                            </div>
  							
                        </div>
                       
                        <!-- end: stack grids here -->
                                 
                    </div>

                </div>

            </div>
        </div>
    </div>

