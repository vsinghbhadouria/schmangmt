<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<script type='text/javascript' src="/schmangmt/dwr/interface/ConcernTrackingManager.js"></script>
      <html:form  action="/ConcernTrackingAction.do?operation=save" onsubmit="return checkValidation();" >
       
       <table align="center" width="97%" cellpadding="5" cellspacing="0">
	   <tr><td class="mainhead" >&nbsp;&nbsp;<bean:message key="app.sch.addconcerntracking.viewconcern"/>
	   </td></tr>
       </table>
      <table   align="center" cellspacing="2" cellpadding="5" width=97% border="0" style="border: 1px solid;">
       <!-- <tr>
          <td align=left width=1%>&nbsp;&nbsp;&nbsp;Shop Code</td>
          <td><html:text property="shopcode" size="30" styleClass="srchbox" style="border:1px solid #000000" value =""/></td>
        </tr>-->

		<tr id="ConcernTitle" >
		  <td align=left ><img src="./images/blank.png" alt="" id="ConcernTitleError"/></td>
          <td align=left >&nbsp;&nbsp;<bean:message key="app.sch.addconcerntracking.concernTitle"/></td>
          <td id="issuetitle" align="left"></td>
		</tr>	
        <tr>
          <td align=left ><img src="./images/blank.png" alt="" id="showStreetAddError"/></td>
          <td align=left valign="top">&nbsp;&nbsp;<bean:message key="app.sch.addconcerntracking.concernDesc"/></td>
          <td><html:textarea property="issuedesc" styleId="issuedesc" style="border:1px solid #000000" rows="5" cols="40"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="shopstreetAddMsg" ></td>
        </tr>
		
		
		<tr id="ConcernCategory" >
		  <td align=left ><img src="./images/blank.png" alt="" id="ConcernCategoryError"/></td>
          <td align=left >&nbsp;&nbsp;<bean:message key="app.sch.addconcerntracking.concernCategory"/></td>
          <td id="category" align="left"></td>
		</tr>	
		
		<tr id="ConcernPriority" >
		  <td align=left ><img src="./images/blank.png" alt="" id="ConcernPriorityError"/></td>
          <td align=left >&nbsp;&nbsp;<bean:message key="app.sch.addconcerntracking.concernPriority"/></td>
          <td id="priority" align="left"></td>
		</tr>	
	    
	    <tr id="ConcernStatus" >
		  <td align=left ><img src="./images/blank.png" alt="" id="ConcernStatusError"/></td>
          <td align=left >&nbsp;&nbsp;<bean:message key="app.sch.addconcerntracking.concernStatus"/></td>
          <td id="status" align="left"></td>
		</tr>	
		
		<tr>
          <td align=left ><img src="./images/blank.png" alt="" id="showStreetAddError"/></td>
          <td align=left valign="top">&nbsp;&nbsp;<bean:message key="app.sch.addconcerntracking.comment"/></td>
          <td><html:textarea property="replydesc" styleId="replydesc" style="border:1px solid #000000" rows="5" cols="40"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="shopstreetAddMsg" ></td>
        </tr>
  
          <tr>
           <td></td>
           <td colspan="2" align="center">
            <input type="button" value ="Cancel" onclick ="javascript:history.back();" />
            
          </td>
        </tr>
      </table>
      </html:form> 
<br>
<script language="JavaScript">
loadProperty();
function loadProperty()
{
      document.getElementById("category").innerHTML='<%=request.getAttribute("category")%>';
	  document.getElementById("priority").innerHTML='<%=request.getAttribute("priority")%>';
	  document.getElementById("issuetitle").innerHTML='<%=request.getAttribute("issuetitle")%>';
    document.getElementById("status").innerHTML='<%=request.getAttribute("status")%>';
}
</script>

