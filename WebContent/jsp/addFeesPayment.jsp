<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import ="java.text.*"%>
<%@ page import ="java.util.*"%>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentManager.js"></script>
<script type='text/javascript' src="/schmangmt/dwr/interface/FeesPaymentManager.js"></script>
<script type='text/javascript'>
function selectTransactionType(val){
	  if(val=='Cheque'){
	  document.getElementById("modeinfotr").style.display='';
	  document.getElementById("bankinfotr").style.display='';
	  }
	  else{
	  document.getElementById("modeinfotr").style.display='none';
	  document.getElementById("bankinfotr").style.display='none';
	  }
}

function checkValidation(){
return true;
}
</script>
<% Integer userid= Integer.valueOf((String)request.getSession().getAttribute("user_id"));
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
%>
<html:form action="/FeesPaymentAction.do?operation=save" method="Post">
<table  style="border: 1px solid #000000"  align="center"  cellspacing="0" cellpadding="2" width=97% >		
			<tr class="mainhead">
				<td align="left" colspan="5">&nbsp;&nbsp;<bean:message key="app.sch.addfeespayment.formtitle"/></td>
			</tr>
			
			<tr style="display: table-row;border: 1px solid #000000" id="searchdist">  
				<td align="left" width="2%"><img src="./images/blank.png" alt="" id="distcodeError"/></td>
				<td align=left width=15%>
	            <input type="button" name="select" value="Search Student" onclick="javascript:opensearch()"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="distcodeMsg"></div></td>
                <td align="left" width=15%></td>  
				<td align="left" width=15% ></td>
				<td align="left" width=60% ></td>
        	</tr>

			<tr id="admissionnotr" style="display:none">
				<td align="left" width="2%"></td>
				<td align="left" width=15% rowspan="1" ><b><bean:message key="app.sch.addfeespayment.admissionno"/></b></td>
				<td id="admission_no" align="left" width=15%></td>
				<td align="left" width=15%><b><bean:message key="app.sch.addfeespayment.dateofbirth"/></b></td>
				<td id="dateofbirth" align="left" width=15%></td>
			</tr>	
			
			<tr id="studentnametr" style="display:none">
				<td align="left" width="2%"></td>
				<td align="left" width=15%><b><bean:message key="app.sch.addfeespayment.studentname"/></b></td>
                <td id="first_name" align="left" width=15%>
				<td align="left" width=15%><b><bean:message key="app.sch.addfeespayment.fathername"/></b></td>
				<td id="father_name" align="left" width=15%></td>
           </tr>
		   <tr id="addresstr" style="display:none">
				<td align="left" width="2%"></td>
				<td align="left" width="15%"><b><bean:message key="app.sch.addfeespayment.address"/></b></td>
				<td align="left" width="15%"><textarea name="address" id="address" rows="3" cols="15"></textarea></td>
				<td align="left" width="15%" ><b><bean:message key="app.sch.addfeespayment.paymentdate"/></b></td>
				<td align="left" width=15% Id="odate" ><html:text property="paymentDate" size="20" styleClass="srchbox" styleId="paymentDate" style="border:1px solid #000000" value="" title="Payment Date" /><bean:message key="app.sch.addfeespayment.dateformat"/></td>
		    </tr> 
			<tr id="classnametr" style="display:none">
				<td align="left" width="2%"></td>
				<td align="left" width="15%"><Label for ="name"><b><bean:message key="app.sch.addfeespayment.session"/></b></Label></td>
				<td align="left" width="15%"><html:select property="sessionid"  styleId= "sessionid" onchange="searchByattribute();"></html:select></td>
				<td align="left" width="15%"><b><bean:message key="app.sch.addfeespayment.class"/></b></td>
                <td id="class_name" align="left" width="15%"></td>
           </tr>	
		   <tr id="monthtr" >
				<td align="left" width="2%"></td>
				<td align="left" width="15%"><Label for ="name"><b><bean:message key="app.sch.addfeespayment.month"/></b></Label></td>
				<td align="left" width="15%"><html:select property ="monthselection" styleId= "monthselection" size="8" multiple="multiple" onchange="selectMonths(this.value);" style="overflow: auto">
				<option value="-1" /><----Select All----></option>  
				<html:options collection="monthList"  property="monthid"  labelProperty="monthname" />
				</html:select></td>
				<td align="left" width="15%"><b><bean:message key="app.sch.addfeespayment.amount"/></b></td>
                <td align="left" width="15%"><html:text property="monthly_fee" size="20" styleClass="srchbox" styleId="monthly_fee" style="border:1px solid #000000" value="" title="monthly Fee"  /></td>    
           </tr>           
		   
		   
		   <tr id="transactiontr" >
				<td align="left" width="2%"></td>
				<td align="left" width="15%"><Label for ="transaction"><b><bean:message key="app.sch.addfeespayment.paymentmode"/></b></Label></td>
				<td align="left" width="15%"><html:radio property="paymentMode" styleClass="srchbox" styleId="cashMode" style="border:1px solid #000000" value="Cash" onchange="selectTransactionType(this.value);" ></html:radio><b>Cash</b></td>
				<td align="left" width="15%"><html:radio property="paymentMode" styleClass="srchbox" styleId="chequeMode" style="border:1px solid #000000" value="Cheque" onchange="selectTransactionType(this.value);" ></html:radio><b>Cheque</b></td>
                <td align="left" width="15%"></td>    
           </tr>	
	
		   
		    <tr id="modeinfotr" style="display:none">
				<td align="left" width="2%"></td>
				<td align="left" width="15%"><Label for ="transaction"><b><bean:message key="app.sch.addfeespayment.chequenumber"/></b></Label></td>
				<td align="left" width="15%"><html:text property="chequeNo" size="20" styleClass="srchbox" styleId="chequeNo" style="border:1px solid #000000" value="" title="cheque Number"  /></td>
				<td align="left" width="15%"><Label for ="transaction"><b><bean:message key="app.sch.addfeespayment.dateoncheque"/></b></td>
                <td align="left" width="15%"><html:text property="dateoncheque" size="20" styleClass="srchbox" styleId="dateoncheque" style="border:1px solid #000000" value="1900-00-00" title="Date on cheque"  /><bean:message key="app.sch.addfeespayment.dateformat"/></td>    
           </tr>	
			<tr id="bankinfotr" style="display:none">
				<td align="left" width="2%"></td>
				<td align="left" width="15%"><Label for ="transaction"><b><bean:message key="app.sch.addfeespayment.bankname"/></b></Label></td>
				<td align="left" width="15%"><html:text property="bankname" size="20" styleClass="srchbox" styleId="bankname" style="border:1px solid #000000" value="" title="monthly Fee"  /></td>
				<td align="left" width="15%"><Label for ="transaction"></td>
                <td align="left" width="15%"></td>    
           </tr>
			<tr id="actiontr" >
				<td align="left" width="2%"></td>
				<td align="left" width="15%"><Label for ="transaction"></Label></td>
				<td align="left" width="15%"></td>
				<td align="left" width="15%">
				<input type="submit" value ="Submit" onclick="return checkValidation();"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" value ="Cancel"  onclick="javascript:history.back();" /></td>
                <td align="left" width="15%"></td>    
           </tr>		   
    
</table>
<html:hidden property="studentid" styleId="studentid" styleClass="srchbox" style="border:1px solid #000000" value="" />
<html:hidden property="classid" styleId="classid" styleClass="srchbox" style="border:1px solid #000000" value="" />
<hidden name="monthlyfee" id="monthlyfee" value="" />
</html:form>      
<script language="javascript">
var x = 0;
function opensearch()
{
	x=1;
	window. open('/schmangmt/FeesPaymentAction.do?operation=searchStudent',"mywindow","location=1,status=1,scrollbars=1,width=700,height=700");
	
	document.getElementById('admissionnotr').style.display='';
    document.getElementById('studentnametr').style.display='';
 	document.getElementById('addresstr').style.display='';
	document.getElementById('classnametr').style.display='';
	
	var selectmonths=document.getElementById("monthselection").value;
	  var len=document.getElementById("monthselection").length;
	   for(var xx=0;xx<len;xx++){
		document.getElementById("monthselection").options[xx].selected=false;
		}
		document.getElementById("monthly_fee").value='0.0';
		document.getElementById("monthlyfee").value='0.0';

	loadSession();
 }

function formsubmit()
{
	 var newId = document.getElementById("newid").value; 
	 document.DistributorOrderForm.action="/FeesPaymentAction.do?operation=view&Id="+newId;
  	 document.DistributorOrderForm.submit();
}

function loadSession()
    {
    	var sessionoption = document.getElementById("sessionid");
		var sessionid= document.getElementById("sessionid").value;
		EduStudentManager.getSession(function(session){
				for(var xx=0;xx<session.length;xx++)
				{
					var catidvalue = session[xx].split(",");
					if(sessionid==catidvalue[0]){
					sessionoption[xx]= new Option(catidvalue[1],catidvalue[0],true); 
					}
					else{
					sessionoption[xx]= new Option(catidvalue[1],catidvalue[0]); 
					}
				}
				 
        	});
       
    }
function searchByattribute(){
		var selectedSession = document.getElementById("sessionid").value;
		var admissionId=document.getElementById("studentid").value;
		var strmonth="&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Monthly Fee&nbsp";
		FeesPaymentManager.getClassBySession(admissionId,selectedSession,function(classname){
			if(classname==null){
				classname=" "+","+" "+","+" ";
				strmonth="";
				}
		     var classinfo=	classname.split(",");
	         document.getElementById("class_name").innerHTML  = classinfo[0]+strmonth+classinfo[1];
	         document.getElementById("class_id").value=classinfo[2];
		});


}

function selectMonths(val){
	  var selectmonths=document.getElementById("monthselection").value;
	  var len=document.getElementById("monthselection").length;
	  var month_fee = document.getElementById("monthlyfee").value;
	  if(selectmonths==-1){
	    for(var xx=1;xx<len;xx++){
		document.getElementById("monthselection").options[xx].selected=true;
		}
		document.getElementById("monthselection").options[0].selected=false;
	}
	  var monthcount=0;
	  for(var xx=1;xx<len;xx++){
		if(document.getElementById("monthselection").options[xx].selected){
		monthcount=monthcount+1;
		}
	}
	document.getElementById("monthly_fee").value=(month_fee*monthcount);	
	document.getElementById("monthselection").options[0].selected=true;
	document.getElementById("monthselection").options[0].selected=false;
	
}
function check(){
  document.getElementById("cashMode").checked=true;
   var selectmonths=document.getElementById("monthselection").value;
	  var len=document.getElementById("monthselection").length;
	   for(var xx=0;xx<len;xx++){
		document.getElementById("monthselection").options[xx].selected=false;
		}
		document.getElementById("monthly_fee").value='0.0';
		document.getElementById("monthlyfee").value='0.0';
  }
  
  check();

</script>