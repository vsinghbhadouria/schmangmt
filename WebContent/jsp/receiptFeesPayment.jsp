<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import ="java.text.*"%>
<%@ page import ="java.util.*"%>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentManager.js"></script>
<script type='text/javascript' src="/schmangmt/dwr/interface/FeesPaymentManager.js"></script>
<link href="styles/default.css" rel="stylesheet" type="text/css" />
<script type='text/javascript'>

</script>
<% Integer userid= Integer.valueOf((String)request.getSession().getAttribute("user_id"));
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
%>
<html:form action="/FeesPaymentAction.do?operation=save" method="Post">
<table style="" align="center" cellspacing="2" cellpadding="5" width=100% border="0">
	<tr><td class="mainhead" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="app.sch.schoolName"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   </td></tr>
 </table>
<div style="border: 0px solid;width:100%" align=center>
      <fieldset id="optionsContainer">
		  <legend><b>Receipt</b></legend>
	
		<table align=center>
		
		<tr id="admissionnotr" >
				<td align="left" width="2%"></td>
				<td align="left" width=15% rowspan="1" ><bean:message key="app.sch.addfeespayment.receiptno"/></td>
				<td id="receiptno" align="left" width=25%></td>
				<td align="left" width=15%><bean:message key="app.sch.addfeespayment.departmentname"/></td>
				<td id="departmentname" align="left" width=25%></td>
		</tr>	
	
		<tr id="admissionnotr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10% rowspan="1" ><bean:message key="app.sch.addfeespayment.admissionno"/></td>
				<td id="admission_no" align="left" width=10%></td>
				<td align="left" width=10%><bean:message key="app.sch.addfeespayment.dateofbirth"/></td>
				<td id="dateofbirth" align="left" width=10%></td>
		</tr>	
		
		<tr id="studentnametr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.addfeespayment.studentname"/></td>
                <td id="studentname" align="left" width=10%>
				<td align="left" width=10%><bean:message key="app.sch.addfeespayment.fathername"/></td>
				<td id="fathername" align="left" width=10%></td>
        </tr>
		<tr id="studentnametr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.addfeespayment.class"/></td>
                <td id="classname" align="left" width=10%>
				<td align="left" width=10%><bean:message key="app.sch.addfeespayment.paymentdate"/></td>
				<td id="paymentdate" align="left" width=10%></td>
        </tr>
		</table>
		
		 <div style="border: 0px solid;width:100%" align=center>
        <fieldset id="optionsContainer11">
		  <legend><b>Fee Collection</b></legend>
		  <table border=1 align=center>
		  <tr id="admissionnotr" >
				<td align="center" width=700><bean:message key="app.sch.addfeespayment.month"/></td>
				<td align="center" width=300><bean:message key="app.sch.addfeespayment.amount"/></td>
		  </tr>	
		  <tr id="admissionnotr" >
				<td align="center" width=700 id="monthname"></td>
				<td align="center" width=300 id="amount"></td>
		  </tr>
		  <tr id="admissionnotr" >
				<td align="center" width=700>Total Amount</td>
				<td align="center" width=300 id="totalamount"></td>
		  </tr>	  
		  </table>
		  
	   </fieldset>
	   <!--<div>
	   <table>
	    <tr id="actiontr" >
				<td align="left" width="2%"></td>
				<td align="left" width="15%"><Label for ="transaction"></Label></td>
				<td align="left" width="15%">
				<input type="submit" value ="Submit" onclick="return checkValidation();"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" value ="Cancel"  onclick="javascript:history.back();" /></td>
                <td align="left" width="15%"></td>    
           </tr>
       </table>		   
	   </div>-->
	   </div>
	  </fieldset>
	  
	 
</div>
<html:hidden property="studentid" styleId="studentid" styleClass="srchbox" style="border:1px solid #000000" value="" />
<html:hidden property="classid" styleId="classid" styleClass="srchbox" style="border:1px solid #000000" value="" />
<hidden name="monthlyfee" id="monthlyfee" value="" />
</html:form>      
<script language="javascript">

  function showData()
    {


          
      	document.getElementById("studentname").innerHTML='<%=request.getAttribute("studentname")%>';
    	document.getElementById("admission_no").innerHTML='<%=request.getAttribute("studentid")%>';
    	document.getElementById("dateofbirth").innerHTML='<%=request.getAttribute("dateofbirth")%>';
		document.getElementById("fathername").innerHTML='<%=request.getAttribute("fathername")%>';
		document.getElementById("classname").innerHTML='<%=request.getAttribute("classname")%>';
		document.getElementById("paymentdate").innerHTML='<%=request.getAttribute("paymentdate")%>';
		document.getElementById("amount").innerHTML='<%=request.getAttribute("amount")%>';
		document.getElementById("totalamount").innerHTML='<%=request.getAttribute("amount")%>';
		document.getElementById("monthname").innerHTML='<%=request.getAttribute("monthList")%>';
		document.getElementById("receiptno").innerHTML='<%=request.getAttribute("receiptno")%>';
		document.getElementById("departmentname").innerHTML='<%=request.getAttribute("departmentname")%>';
		window.print();
		
    }
	showData();

</script>