<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import="java.util.*"%>
<%@page import="com.sch.delegates.EduStudentManager"%>
<!--begin custom header content for this example-->
<style type="text/css">
/* custom styles for this example */
#yui-history-iframe {
  position:absolute;
  top:0; left:0;
  width:1px; height:1px; /* avoid scrollbars */
  visibility:hidden;
}
</style>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentManager.js"></script>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentReportManager.js"></script>
<script type="text/javascript" src="./yui/build/yui/yui-min.js"></script>
<!--end custom header content for this example-->

<!--BEGIN SOURCE CODE FOR EXAMPLE =============================== -->
<form name ="EduStudentReportForm" action="/schmangmt/EduStudentReportAction.do?operation=createReport" method ="post">
<table align="center" width="97%" cellpadding="0" cellspacing="0" >
<tr class="mainhead" height="25px"><td>
		&nbsp;&nbsp;<b><bean:message key="app.sch.prepareReportCard.reportgenerationlist"/></b>
		&nbsp;					
		</td>
		<td align="right" valign="middle" class="mainhead">
					<input type="text" onkeypress="if(event.keyCode==13) searchByattribute()" name="searchbyname" id="searchbyname" size="20" maxlength="25"  class="srchbox" />&nbsp;&nbsp;
					<a href="#" onclick="searchByattribute()">
						<img src="./images/searcharrow.jpg" align="top">
					</a>&nbsp;&nbsp;
        </td>
		</tr>
	  
<tr align="left" style="background-image: url('./images/ltbrgrey1.gif')">
<td colspan="2">    
					<Label for ="name"><b>&nbsp;&nbsp;Session &nbsp;&nbsp; </b></Label>
			        <select name ="session" id= "session" onchange="searchByattribute();">
					</select>
					<Label for ="name"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Class &nbsp;&nbsp; </b></Label>
			        <select name ="class" id= "class" onchange="searchByattribute();">
					</select>
</td>
	</tr>	
<tr><td >
&nbsp;</td></tr>
<tr><td colspan="2">
	<div class="example yui3-skin-sam">
    <div id="dtable"  ></div>
    <button id="btnSelected" class="yui3-button" style="margin-top:1em;"><bean:message key="app.sch.prepareReportCard.reportgenerate"/></button>
    
  
</div>
</td></tr></table>
<table border=0 align=center id="marktr">
		  	
</table>	
</form>	
<div style="margin-bottom:1em;">
<table border=0 style="width:100em;" >
<tr>
<td style="width:1em;">
</td>
<td>
<button id="btnClearSelected" class="yui3-button" style="width:9.3em;"><bean:message key="app.sch.prepareReportCard.clearall"/></button>
</td>	
</tr>  	
</table>
</div>
<div id="doc">
    

    <div class="yui3-g">
        <div class="yui3-u-3-4">
            <div id="main">
                <div class="content"><style scoped>
			
/* css to counter global site css */
.example table {
    width: auto;
}

.yui3-skin-sam .yui3-datatable-col-select {
    text-align: center;
}

#processed {
   margin-top: 2em;
   border: 2px inset;
   border-radius: 5px;
   padding: 1em;
   list-style: none;
}
</style>

<script type="text/javascript">
var searchBysessionkey = "";
var searchByclasskey = "";
  function loadSession()
    {
    	var sessionoption = document.getElementById("session");
    	EduStudentManager.getSession(function(session){
				for(var xx=0;xx<session.length;xx++)
				{
					var catidvalue = session[xx].split(",");
					sessionoption[xx]= new Option(catidvalue[1],catidvalue[0]); 						
				}
				 
        	});
       
    }
	
	function loadClass()
    {
    	var classoption = document.getElementById("class");
    	EduStudentManager.getClasses(function(classinfo){
				for(var xx=0;xx<classinfo.length;xx++)
				{
					var catidvalue = classinfo[xx].split(",");
					classoption[xx]= new Option(catidvalue[1],catidvalue[0]); 						
				}
				 
        	});
       
    }
//Table cell formating for Buttons

 function searchByattribute(){
	 document.getElementById("dtable").innerHTML = "";
	 searchBysessionkey = trim(document.getElementById("session").value);
	 searchByclasskey = trim(document.getElementById("class").value);
	 var searchKey = trim(document.getElementById("searchbyname").value);
 YUI({ filter: 'raw' }).use( "datatable-sort", "datatable-scroll", "cssbutton", function (Y) {
     EduStudentReportManager.getResultData(searchKey,searchBysessionkey,searchByclasskey,function(resultData){
		   obj = eval ("(" + resultData + ")");

    var table = new Y.DataTable({
        columns : [
            {   key:        'select',
                allowHTML:  true, // to avoid HTML escaping
                label:      '<center><input type="checkbox" class="protocol-select-all" title="Toggle ALL records" /></center>',
                formatter:      '<center><input type="checkbox" checked /></center>',
                emptyCellValue: '<center><input type="checkbox" /></center>'
			},
            {   key: 'student_id',   label: 'Stdudent Id' },
            {   key: 'student_name',  label: 'Stdudent Name' },
			{   key: 'dept_name', label: 'Department Name' },
            {   key: 'class', label: 'Class Name' },
			{   key: 'status', label: 'Result' },
			{   key: 'division', label: 'Division' }
	    ],
        data      : obj,
        scrollable: 'y',
        height    : '250px',
        sortable  : ['student_id','student_name'],
        sortBy    : 'student_id',
        recordType: ['select', 'student_id', 'student_name','dept_name','class','status','division','class_id','dept_id','session_id'],
		width	  : '100%'
		}).render("#dtable");

    // To avoid checkbox click causing harmless error in sorting
    // Workaround for bug #2532244
    table.detach('*:change');
	//----------------
    //   "checkbox" Click listeners ...
    //----------------

    // Define a listener on the DT first column for each record's "checkbox",
    //   to set the value of <code>select</code> to the checkbox setting
    table.delegate("click", function(e){
        // undefined to trigger the emptyCellValue
        var checked = e.target.get('checked') || undefined;

        this.getRecord(e.target).set('select', checked);

        // Uncheck the header checkbox
        this.get('contentBox')
            .one('.protocol-select-all').set('checked', false);
    }, ".yui3-datatable-data .yui3-datatable-col-select input", table);


    // Also define a listener on the single TH "checkbox" to
    //   toggle all of the checkboxes
    table.delegate('click', function (e) {
        // undefined to trigger the emptyCellValue
        var checked = e.target.get('checked') || undefined;

        // Set the selected attribute in all records in the ModelList silently
        // to avoid each update triggering a table update
        this.data.invoke('set', 'select', checked, { silent: true });

        // Update the table now that all records have been updated
        this.syncUI();
    }, '.protocol-select-all', table);

    //----------------
    //  CSS-Button click handlers ....
    //----------------
    function process() {
        var ml  = table.data,
            msg = '',
            template = '{index}:{student_id}:{class_id}:{session_id}:{dept_id}';

        ml.each(function (item, i){
            var data = item.getAttrs(['select', 'student_id', 'student_name','class_id','dept_id','session_id']);
            if (data.select) {
                data.index = i;
                msg += Y.Lang.sub(template, data)+",";
				
            }
        });
		var newRow = document.getElementById("marktr").insertRow(0);
		var oCell = newRow.insertCell(0);
		oCell.innerHTML="<input type='hidden' name='subin' id='subin' value='"+msg+"'/>";
        //Y.one("#processed").setHTML(msg || '<li>(None)</li>');
		submitApp();
    }

    Y.one("#btnSelected").on("click", process);

    Y.one("#btnClearSelected").on("click",function () {
        table.data.invoke('set', 'select', undefined);

        // Uncheck the header checkbox
        table.get('contentBox')
            .one('.protocol-select-all').set('checked', false);

        //process();
		});
    });

});
 }
 function submitApp()
 {
	
	var subin = document.getElementById("subin").value;
	document.EduStudentReportForm.submit();
 
 }
 searchByattribute();
 loadSession();
 loadClass();
 
</script>

<!--END SOURCE CODE FOR EXAMPLE =============================== -->









