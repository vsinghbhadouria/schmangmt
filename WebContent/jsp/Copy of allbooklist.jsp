<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="shortcut icon" type="image/ico" href="http://www.sprymedia.co.uk/media/images/favicon.ico">
		
		<title>TableTools example</title>
		<style type="text/css" title="currentStyle">
			@import "./jquery-ui-1.10.3/themes/base/demo_table_jui.css";
			@import "./jquery-ui-1.10.3/themes/base/jquery-ui-1.8.4.custom.css";
			@import "./jquery-ui-1.10.3/themes/base/TableTools_JUI.css";
		</style>
		<script src="./jquery-ui-1.10.3/ui/jquery-1.10.2.js"></script>
		<script src="./jquery-ui-1.10.3/ui/jquery.dataTables.js"></script>
		<script src="./jquery-ui-1.10.3/ui/ZeroClipboard.js"></script>
		<script src="./jquery-ui-1.10.3/ui/TableTools.js"></script> 
		<script type="text/javascript" >
			$(document).ready( function () {
				$('#example').dataTable( {
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
				} );
			} );
		</script>
	</head>
	<body id="dt_example">
<div id="container">
<div id="demo">
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<th>Rendering engine</th>
			<th>Browser</th>
			<th>Platform(s)</th>
			<th>Engine version</th>
			<th>CSS grade</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th>Rendering engine</th>
			<th>Browser</th>
			<th>Platform(s)</th>
			<th>Engine version</th>
			<th>CSS grade</th>
		</tr>
	</tfoot>
</tbody>
</table>
			</div>

	</body>
</html>