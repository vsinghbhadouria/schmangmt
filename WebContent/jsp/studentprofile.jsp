<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import ="java.text.*"%>
<%@ page import ="java.util.*"%>
<%@ page import ="com.sch.to.EduStudentReportTO"%>

<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentReportManager.js"></script>
<%
Integer userid= Integer.valueOf((String)request.getSession().getAttribute("user_id"));
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");	
%>
<html:form action="/EduStudentReportAction.do?operation=saveResult" method="Post">
<div  align=left width="70">
      <fieldset id="optionsContainer" width="70">
		  <legend><b><bean:message key="app.sch.studentprofile.personaldata"/></b></legend>
		<table align=left border=0 width=100%>
		<tr>
		<td width=50%>	
		<table align=left border=0 width=100%>
		
		<tr id="departmentnametr" >
				<td align="left" width="2%"></td>
				<td align="left" width=15% rowspan="1" ><bean:message key="app.sch.prepareReportCard.rollnumber"/></td>
				<td id="student_id" align="left" width=25%></td>
				<td align="left" width=15%><bean:message key="app.sch.studentprofile.user_id"/></td>
				<td id="user_id" align="left" width=25%></td>
		</tr>	

		
		<tr id="studentnametr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.studentname"/></td>
                <td id="studentname" align="left" width=10%>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.fathername"/></td>
				<td id="fathername" align="left" width=10%></td>
        </tr>
		<tr id="classtr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.studentprofile.admissiondate"/></td>
                <td id="admissiondate" align="left" width=10%>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.dateofbirth"/></td>
				<td id="dateofbirth" align="left" width=10%></td>
        </tr>
        <tr id="classtr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.studentprofile.emailid"/></td>
                <td id="emailid" align="left" width=10%>
				<td align="left" width=10%><bean:message key="app.sch.studentprofile.phoneno"/></td>
				<td id="phoneno" align="left" width=10%></td>
        </tr>
        	
       <tr id="classtr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.studentprofile.address"/></td>
                <td id="address" align="left" width=10% colspan=3>
	    </tr>
        
         		
		</table>
		</td>
		
		<td width=30%>
		<table style="border:1px dotted #2B1B17;"><tr><td>	
		<img src="profile-pics/<%=request.getAttribute("user_id")%>.JPG"
	width="200" height="130" alt="Please provide student's photo" align="right"/>
		</td></tr></table>
		</td>
		</tr>
		</table>	
		<%
		   ArrayList<EduStudentReportTO> eduStudentReportToArrayObj=(ArrayList<EduStudentReportTO>)request.getAttribute("studentclassObj");
		
		   for(int xx=0;xx<eduStudentReportToArrayObj.size();xx++){
			   if(eduStudentReportToArrayObj.get(xx).getClassname()!=null){
		%>	
		<fieldset id="educationdata">
		  <legend ><b><bean:message key="app.sch.studentprofile.educationaldata"/>    <%=eduStudentReportToArrayObj.get(xx).getSession_year()%> </b></legend>
		
		<table border=0 align=left id="marktr11" width=100%>
		 <tr>
		 <td width=50%>
		 <div  align=left>
         <table border=0 align=left id="marktr1">
		 <tr>
		  <tr >
		    <td align="left" width=9%><bean:message key="app.sch.prepareReportCard.class"/></td>
			<td align="left" width=15%><%=eduStudentReportToArrayObj.get(xx).getClassname()%></td>
			<td align="left" width=9%><bean:message key="app.sch.prepareReportCard.session"/></td>
			<td align="left" width=14%><%=eduStudentReportToArrayObj.get(xx).getSession_year()%></td>
		  </tr>
		  <tr >
		    <td align="left" width=9%><bean:message key="app.sch.studentprofile.departmentname"/></td>
			<td align="left" width=15%><%=eduStudentReportToArrayObj.get(xx).getDepartmentname()%></td>
			<td align="left" width=9%><bean:message key="app.sch.studentprofile.division"/></td>
			<td align="left" width=14%><%=eduStudentReportToArrayObj.get(xx).getDivision()%></td>
		  </tr>
		  <tr >
		    <td align="left" width=9%><bean:message key="app.sch.studentprofile.percentage"/></td>
			<td align="left" width=15%><%=eduStudentReportToArrayObj.get(xx).getPercentage()%></td>
			<td align="left" width=9%><bean:message key="app.sch.studentprofile.obtained"/></td>
			<td align="left" width=14%><%=eduStudentReportToArrayObj.get(xx).getTotalMarkObtained()%>/<%=eduStudentReportToArrayObj.get(xx).getTotalOutOf()%></td>
		  </tr>
		  
		 </tr>
		 </table>
	
		 </td>
		 <td width=30%>
		 <% 
		 	if(eduStudentReportToArrayObj.get(xx).getSubject()!=null){
		 %>
		 <fieldset id="subjectdata">
		  <legend ><b><bean:message key="app.sch.prepareReportCard.subject"/></b></legend>
		 <table border=0 align=left id="subjectname">
		 	<%
			for(int ii=0;ii<eduStudentReportToArrayObj.get(xx).getSubject().length;ii++){
			%>
		 <tr id="subjecttr" >
		 		<td align="left" width=40%><%=eduStudentReportToArrayObj.get(xx).getSubject()[ii]%></td>
		 </tr>	
          <%
		  }
		  %>		 
		 </table>
		 </fieldset>
			<%
		 	}
			%>
	  
	   </div>
	    </td>
		 </tr>
		  </table>
		 </fieldset>  
		  <%
		 	}
		}
		 %>
	  
	  <table border=0 align="center" cellspacing=2 cellpadding=5 width=100%  > 
		<tr>   
		<td colspan="2" align="center" >
            <input type="button" value ="Cancel"  onclick="javascript:history.back();" />
        </td>
        
        </tr>
      </table>
		 
	  </fieldset>
	
	 
</div>
</html:form>      
<script language="javascript">
  function showData()
    {
     	document.getElementById("studentname").innerHTML='<%=request.getAttribute("studentname")%>';
    	document.getElementById("student_id").innerHTML='<%=request.getAttribute("studentid")%>';
    	document.getElementById("dateofbirth").innerHTML='<%=request.getAttribute("dateofbirth")%>';
		document.getElementById("fathername").innerHTML='<%=request.getAttribute("fathername")%>';
		document.getElementById("user_id").innerHTML='<%=request.getAttribute("user_id")%>';
		document.getElementById("admissiondate").innerHTML='<%=request.getAttribute("admissiondate")%>';
		document.getElementById("emailid").innerHTML='<%=request.getAttribute("emailid")%>';
		document.getElementById("address").innerHTML='<%=request.getAttribute("address")%>';
		document.getElementById("phoneno").innerHTML='<%=request.getAttribute("phoneno")%>';
		
		
	}
	showData();
	</script>