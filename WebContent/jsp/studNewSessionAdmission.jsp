<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import ="java.text.*"%>
<%@ page import ="java.util.*"%>
<%@ page import ="com.sch.to.EduStudentReportTO"%>

<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentReportManager.js"></script>

<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentManager.js"></script>

<script type='text/javascript' src="/schmangmt/dwr/interface/EduStaffManager.js"></script>
<%
Integer userid= Integer.valueOf((String)request.getSession().getAttribute("user_id"));
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");	
%>
<html:form action="/EduStudentReportAction.do?operation=assignedNewSession" method="Post">
<div  align=left width="70">
      <fieldset id="optionsContainer" width="70">
		  <legend><b><bean:message key="app.sch.studentprofile.personaldata"/></b></legend>
		<table align=left border=0 width=100%>
		<tr>
		<td width=50%>	
		<table align=left border=0 width=100%>
		
		<tr id="departmentnametr" >
				<td align="left" width="2%"></td>
				<td align="left" width=15% rowspan="1" ><bean:message key="app.sch.prepareReportCard.rollnumber"/></td>
				<td id="student_id" align="left" width=25%></td>
				<td align="left" width=15%><bean:message key="app.sch.studentprofile.user_id"/></td>
				<td id="user_id" align="left" width=25%></td>
		</tr>	

		
		<tr id="studentnametr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.studentname"/></td>
                <td id="studentname" align="left" width=10%>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.fathername"/></td>
				<td id="fathername" align="left" width=10%></td>
        </tr>
		<tr id="classtr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.studentprofile.admissiondate"/></td>
                <td id="admissiondate" align="left" width=10%>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.dateofbirth"/></td>
				<td id="dateofbirth" align="left" width=10%></td>
        </tr>
        <tr id="classtr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.studentprofile.emailid"/></td>
                <td id="emailid" align="left" width=10%>
				<td align="left" width=10%><bean:message key="app.sch.studentprofile.phoneno"/></td>
				<td id="phoneno" align="left" width=10%></td>
        </tr>
        	
       <tr id="classtr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.studentprofile.address"/></td>
                <td id="address" align="left" width=10% colspan=3>
	    </tr>
        
         		
		</table>
		</td>
		
		<td width=30%>
		<table><tr><td>	
		<img src="profile-pics/<%=request.getAttribute("user_id")%>.JPG"
	width="200" height="130" alt="What We Do" align="right"/>
		</td></tr></table>
		</td>
		</tr>
		</table>	
		<%
		   ArrayList<EduStudentReportTO> eduStudentReportToArrayObj=(ArrayList<EduStudentReportTO>)request.getAttribute("studentclassObj");
		   for(int xx=0;xx<eduStudentReportToArrayObj.size();xx++){
		%>	
		<fieldset id="educationdata">
		  <legend ><b><bean:message key="app.sch.studentprofile.educationaldata"/>    <%=eduStudentReportToArrayObj.get(xx).getSession_year()%> </b></legend>
		
		<table border=0 align=left id="marktr11" width=100%>
		 <tr>
		 <td width=50%>
		 <div  align=left>
         <table border=0 align=left id="marktr1">
		 <tr>
		  <tr >
		    <td align="left" width=9%><bean:message key="app.sch.prepareReportCard.class"/></td>
			<td align="left" width=15%><%=eduStudentReportToArrayObj.get(xx).getClassname()%></td>
			<td align="left" width=9%><bean:message key="app.sch.prepareReportCard.session"/></td>
			<td align="left" width=14%><%=eduStudentReportToArrayObj.get(xx).getSession_year()%></td>
		  </tr>
		  <tr >
		    <td align="left" width=9%><bean:message key="app.sch.studentprofile.departmentname"/></td>
			<td align="left" width=15%><%=eduStudentReportToArrayObj.get(xx).getDepartmentname()%></td>
			<td align="left" width=9%><bean:message key="app.sch.studentprofile.division"/></td>
			<td align="left" width=14%><%=eduStudentReportToArrayObj.get(xx).getDivision()%></td>
		  </tr>
		  <tr >
		    <td align="left" width=9%><bean:message key="app.sch.studentprofile.percentage"/></td>
			<td align="left" width=15%><%=eduStudentReportToArrayObj.get(xx).getPercentage()%></td>
			<td align="left" width=9%><bean:message key="app.sch.studentprofile.obtained"/></td>
			<td align="left" width=14%><%=eduStudentReportToArrayObj.get(xx).getTotalMarkObtained()%>/<%=eduStudentReportToArrayObj.get(xx).getTotalOutOf()%></td>
		  </tr>
		  
		 </tr>
		 </table>
	
		 </td>
		 <td width=30%>
		 <fieldset id="subjectdata">
		  <legend ><b><bean:message key="app.sch.prepareReportCard.subject"/></b></legend>
		 <table border=0 align=left id="subjectname">
		 	<%
			for(int ii=0;ii<eduStudentReportToArrayObj.get(xx).getSubject().length;ii++){
			%>
		 <tr id="subjecttr" >
		 		<td align="left" width=40%><%=eduStudentReportToArrayObj.get(xx).getSubject()[ii]%></td>
		 </tr>	
          <%
		  }
		  %>		 
		 </table>
		 </fieldset>
		
	  
	   </div>
	    </td>
		 </tr>
		  </table>
		 </fieldset>  
		  <%
		 }
		 %>
	  
	  <table border=0 align="center" cellspacing=2 cellpadding=5 width=100%  >
	    	<tr id="errortr" >
				<td align=left><img src="./images/blank.png" alt="" id="highereduError"/></td>
				<td align="left" width=300 colspan=2><div style ='display:none; color: red;' id="isrecordMsg"></div></td>
				
		  	</tr>
	      <tr id="csession_tab" >
          <td align=left><img src="./images/blank.png" alt="" id="highereduError"/></td><td align=left  width=25%><b><bean:message key="app.sch.addfeespayment.session"/></b></td>
		  <td align=left id="csession_year">
		  </td>
         </tr>
	           
          <tr id="dept_tab" >
          <td align=left><img src="./images/blank.png" alt="" id="highereduError"/></td><td align=left  width=25%><b><bean:message key="app.sch.addeduinstituestaff.department"/></b></td>
		  <td align=left>
			<html:select property="department" style="border:1px solid #000000" styleId = "department" value="-1" onchange="getClassInfoByDepartment()" >
				<option value="-1" />none</option>  
				<html:options collection="departments"  property="dept_id"  labelProperty="dept_title" />
			</html:select>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="highereduMsg"></td>
         </tr>
		 
		 		 
		<tr id="class_tab" style="display:none">
          <td align=left><img src="./images/blank.png" alt="" id="studclassError"/></td><td align=left  width=25%><b><bean:message key="app.sch.addedustudent.class"/></b></td>
		  <td align=left>
			<html:select property="educlass" style="border:1px solid #000000" styleId = "educlass" value="-1" onchange="displayCountry()" >
				<html:options collection="classes"  property="classid"  labelProperty="classname" />
			</html:select>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="highereduMsg"></td>
         </tr>
		<tr>
				<td ></td>
				<td colspan="2" >
				<div id='subjecttab' style="display:none" >
					<table>
						<tr>
			    			<td class="tdLabel_style" align=center><bean:message key="app.sch.addeduinstituestaff.allsubjects"/><br>
							 	<select name="allsubjects" Id="allsubjects" size=8 multiple="multiple" style="width: 200px; background-color:#EEEEEE; border:1px solid #000000; font-size : 11px; font-family : Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight : normal;"></select>
							</td>
							<td>
								<a href="javascript:assignSubjects();">
									<img src="./images/moveright.gif" border="0">
								</a>
								<br>
								<a href="javascript:moveSubjects();">
									<img src="./images/moveleft.gif" border="0">
								</a>	
							</td>	
				            <td class="tdLabel_style" align=center><bean:message key="app.sch.addeduinstituestaff.assignedsubjects"/><br>
								<select name="selectedsubjects" Id="selectedsubjects" size=8 multiple="multiple"  style=" width: 200px; background-color:#FFFFFF; border:1px solid #000000; font-size : 11px; font-family : Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight : normal;"></select>
								
							</td>
						</tr>
						<tr >
							<td></td>
							<td></td>
							<td>
								<img src="./images/blank.png" alt="" id="zipcodeError"/><div style ='display:none; color: red;' id="zipcodeMsg">
							</td>
						</tr>
					</table>
					</div>
				</td>
				<td ></td>
			</tr>
			
		 <tr id="admissionid" style="display: none;">
          <td align=left width="1%" ><img src="./images/blank.png" alt="" id="admissionfeeError"/></td>
		  <td align=left  width=15%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.admissionfee"/></b></td>
          <td align="left"><html:text property="admissionfee" size="20" styleClass="srchbox" styleId="admissionfee" style="border:1px solid #000000" title="Admission Fee"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="admissionfeeMsg"></div></td>
         </tr>
         
         <tr id="checkconid" style="display: none;">
          <td align=left width="1%" ><img src="./images/blank.png" alt="" id="admissionfeeError"/></td><td align=left  width=22%>&nbsp;&nbsp;&nbsp;<b></b></td>
          <td align="left"><html:checkbox property="admissionfee" styleId="checkconcessionid" onclick="displayConsession()" />&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.studentconcession"/></b><div style ='display:none; color: red;' id="admissionfeeMsg"></div></td>
         </tr>
         
		 <tr id="concessid" style="display: none;">
          <td align=left width="3%" ><img src="./images/blank.png" alt="" id="concessError"/></td>
		  <td align=left  width=20%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.concessionamount"/></b></td>
          <td align="left" width=60% ><html:text property="concessionamt" size="20" styleClass="srchbox" styleId="concessionamt" style="border:1px solid #000000" value="" title="Concession Amount"  />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="concessMsg"></div></td>
         </tr>
		 <tr id="commentid" style="display: none;">
		 	<td align=left width="1%"><img src="./images/blank.png" alt="" id="commentError"/></td>
		 	<td align=left  width=15%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.comments"/></b></td>
         	<td align="left" ><html:textarea property="comment"   styleId="comment" style="border:1px solid #000000" value="" rows="3" cols="20"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="commentMsg"></div></td>
        </tr>
	   
		<tr>   
		<td align="center" colspan=3>
		<input type="submit" value ="Submit" onclick="return isRecordExist();"/>
		<input type="button" value ="Cancel"  onclick="javascript:history.back();" />
		</td>
	    
        </tr>
      </table>
		 
	  </fieldset>
	
	 
</div>
<html:hidden property="sessionid" styleId="sessionid" styleClass="srchbox" style="border:1px solid #000000" />
<html:hidden property="studentid" styleId="studentid" styleClass="srchbox" style="border:1px solid #000000" value="" />

</html:form>      
<script language="javascript">
  function showData()
    {
	
     	document.getElementById("studentname").innerHTML='<%=request.getAttribute("studentname")%>';
    	document.getElementById("student_id").innerHTML='<%=request.getAttribute("studentid")%>';
    	document.getElementById("dateofbirth").innerHTML='<%=request.getAttribute("dateofbirth")%>';
		document.getElementById("fathername").innerHTML='<%=request.getAttribute("fathername")%>';
		document.getElementById("user_id").innerHTML='<%=request.getAttribute("user_id")%>';
		document.getElementById("admissiondate").innerHTML='<%=request.getAttribute("admissiondate")%>';
		document.getElementById("emailid").innerHTML='<%=request.getAttribute("emailid")%>';
		document.getElementById("address").innerHTML='<%=request.getAttribute("address")%>';
		document.getElementById("phoneno").innerHTML='<%=request.getAttribute("phoneno")%>';
		document.getElementById("csession_year").innerHTML='<%=request.getAttribute("csession_year")%>';
		document.getElementById("sessionid").value='<%=request.getAttribute("csession_id")%>';
		document.getElementById("studentid").value='<%=request.getAttribute("studentid")%>';
	}
	showData();

	//display all countries in dropdown box // display all upper levels in combo box
	
	 function isRecordExist(){

		 var student_id = document.getElementById("studentid").value;
		 var class_id = document.getElementById("educlass").value;
		 var session_id = document.getElementById("sessionid").value;
		 document.getElementById('isrecordMsg').style.display = 'none';
		 var returnfla=true;
		 var reqCome="studNewSessionAdmission";
		 EduStudentReportManager.isRecordExist(student_id,class_id,session_id,reqCome,{
			  async:false,
			  callback: function(recordFlag){

			  if(recordFlag!=false){
			  document.getElementById('isrecordMsg').innerHTML ='New Session already has been assigned for Student ID '+student_id;
			  document.getElementById('isrecordMsg').style.display = 'inline';
			  returnfla=false;
			  }
			  else{
			  returnfla=true;
			  }
			}
			 });
			 
		 formsubmit();
			return returnfla;
		 }
	
	function formsubmit()
	{

		var selSub = document.getElementById("selectedsubjects");
		var slen = selSub.length;
		for(var j=slen-1; j>=0; j--)
		{
			selSub.options[j].selected=true;
		}
	}	
	

	function assignSubjects()
	{
		var assoList = document.getElementById('allsubjects');
		var len = assoList.length;
		var selAssoList = document.getElementById('selectedsubjects');
		for(var j=len-1;j>=0;j--)
		{		
			if(assoList[j].selected && assoList[j].selected!=null){
				var selText = assoList.options[j].text;
				var selValue = assoList.options[j].value;
				if(checkSubjects(selText,selValue)){
					selAssoList[selAssoList.length]= new Option(selText, selValue, true);
				}	
				assoList.remove(j);
			}
		}
	}
	function moveSubjects(){
		var assoList = document.getElementById('allsubjects');
		var selAssoList = document.getElementById('selectedsubjects');
		var len = selAssoList.length;
		for(var j=len-1;j>=0;j--){
			if(selAssoList[j].selected  && selAssoList[j].selected!=null){
				var selText = selAssoList.options[j].text;
				var selValue = selAssoList.options[j].value;
				assoList[assoList.length]= new Option(selText, selValue, true);
				selAssoList.remove(j);
			}
		}	
	}
	function checkSubjects(seltext,selValue){
		flag = true;
		var selAssoList = document.getElementById('selectedsubjects');
		var lenAssoList = selAssoList.length;
		if(lenAssoList!=0){
			for(var xx=0;xx<lenAssoList;xx++){			 
				if(selValue==selAssoList[xx].value){ 
					flag = false; 
					break; 
				}
			}
		} 
		return flag;
	}

function displayCountry(){
	 document.getElementById('admissionid').style.display='';
	 document.getElementById('checkconid').style.display='';
	 document.getElementById('subjecttab').style.display='';
	 var educlassid = document.getElementById("educlass").value;
	 if(educlassid!=-1){
		EduStudentManager.getAdmissionfee(educlassid,function(fee){
		 document.getElementById("admissionfee").value=fee;
		 });
	}
	else{
		document.getElementById('admissionid').style.display='none';
		document.getElementById('checkconid').style.display='none';	 
	}
getSubjectByClass();
}


function getClassInfoByDepartment()
 {
    document.getElementById('class_tab').style.display='';
 	var educlass = document.getElementById('educlass');
	var allsubjects = document.getElementById('allsubjects');
	var selAssoList = document.getElementById('selectedsubjects');
    var deptId = document.getElementById('department').value;
	removeOptions(educlass);
	removeOptions(allsubjects);
	removeOptions(selAssoList);
	EduStaffManager.getClassInfoByDepartment(deptId,function(userlist){
			for(var i=0;i<userlist.length;i++)
 			{
 				var userOptions = userlist[i];
				userOptions = userOptions.split(',');
				educlass[i] = new Option(userOptions[1],userOptions[0]);
 			}
 			});
 }
 
 function getSubjectByClass()
 {
 	var allsubjects = document.getElementById('allsubjects');
	var selAssoList = document.getElementById('selectedsubjects');
	var classid = document.getElementById('educlass').value;
	var deptId = document.getElementById('department').value;
	removeOptions(allsubjects);
	removeOptions(selAssoList);
 	EduStudentManager.getSubjectByClass(deptId,classid,function(userlist){
			for(var i=0;i<userlist.length;i++)
 			{
 				var userOptions = userlist[i];
				userOptions = userOptions.split(',');
				allsubjects[i] = new Option(userOptions[1],userOptions[0]);
 			}
 			});
 }

 
  function removeOptions(allclasses)
{
	var i;
	for(i=allclasses.options.length-1;i>=0;i--)
	{
		allclasses.remove(i);
	}
}
	
  function displayConsession(){
	  var status=document.getElementById('checkconcessionid').checked;
	  if(status==true){
		document.getElementById('concessid').style.display='';
		document.getElementById('commentid').style.display='';
	  }
	  else{
		document.getElementById('concessid').style.display='none';
		document.getElementById('commentid').style.display='none';
	  }
}
	
	</script>