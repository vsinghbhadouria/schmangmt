<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
 <html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" href="./jquery-ui-1.10.3/themes/base/jquery.ui.all.css">
	<script src="./jquery-ui-1.10.3/ui/jquery-1.9.1.js"></script>
	<script src="./jquery-ui-1.10.3/ui/jquery.ui.core.js"></script>
	<script src="./jquery-ui-1.10.3/ui/jquery.ui.widget.js"></script>
	<script src="./jquery-ui-1.10.3/ui/jquery.ui.position.js"></script>
	<script src="./jquery-ui-1.10.3/ui/jquery.ui.menu.js"></script>
	<script src="./jquery-ui-1.10.3/ui/jquery.ui.autocomplete.js"></script>
	<link rel="stylesheet" href="../jquery-ui-1.10.3/demos/demos.css">

<title>Insert title here</title>
<script type='text/javascript' src="/schmangmt/dwr/interface/LibraryManagementManager.js"></script>
<script type='text/javascript' src='./scripts/schmanagmt.js'></script>
<script>
	$(function() {
		 var availableTags = [];
		LibraryManagementManager.getBookNames(function(bookNames){
			for(var xx=0;xx<bookNames.length;xx++){
				availableTags.push("" + bookNames[xx]);

			} 
		});
		$( "#tags" ).autocomplete({
			source: availableTags
		});
	});
	
	
	</script>
	
</head>
<body>

<div class="ui-widget">
	<label for="tags"></label>
	<input id="tags" name="bookname" style="border:1px solid #000000" size=15 onkeyup="getBookIds();" onmouseover="getBookIds();">  <div style ='display:none; color: red;' id="bookNameMsg"></div>
</div>

</body>
</html>