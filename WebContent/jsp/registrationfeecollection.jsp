<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import ="java.text.*"%>
<%@ page import ="java.util.*"%>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentReportManager.js"></script>
<script type='text/javascript'>

</script>

<%
Integer userid= Integer.valueOf((String)request.getSession().getAttribute("user_id"));
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");	
%>
<html:form action="/FeesPaymentAction.do?operation=saveRegPayment" method="post" >
<div  align=left width="50">
      <fieldset id="optionsContainer" width="50">
		  <legend><b><bean:message key="app.sch.registrationfeecollection.registrationDetail"/></b></legend>
	
		<table align=center >
		
		<tr id="departmentnametr" >
				<td align="left" width="2%"></td>
				<td align="left" width=15% rowspan="1" ><bean:message key="app.sch.prepareReportCard.rollnumber"/></td>
				<td id="student_id" align="left" width=25%></td>
				<td align="left" width=15%><bean:message key="app.sch.prepareReportCard.departmentname"/></td>
				<td id="departmentname" align="left" width=25%></td>
		</tr>	

		
		<tr id="studentnametr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.studentname"/></td>
                <td id="studentname" align="left" width=10%>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.fathername"/></td>
				<td id="fathername" align="left" width=10%></td>
        </tr>
		<tr id="classtr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.class"/></td>
                <td id="classname" align="left" width=10%>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.dateofbirth"/></td>
				<td id="dateofbirth" align="left" width=10%></td>
        </tr>
        		
		</table>
		
		 <div  align=left>
        <fieldset id="optionsContainer11">
		  <legend ><b><bean:message key="app.sch.registrationfeecollection.paymentDetail"/></b></legend>
		
		  <table border=0 align=center id="payment" width=69%>
		  <tr id="admissionid">
          <td align=left width="1%" ><img src="./images/blank.png" alt="" id="admissionfeeError"/></td>
		  <td align=left  width=15%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addregistration.registrationfee"/></b></td>
          <td align="left"><html:text property="registrationfee" size="20" styleClass="srchbox" styleId="registrationfee" style="border:1px solid #000000" title="Registration Fee"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="admissionfeeMsg"></div></td>
		  
         </tr>
		 
		  <tr id="checkconid" >
          <td align=left width="1%" ><img src="./images/blank.png" alt="" id="admissionfeeError"/></td>
		  <td align=left  width=22%>&nbsp;&nbsp;&nbsp;<b></b></td>
          <td align="left"><html:checkbox property="registrationfee" styleId="checkconcessionid" onclick="displayConsession()" />&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.studentconcession"/></b><div style ='display:none; color: red;' id="admissionfeeMsg"></div></td>
         </tr>
		 
		 
		  <tr id="concessid" style="display: none;">
          <td align=left width="3%" ><img src="./images/blank.png" alt="" id="concessError"/></td>
		  <td align=left  width=20%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.concessionamount"/></b></td>
          <td align="left" width=60% ><html:text property="concessionamt" size="20" styleClass="srchbox" styleId="concessionamt" style="border:1px solid #000000" value="" title="Concession Amount"  />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="concessMsg"></div></td>
         </tr>
		 <tr id="commentid" style="display: none;">
		 <td align=left width="1%"><img src="./images/blank.png" alt="" id="commentError"/></td>
		 <td align=left  width=15%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.comments"/></b></td>
         <td align="left" ><html:textarea property="comment"   styleId="comment" style="border:1px solid #000000" value="" rows="3" cols="20"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="commentMsg"></div></td>
        </tr>
		 </table>
	   </fieldset>
	   <div>
	   <table>
	    <tr id="actiontr" >
				<td align="left" width="2%"></td>
				<td align="left" width="15%"><Label for ="transaction"></Label></td>
				<td align="left" width="15%">
				<input type="submit" value ="Submit" onclick="return checkValidation();"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="button" value ="Cancel"  onclick="javascript:history.back();" /></td>
                <td align="left" width="15%"></td>    
           </tr>
       </table>		   
	   </div>
	   </div>
	  </fieldset>
	  
	 
</div>
<html:hidden property="studentid" styleId="studentid" styleClass="srchbox" style="border:1px solid #000000" />
</html:form>      
<script language="javascript">
  function showData()
    {
     	document.getElementById("studentname").innerHTML='<%=request.getAttribute("studentname")%>';
    	document.getElementById("student_id").innerHTML='<%=request.getAttribute("studentid")%>';
    	document.getElementById("dateofbirth").innerHTML='<%=request.getAttribute("dateofbirth")%>';
		document.getElementById("fathername").innerHTML='<%=request.getAttribute("fathername")%>';
		document.getElementById("classname").innerHTML='<%=request.getAttribute("classname")%>';
		document.getElementById("departmentname").innerHTML='<%=request.getAttribute("departmentname")%>';
		document.getElementById("studentid").value='<%=request.getAttribute("studentid")%>';
		document.getElementById("dept_id").value='<%=request.getAttribute("dept_id")%>';
		
    }
	showData();
		   function displayConsession(){
		   	  var status=document.getElementById('checkconcessionid').checked;
		   	  if(status==true){
		   		document.getElementById('concessid').style.display='';
		   		document.getElementById('commentid').style.display='';
		   	  }
		   	  else{
		   		document.getElementById('concessid').style.display='none';
		   		document.getElementById('commentid').style.display='none';
		   	  }
		   }
		   function checkValidation(){
		   return true;
		   }
		   
</script>