<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import="java.util.*"%>
<%Random generator = new Random();
int r = generator.nextInt();
%>
<%@page import="com.sch.delegates.EduRegistrationManager"%>
<!--begin custom header content for this example-->
<style type="text/css">
/* custom styles for this example */
#yui-history-iframe {
  position:absolute;
  top:0; left:0;
  width:1px; height:1px; /* avoid scrollbars */
  visibility:hidden;
}
</style>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduRegistrationManager.js"></script>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentManager.js"></script>
<!--end custom header content for this example-->

<!--BEGIN SOURCE CODE FOR EXAMPLE =============================== -->
<table align="center" width="97%" cellpadding="0" cellspacing="0" >
<tr class="mainhead" height="25px"><td >
		&nbsp;&nbsp;<b><bean:message key="app.sch.eduregistrationlist.listformtitle"/></b>
		&nbsp;<a href="/schmangmt/EduRegistrationAction.do?operation=createSetup"><img border="0" src="./images/add2.jpg" id="addSchl" width="7%" height="60%" align="top"></a>						
    	</td>
		<td align="right" valign="middle" class="mainhead">
		 <input type="text" onkeypress="if(event.keyCode==13) searchByattribute()" name="searchbyname" id="searchbyname" size="20" maxlength="25"  class="srchbox" />&nbsp;&nbsp;<a href="#" onclick="searchByattribute()"><img src="./images/searcharrow.jpg" 
		 align="top"></a>&nbsp;&nbsp;&nbsp;
        </td>
</tr>
	  
<tr align="left" style="background-image: url('./images/ltbrgrey1.gif')">
<td colspan="2">    
					<Label for ="name"><b>&nbsp;&nbsp;Session &nbsp;&nbsp; </b></Label>
			        <select name ="session" id= "session" onchange="searchByattribute();">
					</select>
					<Label for ="name"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Class &nbsp;&nbsp; </b></Label>
			        <select name ="class" id= "class" onchange="searchByattribute();">
					</select>
</td>
	</tr>	
<tr><td >
&nbsp;</td></tr>
<tr><td colspan="2">
	<div id="bhmintegration" style="width:100%;"></div>
	<div id="dt-pag-nav" align ="center"></div>
</td></tr></table>

<script type="text/javascript">
var searchBysessionkey = "";
var searchByclasskey = "";
var regFlag="true";
  function loadSession()
    {
    	var sessionoption = document.getElementById("session");
    	EduStudentManager.getSession(function(session){
				for(var xx=0;xx<session.length;xx++)
				{
					var catidvalue = session[xx].split(",");
					sessionoption[xx]= new Option(catidvalue[1],catidvalue[0]); 						
				}
				 
        	});
       
    }
	
	function loadClass()
    {
    	var classoption = document.getElementById("class");
    	EduStudentManager.getClasses(function(classinfo){
				for(var xx=0;xx<classinfo.length;xx++)
				{
					var catidvalue = classinfo[xx].split(",");
					classoption[xx]= new Option(catidvalue[1],catidvalue[0]); 						
				}
				 
        	});
       
    }
//Table cell formating for Buttons
 function searchByattribute() {
	 
	 searchBysessionkey = trim(document.getElementById("session").value);
	 searchByclasskey = trim(document.getElementById("class").value);
    // Column definitions
   YAHOO.example.CustomFormatting = new function() {
    	this.myCustomFormatterEditRegistration = function(elCell, oRecord, oColumn,
				oData) {
			
			if(searchBysessionkey==''){

				elCell.innerHTML = '<a href="/schmangmt/EduRegistrationAction.do?operation=updateSetup&User_id='
					+ oRecord.getData("user_id")+'&Session_id='
					+ oRecord.getData("session_id")+'&Reg_status=A'
					+ '" onMouseOut=over("EditRegistration'
					+ oRecord.getData("user_id")
					+ '","AdmissionIcon.jpg") onMouseOver=over("EditRegistration'
					+ oRecord.getData("user_id")
					+ '","AdmissionIcon.jpg")><img border=0 src="./images/AdmissionIcon.jpg" id="EditRegistration'
					+ oRecord.getData("user_id")
					+ '" title="Click Admission Icon for Admission"></a>';
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};
		
		this.myCustomFormatterDeleteRegistration = function(elCell, oRecord, oColumn,
				oData) {
			
			if(searchBysessionkey==''){

				elCell.innerHTML = '<a href="/schmangmt/EduRegistrationAction.do?operation=delete&User_id='
					+ oRecord.getData("user_id")+'&Session_id='
					+ oRecord.getData("session_id")
					+ '" onMouseOut=over("DeleteRegistration'
					+ oRecord.getData("user_id")
					+ '","close1.gif") onMouseOver=over("DeleteRegistration'
					+ oRecord.getData("user_id")
					+ '","close1.gif")><img border=0 src="./images/close1.gif" id="DeleteRegistration'
					+ oRecord.getData("user_id")
					+ '" title="Click Icon to Delete Registration"></a>';
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};

		this.myCustomFormatterviewDetails = function(elCell, oRecord, oColumn,
				oData) {			
			if(true){
				elCell.innerHTML = '<a href="/schmangmt/EduRegistrationAction.do?operation=viewDetails&User_id='
					+ oRecord.getData("user_id")+'&Session_id='
					+ oRecord.getData("session_id")+'&Reg_status=A'
					+ '" onMouseOut=over("viewDetails'
					+ oRecord.getData("user_id")
					+ '","view.gif") onMouseOver=over("viewDetails'
					+ oRecord.getData("user_id")
					+ '","view.gif")><img border=0 src="./images/view.gif" id="viewDetails'
					+ oRecord.getData("user_id")
					+ '" title="Click Icon to View Student Detail"></a>';
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};
		
		YAHOO.widget.DataTable.Formatter.EditRegistration = this.myCustomFormatterEditRegistration;
		YAHOO.widget.DataTable.Formatter.DeleteRegistration = this.myCustomFormatterDeleteRegistration;
		YAHOO.widget.DataTable.Formatter.viewDetails = this.myCustomFormatterviewDetails;

    };
    
    var searchKey = trim(document.getElementById("searchbyname").value);
    var myColumnDefs = [ // sortable:true enables sorting
                         {key:"first_name", label:"Name", sortable:true,width: 150},
                         {key:"Email", label:"Email", sortable:true,width: 200},
                         {key:"Class", label:"Class", sortable:true, width: 100},
                         {key:"Session", label:"Session", sortable:false,width: 100},
                         {key:"Phone", label:"Phone", sortable:false,width: 100},
                         {key:"viewDetails", label:"View",formatter: "viewDetails",sortable :false,resizeable :false},
                         {key:"EditRegistration", label:"Admission",formatter: "EditRegistration",sortable :false,resizeable :false},
                         {key:"DeleteRegistration", label:"Delete",formatter: "DeleteRegistration",sortable :false,resizeable :false} 
                     ];

    // Custom parser
    var stringToDate = function(sData) {
        var array = sData.split("-");
        return new Date(array[1] + " " + array[0] + ", " + array[2]);
    };
    
    // DataSource instance
    var myDataSource = new YAHOO.util.DataSource("/schmangmt/jsp/eduregistration_json.jsp?ran=<%=r%>&searchBysessionkey="+searchBysessionkey+"&searchByclasskey="+searchByclasskey+"&searchKey="+searchKey+"&regFlag="+regFlag+"&");
    myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSON;
    myDataSource.responseSchema = {
        resultsList: "records",
        fields: ["first_name","Class","Phone","user_id","Email","Session","session_id"],
        
        metaFields: {
            totalRecords: "totalRecords" // Access to value in the server response
        }
    };
   
    // DataTable configuration
    var myConfigs = {
		paginator: new YAHOO.widget.Paginator({rowsPerPage:10, containers : ["dt-pag-nav"], template : "{PageLinks} {RowsPerPageDropdown}", rowsPerPageOptions : [10,25,50,100] }), // Enables pagination 
    	initialRequest: "sort=first_name&dir=asc&startIndex=0&results=10", // Initial request for first page of data
        dynamicData: true, // Enables dynamic server-driven data
        sortedBy : {key:"first_name", dir:YAHOO.widget.DataTable.CLASS_ASC} // Sets UI initial sort arrow
    };

     
    
    // DataTable instance
    var myDataTable = new YAHOO.widget.DataTable("bhmintegration", myColumnDefs, myDataSource, myConfigs);
    // Update totalRecords on the fly with value from server
    myDataTable.handleDataReturnPayload = function(oRequest, oResponse, oPayload) {
        oPayload.totalRecords = oResponse.meta.totalRecords;
        return oPayload;
    }
    
    return {
        ds: myDataSource,
        dt: myDataTable
    };
        
}
 searchByattribute();
 loadSession();
 loadClass();
</script>

<!--END SOURCE CODE FOR EXAMPLE =============================== -->









