<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import ="java.text.*"%>
<%@ page import ="java.util.*"%>
<%@ page import ="com.sch.to.EduStudentReportTO"%>

<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentReportManager.js"></script>


<%
Integer userid= Integer.valueOf((String)request.getSession().getAttribute("user_id"));
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");	
%>
<html:form action="/EduStudentReportAction.do?operation=saveResult" method="Post">
<% 
try{

ArrayList<EduStudentReportTO>  eduStudentReportToArrayObj = (ArrayList<EduStudentReportTO>)request.getAttribute("resultObj");

System.out.println("eduStudentReportToArrayObj   "+eduStudentReportToArrayObj.size()+"  ArraySize        "+eduStudentReportToArrayObj.get(0).getSubject().length);

for(int xx=0;xx<eduStudentReportToArrayObj.size();xx++){
%>
<div  align=left >
<br>
		<table align=center border=1 >
		<td style="background-image: url(./images/backImage.png); background-repeat: repeat;"> 
		
		<table align=center border=0>

		<tr id="logoimage" >
		       
				<td align="left" width=10%><img src="./images/imagesCA4CN5KS.jpg" width="120" height="80" alt="What We Do" align="left" >
			</td>
				<td align="center" width=100%><font size=6><bean:message key="app.sch.prepareReportCard.schoolName"/></font></td>
		  </tr>
		</table>
		<table align=center border=1 width=100%>
		
		
		  <tr id="loimae" >
		       
				<td align="center" width=100%><font size=4>Mark Sheet</font></td>
				
		  </tr>
		  </table>
		
		<table align=center border=0>
		
		
		<tr id="departmentnametr" >
				<td align="left" width="2%"></td>
				<td align="left" width=15% rowspan="1" ><bean:message key="app.sch.prepareReportCard.rollnumber"/></td>
				<td id="student_id" align="left" width=25%><%=eduStudentReportToArrayObj.get(xx).getStudentid() %> </td>
				<td align="left" width=15%><bean:message key="app.sch.prepareReportCard.departmentname"/></td>
				<td id="departmentname" align="left" width=25%><%=eduStudentReportToArrayObj.get(xx).getDepartmentname() %></td>
		</tr>	

		
		<tr id="studentnametr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.studentname"/></td>
                <td id="studentname" align="left" width=10%><%=eduStudentReportToArrayObj.get(xx).getStudentname() %></td>
                <td align="left" width=10%><bean:message key="app.sch.prepareReportCard.class"/></td>		
				<td id="classname" align="left" width=10%><%=eduStudentReportToArrayObj.get(xx).getClassname() %></td>
        </tr>
		<tr id="sessiontr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.fathername"/></td>
                <td id="fathername" align="left" width=10%><%=eduStudentReportToArrayObj.get(xx).getFathername() %></td>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.session"/></td>
				<td id="session" align="left" width=10%><%=eduStudentReportToArrayObj.get(xx).getSession_year() %></td>
        </tr>
        
        <tr id="dateofbirthtr" >
				<td align="left" width="2%"></td>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.insttname"/></td>
                <td id="insttname" align="left" width=10%><bean:message key="app.sch.prepareReportCard.schoolName"/></td>
				<td align="left" width=10%><bean:message key="app.sch.prepareReportCard.dateofbirth"/></td>
				<td id="dateofbirth" align="left" width=10%><%=eduStudentReportToArrayObj.get(xx).getDateofbirth() %></td>
        </tr>
        		
		</table>
		
		 <div  align=left>
       
		 <br>
		 <table border=1 align=center id="marktr">
		 <thead>
		  <tr id="subjecttr" >
		        <td align="center" width=140><bean:message key="app.sch.prepareReportCard.subcode"/></td>
				<td align="center" width=200><bean:message key="app.sch.prepareReportCard.subject"/></td>
				<td align="center" width=200><bean:message key="app.sch.prepareReportCard.outof"/></td>
				<td align="center" width=200><bean:message key="app.sch.prepareReportCard.obtained"/></td>
				
		  </tr>
          </thead>		  
		  <% 
		  for(int ii=0;ii<eduStudentReportToArrayObj.get(xx).getSubject().length;ii++){
			  %>
			  <tbody>
			  <tr>
			    <td align="center" width=140><%=eduStudentReportToArrayObj.get(xx).getSubjectcode()[ii] %></td>
			  	<td align="center" width=200><%=eduStudentReportToArrayObj.get(xx).getSubject()[ii]%></td>
				<td align="center" width=200><%=eduStudentReportToArrayObj.get(xx).getMarkOutOf()[ii]%></td>
				<td align="center" width=200><%=eduStudentReportToArrayObj.get(xx).getMarkObtain()[ii]%></td>
				
			  </tr>
			  </tbody>
			 <%  
			}
		  
		  %>	
		  </table>
	
	   <div>
	   <br>

	   <table border=0 align=center>
	     <thead> 
	    <tr id="totalres" >
		       
		        <td align="center" width=140><bean:message key="app.sch.prepareReportCard.totalresult"/></td>
				<td align="center" width=200><bean:message key="app.sch.prepareReportCard.result"/></td>
				<td align="center" width=200><bean:message key="app.sch.prepareReportCard.percentage"/></td>
				<td align="center" width=200><bean:message key="app.sch.prepareReportCard.division"/></td>
				
		  </tr>	
		    </thead> 
		  <tr id="resoutof" >
		  
		   		<td align="center" width=140><%=eduStudentReportToArrayObj.get(xx).getTotalMarkObtained() %>/<%=eduStudentReportToArrayObj.get(xx).getTotalOutOf() %></td>
				<td align="center" width=200><%=eduStudentReportToArrayObj.get(xx).getResult() %></td>
				<td align="center" width=200><%=eduStudentReportToArrayObj.get(xx).getPercentage() %></td>
				<td align="center" width=200><%=eduStudentReportToArrayObj.get(xx).getDivision() %></td>
								
		  </tr>	
       </table>		   
	    <table border=0 align=center>
	   	  <br>
		  <br>
		   <tr id="signature" >
		       
		        <td align="center" width=140><bean:message key="app.sch.prepareReportCard.place"/></td>
				<td align="center" width=200><bean:message key="app.sch.prepareReportCard.date"/></td>
				<td align="center" width=200></td>
				<td align="center" width=200><bean:message key="app.sch.prepareReportCard.signature"/></td>
				
		  </tr>	
       </table>
	   </div>
	   </div>
  </td>
  </table>
	<br>
	<br>  
</div>

<%
	}
}
catch(Exception exp){
	exp.printStackTrace();
}
%>
<html:hidden property="percentage" styleId="percentage" styleClass="srchbox" style="border:1px solid #000000" />
<html:hidden property="division" styleId="division" styleClass="srchbox" style="border:1px solid #000000" />
<html:hidden property="result" styleId="result" styleClass="srchbox" style="border:1px solid #000000" />
<html:hidden property="session_year" styleId="session_year" styleClass="srchbox" style="border:1px solid #000000" />
<html:hidden property="subject_count" styleId="subject_count" styleClass="srchbox" style="border:1px solid #000000" />
<html:hidden property="isrecordExist" styleId="isrecordExistval" styleClass="srchbox" style="border:1px solid #000000" />
</html:form>      
<script type='text/javascript'>
function printReport(){
window.print();
}
printReport();
</script>