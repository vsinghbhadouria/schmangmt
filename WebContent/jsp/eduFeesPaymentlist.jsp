<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page import="java.util.*"%>
<%Random generator = new Random();
int r = generator.nextInt();
%>
<%@page import="com.sch.delegates.EduStudentManager"%>
<!--begin custom header content for this example-->
<style type="text/css">
/* custom styles for this example */
#yui-history-iframe {
  position:absolute;
  top:0; left:0;
  width:1px; height:1px; /* avoid scrollbars */
  visibility:hidden;
}
</style>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentManager.js"></script>
<!--end custom header content for this example-->

<!--BEGIN SOURCE CODE FOR EXAMPLE =============================== -->
<table align="center" width="97%" cellpadding="0" cellspacing="0" >
<tr class="mainhead" height="25px"><td>
		<b>&nbsp;&nbsp;<bean:message key="app.sch.edustudentlist.listformtitle"/></b>
		</td>
		<td align="right" valign="middle" class="mainhead">
							<input type="text" onkeypress="if(event.keyCode==13) searchByattribute()" name="searchbyname" id="searchbyname" size="20" maxlength="25"  class="srchbox" />&nbsp;&nbsp;
					<a href="#" onclick="searchByattribute()">
						<img src="./images/searcharrow.jpg" align="top">
					</a>&nbsp;&nbsp;
	    </td>
		</tr>
	  
<tr align="left" style="background-image: url('./images/ltbrgrey1.gif')">
<td colspan="2">    
					<Label for ="name"><b>&nbsp;&nbsp;Session &nbsp;&nbsp; </b></Label>
			        <select name ="session" id= "session" onchange="searchByattribute();">
					</select>
					<Label for ="name"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Class &nbsp;&nbsp; </b></Label>
			        <select name ="class" id= "class" onchange="searchByattribute();">
					</select>
</td>
	</tr>	
<tr><td >
&nbsp;</td></tr>
<tr><td colspan="2">
	<div id="bhmintegration"></div>
	<div id="dt-pag-nav" align ="center"></div>
</td></tr></table>

<script type="text/javascript">
function check(url){
//alert("check"+url);
window.open(url,"mywindow","location=1,status=1,scrollbars=1,width=800,height=600");

}

var searchBysessionkey = "";
var searchByclasskey = "";
  function loadSession()
    {
    	var sessionoption = document.getElementById("session");
    	EduStudentManager.getSession(function(session){
				for(var xx=0;xx<session.length;xx++)
				{
					var catidvalue = session[xx].split(",");
					sessionoption[xx]= new Option(catidvalue[1],catidvalue[0]); 						
				}
				 
        	});
       
    }
	
	function loadClass()
    {
    	var classoption = document.getElementById("class");
    	EduStudentManager.getClasses(function(classinfo){
				for(var xx=0;xx<classinfo.length;xx++)
				{
					var catidvalue = classinfo[xx].split(",");
					classoption[xx]= new Option(catidvalue[1],catidvalue[0]); 						
				}
				 
        	});
       
    }
//Table cell formating for Buttons
 function searchByattribute() {
	 
	 searchBysessionkey = trim(document.getElementById("session").value);
	 searchByclasskey = trim(document.getElementById("class").value);
    // Column definitions
   YAHOO.example.CustomFormatting = new function() {
    	this.myCustomFormatterEditFeePayment = function(elCell, oRecord, oColumn,
				oData) {
			
			if(true){
				//window. open('/schmangmt/FeesPaymentAction.do?operation=searchStudent',"mywindow","location=1,status=1,scrollbars=1,width=700,height=700");
				    (elCell.innerHTML =('<a href="/schmangmt/FeesPaymentAction.do?operation=printReceipt&User_id='
					+ oRecord.getData("user_id")+'&Session_id='
					+ oRecord.getData("session_id")+'&receipt_no='
					+ oRecord.getData("receipt_no")
					+ '" onMouseOut=over("EditFeePayment'
					+ oRecord.getData("user_id")
					+ '","receipt_i.jpg") onMouseOver=over("EditFeePayment'
					+ oRecord.getData("user_id")
					+ '","receipt_i.jpg") onclick="check(this.href);return false;"><img border=0 src="./images/receipt_i.jpg" height="35" width="40" id="EditFeePayment'
					+ oRecord.getData("user_id")
					+ '" title="Click Receipt Icon to print the Fee Receipt"></a>'))	;
			}else{
				elCell.innerHTML = '&nbsp;';
			}
		};
		
		YAHOO.widget.DataTable.Formatter.EditFeePayment = this.myCustomFormatterEditFeePayment;
	
    };
    
    var searchKey = trim(document.getElementById("searchbyname").value);
    var myColumnDefs = [ // sortable:true enables sorting
                         {key:"user_id", label:"Admission No.", sortable:true,width:100},
                         {key:"first_name", label:"Name", sortable:true,width:200},
                         {key:"receipt_no", label:"Receipt No.", sortable:true,width:100},
                         {key:"Email", label:"Email", sortable:true,width:200},
                         {key:"Class", label:"Class", sortable:true,width:100},
                         {key:"Section", label:"Section", sortable:false,width:50},
                         {key:"Session", label:"Session", sortable:false,width:100},
                         {key:"Phone", label:"Phone", sortable:false,width:100},
                         {key:"EditFeePayment", label:"Receipt",formatter: "EditFeePayment",sortable :false,resizeable :false}
                        
                     ];

    // Custom parser
    var stringToDate = function(sData) {
        var array = sData.split("-");
        return new Date(array[1] + " " + array[0] + ", " + array[2]);
    };
    
    // DataSource instance
    var myDataSource = new YAHOO.util.DataSource("/schmangmt/jsp/eduFeesPayment_json.jsp?ran=<%=r%>&searchBysessionkey="+searchBysessionkey+"&searchByclasskey="+searchByclasskey+"&searchKey="+searchKey+"&");
    myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSON;
    myDataSource.responseSchema = {
        resultsList: "records",
        fields: ["first_name","Class","Phone","user_id","Email","Section","Session","session_id","receipt_no"],
        
        metaFields: {
            totalRecords: "totalRecords" // Access to value in the server response
        }
    };
   
    // DataTable configuration
    var myConfigs = {
		paginator: new YAHOO.widget.Paginator({rowsPerPage:10, containers : ["dt-pag-nav"], template : "{PageLinks} {RowsPerPageDropdown}", rowsPerPageOptions : [10,25,50,100] }), // Enables pagination 
    	initialRequest: "sort=first_name&dir=asc&startIndex=0&results=10", // Initial request for first page of data
        dynamicData: true, // Enables dynamic server-driven data
        sortedBy : {key:"first_name", dir:YAHOO.widget.DataTable.CLASS_ASC} // Sets UI initial sort arrow
    };

     
    
    // DataTable instance
    var myDataTable = new YAHOO.widget.DataTable("bhmintegration", myColumnDefs, myDataSource, myConfigs);
    // Update totalRecords on the fly with value from server
    myDataTable.handleDataReturnPayload = function(oRequest, oResponse, oPayload) {
        oPayload.totalRecords = oResponse.meta.totalRecords;
        return oPayload;
    }
    
    return {
        ds: myDataSource,
        dt: myDataTable
    };
        
}
 searchByattribute();
 loadSession();
 loadClass();
</script>

<!--END SOURCE CODE FOR EXAMPLE =============================== -->









