<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<script type='text/javascript' src='/dwr/interface/ShopMasterManager.js'></script>
<script type='text/javascript' src='dwr/engine.js'></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script language="JavaScript">
function getAreaDetails()
   {        
	        //alert("zipcode"+document.getElementById("zipcode").value);
	        
	        var zipcode = document.getElementById("zipcode").value;
	        if(zipcode.length==6)
	        {
	        document.getElementById('message').style.display = 'none';
	        ShopMasterManager.getAreaDetails(zipcode,function(AreaDetails){
			var c =AreaDetails.split(",");
			if(c[0].toString()!= 'null')
			{
				
				document.getElementById('location1').style.display= 'inline';
		        document.getElementById('location2').style.display= 'inline';
		        document.getElementById('location3').style.display= 'inline';
		        document.getElementById('location4').style.display= 'inline';
		        document.getElementById('location5').style.display= 'inline';
		        over("showRight","checkbullet.gif")
		        document.getElementById('showRight').style.visibility= 'visible';
		        document.getElementById('showError').style.visibility= 'hidden';
		        document.getElementById('message').style.display = 'none';
		        
		    }
			else
			{
				//alert("Zipcode not Exist...");
				document.getElementById('location1').style.display= 'none';
		        document.getElementById('location2').style.display= 'none';
		        document.getElementById('location3').style.display= 'none';
		        document.getElementById('location4').style.display= 'none';
		        document.getElementById('location5').style.display= 'none';
		        over("showError","error_bang.gif")
		        document.getElementById('showError').style.visibility= 'visible';
		        document.getElementById('showRight').style.visibility= 'hidden';
		        document.getElementById('message').innerHTML ='Zipcode not Exist';
		        document.getElementById('message').style.display = 'inline';
			}
			
			document.getElementById("area_Id").value=c[0];
			document.getElementById("statename").value=c[3];
			document.getElementById("city").innerHTML=c[1];
			document.getElementById("District").innerHTML=c[2];
			document.getElementById("State").innerHTML=c[3];
			document.getElementById("Zone").innerHTML=c[4];
			document.getElementById("Country").innerHTML=c[5];
			//alert(document.getElementById("area_Id").value);
			
		});
	       }
	        else
	        {
	        	document.getElementById('location1').style.display= 'none';
		        document.getElementById('location2').style.display= 'none';
		        document.getElementById('location3').style.display= 'none';
		        document.getElementById('location4').style.display= 'none';
		        document.getElementById('location5').style.display= 'none';
		        over("showError","error_bang.gif")
		        document.getElementById('showError').style.visibility= 'visible';
		        document.getElementById('showRight').style.visibility= 'hidden';
		        document.getElementById('message').innerHTML ='Zipcode Should be 6 digit';
		        document.getElementById('message').style.display = 'inline';
		 
		      
		        
		    }
	    }
function isValid(parm,val) {
	 if (parm == "") return true;
	 for (i=0; i<parm.length; i++) {
	 if (val.indexOf(parm.charAt(i),0) == -1) return false;
	 }
 	return true;
 	}
function checkValidation()
  	 {   
 	   var flag = true;
 	   var issuetitleflg = true;
 	   var addressflg = true;
 	   var addressflg1 = true;
 	   var contectflg = true;
 	   var phoneflg = true;
 	   var ansdiscflg =true;
	   var numb = '0123456789';
	   var lwr = 'abcdefghijklmnopqrstuvwxyz ';
	   var upr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ';
	   var address= '.()-';
	
	   var issuetitle = document.getElementById('issuetitle').value;
	   var issuedisc = document.getElementById('issuedisc').value;
	   var category = document.getElementById('category').value;
	   var status = document.getElementById('status').value;
	   var priority = document.getElementById('priority').value;
	   var ansdisc = document.getElementById('ansdisc').value;
	   var ansdisccheck = document.getElementById('ansbox');
	   ansdisccheck = ansdisccheck.getAttribute("style");
	   //Shop Name validation
	   if(issuetitle!='')
	    {
		  document.getElementById('showNameError').style.display='none';
	      document.getElementById('issuetitleMsg').style.display = 'none';
	      issuetitleflg=true;
	  }
	  else
	  { 
		over("showNameError","error_bang.gif");
        document.getElementById('showNameError').style.display='inline';
        document.getElementById('issuetitleMsg').innerHTML ='Issue Title Should Not be Blank';
        document.getElementById('issuetitleMsg').style.display = 'inline';
        issuetitleflg = false;
	  }
	 //Street Address validation
	   if(issuedisc!='')
	    {
		    
		  document.getElementById('showStreetAddError').style.display='none';
	      document.getElementById('shopstreetAddMsg').style.display = 'none';
	      addressflg = true;
		      
	  }
	  else
	  { 
	   over("showStreetAddError","error_bang.gif");
       document.getElementById('showStreetAddError').style.display='inline';
       document.getElementById('shopstreetAddMsg').innerHTML ='Issue Discription Should Not be Blank';
       document.getElementById('shopstreetAddMsg').style.display = 'inline';
       addressflg = false;
	  }	

	  if(category!='Select Category')
	  {
		   document.getElementById('showStreetAdd2Error').style.display='none';
		   document.getElementById('shopstreetAdd2Msg').style.display = 'none'; 
		   addressflg1 = true;
	  }
	  else
	  { 
	   over("showStreetAdd2Error","error_bang.gif");
      document.getElementById('showStreetAdd2Error').style.display='inline';
      document.getElementById('shopstreetAdd2Msg').innerHTML ='Select the Category';
      document.getElementById('shopstreetAdd2Msg').style.display = 'inline';
      addressflg1 = false;
	  }		  
	 //Contect Person Name validation
	 
	 if(status!='Select Status')
	    {
		  document.getElementById('showContectNameError').style.display='none';
	      document.getElementById('contectNameMsg').style.display = 'none';
	      	  contectflg=true;
		   
	  }
	  else
	  { 
		over("showContectNameError","error_bang.gif");
        document.getElementById('showContectNameError').style.display='inline';
        document.getElementById('contectNameMsg').innerHTML ='Select Status';
        document.getElementById('contectNameMsg').style.display = 'inline';
        contectflg = false;
	  }    
//Phone No. validation
      if(priority!='Select Priority')
	    {
		  document.getElementById('showPhoneNoError').style.display='none';
	      document.getElementById('phoneMsg').style.display = 'none';
	      phoneflg=true;
		   
	  }
	  else
	  { 
		over("showPhoneNoError","error_bang.gif");
        document.getElementById('showPhoneNoError').style.display='inline';
        document.getElementById('phoneMsg').innerHTML ='Select Priority';
        document.getElementById('phoneMsg').style.display = 'inline';
        phoneflg = false;
	  }
      if(ansdisccheck!='display: none;')
      {
       if(ansdisc!='')
	    {
		    
		  document.getElementById('showStreetAddError').style.display='none';
	      document.getElementById('shopstreetAddMsg').style.display = 'none';
	      ansdiscflg = true;
		      
	  }
	  else
	  { 

		over("showansdiscError","error_bang.gif");
     	document.getElementById('showansdiscError').style.display='inline';
     	document.getElementById('ansdiscMsg').innerHTML ='Comment Should Not be Blank';
     	document.getElementById('ansdiscMsg').style.display = 'inline';
     	ansdiscflg = false;
	  }	
	      
      }
           
      if(issuetitleflg && addressflg && contectflg && phoneflg && addressflg1 && ansdiscflg)
      {flag=true;}
      else{flag=false;}
      return flag; 
   }
   function resets()
    {
      document.shopMasterFormBean.action="forwardtoshopmasteradd.do?btnReset=Reset";
      document.shopMasterFormBean.submit();
      return true;
     }

   function cancel()
   {
	 document.ShopMasterForm.action=action="/IssueTrackingAction.do?operation=search";
     document.shopMasterForm.submit();
     return true;
    }
   function checkResolved()
   {
	    var selectedIndex = document.getElementById("status").selectedIndex;
		var status = document.getElementById("status");
		status = status[selectedIndex].value;
		 
		if(status=='Resolved')
		{
			document.getElementById('ansbox').style.display='inline';
		}
		else if(status=='Unresolved')
		{
			document.getElementById('ansbox').style.display='inline';
		}
		else if(status=='Pending')
		{
			document.getElementById('ansbox').style.display='inline';
		}
		else 
		{
			document.getElementById('ansbox').style.display='none';
		}
   }
</script>

    <html:errors />
      <html:form  action="/IssueTrackingAction.do?operation=reply" onsubmit="return checkValidation();" >
        <%
                           
        
                        /* String strshopcode = request.getParameter("shopcode");
                         String strissuetitle = request.getParameter("issuetitle");
                         String straddress1 = request.getParameter("streetAddress");
                         String strphone = request.getParameter("strphone");
                         String strfax = request.getParameter("strfax");
                       
                       
                         String strcontactpersonname =  request.getParameter("shopcontactpersonname");
                         String strzipcode = request.getParameter("zipcode");
                         if(request.getParameter("btnReset")!=null)
                         {
                         	System.out.println("with in if condition");
                         	strshopcode = "";
                         	strissuetitle = "";
                         	straddress1 ="";
                            strcontactpersonname ="";
                         	
                         }*/         
     %>
       <table align="center" width="97%" cellpadding="0" cellspacing="0">
	   <tr><td class="mainhead" >&nbsp;&nbsp;&nbsp;&nbsp;View&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   </td></tr>
       </table>
      <table  style="background-image: url('./images/ltbrgrey1.gif')" align="center" cellspacing="2" cellpadding="5" width=97% border="0">
       <!-- <tr>
          <td align=left width=1%>&nbsp;&nbsp;&nbsp;Shop Code</td>
          <td><html:text property="shopcode" size="30" styleClass="srchbox" style="border:1px solid #000000" value =""/></td>
        </tr>-->
        <tr>
          <td align=left width=1%><img src="./images/blank.png" alt="" id="showNameError"/></td>
          <td align=left width=20%>Issue Title</td>
          <td>
			<label for="issuetitle" id="issuetitle"></label>
			&nbsp;&nbsp;&nbsp;<div  style ='display:none; color: red;' id="issuetitleMsg"></div></td>
        </tr>
        <tr>
          <td align=left width=1%><img src="./images/blank.png" alt="" id="showStreetAddError"/></td>
          <td align=left width=15%>Issue Desc</td>
          <td>
          <label for="issuedisc" id="issuedisc"></label>
		  &nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="shopstreetAddMsg"></td>
        </tr>
        <tr>
          <td align=left width=1%><img src="./images/blank.png" alt="" id="showStreetAdd2Error"/></td>
          <td align=left width=15%>Category</td>
          <td>
          <label for="category" id="category"></label>
		   &nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="shopstreetAdd2Msg"></td>
		  <!--<html:text property="category" size="30" styleClass="srchbox" styleId="category" style="border:1px solid #000000" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="shopstreetAdd2Msg"></td>
        --></tr>
		  <tr>
          <td align=left width=1%><img src="./images/blank.png" alt="" id="showPhoneNoError"/></td>
          <td align=left width=15% >Priority</td>
          <td>
        	<label for="priority" id="priority"></label>
			&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="phoneMsg"></td>
         </tr>
         <tr>
           <td align=left width=1%><img src="./images/blank.png" alt="" id="showContectNameError"/></td>
           <td align=left width=15% >Status</td>
		   <td>
			<label for="status" id="status"></label>
			&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="contectNameMsg"/></td>	
         </tr>
		<tr>
			<td align=left width=1%><img src="./images/blank.png" alt="" id="showansdiscError"/></td>
			<td align=left width=15% >Issue generation Date:</td>
			<td >
			<label for ="issuesubmit" id="issuesubmit"></label>
			&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="ansdiscMsg"></td>
			</td>
		 </tr>
		<tr>
			<td align=left width=1%><img src="./images/blank.png" alt="" id="showansdiscError"/></td>
			<td align=left width=15% >Issue reply Date:</td>
			<td >
			<label for ="issuereply" id="issuereply"></label>
			&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="ansdiscMsg"></td>
			</td>
		 </tr>
          <tr>
			<td align=left width=1%><img src="./images/blank.png" alt="" id="showansdiscError"/></td>
			<td align=left width=15% >Comment:</td>
			<td >
			<label for ="ansdisc" id="ansdisc"></label>
			&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="ansdiscMsg"></td>
			</td>
		 </tr>
        
          <tr>
           <td></td>
           <td colspan="2" align="center">
            <input type="button" value ="Back" onclick ="javascript:history.back();" />
           </td>
        </tr>
      </table>
    <input type="hidden" name ="area_Id" id="area_Id"/>
    <input type="hidden" name ="statename" id="statename"/> 
    </html:form> 
<script language="JavaScript">

function loadEditData()
{
	var category = '<%=request.getAttribute("category")%>';
	var allcategory = document.getElementById('category').innerHTML = category;
	var status = '<%=request.getAttribute("status")%>';
	document.getElementById('status').innerHTML = status;
	var priority = '<%=request.getAttribute("priority")%>';
	document.getElementById('priority').innerHTML = priority;
	var issuetitle = '<%=request.getAttribute("issuetitle")%>';
	document.getElementById('issuetitle').innerHTML = issuetitle;
	var issuedisc = '<%=request.getAttribute("issuedisc")%>';
	document.getElementById('issuedisc').innerHTML = issuedisc;
	var ansdisc = '<%=request.getAttribute("ansdisc")%>';
	document.getElementById('ansdisc').innerHTML = ansdisc;
	var issuesubmit = '<%=request.getAttribute("issuesubmit")%>';
	document.getElementById('issuesubmit').innerHTML = issuesubmit;
	var issuereply = '<%=request.getAttribute("issuereply")%>';
	document.getElementById('issuereply').innerHTML = issuereply;
}

loadEditData();
</script>




