<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<script type='text/javascript' src='/dwr/interface/ShopMasterManager.js'></script>
<script type='text/javascript' src='dwr/engine.js'></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script language="JavaScript">
function checkValidation()
  	 {   
 	   var flag = true;
 	   var statusflg = true;
 	   var ansdiscflg =true;
	   var numb = '0123456789';
	   var lwr = 'abcdefghijklmnopqrstuvwxyz ';
	   var upr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ';
	   var address= '.()-';
	   var status = document.getElementById('status').value;
	   var ansdisc = document.getElementById('ansdisc').value;
	   //Shop Name validation
	   if(status!='Select Status')
	    {
		   document.getElementById('showstatusError').style.display='none';
		   document.getElementById('statusMsg').style.display = 'none';
     	   statusflg=true;
	  	}
	  else
	  { 
		  over("showstatusError","error_bang.gif");
		  document.getElementById('showstatusError').style.display='inline';
		  document.getElementById('statusMsg').innerHTML ='Select the Status';
	      document.getElementById('statusMsg').style.display = 'inline';
	      statusflg = false;
	   
	  }
	
       if(ansdisc!='')
	    {
		  document.getElementById('showansdiscError').style.display='none';
	      document.getElementById('ansdiscMsg').style.display = 'none';
	      ansdiscflg = true;
    	  }
	  else
	  { 

		over("showansdiscError","error_bang.gif");
     	document.getElementById('showansdiscError').style.display='inline';
     	document.getElementById('ansdiscMsg').innerHTML ='Comment Should Not be Blank';
     	document.getElementById('ansdiscMsg').style.display = 'inline';
     	ansdiscflg = false;
	  }
      if(statusflg && ansdiscflg)
      {flag=true;} 
      else{flag=false;}  
      return flag; 
  	}
 </script>

    <html:errors />
      <html:form  action="/IssueTrackingAction.do?operation=editReply" onsubmit="return checkValidation();" >
	   <table  style="border: 1px solid #000000" align="center" cellspacing="2" cellpadding="5" width=97% >
       	<tr><td class="mainhead" colspan="3">
		<div class="barleft-element">
		<div class="barright-elementl"><b>Edit Your Reply</b>&nbsp;&nbsp;&nbsp;</div>
        </div>
		</td></tr>
         <tr>
           <td align=left><img src="./images/blank.png" alt="" id="showstatusError"/></td>
           <td align=left >Status:</td>
		   <td align=left width="90%"><html:select property="status" styleId ="status" onchange="checkResolved();" size="1" style="border:1px solid #000000" >

            <option value="Select Status">Select Status</option>
			<option value="Pending">Pending</option>
			<option value="Resolved">Resolved</option>
			<option value="Unresolved">Unresolved</option>
			</html:select>		
			&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="statusMsg"/></td>	
         </tr>
          <tr id="ansbox" >

			<td align=left><img src="./images/blank.png" alt="" id="showansdiscError"/></td>
			<td align=left width="">Comments:</td>

			<td >
			<html:textarea property="ansdisc" styleId="ansdisc" style="border:1px solid #000000" />
			&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="ansdiscMsg"></td>
			<html:hidden property="replyid"  styleId="replyid" />
			<html:hidden property="issueid"  styleId="issueid"/>
			</td>
		 </tr>
          <tr>
          
		   <td colspan="3" align="center">
            <html:submit  />
            <input type="button" value ="Cancel" onclick ="javascript:history.back();" />
          </td>
        </tr>
      </table>
    <input type="hidden" name ="area_Id" id="area_Id"/>
    <input type="hidden" name ="statename" id="statename"/> 
	<br>
    </html:form> 
<script language="JavaScript">

function loadEditData()
{
	var status = '<%=request.getAttribute("status")%>';
	var allstatus = document.getElementById("status");
 	for(var xx=0;xx<allstatus.length;xx++ )
 	{
 		if(status == allstatus.options[xx].value)
 		{
 			allstatus.selectedIndex=xx;
 		}
 	}
	
}
loadEditData();
</script>




