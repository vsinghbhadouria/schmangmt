<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type='text/javascript'
	src="/schmangmt/dwr/interface/EduStudentManager.js"></script>
<script type='text/javascript'
	src="/schmangmt/dwr/interface/EduStaffManager.js"></script>
<script type='text/javascript' src='./scripts/schmanagmt.js'></script>
<script language="JavaScript">
	//display all countries in dropdown box // display all upper levels in combo box

	function assignSubjects() {
		var assoList = document.getElementById('allList');
		var len = assoList.length;

		var selAssoList = document.getElementById('selectedList');
		for ( var j = len - 1; j >= 0; j--) {
			if (assoList[j].selected && assoList[j].selected != null) {
				var selText = assoList.options[j].text;
				var selValue = assoList.options[j].value;
				if (checkSubjects(selText, selValue)) {
					selAssoList[selAssoList.length] = new Option(selText,
							selValue, true);
				}
				assoList.remove(j);
			}
		}
	}

	function checkSubjects(seltext, selValue) {
		flag = true;
		var selAssoList = document.getElementById('selectedList');
		var lenAssoList = selAssoList.length;
		if (lenAssoList != 0) {
			for ( var xx = 0; xx < lenAssoList; xx++) {
				if (selValue == selAssoList[xx].value) {
					flag = false;
					break;
				}
			}
		}
		return flag;
	}

	function moveSubjects() {
		var assoList = document.getElementById('allList');
		var selAssoList = document.getElementById('selectedList');
		var len = selAssoList.length;
		for ( var j = len - 1; j >= 0; j--) {
			if (selAssoList[j].selected && selAssoList[j].selected != null) {
				var selText = selAssoList.options[j].text;
				var selValue = selAssoList.options[j].value;
				assoList[assoList.length] = new Option(selText, selValue, true);
				selAssoList.remove(j);
			}
		}
	}
	function selectOption(value) {
	document.getElementById('classSelection').style.display='none';
		var allList = document.getElementById('allList');
		var class_id='';
		var selAssoList = document.getElementById('selectedList');
		removeOptions(allList);
		removeOptions(selAssoList);
		if (value == "teacher") {
			document.getElementById('legendLevel').innerHTML = "<b>"
					+ 'Teacher' + "</b>";
			EduStaffManager.getTeacherList('active', function(teacherList) {

				for ( var i = 0; i < teacherList.length; i++) {
					var userOptions = teacherList[i];
					userOptions = userOptions.split(',');
					allList[i] = new Option(userOptions[1], userOptions[1]);
				}

			});
		} else if (value == "student") {
			document.getElementById('legendLevel').innerHTML = "<b>"
					+ 'Student' + "</b>";
			document.getElementById('classSelection').style.display='inline';
			class_id = document.getElementById("class").value;
			EduStudentManager.getStudentList(class_id,'active', function(studentList) {
				for ( var i = 0; i < studentList.length; i++) {
					var userOptions = studentList[i];
					userOptions = userOptions.split(',');
					allList[i] = new Option(userOptions[1], userOptions[1]);
				}

			});
		}

		else if (value == "parent") {
			document.getElementById('legendLevel').innerHTML = "<b>" + 'Parent'
					+ "</b>";
			EduStudentManager.getParentList(function(parentList) {

				for ( var i = 0; i < parentList.length; i++) {
					var userOptions = parentList[i];
					userOptions = userOptions.split(',');
					allList[i] = new Option(userOptions[1], userOptions[1]);
				}

			});
		}

		else if (value == "astudent") {
			document.getElementById('legendLevel').innerHTML = "<b>"
					+ 'Alumni Student' + "</b>";
			EduStudentManager.getStudentList(class_id,'alumni', function(studentList) {
				for ( var i = 0; i < studentList.length; i++) {
					var userOptions = studentList[i];
					userOptions = userOptions.split(',');
					allList[i] = new Option(userOptions[1], userOptions[1]);
				}

			});
		}

		else if (value == "ateacher") {
			document.getElementById('legendLevel').innerHTML = "<b>"
					+ 'Alumni Teacher' + "</b>";
			EduStaffManager.getTeacherList('alumni', function(teacherList) {
				for ( var i = 0; i < teacherList.length; i++) {
					var userOptions = teacherList[i];
					userOptions = userOptions.split(',');
					allList[i] = new Option(userOptions[1], userOptions[1]);
				}
			});
		}
	}

	function removeOptions(allclasses) {
		var i;
		for (i = allclasses.options.length - 1; i >= 0; i--) {
			allclasses.remove(i);
		}
	}
	
	function formsubmit()
	{

		var selSub = document.getElementById("selectedList");
		var slen = selSub.length;
		for(var j=slen-1; j>=0; j--)
		{
			selSub.options[j].selected=true;
		}
	}	
	
	function checkValidation(){
		formsubmit();
		return true;
	}
	
	function loadClass()
    {
    	var classoption = document.getElementById("class");
    	EduStudentManager.getClasses(function(classinfo){
				for(var xx=0;xx<classinfo.length;xx++)
				{
					var catidvalue = classinfo[xx].split(",");
					classoption[xx]= new Option(catidvalue[1],catidvalue[0]); 						
				}
				 
        	});
       
    }
	
</script>

</head>
<body>
<html:form  action="/EduStaffAction.do?operation=emailSetup" method ="post">
	<div style="align:left;width:50em;">
		<fieldset id="optionsContainer" width="50">
			<legend>
				<b>Meeting Invitation</b>
			</legend>

			<table style="align:center;width:50em;">

				<tr id="meetingOption">
					<td align="left" width="15%"><INPUT TYPE="radio"
						name="meeting" styleClass="srchbox" Id="teacher"
						style="border: 1px solid #000000" value="teacher"
						onchange="selectOption(this.value);" checked /><b>Teachers</b></td>
					<td align="left" width="15%"><INPUT TYPE="radio"
						name="meeting" styleClass="srchbox" Id="student"
						style="border: 1px solid #000000" value="student"
						onchange="selectOption(this.value);" /><b>Students</b></td>
					<td align="left" width="15%"><INPUT TYPE="radio"
						name="meeting" styleClass="srchbox" Id="parent"
						style="border: 1px solid #000000" value="parent"
						onchange="selectOption(this.value);" /><b>Parents</b></td>
					<td align="left" width="20%"><INPUT TYPE="radio"
						name="meeting" styleClass="srchbox" Id="alumnistudent"
						style="border: 1px solid #000000" value="astudent"
						onchange="selectOption(this.value);" /><b>Alumni Students</b></td>
					<td align="left" width="20%"><INPUT TYPE="radio"
						name="meeting" styleClass="srchbox" Id="alumniteacher"
						style="border: 1px solid #000000" value="ateacher"
						onchange="selectOption(this.value);" /><b>Alumni Teachers</b></td>
				</tr>

			</table>
			<div id='selectiontab' style="display: inline" width=50>
				<fieldset id="optionsContainer" width="50">
					<legend id="legendLevel"></legend>
					<div id='classSelection' style="display: none">
					<table style="align:left;width:50em;" border=0>
						<tr align="left" style="background-image: url('./images/ltbrgrey1.gif')">
						<td width=15%></td>
						<td colspan="2" width=100%>    
						<Label for ="name" style="margin-left:1em;"><b>Class &nbsp;&nbsp;</b></Label>
						<select name ="class" id= "class" onchange="selectOption('student');">
						</select>
						</td>
						</tr>	
						
					</table>
					</div>
					<table align=center border=0>
						<tr>
							<td class="tdLabel_style" align=center><b><bean:message
									key="app.sch.addeduinstituestaff.allemails"/></b><br> <select
								name="allList" Id="allList" size=10 multiple="multiple"
								style="width: 400px; background-color: #EEEEEE; border: 1px solid #000000; font-size: 11px; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight: normal;"></select>
							</td>
							<td><a href="javascript:assignSubjects();"> <img
									src="./images/moveright.gif" border="0">
							</a> <br> <a href="javascript:moveSubjects();"> <img
									src="./images/moveleft.gif" border="0">
							</a></td>
							<td class="tdLabel_style" align=center><b><bean:message
									key="app.sch.addeduinstituestaff.assignedemails" /></b><br>
								<select name="selectedList" Id="selectedList" size=10
								multiple="multiple"
								style="width: 400px; background-color: #FFFFFF; border: 1px solid #000000; font-size: 11px; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight: normal;"></select>

							</td>
							
						<td>	
						<jsp:include page="datetime.jsp"/>
						</td>
						</tr>

					</table>
				</fieldset>
			</div>
			<table style="" align="center" border=0 cellspacing=2 cellpadding=3 width=100%  > 
				<tr> 
					<td colspan="2" align="center" >
					<input type="submit" value ="Submit" onclick="return checkValidation();"/>
					<input type="button" value ="Cancel"  onclick="javascript:history.back();" />
				</td>
			   </tr>
			</table>
		</fieldset>
		
	</div>
</html:form>
</body>
<script language="JavaScript">
	function defaultSelection() {
		document.getElementById('teacher').checked = true;
		selectOption('teacher');
	}
	defaultSelection();
	loadClass();
</script>
</html>