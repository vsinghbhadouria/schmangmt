<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>    
<%@ page language="java" %>
<%@page import="com.sch.to.LibraryManagementTO"%>
<%@page import="java.util.ArrayList"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="shortcut icon" type="image/ico" href="http://www.sprymedia.co.uk/media/images/favicon.ico">
		
		<title>TableTools example</title>
		<style type="text/css" title="currentStyle">
			@import "./jquery-ui-1.10.3/themes/base/demo_table_jui.css";
			@import "./jquery-ui-1.10.3/themes/base/jquery-ui-1.8.4.custom.css";
			@import "./jquery-ui-1.10.3/themes/base/TableTools_JUI.css";
		</style>
		<script src="./jquery-ui-1.10.3/ui/jquery-1.10.2.js"></script>
		<script src="./jquery-ui-1.10.3/ui/jquery.dataTables.js"></script>
		<script src="./jquery-ui-1.10.3/ui/ZeroClipboard.js"></script>
		<script src="./jquery-ui-1.10.3/ui/TableTools.js"></script> 
		<script type="text/javascript" >
			$(document).ready( function () {
				$('#example').dataTable( {
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",	
					"bProcessing": true,
				//"bServerSide": true,
					//"sAjaxSource": "/schmangmt/jsp/bookTable.jsp",
					
					
					
					
				} );
			} );
		</script>
	</head>
	<body id="dt_example">
<div id="container">
<div id="demo">
<div style="border: 1px solid;width:100%;text-align: left;"> 
 <table cellpadding="0" cellspacing="0" border="0" class="display">
	<tr><td class="mainhead" colspan="3"><bean:message key="app.sch.librarybooklist.addformtitle"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   </td></tr>
</table>	   
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
	<thead>
		<tr>
			<th align="center">Book Name</th> 
			<th align="center">Book Type</th>
			<th align="center">Rack Name</th>
			<th align="center">Total Books Count</th>
			<th align="center">Available Books Count</th>
		</tr>
	</thead>
	<tbody>
	<% 
	ArrayList<LibraryManagementTO> libraryManagementTO = (ArrayList<LibraryManagementTO>)request.getAttribute("allbooks");
	
	for(int xx=0;xx<libraryManagementTO.size();xx++){
		%>
		 <tr>
		 	<td class="center">
		 	<%=libraryManagementTO.get(xx).getBookname()%>
		    </td>
		    <td class="center">
		    <%=libraryManagementTO.get(xx).getBooktypename()%>
		    </td>
		    <td class="center">
		    <%=libraryManagementTO.get(xx).getRackname()%>
		    </td class="center">
		    <td class="center">
		    <%=libraryManagementTO.get(xx).getNumberofcopy()%>
		    </td>
		      <td class="center">
		    <%=libraryManagementTO.get(xx).getAvailablebookcount()%>
		    </td>
		 </tr>
		<%	
	}
	%>
</tbody>	
	<tfoot>
		<tr>
			<th align="center">Book Name</th> 
			<th align="center">Book Type</th>
			<th align="center">Rack Name</th>
			<th align="center">Total Books Count</th>
			<th align="center">Available Books Count</th>
		</tr>
	</tfoot>
</tbody>
</table>
</div>
</div>
</div>
	</body>
</html>