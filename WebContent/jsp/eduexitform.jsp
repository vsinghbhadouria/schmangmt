<%@ page language="java" %>
<%@page import="com.sch.to.EduStaffTO"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
  <head>
 
  <title>Leaving Certificate</title>  
  </head>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduExitManager.js"></script>
 <script language="JavaScript">

	//form validation
 function checkValidation()
  	 {
  	    return true;
	    }

</script>
  <body>
    
    <html:errors />
      <html:form  action="/EduExitAction.do?operation=save" method ="post">
        <%
                         String struserid      = request.getParameter("userid");          
        				 String Msg = (String)request.getAttribute("Msg");
        				 System.out.println("Msg ===================== "+Msg);
     %>
<div style="border: 1px solid;width:97%"  >
 <table style="" align="center" cellspacing="2" cellpadding="5" width=100% >
	<tr height="20px"><td class="mainhead"   colspan="3">&nbsp;Leaving Certificate
	   </td></tr>
	   <% 
	    if(Msg!=null){
	   %>
	     <tr>
          <td align=left width="1%" ><img src="./images/blank.png" alt="" id="studentIdError"/></td>
          <td align=left  width=25%>&nbsp;&nbsp;&nbsp;</td>
          <td align="left"><font color="red"><%= Msg%></font></td>
         </tr> 
       <%
	    }
       %>  
         <tr>
          <td align=left width="1%" ><img src="./images/blank.png" alt="" id="studentIdError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b>Student ID</b></td>
          <td align="left"><html:text property="studentId" size="20" styleClass="srchbox" styleId="studentId" style="border:1px solid #000000" value="" title="Student_id"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="firstnameMsg"></div></td>
         </tr>      
</table>

  <table style="" align="center" cellspacing=0 cellpadding=0 width=97%  > 
   <tr>   
	   <td colspan="2" align="center" >
	            <input type="submit" value ="Submit" onclick="return checkValidation();"/>
	            <input type="button" value ="Cancel"  onclick="javascript:history.back();" />
	  </td>
   </tr>
  </table>
</div> 
<html:hidden property="userid" styleClass="srchbox" style="border:1px solid #000000" value = "<%=struserid %>" />
 
    </html:form>


</body>
</html>

