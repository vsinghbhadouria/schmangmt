<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import ="java.text.*"%>
<%@ page import ="java.util.*"%>
<%@ page import ="com.sch.to.EduStudentReportTO"%>

<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentReportManager.js"></script>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentManager.js"></script>

<%
Integer userid= Integer.valueOf((String)request.getSession().getAttribute("user_id"));
	SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");	
%>


<html:form action="/EduStudentReportAction.do?operation=saveResult" method="Post">

<table align="center" width="97%" cellpadding="0" cellspacing="0" >
<tr class="mainhead" height="25px"><td>
		&nbsp;&nbsp;<b><bean:message key="app.sch.prepareReportCard.greenSheet"/></b>
		&nbsp;					
		</td>
		</tr>
	  
<tr align="left" style="background-image: url('./images/ltbrgrey1.gif')">
<td colspan="2">    
					<Label for ="name"><b>&nbsp;&nbsp;Session &nbsp;&nbsp; </b></Label>
			        <select name ="session" id= "session" onchange="searchByattribute();">
					</select>
					<Label for ="name"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Class &nbsp;&nbsp; </b></Label>
			        <select name ="class" id= "class" onchange="searchByattribute();">
					</select>
</td>
	</tr>	
<tr><td >
&nbsp;</td></tr></table>
<script type="text/javascript">
function searchByattribute(){
	
	 searchBysessionkey = trim(document.getElementById("session").value);
	 searchByclasskey = trim(document.getElementById("class").value);
	if(searchBysessionkey!='' && searchByclasskey!=''){
			 document.EduStudentReportForm.action=action="/schmangmt/EduStudentReportAction.do?operation=greenSheet&sessionId="+searchBysessionkey+"&classId="+searchByclasskey;
		     document.EduStudentReportForm.submit();
		   
	}	
}
</script>

<%
String strclassId="";
String strsessionId="";
ArrayList<EduStudentReportTO>  eduStudentReportToArrayObj = (ArrayList<EduStudentReportTO>)request.getAttribute("resultObj");
if(eduStudentReportToArrayObj!=null && eduStudentReportToArrayObj.size()!=0 && eduStudentReportToArrayObj.get(0).getStudentid()==0){
strclassId=String.valueOf(eduStudentReportToArrayObj.get(0).getClassid());
strsessionId=String.valueOf(eduStudentReportToArrayObj.get(0).getSessionid());
%>
<table align=center border=0 width=100%>
<tr>
<td align=center><b>Record not found</b></td>
</tr>
</table>
<%
}
%>
<div  align=left >
<br>
		<table align=center border=1 width=100%>
		<td style="background-image: url(./images/backImage.png); background-repeat: repeat;"> 
		
		<table align=center border=0>

		<tr id="logoimage" >
		       
				<td align="left" width=10%><img src="./images/imagesCA4CN5KS.jpg" width="120" height="80" alt="What We Do" align="left" >
			</td>
				<td align="center" width=100%><font size=6><bean:message key="app.sch.prepareReportCard.schoolName"/></font></td>
		  </tr>
		</table>	
	
<% 
try{
if(eduStudentReportToArrayObj!=null && eduStudentReportToArrayObj.size()!=0 && eduStudentReportToArrayObj.get(0).getStudentid()!=0){
strclassId=String.valueOf(eduStudentReportToArrayObj.get(0).getClassid());
strsessionId=String.valueOf(eduStudentReportToArrayObj.get(0).getSessionid());
int passStudent=0;
int failStudent=0;
float classStatus=0;
%>
	<table align=center border=0 width=100%>
		
		
		  <tr id="loimae" >
		       
				<td align="center" width=100%><font size=4>Green Sheet For [<%=eduStudentReportToArrayObj.get(0).getClassname()%>]</font></td>
				
		  </tr>
	</table>

	<table align=center border=1 width=100%>
    <tr>
	<td id="student_id" align="center" width=4% colspan=2>Student Name and Id</td>
	<td id="student_id" align="center" width=10 colspan=<%=eduStudentReportToArrayObj.get(0).getSubject().length%>>Subject's Marks</td>
	<td id="student_id" align="center" width=12% colspan=<%=eduStudentReportToArrayObj.get(0).getSubject().length%>>Obtained Marks</td>
	<td id="student_id" align="center" width=4% colspan=4>Result</td>
	</tr>
	<tr id="heading" >
	<td id="student_id" align="center" width=4%>Roll No.</td>
	<td id="student_id" align="center" width=12%>Student Name</td>
	
<%
for(int yy=0;yy<eduStudentReportToArrayObj.get(0).getSubject().length;yy++){
		%>
		<td id="student_id" align="center" width=5%><%=eduStudentReportToArrayObj.get(0).getSubject()[yy]%></td>
<%
	}

	for(int yy=0;yy<eduStudentReportToArrayObj.get(0).getSubject().length;yy++){
		%>
		<td id="student_id" align="center" width=5%><%=eduStudentReportToArrayObj.get(0).getSubject()[yy]%></td>
<%
	}
	
%>
	<td id="student_id" align="center" width=6%>Grand Total</td>
	<td id="student_id" align="center" width=5%>Percentage</td>
	<td id="student_id" align="center" width=5%>Division</td>
	<td id="student_id" align="center" width=5%>Result</td>
</tr>
</table>
<%
for(int xx=0;xx<eduStudentReportToArrayObj.size();xx++){
%>

	<table align=center border=0 width=100%>
		
		<tr id="studentRecord" >
			<td id="student_id" align="center" width=4%><%=eduStudentReportToArrayObj.get(xx).getStudentid()%></td>
			<td id="student_id" align="center" width=12%><%=eduStudentReportToArrayObj.get(xx).getStudentname() %></td>
			
			<%
			
			for(int yy=0;yy<eduStudentReportToArrayObj.get(xx).getMarkOutOf().length;yy++){
			
			%>
			<td id="student_id" align="center" width=5%><%=eduStudentReportToArrayObj.get(xx).getMarkOutOf()[yy] %></td>
			<%
			}
	
			for(int yy=0;yy<eduStudentReportToArrayObj.get(xx).getMarkObtain().length;yy++){
		    %>
			<td id="student_id" align="center" width=5%><%=eduStudentReportToArrayObj.get(xx).getMarkObtain()[yy]%></td>
			<%
			}
			
			%>
			
			<td id="student_id" align="center" width=6%><%=eduStudentReportToArrayObj.get(xx).getTotalMarkObtained() %>/<%=eduStudentReportToArrayObj.get(xx).getTotalOutOf() %></td>
			<td id="student_id" align="center" width=5%><%=eduStudentReportToArrayObj.get(xx).getPercentage() %></td>
			<td id="student_id" align="center" width=5%><%=eduStudentReportToArrayObj.get(xx).getDivision() %></td>
			<td id="student_id" align="center" width=5%><%=eduStudentReportToArrayObj.get(xx).getResult() %></td>
		</tr>	
	</table>
	
<%
		if(eduStudentReportToArrayObj.get(xx).getResult().equals("Pass")){
			passStudent=passStudent+1;
		}
		else{
			failStudent=failStudent+1;
		}
		
	}
	System.out.println("size"+eduStudentReportToArrayObj.size());
	classStatus=(eduStudentReportToArrayObj.size())*100/passStudent;
	%>
	<br>
		<table align=left border=0 width=22%>
			<tr id="schoolstatus" >
			<td id="student_id" align="left" width=10%><b>Class Performance</b></td>
			<td></td>
			</tr>
			<tr id="schoolstatus" >
			<td id="student_id" align="left" width=10%>Number Of Student =</td>
			<td id="student_id" align="left" width=10%><%=eduStudentReportToArrayObj.size()%></td>
			</tr>
			<tr id="passStudent" >
			<td id="student_id" align="left" width=10%>Passed Student =</td>
			<td id="student_id" align="left" width=10%><%=passStudent%></td>
			</tr>
			<tr id="failStudent" >
			<td id="student_id" align="left" width=10%>Failed Student =</td>
			<td id="student_id" align="left" width=10%><%=failStudent%></td>
			</tr>
			<tr id="classStatus" >
			<td id="student_id" align="left" width=10%>Class Status =</td>
			<td id="student_id" align="left" width=10%><%=classStatus%>%</td>
			</tr>
		</table>
	
	
	<%
}
}
catch(Exception exp){
	exp.printStackTrace();
}
%>
</table>
<html:hidden property="classid" styleId="class_id" styleClass="srchbox" style="border:1px solid #000000" value = "<%=strclassId%>"/>
<html:hidden property="sessionid" styleId="session_id" styleClass="srchbox" style="border:1px solid #000000" value="<%=strsessionId%>" />
</html:form>      
<script type="text/javascript">
  function loadSession()
    {
    	var sessionoption = document.getElementById("session");
		var selectedSession=document.getElementById("session_id").value;
    	EduStudentManager.getSession(function(session){
				for(var xx=0;xx<session.length;xx++)
				{
					var catidvalue = session[xx].split(",");
					if(catidvalue[0]==selectedSession){
					sessionoption[xx]= new Option(catidvalue[1],catidvalue[0],true); 
					}
					else{
					sessionoption[xx]= new Option(catidvalue[1],catidvalue[0]); 
					}
				}
				 
        	});
       
    }
	
	function loadClass()
    {
		var selectedClass = document.getElementById("class_id").value;
		var classoption = document.getElementById("class");
    	EduStudentManager.getClasses(function(classinfo){
				for(var xx=0;xx<classinfo.length;xx++)
				{
					var catidvalue = classinfo[xx].split(",");
					if(catidvalue[0]==selectedClass){
					classoption[xx]= new Option(catidvalue[1],catidvalue[0],true); 
					}
					else{
					classoption[xx]= new Option(catidvalue[1],catidvalue[0]);
					}	
				}
				 
        	});
       
    }
	
	 loadSession();
	 loadClass();
	 
</script>
