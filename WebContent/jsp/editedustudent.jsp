<%@ page language="java" %>
<%@page import="com.sch.to.EduStudentTO"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title>Institute Student</title> 
  	<!-- JS -->
  	<script type="text/javascript" src="./date-picker/scripts/html-5.js" charset="utf-8"  ></script>
  	<script type="text/javascript" src="./date-picker/scripts/html5.js" charset="utf-8"  ></script>
	<script type="text/javascript" src="./date-picker/scripts/yui-min.js" charset="utf-8"  ></script>
	<script type="text/javascript" src="./date-picker/scripts/gallery-aui-calendar-datepicker-select.js" charset="utf-8"  ></script>
    <script type="text/javascript" src="./date-picker/scripts/gallery-aui-calendar-datepicker-select-min.js"charset="utf-8"  ></script>
	<script type="text/javascript" src="./date-picker/scripts/collection-min.js" charset="utf-8"  ></script>
	<script type="text/javascript" src="./date-picker/scripts/gallery-aui-button-item-min.js" charset="utf-8"  ></script>
	<script type="text/javascript" src="./date-picker/scripts/gallery-aui-component-min.js" charset="utf-8"  ></script>
	<script type="text/javascript" src="./date-picker/scripts/gallery-aui-node-fx-min.js" charset="utf-8"  ></script>
	<script type="text/javascript" src="./date-picker/scripts/gallery-aui-state-interaction-min.js" charset="utf-8"  ></script>
	<script type="text/javascript" src="./date-picker/scripts/loader-min.js" charset="utf-8"  ></script>
	<link rel="stylesheet" type="text/css" href="./date-picker/styles/gallery-aui-calendar-datepicker-select.css" charset="utf-8" > 
    <!--This comment will work fine in disconnected net on firefox
	<link rel="stylesheet" type="text/css" href="./date-picker/styles/gallery-aui-skin-base-min.css" charset="utf-8" > 
	<link rel="stylesheet" type="text/css" href="./date-picker/styles/gallery-aui-skin-classic-min.css" charset="utf-8" >
	<link rel="stylesheet" type="text/css" href="./date-picker/styles/base-min.css" charset="utf-8" >
	-->
	<link rel="stylesheet" type="text/css" href="./date-picker/styles/gallery-aui-button-item.css" charset="utf-8" >
	<link rel="stylesheet" type="text/css" href="./date-picker/styles/overlay.css" charset="utf-8" >
	
	<style type="text/css">
	
		div.contentBody {
			width:750px;
			margin:2em;
		}
		
		#calendar {
			float:none;
			margin-bottom:1px;
			align="left"
		}
	
	
	</style> 	 	
  </head>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStudentManager.js" ></script>  		
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStaffManager.js"></script>
 <script language="JavaScript">
 function isValid(parm,val) {
	
	 
	 for (i=0; i<parm.length; i++) {
	 if (val.indexOf(parm.charAt(i),0) == -1) return false;
	 }
 	return true;
 	}
 function isEmail(string){
		if (string.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1){
			return true;
		}else{
			return false;
		}
	}
 

	//form validation
 function checkValidation()
  	 {
	   var numb = '0123456789';
	   var lwr  = 'abcdefghijklmnopqrstuvwxyz ';
	   var upr  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ';
	   var firstflg    = true;
	   var middleflg   = true;
	   var lastflg     = true;
	   var fathernameflg = true;
	   var phoneflg    = true;
	   var emailidflg  = true;
	   var genderflg = true;
	   var firstname      = document.getElementById('firstname').value;
	   var middlename     = document.getElementById('middlename').value;
	   var lastname       = document.getElementById('lastname').value;
	   var address    = document.getElementById('address').value;
	   var phoneno        = document.getElementById('phoneno').value;
	   var fathername          = document.getElementById('fathername').value;
	   var emailid        = document.getElementById('emailid').value;
	   var gender = document.getElementById('gender').value;
	   if(firstname!='')
		   {
		  document.getElementById('firstNameError').style.display='none';
	      document.getElementById('firstnameMsg').style.display = 'none';
	      if(isValid(firstname,lwr+upr))
		      {
		    firstflg = true ;
		  }
	      else
		      {
		
	       over("firstNameError","error_bang.gif");
	       document.getElementById('firstNameError').style.display='inline';
	       document.getElementById('firstnameMsg').innerHTML =' Firstname Should be Alphabetic';
	       document.getElementById('firstnameMsg').style.display = 'inline';
		   firstflg = false;
	 	  }
	       
	  }
	  else
		  { 
		over("firstNameError","error_bang.gif");
        document.getElementById('firstNameError').style.display='inline';
        document.getElementById('firstnameMsg').innerHTML =' Firstname Should Not be Blank';
        document.getElementById('firstnameMsg').style.display = 'inline';
        firstflg = false;
	  }
		if(lastname!='')
		  {
			  document.getElementById('lastNameError').style.display='none';
		      document.getElementById('lastnameMsg').style.display = 'none';
		      if(isValid(lastname,lwr+upr))
		      {
			    lastflg = true;
			  }
		      else
			  {
			
		       over("lastNameError","error_bang.gif");
		       document.getElementById('lastNameError').style.display='inline';
		       document.getElementById('lastnameMsg').innerHTML =' Lastname Should be Alphabetic';
		       document.getElementById('lastnameMsg').style.display = 'inline';
			   lastflg = false;
		 	  }
		       
		  }
		  else
		  { 
		    over("lastNameError","error_bang.gif");
	        document.getElementById('lastNameError').style.display='inline';
	        document.getElementById('lastnameMsg').innerHTML =' Father name Should Not be Blank';
	        document.getElementById('lastnameMsg').style.display = 'inline';
	        lastflg = false;
		  }
		if(fathername!='')
		  {
			  document.getElementById('fatherNameError').style.display='none';
		      document.getElementById('fatherNameMsg').style.display = 'none';
		      if(isValid(fathername,lwr+upr))
		      {
			    faternamehflg = true;
			  }
		      else
			  {
			
		       over("fatherNameError","error_bang.gif");
		       document.getElementById('fatherNameError').style.display='inline';
		       document.getElementById('fatherNameMsg').innerHTML ='Father name Should be Alphabetic';
		       document.getElementById('fatherNameMsg').style.display = 'inline';
			   fathernameflg = false;
		 	  }
		       
		  }
		  else
		  { 
		    over("fatherNameError","error_bang.gif");
	        document.getElementById('fatherNameError').style.display='inline';
	        document.getElementById('fatherNameMsg').innerHTML =' Father name Should Not be Blank';
	        document.getElementById('fatherNameMsg').style.display = 'inline';
	        fathernameflg = false;
		  }
	  if(phoneno!='')
		    {
			  document.getElementById('phoneError').style.display='none';
		      document.getElementById('phoneMsg').style.display = 'none';
		      if(isValid(phoneno,numb))
		      {
			    phoneflg = true ;
			  }
		      else
			  {
			
		       over("phoneError","error_bang.gif");
		       document.getElementById('phoneError').style.display='inline';
		       document.getElementById('phoneMsg').innerHTML =' Phone number Should be Numeric';
		       document.getElementById('phoneMsg').style.display = 'inline';
			   phoneflg = false;
		 	  }
		  	
		  }
		  else
		  { 
			over("phoneError","error_bang.gif");
	        document.getElementById('phoneError').style.display='inline';
	        document.getElementById('phoneMsg').innerHTML =' Phone number Should Not be Blank';
	        document.getElementById('phoneMsg').style.display = 'inline';
	        phoneflg = false;
		  }
  	 
		
		  if(emailid!='')
		   {
			  document.getElementById('emailidError').style.display='none';
		      document.getElementById('emailidMsg').style.display = 'none';
		      if(isEmail(emailid))
			      {
			    emailidflg = true ;
			  }
		      else
			      {
			
		       over("emailidError","error_bang.gif");
		       document.getElementById('emailidError').style.display='inline';
		       document.getElementById('emailidMsg').innerHTML =' Enter a valid Emailid ';
		       document.getElementById('emailidMsg').style.display = 'inline';
			   emailidflg = false;
		 	  }
	      	}
			else{ 
			  over("emailidError","error_bang.gif");
	          document.getElementById('emailidError').style.display='inline';
	          document.getElementById('emailidMsg').innerHTML =' Email Id Should Not be Blank';
	          document.getElementById('emailidMsg').style.display = 'inline';
	          emailidflg = false;
		     }
		  if(gender!=''){
			   document.getElementById('genderError').style.display='none';
			   document.getElementById('genderMsg').style.display = 'none';
			   genderflg=true;
			 }
		   else{
			   over("genderError","error_bang.gif");
		       document.getElementById('genderError').style.display='inline';
		       document.getElementById('genderMsg').innerHTML =' Select one option ';
		       document.getElementById('genderMsg').style.display = 'inline';
			   genderflg = false;
			   
		   }   
			
		  if(firstflg && middleflg && lastflg &&  phoneflg && emailidflg && fathernameflg && genderflg){
		  flag = true;
		  }
		  else{
			  flag = false;
		}
		  formsubmit();
	 return flag;
   }
//display all countries in dropdown box // display all upper levels in combo box
function getFeeByClass(){
	 document.getElementById('admissionid').style.display='';
	 document.getElementById('checkconid').style.display='';	 
	 var educlassid = document.getElementById("educlass").value;
	 if(educlassid!=-1){
		EduStudentManager.getAdmissionfee(educlassid,function(fee){
		 document.getElementById("admissionfee").value=fee;
		 });
	}
	else{
		document.getElementById('admissionid').style.display='none';
		document.getElementById('checkconid').style.display='none';	 
	}
  getSubjectByClass();
}

</script>
  <body>
    
    <html:errors />
      <html:form  action="/EduStudentAction.do?operation=update" method ="post">

   <div style="border: 1px solid;width:97%"  >
 <table style="" align="center" cellspacing="2" cellpadding="5" width=100% >
	<tr><td class="mainhead" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="app.sch.editedustudent.editformtitle"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   </td></tr>
        <tr>
          <td align=left width="1%" ><img src="./images/blank.png" alt="" id="firstNameError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.firstname"/></b></td>
          <td align="left"><html:text property="firstname" size="20" styleClass="srchbox" styleId="firstname" style="border:1px solid #000000" title="First Name"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="firstnameMsg"></div></td>
         </tr>
<tr>
 <td align=left ><img src="./images/blank.png" alt="" id="middleNameError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.middlename"/></b></td>
          <td><html:text property="middlename" size="20" styleClass="srchbox" styleId="middlename" style="border:1px solid #000000" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="middlenameMsg"></div></td>
        </tr>
        <tr>
<tr>
 <td align=left ><img src="./images/blank.png" alt="" id="lastNameError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.lastname"/></b></td>
          <td><html:text property="lastname" size="20" styleClass="srchbox" styleId="lastname" style="border:1px solid #000000" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="lastnameMsg"></div></td>
        </tr>
        
         <tr>
          <td align=left><img src="./images/blank.png" alt="" id="genderError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.sex"/></b></td>
		  <td align=left>
			<html:text property="gender" size="20" styleClass="srchbox" style="border:1px solid #000000" styleId = "gender" readonly="true"/>
		    &nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="genderMsg"></td>
         </tr>
               
        <tr>
          <td align=left ><img src="./images/blank.png" alt="" id="phoneError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.phoneno"/></b></td>
          <td><html:text property="phoneno" size="20" styleClass="srchbox" styleId="phoneno" style="border:1px solid #000000" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="phoneMsg"></div></td>
    </tr>
    <tr>      <td align=left><img src="./images/blank.png" alt="" id="addressError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.address"/></b></td>
          <td><html:textarea property="address"   styleId="address" style="border:1px solid #000000" rows="5" cols="30"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="addressMsg"></div></td>
        </tr>
		<tr>
          <td align=left ><img src="./images/blank.png" alt="" id="emailidError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.emailid"/></b></td>
          <td align="left"><html:text property="emailid" size="35" styleClass="srchbox" styleId="emailid" style="border:1px solid #000000" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="emailidMsg"></div></td>
         </tr>

		 <tr>
          <td align=left><img src="./images/blank.png" alt="" id="fatherNameError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.fathername"/></b></td>
          <td align="left"><html:text property="fathername" size="35" styleClass="srchbox" styleId="fathername" style="border:1px solid #000000"  />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="fatherNameMsg"></div></td>
         </tr>
<tr> <td align=left><img src="./images/blank.png" alt="" id="mothernameError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.mothername"/></b></td>
		  <td><html:text property="mothername" size="20" styleClass="srchbox" styleId="mothername" style="border:1px solid #000000" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="mothernameMsg"></td>
           </tr>
<tr>
          <td align=left ><img src="./images/blank.png" alt="" id="parentemailidError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.parentemailid"/></b></td>
          <td align="left"><html:text property="parentemailid" size="35" styleClass="srchbox" styleId="parentemailid" style="border:1px solid #000000" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="parentemailidMsg"></div></td>
         </tr>       
		 <tr>
          <td align=left><img src="./images/blank.png" alt="" id="previousinstituteError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.studentpreviousinstitute"/></b></td>
          <td align="left"><html:text property="previousinstitute" size="35" styleClass="srchbox" styleId="previousinstitute" style="border:1px solid #000000" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="previousinstituteMsg"></div></td>
         </tr>
<tr>
          <td align=left><img src="./images/blank.png" alt="" id="parenthighereduError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.parenthigherqualification"/></b></td>
		  <td align=left>
			<html:select property="parentqualification" style="border:1px solid #000000" styleId = "parentqualification" value="-1"  >
			<% 
						ArrayList<EduStudentTO>obj = (ArrayList<EduStudentTO>)request.getAttribute("parentqualifications");
						Integer currentlevelId  = (Integer)request.getAttribute("parentqualification_id");
					
						for(int xx = 0;xx<obj.size();xx++)
						{
						if(currentlevelId==obj.get(xx).getParentqualification_id())
						 {
						 %>
                                
							  <option  value="<%=obj.get(xx).getParentqualification_id()%>" selected><%=obj.get(xx).getParentqualification_title()%></option>
						 <% }
						 else
						 {%>
							  <option  value="<%=obj.get(xx).getParentqualification_id()%>"><%=obj.get(xx).getParentqualification_title()%></option>
						 <% }
						}
						%>
				<!-- <option value="-1" />none</option>  
				<html:options collection="parentqualifications"  property="parentqualification_id"  labelProperty="parentqualification_title" />-->
			</html:select>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="highereduMsg"></td>
         </tr>		 
		 <tr>
          <td align=left><img src="./images/blank.png" alt="" id="studentpreviousqualiError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.studentpreviousqualification"/></b></td>
		  <td align=left>
			<html:select property="studprequalification" style="border:1px solid #000000" styleId = "studprequalification" value="-1" >
				<% 
						ArrayList<EduStudentTO>obj = (ArrayList<EduStudentTO>)request.getAttribute("studprequalifications");
						Integer currentlevelId  = (Integer)request.getAttribute("studprequalification_id");
					
						for(int xx = 0;xx<obj.size();xx++)
						{
						if(currentlevelId==obj.get(xx).getStudprequalification_id())
						 {
						 %>
                                
							  <option  value="<%=obj.get(xx).getStudprequalification_id()%>" selected><%=obj.get(xx).getStudprequalification_title()%></option>
						 <% }
						 else
						 {%>
							  <option  value="<%=obj.get(xx).getStudprequalification_id()%>"><%=obj.get(xx).getStudprequalification_title()%></option>
						 <% }
						}
						%>
				<!-- <option value="-1" />none</option>  
				<html:options collection="studprequalifications"  property="studprequalification_id"  labelProperty="studprequalification_title" />-->
			</html:select>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="highereduMsg"></td>
         </tr>
        
		 <tr>
          <td align="left"><img src="./images/blank.png" alt="" id="birthyearError"/></td><td align=left  width=25%>&nbsp;&nbsp;<b>			<bean:message key="app.sch.addedustudent.dateofbirth"/></b></td>
		  <td>
           <div id="calendar">
		  	<html:select property="birthday"  style="border:1px solid #000000" styleId = "dayselect" styleClass="yui3-datepicker-day" >
			</html:select>
			<html:select property="birthmonth" style="border:1px solid #000000" styleId = "monthselect"  styleClass="yui3-datepicker-month">
			</html:select>
			<html:select property="birthyear" style="border:1px solid #000000" styleId = "yearselect" styleClass="yui3-datepicker-year">
			</html:select>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="birthyearMsg"></div>
			</div>
		  </td>
         </tr>    
          <tr>
          <td align="left"><img src="./images/blank.png" alt="" id="admissiondateError"/></td><td align=left  width=25%>&nbsp;&nbsp;<b>			<bean:message key="app.sch.addedustudent.admissiondate"/></b></td>
		  <td>
           <div id="calendar1">
		  	<html:select property="admissionday"  style="border:1px solid #000000" styleId = "admissionday" styleClass="yui3-datepicker-day" >
			</html:select>
			<html:select property="admissionmonth" style="border:1px solid #000000" styleId = "admissionmonth"  styleClass="yui3-datepicker-month">
			</html:select>
			<html:select property="admissionyear" style="border:1px solid #000000" styleId = "admissionyear" styleClass="yui3-datepicker-year">
			</html:select>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="admissiondateMsg"></div>
			</div>
		  </td>
         </tr>

                <tr id="dept_tab" >
          <td align=left><img src="./images/blank.png" alt="" id="highereduError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addeduinstituestaff.department"/></b></td>
		  <td align=left>
			<html:select property="department" style="border:1px solid #000000" styleId = "department" value="-1" onchange="getClassInfoByDepartment()" >
				<% 
						ArrayList<EduStudentTO>obj = (ArrayList<EduStudentTO>)request.getAttribute("departments");
						Integer currentlevelId  = (Integer)request.getAttribute("deptid");
						for(int xx = 0;xx<obj.size();xx++)
						{
						if(currentlevelId==obj.get(xx).getDept_id())
						 {
						 %>
                                
							  <option  value="<%=obj.get(xx).getDept_id()%>" selected><%=obj.get(xx).getDept_title()%></option>
						 <% }
						 else
						 {%>
							  <option  value="<%=obj.get(xx).getDept_id()%>"><%=obj.get(xx).getDept_title()%></option>
						 <% }
						}
						%>
			</html:select>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="highereduMsg"></td>
         </tr>
		 
<tr>
          <td align=left><img src="./images/blank.png" alt="" id="studclassError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.class"/></b></td>
		  <td align=left>
			<html:select property="educlass" style="border:1px solid #000000" styleId = "educlass" value="-1" onchange="getFeeByClass()" >
				<% 
						ArrayList<EduStudentTO>obj = (ArrayList<EduStudentTO>)request.getAttribute("classes");
						Integer currentlevelId  = (Integer)request.getAttribute("classid");
						for(int xx = 0;xx<obj.size();xx++)
						{
						if(currentlevelId==obj.get(xx).getClassid())
						 {
						 %>
                                
							  <option  value="<%=obj.get(xx).getClassid()%>" selected><%=obj.get(xx).getClassname()%></option>
						 <% }
						 else
						 {%>
							  <option  value="<%=obj.get(xx).getClassid()%>"><%=obj.get(xx).getClassname()%></option>
						 <% }
						}
						%>
		<!--<option value="-1" />none</option>  
				<html:options collection="classes"  property="classid"  labelProperty="classname" />-->
			</html:select>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="highereduMsg"></td>
     <tr>
				<td ></td>
				<td colspan="2" >
				<div id='subjecttab' style="display:inline" >
					<table>
						<tr>
			    			<td class="tdLabel_style" align=center><bean:message key="app.sch.addeduinstituestaff.allsubjects"/><br>
							 	<select name="allsubjects" Id="allsubjects" size=8 multiple="multiple" style="width: 200px; background-color:#EEEEEE; border:1px solid #000000; font-size : 11px; font-family : Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight : normal;"></select>
							</td>
							<td>
								<a href="javascript:assignSubjects();">
									<img src="./images/moveright.gif" border="0">
								</a>
								<br>
								<a href="javascript:moveSubjects();">
									<img src="./images/moveleft.gif" border="0">
								</a>	
							</td>	
				            <td class="tdLabel_style" align=center><bean:message key="app.sch.addeduinstituestaff.assignedsubjects"/><br>
								<select name="selectedsubjects" Id="selectedsubjects" size=8 multiple="multiple"  style=" width: 200px; background-color:#FFFFFF; border:1px solid #000000; font-size : 11px; font-family : Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight : normal;"></select>
								
							</td>
						</tr>
						<tr >
							<td></td>
							<td></td>
							<td>
								<img src="./images/blank.png" alt="" id="zipcodeError"/><div style ='display:none; color: red;' id="zipcodeMsg">
							</td>
						</tr>
					</table>
					</div>
				</td>
				<td ></td>
			</tr>
		  <tr id="admissionid" style="display: none;">
          <td align=left width="1%" ><img src="./images/blank.png" alt="" id="admissionfeeError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.admissionfee"/></b></td>
          <td align="left"><html:text property="admissionfee" size="20" styleClass="srchbox" styleId="admissionfee" style="border:1px solid #000000" value="" title="Admission Fee"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="admissionfeeMsg"></div></td>
         </tr>
		 <tr id="checkconid" style="display: none;">
          <td align=left width="1%" ><img src="./images/blank.png" alt="" id="admissionfeeError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b></b></td>
          <td align="left"><html:checkbox property="admissionfee" styleId="checkconcessionid" onclick="displayConsession()" />&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.studentconcession"/></b><div style ='display:none; color: red;' id="admissionfeeMsg"></div></td>
         </tr>
		  <tr id="concessid" style="display: none;">
          <td align=left width="1%" ><img src="./images/blank.png" alt="" id="admissionfeeError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.concessionamount"/></b></td>
          <td align="left"><html:text property="concessionamt" size="20" styleClass="srchbox" styleId="concessionamt" style="border:1px solid #000000" value="" title="Admission Fee"  />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="admissionfeeMsg"></div></td>
         </tr>
		 <tr id="commentid" style="display: none;"><td align=left><img src="./images/blank.png" alt="" id="commentError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addedustudent.comments"/></b></td>
          <td><html:textarea property="comment"   styleId="comment" style="border:1px solid #000000" value="" rows="3" cols="20"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="commentMsg"></div></td>
        </tr>

</table>


  <table style="" align="center" cellspacing=2 cellpadding=5 width=97%  > 
   <tr>   <td colspan="2" align="center" >
            <input type="submit" value ="Submit" onclick="return checkValidation();"/>
            <input type="button" value ="Cancel"  onclick="javascript:history.back();" />
            </td>
        
        </tr>
      </table>
     </div> 
<html:hidden property="studentid" styleId ="studentid" styleClass="srchbox" style="border:1px solid #000000" />

    </html:form>

<script>
    var birthdate='<%=request.getAttribute("birthdate")%>'
    var admissiondate='<%=request.getAttribute("admissiondate")%>'        
	var startyear='<%=request.getAttribute("startyear")%>'
	var currentyear='<%=request.getAttribute("currentyear")%>'
	//alert("birthdate"+birthdate+"admissiondate"+admissiondate);
	//alert(birthdate+" "+startyear+" "+currentyear);
YUI({
    gallery: 'gallery-2010.06.07-17-52',
    modules: {
        'gallery-aui-skin-base': {
            fullpath: 'date-picker/styles/gallery-aui-skin-base-min.css',
            type: 'css'
        },
        'gallery-aui-skin-classic': {
            fullpath: 'date-picker/styles/gallery-aui-skin-classic-min.css',
            type: 'css',
            requires: ['gallery-aui-skin-base']
        }
    }
}).use('gallery-aui-calendar-datepicker-select', function(Y) {
    var datePickerSelect = new Y.DatePickerSelect({
		displayBoundingBox: '#calendar',
		yearRange: [startyear, currentyear],
	    dates: [birthdate],
	    dateFormat: '%d/%m/%y %A',
	    dayField: Y.one("#dayselect"),
		dayFieldName: "birthday",
		monthField: Y.one("#monthselect"),
		monthFieldName: "birthmonth",
		yearField: Y.one("#yearselect"),
		yearFieldName: "birthyear"
	}).render();

    var datePickerSelect1 = new Y.DatePickerSelect({
		displayBoundingBox: '#calendar1',
		yearRange: [startyear,currentyear],
		dates: [admissiondate],
	    dateFormat: '%d/%m/%y %A',
		dayField: Y.one("#admissionday"),
		dayFieldName: "admissionday",
		monthField: Y.one("#admissionmonth"),
		monthFieldName: "admissionmonth",
		yearField: Y.one("#admissionyear"),
		yearFieldName: "admissionyear"
	}).render();
});

</script>
</body>
</html>
<script>
function formsubmit()
{
	var selSub = document.getElementById("selectedsubjects");
	var slen = selSub.length;
	for(var j=slen-1; j>=0; j--)
	{
		selSub.options[j].selected=true;
	}
}	

function displayConsession(){
	  var status=document.getElementById('checkconcessionid').checked;
	  if(status==true){
		document.getElementById('concessid').style.display='';
		document.getElementById('commentid').style.display='';
	  }
	  else{
		document.getElementById('concessid').style.display='none';
		document.getElementById('commentid').style.display='none';
	  }
}
function checkConcessionamt(){
		var concessionamt = document.getElementById('concessionamt').value;
		if(concessionamt!=0){
		     document.getElementById('checkconid').style.display='';
			 document.getElementById('checkconcessionid').status=true;
		     displayConsession();
		}
}
 checkConcessionamt();
 	function assignSubjects()
	{
		var assoList = document.getElementById('allsubjects');
		var len = assoList.length;
		var selAssoList = document.getElementById('selectedsubjects');
		for(var j=len-1;j>=0;j--)
		{		
			if(assoList[j].selected && assoList[j].selected!=null){
				var selText = assoList.options[j].text;
				var selValue = assoList.options[j].value;
				if(checkSubjects(selText,selValue)){
					selAssoList[selAssoList.length]= new Option(selText, selValue, true);
				}	
				assoList.remove(j);
			}
		}
	}
	function moveSubjects(){
		var assoList = document.getElementById('allsubjects');
		var selAssoList = document.getElementById('selectedsubjects');
		var len = selAssoList.length;
		for(var j=len-1;j>=0;j--){
			if(selAssoList[j].selected  && selAssoList[j].selected!=null){
				var selText = selAssoList.options[j].text;
				var selValue = selAssoList.options[j].value;
				assoList[assoList.length]= new Option(selText, selValue, true);
				selAssoList.remove(j);
			}
		}	
	}
	function checkSubjects(seltext,selValue){
		flag = true;
		var selAssoList = document.getElementById('selectedsubjects');
		var lenAssoList = selAssoList.length;
		if(lenAssoList!=0){
			for(var xx=0;xx<lenAssoList;xx++){			 
				if(selValue==selAssoList[xx].value){ 
					flag = false; 
					break; 
				}
			}
		} 
		return flag;
	}
 function getSubjectByClass()
 {
 	var allsubjects = document.getElementById('allsubjects');
	var selectedsubjects = document.getElementById('selectedsubjects');
	var deptId = document.getElementById('department').value;
	var userId = '<%=request.getAttribute("userid")%>';
	var classId = document.getElementById('educlass').value;
	var sessionId='<%=request.getAttribute("sessionId")%>';
	removeOptions(allsubjects);
	removeOptions(selectedsubjects);
	EduStudentManager.getSelectedSubject(userId,classId,sessionId,function(associatedsubject){
		for(var i=0;i<associatedsubject.length;i++)
			{
				var selectedUserOptions = associatedsubject[i];
				selectedUserOptions = selectedUserOptions.split(',');
				selectedsubjects[i] = new Option(selectedUserOptions[1],selectedUserOptions[0]);
			}
 		   });
	
 	EduStudentManager.getSubjectByClass(deptId,classId,function(userlist){
			for(var i=0;i<userlist.length;i++)
 			{
 				var userOptions = userlist[i];
				userOptions = userOptions.split(',');
				allsubjects[i] = new Option(userOptions[1],userOptions[0]);
 			}
 			});
 }
 getSubjectByClass();
 function getClassInfoByDepartment()
 {
    var educlass = document.getElementById('educlass');
	var allsubjects = document.getElementById('allsubjects');
	var selectedsubjects = document.getElementById('selectedsubjects');
    var deptId = document.getElementById('department').value;
	removeOptions(educlass);
	removeOptions(allsubjects);
	removeOptions(selectedsubjects);
	EduStaffManager.getClassInfoByDepartment(deptId,function(userlist){
			for(var i=0;i<userlist.length;i++)
 			{
 				var userOptions = userlist[i];
				userOptions = userOptions.split(',');
				educlass[i] = new Option(userOptions[1],userOptions[0]);
 			}
 			});
 }
 function removeOptions(allclasses)
 {
 	var i;
 	for(i=allclasses.options.length-1;i>=0;i--)
 	{
 		allclasses.remove(i);
 	}
 }

</script>
