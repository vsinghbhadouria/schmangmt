<%@ page language="java" %>
<%@page import="com.sch.to.EduStaffTO"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
<head>
 
<title>Edit Employees</title>  
</head>
<script type='text/javascript' src="/schmangmt/dwr/interface/EduStaffManager.js"></script>
<!-- Combo-handled YUI CSS files: --> 
<link rel="stylesheet" type="text/css" href="./date-picker/styles/calendar.css"> 
<!-- Combo-handled YUI JS files: --> 
<script type="text/javascript" src="./scripts/button-min.js"></script>
<script type="text/javascript" src="./scripts/dragdrop-min.js"></script>
<script type="text/javascript" src="./date-picker/scripts/container-min.js"></script>

 <script language="JavaScript">
 function isValid(parm,val) {
	
	 
	 for (i=0; i<parm.length; i++) {
	 if (val.indexOf(parm.charAt(i),0) == -1) return false;
	 }
 	return true;
 	}
 function isEmail(string){
		if (string.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1){
			return true;
		}else{
			return false;
		}
	}
 
 //getClassInfo();
	//form validation
 function checkValidation()
  	 {
	   var numb = '0123456789';
	   var lwr  = 'abcdefghijklmnopqrstuvwxyz ';
	   var upr  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ';
	   var firstflg    = true;
	   var middleflg   = true;
	   var lastflg     = true;
	   var salaryflg     = true;
	   var phoneflg    = true;
	   var levelflg    = true;
	   var emailidflg  = true;
	   var passwordflg = true;
	   var countryflg  = true;
	   var flag        = true;
	   var firstname      = document.getElementById('firstname').value;
	   var middlename     = document.getElementById('middlename').value;
	   var lastname       = document.getElementById('lastname').value;
	   var address    = document.getElementById('address').value;
	   var phoneno        = document.getElementById('phoneno').value;
	   var salary          = document.getElementById('salary').value;
	   var level          = document.getElementById('level').value;
	   var emailid        = document.getElementById('emailid').value;
	   if(firstname!='')
		   {
		  document.getElementById('firstNameError').style.display='none';
	      document.getElementById('firstnameMsg').style.display = 'none';
	      if(isValid(firstname,lwr+upr))
		      {
		    firstflg = true ;
		  }
	      else
		      {
		
	       over("firstNameError","error_bang.gif");
	       document.getElementById('firstNameError').style.display='inline';
	       document.getElementById('firstnameMsg').innerHTML =' Firstname Should be Alphabetic';
	       document.getElementById('firstnameMsg').style.display = 'inline';
		   firstflg = false;
	 	  }
	       
	  }
	  else
		  { 
		over("firstNameError","error_bang.gif");
        document.getElementById('firstNameError').style.display='inline';
        document.getElementById('firstnameMsg').innerHTML =' Firstname Should Not be Blank';
        document.getElementById('firstnameMsg').style.display = 'inline';
        firstflg = false;
	  }
		if(lastname!='')
		  {
			  document.getElementById('lastNameError').style.display='none';
		      document.getElementById('lastnameMsg').style.display = 'none';
		      if(isValid(lastname,lwr+upr))
		      {
			    lastflg = true;
			  }
		      else
			  {
			
		       over("lastNameError","error_bang.gif");
		       document.getElementById('lastNameError').style.display='inline';
		       document.getElementById('lastnameMsg').innerHTML =' Lastname Should be Alphabetic';
		       document.getElementById('lastnameMsg').style.display = 'inline';
			   lastflg = false;
		 	  }
		       
		  }
		  else
		  { 
		    over("lastNameError","error_bang.gif");
	        document.getElementById('lastNameError').style.display='inline';
	        document.getElementById('lastnameMsg').innerHTML =' Lastname Should Not be Blank';
	        document.getElementById('lastnameMsg').style.display = 'inline';
	        lastflg = false;
		  }
		  if(salary!='')
		  {
			  document.getElementById('salaryError').style.display='none';
		      document.getElementById('salaryMsg').style.display = 'none';
			  salaryflg = true;
		       
		  }
		  else
		  { 
			over("salaryError","error_bang.gif");
	        document.getElementById('salaryError').style.display='inline';
	        document.getElementById('salaryMsg').innerHTML =' Salary Should Not be Blank';
	        document.getElementById('salaryMsg').style.display = 'inline';
	        salaryflg = false;
		  }
		  if(phoneno!='')
		    {
			  document.getElementById('phoneError').style.display='none';
		      document.getElementById('phoneMsg').style.display = 'none';
		      if(isValid(phoneno,numb))
		      {
			    phoneflg = true ;
			  }
		      else
			  {
			
		       over("phoneError","error_bang.gif");
		       document.getElementById('phoneError').style.display='inline';
		       document.getElementById('phoneMsg').innerHTML =' Phone number Should be Numeric';
		       document.getElementById('phoneMsg').style.display = 'inline';
			   phoneflg = false;
		 	  }
		  	
		  }
		  else
		  { 
			over("phoneError","error_bang.gif");
	        document.getElementById('phoneError').style.display='inline';
	        document.getElementById('phoneMsg').innerHTML =' Phone number Should Not be Blank';
	        document.getElementById('phoneMsg').style.display = 'inline';
	        phoneflg = false;
		  }
  	 
		
		  if(level =="-1")
		  {
				over("levelError","error_bang.gif");
		        document.getElementById('levelError').style.display='inline';
		        document.getElementById('levelMsg').innerHTML ='Select a Designation';
		        document.getElementById('levelMsg').style.display = 'inline';
		        levelflg = false;
		  }  
		  else
		  {
			  document.getElementById('levelError').style.display='none';
		      document.getElementById('levelMsg').style.display = 'none';
		  } 
		  if(emailid!='')
		   {
		  document.getElementById('emailidError').style.display='none';
	      document.getElementById('emailidMsg').style.display = 'none';
	      if(isEmail(emailid))
		      {
		    emailidflg = true ;
		  }
	      else
		      {
		
	       over("emailidError","error_bang.gif");
	       document.getElementById('emailidError').style.display='inline';
	       document.getElementById('emailidMsg').innerHTML =' Enter a valid Emailid ';
	       document.getElementById('emailidMsg').style.display = 'inline';
		   emailidflg = false;
	 	  }
	      
	  }
	  else
		 { 
		  over("emailidError","error_bang.gif");
          document.getElementById('emailidError').style.display='inline';
          document.getElementById('emailidMsg').innerHTML =' Email Id Should Not be Blank';
          document.getElementById('emailidMsg').style.display = 'inline';
          emailidflg = false;
	     } 
	
		  if(firstflg && middleflg && lastflg && levelflg && phoneflg && emailidflg && salaryflg){
		  flag = true;
		  }
		  else{
			  flag = false;
		}
		  formsubmit();	  
	  return flag;
   }
//display all countries in dropdown box // display all upper levels in combo box
 function displayCountry()
  {
	 
	var keyword = document.getElementById("level").value;
	     
	           return true;
  }
  
   function displayDepartmentBox()
  {

	 var keyword = document.getElementById("level").value;
	    if(keyword==3) {
	    	 document.getElementById('dept_tab').style.display='';
			 document.getElementById('tab2').style.display='none';
			 document.getElementById('subjecttab').style.display='none';
		} 
	    else{

	    	 document.getElementById('dept_tab').style.display='none';
			 document.getElementById('tab2').style.display='none';
			 document.getElementById('subjecttab').style.display='none';
			 
		}
	 
	
     return true;
  }
 function  displayClassSelBox()
 {
		var keyword = document.getElementById("level").value;
		if(keyword==3) {
	    	document.getElementById('tab2').style.display='inline';
			document.getElementById('subjecttab').style.display='inline';
		} 
	    else{

	    	document.getElementById('tab2').style.display='none';
			document.getElementById('subjecttab').style.display='none';
			
		}
		getClassInfo();
		getSubjectByDept();
        return true;

 } 	
		function assignClasses()
	{
		var assoList = document.getElementById('allclasses');
		var len = assoList.length;
		var selAssoList = document.getElementById('selectedclasses');
		for(var j=len-1;j>=0;j--)
		{		
			if(assoList[j].selected && assoList[j].selected!=null){
				var selText = assoList.options[j].text;
				var selValue = assoList.options[j].value;
				if(checkClasses(selText,selValue)){
					selAssoList[selAssoList.length]= new Option(selText, selValue, true);
				}	
				assoList.remove(j);
			}
		}
	}
	function moveClasses(){
		var assoList = document.getElementById('allclasses');
		var selAssoList = document.getElementById('selectedclasses');
		var len = selAssoList.length;
		for(var j=len-1;j>=0;j--){
			if(selAssoList[j].selected  && selAssoList[j].selected!=null){
				var selText = selAssoList.options[j].text;
				var selValue = selAssoList.options[j].value;
				assoList[assoList.length]= new Option(selText, selValue, true);
				selAssoList.remove(j);
			}
		}	
	}
	function checkClasses(seltext,selValue){
		flag = true;
		var selAssoList = document.getElementById('selectedclasses');
		var lenAssoList = selAssoList.length;
		if(lenAssoList!=0){
			for(var xx=0;xx<lenAssoList;xx++){			 
				if(selValue==selAssoList[xx].value){ 
					flag = false; 
					break; 
				}
			}
		} 
		return flag;
	}
	
	function assignSubjects()
	{
		var assoList = document.getElementById('allsubjects');
		var len = assoList.length;
		var selAssoList = document.getElementById('selectedsubjects');
		for(var j=len-1;j>=0;j--)
		{		
			if(assoList[j].selected && assoList[j].selected!=null){
				var selText = assoList.options[j].text;
				var selValue = assoList.options[j].value;
				if(checkSubjects(selText,selValue)){
					selAssoList[selAssoList.length]= new Option(selText, selValue, true);
				}	
				assoList.remove(j);
			}
		}
	}
	function moveSubjects(){
		var assoList = document.getElementById('allsubjects');
		var selAssoList = document.getElementById('selectedsubjects');
		var len = selAssoList.length;
		for(var j=len-1;j>=0;j--){
			if(selAssoList[j].selected  && selAssoList[j].selected!=null){
				var selText = selAssoList.options[j].text;
				var selValue = selAssoList.options[j].value;
				assoList[assoList.length]= new Option(selText, selValue, true);
				selAssoList.remove(j);
			}
		}	
	}
	function checkSubjects(seltext,selValue){
		flag = true;
		var selAssoList = document.getElementById('selectedsubjects');
		var lenAssoList = selAssoList.length;
		if(lenAssoList!=0){
			for(var xx=0;xx<lenAssoList;xx++){			 
				if(selValue==selAssoList[xx].value){ 
					flag = false; 
					break; 
				}
			}
		} 
		return flag;
	}
</script>
  <body>
    
    <html:errors />
      <html:form  action="/EduStaffAction.do?operation=update" method ="post">
        <%
                           
                         String strfirstname   = request.getParameter("firstname");
                         String strmiddlename  = request.getParameter("middlename");
                         String strlastname    = request.getParameter("lastname");
                         String strphoneno     = request.getParameter("phoneno");
                         String straddress = request.getParameter("address");
                         String strdesignation_id       = request.getParameter("designation_id");
                         String strqualification_id        = request.getParameter("qualification_id");
                         String strpreviousexp    = request.getParameter("previousexp");
                         String strsalary       = request.getParameter("salary");
                         String strjoiningdate     = request.getParameter("joiningdate");
                         String struserid      = request.getParameter("userid");
                         String stremailid     = request.getParameter("emailid");
                         if(request.getParameter("btnReset")!=null)
                         {
                         	System.out.println("with in if condition");
                         	strfirstname = "";
                         	strlastname = "";
                         	strmiddlename = "";
                         	strphoneno ="";
                         	stremailid ="";
                         	straddress ="";
                         	strpreviousexp ="";
                         	strjoiningdate ="";
                         }
                           
                         
     %>
<div style="border: 1px solid;width:97%"  >
 <table style="" align="center" cellspacing="2" cellpadding="5" width=100% >
	<tr><td class="mainhead" colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;<bean:message key="app.sch.eduinstitutestafflist.addformtitle"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	   </td></tr>
        <tr>
          <td align=left width="1%" ><img src="./images/blank.png" alt="" id="firstNameError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addeduinstituestaff.firstname"/></b></td>
          <td align="left"><html:text property="firstname" size="20" styleClass="srchbox" styleId="firstname" style="border:1px solid #000000" value="<%= strfirstname%>" title="First Name"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="firstnameMsg"></div></td>
         </tr>
<tr>
 <td align=left ><img src="./images/blank.png" alt="" id="middleNameError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addeduinstituestaff.middlename"/></b></td>
          <td><html:text property="middlename" size="20" styleClass="srchbox" styleId="middlename" style="border:1px solid #000000" value="<%= strmiddlename%>" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="middlenameMsg"></div></td>
        </tr>
        <tr>
<tr>
 <td align=left ><img src="./images/blank.png" alt="" id="lastNameError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addeduinstituestaff.lastname"/></b></td>
          <td><html:text property="lastname" size="20" styleClass="srchbox" styleId="lastname" style="border:1px solid #000000" value="<%= strlastname%>" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="lastnameMsg"></div></td>
        </tr>
        <tr>
          <td align=left ><img src="./images/blank.png" alt="" id="phoneError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addeduinstituestaff.phoneno"/></b></td>
          <td><html:text property="phoneno" size="20" styleClass="srchbox" styleId="phoneno" style="border:1px solid #000000" value="<%= strphoneno%>" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="phoneMsg"></div></td>
    </tr><tr>      <td align=left><img src="./images/blank.png" alt="" id="addressError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addeduinstituestaff.address"/></b></td>
          <td><html:textarea property="address"   styleId="address" style="border:1px solid #000000" rows="5" cols="30" value="<%= straddress%>"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="addressMsg"></div></td>
        </tr>
<tr> <td align=left><img src="./images/blank.png" alt="" id="salaryError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addeduinstituestaff.salary"/></b></td>
		  <td><html:text property="salary" size="20" styleClass="srchbox" styleId="salary" style="border:1px solid #000000" value="<%= strsalary%>" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="salaryMsg"></td>
           </tr>
<tr>
          <td align=left ><img src="./images/blank.png" alt="" id="emailidError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addeduinstituestaff.emailid"/></b></td>
          <td align="left"><html:text property="emailid" size="35" styleClass="srchbox" styleId="emailid" style="border:1px solid #000000" value="<%= stremailid%>" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="emailidMsg"></div></td>
         </tr>                         
<tr>
          <td align=left><img src="./images/blank.png" alt="" id="highereduError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addeduinstituestaff.higherqualification"/></b></td>
		  <td align=left>
			<html:select property="qualification" style="border:1px solid #000000" styleId = "qualification" value="-1" onchange="displayCountry()" >
				<% 
						ArrayList<EduStaffTO> obj = (ArrayList<EduStaffTO>)request.getAttribute("qualifications");
						Integer currentlevelId  = (Integer)request.getAttribute("assoqualification_id");
					
						for(int xx = 0;xx<obj.size();xx++)
						{
						if(currentlevelId==obj.get(xx).getQualification_id())
						 {
						 %>
                                
							  <option  value="<%=obj.get(xx).getQualification_id()%>" selected><%=obj.get(xx).getQualification_title()%></option>
						 <% }
						 else
						 {%>
							  <option  value="<%=obj.get(xx).getQualification_id()%>"><%=obj.get(xx).getQualification_title()%></option>
						 <% }
						}
						%>
				<!--   <option value="-1" />none</option>  
				<html:options collection="qualifications"  property="qualification_id"  labelProperty="qualification_title" />-->
			</html:select>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="highereduMsg"></td>
         </tr>
         <tr>
          <td align=left><img src="./images/blank.png" alt="" id="previousexpError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addeduinstituestaff.previousexp"/></b></td>
          <td align="left"><html:text property="previousexp" size="35" styleClass="srchbox" styleId="previousexp" style="border:1px solid #000000" value="<%= strpreviousexp%>" />&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="previousexpdMsg"></div></td>
         </tr>
         <tr>
          <td align=left><img src="./images/blank.png" alt="" id="joiningdateError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addeduinstituestaff.joiningdate"/></b></td>
		  
		  <td align="left">
				<div >

				<div class="box">
       			<div class="datefield">
       
        		<html:text property="joiningdate" styleId ="joiningdate" value="<%= strjoiningdate%>" size="27"  styleClass="srchbox" style="border:1px solid #000000" />&nbsp;
				<button type="button" id="show" title="Show Calendar">
					<img src="./yui/build/calendar/assets/calbtn.gif" width="18" height="18" alt="Calendar" >
				</button>
				&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="joiningdateMsg"></div>
				</div>
       			</div>
       			</div>
			</td>
         </tr>
        <tr>
          		    <td align=left ><img src="./images/blank.png" alt="" id="levelError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addeduinstituestaff.level"/></b></td>
                    <td align=left>
						<html:select property="level" style="border:1px solid #000000" styleId = "level" value="-1" onchange="displayDepartmentBox()" >
						
						
						<% 
						ArrayList<EduStaffTO> obj = (ArrayList<EduStaffTO>)request.getAttribute("levels");
						Integer currentlevelId  = (Integer)request.getAttribute("assodesignation_id");
					
						for(int xx = 0;xx<obj.size();xx++)
						{
						if(currentlevelId==obj.get(xx).getDesignation_id())
						 {
						 %>
                                
							    <option  value="<%=obj.get(xx).getDesignation_id()%>" selected><%=obj.get(xx).getDesignation_title()%></option>
						 <% }
						 else
						 {%>
							  <option  value="<%=obj.get(xx).getDesignation_id()%>"><%=obj.get(xx).getDesignation_title()%></option>
						 <% }
						}
						%>
						
						<!--       <option value="-1" />none</option>  
							<html:options collection="levels"  property="designation_id"  labelProperty="designation_title" /> -->
  						</html:select>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="levelMsg"></td> 
						
					
         </tr>
		 
		 <tr id="dept_tab" style="display:none">
          <td align=left><img src="./images/blank.png" alt="" id="highereduError"/></td><td align=left  width=25%>&nbsp;&nbsp;&nbsp;<b><bean:message key="app.sch.addeduinstituestaff.department"/></b></td>
		<td align=left>
						<html:select property="department" style="border:1px solid #000000" styleId = "department" value="-1" onchange="displayClassSelBox()" >
						
						
						<% 
						ArrayList<EduStaffTO> obj = (ArrayList<EduStaffTO>)request.getAttribute("departments");
						Integer currentdeptId  = (Integer)request.getAttribute("assodept_id");
					
						for(int xx = 0;xx<obj.size();xx++)
						{
						if(currentdeptId==obj.get(xx).getDept_id())
						 {
						 %>
                                
							    <option  value="<%=obj.get(xx).getDept_id()%>" selected><%=obj.get(xx).getDept_title()%></option>
						 <% }
						 else
						 {%>
							  <option  value="<%=obj.get(xx).getDept_id()%>"><%=obj.get(xx).getDept_title()%></option>
						 <% }
						}
						%>
						
						<!--       <option value="-1" />none</option>  
							<html:options collection="levels"  property="designation_id"  labelProperty="designation_title" /> -->
  						</html:select>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="levelMsg"></td> 
 
<tr>
				<td ></td>
				<td colspan="2" >
				<div id='tab2' style="display:none" >
					<table>
						<tr>
			    			<td class="tdLabel_style" align=center><bean:message key="app.sch.addeduinstituestaff.allclasses"/><br>
							 	<select name="allclasses" Id="allclasses" size=8 multiple="multiple" style="width: 200px; background-color:#EEEEEE; border:1px solid #000000; font-size : 11px; font-family : Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight : normal;"></select>
							</td>
							<td>
								<a href="javascript:assignClasses();">
									<img src="./images/moveright.gif" border="0">
								</a>
								<br>
								<a href="javascript:moveClasses();">
									<img src="./images/moveleft.gif" border="0">
								</a>	
							</td>	
				            <td class="tdLabel_style" align=center><bean:message key="app.sch.addeduinstituestaff.assignedclasses"/><br>
								<select name="selectedclasses" Id="selectedclasses" size=8 multiple="multiple"  style=" width: 200px; background-color:#FFFFFF; border:1px solid #000000; font-size : 11px; font-family : Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight : normal;"></select>
								
							</td>
						</tr>
						<tr >
							<td></td>
							<td></td>
							<td>
								<img src="./images/blank.png" alt="" id="zipcodeError"/><div style ='display:none; color: red;' id="zipcodeMsg">
							</td>
						</tr>
					</table>
					</div>
				</td>
				<td ></td>
			</tr>
			
			
<tr>
				<td ></td>
				<td colspan="2" >
				<div id='subjecttab' style="display:none" >
					<table>
						<tr>
			    			<td class="tdLabel_style" align=center><bean:message key="app.sch.addeduinstituestaff.allsubjects"/><br>
							 	<select name="allsubjects" Id="allsubjects" size=8 multiple="multiple" style="width: 200px; background-color:#EEEEEE; border:1px solid #000000; font-size : 11px; font-family : Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight : normal;"></select>
							</td>
							<td>
								<a href="javascript:assignSubjects();">
									<img src="./images/moveright.gif" border="0">
								</a>
								<br>
								<a href="javascript:moveSubjects();">
									<img src="./images/moveleft.gif" border="0">
								</a>	
							</td>	
				            <td class="tdLabel_style" align=center><bean:message key="app.sch.addeduinstituestaff.assignedsubjects"/><br>
								<select name="selectedsubjects" Id="selectedsubjects" size=8 multiple="multiple"  style=" width: 200px; background-color:#FFFFFF; border:1px solid #000000; font-size : 11px; font-family : Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight : normal;"></select>
								
							</td>
						</tr>
						<tr >
							<td></td>
							<td></td>
							<td>
								<img src="./images/blank.png" alt="" id="zipcodeError"/><div style ='display:none; color: red;' id="zipcodeMsg">
							</td>
						</tr>
					</table>
					</div>
				</td>
				<td ></td>
			</tr>

      
</table>


  <table style="" align="center" cellspacing=2 cellpadding=5 width=97%  > 
   <tr>   <td colspan="2" align="center" >
            <input type="submit" value ="Submit" onclick="return checkValidation();"/>
            <input type="button" value ="Cancel"  onclick="javascript:history.back();" />
            </td>
        
        </tr>
      </table>
     </div> 
<html:hidden property="userid" styleClass="srchbox" style="border:1px solid #000000" value = "<%=struserid %>" />
 
    </html:form>


</body>
</html>
<script>
function formsubmit()
{
	var selAsso = document.getElementById("selectedclasses");
	var plen = selAsso.length;
	for(var j=plen-1; j>=0; j--)
	{
		selAsso.options[j].selected=true;
	}

	var selSub = document.getElementById("selectedsubjects");
	var slen = selSub.length;
	for(var j=slen-1; j>=0; j--)
	{
		selSub.options[j].selected=true;
	}
}	

 function getSubjectByDept()
 {
 	var allsubjects = document.getElementById('allsubjects');
	var selectedsubjects = document.getElementById('selectedsubjects');
	var deptId = document.getElementById('department').value;
	var userId = '<%=request.getAttribute("userid")%>';
	removeOptions(allsubjects);
	removeOptions(selectedsubjects);
	EduStaffManager.getSelectedSubject(userId,deptId,function(associatedsubject){
		for(var i=0;i<associatedsubject.length;i++)
			{
				var selectedUserOptions = associatedsubject[i];
				selectedUserOptions = selectedUserOptions.split(',');
				selectedsubjects[i] = new Option(selectedUserOptions[1],selectedUserOptions[0]);
			}
 		   });
	
 	EduStaffManager.getSubjectByDept(deptId,function(userlist){
			for(var i=0;i<userlist.length;i++)
 			{
 				var userOptions = userlist[i];
				userOptions = userOptions.split(',');
				allsubjects[i] = new Option(userOptions[1],userOptions[0]);
 			}
 			});
 }
getSubjectByDept();
 
function getClassInfo()
 {
 	var allclasses = document.getElementById('allclasses');
	var selectedclasses = document.getElementById('selectedclasses');
	var userId = '<%=request.getAttribute("userid")%>';
	var assodesignation_id = '<%=request.getAttribute("assodesignation_id")%>';
	var deptId = document.getElementById('department').value;
	removeOptions(allclasses);
	removeOptions(selectedclasses);
	EduStaffManager.getSelectedClass(userId,deptId,function(associatedclass){
		for(var i=0;i<associatedclass.length;i++)
			{
				var selectedUserOptions = associatedclass[i];
				selectedUserOptions = selectedUserOptions.split(',');
				selectedclasses[i] = new Option(selectedUserOptions[1],selectedUserOptions[0]);
			}
 		   });
		   EduStaffManager.getClassInfoByDepartment(deptId,function(userlist){
			for(var i=0;i<userlist.length;i++)
 			{
			    var cval=0;
 				var userOptions = userlist[i];
				userOptions = userOptions.split(',');
				allclasses[i] = new Option(userOptions[1],userOptions[0]);
			}
			});
			if(assodesignation_id==3)
			{
				document.getElementById('tab2').style.display='inline';
				document.getElementById('subjecttab').style.display='inline';
				document.getElementById('dept_tab').style.display='';
			}
 }
  getClassInfo();
  
  function removeOptions(allclasses)
{
	var i;
	for(i=allclasses.options.length-1;i>=0;i--)
	{
		allclasses.remove(i);
	}
}
  YAHOO.util.Event.onDOMReady(function(){

        var Event = YAHOO.util.Event,
            Dom = YAHOO.util.Dom,
            dialog,
            calendar;

        var showBtn = Dom.get("show");
          Event.on(showBtn, "click", function() {
           // Lazy Dialog Creation - Wait to create the Dialog, and setup document click listeners, until the first time the button is clicked.
            if (!dialog) {
               // Hide Calendar if we click anywhere in the document other than the calendar
                Event.on(document, "click", function(e) {
                    var el = Event.getTarget(e);
                    var dialogEl = dialog.element;
                    if (el != dialogEl && !Dom.isAncestor(dialogEl, el) && el != showBtn && !Dom.isAncestor(showBtn, el)) {
                        dialog.hide();
                    }
                });
                function resetHandler() {
                    // Reset the current calendar page to the select date, or 
                    // to today if nothing is selected.
                    var selDates = calendar.getSelectedDates();
                    var resetDate;
        
                    if (selDates.length > 0) {
                        resetDate = selDates[0];
                    } else {
                        resetDate = calendar.today;
                    }
        
                    calendar.cfg.setProperty("pagedate", resetDate);
                    calendar.render();
                }
        
                function closeHandler() {
                    dialog.hide();
                }
         
                dialog = new YAHOO.widget.Dialog("container", {
                    visible:false,
                    context:["show", "tl", "bl"],
                    draggable:false,
                    close:true
                });
                dialog.setHeader('Pick A Joining Date');
                dialog.setBody('<div id="cal"></div>');
                dialog.render(document.body);

                dialog.showEvent.subscribe(function() {
                    if (YAHOO.env.ua.ie) {
                        // Since we're hiding the table using yui-overlay-hidden, we 
                        // want to let the dialog know that the content size has changed, when
                        // shown
                        dialog.fireEvent("changeContent");
                    }
                });
            }

            // Lazy Calendar Creation - Wait to create the Calendar until the first time the button is clicked.
            if (!calendar) {

                calendar = new YAHOO.widget.Calendar("cal", {
                    iframe:false,          // Turn iframe off, since container has iframe support.
                    hide_blank_weeks:true  // Enable, to demonstrate how we handle changing height, using changeContent
                });
                calendar.render();

                calendar.selectEvent.subscribe(function() {
                    if (calendar.getSelectedDates().length > 0) {

                        var selDate = calendar.getSelectedDates()[0];

                        // Pretty Date Output, using Calendar's Locale values: Friday, 8 February 2008
                        var wStr = calendar.cfg.getProperty("WEEKDAYS_LONG")[selDate.getDay()];
                        var dStr = selDate.getDate();
                        var mStr = calendar.cfg.getProperty("MONTHS_LONG")[selDate.getMonth()];
                        var yStr = selDate.getFullYear();
        				var month = selDate.getMonth();
        				month = month+1;
        				//var dt =dStr+"-"+month+"-"+yStr;
						var dt =yStr+"-"+month+"-"+dStr;
                        Dom.get("joiningdate").value = dt;
                    } else {
                        Dom.get("joiningdate").value = "";
                    }
                    dialog.hide();
                });

                calendar.renderEvent.subscribe(function() {
                    // Tell Dialog it's contents have changed, which allows 
                    // container to redraw the underlay (for IE6/Safari2)
                    dialog.fireEvent("changeContent");
                });
            }

            var seldate = calendar.getSelectedDates();

            if (seldate.length > 0) {
                // Set the pagedate to show the selected date if it exists
                calendar.cfg.setProperty("pagedate", seldate[0]);
                calendar.render();
            }

            dialog.show();
        });
	});

</script>
