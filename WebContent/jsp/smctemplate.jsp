<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html>
<head>
    <title><tiles:getAsString name="title" ignore="true"/></title>
	<link href="styles/default.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="./yui/build/fonts/fonts-min.css" />
	<link rel="stylesheet" type="text/css" href="./yui/build/container/assets/skins/sam/container.css" />
	<link rel="stylesheet" type="text/css" href="./yui/build/datatable/assets/skins/sam/datatable.css" />
	<link rel="stylesheet" type="text/css" href="./yui/build/tabview/assets/skins/sam/tabview.css" />
	<link rel="stylesheet" type="text/css" href="./yui/build/paginator/assets/skins/sam/paginator.css" />
	<link rel="stylesheet" type="text/css" href="./yui/build/calendar/assets/skins/sam/calendar.css" />
	<link rel="stylesheet" type="text/css" href="./yui/build/menu/assets/skins/sam/menu.css"/>
	
	<script type="text/javascript" src="./yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
	<script type="text/javascript" src="./yui/build/container/container_core.js"></script>
	 <script type="text/javascript" src="./yui/build/menu/menu.js"></script>
</head>
  <body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" class="yui-skin-sam">

  <table width="90%"  align="center" style="border:1px dotted #666666;" cellspacing="0" cellpadding="0">
 	 <tr>
			<td style="width:10em;">
			<img src="./images/imagesCA4CN5KS.jpg" width="150%" height="120" alt="What We Do" align="left"/>
			</td>
			<td style="width:90em;">
			<img src="./images/schoolname.gif" width="94%" height="120" alt="What We Do" align="right"/>
			</td>			
	</tr>
 <tr>
  <td align="left" colspan="2" >
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr><td ><tiles:insert attribute="topfrontnav"/></td>
		</tr>
				
    </table>
  </td>
</tr> 
 	<tr>
		<td colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				
				<td align="center" style="background-color: #F5F5F5;">
				<div >
					<tiles:insert attribute="body"/>
					<tiles:insert attribute="login"/>
				</div>
				</td>
				
				</tr>
			</table>
		</td>
	</tr>
	

<tr>
	<td colspan="2">
		<tiles:insert attribute="footer"/>
	</td>
  </tr>
</table>

  </body>

</html>