<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<script type='text/javascript' src="/schmangmt/dwr/interface/ConcernTrackingManager.js"></script>
<script type="text/javascript">

function checkValidation()
  	 {   
 	   var  flag = true;
 	   var  issuetitleflg = true;
 	   var  issuedescflg = true;
 	   var  categoryflg = true;
 	   var  priorityflg=true;
 	   var issuetitle = document.getElementById('issuetitle').value;
	   var issuedesc = document.getElementById('issuedesc').value;
	   var category = document.getElementById('category').value;
	   var priority = document.getElementById('priority').value;

	   //Shop Name validation
	   if(issuetitle!='')
	    {
		  document.getElementById('showNameError').style.display='none';
	      document.getElementById('issuetitleMsg').style.display = 'none';
	      issuetitleflg=true;
	  }
	  else
	  { 
		over("showNameError","error_bang.gif");
        document.getElementById('showNameError').style.display='inline';
        document.getElementById('issuetitleMsg').innerHTML ='Issue Title Should Not be Blank';
        document.getElementById('issuetitleMsg').style.display = 'inline';
        issuetitleflg = false;
	  }
	 //Street Address validation
	   if(issuedesc!='')
	    {
		    
		  document.getElementById('showStreetAddError').style.display='none';
	      document.getElementById('shopstreetAddMsg').style.display = 'none';
	      issuedescflg = true;
		      
	  }
	  else
	  { 
	   over("showStreetAddError","error_bang.gif");
       document.getElementById('showStreetAddError').style.display='inline';
       document.getElementById('shopstreetAddMsg').innerHTML ='Issue Discription Should Not be Blank';
       document.getElementById('shopstreetAddMsg').style.display = 'inline';
       issuedescflg = false;
	  }	

	  if(category!='Select Category')
	  {
		   document.getElementById('showStreetAdd2Error').style.display='none';
		   document.getElementById('shopstreetAdd2Msg').style.display = 'none'; 
		   categoryflg = true;
	  }
	  else
	  { 
	   over("showStreetAdd2Error","error_bang.gif");
      document.getElementById('showStreetAdd2Error').style.display='inline';
      document.getElementById('shopstreetAdd2Msg').innerHTML ='Select the Category';
      document.getElementById('shopstreetAdd2Msg').style.display = 'inline';
      categoryflg = false;
	  }		  
	    
//Phone No. validation
      if(priority!='Select Priority')
	    {
		  document.getElementById('showPhoneNoError').style.display='none';
	      document.getElementById('phoneMsg').style.display = 'none';
	      priorityflg=true;
		   
	  }
	  else
	  { 
		over("showPhoneNoError","error_bang.gif");
        document.getElementById('showPhoneNoError').style.display='inline';
        document.getElementById('phoneMsg').innerHTML ='Select Priority';
        document.getElementById('phoneMsg').style.display = 'inline';
        priorityflg = false;
	  }          
      if(issuetitleflg && issuedescflg  && priorityflg && categoryflg)
       {
          flag=true;
       }
      else{
          flag=false;
      }
      return flag; 
   }
  

</script>
      <html:form  action="/ConcernTrackingAction.do?operation=save" onsubmit="return checkValidation();" >
       
       <table align="center" width="97%" cellpadding="5" cellspacing="0">
	   <tr><td class="mainhead" >&nbsp;&nbsp;<bean:message key="app.sch.addconcerntracking.newconcern"/>
	   </td></tr>
       </table>
      <table   align="center" cellspacing="2" cellpadding="5" width=97% border="0" style="border: 1px solid;">
       <!-- <tr>
          <td align=left width=1%>&nbsp;&nbsp;&nbsp;Shop Code</td>
          <td><html:text property="shopcode" size="30" styleClass="srchbox" style="border:1px solid #000000" value =""/></td>
        </tr>-->
        <tr>
          <td align=left width="15"><img src="./images/blank.png" alt="" id="showNameError"/></td>
          <td align=left >&nbsp;&nbsp;<bean:message key="app.sch.addconcerntracking.concernTitle"/></td>
          <td><html:text property="issuetitle" styleId ="issuetitle" size="30" styleClass="srchbox" style="border:1px solid #000000" value ="" />&nbsp;&nbsp;&nbsp;<div  style ='display:none; color: red;' id="issuetitleMsg"></div></td>
        </tr>
        <tr>
          <td align=left ><img src="./images/blank.png" alt="" id="showStreetAddError"/></td>
          <td align=left valign="top">&nbsp;&nbsp;<bean:message key="app.sch.addconcerntracking.concernDesc"/></td>
          <td><html:textarea property="issuedesc" styleId="issuedesc" style="border:1px solid #000000" value ="" rows="5" cols="40"/>&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="shopstreetAddMsg" ></td>
        </tr>
        <tr>
          <td align=left ><img src="./images/blank.png" alt="" id="showStreetAdd2Error"/></td>
          <td align=left >&nbsp;&nbsp;<bean:message key="app.sch.addconcerntracking.concernCategory"/></td>
          <td>
          <html:select property="category" size="1"  styleId="category"   style="border:1px solid #000000" >
		  </html:select>
          &nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="shopstreetAdd2Msg"></td>
		 </tr>
		  <tr>
          <td align=left ><img src="./images/blank.png" alt="" id="showPhoneNoError"/></td>
          <td align=left >&nbsp;&nbsp;<bean:message key="app.sch.addconcerntracking.concernPriority"/></td>
          <td width=85%><html:select property="priority" styleId="priority" size="1"  style="border:1px solid #000000" >
            <option value="Select Priority">Select Priority</option>
			<option value="High">High</option>
			<option value="Medium">Medium</option>
			<option value="Low">Low</option>			
          </html:select>
			&nbsp;&nbsp;&nbsp;<div style ='display:none; color: red;' id="phoneMsg"></td>
         </tr>
         
          <tr>
           <td></td>
           <td colspan="2" align="right">
            <html:submit  />
            <input type="button" value ="Cancel" onclick ="javascript:history.back();" />
            
          </td>
        </tr>
      </table>
   </html:form> 
<br>
<script language="JavaScript">
loadProperty();
function loadProperty()
{
	var categoryoption = document.getElementById("category");
	ConcernTrackingManager.getAllCategorys(function(category){
			for(var xx=0;xx<category.length;xx++)
			{
				var catidvalue = category[xx].split(",");
				categoryoption[xx]= new Option(catidvalue[1],catidvalue[0]); 						
			}
			 
    	});
   
}
</script>

