<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">  
<html lang="en" xmlns="http://www.w3.org/1999/xhtml"> 
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="Description" content="jQuery Timepicker Addon.  Add a timepicker to your jQuery UI Datepicker.  With options to show only time, format time, and much more." />
		<meta name="Keywords" content="jQuery, UI, datepicker, timepicker, datetime, time, format" />
		
		<style type="text/css"> 
		pre{
				margin:0;
				padding:0;
				border:0;
			}
			pre{ font-size: 12px; line-height: 16px; padding: 5px 5px 5px 10px; margin: 10px 0; background-color: #e4f4d4; border-left: solid 5px #9EC45F; overflow: auto; tab-size: 4; -moz-tab-size: 4; -o-tab-size: 4; -webkit-tab-size: 4; }

			#tabs{ margin: 20px -20px; border: none; }
			#tabs, #ui-datepicker-div, .ui-datepicker{ font-size: 85%; }
							
			.example-container{ background-color: #ffffff; border-bottom: solid 2px #ffffff; margin: 0 0 20px 40px; padding: 20px; }
			.example-container input{ border: solid 1px #aaa; padding: 4px; width: 175px; }
			
		</style> 
		
        <script type="text/javascript" src="./jquery-ui-1.10.3/ui/jquery-1.10.2.js"></script>
		<script type="text/javascript" src="./jquery-ui-1.10.3/ui/jquery-ui.js"></script>
		<script type="text/javascript" src="./jquery-ui-1.10.3/ui/jquery-ui-timepicker-addon.js"></script>
		<script type="text/javascript" src="./jquery-ui-1.10.3/ui/jquery-ui-sliderAccess.js"></script>
		<link rel="stylesheet" media="all" type="text/css" href="./jquery-ui-1.10.3/themes/base/jquery-ui.css" />
		<link rel="stylesheet" media="all" type="text/css" href="./jquery-ui-1.10.3/themes/base/jquery-ui-timepicker-addon.css" />
	</head> 
	<body> 
<div id="tp-examples">
	<div class="example-container">
		<p><b>Meeting date and time:</b></p>
		<div>
	 		<input type="text" name="basic_example_3" id="basic_example_3" value="" />
		</div>
		
<pre style="display: none">
$('#basic_example_3').datetimepicker({
	timeFormat: "hh:mm tt"
});
</pre>

	</div>

</div>

		<script type="text/javascript">
			
			$(function(){
				$('#tabs').tabs();
		
				$('.example-container > pre').each(function(i){
					eval($(this).text());
				});
			});
			
		</script>

	</body> 
</html>