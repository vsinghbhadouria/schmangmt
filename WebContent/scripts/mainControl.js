var moPSControlInfo = null;

function getPSControlInfo() {
	if (self == getHeaderFrame()) {
		return getDataFrame().getPSControlInfo();
	}

	if (moPSControlInfo == null) {
		moPSControlInfo = new PSControlInfo();
		moPSControlInfo.read();
	}
	return moPSControlInfo;
}

function PSControlInfo() {
	this.msAction = null;
	this.moActionElementId = null;
	this.msSessionId = null;
	this.msTimeStamp = null;
	this.maDataSection = null;
	this.miScreenMode = -999;
	this.miControlFlags = 0;

	var loDocument = document;
	if (self == getHeaderFrame()) {
		loDocument = getDataDocument();
	}
	//alert('loDocument = ' + loDocument);
	//alert('loDocument.DataForm = ' + loDocument.DataForm);

	this.moForm = loDocument.DataForm;
}

PSControlInfo.mcSessionId = 'A';
PSControlInfo.mcTimeStamp = 'B';
PSControlInfo.mcAction = 'C';
PSControlInfo.mcActionElementId = 'D';
PSControlInfo.mcDataSection = 'E';
PSControlInfo.mcScreenMode = 'F';
PSControlInfo.mcControlFlags = 'G';

PSControlInfo.prototype.clone = function() {
	var loPSControlInfo = new PSControlInfo();
	alert('in control');
	loPSControlInfo.msAction = this.msAction;
	loPSControlInfo.moActionElementId = this.moActionElementId;
	loPSControlInfo.msSessionId = this.msSessionId;
	loPSControlInfo.msTimeStamp = this.msTimeStamp;
	loPSControlInfo.maDataSection = this.maDataSection;
	loPSControlInfo.miScreenMode = this.miScreenMode;
	loPSControlInfo.miControlFlags = this.miControlFlags;
	loPSControlInfo.moForm = this.moForm;
	return loPSControlInfo;
}

PSControlInfo.prototype.read = function() {
	var lsControlInfo = this.moForm.control_info.value;
	// this.parseDelimitedText(lsControlInfo);
}

PSControlInfo.prototype.submitForm = function() {
	var loForm = this.moForm;
	var lsControlInfo = this.moForm.control_info.value;
	// var lsControlInfo = this.getDelimitedText();
	loForm.control_info.value = lsControlInfo;
	// if (moPSLayoutInfo != null)
	// {
	// var lsLayoutInfo = moPSLayoutInfo.getDelimitedText();
	// loForm.layout_info.value = lsLayoutInfo;
	// }
	loForm.action = "PSController";
	loForm.method = "post";
	// loForm.ownerDocument.body.style.cursor = "wait";
	// alert(loForm.outerHTML);
	loForm.submit();
}

function getDataFrame() {
	return top.DataFrame;
}

function getHeaderFrame() {
	return top.HeaderFrame;
}

function getDataDocument() {
	return getDataFrame().document;
}

function performAction(fsElementId) {
	var lsAction = null;
	alert('fsElementId = ' + fsElementId);
	moPSControlInfo = new PSControlInfo();
	if (arguments.length > 1) {
		// lsAction = "";
		// for (var liCtr = 1; liCtr < arguments.length; liCtr++)
		// {
		// if (liCtr > 1) lsAction += AppConstants.DELIM_SUB;
		// lsAction += arguments[liCtr];
		// }
	}
	submitRequest(lsAction);
}

function submitRequest(fsAction) {
	// prepareControlInfo(fsElementId, fsAction);
	// appendMonitor("Submit Form");
	moPSControlInfo.submitForm();
}