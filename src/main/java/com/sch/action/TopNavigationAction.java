package com.sch.action;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.sch.delegates.*;
import com.sch.form.TopNavigationForm;
import com.sch.to.TopNavigationTO;


public class TopNavigationAction extends DispatchAction{
	public static Logger logger  = Logger.getLogger(TopNavigationAction.class);
	public ActionForward search(ActionMapping mapping, ActionForm form, 
		HttpServletRequest request,HttpServletResponse response) throws Exception{
		TopNavigationForm TopNavigationForm = (TopNavigationForm)form;
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		try
		{	HttpSession session =  request.getSession(false);
			TopNavigationManager TopNavigationManager = new TopNavigationManager(); 
			logger.debug("call the getAllMenu method passing the ug_id==>"+session.getAttribute("ug_id"));
			ArrayList<TopNavigationTO> fullMenuList = TopNavigationManager.getAllMenu((String)session.getAttribute("ug_id")!=null?Integer.valueOf((String)session.getAttribute("ug_id")):1);
			logger.debug("getAllMenu method process completed...");
			TopNavigationForm.setListfullMenu(fullMenuList);
		}catch(Exception exp){exp.printStackTrace();}
		return mapping.findForward("search");
	}

}
