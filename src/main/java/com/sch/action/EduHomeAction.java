package com.sch.action;

import javax.servlet.http.*;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
public class EduHomeAction extends DispatchAction
{	
	
	public ActionForward search(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		return mapping.findForward("search");
	}
	public ActionForward home(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		return mapping.findForward("home");
	}
	
	public ActionForward aboutUs(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		return mapping.findForward("aboutUs");
	}

	public ActionForward lifeStyles(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		return mapping.findForward("lifeStyles");
	}

	public ActionForward events(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		return mapping.findForward("events");
	}

	public ActionForward contactUs(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		return mapping.findForward("contactUs");
	}

}
