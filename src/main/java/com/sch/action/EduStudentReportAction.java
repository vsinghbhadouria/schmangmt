package com.sch.action;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.*;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.sch.delegates.EduStudentManager;
import com.sch.delegates.EduStudentReportManager;
import com.sch.delegates.LoginManager;
import com.sch.form.EduStudentForm;
import com.sch.form.EduStudentReportForm;
import com.sch.to.EduStaffTO;
import com.sch.to.EduStudentReportTO;
import com.sch.to.EduStudentTO;
import com.sch.constant.PortableConstant;
public class EduStudentReportAction extends DispatchAction
{	
	private int f_Id = 2;
	LoginManager loginManager =  new LoginManager();
    public static Logger logger=Logger.getLogger(EduStudentReportAction.class); 
	public ActionForward search(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null){
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("search");
	}
	
	public ActionForward reportList(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null){
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("reportList");
	}
	
	public ActionForward createSetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		logger.debug("Setup in process for Student Report Card............ ");
		EduStudentReportForm eduStudentReportFormObj = (EduStudentReportForm)form;
		EduStudentReportManager eduStudentManagerObj = new EduStudentReportManager();
		EduStudentReportTO eduStudentReportToObj = new EduStudentReportTO();
		eduStudentReportToObj = eduStudentManagerObj.getEduStudentDatabyUserId(request.getParameter("User_id"),request.getParameter("Session_id"));
		eduStudentReportFormObj.setStudentid(eduStudentReportToObj.getStudentid());
		eduStudentReportFormObj.setStudentname(eduStudentReportToObj.getStudentname());
		eduStudentReportFormObj.setDateofbirth(eduStudentReportToObj.getDateofbirth());
		eduStudentReportFormObj.setDepartmentname(eduStudentReportToObj.getDepartmentname());
		eduStudentReportFormObj.setFathername(eduStudentReportToObj.getFathername());
		eduStudentReportFormObj.setClassid(eduStudentReportToObj.getClassid());
		eduStudentReportFormObj.setSessionid(eduStudentReportToObj.getSessionid());
		eduStudentReportFormObj.setDept_id(eduStudentReportToObj.getDept_id());
		eduStudentReportFormObj.setSession_year(eduStudentReportToObj.getSession_year());
		request.setAttribute("studentname",eduStudentReportToObj.getStudentname());
		request.setAttribute("studentid", eduStudentReportToObj.getStudentid());
		request.setAttribute("dateofbirth", eduStudentReportToObj.getDateofbirth());
		request.setAttribute("fathername", eduStudentReportToObj.getFathername());
		request.setAttribute("classname", eduStudentReportToObj.getClassname());
		request.setAttribute("classid", eduStudentReportToObj.getClassid());
		request.setAttribute("departmentname", eduStudentReportToObj.getDepartmentname());
		request.setAttribute("dept_id", eduStudentReportToObj.getDept_id());
		
		logger.debug("Setup completed for Student Report Card............ "+eduStudentReportToObj.getDept_id());
		return mapping.findForward("createSetup");
	}
	public ActionForward saveResult(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{

		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		try {       
		    	  
				  EduStudentReportForm eduStudentReportFormObj = (EduStudentReportForm)form;  
		          EduStudentReportManager eduStudentReportManagerObj = new EduStudentReportManager();
		          EduStudentReportTO eduStudentReportTOObj = new EduStudentReportTO();
		          eduStudentReportTOObj.setStudentid(eduStudentReportFormObj.getStudentid());
		          eduStudentReportTOObj.setClassid(eduStudentReportFormObj.getClassid());
		          eduStudentReportTOObj.setSessionid(eduStudentReportFormObj.getSessionid());
		          eduStudentReportTOObj.setDept_id(eduStudentReportFormObj.getDept_id());
		          eduStudentReportTOObj.setTotalOutOf(Integer.valueOf(request.getParameter("totaloutof")));
		          eduStudentReportTOObj.setTotalMarkObtained(Float.valueOf(request.getParameter("markObtained")));
		          eduStudentReportTOObj.setPercentage(Float.valueOf(request.getParameter("percentage")));
		          eduStudentReportTOObj.setDivision(request.getParameter("division"));
		          eduStudentReportTOObj.setResult(request.getParameter("result"));
		          System.out.println("studentId "+eduStudentReportFormObj.getStudentid()+"ClassId "+eduStudentReportFormObj.getClassid()+"SessionId"+eduStudentReportFormObj.getSessionid()+"Session_year"+eduStudentReportFormObj.getSession_year()+"sUBJECTcOUNT"+eduStudentReportFormObj.getSubject_count()+"Department Id "+eduStudentReportFormObj.getDept_id());
		          System.out.println("subjectId "+request.getParameter("sub"+1)+"Outof "+request.getParameter("outof"+1)+"markObtained "+request.getParameter("markObtained"+1));
		          ArrayList<EduStudentReportTO> markObj = new ArrayList<EduStudentReportTO>();
		          for(int count=1;count<=eduStudentReportFormObj.getSubject_count();count++){
		        	  EduStudentReportTO eduStudentReportToObj = new EduStudentReportTO(); 
		        	  eduStudentReportToObj.setSubjectid(new Integer(request.getParameter("sub"+count)));
		        	  eduStudentReportToObj.setOutOf(new Integer(request.getParameter("outof"+count)));
		        	  eduStudentReportToObj.setMarkObtained(new Float(request.getParameter("markObtained"+count)));
		        	  markObj.add(eduStudentReportToObj);
		          }
		          
		          logger.debug("Before call of saveEduStudentReportData............ ");
		          eduStudentReportManagerObj.saveEduStudentReportData(markObj,eduStudentReportTOObj);
		          logger.debug("After call of saveEduStudentReportData............ "); 
		      }
		    catch ( Exception exp ){
		    	exp.getMessage();
		    	logger.debug("Method save :: ERROR "+exp.getMessage());	
		    	}
		return mapping.findForward("saveResult");
	}

	public ActionForward updateSetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null){
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		try
		{
		 logger.debug("updateSetup in process for student............ ");	
		 PortableConstant PortableConstantObj = new PortableConstant();
		 Calendar cal = Calendar.getInstance();
		 DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 EduStudentForm eduStudentFormObj = (EduStudentForm)form;
	     EduStudentManager eduStudentManagerObj = new EduStudentManager();
	     System.out.println(request.getParameter("User_id")+","+request.getParameter("Session_id"));
	     EduStudentTO eduStudentToObj = eduStudentManagerObj.getEduStudentDatabyUserId(request.getParameter("User_id"),request.getParameter("Session_id")); 
	     eduStudentFormObj.setFirstname(eduStudentToObj.getFirstname());
	     eduStudentFormObj.setStudentid(eduStudentToObj.getStudentid());
	     eduStudentFormObj.setLastname(eduStudentToObj.getLastname());
	     eduStudentFormObj.setPhoneno(eduStudentToObj.getPhoneno());
	     eduStudentFormObj.setAddress(eduStudentToObj.getAddress());
	     eduStudentFormObj.setAdmissiondate(eduStudentToObj.getAdmissiondate());
	     eduStudentFormObj.setMiddlename(eduStudentToObj.getMiddlename());
	     eduStudentFormObj.setEmailid(eduStudentToObj.getEmailid());
	     eduStudentFormObj.setFathername(eduStudentToObj.getFathername());
	     eduStudentFormObj.setMothername(eduStudentToObj.getMothername());
	     eduStudentFormObj.setParentemailid(eduStudentToObj.getParentemailid());
	     eduStudentFormObj.setClassid(eduStudentToObj.getClassid());
	     eduStudentFormObj.setDateofbirth(eduStudentToObj.getDateofbirth());
	     eduStudentFormObj.setBirthday(eduStudentToObj.getBirthday());
	     eduStudentFormObj.setBirthmonth(eduStudentToObj.getBirthmonth());
	     eduStudentFormObj.setBirthyear(eduStudentToObj.getBirthyear());
	     eduStudentFormObj.setGender(eduStudentToObj.getGender());
	     eduStudentFormObj.setComment(eduStudentToObj.getComment());
	     eduStudentFormObj.setAdmissionfee(eduStudentToObj.getAdmissionfee());
	     eduStudentFormObj.setConcessionamt(eduStudentToObj.getConcessionamt());
	     eduStudentFormObj.setPreviousinstitute(eduStudentToObj.getPreviousinstitute());
	     eduStudentFormObj.setParentqualification_id(eduStudentToObj.getParentqualification_id());
	     eduStudentFormObj.setAdmissiondate(eduStudentToObj.getAdmissiondate());
		 ArrayList<EduStudentTO> studprequalifications = eduStudentManagerObj.getStudentQualification();
		 eduStudentFormObj.setStudprequalifications(studprequalifications);
		 request.setAttribute("studprequalifications",studprequalifications);
		 request.setAttribute("studprequalification_id",eduStudentToObj.getStudprequalification_id());
		 ArrayList<EduStudentTO> parentqualifications = eduStudentManagerObj.getParentHigherQualification();
		 eduStudentFormObj.setParentqualifications(parentqualifications);
		 request.setAttribute("parentqualifications",parentqualifications);
		 request.setAttribute("parentqualification_id",eduStudentToObj.getParentqualification_id());
		 ArrayList<EduStudentTO> classes = eduStudentManagerObj.getClassInfo(request.getParameter("User_id"));
	 	 eduStudentFormObj.setClasses(classes);
		 request.setAttribute("classes",classes);
		 request.setAttribute("classid",eduStudentToObj.getClassid());
		 ArrayList<EduStudentTO> admissionfees = eduStudentManagerObj.getAdmissionfees();
		 eduStudentFormObj.setAdmissionfees(admissionfees);
		 request.setAttribute("admissionfees",admissionfees);
		 request.setAttribute("startyear",PortableConstantObj.getProperty(PortableConstant.DATEPICKERSTARTYEAR));
		 request.setAttribute("currentyear",PortableConstantObj.getProperty(PortableConstant.DATEPICKERCURRENTYEAR));
		 request.setAttribute("birthdate", (eduStudentFormObj.getBirthmonth()+1)+"/"+eduStudentFormObj.getBirthday()+"/"+eduStudentFormObj.getBirthyear());
		 cal.setTime(dateFormat.parse(eduStudentFormObj.getAdmissiondate()));
		 cal.add(Calendar.MONTH,1);
		 request.setAttribute("admissiondate",dateFormat.format(cal.getTime()));
		 ArrayList<EduStudentTO> departments = eduStudentManagerObj.getDepartment();
		 eduStudentFormObj.setDepartments(departments);
		 request.setAttribute("deptid",eduStudentToObj.getDept_id());
		 request.setAttribute("departments",departments);
		 request.setAttribute("userid",eduStudentToObj.getStudentid());
		 logger.debug("with in updateSetup>>>> birthdate"+eduStudentFormObj.getBirthmonth()+"/"+eduStudentFormObj.getBirthday()+"/"+eduStudentFormObj.getBirthyear()+"admissiondate"+eduStudentFormObj.getAdmissiondate());	
		 logger.debug("updateSetup is completed for student............ ");	
	   }
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method updateSetup :: ERROR "+exp.getMessage());	
		}
		 return mapping.findForward("updateSetup");
	}

   public ActionForward update(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
   {
	   if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
	   String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
	   EduStudentForm eduStudentFormObj = (EduStudentForm)form;
	   EduStudentManager eduStudentManagerObj = new EduStudentManager();
	   EduStudentTO eduStudentToObj = new EduStudentTO();
	   try
	   {
	   	 eduStudentToObj.setFirstname(eduStudentFormObj.getFirstname());
	     eduStudentToObj.setLastname(eduStudentFormObj.getLastname());
	     eduStudentToObj.setPhoneno(eduStudentFormObj.getPhoneno());
	     eduStudentToObj.setAddress(eduStudentFormObj.getAddress());
	     eduStudentToObj.setAdmissiondate(eduStudentFormObj.getAdmissionmonth()+"/"+eduStudentFormObj.getAdmissionday()+"/"+eduStudentFormObj.getAdmissionyear());
	     eduStudentToObj.setMiddlename(eduStudentFormObj.getMiddlename());
	     eduStudentToObj.setEmailid(eduStudentFormObj.getEmailid());
	     eduStudentToObj.setFathername(eduStudentFormObj.getFathername());
	     eduStudentToObj.setMothername(eduStudentFormObj.getMothername());
	     eduStudentToObj.setParentemailid(eduStudentFormObj.getParentemailid());
	     eduStudentToObj.setEduclass(eduStudentFormObj.getEduclass());
	     eduStudentToObj.setDateofbirth(eduStudentFormObj.getDateofbirth());
         eduStudentToObj.setBirthday(eduStudentFormObj.getBirthday());
         eduStudentToObj.setBirthmonth(eduStudentFormObj.getBirthmonth());
         eduStudentToObj.setBirthyear(eduStudentFormObj.getBirthyear());
	     eduStudentToObj.setGender(eduStudentFormObj.getGender());
	     eduStudentToObj.setComment(eduStudentFormObj.getComment());
	     eduStudentToObj.setAdmissionfee(eduStudentFormObj.getAdmissionfee());
	     eduStudentToObj.setConcessionamt(eduStudentFormObj.getConcessionamt());
	     eduStudentToObj.setPreviousinstitute(eduStudentFormObj.getPreviousinstitute());
	     eduStudentToObj.setParentqualification(eduStudentFormObj.getParentqualification());
	     eduStudentToObj.setStudprequalification(eduStudentFormObj.getStudprequalification());
	     eduStudentToObj.setDept_id(Integer.parseInt(eduStudentFormObj.getDepartment()));
	     eduStudentToObj.setStudentid(eduStudentFormObj.getStudentid());
	     if(request.getParameterValues("selectedsubjects")!=null ){
	    	 eduStudentFormObj.setAssignsubjected(request.getParameterValues("selectedsubjects"));    		
			      }
		   logger.debug("Updation in process for student............ ");
		   eduStudentManagerObj.updateStudentData(eduStudentToObj,eduStudentFormObj);
		   logger.debug("Updation is completed for student............ ");

	   }
	   catch(Exception exp)
	   {
		   exp.printStackTrace();
		   logger.debug("Method update :: ERROR "+exp.getMessage());	

	   }
	   return mapping.findForward("update");
   
}
	
  public ActionForward delete(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
   {  
	  if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
	   String ug_id = request.getSession().getAttribute("ug_id").toString();
	  if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
	   EduStudentManager eduStudentManagerObj = new EduStudentManager();
	   eduStudentManagerObj.deleteStudentDetailbyUserId(request.getParameter("User_id"));
	   logger.debug("Deletion is completed for student............ ");
	   return mapping.findForward("delete");
   }
  
  
	public ActionForward viewDetails(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null){
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		try
		{
	     logger.debug("View data in process for student............ ");
	     logger.debug("Method viewDetails :: user id "+request.getParameter("User_id")+" Session_id "+request.getParameter("Session_id")+" Stud_staus "+request.getParameter("Stud_status"));
		 Calendar cal = Calendar.getInstance();
		 DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 EduStudentManager eduStudentManagerObj = new EduStudentManager();
	     EduStudentTO eduStudentToObj = eduStudentManagerObj.viewEduStudentDatabyUserId(request.getParameter("User_id"),request.getParameter("Session_id"),request.getParameter("Stud_status"));
	     request.setAttribute("firstname",eduStudentToObj.getFirstname());
	     request.setAttribute("lastname",eduStudentToObj.getLastname());
	     request.setAttribute("phoneno",eduStudentToObj.getPhoneno());
	     request.setAttribute("address",eduStudentToObj.getAddress());
	     request.setAttribute("admissiondate",eduStudentToObj.getAdmissiondate());
	     request.setAttribute("middlename",eduStudentToObj.getMiddlename());
	     request.setAttribute("emailid",eduStudentToObj.getEmailid());
	     request.setAttribute("fathername",eduStudentToObj.getFathername());
	     request.setAttribute("mothername",eduStudentToObj.getMothername());
	     request.setAttribute("parentemailid",eduStudentToObj.getParentemailid());
	     request.setAttribute("classname",eduStudentToObj.getClassname());
	     request.setAttribute("gender",eduStudentToObj.getGender());
	     request.setAttribute("student_prev_qualification",eduStudentToObj.getStudprequalification_title());
	     request.setAttribute("parent_higher_qualification",eduStudentToObj.getParentqualification_title());
	     request.setAttribute("previous_edu_center",eduStudentToObj.getPreviousinstitute());
	     request.setAttribute("admissionfee",eduStudentToObj.getAdmissionfee());
	     request.setAttribute("concessionamt",eduStudentToObj.getConcessionamt());
	     request.setAttribute("comment",eduStudentToObj.getComment());
	     System.out.println("birth date"+(eduStudentToObj.getBirthmonth()+1)+"/"+eduStudentToObj.getBirthday()+"/"+eduStudentToObj.getBirthyear());
	     DateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy");
	     request.setAttribute("birthofdate", df1.format(dateFormat.parse((eduStudentToObj.getBirthmonth()+1)+"/"+eduStudentToObj.getBirthday()+"/"+eduStudentToObj.getBirthyear())));
	     cal.setTime(dateFormat.parse(eduStudentToObj.getAdmissiondate()));
		 cal.add(Calendar.MONTH,1);		
		 request.setAttribute("admissionofdate",df1.format(cal.getTime()));
	     logger.debug("Method viewDetails :: birthdate "+request.getAttribute("birthofdate")+" admissiondate "+request.getAttribute("admissionofdate"));
	     logger.debug("View data is completed for student............ ");

		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method viewDetails :: ERROR "+exp.getMessage());	
			
		}
		 return mapping.findForward("viewDetails");
	}
	
	 public void sendMail(EduStudentTO mailInfoObj,String studentPassword,String parentPassword)
	  {
		  logger.debug("Mail Sending Started............ "+mailInfoObj.getFirstname()+" "+mailInfoObj.getMiddlename()+" "+mailInfoObj.getLastname());
		 try{
		  Properties props = new Properties();
		  PortableConstant PortableConstantObj = new PortableConstant();
		  props.put("mail.smtp.host",PortableConstant.SMTP_HOST_NAME);
		  props.put("mail.smtp.auth","true");
		  //props.put("mail.debug","true");
		  props.put("mail.smtp.port",PortableConstant.SMTP_PORT);
		  props.put("mail.smtp.socketFactory.port",PortableConstant.SMTP_PORT);
		  props.put("mail.smtp.socketFactory.class",PortableConstant.SSL_FACTORY);
		  //props.put("mail.smtp.socketFactory.fallback","false");
		  final String emailId =  PortableConstantObj.getProperty(PortableConstant.EMAILFROMADDRESS);
		  final String password =  PortableConstantObj.getProperty(PortableConstant.EMAILFROMPASSWORD);
		  logger.debug("Email Id and Password used for PasswordAuthentication...... "+emailId+"       "+password);
		  Session session = Session.getDefaultInstance(props,
		  new javax.mail.Authenticator() {
		  protected PasswordAuthentication getPasswordAuthentication() {
		  return new PasswordAuthentication(emailId,password);
		  }
		  });
		  session.setDebug(PortableConstant.debug);
		  Message msg = new MimeMessage(session);
		  Message parentmsg = new MimeMessage(session);
		  InternetAddress addressFrom = new InternetAddress(PortableConstantObj.getProperty(PortableConstant.EMAILFROMADDRESS));
		  msg.setFrom(addressFrom);
		  parentmsg.setFrom(addressFrom);
		  InternetAddress addressTo = new InternetAddress (mailInfoObj.getEmailid());
		  msg.setRecipient(Message.RecipientType.TO, addressTo);
		  InternetAddress parentaddressTo = new InternetAddress (mailInfoObj.getParentemailid());
		  parentmsg.setRecipient(Message.RecipientType.TO, parentaddressTo);
		  // Setting the Subject and Content Type
		  msg.setSubject(PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT));
		  parentmsg.setSubject(PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT));
		  //msg.setContent(PortableConstant.EMAILMSGTXT, "text/plain");
		  //msg.setContent("Hi, "+mailInfoObj.getFirstname()+" "+mailInfoObj.getMiddlename()+" "+mailInfoObj.getLastname(), "text/html");
		  msg.setContent("Hi "+mailInfoObj.getFirstname()+" "+mailInfoObj.getMiddlename()+" "+mailInfoObj.getLastname()+","+"<br>"+PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT)+"!<br><br>We are informing you your Registration is in process.Shortly we will inform you about status. <br><br>"+PortableConstantObj.getProperty(PortableConstant.REGARDS)+"<br>"+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL), "text/html");
		  parentmsg.setContent("Hi "+mailInfoObj.getFathername()+","+"<br>"+PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT)+"!<br><br>Thanks to showing interest in the institute "+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL)+" as a parent. We are dedicate to improve growth of our students.Your parent id is "+mailInfoObj.getParentid()+" and credential is "+mailInfoObj.getParentUser()+"/"+parentPassword+"<br><br>"+PortableConstantObj.getProperty(PortableConstant.REGARDS)+"<br>"+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL), "text/html");
		  Transport.send(msg);
		 // Transport.send(parentmsg);
	    }
		 catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); 
				logger.debug("Mail Sending MessagingException sendMail :: "+e.getMessage());
			}
		 catch (Exception exp) {
				// TODO Auto-generated catch block
				exp.printStackTrace();
				logger.debug("Mail Sending Exception sendMail :: "+exp.getMessage());
			}
	  }
	 
	 
	 
	 public ActionForward createReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
		{
			if(request.getSession().getAttribute("user_name") == null)
			{
				return mapping.findForward("userlogout");
			}
			String ug_id = request.getSession().getAttribute("ug_id").toString();
			if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
				return mapping.findForward("userlogout");
			}
			logger.debug("Setup in process for createReport............ ");
			EduStudentReportForm eduStudentReportFormObj = (EduStudentReportForm)form;
			EduStudentReportManager eduStudentManagerObj = new EduStudentReportManager();
			EduStudentReportTO eduStudentReportToObj = new EduStudentReportTO();
			
			String strToken= request.getParameter("subin"); 
			System.out.println("strTokenstrTokenstrTokenstrTokenstrToken   "+strToken);
			
			ArrayList<EduStudentReportTO> eduStudentReportToArrayObj = eduStudentManagerObj.getSubjectMarkByStudent(strToken);
			request.setAttribute("resultObj",eduStudentReportToArrayObj);
			
			System.out.println("eduStudentReportToArrayObj   "+eduStudentReportToArrayObj.size()+"  ArraySize        "+eduStudentReportToArrayObj.get(0).getSubject().length);
			logger.debug("Setup completed for createReport............ "+strToken);
			request.setAttribute("subin", strToken);
			return mapping.findForward("createReport");
		}
	 
	 public ActionForward printReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
		{
			if(request.getSession().getAttribute("user_name") == null)
			{
				return mapping.findForward("userlogout");
			}
			String ug_id = request.getSession().getAttribute("ug_id").toString();
			if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
				return mapping.findForward("userlogout");
			}
			logger.debug("Setup in process for createReport............ ");
			EduStudentReportForm eduStudentReportFormObj = (EduStudentReportForm)form;
			EduStudentReportManager eduStudentManagerObj = new EduStudentReportManager();
			EduStudentReportTO eduStudentReportToObj = new EduStudentReportTO();
			
			String strToken= request.getParameter("subin"); 
			System.out.println("strTokenstrTokenstrTokenstrTokenstrToken   "+strToken);
			
			ArrayList<EduStudentReportTO> eduStudentReportToArrayObj = eduStudentManagerObj.getSubjectMarkByStudent(strToken);
			request.setAttribute("resultObj",eduStudentReportToArrayObj);
			
			System.out.println("eduStudentReportToArrayObj   "+eduStudentReportToArrayObj.size()+"  ArraySize        "+eduStudentReportToArrayObj.get(0).getSubject().length);
			logger.debug("Setup completed for createReport............ "+strToken);
			request.setAttribute("subin", strToken);
			return mapping.findForward("printReport");
		}
	 public ActionForward studentProfile(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
		{
			if(request.getSession().getAttribute("user_name") == null)
			{
				return mapping.findForward("userlogout");
			}
			String ug_id = request.getSession().getAttribute("ug_id").toString();
			if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
				return mapping.findForward("userlogout");
			}
			String returnKey=null;
			if(ug_id!=null && ug_id.equals("6")){
			returnKey="studentProfile";
			String user_id = request.getSession().getAttribute("user_id").toString();
			logger.debug("Setup in process for Student Report Card............ "+user_id);
			EduStudentReportForm eduStudentReportFormObj = (EduStudentReportForm)form;
			EduStudentReportManager eduStudentManagerObj = new EduStudentReportManager();
			EduStudentReportTO eduStudentReportToObj = new EduStudentReportTO();
			eduStudentReportToObj = eduStudentManagerObj.getEduStudentDatabyStudentId(user_id,"A");
			eduStudentReportFormObj.setStudentid(eduStudentReportToObj.getStudentid());
			eduStudentReportFormObj.setStudentname(eduStudentReportToObj.getStudentname());
			eduStudentReportFormObj.setDateofbirth(eduStudentReportToObj.getDateofbirth());
			eduStudentReportFormObj.setDepartmentname(eduStudentReportToObj.getDepartmentname());
			eduStudentReportFormObj.setFathername(eduStudentReportToObj.getFathername());
			eduStudentReportFormObj.setClassid(eduStudentReportToObj.getClassid());
			eduStudentReportFormObj.setSessionid(eduStudentReportToObj.getSessionid());
			eduStudentReportFormObj.setDept_id(eduStudentReportToObj.getDept_id());
			eduStudentReportFormObj.setSession_year(eduStudentReportToObj.getSession_year());
			request.setAttribute("studentname",eduStudentReportToObj.getStudentname());
			request.setAttribute("studentid", eduStudentReportToObj.getStudentid());
			request.setAttribute("dateofbirth", eduStudentReportToObj.getDateofbirth());
			request.setAttribute("fathername", eduStudentReportToObj.getFathername());
			request.setAttribute("classname", eduStudentReportToObj.getClassname());
			request.setAttribute("classid", eduStudentReportToObj.getClassid());
			request.setAttribute("departmentname", eduStudentReportToObj.getDepartmentname());
			request.setAttribute("dept_id", eduStudentReportToObj.getDept_id());
			
			request.setAttribute("user_id", eduStudentReportToObj.getUserid());
			request.setAttribute("admissiondate", eduStudentReportToObj.getAdmissiondate());
			request.setAttribute("emailid", eduStudentReportToObj.getEmailid());
			request.setAttribute("address", eduStudentReportToObj.getAddress());
			request.setAttribute("phoneno", eduStudentReportToObj.getPhoneno());
			System.out.println("user_id"+eduStudentReportToObj.getUserid()+"admissiondate"+eduStudentReportToObj.getAdmissiondate()+"emailid"+eduStudentReportToObj.getEmailid()+"address"+eduStudentReportToObj.getAddress()+"phoneno"+eduStudentReportToObj.getPhoneno());
			ArrayList<EduStudentReportTO> eduStudentReportToArrayObj = eduStudentManagerObj.getSubjectClassByStudent(user_id);
			request.setAttribute("studentclassObj",eduStudentReportToArrayObj);
			logger.debug("Setup completed for Student Report Card............ "+eduStudentReportToObj.getDept_id());
			for(int xx=0;xx<eduStudentReportToArrayObj.size();xx++){
				logger.debug("Student Details ========="+eduStudentReportToArrayObj.get(xx).getSessionid()+"  "+ eduStudentReportToArrayObj.get(xx).getClassname());	
				for(int ii=0;ii<eduStudentReportToArrayObj.get(xx).getSubject().length;ii++){
					logger.debug("Subject Details ========="+eduStudentReportToArrayObj.get(xx).getSubject()[ii]);	
				}	
			}
			
		}
			else{
				
				returnKey="studentProfileList";
			}
			return mapping.findForward(returnKey);
		}

	 public ActionForward viewStudentProfile(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
		{
			if(request.getSession().getAttribute("user_name") == null)
			{
				return mapping.findForward("userlogout");
			}
			String ug_id = request.getSession().getAttribute("ug_id").toString();
			if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
				return mapping.findForward("userlogout");
			}
			String user_id = request.getParameter("User_id");
			String Stud_status = request.getParameter("Stud_status");
			logger.debug("Setup in process for Student Report Card............ ");
			EduStudentReportForm eduStudentReportFormObj = (EduStudentReportForm)form;
			EduStudentReportManager eduStudentManagerObj = new EduStudentReportManager();
			EduStudentReportTO eduStudentReportToObj = new EduStudentReportTO();
			eduStudentReportToObj = eduStudentManagerObj.getEduStudentDatabyStudentId(user_id,Stud_status);
			eduStudentReportFormObj.setStudentid(eduStudentReportToObj.getStudentid());
			eduStudentReportFormObj.setStudentname(eduStudentReportToObj.getStudentname());
			eduStudentReportFormObj.setDateofbirth(eduStudentReportToObj.getDateofbirth());
			eduStudentReportFormObj.setFathername(eduStudentReportToObj.getFathername());
			request.setAttribute("studentname",eduStudentReportToObj.getStudentname());
			request.setAttribute("studentid", eduStudentReportToObj.getStudentid());
			request.setAttribute("dateofbirth", eduStudentReportToObj.getDateofbirth());
			request.setAttribute("fathername", eduStudentReportToObj.getFathername());
			request.setAttribute("classname", eduStudentReportToObj.getClassname());
			request.setAttribute("user_id", eduStudentReportToObj.getUserid());
			request.setAttribute("admissiondate", eduStudentReportToObj.getAdmissiondate());
			request.setAttribute("emailid", eduStudentReportToObj.getEmailid());
			request.setAttribute("address", eduStudentReportToObj.getAddress());
			request.setAttribute("phoneno", eduStudentReportToObj.getPhoneno());
			System.out.println("user_id"+eduStudentReportToObj.getUserid()+"admissiondate"+eduStudentReportToObj.getAdmissiondate()+"emailid"+eduStudentReportToObj.getEmailid()+"address"+eduStudentReportToObj.getAddress()+"phoneno"+eduStudentReportToObj.getPhoneno()+"student name"+eduStudentReportToObj.getStudentname());
			ArrayList<EduStudentReportTO> eduStudentReportToArrayObj = eduStudentManagerObj.getSubjectClassByStudent(user_id);
			request.setAttribute("studentclassObj",eduStudentReportToArrayObj);
			if(eduStudentReportToArrayObj.get(0).getSubject()!=null){
				for(int xx=0;xx<eduStudentReportToArrayObj.size();xx++){
					logger.debug("Student Details ========="+eduStudentReportToArrayObj.get(xx).getSessionid()+"  "+ eduStudentReportToArrayObj.get(xx).getClassname());	
					for(int ii=0;ii<eduStudentReportToArrayObj.get(xx).getSubject().length;ii++){
						logger.debug("Subject Details ========="+eduStudentReportToArrayObj.get(xx).getSubject()[ii]);	
					}	
				}
			}
			return mapping.findForward("studentProfile");
		}

	 public ActionForward createNewStudentSetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
		{
			if(request.getSession().getAttribute("user_name") == null)
			{
				return mapping.findForward("userlogout");
			}
			String ug_id = request.getSession().getAttribute("ug_id").toString();
			if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
				return mapping.findForward("userlogout");
			}
			String user_id = request.getParameter("User_id");
			String class_id = request.getParameter("class_id");
			String session_id = request.getParameter("session_id");
			logger.debug("Setup in process for Student Report Card............ ");
			EduStudentReportForm eduStudentReportFormObj = (EduStudentReportForm)form;
			EduStudentReportManager eduStudentManagerObj = new EduStudentReportManager();
			EduStudentReportTO eduStudentReportToObj = new EduStudentReportTO();
			eduStudentReportToObj = eduStudentManagerObj.getEduStudentDatabyStudentId(user_id,"A");
			eduStudentReportFormObj.setStudentid(eduStudentReportToObj.getStudentid());
			eduStudentReportFormObj.setStudentname(eduStudentReportToObj.getStudentname());
			eduStudentReportFormObj.setDateofbirth(eduStudentReportToObj.getDateofbirth());
			eduStudentReportFormObj.setDepartmentname(eduStudentReportToObj.getDepartmentname());
			eduStudentReportFormObj.setFathername(eduStudentReportToObj.getFathername());
			eduStudentReportFormObj.setClassid(eduStudentReportToObj.getClassid());
			eduStudentReportFormObj.setSessionid(eduStudentReportToObj.getSessionid());
			eduStudentReportFormObj.setDept_id(eduStudentReportToObj.getDept_id());
			eduStudentReportFormObj.setSession_year(eduStudentReportToObj.getSession_year());
			request.setAttribute("studentname",eduStudentReportToObj.getStudentname());
			request.setAttribute("studentid", eduStudentReportToObj.getStudentid());
			request.setAttribute("dateofbirth", eduStudentReportToObj.getDateofbirth());
			request.setAttribute("fathername", eduStudentReportToObj.getFathername());
			request.setAttribute("classname", eduStudentReportToObj.getClassname());
			request.setAttribute("classid", eduStudentReportToObj.getClassid());
			request.setAttribute("departmentname", eduStudentReportToObj.getDepartmentname());
			request.setAttribute("dept_id", eduStudentReportToObj.getDept_id());
			
			request.setAttribute("user_id", eduStudentReportToObj.getUserid());
			request.setAttribute("admissiondate", eduStudentReportToObj.getAdmissiondate());
			request.setAttribute("emailid", eduStudentReportToObj.getEmailid());
			request.setAttribute("address", eduStudentReportToObj.getAddress());
			request.setAttribute("phoneno", eduStudentReportToObj.getPhoneno());
			System.out.println("user_id"+eduStudentReportToObj.getUserid()+"admissiondate"+eduStudentReportToObj.getAdmissiondate()+"emailid"+eduStudentReportToObj.getEmailid()+"address"+eduStudentReportToObj.getAddress()+"phoneno"+eduStudentReportToObj.getPhoneno());
			ArrayList<EduStudentReportTO> eduStudentReportToArrayObj = eduStudentManagerObj.getSubjectClassByStudent(user_id,class_id,session_id);
			request.setAttribute("studentclassObj",eduStudentReportToArrayObj);
			logger.debug("Setup completed for Student Report Card............ "+eduStudentReportToObj.getDept_id());
			for(int xx=0;xx<eduStudentReportToArrayObj.size();xx++){
				logger.debug("Student Details ========="+eduStudentReportToArrayObj.get(xx).getSessionid()+"  "+ eduStudentReportToArrayObj.get(xx).getClassname());	
				for(int ii=0;ii<eduStudentReportToArrayObj.get(xx).getSubject().length;ii++){
					logger.debug("Subject Details ========="+eduStudentReportToArrayObj.get(xx).getSubject()[ii]);	
				}	
			}
			ArrayList<EduStudentReportTO> classes = eduStudentManagerObj.getClassInfo();
			eduStudentReportFormObj.setClasses(classes);
			request.setAttribute("classes",classes);
			ArrayList<EduStudentReportTO> departments = eduStudentManagerObj.getDepartment();
			eduStudentReportFormObj.setDepartments(departments);
			request.setAttribute("departments",departments);
			EduStudentReportTO comingSessionObj =  eduStudentManagerObj.getComingSession((Integer.parseInt(session_id)+1));
			request.setAttribute("csession_id",comingSessionObj.getSessionid());
			request.setAttribute("csession_year", comingSessionObj.getSession_year());
			return mapping.findForward("createNewStudentSetup");
		}
	 public ActionForward newSession(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
		{
			if(request.getSession().getAttribute("user_name") == null){
				return mapping.findForward("userlogout");
			}
			String ug_id = request.getSession().getAttribute("ug_id").toString();
			if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
				return mapping.findForward("userlogout");
			}
			return mapping.findForward("newSession");
		}
	 
	 public ActionForward assignedNewSession(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
		{
			if(request.getSession().getAttribute("user_name") == null){
				return mapping.findForward("userlogout");
			}
			String ug_id = request.getSession().getAttribute("ug_id").toString();
			if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
				return mapping.findForward("userlogout");
			}
			EduStudentReportForm eduStudentReportFormObj = (EduStudentReportForm)form;
			EduStudentReportManager eduStudentManagerObj = new EduStudentReportManager();
			EduStudentReportTO eduStudentReportToObj = new EduStudentReportTO();
			eduStudentReportToObj.setDept_id(Integer.parseInt(eduStudentReportFormObj.getDepartment()));
	        eduStudentReportToObj.setSessionid(eduStudentReportFormObj.getSessionid());
	        eduStudentReportToObj.setClassid(Integer.valueOf(eduStudentReportFormObj.getEduclass()));
	        eduStudentReportToObj.setStudentid(eduStudentReportFormObj.getStudentid());
	        eduStudentReportToObj.setAdmissionfee(eduStudentReportFormObj.getAdmissionfee());
	        eduStudentReportToObj.setConcessionamt(eduStudentReportFormObj.getConcessionamt());
	        eduStudentReportToObj.setComment(eduStudentReportFormObj.getComment());
	        System.out.println(" Session id  "+eduStudentReportFormObj.getSessionid()+" class id "+eduStudentReportFormObj.getEduclass()+" dept_id "+eduStudentReportFormObj.getDepartment());
			if(request.getParameterValues("selectedsubjects")!=null ){
				eduStudentReportFormObj.setAssignsubjected(request.getParameterValues("selectedsubjects"));
				System.out.println("Size================="+request.getParameterValues("selectedsubjects").length);
			      }
			boolean bool =  eduStudentManagerObj.assignedClassSubToNewSession(eduStudentReportToObj,eduStudentReportFormObj);
			logger.debug("New Session Assigned status............ "+bool);
			return mapping.findForward("newSession");
		}
	 
	 
	 public ActionForward greenSheet(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
		{
			if(request.getSession().getAttribute("user_name") == null)
			{
				return mapping.findForward("userlogout");
			}
			String ug_id = request.getSession().getAttribute("ug_id").toString();
			if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
				return mapping.findForward("userlogout");
			}
			logger.debug("Setup in process for createReport............ ");
			EduStudentReportForm eduStudentReportFormObj = (EduStudentReportForm)form;
			EduStudentReportManager eduStudentManagerObj = new EduStudentReportManager();
			EduStudentReportTO eduStudentReportToObj = new EduStudentReportTO();
			
			String sessionId = request.getParameter("sessionId");
			String classId = request.getParameter("classId");
			ArrayList<EduStudentReportTO> eduStudentReportToArrayObj=null;
			ArrayList<EduStudentReportTO> subjectArrayByclassId=null;
			if(sessionId!=null && classId!=null){
				eduStudentReportToArrayObj = eduStudentManagerObj.getDataForGreenSheet(classId,sessionId);
			}	
			request.setAttribute("resultObj",eduStudentReportToArrayObj);
			return mapping.findForward("greenSheet");
		}
	 
	 
	 
}
