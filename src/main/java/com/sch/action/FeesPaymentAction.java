package com.sch.action;

import java.util.ArrayList;

import javax.servlet.http.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.sch.delegates.EduRegistrationManager;
import com.sch.delegates.EduStudentReportManager;
import com.sch.delegates.FeesPaymentManager;
import com.sch.delegates.LoginManager;
import com.sch.form.EduStudentForm;
import com.sch.form.EduStudentReportForm;
import com.sch.form.FeesPaymentForm;
import com.sch.to.EduRegistrationTO;
import com.sch.to.EduStudentReportTO;
import com.sch.to.EduStudentTO;
import com.sch.to.FeesPaymentTO;
public class FeesPaymentAction extends DispatchAction
{
	// Logging object and utility method
	String errorMsg = "";
	private int f_Id = 2;
	LoginManager loginManager =  new LoginManager();
	 public static Logger logger=Logger.getLogger(FeesPaymentAction.class);
	public ActionForward search(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("search");
	}
	
	public ActionForward createSetup(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		FeesPaymentForm feesPaymentFormObj = (FeesPaymentForm)form;
		FeesPaymentManager manobj = new FeesPaymentManager();
		logger.debug("Get the List of Month............");
		ArrayList<FeesPaymentTO> monthList = manobj.getMonthList();
		feesPaymentFormObj.setMonthList(monthList);
		request.setAttribute("monthList", monthList);
		return mapping.findForward("createSetup");
	}
	public ActionForward searchStudent(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		String searchKey =request.getParameter("keyword");
		if(null==searchKey){
			searchKey="";		
			}
		logger.debug("Get the List of Student");
		FeesPaymentManager manobj = new FeesPaymentManager();
		ArrayList<FeesPaymentTO> studentList = manobj.getStudentList(searchKey,""); 
		request.setAttribute("studentList", studentList);
		return mapping.findForward("searchStudent");
	}
	
	public ActionForward save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		FeesPaymentForm feesPaymentFormObj = (FeesPaymentForm)form;
		
		FeesPaymentManager manobj = new FeesPaymentManager();
		System.out.println("student_id>>>>"+feesPaymentFormObj.getStudentid()+"Class Id>>>"+feesPaymentFormObj.getClassid());
		FeesPaymentTO feesPaymentTOObj = new FeesPaymentTO();
		feesPaymentTOObj.setStudentid(feesPaymentFormObj.getStudentid());
		feesPaymentTOObj.setMonthly_fee(feesPaymentFormObj.getMonthly_fee());
		feesPaymentTOObj.setBankname(feesPaymentFormObj.getBankname());
		feesPaymentTOObj.setChequeNo(feesPaymentFormObj.getChequeNo());
		feesPaymentTOObj.setClassid(feesPaymentFormObj.getClassid());
		feesPaymentTOObj.setSessionid(feesPaymentFormObj.getSessionid());
		feesPaymentTOObj.setDateoncheque(feesPaymentFormObj.getDateoncheque());
		feesPaymentTOObj.setPaymentDate(feesPaymentFormObj.getPaymentDate());
		feesPaymentTOObj.setPaymentMode(feesPaymentFormObj.getPaymentMode());
		feesPaymentTOObj.setMonthselection(feesPaymentFormObj.getMonthselection());
		manobj.saveFeePayment(feesPaymentTOObj); 
		
		ArrayList<FeesPaymentTO> monthList = manobj.getMonthList();
		feesPaymentFormObj.setMonthList(monthList);
		request.setAttribute("monthList", monthList);
		
		return mapping.findForward("save");
	}

	public ActionForward order(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("order");
	}
	
	public ActionForward receiptSetup(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("receiptSetup");
	}

	public ActionForward printReceipt(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)throws Exception {
		
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		FeesPaymentForm feesPaymentFormObj = (FeesPaymentForm)form;
		FeesPaymentManager eduFeesPaymentManagerObj = new FeesPaymentManager();
		FeesPaymentTO  eduFeesPaymentTOObj = eduFeesPaymentManagerObj.getCollationDetailByUserId(request.getParameter("User_id"),request.getParameter("Session_id"),request.getParameter("receipt_no")); 
		logger.debug("Student Name"+eduFeesPaymentTOObj.getFirstname()+" "+eduFeesPaymentTOObj.getMiddlename()+" "+eduFeesPaymentTOObj.getLastname()+"  "+eduFeesPaymentTOObj.getPaymentDate()+"receipt no."+request.getParameter("receipt_no"));
		request.setAttribute("studentname",eduFeesPaymentTOObj.getFirstname()+" "+eduFeesPaymentTOObj.getMiddlename()+" "+eduFeesPaymentTOObj.getLastname());
		request.setAttribute("studentid", eduFeesPaymentTOObj.getStudentid());
		request.setAttribute("dateofbirth", eduFeesPaymentTOObj.getDateofbirth());
		request.setAttribute("fathername", eduFeesPaymentTOObj.getFathername());
		request.setAttribute("classname", eduFeesPaymentTOObj.getClassname());
		request.setAttribute("paymentdate", eduFeesPaymentTOObj.getPaymentDate());
		request.setAttribute("monthname", eduFeesPaymentTOObj.getMonthname());
		request.setAttribute("amount", eduFeesPaymentTOObj.getMonthly_fee());
		request.setAttribute("receiptno", eduFeesPaymentTOObj.getReceptNo());
		request.setAttribute("departmentname", eduFeesPaymentTOObj.getDept_name());
		ArrayList<FeesPaymentTO> monthList = eduFeesPaymentManagerObj.getPayedMonthList(request.getParameter("User_id"),request.getParameter("Session_id"),request.getParameter("receipt_no"));
		String month=null;
		for(int xx=0;xx<monthList.size();xx++){
			if(month!=null)
				month=month+"<BR>"+monthList.get(xx).getMonthname();
			else
				month=monthList.get(xx).getMonthname();
		}
		logger.debug("Payed Month List"+month);
		feesPaymentFormObj.setMonthList(monthList);
		request.setAttribute("monthList", month);
		return mapping.findForward("printReceipt");
	}
	
	public ActionForward registrationfeeSetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null){
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("registrationfeeSetup");
	}
	
	 public ActionForward registrationfeecollectionSetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
		{
			if(request.getSession().getAttribute("user_name") == null)
			{
				return mapping.findForward("userlogout");
			}
			String ug_id = request.getSession().getAttribute("ug_id").toString();
			if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
				return mapping.findForward("userlogout");
			}
			logger.debug("Setup in process for Register Student............ ");
			FeesPaymentForm feesPaymentFormObj = (FeesPaymentForm)form;
			FeesPaymentManager eduFeesPaymentManagerObj = new FeesPaymentManager();
			FeesPaymentTO  eduFeesPaymentTOObj = eduFeesPaymentManagerObj.getEduRegistrationDatabyUserId(request.getParameter("User_id"),request.getParameter("Session_id"),request.getParameter("Reg_status"));
			feesPaymentFormObj.setStudentid(eduFeesPaymentTOObj.getStudentid());
		    eduFeesPaymentTOObj.getFirstname();
		    eduFeesPaymentTOObj.getMiddlename();
		    eduFeesPaymentTOObj.getLastname();
		    feesPaymentFormObj.setDateofbirth(eduFeesPaymentTOObj.getDateofbirth());
		    feesPaymentFormObj.setDept_name(eduFeesPaymentTOObj.getDept_name());
		    feesPaymentFormObj.setFathername(eduFeesPaymentTOObj.getFathername());
		    feesPaymentFormObj.setClassid(eduFeesPaymentTOObj.getClassid());
		    feesPaymentFormObj.setSessionid(eduFeesPaymentTOObj.getSessionid());
		    feesPaymentFormObj.setDept_id(eduFeesPaymentTOObj.getDept_id());
		    feesPaymentFormObj.setSessionyear(eduFeesPaymentTOObj.getSessionyear());
		    feesPaymentFormObj.setRegistrationfee(eduFeesPaymentTOObj.getRegistrationfee());
			request.setAttribute("studentname",eduFeesPaymentTOObj.getFirstname()+" "+eduFeesPaymentTOObj.getMiddlename()+" "+eduFeesPaymentTOObj.getLastname());
			request.setAttribute("studentid", eduFeesPaymentTOObj.getStudentid());
			request.setAttribute("dateofbirth", eduFeesPaymentTOObj.getDateofbirth());
			request.setAttribute("fathername", eduFeesPaymentTOObj.getFathername());
			request.setAttribute("classname", eduFeesPaymentTOObj.getClassname());
			request.setAttribute("classid", eduFeesPaymentTOObj.getClassid());
			request.setAttribute("departmentname", eduFeesPaymentTOObj.getDept_name());
			request.setAttribute("dept_id", eduFeesPaymentTOObj.getDept_id());
			
			logger.debug("Setup completed for Student Report Card............ "+eduFeesPaymentTOObj.getDept_id());
			return mapping.findForward("registrationfeecollectionSetup");
		}
	 
	 public ActionForward saveRegPayment(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response)throws Exception {
			
			if(request.getSession().getAttribute("user_name") == null)
			{
				return mapping.findForward("userlogout");
			}
			String ug_id = request.getSession().getAttribute("ug_id").toString();
			if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
				return mapping.findForward("userlogout");
			}
			FeesPaymentForm feesPaymentFormObj = (FeesPaymentForm)form;
			
			FeesPaymentManager manobj = new FeesPaymentManager();
			System.out.println("student_id>>>>"+feesPaymentFormObj.getStudentid()+"Class Id>>>"+feesPaymentFormObj.getClassid());
			FeesPaymentTO feesPaymentTOObj = new FeesPaymentTO();
			feesPaymentTOObj.setStudentid(feesPaymentFormObj.getStudentid());
			feesPaymentTOObj.setRegistrationfee(feesPaymentFormObj.getRegistrationfee());
			feesPaymentTOObj.setConcessionamt(feesPaymentFormObj.getConcessionamt());
			feesPaymentTOObj.setComment(feesPaymentFormObj.getComment());
			manobj.saveRegPayment(feesPaymentTOObj); 
			return mapping.findForward("registrationfeeSetup");
		}
	 
	
}