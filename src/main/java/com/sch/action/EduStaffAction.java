package com.sch.action;

import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.*;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.sch.delegates.LoginManager;
import com.sch.delegates.EduStaffManager;
import com.sch.form.EduStaffForm;
import com.sch.to.EduStaffTO;
import com.sch.to.EduStudentReportTO;
import com.sch.constant.PortableConstant;

public class EduStaffAction extends DispatchAction {
	private int f_Id = 7;
	LoginManager loginManager = new LoginManager();
	public static Logger logger = Logger.getLogger(EduStaffAction.class);

	public ActionForward search(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("search");
	}

	public ActionForward searchAlumni(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("searchAlumni");
	}

	public ActionForward createSetup(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		logger.debug("Setup in process for staff............ ");
		EduStaffForm eduStaffFormObj = (EduStaffForm) form;
		EduStaffManager eduStaffManagerObj = new EduStaffManager();
		ArrayList<EduStaffTO> levels = eduStaffManagerObj.getStaffLevel();
		eduStaffFormObj.setLevels(levels);
		request.setAttribute("levels", levels);
		ArrayList<EduStaffTO> qualifications = eduStaffManagerObj
				.getStaffQualification();
		eduStaffFormObj.setQualifications(qualifications);
		request.setAttribute("qualifications", qualifications);
		ArrayList<EduStaffTO> departments = eduStaffManagerObj.getDepartment();
		eduStaffFormObj.setDepartments(departments);
		request.setAttribute("departments", departments);
		logger.debug("Setup completed for staff............ ");
		return mapping.findForward("createSetup");
	}

	public ActionForward save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		try {

			EduStaffForm eduStaffFormObj = (EduStaffForm) form;
			EduStaffTO eduStaffToObj = new EduStaffTO();
			EduStaffManager eduStaffManagerObj = new EduStaffManager();
			eduStaffToObj.setFirstname(eduStaffFormObj.getFirstname());
			eduStaffToObj.setMiddlename(eduStaffFormObj.getMiddlename());
			eduStaffToObj.setLastname(eduStaffFormObj.getLastname());
			eduStaffToObj.setPhoneno(eduStaffFormObj.getPhoneno());
			eduStaffToObj.setSalary(eduStaffFormObj.getSalary());
			eduStaffToObj.setQualification(eduStaffFormObj.getQualification());
			eduStaffToObj.setEmailid(eduStaffFormObj.getEmailid());
			eduStaffToObj.setPreviousexp(eduStaffFormObj.getPreviousexp());
			eduStaffToObj.setJoiningdate(eduStaffFormObj.getJoiningdate());
			eduStaffToObj.setAddress(eduStaffFormObj.getAddress());
			eduStaffToObj.setLevel(eduStaffFormObj.getLevel());
			StringTokenizer st = new StringTokenizer(
					eduStaffFormObj.getJoiningdate(), "-");
			String password = "";
			while (st.hasMoreTokens())
				password = password + st.nextToken();
			System.out.println("password" + password);
			eduStaffToObj.setPassword(eduStaffFormObj.getPassword());
			eduStaffToObj.setDept_id(Integer.parseInt(eduStaffFormObj
					.getDepartment()));
			if (request.getParameterValues("selectedclasses") != null) {
				eduStaffFormObj.setAssignclassed(request
						.getParameterValues("selectedclasses"));
			}
			if (request.getParameterValues("selectedsubjects") != null) {
				eduStaffFormObj.setAssignsubjected(request
						.getParameterValues("selectedsubjects"));
			}
			logger.debug("Insertion in process for staff............ ");
			EduStaffTO mailInfoObj = eduStaffManagerObj.saveEduStaffData(
					eduStaffToObj, eduStaffFormObj);
			logger.debug("Insertion in completed for staff............");
			sendMail(mailInfoObj, password);
		} catch (Exception e) {
			e.getMessage();
		}
		return mapping.findForward("save");
	}

	public ActionForward updateSetup(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		logger.debug("updateSetup in process for staff............ ");
		EduStaffForm eduStaffFormObj = (EduStaffForm) form;
		EduStaffManager eduStaffManagerObj = new EduStaffManager();
		EduStaffTO eduStaffToObj = eduStaffManagerObj.getEduStaffDatabyUserId(
				request.getParameter("User_id"),
				request.getParameter("Emp_status"));
		eduStaffFormObj.setFirstname(eduStaffToObj.getFirstname());
		eduStaffFormObj.setUserid(eduStaffToObj.getUserid());
		eduStaffFormObj.setLastname(eduStaffToObj.getLastname());
		eduStaffFormObj.setPhoneno(eduStaffToObj.getPhoneno());
		eduStaffFormObj.setAddress(eduStaffToObj.getAddress());
		eduStaffFormObj.setDesignation_id(eduStaffToObj.getDesignation_id());
		eduStaffFormObj.setMiddlename(eduStaffToObj.getMiddlename());
		eduStaffFormObj.setEmailid(eduStaffToObj.getEmailid());
		eduStaffFormObj.setSalary(eduStaffToObj.getSalary());
		eduStaffFormObj.setJoiningdate(eduStaffToObj.getJoiningdate());
		eduStaffFormObj.setPreviousexp(eduStaffToObj.getPreviousexp());
		eduStaffFormObj
				.setQualification_id(eduStaffToObj.getQualification_id());
		ArrayList<EduStaffTO> levels = eduStaffManagerObj.getStaffLevel();
		eduStaffFormObj.setLevels(levels);
		request.setAttribute("levels", levels);
		request.setAttribute("assodesignation_id",
				eduStaffFormObj.getDesignation_id());
		ArrayList<EduStaffTO> departments = eduStaffManagerObj.getDepartment();
		eduStaffFormObj.setDepartments(departments);
		request.setAttribute("departments", departments);
		request.setAttribute("assodept_id", eduStaffFormObj.getDept_id());
		ArrayList<EduStaffTO> qualifications = eduStaffManagerObj
				.getStaffQualification();
		eduStaffFormObj.setQualifications(qualifications);
		request.setAttribute("qualifications", qualifications);
		request.setAttribute("assoqualification_id",
				eduStaffFormObj.getQualification_id());
		request.setAttribute("userid", eduStaffFormObj.getUserid());
		logger.debug("UpdateSetup is completed for staff............ ");
		return mapping.findForward("updateSetup");
	}

	public ActionForward update(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}

		EduStaffForm eduStaffFormObj = (EduStaffForm) form;
		EduStaffManager eduStaffManagerObj = new EduStaffManager();
		EduStaffTO eduStaffToObj = new EduStaffTO();
		eduStaffToObj.setFirstname(eduStaffFormObj.getFirstname());
		eduStaffToObj.setMiddlename(eduStaffFormObj.getMiddlename());
		eduStaffToObj.setLastname(eduStaffFormObj.getLastname());
		eduStaffToObj.setUserid(eduStaffFormObj.getUserid());
		eduStaffToObj.setAddress(eduStaffFormObj.getAddress());
		eduStaffToObj.setEmailid(eduStaffFormObj.getEmailid());
		eduStaffToObj.setLevel(eduStaffFormObj.getLevel());
		eduStaffToObj.setPreviousexp(eduStaffFormObj.getPreviousexp());
		eduStaffToObj.setPhoneno(eduStaffFormObj.getPhoneno());
		eduStaffToObj.setJoiningdate(eduStaffFormObj.getJoiningdate());
		eduStaffToObj.setQualification(eduStaffFormObj.getQualification());
		eduStaffToObj.setSalary(eduStaffFormObj.getSalary());
		eduStaffToObj.setDept_id(Integer.parseInt(eduStaffFormObj
				.getDepartment()));
		if (request.getParameterValues("selectedclasses") != null) {
			eduStaffFormObj.setAssignclassed(request
					.getParameterValues("selectedclasses"));
			System.out.println("Assigned Class"
					+ request.getParameterValues("selectedclasses").length);
		}
		if (request.getParameterValues("selectedsubjects") != null) {
			eduStaffFormObj.setAssignsubjected(request
					.getParameterValues("selectedsubjects"));
		}
		logger.debug("Updation in process for staff............ ");
		eduStaffManagerObj.updateStaffData(eduStaffToObj, eduStaffFormObj);
		logger.debug("Updation is completed for staff............ ");
		return mapping.findForward("update");

	}

	public ActionForward delete(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		EduStaffManager eduStaffManagerObj = new EduStaffManager();
		eduStaffManagerObj.deleteStaffDetailbyUserId(request
				.getParameter("User_id"));
		logger.debug("Deletion is completed for staff............ ");
		return mapping.findForward("delete");
	}

	public ActionForward viewSetup(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		logger.debug("updateSetup in process for staff............ ");
		EduStaffForm eduStaffFormObj = (EduStaffForm) form;
		EduStaffManager eduStaffManagerObj = new EduStaffManager();
		EduStaffTO eduStaffToObj = eduStaffManagerObj.getEduStaffDatabyUserId(
				request.getParameter("User_id"),
				request.getParameter("Emp_status"));
		eduStaffFormObj.setFirstname(eduStaffToObj.getFirstname());
		eduStaffFormObj.setUserid(eduStaffToObj.getUserid());
		eduStaffFormObj.setLastname(eduStaffToObj.getLastname());
		eduStaffFormObj.setPhoneno(eduStaffToObj.getPhoneno());
		eduStaffFormObj.setAddress(eduStaffToObj.getAddress());
		eduStaffFormObj.setDesignation_id(eduStaffToObj.getDesignation_id());
		eduStaffFormObj.setMiddlename(eduStaffToObj.getMiddlename());
		eduStaffFormObj.setEmailid(eduStaffToObj.getEmailid());
		eduStaffFormObj.setSalary(eduStaffToObj.getSalary());
		eduStaffFormObj.setJoiningdate(eduStaffToObj.getJoiningdate());
		eduStaffFormObj.setPreviousexp(eduStaffToObj.getPreviousexp());
		eduStaffFormObj
				.setQualification_id(eduStaffToObj.getQualification_id());
		ArrayList<EduStaffTO> levels = eduStaffManagerObj.getStaffLevel();
		eduStaffFormObj.setLevels(levels);
		request.setAttribute("levels", levels);
		request.setAttribute("assodesignation_id",
				eduStaffFormObj.getDesignation_id());
		ArrayList<EduStaffTO> departments = eduStaffManagerObj.getDepartment();
		eduStaffFormObj.setDepartments(departments);
		request.setAttribute("departments", departments);
		request.setAttribute("assodept_id", eduStaffFormObj.getDept_id());
		ArrayList<EduStaffTO> qualifications = eduStaffManagerObj
				.getStaffQualification();
		eduStaffFormObj.setQualifications(qualifications);
		request.setAttribute("qualifications", qualifications);
		request.setAttribute("assoqualification_id",
				eduStaffFormObj.getQualification_id());
		request.setAttribute("userid", eduStaffFormObj.getUserid());
		logger.debug("UpdateSetup is completed for staff............ ");
		return mapping.findForward("viewSetup");
	}

	public void sendMail(EduStaffTO mailInfoObj, String password) {
		logger.debug("Mail Sending Started............ "
				+ mailInfoObj.getFirstname() + " "
				+ mailInfoObj.getMiddlename() + " " + mailInfoObj.getLastname());
		try {
			Properties props = new Properties();
			PortableConstant PortableConstantObj = new PortableConstant();
			props.put("mail.smtp.host", PortableConstant.SMTP_HOST_NAME);
			props.put("mail.smtp.auth", "true");
			// props.put("mail.debug","true");
			props.put("mail.smtp.port", PortableConstant.SMTP_PORT);
			props.put("mail.smtp.socketFactory.port",
					PortableConstant.SMTP_PORT);
			props.put("mail.smtp.socketFactory.class",
					PortableConstant.SSL_FACTORY);
			// props.put("mail.smtp.socketFactory.fallback","false");
			final String emailId = PortableConstantObj
					.getProperty(PortableConstant.EMAILFROMADDRESS);
			final String authpassword = PortableConstantObj
					.getProperty(PortableConstant.EMAILFROMPASSWORD);
			logger.debug("Email Id and Password used for PasswordAuthentication......     "
					+ emailId + ".......       " + authpassword);
			Session session = Session.getDefaultInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(emailId,
									authpassword);
						}
					});
			session.setDebug(PortableConstant.debug);
			Message msg = new MimeMessage(session);
			InternetAddress addressFrom = new InternetAddress(
					PortableConstantObj
							.getProperty(PortableConstant.EMAILFROMADDRESS));
			msg.setFrom(addressFrom);
			InternetAddress addressTo = new InternetAddress(
					mailInfoObj.getEmailid());
			msg.setRecipient(Message.RecipientType.TO, addressTo);

			// Setting the Subject and Content Type
			msg.setSubject(PortableConstantObj
					.getProperty(PortableConstant.EMAILSUBJECTTXT));
			// msg.setContent(PortableConstant.EMAILMSGTXT, "text/plain");
			// msg.setContent("Hi, "+mailInfoObj.getFirstname()+" "+mailInfoObj.getMiddlename()+" "+mailInfoObj.getLastname(),
			// "text/html");
			msg.setContent(
					"Hi "
							+ mailInfoObj.getFirstname()
							+ " "
							+ mailInfoObj.getMiddlename()
							+ " "
							+ mailInfoObj.getLastname()
							+ ","
							+ "<br>"
							+ PortableConstantObj
									.getProperty(PortableConstant.EMAILSUBJECTTXT)
							+ "!<br><br>Congratulations on accepting your offer to work with "
							+ PortableConstantObj
									.getProperty(PortableConstant.NAMESCHOOL)
							+ ". Your employee id is "
							+ mailInfoObj.getEmpid()
							+ " and credential is "
							+ mailInfoObj.getLoginuser()
							+ "/"
							+ password
							+ "<br><br>"
							+ PortableConstantObj
									.getProperty(PortableConstant.REGARDS)
							+ "<br>"
							+ PortableConstantObj
									.getProperty(PortableConstant.NAMESCHOOL),
					"text/html");
			Transport.send(msg);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.debug("Mail Sending MessagingException sendMail :: "
					+ e.getMessage());
		} catch (Exception exp) {
			// TODO Auto-generated catch block
			exp.printStackTrace();
			logger.debug("Mail Sending Exception sendMail :: "
					+ exp.getMessage());
		}
	}

	public ActionForward empProfile(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("empProfile");
	}

	public ActionForward viewProfile(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		String user_id = request.getParameter("User_id");
		logger.debug("Setup in process for Student Report Card............ ");

		EduStaffForm eduStaffFormObj = (EduStaffForm) form;
		EduStaffManager eduStaffManagerObj = new EduStaffManager();
		EduStaffTO eduStaffToObj = eduStaffManagerObj.getEduStaffDatabyUserId(
				request.getParameter("User_id"),
				request.getParameter("Emp_status"));
		eduStaffFormObj.setFirstname(eduStaffToObj.getFirstname());
		eduStaffFormObj.setUserid(eduStaffToObj.getUserid());
		eduStaffFormObj.setLastname(eduStaffToObj.getLastname());
		eduStaffFormObj.setPhoneno(eduStaffToObj.getPhoneno());
		eduStaffFormObj.setAddress(eduStaffToObj.getAddress());
		eduStaffFormObj.setDesignation_id(eduStaffToObj.getDesignation_id());
		eduStaffFormObj.setMiddlename(eduStaffToObj.getMiddlename());
		eduStaffFormObj.setEmailid(eduStaffToObj.getEmailid());
		eduStaffFormObj.setSalary(eduStaffToObj.getSalary());
		eduStaffFormObj.setJoiningdate(eduStaffToObj.getJoiningdate());
		eduStaffFormObj.setPreviousexp(eduStaffToObj.getPreviousexp());
		eduStaffFormObj
				.setQualification_id(eduStaffToObj.getQualification_id());

		request.setAttribute("employeename", eduStaffToObj.getFirstname() + " "
				+ eduStaffToObj.getLastname());
		request.setAttribute("employeeno", eduStaffToObj.getUserid());
		request.setAttribute("dateofbirth", eduStaffToObj.getJoiningdate());
		request.setAttribute("qualification_title",
				eduStaffToObj.getQualification_title());
		request.setAttribute("classname", eduStaffToObj.getPreviousexp());
		request.setAttribute("user_id", eduStaffToObj.getLoginuser());
		request.setAttribute("admissiondate", eduStaffToObj.getJoiningdate());
		request.setAttribute("emailid", eduStaffToObj.getEmailid());
		request.setAttribute("address", eduStaffToObj.getAddress());
		request.setAttribute("phoneno", eduStaffToObj.getPhoneno());
		System.out.println("user_id" + eduStaffToObj.getUserid()
				+ "admissiondate" + eduStaffToObj.getJoiningdate() + "emailid"
				+ eduStaffToObj.getEmailid() + "address"
				+ eduStaffToObj.getAddress() + "phoneno"
				+ eduStaffToObj.getPhoneno() + "student name"
				+ eduStaffToObj.getFirstname());

		ArrayList<EduStudentReportTO> eduStudentReportToArrayObj = eduStaffManagerObj
				.getSubjectClassByUser(user_id);
		request.setAttribute("studentclassObj", eduStudentReportToArrayObj);
		logger.debug("Setup completed for Student Report Card............ "
				+ eduStaffToObj.getDept_id() + "Class Name"
				+ eduStudentReportToArrayObj.get(0).getClassname());
		return mapping.findForward("viewProfile");

	}

	public ActionForward meetingSetup(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out
				.println("meetingSetupmeetingSetupmeetingSetupmeetingSetup111111111");
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		System.out
				.println("meetingSetupmeetingSetupmeetingSetupmeetingSetup2222222222");
		return mapping.findForward("meetingSetup");

	}

	public ActionForward emailSetup(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
	
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		EduStaffForm eduStaffFormObj = (EduStaffForm) form;
		String str = null;
		String selectedOption = request.getParameter("meeting");
		String datetime = request.getParameter("basic_example_3");
		EduStaffTO eduStaffToObj1=null;
		EduStaffTO eduStaffToObj = new EduStaffTO();
		if(selectedOption.equals("student")){
			EduStaffManager eduStaffManagerObj = new EduStaffManager();
			eduStaffToObj1 = eduStaffManagerObj.getClassname(request.getParameter("class"));
			eduStaffToObj.setClassname(eduStaffToObj1.getClassname());
			
		}
		if (request.getParameterValues("selectedList") != null) {

			eduStaffFormObj.setEmailIds(request
					.getParameterValues("selectedList"));
			
			for (int xx = 0; xx < eduStaffFormObj.getEmailIds().length; xx++) {

				if (str != null)
					str = str+ ","+ eduStaffFormObj.getEmailIds()[xx].substring(eduStaffFormObj.getEmailIds()[xx].indexOf("<--->") + 5,eduStaffFormObj.getEmailIds()[xx].length());
				else
					str = eduStaffFormObj.getEmailIds()[xx].substring(eduStaffFormObj.getEmailIds()[xx].indexOf("<--->") + 5,eduStaffFormObj.getEmailIds()[xx].length());

			}
		
		}
		eduStaffToObj.setLevel(selectedOption);
		eduStaffToObj.setEmailid(str);
		eduStaffToObj.setJoiningdate(datetime);
		sendMeetingMail(eduStaffToObj);
		return mapping.findForward("meetingSetup");
	}
	
	 public void sendMeetingMail(EduStaffTO mailInfoObj)
	  {
		  logger.debug("Mail Sending Started For Mail Id............ "+mailInfoObj.getEmailid());
		 try{
		  Properties props = new Properties();
		  PortableConstant PortableConstantObj = new PortableConstant();
		  props.put("mail.smtp.host",PortableConstant.SMTP_HOST_NAME);
		  props.put("mail.smtp.auth","true");
		  //props.put("mail.debug","true");
		  props.put("mail.smtp.port",PortableConstant.SMTP_PORT);
		  props.put("mail.smtp.socketFactory.port",PortableConstant.SMTP_PORT);
		  props.put("mail.smtp.socketFactory.class",PortableConstant.SSL_FACTORY);
		  //props.put("mail.smtp.socketFactory.fallback","false");
		  final String emailId =  PortableConstantObj.getProperty(PortableConstant.EMAILFROMADDRESS);
		  final String password =  PortableConstantObj.getProperty(PortableConstant.EMAILFROMPASSWORD);
		  logger.debug("Email Id and Password used for PasswordAuthentication...... "+emailId+"       "+password);
		  Session session = Session.getDefaultInstance(props,
		  new javax.mail.Authenticator() {
		  protected PasswordAuthentication getPasswordAuthentication() {
		  return new PasswordAuthentication(emailId,password);
		  }
		  });
		  session.setDebug(PortableConstant.debug);
		  Message msg = new MimeMessage(session);
		  Message parentmsg = new MimeMessage(session);
		  InternetAddress addressFrom = new InternetAddress(PortableConstantObj.getProperty(PortableConstant.EMAILFROMADDRESS));
		  msg.setFrom(addressFrom);
		  parentmsg.setFrom(addressFrom);
		  msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailInfoObj.getEmailid()));
		  // Setting the Subject and Content Type
		  msg.setSubject(PortableConstantObj.getProperty(PortableConstant.MEETINGSUBJECTTXT)+" "+mailInfoObj.getJoiningdate());
		  //msg.setContent(PortableConstant.EMAILMSGTXT, "text/plain");
		  //msg.setContent("Hi, "+mailInfoObj.getFirstname()+" "+mailInfoObj.getMiddlename()+" "+mailInfoObj.getLastname(), "text/html");
		  String className=mailInfoObj.getClassname()!=null?mailInfoObj.getClassname():"all";
		  if(mailInfoObj.getLevel().equals("teacher"))
			  msg.setContent("Hi ,<br>"+PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT)+"!<br><br>We would like to invite all teacher meeting.<br><br>"+PortableConstantObj.getProperty(PortableConstant.REGARDS)+"<br>"+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL), "text/html");
		  else if(mailInfoObj.getLevel().equals("student"))
			  msg.setContent("Hi ,<br>"+PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT)+"!<br><br>We  would like to invite a meeting of "+className+" class student.<br><br>"+PortableConstantObj.getProperty(PortableConstant.REGARDS)+"<br>"+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL), "text/html");
		  else if(mailInfoObj.getLevel().equals("ateacher"))
			  msg.setContent("Hi ,<br>"+PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT)+"!<br><br>We would like to in alumni teachers.<br><br>"+PortableConstantObj.getProperty(PortableConstant.REGARDS)+"<br>"+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL), "text/html");
		  else if(mailInfoObj.getLevel().equals("astudent"))
			  msg.setContent("Hi ,<br>"+PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT)+"!<br><br>We would like to invite alumni students. <br><br>"+PortableConstantObj.getProperty(PortableConstant.REGARDS)+"<br>"+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL), "text/html");
		  else
			  msg.setContent("Hi ,<br>"+PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT)+"!<br><br>We would like to invite parent meeting.<br><br>"+PortableConstantObj.getProperty(PortableConstant.REGARDS)+"<br>"+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL), "text/html");
		  Transport.send(msg);
	    }
		 catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); 
				logger.debug("Mail Sending MessagingException sendMail :: "+e.getMessage());
			}
		 catch (Exception exp) {
				// TODO Auto-generated catch block
				exp.printStackTrace();
				logger.debug("Mail Sending Exception sendMail :: "+exp.getMessage());
			}
	  }

}