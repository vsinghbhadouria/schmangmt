package com.sch.action;
import javax.servlet.http.*;

import org.apache.log4j.Logger;
import org.apache.struts.action.*;

public class UserSessionLogOutAction extends Action

{
	public static Logger logger = Logger.getLogger(UserSessionLogOutAction.class);
	public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		HttpSession session = request.getSession();
		String User_name = session.getAttribute("user_name").toString();
		logger.debug("Session before invalidate==>User Name "+session.getAttribute("user_name")+" User Title "+session.getAttribute("ug_title"));
		session.invalidate();
		logger.debug("User "+User_name+" has lost the session");
		return mapping.findForward("userlogout");	
	}

}
