package com.sch.action;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.sch.delegates.LoginManager;
import com.sch.delegates.EduExitManager;
import com.sch.form.EduExitForm;
import com.sch.to.EduExitTO;
import java.io.FileNotFoundException;
import com.sch.constant.PortableConstant;
public class EduExitAction extends DispatchAction
{	
	private int f_Id = 7;
	private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
			Font.BOLD);
	private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
			Font.NORMAL, BaseColor.RED);
	private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
			Font.BOLDITALIC);
	private static Font smallBold = new Font(Font.FontFamily.HELVETICA, 10,
			Font.NORMAL);
	LoginManager loginManager =  new LoginManager();
	public static Logger logger = Logger.getLogger(EduExitAction.class);
	public ActionForward search(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("search");
	}
	
	
	public ActionForward save(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{

		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		HttpSession session = request.getSession();  
        EduExitForm eduExitFormObj = (EduExitForm)form;
        EduExitTO eduExitToObj = new EduExitTO();
	try {       
		   EduExitManager eduExitManagerObj = new EduExitManager();	
		   eduExitToObj.setStudentId(eduExitFormObj.getStudentId());
		   logger.debug("Get the student detail for leaving certificate :: Student_id "+eduExitToObj.getStudentId());
		   eduExitToObj = eduExitManagerObj.downloadCertificate(eduExitToObj);
		   logger.debug("First Name "+eduExitToObj.getFirstname()+" Last Name "+eduExitToObj.getLastname()+" Middle Name "+eduExitToObj.getMiddlename()+" Admission Date "+eduExitToObj.getAdmission_date());
		}
		    catch ( Exception exp ){
		    	exp.getMessage();
				logger.debug("Method save :: ERROR "+exp.getMessage());
		    	}
		    request.removeAttribute("Msg");
		    if(eduExitToObj.getStudentId()!=null && eduExitToObj.getStudentId().length()>0){
		    doDownload( mapping, form, request,response,eduExitToObj);
		    request.setAttribute("Msg"," ");
		    }
		    else{
		    	request.setAttribute("Msg","First Complete the exit formality");
		    }
		return mapping.findForward("save");
	}
	public void doDownload( ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response,EduExitTO eduExitToObj)throws Exception
	{
		
		Document document = new Document();
	   try{
		     PortableConstant PortableConstantObj = new PortableConstant();
		     PortableConstantObj.loadProperty();
			 String schoolName=PortableConstantObj.getProperty(PortableConstant.SCHOOLNAME);
			 String certificateName=PortableConstantObj.getProperty(PortableConstant.CERTIFICATENAME);
		     DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		     DateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy");
		  	 String filename=eduExitToObj.getFirstname()+"_"+eduExitToObj.getMiddlename()+"_"+eduExitToObj.getStudentId()+"_leavingsertificate.pdf";
		  	 logger.debug("Method doDownload :: fileName "+filename);
		   	 response.setContentType("application/pdf");
		   	// response.setHeader("Content-Disposition", "inline;filename=leavingsertificate.pdf");
		   	 response.setHeader("Content-disposition",
	                  "attachment; filename=" +
	                  filename );

		     PdfWriter.getInstance(document,response.getOutputStream());
             document.open();
             Paragraph schoolname = new Paragraph(schoolName,redFont);
             schoolname.setAlignment(Element.ALIGN_CENTER);
             addEmptyLine(schoolname,2);
             document.add(schoolname);
             Paragraph formname = new Paragraph(certificateName,subFont);
             addEmptyLine(formname,2);
             formname.setAlignment(Element.ALIGN_CENTER);
             document.add(formname);  
             Paragraph studentname = new Paragraph("This is the certify that ........"+eduExitToObj.getFirstname()+" "+eduExitToObj.getMiddlename()+" "+"...........",smallBold);
             studentname.setAlignment(Element.ALIGN_LEFT);
             addEmptyLine(studentname,1);  
             document.add(studentname);
             Paragraph fathername = new Paragraph("S/o,D/o, Shri ....................."+eduExitToObj.getFathername()+"..........",smallBold);
             fathername.setAlignment(Element.ALIGN_LEFT);
             addEmptyLine(fathername,1);
             document.add(fathername);
             Paragraph address = new Paragraph("Resident of .......... "+eduExitToObj.getAddress()+".......was admitted to this school for class "+eduExitToObj.getClassname()+" on " +
             df1.format(df.parse(eduExitToObj.getAdmission_date())),smallBold);
             address.setAlignment(Element.ALIGN_LEFT);
             addEmptyLine(address,1);
             document.add(address);
             Paragraph character = new Paragraph("He/She was found to be a sincere & Hard Working Student.His / Her Conduct was good as far as known to the undersigned.",smallBold);
             character.setAlignment(Element.ALIGN_LEFT);
             addEmptyLine(character,1);
             document.add(character);
             Paragraph wishes = new Paragraph("I wish him / her all success in life.",smallBold);
             wishes.setAlignment(Element.ALIGN_LEFT);
             addEmptyLine(wishes,1);
             document.add(wishes);
             Paragraph status = new Paragraph("The Student was involved / not involved in ragging.",smallBold);
             status.setAlignment(Element.ALIGN_LEFT);
             addEmptyLine(status,3);
             document.add(status);
             Paragraph signature = new Paragraph("Place................                              " +
             "                                                         Signature....................");
             signature.setAlignment(Element.ALIGN_LEFT);
             addEmptyLine(signature,1);
             document.add(signature);
             Paragraph disgnation= new Paragraph("Date.................                                " +
             "		                                                      Disgnation..................");
             disgnation.setAlignment(Element.ALIGN_LEFT);
             addEmptyLine(disgnation,1);
             document.add(disgnation);
			
	   }
	   catch (DocumentException exp) {
		   exp.printStackTrace();
           logger.debug("Method doDownload :: DocumentException "+exp.getMessage());
       } catch (FileNotFoundException exp) {
    	   exp.printStackTrace();
    	   logger.debug("Method doDownload :: FileNotFoundException "+exp.getMessage());
       } 
	   catch(Exception exp){
		   exp.printStackTrace();
		   logger.debug("Method doDownload :: Exception "+exp.getMessage());
	   }
	   finally {
           document.close();
       }
	}
	
	private static void addEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

}
