package com.sch.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.*;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.sch.constant.PortableConstant;
import com.sch.delegates.LibraryManagementManager;
import com.sch.delegates.LoginManager;
import com.sch.form.LibraryManagementForm;
import com.sch.to.LibraryManagementTO;

public class LibraryManagementAction extends DispatchAction {
	private int f_Id = 7;
	LoginManager loginManager = new LoginManager();
	public static Logger logger = Logger.getLogger(LibraryManagementAction.class);

	public ActionForward search(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("search");
	}

	public ActionForward createSetup(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		LibraryManagementForm libraryManagementFormObj = (LibraryManagementForm) form;
		LibraryManagementManager libraryManagementManagerObj = new LibraryManagementManager();
		ArrayList<LibraryManagementTO> bookTypes = libraryManagementManagerObj.getBooktypes();
		libraryManagementFormObj.setBookTypes(bookTypes);
		request.setAttribute("bookTypes",bookTypes);
		return mapping.findForward("createSetup");
	}
	
	public ActionForward save(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		LibraryManagementForm libraryManagementFormObj = (LibraryManagementForm) form;
		LibraryManagementManager libraryManagementManagerObj = new LibraryManagementManager();
		LibraryManagementTO libraryManagementTO = new LibraryManagementTO();
		libraryManagementTO.setBookname(request.getParameter("bookname"));
		libraryManagementTO.setBooktypeid(libraryManagementFormObj.getBooktypeid());
		libraryManagementTO.setBooktypename(libraryManagementFormObj.getBooktypename());
		libraryManagementTO.setNumberofcopy(libraryManagementFormObj.getNumberofcopy());
		libraryManagementTO.setRackid(libraryManagementFormObj.getRackid());
		if(libraryManagementFormObj.getBookid()!=null){
			String bookIds[] =libraryManagementFormObj.getBookid().split(" ");
			libraryManagementTO.setBookids(bookIds);
			
		}
		libraryManagementManagerObj.addLibraryBooks(libraryManagementTO,libraryManagementFormObj);
		ArrayList<LibraryManagementTO> bookTypes = libraryManagementManagerObj.getBooktypes();
		libraryManagementFormObj.setBookTypes(bookTypes);
		request.setAttribute("bookTypes",bookTypes);
		return mapping.findForward("createSetup");
	}
	
	
	public ActionForward bookList(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		LibraryManagementForm libraryManagementFormObj = (LibraryManagementForm) form;
		LibraryManagementManager libraryManagementManagerObj = new LibraryManagementManager();
		ArrayList<LibraryManagementTO> allbooks = libraryManagementManagerObj.getAllbooks();
		libraryManagementFormObj.setBookTypes(allbooks);
		request.setAttribute("allbooks",allbooks);
		return mapping.findForward("bookList");
	}
	
	public ActionForward issueSetup(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws Exception {
				if (request.getSession().getAttribute("user_name") == null) {
					return mapping.findForward("userlogout");
				}
				String ug_id = request.getSession().getAttribute("ug_id").toString();
				if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
					return mapping.findForward("userlogout");
				}
	return mapping.findForward("issueSetup");
	}
	
	public ActionForward assignBooks(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		LibraryManagementForm libraryManagementFormObj = (LibraryManagementForm) form;
		
		System.out.println("meeting "+request.getParameter("meeting")+"  "+libraryManagementFormObj.getBookid()+" "+request.getParameter("user_id")+"  "+request.getParameter("empId")+"  "+libraryManagementFormObj.getBookmasterid());
		LibraryManagementTO libraryManagementTO = new LibraryManagementTO();
		libraryManagementTO.setBookid(libraryManagementFormObj.getBookid());
		libraryManagementTO.setBookmasterid(libraryManagementFormObj.getBookmasterid());
		if(request.getParameter("meeting").equalsIgnoreCase("Teacher")){
		libraryManagementTO.setUserId(request.getParameter("empId"));
		libraryManagementTO.setUserType("T");
		}
		else{
			libraryManagementTO.setUserId(request.getParameter("user_id"));
			libraryManagementTO.setUserType("S");
		}
		LibraryManagementManager libraryManagementManagerObj = new LibraryManagementManager();
		libraryManagementManagerObj.assignBookToUser(libraryManagementTO);
		libraryManagementTO.setBookname(libraryManagementFormObj.getBookname());
		libraryManagementTO.setUserName(libraryManagementFormObj.getUsername());
		libraryManagementTO.setEmailId(libraryManagementFormObj.getEmailid());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		libraryManagementTO.setIssueDate(dateFormat.format(cal.getTime()));
		cal.add(Calendar.DATE, 15);
		libraryManagementTO.setDueDate(dateFormat.format(cal.getTime()));
		sendMail(libraryManagementTO,"A");
		return mapping.findForward("issueSetup");
	}
	
	public ActionForward assignedBooklist(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		LibraryManagementForm libraryManagementFormObj = (LibraryManagementForm) form;
		LibraryManagementManager libraryManagementManagerObj = new LibraryManagementManager();
		ArrayList<LibraryManagementTO> allbooks = libraryManagementManagerObj.getAssignedbooks();
		libraryManagementFormObj.setBookTypes(allbooks);
		request.setAttribute("allbooks",allbooks);
		return mapping.findForward("assignedBooklist");
	}
	
	public ActionForward returnSetup(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws Exception {
				if (request.getSession().getAttribute("user_name") == null) {
					return mapping.findForward("userlogout");
				}
				String ug_id = request.getSession().getAttribute("ug_id").toString();
				if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
					return mapping.findForward("userlogout");
				}
		
	return mapping.findForward("returnSetup");
	}
	
	public ActionForward returnBook(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws Exception {
				if (request.getSession().getAttribute("user_name") == null) {
					return mapping.findForward("userlogout");
				}
				String ug_id = request.getSession().getAttribute("ug_id").toString();
				if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
					return mapping.findForward("userlogout");
				}
				LibraryManagementForm libraryManagementFormObj = (LibraryManagementForm) form;
				LibraryManagementManager libraryManagementManagerObj = new LibraryManagementManager();
				LibraryManagementTO libraryManagementTO = new LibraryManagementTO();
				libraryManagementTO.setBookid(request.getParameter("bookId"));
				libraryManagementTO.setUserId(libraryManagementFormObj.getUserId());
				System.out.println("userid "+libraryManagementFormObj.getUserId());
				LibraryManagementTO returnBookObj = libraryManagementManagerObj.returnBook(libraryManagementTO);
				ArrayList<LibraryManagementTO> allbooks = libraryManagementManagerObj.getReturnedbooks();
				libraryManagementFormObj.setBookTypes(allbooks);
				request.setAttribute("allbooks",allbooks);
				sendMail(returnBookObj,"R");
				return mapping.findForward("returnedBooklist");
	}
	
	public ActionForward returnedBooklist(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (request.getSession().getAttribute("user_name") == null) {
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if (!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)) {
			return mapping.findForward("userlogout");
		}
		LibraryManagementForm libraryManagementFormObj = (LibraryManagementForm) form;
		LibraryManagementManager libraryManagementManagerObj = new LibraryManagementManager();
		ArrayList<LibraryManagementTO> allbooks = libraryManagementManagerObj.getReturnedbooks();
		libraryManagementFormObj.setBookTypes(allbooks);
		request.setAttribute("allbooks",allbooks);
		return mapping.findForward("returnedBooklist");
	}
	
	public void sendMail(LibraryManagementTO libraryManagementTO,String token){
		
		System.out.println("User Name "+libraryManagementTO.getUserName()+" Book Name "+libraryManagementTO.getBookname());
		System.out.println("Book Id "+libraryManagementTO.getBookid()+" "+libraryManagementTO.getEmailId());
		System.out.println("Book Id "+libraryManagementTO.getIssueDate()+" "+libraryManagementTO.getDueDate());
		String message=null;
		  logger.debug("Mail Sending Started............ "+libraryManagementTO.getUserName()+" "+libraryManagementTO.getBookname()+" "+libraryManagementTO.getEmailId());
			 try{
			  Properties props = new Properties();
			  PortableConstant PortableConstantObj = new PortableConstant();
			  props.put("mail.smtp.host",PortableConstant.SMTP_HOST_NAME);
			  props.put("mail.smtp.auth","true");
			  //props.put("mail.debug","true");
			  props.put("mail.smtp.port",PortableConstant.SMTP_PORT);
			  props.put("mail.smtp.socketFactory.port",PortableConstant.SMTP_PORT);
			  props.put("mail.smtp.socketFactory.class",PortableConstant.SSL_FACTORY);
			  //props.put("mail.smtp.socketFactory.fallback","false");
			  final String emailId =  PortableConstantObj.getProperty(PortableConstant.EMAILFROMADDRESS);
			  final String password =  PortableConstantObj.getProperty(PortableConstant.EMAILFROMPASSWORD);
			  logger.debug("Email Id and Password used for PasswordAuthentication...... "+emailId+"       "+password);
			  Session session = Session.getDefaultInstance(props,
			  new javax.mail.Authenticator() {
			  protected PasswordAuthentication getPasswordAuthentication() {
			  return new PasswordAuthentication(emailId,password);
			  }
			  });
			  session.setDebug(PortableConstant.debug);
			  Message msg = new MimeMessage(session);
			  InternetAddress addressFrom = new InternetAddress(PortableConstantObj.getProperty(PortableConstant.EMAILFROMADDRESS));
			  msg.setFrom(addressFrom);
			  InternetAddress addressTo = new InternetAddress (libraryManagementTO.getEmailId());
			  msg.setRecipient(Message.RecipientType.TO, addressTo);
			  String itemColumn=null;
			  String itemDetails=null;
			  if(token=="A"){
				  msg.setSubject(PortableConstantObj.getProperty(PortableConstant.EMAILBOOKISSUESUBJECTTXT)+libraryManagementTO.getUserId());
				  message="!<br><br>The following document borrowed from Information Resource Centre.";
				  itemColumn="<table><tbody><tr><td><b>&nbsp;&nbsp;Material No&nbsp;&nbsp;</b></td><td>&nbsp;&nbsp;<b>Title</b>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;<b>Date of Issue</b>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;<b>Due Date</b>&nbsp;&nbsp;</td></tr>";
				  itemDetails="<tr><td>&nbsp;&nbsp;"+libraryManagementTO.getBookid()+"&nbsp;&nbsp;</td><td>&nbsp;&nbsp;"+libraryManagementTO.getBookname()+"&nbsp;&nbsp;</td><td>&nbsp;&nbsp;"+libraryManagementTO.getIssueDate()+"&nbsp;&nbsp;</td><td>&nbsp;&nbsp;"+libraryManagementTO.getDueDate()+"&nbsp;&nbsp;</td></tr></tbody></table>";
				
			  }
			  else if(token=="R"){
				  msg.setSubject(PortableConstantObj.getProperty(PortableConstant.EMAILBOOKRETURNSUBJECTTXT)+libraryManagementTO.getUserId());
				  message="!<br><br>The following document borrowed from Information Resource Centre is returned on "+libraryManagementTO.getReturnDate();
				  itemColumn="<table><tbody><tr><td><b>&nbsp;&nbsp;Material No&nbsp;&nbsp;</b></td><td>&nbsp;&nbsp;<b>Title</b>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;<b>Date of Issue</b>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;<b>Due Date</b><td>&nbsp;&nbsp;<b>Return Date</b</td></tr>";
				  itemDetails="<tr><td>&nbsp;&nbsp;"+libraryManagementTO.getBookid()+"&nbsp;&nbsp;</td><td>&nbsp;&nbsp;"+libraryManagementTO.getBookname()+"&nbsp;&nbsp;</td><td>&nbsp;&nbsp;"+libraryManagementTO.getIssueDate()+"&nbsp;&nbsp;</td><td>&nbsp;&nbsp;"+libraryManagementTO.getDueDate()+"&nbsp;&nbsp;</td><td>&nbsp;&nbsp;"+libraryManagementTO.getReturnDate()+"&nbsp;&nbsp;</td></tr></tbody></table>";
			  }  
			  msg.setContent("Hi "+libraryManagementTO.getUserName()+","+"<br>"+PortableConstantObj.getProperty(PortableConstant.LAIBRARYSERVICES)+message+"<br><br>&nbsp;&nbsp;Item Details<br>"+itemColumn+"<br>"+itemDetails+"<br><br>"+PortableConstantObj.getProperty(PortableConstant.REGARDS)+"<br>"+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL), "text/html");
			  Transport.send(msg);
		    }
			 catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace(); 
					logger.debug("Mail Sending MessagingException sendMail :: "+e.getMessage());
				}
			 catch (Exception exp) {
					// TODO Auto-generated catch block
					exp.printStackTrace();
					logger.debug("Mail Sending Exception sendMail :: "+exp.getMessage());
				}
		  }
}