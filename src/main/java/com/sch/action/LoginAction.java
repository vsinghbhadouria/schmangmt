package com.sch.action;
import javax.servlet.http.*;

import org.apache.log4j.Logger;
import org.apache.struts.action.*;
import org.apache.struts.actions.DispatchAction;

import com.sch.constant.PortableConstant;
import com.sch.delegates.LoginManager;
import com.sch.form.LoginForm;
import com.sch.to.LoginTO;
public class LoginAction extends DispatchAction
{
	public static Logger logger = Logger.getLogger(LoginAction.class);
	public ActionForward logIn(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
		String key = null;
		HttpSession session = request.getSession(true);
		LoginForm objLoginForm = (LoginForm)form;
		LoginManager objloginmanager = new LoginManager();
		logger.error("Login in process......error..........");
		logger.debug("Login in process......debug..........");
		logger.info("Login in process......info..........");
		logger.fatal("Login in process......fatal..........");
		logger.trace("Login in process......trace..........");
		LoginTO objloginto = objloginmanager.checkLogin(objLoginForm.getLoginid(),objLoginForm.getPassword(),objLoginForm.getUserType());
		logger.debug("Login in done for USER==>"+objloginto.getUsername()+" "+objloginto.getUserlastname()+" USER_ID "+objloginto.getUserid());
		if(objloginto.getUserid()!=null)
		{   
			session.setAttribute("user_id",objloginto.getUserid());
			session.setAttribute("user_name",objloginto.getUsername());
			session.setAttribute("user_last_name", objloginto.getUserlastname());
			session.setAttribute("ug_title",objloginto.getUg_title());
			session.setAttribute("ug_id",objloginto.getUg_id());
			session.setAttribute("welcome_msg", objloginto.getWelcome_msg());
			session.setAttribute("ugCode", objloginto.getUgCode());
			objloginmanager.loginUserInfo(objloginto);
			key = "logIn";
			PortableConstant portableConstant = new PortableConstant();
			portableConstant.loadProperty();
		}
		else
		{
		   
		   session.setAttribute("errorMsg","Authentication failed,invalid Login Information");
		   key = "errorlogin";
			
		}
		logger.debug("User Group Title==>"+objloginto.getUg_title()+" User Group ID "+objloginto.getUg_id()+" User Group Code "+objloginto.getUgCode());
		return mapping.findForward(key);	
	}
	public ActionForward changePassword(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("changePassword");	
	}
	public ActionForward savePassword(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
		HttpSession session = request.getSession(true);
		if(session.getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		LoginManager LoginManagerObj = new LoginManager();
		LoginForm objLoginForm = (LoginForm)form;
		LoginTO loginToObj = new LoginTO();
		loginToObj.setNewpassword(objLoginForm.getNewpassword());
		loginToObj.setPassword(objLoginForm.getPassword());
		loginToObj.setUserid(session.getAttribute("user_id").toString());
		loginToObj.setUg_id(session.getAttribute("ug_id").toString());
		LoginManagerObj.savePassword(loginToObj);
		return mapping.findForward("savePassword");	
	}
	public ActionForward loginUsers(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("loginUsers");	
	}
	public ActionForward home(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("home");	
	}
	
	public ActionForward loginSetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
		return mapping.findForward("loginSetup");	
	}
	
	public ActionForward forgetPassword(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	{
		
		return mapping.findForward("forgetPassword");	
	}
}