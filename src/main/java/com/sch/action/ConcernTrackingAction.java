package com.sch.action;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.sch.delegates.ConcernTrackingManager;
import com.sch.delegates.LoginManager;
import com.sch.form.ConcernTrackingForm;
import com.sch.to.ConcernTrackingTO;
public class ConcernTrackingAction extends DispatchAction
{
	
	
	private int f_Id = 7;
	LoginManager loginManager =  new LoginManager();
    public static Logger logger = Logger.getLogger(ConcernTrackingAction.class);
	// Logging object and utility method
	public ActionForward search(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		logger.debug("Starting to loading the Concern Data");
		return mapping.findForward("search");
	}
	public ActionForward save(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		ConcernTrackingForm objissuetracking = (ConcernTrackingForm)form;
		ConcernTrackingTO objissuetrackingto= new ConcernTrackingTO();
		objissuetrackingto.setUsername(request.getSession().getAttribute("user_name").toString());
		objissuetrackingto.setUserid(request.getSession().getAttribute("user_id").toString());
		objissuetrackingto.setIssuetitle(objissuetracking.getIssuetitle());
		objissuetrackingto.setCategory(objissuetracking.getCategory());
		objissuetrackingto.setIssuedesc(objissuetracking.getIssuedesc());
		objissuetrackingto.setPriority(objissuetracking.getPriority());
		ConcernTrackingManager objissuemanager = new ConcernTrackingManager();
		logger.debug("Save: Before calling saveIssue Method");
		objissuemanager.saveIssue(objissuetrackingto);
		logger.debug("Save: after Calling saveIssue Method");
		return mapping.findForward("save");
	}
	public ActionForward createSetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("createSetup");
	}
	public ActionForward updateSetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		ConcernTrackingManager objissuemanager = new ConcernTrackingManager();
		ConcernTrackingTO objissueto = objissuemanager.getConcernByissueId(request.getParameter("Issue_id"));
		ConcernTrackingForm objissuetracking = (ConcernTrackingForm)form;
		objissuetracking.setIssuetitle(objissueto.getIssuetitle());
		objissuetracking.setIssuedesc(objissueto.getIssuedesc());
		objissuetracking.setCategory(objissueto.getCategory());
		objissuetracking.setPriority(objissueto.getPriority());
		objissuetracking.setStatus(objissueto.getStatus());
		objissuetracking.setIssueid(objissueto.getIssueid());
		objissuetracking.setReplydesc(objissueto.getReplydesc());
		request.setAttribute("ansdisc", objissueto.getReplydesc());
		request.setAttribute("category", objissueto.getCategory());
		request.setAttribute("statusid", objissueto.getStatusid());
		request.setAttribute("priority", objissueto.getPriority());
		request.setAttribute("issuetitle",objissueto.getIssuetitle());
		request.setAttribute("issuedisc", objissueto.getIssuedesc());
		request.setAttribute("concernSubmittedDate", objissueto.getIssuesubmiteddate());
		request.setAttribute("issueid", objissueto.getIssueid());
		request.setAttribute("catId", objissueto.getCatid());
		
		request.setAttribute("issuesubmit", objissueto.getIssuesubmiteddate()!=null? objissueto.getIssuesubmiteddate():"N/A");
		request.setAttribute("issuereply", objissueto.getIssuerepydate()!=null? objissueto.getIssuerepydate():"N/A");
		
		ArrayList<ConcernTrackingTO> allStatus = objissuemanager.getConcernStatus();
		request.setAttribute("allStatus",allStatus);
		
		ArrayList<ConcernTrackingTO> allCategory = objissuemanager.getCategory();
		request.setAttribute("allCategory",allCategory);
		
		logger.debug("update Setup "+objissueto.getIssueid()+"Status=="+allStatus.size());
	return mapping.findForward("updateSetup");
	}
	public ActionForward edit(ActionMapping mapping,ActionForm form,HttpServletRequest request ,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		System.out.println("with in elit method");
		ConcernTrackingManager objissuemanager = new ConcernTrackingManager();
		ConcernTrackingForm objissuetracking = (ConcernTrackingForm)form;
		ConcernTrackingTO objissuetrackingto= new ConcernTrackingTO();
		objissuetrackingto.setIssuetitle(objissuetracking.getIssuetitle());
		objissuetrackingto.setIssuedesc(objissuetracking.getIssuedesc());
		objissuetrackingto.setCategory(objissuetracking.getCategory());
		objissuetrackingto.setStatus(objissuetracking.getStatus());
		objissuetrackingto.setPriority(objissuetracking.getPriority());
		objissuetrackingto.setIssueid(objissuetracking.getIssueid());
		objissuetrackingto.setReplydesc(objissuetracking.getReplydesc());
		System.out.println("before call of editIssueTrackingByIssueId");
		objissuemanager.editIssueTrackingByIssueId(objissuetrackingto);
	  return mapping.findForward("edit");
	}
	public ActionForward delete(ActionMapping mapping,ActionForm form,HttpServletRequest request ,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		System.out.println("with in elit method");
		ConcernTrackingManager objissuemanager = new ConcernTrackingManager();
		System.out.println("objissuetrackingto"+request.getParameter("Issue_id"));
		objissuemanager.deleteIssueTracking(request.getParameter("Issue_id"));
		return mapping.findForward("delete");
	}
	public ActionForward replySetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		ConcernTrackingManager objissuemanager = new ConcernTrackingManager();
		ConcernTrackingTO objissueto = objissuemanager.getConcernByissueId(request.getParameter("Issue_id"));
		ConcernTrackingForm objissuetracking = (ConcernTrackingForm)form;
		objissuetracking.setIssuetitle(objissueto.getIssuetitle());
		objissuetracking.setIssuedesc(objissueto.getIssuedesc());
		objissuetracking.setCategory(objissueto.getCategory());
		objissuetracking.setPriority(objissueto.getPriority());
		objissuetracking.setStatus(objissueto.getStatus());
		objissuetracking.setIssueid(objissueto.getIssueid());
		objissuetracking.setReplydesc(objissueto.getReplydesc());
		String ansdisc = objissueto.getReplydesc()!=null?objissueto.getReplydesc().replace("'","\\'"):"N/A";
		String issuedisc = objissueto.getIssuedesc()!=null?objissueto.getIssuedesc().replace("'","\\'"):"N/A";
		//System.out.println("<<<<<<<<issue_id>>>>>>>"+request.getParameter("IssueId"));
		request.setAttribute("ansdisc", objissueto.getReplydesc());
		request.setAttribute("category", objissueto.getCategory());
		request.setAttribute("statusid", objissueto.getStatusid());
		request.setAttribute("priority", objissueto.getPriority());
		request.setAttribute("issuetitle",objissueto.getIssuetitle());
		request.setAttribute("issuedisc", objissueto.getIssuedesc());
		request.setAttribute("concernSubmittedDate", objissueto.getIssuesubmiteddate());
		request.setAttribute("issueid", objissueto.getIssueid());
		request.setAttribute("issuesubmit", objissueto.getIssuesubmiteddate()!=null? objissueto.getIssuesubmiteddate():"N/A");
		request.setAttribute("issuereply", objissueto.getIssuerepydate()!=null? objissueto.getIssuerepydate():"N/A");
		
		ArrayList<ConcernTrackingTO> allStatus = objissuemanager.getConcernStatus();
		request.setAttribute("allStatus",allStatus);
		logger.debug("View Setup "+objissueto.getIssueid()+"Status=="+allStatus.size());

		return mapping.findForward("replySetup");
	}
	public ActionForward reply(ActionMapping mapping,ActionForm form,HttpServletRequest request ,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss"); 
		ConcernTrackingManager objissuemanager = new ConcernTrackingManager();
		ConcernTrackingForm objissuetracking = (ConcernTrackingForm)form;
		System.out.println("with in reply method"+request.getParameter("issueid")+"   "+objissuetracking.getReplydesc());
		ConcernTrackingTO objissuetrackingto= new ConcernTrackingTO();
		objissuetrackingto.setStatus(objissuetracking.getStatus());
		objissuetrackingto.setReplydesc(objissuetracking.getReplydesc());
		objissuetrackingto.setIssueid(objissuetracking.getIssueid());
		System.out.println("before call of editIssueTrackingByIssueId");
		objissuemanager.replyConcernByIssueId(objissuetrackingto);
		
		return mapping.findForward("reply");
	}
	
	public ActionForward viewSetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		ConcernTrackingManager objissuemanager = new ConcernTrackingManager();
		ConcernTrackingTO objissueto = objissuemanager.getConcernByissueId(request.getParameter("Issue_id"));
		ConcernTrackingForm objissuetracking = (ConcernTrackingForm)form;
		objissuetracking.setIssuetitle(objissueto.getIssuetitle());
		objissuetracking.setIssuedesc(objissueto.getIssuedesc());
		objissuetracking.setCategory(objissueto.getCategory());
		objissuetracking.setPriority(objissueto.getPriority());
		objissuetracking.setStatus(objissueto.getStatus());
		objissuetracking.setIssueid(objissueto.getIssueid());
		objissuetracking.setReplydesc(objissueto.getReplydesc());
		String ansdisc = objissueto.getReplydesc()!=null?objissueto.getReplydesc().replace("'","\\'"):"N/A";
		String issuedisc = objissueto.getIssuedesc()!=null?objissueto.getIssuedesc().replace("'","\\'"):"N/A";
		//System.out.println("<<<<<<<<issue_id>>>>>>>"+request.getParameter("IssueId"));
		request.setAttribute("ansdisc", objissueto.getReplydesc());
		request.setAttribute("category", objissueto.getCategory());
		request.setAttribute("status", objissueto.getStatus());
		request.setAttribute("priority", objissueto.getPriority());
		request.setAttribute("issuetitle",objissueto.getIssuetitle());
		request.setAttribute("issuedisc", objissueto.getIssuedesc());
		request.setAttribute("issueid", objissueto.getIssueid());
		request.setAttribute("concernSubmittedDate", objissueto.getIssuesubmiteddate());
		request.setAttribute("issuesubmit", objissueto.getIssuesubmiteddate()!=null? objissueto.getIssuesubmiteddate():"N/A");
		request.setAttribute("issuereply", objissueto.getIssuerepydate()!=null? objissueto.getIssuerepydate():"N/A");
		logger.debug("View Setup "+objissueto.getIssueid());
		return mapping.findForward("viewSetup");
		
	}
	public ActionForward deleteReply(ActionMapping mapping,ActionForm form,HttpServletRequest request ,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		System.out.println("with in elit method");
		ConcernTrackingManager objissuemanager = new ConcernTrackingManager();
		ConcernTrackingForm objissuetracking = (ConcernTrackingForm)form;
		ConcernTrackingTO objissuetrackingto= new ConcernTrackingTO();
		System.out.println("objissuetrackingto"+request.getParameter("reply_id"));
		objissuemanager.deleteIssuereply(request.getParameter("reply_id"));
		return mapping.findForward("deleteReply");
	}
	
	public ActionForward EditSetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		ConcernTrackingManager objissuemanager = new ConcernTrackingManager();
		ConcernTrackingTO objissueto = objissuemanager.getDataByreplyId(request.getParameter("reply_id"));
		ConcernTrackingForm objissuetracking = (ConcernTrackingForm)form;
		objissuetracking.setAnsdisc(objissueto.getAnsdisc());
		objissuetracking.setStatus(objissueto.getStatus());
		objissuetracking.setReplyid(objissueto.getReplyid());
		objissuetracking.setIssueid(objissueto.getIssueid());
		request.setAttribute("status",objissueto.getStatus());
		return mapping.findForward("EditSetup");
	}
	
	public ActionForward editReply(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		ConcernTrackingManager objissuemanager = new ConcernTrackingManager();
		ConcernTrackingForm objissuetracking = (ConcernTrackingForm)form;
		ConcernTrackingTO objissuetrackingto= new ConcernTrackingTO();
		objissuetrackingto.setStatus(objissuetracking.getStatus());
		objissuetrackingto.setIssueid(objissuetracking.getIssueid());
		objissuetrackingto.setReplyid(objissuetracking.getReplyid());
		objissuetrackingto.setAnsdisc(objissuetracking.getAnsdisc());
		objissuemanager.editIssueReplyByIssueId(objissuetrackingto);
		return mapping.findForward("editReply");
	}
	

	
}
