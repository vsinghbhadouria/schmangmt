package com.sch.action;			
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.*;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.sch.constant.PortableConstant;
import com.sch.delegates.EduStudentManager;
import com.sch.delegates.EduStudentReportManager;
import com.sch.delegates.LoginManager;
import com.sch.delegates.EduRegistrationManager;
import com.sch.form.EduRegistrationForm;
import com.sch.form.EduStudentReportForm;
import com.sch.to.EduRegistrationTO;
import com.sch.to.EduStudentReportTO;
import com.sch.to.EduStudentTO;


public class EduRegistrationAction extends DispatchAction
{	
	private int f_Id = 2;
	LoginManager loginManager =  new LoginManager();
    Logger logger = Logger.getLogger(EduRegistrationAction.class);
	public ActionForward search(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null){
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		return mapping.findForward("search");
	}
	
	
	public ActionForward createSetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		PortableConstant PortableConstantObj = new PortableConstant();
		logger.debug("Setup in process for registration............ ");
		EduRegistrationForm eduRegistrationFormObj = (EduRegistrationForm)form;
		EduRegistrationManager eduRegistrationManagerObj = new EduRegistrationManager();
		ArrayList<EduRegistrationTO> studprequalifications = eduRegistrationManagerObj.getRegistrationQualification();
		eduRegistrationFormObj.setStudprequalifications(studprequalifications);
		request.setAttribute("studprequalifications",studprequalifications);
		ArrayList<EduRegistrationTO> parentqualifications = eduRegistrationManagerObj.getParentHigherQualification();
		eduRegistrationFormObj.setParentqualifications(parentqualifications);
		request.setAttribute("parentqualifications",parentqualifications);
		ArrayList<EduRegistrationTO> classes = eduRegistrationManagerObj.getClassInfo();
		eduRegistrationFormObj.setClasses(classes);
		request.setAttribute("classes",classes);
		ArrayList<EduRegistrationTO> departments = eduRegistrationManagerObj.getDepartment();
		eduRegistrationFormObj.setDepartments(departments);
		request.setAttribute("departments",departments);
		ArrayList<EduRegistrationTO> admissionfees = eduRegistrationManagerObj.getAdmissionfees();
		eduRegistrationFormObj.setAdmissionfees(admissionfees);
		request.setAttribute("admissionfees",admissionfees);
		request.setAttribute("startyear",PortableConstantObj.getProperty(PortableConstant.DATEPICKERSTARTYEAR));
		request.setAttribute("currentyear",PortableConstantObj.getProperty(PortableConstant.DATEPICKERCURRENTYEAR));
		logger.debug("Setup completed for registration............ ");
		return mapping.findForward("createSetup");
	}
	public ActionForward save(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{

		/*if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}*/
	try {       
		    	  
				  EduRegistrationForm eduRegistrationFormObj = (EduRegistrationForm)form;
		          EduRegistrationTO eduRegistrationToObj = new EduRegistrationTO();
		          EduRegistrationManager eduRegistrationManagerObj = new EduRegistrationManager();
		          eduRegistrationToObj.setFirstname(eduRegistrationFormObj.getFirstname());
		          eduRegistrationToObj.setMiddlename(eduRegistrationFormObj.getMiddlename());
		          eduRegistrationToObj.setLastname(eduRegistrationFormObj.getLastname());
		          eduRegistrationToObj.setPhoneno(eduRegistrationFormObj.getPhoneno());
		          eduRegistrationToObj.setFathername(eduRegistrationFormObj.getFathername());
		          eduRegistrationToObj.setMothername(eduRegistrationFormObj.getMothername());
		          eduRegistrationToObj.setEmailid(eduRegistrationFormObj.getEmailid());
		          eduRegistrationToObj.setParentemailid(eduRegistrationFormObj.getParentemailid());
		          eduRegistrationToObj.setDateofbirth(eduRegistrationFormObj.getDateofbirth());
		          eduRegistrationToObj.setAddress(eduRegistrationFormObj.getAddress());
		          eduRegistrationToObj.setRegistrationdate(eduRegistrationFormObj.getRegistrationdate());
		          eduRegistrationToObj.setStudprequalification(eduRegistrationFormObj.getStudprequalification());
		          eduRegistrationToObj.setParentqualification(eduRegistrationFormObj.getParentqualification());
		          eduRegistrationToObj.setPreviousinstitute(eduRegistrationFormObj.getPreviousinstitute());
		          eduRegistrationToObj.setParentqualification(eduRegistrationFormObj.getParentqualification());
		          eduRegistrationToObj.setClassid(Integer.valueOf(eduRegistrationFormObj.getEduclass()));
		          eduRegistrationToObj.setRegistrationfee(eduRegistrationFormObj.getRegistrationfee());
		          eduRegistrationToObj.setGender(eduRegistrationFormObj.getGender());
		          eduRegistrationToObj.setConcessionamt(eduRegistrationFormObj.getConcessionamt());
		          eduRegistrationToObj.setComment(eduRegistrationFormObj.getComment());
		          eduRegistrationToObj.setBirthday(eduRegistrationFormObj.getBirthday());
		          eduRegistrationToObj.setBirthmonth(eduRegistrationFormObj.getBirthmonth());
		          eduRegistrationToObj.setBirthyear(eduRegistrationFormObj.getBirthyear());
		          eduRegistrationToObj.setRegistrationday(eduRegistrationFormObj.getRegistrationday());
		          eduRegistrationToObj.setRegistrationmonth(eduRegistrationFormObj.getRegistrationmonth());
		          eduRegistrationToObj.setRegistrationyear(eduRegistrationFormObj.getRegistrationyear());
		          eduRegistrationToObj.setDept_id(Integer.parseInt(eduRegistrationFormObj.getDepartment()));
		          eduRegistrationToObj.setRegstatus(eduRegistrationFormObj.getRegstatus());
		          logger.debug("Insertion in process for registration............ ");
		          eduRegistrationManagerObj.saveEduRegistrationData(eduRegistrationToObj);
		          logger.debug("Insertion in completed for registration............ ");	
		          
	}
		    catch ( Exception exp ){
		    	exp.getMessage();
		    	logger.debug("Method save :: ERROR "+exp.getMessage());	
		    	}
		return mapping.findForward("save");
	}

	public ActionForward updateSetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null){
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		 logger.debug("updateSetup in process for registration............ ");	
		 PortableConstant PortableConstantObj = new PortableConstant();
		 Calendar cal = Calendar.getInstance();
		 DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 System.out.println(request.getParameter("User_id")+","+request.getParameter("Session_id"));
		 EduRegistrationForm eduRegistrationFormObj = (EduRegistrationForm)form;
	     EduRegistrationManager eduRegistrationManagerObj = new EduRegistrationManager();
	     EduRegistrationTO eduRegistrationToObj = eduRegistrationManagerObj.getEduRegistrationDatabyUserId(request.getParameter("User_id"),request.getParameter("Session_id"),request.getParameter("Reg_status")); 
	     eduRegistrationFormObj.setFirstname(eduRegistrationToObj.getFirstname());
	     eduRegistrationFormObj.setRegistrationid(eduRegistrationToObj.getRegistrationid());
	     eduRegistrationFormObj.setLastname(eduRegistrationToObj.getLastname());
	     eduRegistrationFormObj.setMiddlename(eduRegistrationToObj.getMiddlename());
	     eduRegistrationFormObj.setPhoneno(eduRegistrationToObj.getPhoneno());
	     eduRegistrationFormObj.setAddress(eduRegistrationToObj.getAddress());
	     eduRegistrationFormObj.setRegistrationdate(eduRegistrationToObj.getRegistrationdate());
	   	 eduRegistrationFormObj.setEmailid(eduRegistrationToObj.getEmailid());
	     eduRegistrationFormObj.setFathername(eduRegistrationToObj.getFathername());
	     eduRegistrationFormObj.setClassid(eduRegistrationToObj.getClassid());
	     eduRegistrationFormObj.setDateofbirth(eduRegistrationToObj.getDateofbirth());
	     eduRegistrationFormObj.setGender(eduRegistrationToObj.getGender());
	     ArrayList<EduRegistrationTO> classes = eduRegistrationManagerObj.getClassInfo(request.getParameter("User_id"));
	     eduRegistrationFormObj.setFathername(eduRegistrationToObj.getFathername());
	     eduRegistrationFormObj.setMothername(eduRegistrationToObj.getMothername());
	     eduRegistrationFormObj.setParentemailid(eduRegistrationToObj.getParentemailid());
	     eduRegistrationFormObj.setClassid(eduRegistrationToObj.getClassid());
	     eduRegistrationFormObj.setPreviousinstitute(eduRegistrationToObj.getPreviousinstitute());
	     eduRegistrationFormObj.setParentqualification_id(eduRegistrationToObj.getParentqualification_id());
	     ArrayList<EduRegistrationTO> studprequalifications = eduRegistrationManagerObj.getRegistrationQualification();
		 eduRegistrationFormObj.setStudprequalifications(studprequalifications);
		 request.setAttribute("studprequalifications",studprequalifications);
		 request.setAttribute("studprequalification_id",eduRegistrationToObj.getStudprequalification_id());
		 ArrayList<EduRegistrationTO> parentqualifications = eduRegistrationManagerObj.getParentHigherQualification();
		 eduRegistrationFormObj.setParentqualifications(parentqualifications);
		 request.setAttribute("parentqualifications",parentqualifications);
		 request.setAttribute("parentqualification_id",eduRegistrationToObj.getParentqualification_id());
		 request.setAttribute("startyear",PortableConstantObj.getProperty(PortableConstant.DATEPICKERSTARTYEAR));
		 request.setAttribute("currentyear",PortableConstantObj.getProperty(PortableConstant.DATEPICKERCURRENTYEAR));
		 eduRegistrationFormObj.setClasses(classes);
		 request.setAttribute("classes",classes);
		 request.setAttribute("classid",eduRegistrationToObj.getClassid());
		 request.setAttribute("startyear",PortableConstantObj.getProperty(PortableConstant.DATEPICKERSTARTYEAR));
		 request.setAttribute("currentyear",PortableConstantObj.getProperty(PortableConstant.DATEPICKERCURRENTYEAR));
		 cal.setTime(dateFormat.parse(eduRegistrationFormObj.getDateofbirth()));
		 cal.add(Calendar.MONTH,1);
		 request.setAttribute("birthdate",dateFormat.format(cal.getTime()));
		 cal.setTime(dateFormat.parse(eduRegistrationFormObj.getRegistrationdate()));
		 cal.add(Calendar.MONTH,1);
		 request.setAttribute("registrationdate",dateFormat.format(cal.getTime()));
		 ArrayList<EduRegistrationTO> departments = eduRegistrationManagerObj.getDepartment();
		 request.setAttribute("deptid",eduRegistrationToObj.getDept_id());
		 eduRegistrationFormObj.setDepartments(departments);
		 request.setAttribute("departments",departments);
		 logger.debug("updateSetup is completed for registration............ ");	
		 return mapping.findForward("updateSetup");
	}

   public ActionForward update(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
   {
	   if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
	   String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
	   EduRegistrationForm eduRegistrationFormObj = (EduRegistrationForm)form;
	   EduRegistrationManager eduRegistrationManagerObj = new EduRegistrationManager();
	   EduRegistrationTO eduRegistrationToObj = new EduRegistrationTO();
	   try
	   {
	   	 eduRegistrationToObj.setFirstname(eduRegistrationFormObj.getFirstname());
	     eduRegistrationToObj.setLastname(eduRegistrationFormObj.getLastname());
	     eduRegistrationToObj.setPhoneno(eduRegistrationFormObj.getPhoneno());
	     eduRegistrationToObj.setAddress(eduRegistrationFormObj.getAddress());
	     eduRegistrationToObj.setRegistrationdate(eduRegistrationFormObj.getRegistrationdate());
	     eduRegistrationToObj.setMiddlename(eduRegistrationFormObj.getMiddlename());
	     eduRegistrationToObj.setEmailid(eduRegistrationFormObj.getEmailid());
	     eduRegistrationToObj.setFathername(eduRegistrationFormObj.getFathername());
	     eduRegistrationToObj.setEduclass(eduRegistrationFormObj.getEduclass());
	     eduRegistrationToObj.setDateofbirth(eduRegistrationFormObj.getDateofbirth());
	     eduRegistrationToObj.setGender(eduRegistrationFormObj.getGender());
	     eduRegistrationToObj.setRegistrationid(eduRegistrationFormObj.getRegistrationid());
	     logger.debug("Updation in process for registration............ ");
		 eduRegistrationManagerObj.updateRegistrationData(eduRegistrationToObj);
		 logger.debug("Updation is completed for registration............ ");

	   }
	   catch(Exception exp)
	   {
		   exp.printStackTrace();
	       logger.debug("Method update :: ERROR "+exp.getMessage());	
		   
	   }
	   return mapping.findForward("update");
   
}
   
   public ActionForward viewDetails(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		if(request.getSession().getAttribute("user_name") == null){
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
		try
		{  
		 logger.debug("View data in process for registration............ ");
		 logger.debug("Method viewDetails :: user id "+request.getParameter("User_id")+" Session_id "+request.getParameter("Session_id")+" reg_status "+request.getParameter("Reg_status"));
		 Calendar cal = Calendar.getInstance();
		 DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		 EduRegistrationManager eduRegistrationManagerObj = new EduRegistrationManager();
	     EduRegistrationTO eduStudentToObj = eduRegistrationManagerObj.getEduRegistrationDatabyUserId(request.getParameter("User_id"),request.getParameter("Session_id"),request.getParameter("Reg_status"));
	     request.setAttribute("firstname",eduStudentToObj.getFirstname());
	     request.setAttribute("lastname",eduStudentToObj.getLastname());
	     request.setAttribute("phoneno",eduStudentToObj.getPhoneno());
	     request.setAttribute("address",eduStudentToObj.getAddress());
	     request.setAttribute("admissiondate",eduStudentToObj.getRegistrationdate());
	     request.setAttribute("middlename",eduStudentToObj.getMiddlename());
	     request.setAttribute("emailid",eduStudentToObj.getEmailid());
	     request.setAttribute("fathername",eduStudentToObj.getFathername());
	     request.setAttribute("mothername",eduStudentToObj.getMothername());
	     request.setAttribute("parentemailid",eduStudentToObj.getParentemailid());
	     request.setAttribute("classname",eduStudentToObj.getClassname());
	     request.setAttribute("gender",eduStudentToObj.getGender());
	     request.setAttribute("student_prev_qualification",eduStudentToObj.getStudprequalification_title());
	     request.setAttribute("parent_higher_qualification",eduStudentToObj.getParentqualification_title());
	     request.setAttribute("previous_edu_center",eduStudentToObj.getPreviousinstitute());
	     request.setAttribute("registrationfee",eduStudentToObj.getRegistrationfee());
	     request.setAttribute("concessionamt",eduStudentToObj.getConcessionamt());
	     request.setAttribute("comment",eduStudentToObj.getComment());
	     System.out.println("birth date"+(eduStudentToObj.getBirthmonth()+1)+"/"+eduStudentToObj.getBirthday()+"/"+eduStudentToObj.getBirthyear());
	     DateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy");
	     cal.setTime(dateFormat.parse(eduStudentToObj.getDateofbirth()));
		 cal.add(Calendar.MONTH,1);		
		 request.setAttribute("birthofdate",df1.format(cal.getTime()));
	     cal.setTime(dateFormat.parse(eduStudentToObj.getRegistrationdate()));
		 cal.add(Calendar.MONTH,1);		
		 request.setAttribute("admissionofdate",df1.format(cal.getTime()));
		 logger.debug("Method viewDetails :: birthdate "+request.getAttribute("birthofdate")+" admissiondate "+request.getAttribute("admissionofdate"));
		 logger.debug("View data is completed for student............ ");
	   }
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method viewDetails :: ERROR "+exp.getMessage());	
			
		}
		 return mapping.findForward("viewDetails");
	}
	
  public ActionForward delete(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
   {  
	  if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
	  String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
	   EduRegistrationManager eduRegistrationManagerObj = new EduRegistrationManager();
	   eduRegistrationManagerObj.deleteRegistrationDetailbyUserId(request.getParameter("User_id"),request.getParameter("Session_id"));
	   logger.debug("Deletion is completed for registration............ ");
	   return mapping.findForward("delete");
   }
  
  
  public ActionForward saveAdmission(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{

		if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}
	try {       
		    	  
				  EduRegistrationForm eduRegistrationFormObj = (EduRegistrationForm)form;
		          EduRegistrationTO eduRegistrationToObj = new EduRegistrationTO();
		          EduRegistrationManager eduRegistrationManagerObj = new EduRegistrationManager();
		          eduRegistrationToObj.setFirstname(eduRegistrationFormObj.getFirstname());
		          eduRegistrationToObj.setMiddlename(eduRegistrationFormObj.getMiddlename());
		          eduRegistrationToObj.setLastname(eduRegistrationFormObj.getLastname());
		          eduRegistrationToObj.setPhoneno(eduRegistrationFormObj.getPhoneno());
		          eduRegistrationToObj.setFathername(eduRegistrationFormObj.getFathername());
		          eduRegistrationToObj.setMothername(eduRegistrationFormObj.getMothername());
		          eduRegistrationToObj.setEmailid(eduRegistrationFormObj.getEmailid());
		          eduRegistrationToObj.setParentemailid(eduRegistrationFormObj.getParentemailid());
		          eduRegistrationToObj.setDateofbirth(eduRegistrationFormObj.getDateofbirth());
		          eduRegistrationToObj.setPassword(eduRegistrationFormObj.getPassword());
		          eduRegistrationToObj.setParentpassword(eduRegistrationFormObj.getParentpassword());
		          eduRegistrationToObj.setAddress(eduRegistrationFormObj.getAddress());
		          eduRegistrationToObj.setRegistrationdate(eduRegistrationFormObj.getRegistrationmonth()+"/"+eduRegistrationFormObj.getRegistrationday()+"/"+eduRegistrationFormObj.getRegistrationyear());
		          eduRegistrationToObj.setStudprequalification(eduRegistrationFormObj.getStudprequalification());
		          eduRegistrationToObj.setParentqualification(eduRegistrationFormObj.getParentqualification());
		          eduRegistrationToObj.setPreviousinstitute(eduRegistrationFormObj.getPreviousinstitute());
		          eduRegistrationToObj.setParentqualification(eduRegistrationFormObj.getParentqualification());
		          eduRegistrationToObj.setClassid(Integer.valueOf(eduRegistrationFormObj.getEduclass()));
		          eduRegistrationToObj.setAdmissionfee(eduRegistrationFormObj.getAdmissionfee());
		          eduRegistrationToObj.setGender(eduRegistrationFormObj.getGender());
		          eduRegistrationToObj.setConcessionamt(eduRegistrationFormObj.getConcessionamt());
		          eduRegistrationToObj.setBirthday(eduRegistrationFormObj.getBirthday());
		          eduRegistrationToObj.setBirthmonth(eduRegistrationFormObj.getBirthmonth());
		          eduRegistrationToObj.setBirthyear(eduRegistrationFormObj.getBirthyear());
		          eduRegistrationToObj.setDept_id(Integer.parseInt(eduRegistrationFormObj.getDepartment()));
		          eduRegistrationToObj.setRegistrationid(eduRegistrationFormObj.getRegistrationid());
		          eduRegistrationToObj.setSessionId(eduRegistrationFormObj.getSessionId());
		          if(request.getParameterValues("selectedsubjects")!=null ){
		        	  eduRegistrationFormObj.setAssignsubjected(request.getParameterValues("selectedsubjects"));    		
				      }
		          logger.debug("Admission through registration is in process....................");
		          EduRegistrationTO eduRegistrationTOObj = eduRegistrationManagerObj.saveEduStudentData(eduRegistrationToObj,eduRegistrationFormObj);
		          logger.debug("Admission through registration is completed.....................");
		          sendMail(eduRegistrationTOObj,eduRegistrationToObj.getBirthyear()+""+(eduRegistrationToObj.getBirthmonth()+1)+""+eduRegistrationToObj.getBirthday(),eduRegistrationFormObj.getRegistrationyear()+""+(eduRegistrationFormObj.getRegistrationmonth()+1)+""+eduRegistrationFormObj.getRegistrationday());
		         
	}
		    catch ( Exception exp ){
		    	exp.getMessage();
		    	logger.debug("Method saveAdmission :: ERROR "+exp.getMessage());
		    	}
		return mapping.findForward("saveAdmission");
	}
  
  public ActionForward registrationSetup(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception
	{
		/*if(request.getSession().getAttribute("user_name") == null)
		{
			return mapping.findForward("userlogout");
		}
		String ug_id = request.getSession().getAttribute("ug_id").toString();
		if(!loginManager.checkAccess(Integer.parseInt(ug_id), f_Id)){
			return mapping.findForward("userlogout");
		}*/
		PortableConstant PortableConstantObj = new PortableConstant();
		PortableConstantObj.loadProperty();
		logger.debug("Setup in process for registration............ ");
		EduRegistrationForm eduRegistrationFormObj = (EduRegistrationForm)form;
		EduRegistrationManager eduRegistrationManagerObj = new EduRegistrationManager();
		ArrayList<EduRegistrationTO> studprequalifications = eduRegistrationManagerObj.getRegistrationQualification();
		eduRegistrationFormObj.setStudprequalifications(studprequalifications);
		request.setAttribute("studprequalifications",studprequalifications);
		ArrayList<EduRegistrationTO> parentqualifications = eduRegistrationManagerObj.getParentHigherQualification();
		eduRegistrationFormObj.setParentqualifications(parentqualifications);
		request.setAttribute("parentqualifications",parentqualifications);
		ArrayList<EduRegistrationTO> classes = eduRegistrationManagerObj.getClassInfo();
		eduRegistrationFormObj.setClasses(classes);
		request.setAttribute("classes",classes);
		ArrayList<EduRegistrationTO> departments = eduRegistrationManagerObj.getDepartment();
		eduRegistrationFormObj.setDepartments(departments);
		request.setAttribute("departments",departments);
		ArrayList<EduRegistrationTO> admissionfees = eduRegistrationManagerObj.getAdmissionfees();
		eduRegistrationFormObj.setAdmissionfees(admissionfees);
		request.setAttribute("admissionfees",admissionfees);
		request.setAttribute("startyear",PortableConstantObj.getProperty(PortableConstant.DATEPICKERSTARTYEAR));
		request.setAttribute("currentyear",PortableConstantObj.getProperty(PortableConstant.DATEPICKERCURRENTYEAR));
		logger.debug("Setup completed for registration............ ");
		return mapping.findForward("registrationSetup");
	}

  public void sendMail(EduRegistrationTO mailInfoObj,String studentPassword,String parentPassword)
  {
	  logger.debug("Mail Sending Started............ "+mailInfoObj.getFirstname()+" "+mailInfoObj.getMiddlename()+" "+mailInfoObj.getLastname());
	 try{
	  Properties props = new Properties();
	  PortableConstant PortableConstantObj = new PortableConstant();
	  props.put("mail.smtp.host",PortableConstant.SMTP_HOST_NAME);
	  props.put("mail.smtp.auth","true");
	  //props.put("mail.debug","true");
	  props.put("mail.smtp.port",PortableConstant.SMTP_PORT);
	  props.put("mail.smtp.socketFactory.port",PortableConstant.SMTP_PORT);
	  props.put("mail.smtp.socketFactory.class",PortableConstant.SSL_FACTORY);
	  //props.put("mail.smtp.socketFactory.fallback","false");
	  final String emailId =  PortableConstantObj.getProperty(PortableConstant.EMAILFROMADDRESS);
	  final String password =  PortableConstantObj.getProperty(PortableConstant.EMAILFROMPASSWORD);
	  logger.debug("Email Id and Password used for PasswordAuthentication...... "+emailId+"       "+password);
	  
	  Session session = Session.getDefaultInstance(props,
	  new javax.mail.Authenticator() {
	  protected PasswordAuthentication getPasswordAuthentication() {
	  return new PasswordAuthentication(emailId,password);
	  }
	  });
	  session.setDebug(PortableConstant.debug);
	  Message msg = new MimeMessage(session);
	  Message parentmsg = new MimeMessage(session);
	  InternetAddress addressFrom = new InternetAddress(PortableConstantObj.getProperty(PortableConstant.EMAILFROMADDRESS));
	  msg.setFrom(addressFrom);
	  parentmsg.setFrom(addressFrom);
	  InternetAddress addressTo = new InternetAddress (mailInfoObj.getEmailid());
	  msg.setRecipient(Message.RecipientType.TO, addressTo);
	  InternetAddress parentaddressTo = new InternetAddress (mailInfoObj.getParentemailid());
	  parentmsg.setRecipient(Message.RecipientType.TO, parentaddressTo);
	  // Setting the Subject and Content Type
	  msg.setSubject(PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT));
	  parentmsg.setSubject(PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT));
	  //msg.setContent(PortableConstant.EMAILMSGTXT, "text/plain");
	  //msg.setContent("Hi, "+mailInfoObj.getFirstname()+" "+mailInfoObj.getMiddlename()+" "+mailInfoObj.getLastname(), "text/html");
	  msg.setContent("Hi "+mailInfoObj.getFirstname()+" "+mailInfoObj.getMiddlename()+" "+mailInfoObj.getLastname()+","+"<br>"+PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT)+"!<br><br>We are informing you your admission process is completed and your student id is "+mailInfoObj.getStudentid()+" and credential is "+mailInfoObj.getStudentUser()+"/"+studentPassword+"<br><br>"+PortableConstantObj.getProperty(PortableConstant.REGARDS)+"<br>"+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL), "text/html");
	  parentmsg.setContent("Hi "+mailInfoObj.getFathername()+","+"<br>"+PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT)+"!<br><br>Thanks to showing interest in the institute "+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL)+" as a parent. We are dedicate to improve growth of our students.Your parent id is "+mailInfoObj.getParentid()+" and credential is "+mailInfoObj.getParentUser()+"/"+parentPassword+"<br><br>"+PortableConstantObj.getProperty(PortableConstant.REGARDS)+"<br>"+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL), "text/html");
	  Transport.send(msg);
	  Transport.send(parentmsg);
    }
	 catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
			logger.debug("Mail Sending MessagingException sendMail :: "+e.getMessage());
		}
	 catch (Exception exp) {
			// TODO Auto-generated catch block
			exp.printStackTrace();
			logger.debug("Mail Sending Exception sendMail :: "+exp.getMessage());
		}
  }
  
	 public void sendMailForReg(EduStudentTO mailInfoObj,String studentPassword,String parentPassword)
	  {
		  logger.debug("Mail Sending Started............ "+mailInfoObj.getFirstname()+" "+mailInfoObj.getMiddlename()+" "+mailInfoObj.getLastname());
		 try{
		  Properties props = new Properties();
		  PortableConstant PortableConstantObj = new PortableConstant();
		  props.put("mail.smtp.host",PortableConstant.SMTP_HOST_NAME);
		  props.put("mail.smtp.auth","true");
		  //props.put("mail.debug","true");
		  props.put("mail.smtp.port",PortableConstant.SMTP_PORT);
		  props.put("mail.smtp.socketFactory.port",PortableConstant.SMTP_PORT);
		  props.put("mail.smtp.socketFactory.class",PortableConstant.SSL_FACTORY);
		  //props.put("mail.smtp.socketFactory.fallback","false");
		  final String emailId =  PortableConstantObj.getProperty(PortableConstant.EMAILFROMADDRESS);
		  final String password =  PortableConstantObj.getProperty(PortableConstant.EMAILFROMPASSWORD);
		  logger.debug("Email Id and Password used for PasswordAuthentication...... "+emailId+"       "+password);
		  Session session = Session.getDefaultInstance(props,
		  new javax.mail.Authenticator() {
		  protected PasswordAuthentication getPasswordAuthentication() {
		  return new PasswordAuthentication(emailId,password);
		  }
		  });
		  session.setDebug(PortableConstant.debug);
		  Message msg = new MimeMessage(session);
		  Message parentmsg = new MimeMessage(session);
		  InternetAddress addressFrom = new InternetAddress(PortableConstantObj.getProperty(PortableConstant.EMAILFROMADDRESS));
		  msg.setFrom(addressFrom);
		  parentmsg.setFrom(addressFrom);
		  InternetAddress addressTo = new InternetAddress (mailInfoObj.getEmailid());
		  msg.setRecipient(Message.RecipientType.TO, addressTo);
		  InternetAddress parentaddressTo = new InternetAddress (mailInfoObj.getParentemailid());
		  parentmsg.setRecipient(Message.RecipientType.TO, parentaddressTo);
		  // Setting the Subject and Content Type
		  msg.setSubject(PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT));
		  parentmsg.setSubject(PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT));
		  //msg.setContent(PortableConstant.EMAILMSGTXT, "text/plain");
		  //msg.setContent("Hi, "+mailInfoObj.getFirstname()+" "+mailInfoObj.getMiddlename()+" "+mailInfoObj.getLastname(), "text/html");
		  msg.setContent("Hi "+mailInfoObj.getFirstname()+" "+mailInfoObj.getMiddlename()+" "+mailInfoObj.getLastname()+","+"<br>"+PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT)+"!<br><br>We are informing you your Registration is in process.Shortly we will inform you about status. <br><br>"+PortableConstantObj.getProperty(PortableConstant.REGARDS)+"<br>"+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL), "text/html");
		  parentmsg.setContent("Hi "+mailInfoObj.getFathername()+","+"<br>"+PortableConstantObj.getProperty(PortableConstant.EMAILSUBJECTTXT)+"!<br><br>Thanks to showing interest in the institute "+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL)+" as a parent. We are dedicate to improve growth of our students.Your parent id is "+mailInfoObj.getParentid()+" and credential is "+mailInfoObj.getParentUser()+"/"+parentPassword+"<br><br>"+PortableConstantObj.getProperty(PortableConstant.REGARDS)+"<br>"+PortableConstantObj.getProperty(PortableConstant.NAMESCHOOL), "text/html");
		  Transport.send(msg);
		 // Transport.send(parentmsg);
	    }
		 catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); 
				logger.debug("Mail Sending MessagingException sendMail :: "+e.getMessage());
			}
		 catch (Exception exp) {
				// TODO Auto-generated catch block
				exp.printStackTrace();
				logger.debug("Mail Sending Exception sendMail :: "+exp.getMessage());
			}
	  }
	 
	 
	 
	
   
}
