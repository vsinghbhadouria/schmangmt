package com.sch.to;

public class ConcernTrackingTO {
	
	public Integer getIssueid() {
		return issueid;
	}
	public void setIssueid(Integer issueid) {
		this.issueid = issueid;
	}
	public String getUsername() {
		return username;
	}
    public void setUsername(String username) {
		this.username = username;
	}
	public String getDesgnation() {
		return desgnation;
	}
	public void setDesgnation(String desgnation) {
		this.desgnation = desgnation;
	}
	public String getIssuetitle() {
		return issuetitle;
	}
	public void setIssuetitle(String issuetitle) {
		this.issuetitle = issuetitle;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	protected String  userid;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	protected String ansdisc;
	public String getAnsdisc() {
		return ansdisc;
	}
	public void setAnsdisc(String ansdisc) {
		this.ansdisc = ansdisc;
	}
	protected String  issuesubmiteddate;
	public String getIssuesubmiteddate() {
		return issuesubmiteddate;
	}
	public void setIssuesubmiteddate(String issuesubmiteddate) {
		this.issuesubmiteddate = issuesubmiteddate;
	}
	public String getIssuerepydate() {
		return issuerepydate;
	}
	public void setIssuerepydate(String issuerepydate) {
		this.issuerepydate = issuerepydate;
	}
	protected Integer  replyid;
	public Integer getReplyid() {
		return replyid;
	}
	public void setReplyid(Integer replyid) {
		this.replyid = replyid;
	}
	protected String  issuerepydate;
	protected Integer issueid;
	protected String  username;
	protected String  desgnation;
	protected String  issuetitle;
	protected String  issuedesc;
	protected String  category;
	protected String  status;
	protected String  priority;
	protected Integer catid;
	protected String  replydesc;
	protected int statusid;
	
	
	public int getStatusid() {
		return statusid;
	}
	public void setStatusid(int statusid) {
		this.statusid = statusid;
	}
	public String getReplydesc() {
		return replydesc;
	}
	public void setReplydesc(String replydesc) {
		this.replydesc = replydesc;
	}
	public Integer getCatid() {
		return catid;
	}
	public void setCatid(Integer catid) {
		this.catid = catid;
	}
	public String getIssuedesc() {
		return issuedesc;
	}
	public void setIssuedesc(String issuedesc) {
		this.issuedesc = issuedesc;
	}
	

}
