package com.sch.to;

import java.io.Serializable;
public class LibraryManagementTO implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected String bookname;
	protected String booktypename;
	protected int booktypeid;
	protected String bookid;
	protected int bookmasterid;
	protected int rackid;
	protected String rackname;
	protected int numberofcopy;
	protected String bookids[]=null;
	protected int bookcopyid;
	protected int availablebookcount;
	
	protected String userType=null;
	protected String userId=null;
	protected String userName=null;
	protected String issueDate=null;
	protected String dueDate=null;
	protected String status=null;
	protected String emailId=null;
	protected String contact_no=null;
	protected String returnDate=null;
	
	
	public String getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getContact_no() {
		return contact_no;
	}
	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getAvailablebookcount() {
		return availablebookcount;
	}
	public void setAvailablebookcount(int availablebookcount) {
		this.availablebookcount = availablebookcount;
	}
	public int getBookcopyid() {
		return bookcopyid;
	}
	public void setBookcopyid(int bookcopyid) {
		this.bookcopyid = bookcopyid;
	}
	public String[] getBookids() {
		return bookids;
	}
	public void setBookids(String[] bookids) {
		this.bookids = bookids;
	}
	public String getBookname() {
		return bookname;
	}
	public void setBookname(String bookname) {
		this.bookname = bookname;
	}
	public String getBooktypename() {
		return booktypename;
	}
	public void setBooktypename(String booktypename) {
		this.booktypename = booktypename;
	}
	public int getBooktypeid() {
		return booktypeid;
	}
	public void setBooktypeid(int booktypeid) {
		this.booktypeid = booktypeid;
	}
	public String getBookid() {
		return bookid;
	}
	public void setBookid(String bookid) {
		this.bookid = bookid;
	}
	public int getBookmasterid() {
		return bookmasterid;
	}
	public void setBookmasterid(int bookmasterid) {
		this.bookmasterid = bookmasterid;
	}
	public int getRackid() {
		return rackid;
	}
	public void setRackid(int rackid) {
		this.rackid = rackid;
	}
	public String getRackname() {
		return rackname;
	}
	public void setRackname(String rackname) {
		this.rackname = rackname;
	}
	public int getNumberofcopy() {
		return numberofcopy;
	}
	public void setNumberofcopy(int numberofcopy) {
		this.numberofcopy = numberofcopy;
	}
	
	
	
}