package com.sch.to;

import java.io.Serializable;

public class TopNavigationTO implements Serializable{

	public Integer funcID;
	public String funcTITLE;
	public Integer pfuncID;
	public String funcURL;
	public TopNavigationTO(Integer funcID, String funcTITLE, String funcURL, Integer pfuncID) {
		super();
		this.funcID = funcID;
		this.funcTITLE = funcTITLE;
		this.funcURL = funcURL;
		this.pfuncID = pfuncID;
	}
	public Integer getFuncID() {
		return funcID;
	}
	public void setFuncID(Integer funcID) {
		this.funcID = funcID;
	}
	public String getFuncTITLE() {
		return funcTITLE;
	}
	public void setFuncTITLE(String funcTITLE) {
		this.funcTITLE = funcTITLE;
	}
	public Integer getPfuncID() {
		return pfuncID;
	}
	public void setPfuncID(Integer pfuncID) {
		this.pfuncID = pfuncID;
	}
	public String getFuncURL() {
		return funcURL;
	}
	public void setFuncURL(String funcURL) {
		this.funcURL = funcURL;
	}
	
	
	

}
