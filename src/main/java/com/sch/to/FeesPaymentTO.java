package com.sch.to;

import java.io.Serializable;
public class FeesPaymentTO implements Serializable
{

	protected String dateofbirth;
	protected Integer studentid;
	protected String firstname;
	protected String middlename;
	protected String lastname;
	protected String fathername;
	protected String address;
	protected int classid;
	protected String classname;
	protected String section;
    protected int sessionid;
    protected String monthselection[];
   	protected String monthly_fee;
    protected Integer monthid;
    protected String monthname;
    protected String paymentMode;
    protected String receptNo;
    protected String paymentDate;
    protected String chequeNo;
    protected String dateoncheque;
    protected String bankname;
    protected String dept_name;
    protected String emailId;
    protected String sessionyear;
    protected String phoneno;
    protected int dept_id;
    
    protected Integer registrationfee;
	protected Integer concessionamt;
	protected String comment;
	
	
    
    public Integer getRegistrationfee() {
		return registrationfee;
	}
	public void setRegistrationfee(Integer registrationfee) {
		this.registrationfee = registrationfee;
	}
	public Integer getConcessionamt() {
		return concessionamt;
	}
	public void setConcessionamt(Integer concessionamt) {
		this.concessionamt = concessionamt;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int getDept_id() {
		return dept_id;
	}
	public void setDept_id(int dept_id) {
		this.dept_id = dept_id;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getSessionyear() {
		return sessionyear;
	}
	public void setSessionyear(String sessionyear) {
		this.sessionyear = sessionyear;
	}
	public String getDept_name() {
		return dept_name;
	}
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	public String getBankname() {
		return bankname;
	}
	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
    public String getDateoncheque() {
		return dateoncheque;
	}
	public void setDateoncheque(String dateoncheque) {
		this.dateoncheque = dateoncheque;
	}
    
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getReceptNo() {
		return receptNo;
	}
	public void setReceptNo(String receptNo) {
		this.receptNo = receptNo;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getChequeNo() {
		return chequeNo;
	}
	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}
	public Integer getMonthid() {
		return monthid;
	}
	public void setMonthid(Integer monthid) {
		this.monthid = monthid;
	}
	public String getMonthname() {
		return monthname;
	}
	public void setMonthname(String monthname) {
		this.monthname = monthname;
	}
	public String getMonthly_fee() {
		return monthly_fee;
	}
	public void setMonthly_fee(String monthly_fee) {
		this.monthly_fee = monthly_fee;
	}
	public String getFathername() {
		return fathername;
	}
	public void setFathername(String fathername) {
		this.fathername = fathername;
	}
	public String getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}
	public Integer getStudentid() {
		return studentid;
	}
	public void setStudentid(Integer studentid) {
		this.studentid = studentid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getClassid() {
		return classid;
	}
	public void setClassid(int classid) {
		this.classid = classid;
	}
	public String getClassname() {
		return classname;
	}
	public void setClassname(String classname) {
		this.classname = classname;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public int getSessionid() {
		return sessionid;
	}
	public void setSessionid(int sessionid) {
		this.sessionid = sessionid;
	}
	public String[] getMonthselection() {
		return monthselection;
	}
	public void setMonthselection(String[] monthselection) {
		this.monthselection = monthselection;
	}
    
	public FeesPaymentTO()
	  {
		// TODO Auto-generated constructor stub
	  }
}
