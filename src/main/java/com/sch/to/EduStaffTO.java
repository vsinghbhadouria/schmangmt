package com.sch.to;

import java.io.Serializable;
import java.util.ArrayList;

public class EduStaffTO implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Integer empid;
	protected Integer parentid;
	protected String firstname;
	protected String middlename;
	protected String lastname;
	protected String phoneno;
	protected String salary;
	protected String higheredu;
	protected String previousexp;
	protected String joiningdate;
	protected String level;
	protected String qualification;
	protected String emailid;
	protected String address;
	protected Integer designation_id;
	protected String  designation_title;
	protected int userid;
	protected int classid;
	protected String classname;
	protected String section;
	protected Integer qualification_id;
	protected String qualification_title;
	protected String password;
	protected Integer dept_id;
	protected String dept_title;
	protected int subjectid;
	protected String subjectname;
    protected String loginuser;
    protected String emailIds[];

public String[] getEmailIds() {
		return emailIds;
	}



	public void setEmailIds(String[] emailIds) {
		this.emailIds = emailIds;
	}



public String getLoginuser() {
		return loginuser;
	}



	public void setLoginuser(String loginuser) {
		this.loginuser = loginuser;
	}



public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



public int getSubjectid() {
		return subjectid;
	}



	public void setSubjectid(int subjectid) {
		this.subjectid = subjectid;
	}



	public String getSubjectname() {
		return subjectname;
	}



	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}



public Integer getDept_id() {
		return dept_id;
	}



	public void setDept_id(Integer dept_id) {
		this.dept_id = dept_id;
	}



	public String getDept_title() {
		return dept_title;
	}



	public void setDept_title(String dept_title) {
		this.dept_title = dept_title;
	}



public String getQualification() {
		return qualification;
	}



	public void setQualification(String qualification) {
		this.qualification = qualification;
	}



public Integer getEmpid() {
		return empid;
	}



	public void setEmpid(Integer empid) {
		this.empid = empid;
	}



	public Integer getParentid() {
		return parentid;
	}



	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}



	public String getFirstname() {
		return firstname;
	}



	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}



	public String getMiddlename() {
		return middlename;
	}



	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}



	public String getLastname() {
		return lastname;
	}



	public void setLastname(String lastname) {
		this.lastname = lastname;
	}



	public String getPhoneno() {
		return phoneno;
	}



	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}



	public String getSalary() {
		return salary;
	}



	public void setSalary(String salary) {
		this.salary = salary;
	}



	public String getHigheredu() {
		return higheredu;
	}



	public void setHigheredu(String higheredu) {
		this.higheredu = higheredu;
	}



	public String getPreviousexp() {
		return previousexp;
	}



	public void setPreviousexp(String previousexp) {
		this.previousexp = previousexp;
	}



	public String getJoiningdate() {
		return joiningdate;
	}



	public void setJoiningdate(String joiningdate) {
		this.joiningdate = joiningdate;
	}



	public String getLevel() {
		return level;
	}



	public void setLevel(String level) {
		this.level = level;
	}



	public String getEmailid() {
		return emailid;
	}



	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public Integer getDesignation_id() {
		return designation_id;
	}



	public void setDesignation_id(Integer designation_id) {
		this.designation_id = designation_id;
	}



	public String getDesignation_title() {
		return designation_title;
	}



	public void setDesignation_title(String designation_title) {
		this.designation_title = designation_title;
	}



	public int getUserid() {
		return userid;
	}



	public void setUserid(int userid) {
		this.userid = userid;
	}



	public int getClassid() {
		return classid;
	}



	public void setClassid(int classid) {
		this.classid = classid;
	}



	public String getClassname() {
		return classname;
	}



	public void setClassname(String classname) {
		this.classname = classname;
	}



	public String getSection() {
		return section;
	}



	public void setSection(String section) {
		this.section = section;
	}



	public Integer getQualification_id() {
		return qualification_id;
	}



	public void setQualification_id(Integer qualification_id) {
		this.qualification_id = qualification_id;
	}



	public String getQualification_title() {
		return qualification_title;
	}



	public void setQualification_title(String qualification_title) {
		this.qualification_title = qualification_title;
	}



	public static long getSerialVersionUID() {
		return serialVersionUID;
	}



public EduStaffTO()
  {
	// TODO Auto-generated constructor stub
  }
public EduStaffTO(Integer emp_id,String first_name,String middle_name,String last_name,String user_id,String designation_title,String email_id)
{
	empid=emp_id;
	firstname=first_name;
	middlename =middle_name;
	lastname=last_name;
	loginuser=user_id;
	this.designation_title=designation_title;
	emailid=email_id;
	// TODO Auto-generated constructor stub
}
}