package com.sch.to;

import java.io.Serializable;

public class EduStudentReportTO implements Serializable
{
    /**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	protected Integer studentid;
	protected Integer parentid;
	protected String studentname;
	protected String phoneno;
	protected String fathername;
	protected String userid;
	protected String classname;
	protected Integer classid;
	protected String dateofbirth;
	protected String departmentname;
	protected Integer dept_id;
	protected String assignsubjected[];
	protected String subjectname;
	protected Integer sessionid;
	protected String session_year;
	protected Integer subject_count;
	protected Float percentage;
	protected String division;
	protected String result;
	protected String subject[];
	protected String markOutOf[];
	protected String markObtain[];
	protected String admissiondate;
	protected String emailid;
	protected String address;
	protected String educlass;
	protected String dept_title;
	protected Integer admissionfee;
	protected Integer concessionamt;
	protected String comment;
	protected String subjectcode[];
	
	
	public String[] getSubjectcode() {
		return subjectcode;
	}
	public void setSubjectcode(String[] subjectcode) {
		this.subjectcode = subjectcode;
	}
	public Integer getAdmissionfee() {
		return admissionfee;
	}
	public void setAdmissionfee(Integer admissionfee) {
		this.admissionfee = admissionfee;
	}
	public Integer getConcessionamt() {
		return concessionamt;
	}
	public void setConcessionamt(Integer concessionamt) {
		this.concessionamt = concessionamt;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getEduclass() {
		return educlass;
	}
	public void setEduclass(String educlass) {
		this.educlass = educlass;
	}
	public String getDept_title() {
		return dept_title;
	}
	public void setDept_title(String deptTitle) {
		dept_title = deptTitle;
	}
	public String getAdmissiondate() {
		return admissiondate;
	}
	public void setAdmissiondate(String admissiondate) {
		this.admissiondate = admissiondate;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String[] getSubject() {
		return subject;
	}
	public void setSubject(String[] subject) {
		this.subject = subject;
	}
	public String[] getMarkOutOf() {
		return markOutOf;
	}
	public void setMarkOutOf(String[] markOutOf) {
		this.markOutOf = markOutOf;
	}
	public String[] getMarkObtain() {
		return markObtain;
	}
	public void setMarkObtain(String[] markObtain) {
		this.markObtain = markObtain;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public Float getPercentage() {
		return percentage;
	}
	public void setPercentage(Float percentage) {
		this.percentage = percentage;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public Integer getSubject_count() {
		return subject_count;
	}
	public void setSubject_count(Integer subjectCount) {
		subject_count = subjectCount;
	}
	public String getSession_year() {
		return session_year;
	}
	public void setSession_year(String sessionYear) {
		session_year = sessionYear;
	}
	public Integer getSessionid() {
		return sessionid;
	}
	public void setSessionid(Integer sessionid) {
		this.sessionid = sessionid;
	}
	public int getSubjectid() {
		return subjectid;
	}
	public void setSubjectid(int subjectid) {
		this.subjectid = subjectid;
	}
	public String getSubjectname() {
		return subjectname;
	}
	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}
	public Integer getStudentid() {
		return studentid;
	}
	public void setStudentid(Integer studentid) {
		this.studentid = studentid;
	}
	public Integer getParentid() {
		return parentid;
	}
	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}
	public String getStudentname() {
		return studentname;
	}
	public void setStudentname(String studentname) {
		this.studentname = studentname;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public String getFathername() {
		return fathername;
	}
	public void setFathername(String fathername) {
		this.fathername = fathername;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getClassname() {
		return classname;
	}
	public void setClassname(String classname) {
		this.classname = classname;
	}
	public Integer getClassid() {
		return classid;
	}
	public void setClassid(Integer classid) {
		this.classid = classid;
	}
	public String getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}
	public String getDepartmentname() {
		return departmentname;
	}
	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}
	public Integer getDept_id() {
		return dept_id;
	}
	public void setDept_id(Integer deptId) {
		dept_id = deptId;
	}
	public String[] getAssignsubjected() {
		return assignsubjected;
	}
	public void setAssignsubjected(String[] assignsubjected) {
		this.assignsubjected = assignsubjected;
	}
	protected Integer outOf;
	protected Float markObtained;
	protected int subjectid;
	protected Integer totalOutOf;
	protected Float totalMarkObtained;
	
	public Integer getTotalOutOf() {
		return totalOutOf;
	}
	public void setTotalOutOf(Integer totalOutOf) {
		this.totalOutOf = totalOutOf;
	}
	public Float getTotalMarkObtained() {
		return totalMarkObtained;
	}
	public void setTotalMarkObtained(Float totalMarkObtained) {
		this.totalMarkObtained = totalMarkObtained;
	}
	public Integer getOutOf() {
		return outOf;
	}
	public void setOutOf(Integer outOf) {
		this.outOf = outOf;
	}
	public Float getMarkObtained() {
		return markObtained;
	}
	public void setMarkObtained(Float markObtained) {
		this.markObtained = markObtained;
	}
	
}