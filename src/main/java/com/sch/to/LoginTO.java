


package com.sch.to;
public class LoginTO 
{

	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	protected String loginid =null;
	protected String password =null;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	protected String userid = null;
	protected String username = null;
	protected String email = null;
	protected String ug_title = null;
	protected String ug_id = null;
	protected String userlastname = null;
	protected String welcome_msg = null;
	protected String newpassword = null;
	protected String ugCode = null;
	
	public String getUgCode() {
		return ugCode;
	}
	public void setUgCode(String ugCode) {
		this.ugCode = ugCode;
	}
	public String getNewpassword()
	{
		return newpassword;
	}
	public void setNewpassword(String newpassword)
	{
		this.newpassword = newpassword;
	}
	public String getWelcome_msg()
	{
		return welcome_msg;
	}
	public void setWelcome_msg(String welcome_msg)
	{
		this.welcome_msg = welcome_msg;
	}
	public String getUserlastname()
	{
		return userlastname;
	}
	public void setUserlastname(String userlastname)
	{
		this.userlastname = userlastname;
	}
	public String getUg_id() {
		return ug_id;
	}
	public void setUg_id(String ug_id) {
		this.ug_id = ug_id;
	}
	public String getUg_title() {
		return ug_title;
	}
	public void setUg_title(String ug_title) {
		this.ug_title = ug_title;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
