package com.sch.to;

import java.io.Serializable;
import java.util.ArrayList;

public class EduExitTO implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String studentId;
	protected String firstname;
	protected String middlename;
	protected String lastname;
	protected String admission_date;
	protected String fathername;
	protected int userid;
	protected String address;
	protected String classname;
	
	
public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getClassname() {
		return classname;
	}


	public void setClassname(String classname) {
		this.classname = classname;
	}


public String getFathername() {
		return fathername;
	}


	public void setFathername(String fathername) {
		this.fathername = fathername;
	}


public int getUserid() {
		return userid;
	}


	public void setUserid(int userid) {
		this.userid = userid;
	}


public String getFirstname() {
		return firstname;
	}


	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}


	public String getMiddlename() {
		return middlename;
	}


	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}


	public String getLastname() {
		return lastname;
	}


	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public String getAdmission_date() {
		return admission_date;
	}


	public void setAdmission_date(String admission_date) {
		this.admission_date = admission_date;
	}


public String getStudentId() {
		return studentId;
	}


	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}


public EduExitTO()
  {
	// TODO Auto-generated constructor stub
  }

}