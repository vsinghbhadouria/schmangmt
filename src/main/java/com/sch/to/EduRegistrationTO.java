package com.sch.to;

import java.io.Serializable;
import java.util.ArrayList;

public class EduRegistrationTO implements Serializable
{
 
	private static final long serialVersionUID = 1L;
	protected Integer studprequalification_id;
	protected String dateofbirth;
	protected String registrationdate;
	protected Integer registrationid;	
	protected Integer parentqualification_id;
	protected String studprequalification;
	protected Integer studentid;
	protected Integer parentid;
	protected String firstname;
	protected String middlename;
	protected String lastname;
	protected String phoneno;
	protected String fathername;
	protected String mothername;
	protected String parentemailid;
	protected String previousinstitute;
	protected String parentqualification;
	protected String qualification;
	protected String emailid;
	protected String address;
	protected int userid;
	protected int classid;
	protected String classname;
	protected String section;
	protected Integer qualification_id;
	protected String qualification_title;
	protected String parentqualification_title;
	protected String studprequalification_title;
	protected Integer admissionfee_id;
	protected Integer admissionfee;
	protected String gender;
	protected Integer concessionamt;
	protected String comment;
	protected String educlass;
	protected int sessionId;
	protected String session;
    protected int birthyear;
	protected int birthmonth;
	protected int birthday;
	protected int registrationyear;
	protected int registrationmonth;
	protected int registrationday;
	protected Integer dept_id;
	protected String dept_title;
	protected int subjectid;
	protected String subjectname;
	protected String password;
	protected String parentpassword;
	protected String studentUser;
	protected String parentUser;
	protected String Parentpassword;
	protected Integer registrationfee;
    protected String regstatus;
	
	public String getRegstatus() {
		return regstatus;
	}
	public void setRegstatus(String regstatus) {
		this.regstatus = regstatus;
	}
	
	public Integer getRegistrationfee() {
		return registrationfee;
	}


	public void setRegistrationfee(Integer registrationfee) {
		this.registrationfee = registrationfee;
	}


	public String getStudentUser() {
		return studentUser;
	}


	public void setStudentUser(String studentUser) {
		this.studentUser = studentUser;
	}


	public String getParentUser() {
		return parentUser;
	}


	public void setParentUser(String parentUser) {
		this.parentUser = parentUser;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getParentpassword() {
		return parentpassword;
	}


	public void setParentpassword(String parentpassword) {
		this.parentpassword = parentpassword;
	}


	public Integer getDept_id() {
		return dept_id;
	}


	public void setDept_id(Integer dept_id) {
		this.dept_id = dept_id;
	}


	public String getDept_title() {
		return dept_title;
	}


	public void setDept_title(String dept_title) {
		this.dept_title = dept_title;
	}


	public int getSubjectid() {
		return subjectid;
	}


	public void setSubjectid(int subjectid) {
		this.subjectid = subjectid;
	}


	public String getSubjectname() {
		return subjectname;
	}


	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}


	public int getRegistrationyear() {
		return registrationyear;
	}


	public void setRegistrationyear(int registrationyear) {
		this.registrationyear = registrationyear;
	}


	public int getRegistrationmonth() {
		return registrationmonth;
	}


	public void setRegistrationmonth(int registrationmonth) {
		this.registrationmonth = registrationmonth;
	}


	public int getRegistrationday() {
		return registrationday;
	}


	public void setRegistrationday(int registrationday) {
		this.registrationday = registrationday;
	}


	public int getBirthyear() {
		return birthyear;
	}


	public void setBirthyear(int birthyear) {
		this.birthyear = birthyear;
	}


	public int getBirthmonth() {
		return birthmonth;
	}


	public void setBirthmonth(int birthmonth) {
		this.birthmonth = birthmonth;
	}


	public int getBirthday() {
		return birthday;
	}


	public void setBirthday(int birthday) {
		this.birthday = birthday;
	}


	public int getSessionId() {
		return sessionId;
	}


	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}


	public String getSession() {
		return session;
	}


	public void setSession(String session) {
		this.session = session;
	}


	public Integer getRegistrationid() {
		return registrationid;
	}


	public void setRegistrationid(Integer registrationid) {
		this.registrationid = registrationid;
	}


	public String getEduclass() {
		return educlass;
	}


	public void setEduclass(String educlass) {
		this.educlass = educlass;
	}


	public Integer getConcessionamt() {
		return concessionamt;
	}


	public void setConcessionamt(Integer concessionamt) {
		this.concessionamt = concessionamt;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public Integer getAdmissionfee_id() {
		return admissionfee_id;
	}


	public void setAdmissionfee_id(Integer admissionfee_id) {
		this.admissionfee_id = admissionfee_id;
	}


	public Integer getAdmissionfee() {
		return admissionfee;
	}


	public void setAdmissionfee(Integer admissionfee) {
		this.admissionfee = admissionfee;
	}


	public String getStudprequalification_title() {
		return studprequalification_title;
	}


	public void setStudprequalification_title(String studprequalification_title) {
		this.studprequalification_title = studprequalification_title;
	}



public String getParentqualification_title() {
		return parentqualification_title;
	}


	public void setParentqualification_title(String parentqualification_title) {
		this.parentqualification_title = parentqualification_title;
	}


public Integer getStudprequalification_id() {
		return studprequalification_id;
	}


	public void setStudprequalification_id(Integer studprequalification_id) {
		this.studprequalification_id = studprequalification_id;
	}


	public String getDateofbirth() {
		return dateofbirth;
	}


	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}



	public String getRegistrationdate() {
		return registrationdate;
	}


	public void setRegistrationdate(String registrationdate) {
		this.registrationdate = registrationdate;
	}


	public Integer getParentqualification_id() {
		return parentqualification_id;
	}


	public void setParentqualification_id(Integer parentqualification_id) {
		this.parentqualification_id = parentqualification_id;
	}


	public String getStudprequalification() {
		return studprequalification;
	}


	public void setStudprequalification(String studprequalification) {
		this.studprequalification = studprequalification;
	}


	public Integer getStudentid() {
		return studentid;
	}


	public void setStudentid(Integer studentid) {
		this.studentid = studentid;
	}


	public Integer getParentid() {
		return parentid;
	}


	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}


	public String getFirstname() {
		return firstname;
	}


	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}


	public String getMiddlename() {
		return middlename;
	}


	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}


	public String getLastname() {
		return lastname;
	}


	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public String getPhoneno() {
		return phoneno;
	}


	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}


	public String getFathername() {
		return fathername;
	}


	public void setFathername(String fathername) {
		this.fathername = fathername;
	}


	public String getMothername() {
		return mothername;
	}


	public void setMothername(String mothername) {
		this.mothername = mothername;
	}


	public String getParentemailid() {
		return parentemailid;
	}


	public void setParentemailid(String parentemailid) {
		this.parentemailid = parentemailid;
	}


	public String getPreviousinstitute() {
		return previousinstitute;
	}


	public void setPreviousinstitute(String previousinstitute) {
		this.previousinstitute = previousinstitute;
	}


	public String getParentqualification() {
		return parentqualification;
	}


	public void setParentqualification(String parentqualification) {
		this.parentqualification = parentqualification;
	}


	public String getQualification() {
		return qualification;
	}


	public void setQualification(String qualification) {
		this.qualification = qualification;
	}


	public String getEmailid() {
		return emailid;
	}


	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public int getUserid() {
		return userid;
	}


	public void setUserid(int userid) {
		this.userid = userid;
	}


	public int getClassid() {
		return classid;
	}


	public void setClassid(int classid) {
		this.classid = classid;
	}


	public String getClassname() {
		return classname;
	}


	public void setClassname(String classname) {
		this.classname = classname;
	}


	public String getSection() {
		return section;
	}


	public void setSection(String section) {
		this.section = section;
	}


	public Integer getQualification_id() {
		return qualification_id;
	}


	public void setQualification_id(Integer qualification_id) {
		this.qualification_id = qualification_id;
	}


	public String getQualification_title() {
		return qualification_title;
	}


	public void setQualification_title(String qualification_title) {
		this.qualification_title = qualification_title;
	}

	public EduRegistrationTO(Integer student_id,String first_name, String middle_name,String last_name,String userid,String password,String email_id,Integer parent_id,String father_name,String parentuser,String parentpassword,String parentemailid)
	{
		studentid=student_id;
		firstname=first_name;
		middlename=middle_name;
		lastname=last_name;
		studentUser=userid;
		this.password=password;
		this.emailid=email_id;
		this.parentid=parent_id;
		this.fathername=father_name;
		this.parentUser=parentuser;
		this.Parentpassword=parentpassword;
		this.parentemailid=parentemailid;
		// TODO Auto-generated constructor stub
	}

public EduRegistrationTO()
  {
	// TODO Auto-generated constructor stub
  }

}