 package com.sch.delegates;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sch.dao.EduStudentDAO;
import com.sch.dao.EduStudentReportDAO;
import com.sch.form.EduStudentForm;
import com.sch.form.EduStudentReportForm;
import com.sch.to.*;



public class EduStudentReportManager 
{
	EduStudentReportDAO StudentReportDAO; 
	public static Logger logger = Logger.getLogger(EduStudentReportManager.class);
    public EduStudentReportManager() {

    	StudentReportDAO = new EduStudentReportDAO();
    }
  	public EduStudentReportTO getEduStudentDatabyUserId(String user_Id,String session_Id)
	{
	  return StudentReportDAO.getEduStudentDatabyUserId(user_Id,session_Id);	
    }
  	
  	public EduStudentReportTO getEduStudentDatabyStudentId(String user_Id,String Stud_status)
	{
	  return StudentReportDAO.getEduStudentDatabyStudentId(user_Id,Stud_status);	
    }
			
  	
		 public ArrayList<String>getSelectedSubject(int userid,int classid,String sessionId)
		  {
				System.out.println("userid"+userid+"classid "+classid+"sessionId "+sessionId);
		      ArrayList<EduStudentReportTO> selectedSubjectObj = StudentReportDAO.getSelectedSubject(userid,classid,sessionId);
			  ArrayList<String> unAssoUser= new ArrayList<String>();
				try {
				  for(EduStudentReportTO iter: selectedSubjectObj){
						unAssoUser.add(iter.getSubjectid()+","+iter.getSubjectname());
						System.out.println(iter.getSubjectid()+","+iter.getSubjectname());
		   				}
		   		     }
		   		      catch(Exception exp){exp.printStackTrace();
		   		      logger.debug("Method getSelectedSubject :: ERROR "+exp.getMessage());
		   		      }
		   		      
		    	return unAssoUser;
		      }
		 public void saveEduStudentReportData(ArrayList<EduStudentReportTO> markObj,EduStudentReportTO eduStudentReportTOObj)
		    {
		    	 StudentReportDAO.saveEduStudentReportData(markObj,eduStudentReportTOObj);
		    
		    }
		 public boolean isRecordExist(int student_id,int class_id,int session_id,String reqCome){
			 
			 return StudentReportDAO.isRecordExist(student_id, class_id, session_id,reqCome);
		 }
		 public String getResultData(String SearchKey,String searchBysessionkey,String searchByclasskey) 
		    {
		    	
				logger.debug("getResultData method==>"+"SearchKey"+SearchKey+"searchBysessionkey==>>"+searchBysessionkey+"searchByclasskey==>>"+searchByclasskey);
				ArrayList<EduStudentReportTO> eduStudentTOArrayObj = StudentReportDAO.getResultData("%"+SearchKey+"%","%"+searchBysessionkey+"%","%"+searchByclasskey+"%");
				String jsonStr ="";
			    JSONArray jsonArray = new JSONArray();
			    JSONObject jsonObj = new JSONObject();
			    
				try {
					     
					  int totalreturnrow = 0;
				      for(Iterator<EduStudentReportTO> iter = eduStudentTOArrayObj.iterator(); iter.hasNext();){
			   	      
				    	  EduStudentReportTO  eduStudentReportTOObj = iter.next();
					      JSONObject jsonObj1 = new JSONObject();
					      jsonObj1.put("student_id", eduStudentReportTOObj.getStudentid());
					      jsonObj1.put("student_name", eduStudentReportTOObj.getStudentname());
						  jsonObj1.put("dept_name",eduStudentReportTOObj.getDepartmentname());
						  jsonObj1.put("class", eduStudentReportTOObj.getClassname());
						  jsonObj1.put("status",eduStudentReportTOObj.getResult());
						  jsonObj1.put("division",eduStudentReportTOObj.getDivision());
						  jsonObj1.put("class_id",eduStudentReportTOObj.getClassid());
						  jsonObj1.put("session_id",eduStudentReportTOObj.getSessionid());
						  jsonObj1.put("dept_id",eduStudentReportTOObj.getDept_id());
						  // totalreturnrow = studentReporttoojb.getTotalreturnrow();
					      jsonArray.put(jsonObj1);
					    
					}  
				          jsonObj.put("records",jsonArray);
				       
			   }catch (JSONException exp) {
				// TODO Auto-generated catch block
				exp.printStackTrace();
				logger.debug("Method getStudentDetails :: ERROR "+exp.getMessage());
			    }
			   	jsonStr = jsonObj.toString();  
			   	logger.debug("Return a JSONObject from getStudentDetails method");
				return jsonArray.toString();
		   }
		 
		 public ArrayList<EduStudentReportTO> getSubjectMarkByStudent(String strToken){
			 
			 return StudentReportDAO.getSubjectMarkByStudent(strToken);
			 
			 
		 }
		 public ArrayList<EduStudentReportTO> getDataForGreenSheet(String classId,String sessionId){
			 
			 return StudentReportDAO.getDataForGreenSheet(classId,sessionId);
			 
		 }
 		 
		 public ArrayList<EduStudentReportTO> getSubjectClassByStudent(String student_id){
			 
			 return StudentReportDAO.getSubjectClassByStudent(student_id);
		 }
         public ArrayList<EduStudentReportTO> getSubjectClassByStudent(String student_id,String class_id,String session_id){
			 
			 return StudentReportDAO.getSubjectClassByStudent(student_id,class_id,session_id);
		 }
		 public String getStudentProfileList(String recordsReturned,String startIndex,String sort,String dir,String SearchKey) 
		    {
		    	
				int totalRecord = recordsReturned!=null?Integer.parseInt(recordsReturned):0;
				int startInd = startIndex!=null?Integer.parseInt(startIndex):0;
				logger.debug("Parameter for method getStudentDetails recordsReturned==>"+recordsReturned+"startIndex==>"+startIndex+"sort==>"+sort+"dir==>"+dir+"SearchKey"+SearchKey);
				ArrayList<EduStudentReportTO> eduStudentTOArrayObj = StudentReportDAO.getStudentProfileList(totalRecord,startInd,sort,dir,"%"+SearchKey+"%");
				String jsonStr ="";
			    JSONArray jsonArray = new JSONArray();
			    JSONObject jsonObj = new JSONObject();
			    
				try {
					     
					  int totalreturnrow = 0;
				      for(Iterator<EduStudentReportTO> iter = eduStudentTOArrayObj.iterator(); iter.hasNext();){
			   	      
				    	  EduStudentReportTO  eduStudentTOObj = iter.next();
					      JSONObject jsonObj1 = new JSONObject();
					      jsonObj1.put("student_id", eduStudentTOObj.getStudentid());
					      jsonObj1.put("user_id", eduStudentTOObj.getUserid());
					      jsonObj1.put("student_name", eduStudentTOObj.getStudentname());
						  jsonObj1.put("Phone", eduStudentTOObj.getPhoneno());
						  jsonObj1.put("Email",eduStudentTOObj.getEmailid());
						  jsonObj1.put("admissiondate",eduStudentTOObj.getAdmissiondate());
						  jsonObj1.put("dateofbirth",eduStudentTOObj.getDateofbirth());
						  // totalreturnrow = studentReporttoojb.getTotalreturnrow();
					      jsonArray.put(jsonObj1);
					    
					}  
				          jsonObj.put("recordsReturned", ""+totalreturnrow);
						  jsonObj.put("totalRecords", ""+totalRecord);
						  jsonObj.put("startIndex", startIndex);
						  jsonObj.put("sort", sort);
						  jsonObj.put("dir",dir);
						  jsonObj.put("pageSize",recordsReturned);
					      jsonObj.put("records",jsonArray);
				       
			   }catch (JSONException exp) {
				// TODO Auto-generated catch block
				exp.printStackTrace();
				logger.debug("Method getStudentDetails :: ERROR "+exp.getMessage());
			    }
			   	jsonStr = jsonObj.toString();  
			   	logger.debug("Return a JSONObject from getStudentDetails method");
				return jsonStr;
		   }
		 
		 public String getResultDataForNewSession(String recordsReturned,String startIndex,String sort,String dir,String SearchKey,String searchBysessionkey,String searchByclasskey) 
		    {
			 
				int totalRecord = recordsReturned!=null?Integer.parseInt(recordsReturned):0;
				logger.debug("getResultDataForNewSession method==>"+"SearchKey"+SearchKey+"searchBysessionkey==>>"+searchBysessionkey+"searchByclasskey==>>"+searchByclasskey);
				ArrayList<EduStudentReportTO> eduStudentTOArrayObj = StudentReportDAO.getResultData("%"+SearchKey+"%","%"+searchBysessionkey+"%","%"+searchByclasskey+"%");
				String jsonStr ="";
			    JSONArray jsonArray = new JSONArray();
			    JSONObject jsonObj = new JSONObject();
			    
				try {
					     
					  int totalreturnrow = 0;
				      for(Iterator<EduStudentReportTO> iter = eduStudentTOArrayObj.iterator(); iter.hasNext();){
			   	      
				    	  EduStudentReportTO  eduStudentReportTOObj = iter.next();
					      JSONObject jsonObj1 = new JSONObject();
					      jsonObj1.put("student_id", eduStudentReportTOObj.getStudentid());
					      jsonObj1.put("student_name", eduStudentReportTOObj.getStudentname());
						  jsonObj1.put("dept_name",eduStudentReportTOObj.getDepartmentname());
						  jsonObj1.put("class", eduStudentReportTOObj.getClassname());
						  jsonObj1.put("status",eduStudentReportTOObj.getResult());
						  jsonObj1.put("division",eduStudentReportTOObj.getDivision());
						  jsonObj1.put("class_id",eduStudentReportTOObj.getClassid());
						  jsonObj1.put("session_id",eduStudentReportTOObj.getSessionid());
						  jsonObj1.put("dept_id",eduStudentReportTOObj.getDept_id());
						  // totalreturnrow = studentReporttoojb.getTotalreturnrow();
					      jsonArray.put(jsonObj1);
					      totalreturnrow=totalreturnrow+1;
					    
					}  
				      
				      jsonObj.put("recordsReturned", ""+totalreturnrow);
					  jsonObj.put("totalRecords", ""+totalRecord);
					  jsonObj.put("startIndex", startIndex);
					  jsonObj.put("sort", sort);
					  jsonObj.put("dir",dir);
					  jsonObj.put("pageSize",recordsReturned);
				      jsonObj.put("records",jsonArray);
		    
			   }catch (JSONException exp) {
				// TODO Auto-generated catch block
				exp.printStackTrace();
				logger.debug("Method getStudentDetails :: ERROR "+exp.getMessage());
			    }
			   	jsonStr = jsonObj.toString();  
			   	logger.debug("Return a JSONObject from getResultDataForNewSession method"+jsonStr);
				return jsonStr;
		   }

	public ArrayList<EduStudentReportTO> getClassInfo()
	 {
		return StudentReportDAO.getClassinfo();
	}
			
	 public ArrayList<EduStudentReportTO> getDepartment()
		{
			return StudentReportDAO.getDepartment();
	}
	 
	 public EduStudentReportTO getComingSession(Integer sessionid){
		 
		 return StudentReportDAO.getComingSession(sessionid);
	 }

	 public boolean assignedClassSubToNewSession(EduStudentReportTO eduStudentReportToObj,EduStudentReportForm eduStudentReportFormObj){
		 
		 return StudentReportDAO.assignedClassSubToNewSession(eduStudentReportToObj,eduStudentReportFormObj);
	 }
}
