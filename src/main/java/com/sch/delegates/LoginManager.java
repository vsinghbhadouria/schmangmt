package com.sch.delegates;

import org.apache.log4j.Logger;

import com.sch.dao.LoginDAO;
import com.sch.to.LoginTO;
public class LoginManager
{
  LoginDAO LoginDAO;
  public static Logger logger = Logger.getLogger(LoginManager.class);
  public LoginManager()
  {
    this.LoginDAO = new LoginDAO();
  }

  public LoginTO checkLogin(String loginid, String password,Integer userType)
  {
    return this.LoginDAO.checkLogin(loginid, password,userType);
  }

  public boolean checkAccess(Integer ug_id, Integer f_id) {
    boolean flag = this.LoginDAO.checkAccess(ug_id, f_id);
    logger.debug("Method checkAccess :: Access Status "+flag);
    return flag;
  }

  public Boolean checkPassword(String password, String user_id,String ug_id)
  {
    boolean flag = this.LoginDAO.checkPassword(password, user_id,ug_id);
    logger.debug("Method checkPassword :: Password Validity "+flag);
    return Boolean.valueOf(flag);
  }

  public void savePassword(LoginTO loginToObj)
  {
    this.LoginDAO.savePassword(loginToObj);
  }

  public void loginUserInfo(LoginTO objloginto) {
    this.LoginDAO.loginUserInfo(objloginto);
  }
}