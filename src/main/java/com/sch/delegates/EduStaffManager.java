 package com.sch.delegates;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.sch.dao.EduStaffDAO;
import com.sch.form.EduStaffForm;
import com.sch.to.EduStaffTO;
import com.sch.to.EduStudentReportTO;
import com.sch.to.EduStudentTO;

public class EduStaffManager 
{
    
	EduStaffDAO eduStaffDAOObj; 	
	public static Logger logger = Logger.getLogger(EduStaffManager.class);
    public EduStaffManager() {

    	eduStaffDAOObj = new EduStaffDAO();
    }
    public String getStaffDetails(String recordsReturned,String startIndex,String sort,String dir,String SearchKey) 
    {
    	
    	
		int totalRecord = recordsReturned!=null?Integer.parseInt(recordsReturned):0;
		int startInd = startIndex!=null?Integer.parseInt(startIndex):0; 
		logger.debug("parameter for method getStaffDetails recordsReturned==>"+recordsReturned+" startIndex==>"+startIndex+" sort==>"+sort+" dir==>"+dir+" SearchKey==>"+SearchKey);
		ArrayList<EduStaffTO> eduStaffTOArrayObj = eduStaffDAOObj.getStaffDetails(totalRecord,startInd,sort,dir,"%"+SearchKey+"%");
		String jsonStr ="";
	    JSONArray jsonArray = new JSONArray();
	    JSONObject jsonObj = new JSONObject();
	    
		try {
			     
			  int totalreturnrow = 0;
		      for(Iterator<EduStaffTO> iter = eduStaffTOArrayObj.iterator(); iter.hasNext();){
	   	      
		    	  EduStaffTO  eduStaffTOObj = iter.next();
			      JSONObject jsonObj1 = new JSONObject();
			      jsonObj1.put("user_id", eduStaffTOObj.getEmpid());
			      jsonObj1.put("first_name", eduStaffTOObj.getFirstname()+" "+eduStaffTOObj.getLastname());
				  jsonObj1.put("emp_title", eduStaffTOObj.getLevel());
				  jsonObj1.put("Phone", eduStaffTOObj.getPhoneno());
				  jsonObj1.put("Email",eduStaffTOObj.getEmailid());
				 // totalreturnrow = edustaffToObjtoojb.getTotalreturnrow();
			      jsonArray.put(jsonObj1);
			     
			}  
		          jsonObj.put("recordsReturned", ""+totalreturnrow);
				  jsonObj.put("totalRecords", ""+totalRecord);
				  jsonObj.put("startIndex", startIndex);
				  jsonObj.put("sort", sort);
				  jsonObj.put("dir",dir);
				  jsonObj.put("pageSize",recordsReturned);
			      jsonObj.put("records",jsonArray);
		       
	   }catch (JSONException exp) {
		exp.printStackTrace();
		logger.debug("Method getStudentDetails :: ERROR "+exp.getMessage());
	    }
	   	jsonStr = jsonObj.toString();  
	   	logger.debug("Return a JSONObject from getStaffDetails method");
		return jsonStr;
   }
    public EduStaffTO saveEduStaffData(EduStaffTO eduStaffTOObj,EduStaffForm eduStaffFormObj)
    {
    	return eduStaffDAOObj.saveEduStaffData(eduStaffTOObj,eduStaffFormObj);
    
    }
    public void updateStaffData(EduStaffTO edustaffToObj,EduStaffForm edustaffFormObj)
    {
    	eduStaffDAOObj.updateStaffData(edustaffToObj,edustaffFormObj);
    
    }
    public ArrayList<EduStaffTO> getStaffLevel()
	{
    	return eduStaffDAOObj.getStaffLevel();
	}
	public ArrayList<EduStaffTO> getStaffQualification()
	{
    	return eduStaffDAOObj.getStaffQualification();
    }
	public ArrayList<EduStaffTO> getDepartment()
	{
    	return eduStaffDAOObj.getDepartment();
    }
	
	public ArrayList<String> getClassInfo()
    {
      ArrayList<EduStaffTO> userlist = eduStaffDAOObj.getClassinfo();
	  ArrayList<String> unAssoUser= new ArrayList<String>();
	  
	try {
		  for(EduStaffTO iter: userlist){
			unAssoUser.add(iter.getClassid()+","+iter.getClassname());
		}
     }
      catch(Exception exp){exp.printStackTrace();
      logger.debug("Method getClassInfo :: ERROR "+exp.getMessage());
      }
      
      return unAssoUser;
    }
	
	
	public ArrayList<String> getClassInfoByDepartment(String dept_id)
    {
		
      ArrayList<EduStaffTO> userlist = eduStaffDAOObj.getClassinfoByDepartment(dept_id);
	  ArrayList<String> unAssoUser= new ArrayList<String>();
	  
	try {
		  for(EduStaffTO iter: userlist){
			unAssoUser.add(iter.getClassid()+","+iter.getClassname());
		}
     }
      catch(Exception exp){exp.printStackTrace();
      logger.debug("Method getClassInfoByDepartment :: ERROR "+exp.getMessage());
      }
      
      return unAssoUser;
    }
	public ArrayList<String> getSubjectByDept(String dept_id)
    {
		
      ArrayList<EduStaffTO> userlist = eduStaffDAOObj.getSubjectByDept(dept_id);
	  ArrayList<String> unAssoUser= new ArrayList<String>();
	  
	try {
		  for(EduStaffTO iter: userlist){
			unAssoUser.add(iter.getClassid()+","+iter.getClassname());
		}
     }
      catch(Exception exp){exp.printStackTrace();
      logger.debug("Method getSubjectByDept :: ERROR "+exp.getMessage());
      }
      
      return unAssoUser;
    }

  public ArrayList<String> getUnselectedClassInfo(String userid)
    {
      ArrayList<EduStaffTO> userlist = eduStaffDAOObj.getUnAssoClassInfo(userid);
	  ArrayList<String> unAssoUser= new ArrayList<String>();
	  
	try {
		  for(EduStaffTO iter: userlist){
			unAssoUser.add(iter.getClassid()+","+iter.getClassname());
			}
	     }
	      catch(Exception exp){exp.printStackTrace();
	      logger.debug("Method getUnselectedClassInfo :: ERROR "+exp.getMessage());
	      }
	      
	return unAssoUser;
    }

  public ArrayList<String>getSelectedClass(String userid,String deptid)
  {
      ArrayList<EduStaffTO> selectedClass = eduStaffDAOObj.getSelectedClass(userid,deptid);
	  ArrayList<String> unAssoUser= new ArrayList<String>();
		try {
		  for(EduStaffTO iter: selectedClass){
				unAssoUser.add(iter.getClassid()+","+iter.getClassname());
   				}
   		     }
   		      catch(Exception exp){exp.printStackTrace();
   		      logger.debug("Method getSelectedClass :: ERROR "+exp.getMessage());
   		      }
   		      
    	return unAssoUser;
      }
  public ArrayList<String>getSelectedSubject(String userid,String deptid)
  {
      ArrayList<EduStaffTO> selectedSubject = eduStaffDAOObj.getSelectedSubject(userid,deptid);
	  ArrayList<String> unAssoUser= new ArrayList<String>();
		try {
		  for(EduStaffTO iter: selectedSubject){
				unAssoUser.add(iter.getSubjectid()+","+iter.getSubjectname());
   				}
   		     }
   		      catch(Exception exp){exp.printStackTrace();
   		      logger.debug("Method getSelectedSubject :: ERROR "+exp.getMessage());
   		      }
   		      
    	return unAssoUser;
      }
  
	  public EduStaffTO getEduStaffDatabyUserId(String user_Id,String emp_status)
	  {
	   return eduStaffDAOObj.getEduStaffDatabyUserId(user_Id,emp_status);	
	  }

	  public void deleteStaffDetailbyUserId(String userid) {
			eduStaffDAOObj.deleteStaffDetailbyUserId(userid);
		}
	  
	  public String getAlumniStaffDetails(String recordsReturned,String startIndex,String sort,String dir,String SearchKey) 
	    {
	    	
	    	
			int totalRecord = recordsReturned!=null?Integer.parseInt(recordsReturned):0;
			int startInd = startIndex!=null?Integer.parseInt(startIndex):0; 
			logger.debug("parameter for method getStaffDetails recordsReturned==>"+recordsReturned+" startIndex==>"+startIndex+" sort==>"+sort+" dir==>"+dir+" SearchKey==>"+SearchKey);
			ArrayList<EduStaffTO> eduStaffTOArrayObj = eduStaffDAOObj.getAlumniStaffDetails(totalRecord,startInd,sort,dir,"%"+SearchKey+"%");
			String jsonStr ="";
		    JSONArray jsonArray = new JSONArray();
		    JSONObject jsonObj = new JSONObject();
		    
			try {
				     
				  int totalreturnrow = 0;
			      for(Iterator<EduStaffTO> iter = eduStaffTOArrayObj.iterator(); iter.hasNext();){
		   	      
			    	  EduStaffTO  eduStaffTOObj = iter.next();
				      JSONObject jsonObj1 = new JSONObject();
				      jsonObj1.put("user_id", eduStaffTOObj.getEmpid());
				      jsonObj1.put("first_name", eduStaffTOObj.getFirstname()+" "+eduStaffTOObj.getLastname());
					  jsonObj1.put("emp_title", eduStaffTOObj.getLevel());
					  jsonObj1.put("Phone", eduStaffTOObj.getPhoneno());
					  jsonObj1.put("Email",eduStaffTOObj.getEmailid());
					 // totalreturnrow = edustaffToObjtoojb.getTotalreturnrow();
				      jsonArray.put(jsonObj1);
				     
				}  
			          jsonObj.put("recordsReturned", ""+totalreturnrow);
					  jsonObj.put("totalRecords", ""+totalRecord);
					  jsonObj.put("startIndex", startIndex);
					  jsonObj.put("sort", sort);
					  jsonObj.put("dir",dir);
					  jsonObj.put("pageSize",recordsReturned);
				      jsonObj.put("records",jsonArray);
			       
		   }catch (JSONException exp) {
			exp.printStackTrace();
			logger.debug("Method getStudentDetails :: ERROR "+exp.getMessage());
		    }
		   	jsonStr = jsonObj.toString();  
		   	logger.debug("Return a JSONObject from getStaffDetails method");
			return jsonStr;
	   }
	  
	 public ArrayList<EduStudentReportTO> getSubjectClassByUser(String user_id){
			 
			 return eduStaffDAOObj.getSubjectClassByUser(user_id);
		 }
	 
	 public ArrayList<String>getTeacherList(String selectionFlag)
	  {
	      ArrayList<EduStaffTO> selectedSubject = eduStaffDAOObj.getTeacherList(selectionFlag);
		  ArrayList<String> unAssoUser= new ArrayList<String>();
			try {
			  for(EduStaffTO iter: selectedSubject){
					unAssoUser.add(iter.getEmpid()+","+iter.getEmpid()+" "+iter.getFirstname());
	   				}
	   		     }
	   		      catch(Exception exp){exp.printStackTrace();
	   		      logger.debug("Method getTeacherList :: ERROR "+exp.getMessage());
	   		      }
	   		      
	    	return unAssoUser;
	  }
	  
	 public EduStaffTO getClassname(String class_id ){
		 
		 return eduStaffDAOObj.getClassname(class_id);
	 }
   	
}
