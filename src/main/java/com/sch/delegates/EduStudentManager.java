 package com.sch.delegates;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.sch.dao.EduStudentDAO;
import com.sch.form.EduStudentForm;
import com.sch.to.*;



public class EduStudentManager 
{
	EduStudentDAO eduStudentDAO; 
	public static Logger logger = Logger.getLogger(EduStudentManager.class);
    public EduStudentManager() {

    	eduStudentDAO = new EduStudentDAO();
    }
    public String getStudentDetails(String recordsReturned,String startIndex,String sort,String dir,String SearchKey,String searchBysessionkey,String searchByclasskey) 
    {
    	
		int totalRecord = recordsReturned!=null?Integer.parseInt(recordsReturned):0;
		int startInd = startIndex!=null?Integer.parseInt(startIndex):0;
		logger.debug("Parameter for method getStudentDetails recordsReturned==>"+recordsReturned+"startIndex==>"+startIndex+"sort==>"+sort+"dir==>"+dir+"SearchKey"+SearchKey+"searchBysessionkey==>>"+searchBysessionkey+"searchByclasskey==>>"+searchByclasskey);
		ArrayList<EduStudentTO> eduStudentTOArrayObj = eduStudentDAO.getStudentDetails(totalRecord,startInd,sort,dir,"%"+SearchKey+"%","%"+searchBysessionkey+"%","%"+searchByclasskey+"%");
		String jsonStr ="";
	    JSONArray jsonArray = new JSONArray();
	    JSONObject jsonObj = new JSONObject();
	    
		try {
			     
			  int totalreturnrow = 0;
		      for(Iterator<EduStudentTO> iter = eduStudentTOArrayObj.iterator(); iter.hasNext();){
	   	      
		    	  EduStudentTO  eduStudentTOObj = iter.next();
			      JSONObject jsonObj1 = new JSONObject();
			      jsonObj1.put("user_id", eduStudentTOObj.getStudentid());
			      jsonObj1.put("first_name", eduStudentTOObj.getFirstname()+" "+eduStudentTOObj.getLastname());
				  jsonObj1.put("Phone", eduStudentTOObj.getPhoneno());
				  jsonObj1.put("Email",eduStudentTOObj.getEmailid());
				  jsonObj1.put("Class",eduStudentTOObj.getClassname());
				  jsonObj1.put("Section",eduStudentTOObj.getSection());
				  jsonObj1.put("Session",eduStudentTOObj.getSession());
				  jsonObj1.put("session_id",eduStudentTOObj.getSessionId());
				  // totalreturnrow = eduStudenttoojb.getTotalreturnrow();
			      jsonArray.put(jsonObj1);
			    
			}  
		          jsonObj.put("recordsReturned", ""+totalreturnrow);
				  jsonObj.put("totalRecords", ""+totalRecord);
				  jsonObj.put("startIndex", startIndex);
				  jsonObj.put("sort", sort);
				  jsonObj.put("dir",dir);
				  jsonObj.put("pageSize",recordsReturned);
			      jsonObj.put("records",jsonArray);
		       
	   }catch (JSONException exp) {
		// TODO Auto-generated catch block
		exp.printStackTrace();
		logger.debug("Method getStudentDetails :: ERROR "+exp.getMessage());
	    }
	   	jsonStr = jsonObj.toString();  
	   	logger.debug("Return a JSONObject from getStudentDetails method");
		return jsonStr;
   }
    public EduStudentTO saveEduStudentData(EduStudentTO eduStudentTOObj,EduStudentForm eduStudentFormObj)
    {
    	return eduStudentDAO.saveEduStudentData(eduStudentTOObj,eduStudentFormObj);
    
    }
    public void updateStudentData(EduStudentTO eduStudentTOObj,EduStudentForm eduStudentFormObj)
    {
    	eduStudentDAO.updateStudentData(eduStudentTOObj,eduStudentFormObj);
    
    }
/*    public ArrayList<EduStudentTO> getStudentLevel()
	{
    	return eduStudentDAO.getStudentLevel();
	}
*/	public ArrayList<EduStudentTO> getStudentQualification()
	{
    	return eduStudentDAO.getStudentQualification();
    }
	public ArrayList<EduStudentTO> getParentHigherQualification()
	{
    	return eduStudentDAO.getParentHigherQualification();
    }
	
	public ArrayList<EduStudentTO> getClassInfo(String userId)
    {
      return eduStudentDAO.getClassinfo(userId);
    }
	public ArrayList<EduStudentTO> getClassInfo()
    {
      return eduStudentDAO.getClassinfo();
    }
	public ArrayList<EduStudentTO> getAdmissionfees()
    {
      return eduStudentDAO.getAdmissionfees();
    }
	
	public Integer getAdmissionfee(Integer classid)
    {
      return eduStudentDAO.getAdmissionfee(classid);
    }
	public EduStudentTO getEduStudentDatabyUserId(String user_Id,String session_Id)
	{
	  return eduStudentDAO.getEduStudentDatabyUserId(user_Id,session_Id);	
    }
	public EduStudentTO viewEduStudentDatabyUserId(String user_Id,String session_Id,String stud_flag)
	{
	  return eduStudentDAO.viewEduStudentDatabyUserId(user_Id,session_Id,stud_flag);	
    }

	public void deleteStudentDetailbyUserId(String student_Id) {
			eduStudentDAO.deleteStudentDetailbyUserId(student_Id);
		}
	public ArrayList<String> getClasses()
	    {
	      ArrayList<EduStudentTO> userlist = eduStudentDAO.getClassinfo();
		  ArrayList<String> unAssoUser= new ArrayList<String>();
		  unAssoUser.add(""+","+"-----Classes-----");
		  
		try {
			  for(EduStudentTO iter: userlist){
				unAssoUser.add(iter.getClassid()+","+iter.getClassname());
			}
	     }
	      catch(Exception exp){exp.printStackTrace();
	      logger.debug("Method getClasses :: ERROR "+exp.getMessage());
	      }
	      
	      return unAssoUser;
	    }
	  public ArrayList<String> getSession()
	  {
		  ArrayList<String> sessionList = new ArrayList<String>();
		  sessionList = eduStudentDAO.getSession();
		  System.out.println("SiZe of array List"+sessionList.size());
		  return sessionList;
	  }
	  public ArrayList<String> getCurrentSession()
	  {
		  ArrayList<String> sessionList = new ArrayList<String>();
		  sessionList = eduStudentDAO.getCurrentSession();
		  System.out.println("SiZe of array List"+sessionList.size());
		  return sessionList;
	  }
	  
	  public ArrayList<EduStudentTO> getDepartment()
		{
	    	return eduStudentDAO.getDepartment();
	    }
		public ArrayList<String> getSubjectByClass(String dept_id,String classid)
	    {
			
	      ArrayList<EduStudentTO> userlist = eduStudentDAO.getSubjectByClass(dept_id,classid);
		  ArrayList<String> unAssoUser= new ArrayList<String>();
		  
		try {
			  for(EduStudentTO iter: userlist){
				unAssoUser.add(iter.getClassid()+","+iter.getClassname());
			}
	     }
	      catch(Exception exp){exp.printStackTrace();
	      logger.debug("Method getSubjectByClass :: ERROR "+exp.getMessage());
	      }
	      
	      return unAssoUser;
	    }
		
		 public ArrayList<String>getSelectedSubject(String userid,String classid,String sessionId)
		  {
		      ArrayList<EduStudentTO> selectedSubjectObj = eduStudentDAO.getSelectedSubject(userid,classid,sessionId);
			  ArrayList<String> unAssoUser= new ArrayList<String>();
				try {
				  for(EduStudentTO iter: selectedSubjectObj){
						unAssoUser.add(iter.getSubjectid()+","+iter.getSubjectname());
		   				}
		   		     }
		   		      catch(Exception exp){exp.printStackTrace();
		   		      logger.debug("Method getSelectedSubject :: ERROR "+exp.getMessage());
		   		      }
		   		      
		    	return unAssoUser;
		      }
		    public String getAlumniStudentDetails(String recordsReturned,String startIndex,String sort,String dir,String SearchKey,String searchBysessionkey,String searchByclasskey) 
		    {
		    	
				int totalRecord = recordsReturned!=null?Integer.parseInt(recordsReturned):0;
				int startInd = startIndex!=null?Integer.parseInt(startIndex):0;
				logger.debug("Parameter for method getStudentDetails recordsReturned==>"+recordsReturned+"startIndex==>"+startIndex+"sort==>"+sort+"dir==>"+dir+"SearchKey"+SearchKey+"searchBysessionkey==>>"+searchBysessionkey+"searchByclasskey==>>"+searchByclasskey);
				ArrayList<EduStudentTO> eduStudentTOArrayObj = eduStudentDAO.getAlumniStudentDetails(totalRecord,startInd,sort,dir,"%"+SearchKey+"%","%"+searchBysessionkey+"%","%"+searchByclasskey+"%");
				String jsonStr ="";
			    JSONArray jsonArray = new JSONArray();
			    JSONObject jsonObj = new JSONObject();
			    
				try {
					     
					  int totalreturnrow = 0;
				      for(Iterator<EduStudentTO> iter = eduStudentTOArrayObj.iterator(); iter.hasNext();){
			   	      
				    	  EduStudentTO  eduStudentTOObj = iter.next();
					      JSONObject jsonObj1 = new JSONObject();
					      jsonObj1.put("user_id", eduStudentTOObj.getStudentid());
					      jsonObj1.put("first_name", eduStudentTOObj.getFirstname()+" "+eduStudentTOObj.getLastname());
						  jsonObj1.put("Phone", eduStudentTOObj.getPhoneno());
						  jsonObj1.put("Email",eduStudentTOObj.getEmailid());
						  jsonObj1.put("Class",eduStudentTOObj.getClassname());
						  jsonObj1.put("Section",eduStudentTOObj.getSection());
						  jsonObj1.put("Session",eduStudentTOObj.getSession());
						  jsonObj1.put("session_id",eduStudentTOObj.getSessionId());
						  // totalreturnrow = eduStudenttoojb.getTotalreturnrow();
					      jsonArray.put(jsonObj1);
					    
					}  
				          jsonObj.put("recordsReturned", ""+totalreturnrow);
						  jsonObj.put("totalRecords", ""+totalRecord);
						  jsonObj.put("startIndex", startIndex);
						  jsonObj.put("sort", sort);
						  jsonObj.put("dir",dir);
						  jsonObj.put("pageSize",recordsReturned);
					      jsonObj.put("records",jsonArray);
				       
			   }catch (JSONException exp) {
				// TODO Auto-generated catch block
				exp.printStackTrace();
				logger.debug("Method getStudentDetails :: ERROR "+exp.getMessage());
			    }
			   	jsonStr = jsonObj.toString();  
			   	logger.debug("Return a JSONObject from getStudentDetails method");
				return jsonStr;
		   }
		    
		    
		    
			 public ArrayList<String>getStudentList(String classId,String selectionFlag)
			  {
			      ArrayList<EduStudentTO> selectedSubject = eduStudentDAO.getStudentList(classId,selectionFlag);
				  ArrayList<String> unAssoUser= new ArrayList<String>();
					try {
					  for(EduStudentTO iter: selectedSubject){
							unAssoUser.add(iter.getStudentid()+","+iter.getStudentid()+" "+iter.getFirstname());
			   				}
			   		     }
			   		      catch(Exception exp){exp.printStackTrace();
			   		      logger.debug("Method getParentList :: ERROR "+exp.getMessage());
			   		      }
			   		      
			    	return unAssoUser;
			  }
			 
			 public ArrayList<String>getParentList()
			  {
			      ArrayList<EduStudentTO> selectedSubject = eduStudentDAO.getParentList();
				  ArrayList<String> unAssoUser= new ArrayList<String>();
					try {
					  for(EduStudentTO iter: selectedSubject){
							unAssoUser.add(iter.getParentid()+","+iter.getParentid()+" "+iter.getFathername());
			   				}
			   		     }
			   		      catch(Exception exp){exp.printStackTrace();
			   		      logger.debug("Method getParentList :: ERROR "+exp.getMessage());
			   		      }
			   		      
			    	return unAssoUser;
			  }
			 
			 
	}
