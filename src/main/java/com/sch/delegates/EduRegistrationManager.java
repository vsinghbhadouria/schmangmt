 package com.sch.delegates;


import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.sch.dao.EduRegistrationDAO;
import com.sch.form.EduRegistrationForm;
import com.sch.to.*;
public class EduRegistrationManager 
{
    
	EduRegistrationDAO EduRegistrationDAO; 	
	public static Logger logger = Logger.getLogger(EduRegistrationManager.class);
    public EduRegistrationManager() {

    	EduRegistrationDAO = new EduRegistrationDAO();
    }
      
    public String getRegistrationDetails(String recordsReturned,String startIndex,String sort,String dir,String SearchKey,String searchBysessionkey,String searchByclasskey,String regFlag) 
    {
    	
		int totalRecord = recordsReturned!=null?Integer.parseInt(recordsReturned):0;
		int startInd = startIndex!=null?Integer.parseInt(startIndex):0; 
		logger.debug("Parameter for method getRegistrationDetails recordsReturned==>"+recordsReturned+"startIndex==>"+startIndex+"sort==>"+sort+"dir==>"+dir+"SearchKey"+SearchKey+"searchBysessionkey==>>"+searchBysessionkey+"searchByclasskey==>>"+searchByclasskey);
		ArrayList<EduRegistrationTO> EduRegistrationTOArrayObj = EduRegistrationDAO.getRegistrationDetails(totalRecord,startInd,sort,dir,"%"+SearchKey+"%","%"+searchBysessionkey+"%","%"+searchByclasskey+"%",regFlag);
		String jsonStr ="";
	    JSONArray jsonArray = new JSONArray();
	    JSONObject jsonObj = new JSONObject();
		try {
			  int totalreturnrow = 0;
		      for(Iterator<EduRegistrationTO> iter = EduRegistrationTOArrayObj.iterator(); iter.hasNext();){
		    	  EduRegistrationTO  eduRegistrationTOObj = iter.next();
			      JSONObject jsonObj1 = new JSONObject();
			      jsonObj1.put("user_id", eduRegistrationTOObj.getRegistrationid());
			      jsonObj1.put("first_name", eduRegistrationTOObj.getFirstname()+" "+eduRegistrationTOObj.getLastname());
				  jsonObj1.put("Phone", eduRegistrationTOObj.getPhoneno());
				  jsonObj1.put("Email",eduRegistrationTOObj.getEmailid());
				  jsonObj1.put("Class",eduRegistrationTOObj.getClassname());
			      jsonObj1.put("Session",eduRegistrationTOObj.getSession());
				  jsonObj1.put("session_id",eduRegistrationTOObj.getSessionId());
				  jsonArray.put(jsonObj1);
			    
			}  
		          jsonObj.put("recordsReturned", ""+totalreturnrow);
				  jsonObj.put("totalRecords", ""+totalRecord);
				  jsonObj.put("startIndex", startIndex);
				  jsonObj.put("sort", sort);
				  jsonObj.put("dir",dir);
				  jsonObj.put("pageSize",recordsReturned);
			      jsonObj.put("records",jsonArray);
		       
	   }catch (JSONException exp) {
		// TODO Auto-generated catch block
		exp.printStackTrace();
		logger.debug("Method getRegistrationDetails :: ERROR "+exp.getMessage());
	    }
	   	jsonStr = jsonObj.toString();   	
	   	logger.debug("Return a JSONObject from getRegistrationDetails method");
		return jsonStr;
   }
    public void saveEduRegistrationData(EduRegistrationTO eduRegistrationTOObj)
    {
    	EduRegistrationDAO.saveEduRegistrationData(eduRegistrationTOObj);
    
    }
    public void updateRegistrationData(EduRegistrationTO eduRegistrationTOOJ)
    {
    	EduRegistrationDAO.updateRegistrationData(eduRegistrationTOOJ);
    
    }
/*    public ArrayList<EduRegistrationTO> getRegistrationLevel()
	{
    	return EduRegistrationDAO.getRegistrationLevel();
	}
*/	public ArrayList<EduRegistrationTO> getRegistrationQualification()
	{
    	return EduRegistrationDAO.getRegistrationQualification();
    }
	public ArrayList<EduRegistrationTO> getParentHigherQualification()
	{
    	return EduRegistrationDAO.getParentHigherQualification();
    }
	
	public ArrayList<EduRegistrationTO> getClassInfo()
    {
      return EduRegistrationDAO.getClassinfo();
    }
	public ArrayList<EduRegistrationTO> getAdmissionfees()
    {
      return EduRegistrationDAO.getAdmissionfees();
    }
	
	public Integer getRegistrationfee(Integer classid)
    {
      return EduRegistrationDAO.getRegistrationfee(classid);
    }
	public EduRegistrationTO getEduRegistrationDatabyUserId(String user_Id,String session_Id,String reg_status)
	{
	  return EduRegistrationDAO.getEduRegistrationDatabyUserId(user_Id,session_Id,reg_status);	
    }

	  public void deleteRegistrationDetailbyUserId(String student_Id,String Session_id) {
			EduRegistrationDAO.deleteRegistrationDetailbyUserId(student_Id,Session_id);
		}
	  
	  public EduRegistrationTO saveEduStudentData(EduRegistrationTO eduRegistrationTOObj,EduRegistrationForm eduRegistrationFormObj)
	    {
	    	return EduRegistrationDAO.saveEduStudentData(eduRegistrationTOObj,eduRegistrationFormObj);
	    
	    }
	  
	  public ArrayList<EduRegistrationTO> getDepartment()
		{
	    	return EduRegistrationDAO.getDepartment();
	    }
	  public ArrayList<EduRegistrationTO> getClassInfo(String userId)
	    {
	      return EduRegistrationDAO.getClassinfo(userId);
	    }
  
}
