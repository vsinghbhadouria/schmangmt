package com.sch.delegates;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.sch.dao.ConcernTrackingDAO;

import com.sch.to.*;
public class ConcernTrackingManager 
{
	ConcernTrackingDAO ConcernTrackingDAO; 	
	
	public static Logger logger = Logger.getLogger(ConcernTrackingManager.class);
	
    public ConcernTrackingManager() {

	ConcernTrackingDAO = new ConcernTrackingDAO();
    }
    public String getIssueTrackingDetail(String recordsReturned,String startIndex,String sort,String dir ,String SearchKey ,String searchcategory,String searchpriority,String userid) 
    {
    	
		int totalRecord = recordsReturned!=null?Integer.parseInt(recordsReturned):0;
		int startInd = startIndex!=null?Integer.parseInt(startIndex):0; 
		
		int totalRecordinDB = ConcernTrackingDAO.getTotalRecord(SearchKey ,searchcategory,searchpriority,userid);
		
		logger.debug("");
		System.out.println("inside manager part"+totalRecordinDB);
		if(totalRecord>totalRecordinDB)
		{
			totalRecord = totalRecordinDB;
		}
		ArrayList<ConcernTrackingTO> issuetrackingobj = ConcernTrackingDAO.getIssueTrackingDetail(totalRecord,startInd,sort,dir,SearchKey ,searchcategory,searchpriority,userid);
		String jsonStr ="";
	    JSONArray jsonArray = new JSONArray();
	    JSONObject jsonObj = new JSONObject();
	    
		try {
			     
				  int totalreturnrow = 0;
		      for(Iterator<ConcernTrackingTO> iter = issuetrackingobj.iterator(); iter.hasNext();){
		    	  ConcernTrackingTO  issuetrackingtoojb = iter.next();
			      JSONObject jsonObj1 = new JSONObject();
			      String date = issuetrackingtoojb.getIssuesubmiteddate();
		    	  String date1[] = date.split(" "); 
			      String dates[] = date1[0].split("-");
			      dates[0] = dates[2]+"-"+dates[1]+"-"+dates[0];
			      jsonObj1.put("IssueId", issuetrackingtoojb.getIssueid());
				  jsonObj1.put("user_first_name", issuetrackingtoojb.getUsername());
				  jsonObj1.put("Issue_Title", issuetrackingtoojb.getIssuetitle());
				  jsonObj1.put("Issue_Desc", issuetrackingtoojb.getIssuedesc());
				  jsonObj1.put("ug_title", issuetrackingtoojb.getDesgnation());
				  jsonObj1.put("Category", issuetrackingtoojb.getCategory());
				  jsonObj1.put("Priority", issuetrackingtoojb.getPriority());
				  jsonObj1.put("Current_Status", issuetrackingtoojb.getStatus());
				  jsonObj1.put("user_id", issuetrackingtoojb.getUserid());
				  jsonObj1.put("reply", issuetrackingtoojb.getReplyid());
				  jsonObj1.put("issue_submited_date",dates[0]);
				  jsonArray.put(jsonObj1);
			}  
		          jsonObj.put("recordsReturned", ""+totalreturnrow);
				  jsonObj.put("totalRecords", ""+totalRecordinDB);
				  jsonObj.put("startIndex", startIndex);
				  jsonObj.put("sort", sort);
				  jsonObj.put("dir",dir);
				  jsonObj.put("pageSize",recordsReturned);
			      jsonObj.put("records",jsonArray);
		       
	   }catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	   	jsonStr = jsonObj.toString();
	   	return jsonStr;
   }
    public String getIssueTrackingDetailbyIssue_id(String recordsReturned,String startIndex,String sort,String dir ,String SearchKey ,String searchcategory,String searchpriority,String issue_id) 
    {
    	
		int totalRecord = recordsReturned!=null?Integer.parseInt(recordsReturned):0;
		int startInd = startIndex!=null?Integer.parseInt(startIndex):0; 
		totalRecord = totalRecord+startInd;
		int totalRecordinDB = ConcernTrackingDAO.getTotalRecordbyIssueId((issue_id.trim()));
		if(totalRecord>totalRecordinDB)
		{
			totalRecord = totalRecordinDB;
		}
		ArrayList<ConcernTrackingTO> issuetrackingobj = ConcernTrackingDAO.getIssueTrackingDetailbyIssue_id(totalRecord,startInd,sort,dir,SearchKey ,searchcategory,searchpriority,issue_id);
		String jsonStr ="";
	    JSONArray jsonArray = new JSONArray();
	    JSONObject jsonObj = new JSONObject();
	    
		try {
				  int totalreturnrow = 0;
		          for(Iterator<ConcernTrackingTO> iter = issuetrackingobj.iterator(); iter.hasNext();){
		    	  ConcernTrackingTO  issuetrackingtoojb = iter.next();
		    	  String date = issuetrackingtoojb.getIssuerepydate();
		    	  String date1[] = date.split(" "); 
			      String dates[] = date1[0].split("-");
			      dates[0] = dates[2]+"-"+dates[1]+"-"+dates[0]+" "+date1[1];
			      JSONObject jsonObj1 = new JSONObject();
			      jsonObj1.put("IssueId", issuetrackingtoojb.getIssueid());
				  jsonObj1.put("user_first_name", issuetrackingtoojb.getUsername());
				  jsonObj1.put("reply_desc", issuetrackingtoojb.getAnsdisc());
				  jsonObj1.put("ug_title", issuetrackingtoojb.getDesgnation());
				  jsonObj1.put("issue_reply_date",dates[0]);
				  jsonObj1.put("reply_id", issuetrackingtoojb.getReplyid());
				  jsonObj1.put("Status", issuetrackingtoojb.getStatus());
				  jsonObj1.put("user_id", issuetrackingtoojb.getUserid());
				  jsonArray.put(jsonObj1);
			}  
		          jsonObj.put("recordsReturned", ""+totalreturnrow);
				  jsonObj.put("totalRecords", ""+totalRecordinDB);
				  jsonObj.put("startIndex", startIndex);
				  jsonObj.put("sort", sort);
				  jsonObj.put("dir",dir);
				  jsonObj.put("pageSize",recordsReturned);
			      jsonObj.put("records",jsonArray);
	   }catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	   	jsonStr = jsonObj.toString();
		return jsonStr;
   }
    
    public void saveIssue(ConcernTrackingTO objissueto)
    {
    	ConcernTrackingDAO.saveIssue(objissueto);
    }
   public ConcernTrackingTO getConcernByissueId(String issueid)
   {
	   
	   
	   return ConcernTrackingDAO.getConcernByissueId(issueid);
	   
   }
   public void editIssueTrackingByIssueId(ConcernTrackingTO objissuetrackingto)
   {
	   ConcernTrackingDAO.editIssueTrackingByIssueId( objissuetrackingto);
   }
   public void deleteIssueTracking(String issueid)
   {
	   ConcernTrackingDAO.deleteIssueTracking( issueid);
   }
   public void replyConcernByIssueId(ConcernTrackingTO replyObj)
   {
	   ConcernTrackingDAO.replyConcernByIssueId(replyObj);
   }
   public void deleteIssuereply(String reply_id)
   {
	   ConcernTrackingDAO.deleteIssuereply(reply_id);
   }
   public ConcernTrackingTO getDataByreplyId(String replyid)
   {
	   return ConcernTrackingDAO.getDataByreplyId(replyid);
	}
   public void editIssueReplyByIssueId(ConcernTrackingTO objissue)
   {
	   ConcernTrackingDAO.editIssueReplyByIssueId(objissue);
   }
   public String checkForExecutive(String userid)
   {
	   
	   return ConcernTrackingDAO.checkForExecutive(userid);
   }
   public  ArrayList<String> getAllCategory()
   {
	   ArrayList<ConcernTrackingTO> objissuetracking = ConcernTrackingDAO.getAllCategory();
	   ArrayList<String> categoryList= new ArrayList<String>();

	   categoryList.add( ""+","+"Select Category");
	   
	   for(ConcernTrackingTO objissue :objissuetracking)
	   {
		   categoryList.add(objissue.getCatid()+","+objissue.getCategory());
	   }
	   return categoryList;
   }
   
  
   public String[] getAllCategorys()
   {
	   ArrayList<ConcernTrackingTO> objissuetracking = ConcernTrackingDAO.getAllCategory();
	   String str[]= new String[(objissuetracking.size()+1)];
	   str[0] = "Select Category"+","+"Select Category";
	   int xx =1;
	   for(ConcernTrackingTO objissue :objissuetracking)
	   {
		   str[xx] = objissue.getCatid()+","+objissue.getCategory();
	   	   xx=xx+1;
	   }
	   return str;
   }
   
   
   public String getConcernDetails(String recordsReturned,String startIndex,String sort,String dir,String SearchKey,String searchStatuskey,String searchCategory,String searchPrioritykey) 
   {
   	
		int totalRecord = recordsReturned!=null?Integer.parseInt(recordsReturned):0;
		int startInd = startIndex!=null?Integer.parseInt(startIndex):0;
		logger.debug("Parameter for method getConcernDetails recordsReturned==>"+recordsReturned+"startIndex==>"+startIndex+"sort==>"+sort+"dir==>"+dir+"SearchKey"+SearchKey+"searchBysessionkey==>>"+searchStatuskey+"searchByclasskey==>>"+searchPrioritykey);
		ArrayList<ConcernTrackingTO> eduStudentTOArrayObj = ConcernTrackingDAO.getConcernDetails(totalRecord,startInd,sort,dir,"%"+SearchKey+"%","%"+searchStatuskey+"%","%"+searchCategory+"%","%"+searchPrioritykey+"%");
		String jsonStr ="";
	    JSONArray jsonArray = new JSONArray();
	    JSONObject jsonObj = new JSONObject();
	    
		try {
			     
			     logger.debug("Concern Data List  "+eduStudentTOArrayObj.size());
			  int totalreturnrow = 0;
		      for(Iterator<ConcernTrackingTO> iter = eduStudentTOArrayObj.iterator(); iter.hasNext();){
	   	      
		    	  ConcernTrackingTO  eduStudentTOObj = iter.next();
			      JSONObject jsonObj1 = new JSONObject();
			      jsonObj1.put("issue_id", eduStudentTOObj.getIssueid());
			      jsonObj1.put("issue_title", eduStudentTOObj.getIssuetitle());
				  jsonObj1.put("issue_submited_date", eduStudentTOObj.getIssuesubmiteddate());
				  jsonObj1.put("priority",eduStudentTOObj.getPriority());
				  jsonObj1.put("current_status",eduStudentTOObj.getStatus());
				  jsonObj1.put("category",eduStudentTOObj.getCategory());
				  // totalreturnrow = eduStudenttoojb.getTotalreturnrow();
			      jsonArray.put(jsonObj1);
			    
			}  
		          jsonObj.put("recordsReturned", ""+totalreturnrow);
				  jsonObj.put("totalRecords", ""+totalRecord);
				  jsonObj.put("startIndex", startIndex);
				  jsonObj.put("sort", sort);
				  jsonObj.put("dir",dir);
				  jsonObj.put("pageSize",recordsReturned);
			      jsonObj.put("records",jsonArray);
		       
	   }catch (JSONException exp) {
		// TODO Auto-generated catch block
		exp.printStackTrace();
		logger.debug("Method getConcernDetails :: ERROR "+exp.getMessage());
	    }
	   	jsonStr = jsonObj.toString();  
	   	logger.debug("Return a JSONObject from getConcernDetails method"+jsonStr);
		return jsonStr;
  }
   
   public ArrayList<ConcernTrackingTO> getConcernStatus(){
	   
	   return ConcernTrackingDAO.getConcernStatus();
   }
   
   public ArrayList<ConcernTrackingTO> getCategory()
   {
	   return ConcernTrackingDAO.getAllCategory();
	   
   }
 }
