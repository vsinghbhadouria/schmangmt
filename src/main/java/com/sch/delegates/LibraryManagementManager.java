 package com.sch.delegates;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sch.dao.LibraryManagementDAO;
import com.sch.form.EduStudentForm;
import com.sch.form.LibraryManagementForm;
import com.sch.to.EduStaffTO;
import com.sch.to.EduStudentTO;
import com.sch.to.LibraryManagementTO;

public class LibraryManagementManager 
{
    
	LibraryManagementDAO libraryManagementDAOObj; 	
	public static Logger logger = Logger.getLogger(LibraryManagementManager.class);
    public LibraryManagementManager() {

    	libraryManagementDAOObj = new LibraryManagementDAO();
    }
    
	public ArrayList<LibraryManagementTO> getBooktypes()
	{
    	return libraryManagementDAOObj.getBooktypes();
    }
	
	public ArrayList<String> getRackIds(int bookTypeId)
    {
		
      ArrayList<LibraryManagementTO> rackArray = libraryManagementDAOObj.getRackIds(bookTypeId);
	  ArrayList<String> rackDetailbybookType= new ArrayList<String>();
	  
	try {
		  for(LibraryManagementTO iter: rackArray){
			  rackDetailbybookType.add(iter.getRackid()+","+iter.getRackname());
			  System.out.println(iter.getRackid()+","+iter.getRackname());
		}
     }
      catch(Exception exp){exp.printStackTrace();
      logger.debug("Method getClassInfoByDepartment :: ERROR "+exp.getMessage());
      }
      
      return rackDetailbybookType;
    }
	
	public String genrateBookIds(String bookName,int numberOfCopy,int bookTypeId)
    {
		
      return  libraryManagementDAOObj.genrateBookIds(bookName,numberOfCopy,bookTypeId);

    }
	
    public LibraryManagementTO addLibraryBooks(LibraryManagementTO libraryManagementTO,LibraryManagementForm libraryManagementForm)
    {
    	return libraryManagementDAOObj.addLibraryBooks(libraryManagementTO,libraryManagementForm);
    
    }
	public ArrayList<String> getBookNames()
    {
		
      ArrayList<LibraryManagementTO> bookNames = libraryManagementDAOObj.getBookNames();
	  ArrayList<String> bookNameArray= new ArrayList<String>();
	  
	try {
		  for(LibraryManagementTO iter: bookNames){
			  bookNameArray.add(iter.getBookname());
			  System.out.println(iter.getBookname());
		}
     }
      catch(Exception exp){exp.printStackTrace();
      logger.debug("Method getClassInfoByDepartment :: ERROR "+exp.getMessage());
      }
      
      return bookNameArray;
    }
		
	public ArrayList<LibraryManagementTO> getAllbooks()
	{
    	return libraryManagementDAOObj.getAllbooks();
    }
	
	public ArrayList<String> getAvailableBookIds(String bookName)
    {
		ArrayList<LibraryManagementTO>bookidarray = libraryManagementDAOObj.getAvailableBookIds(bookName);
        ArrayList<String> bookIdsArray= new ArrayList<String>();
	  
  	try {
  		  for(LibraryManagementTO iter: bookidarray){
  			  bookIdsArray.add(iter.getBookcopyid()+","+iter.getBookid());
  		}
       }
        catch(Exception exp){exp.printStackTrace();
        logger.debug("Method getClassInfoByDepartment :: ERROR "+exp.getMessage());
        }
        
        return bookIdsArray;


    }
	
	public String getAvailableBookcountandtype(String bookName)
    {
		
		ArrayList<LibraryManagementTO>bookidarray = libraryManagementDAOObj.getAvailableBookcountandtype(bookName);
        String bookIdsArray= new String();
	  
  	try {
  		  for(LibraryManagementTO iter: bookidarray){
  			bookIdsArray=iter.getAvailablebookcount()+","+iter.getRackname()+","+iter.getBooktypename()+","+iter.getBookmasterid();
  		}
       }
        catch(Exception exp){exp.printStackTrace();
        logger.debug("Method getClassInfoByDepartment :: ERROR "+exp.getMessage());
        }
        
        return bookIdsArray;


    }
	
	public String getStudentDataByUserId(String userId)
    {
		
		ArrayList<EduStudentTO>bookidarray = libraryManagementDAOObj.getStudentDataByUserId(userId);
        String bookIdsArray= new String();
	  
  	try {
  		  for(EduStudentTO iter: bookidarray){
  			bookIdsArray=iter.getFirstname()+","+iter.getClassname()+","+iter.getEmailid()+","+"Active";
  			}
       }
        catch(Exception exp){exp.printStackTrace();
        logger.debug("Method getClassInfoByDepartment :: ERROR "+exp.getMessage());
        }
        
        return bookIdsArray;


    }
	
	public String getTeacherDataByUserId(String userId)
    {
		
		ArrayList<EduStaffTO>bookidarray = libraryManagementDAOObj.getTeacherDataByUserId(userId);
        String bookIdsArray= new String();
	  
  	try {
  		  for(EduStaffTO iter: bookidarray){
  			bookIdsArray=iter.getFirstname()+","+iter.getQualification_title()+","+iter.getEmailid()+","+iter.getPhoneno();
  			}
       }
        catch(Exception exp){exp.printStackTrace();
        logger.debug("Method getClassInfoByDepartment :: ERROR "+exp.getMessage());
        }
        
        return bookIdsArray;


    }
	
	
	public void assignBookToUser(LibraryManagementTO libraryManagementTO){
		
		libraryManagementDAOObj.assignBookToUser(libraryManagementTO);
	}
	
	public ArrayList<LibraryManagementTO> getAssignedbooks()
	{
    	return libraryManagementDAOObj.getAssignedbooks();
    }
	public ArrayList<LibraryManagementTO> getReturnedbooks()
	{
    	return libraryManagementDAOObj.getReturnedbooks();
    }
	public String getassignedBookByBookId(String bookId)
	{
		ArrayList<LibraryManagementTO> assignedBook = libraryManagementDAOObj.getassignedBookByBookId(bookId);
    	
        String bookInfo= new String();
	  
  	try {
  		  for(LibraryManagementTO iter: assignedBook){
  			bookInfo=iter.getUserName()+","+iter.getUserId()+","+iter.getEmailId()+","+iter.getContact_no()+","+iter.getStatus()+","+iter.getUserType()+","+iter.getBookname()+","+iter.getBooktypename()+","+iter.getBookid()+","+iter.getRackname()+","+iter.getIssueDate()+","+iter.getDueDate();
  			}
       }
        catch(Exception exp){exp.printStackTrace();
        logger.debug("Method getClassInfoByDepartment :: ERROR "+exp.getMessage());
        }
        
        return bookInfo;
    }
	
	public LibraryManagementTO returnBook(LibraryManagementTO libraryManagementTO){
		
		return libraryManagementDAOObj.returnBook(libraryManagementTO);
	}
	
}
