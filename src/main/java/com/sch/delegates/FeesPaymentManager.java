package com.sch.delegates;
import java.util.ArrayList;
import java.util.Iterator;
import com.sch.dao.*;
import com.sch.to.FeesPaymentTO;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
public class FeesPaymentManager
{
	FeesPaymentDAO FeesPaymentDAOObj=null;
	public static Logger logger = Logger.getLogger(EduStudentManager.class);
	public FeesPaymentManager()
	{
	  FeesPaymentDAOObj =new FeesPaymentDAO();
	}
	public ArrayList<FeesPaymentTO> getStudentList(String SearchKey, String searchBysessionkey)
	{
		return FeesPaymentDAOObj.getStudentList(SearchKey, searchBysessionkey);
		
	}
	public String getClassBySession(String admissionId,int selectedSessionId)
	{
		return FeesPaymentDAOObj.getClassBySession(admissionId,selectedSessionId);
	}
	public ArrayList<FeesPaymentTO> getMonthList()
	{
		return FeesPaymentDAOObj.getMonthList();
	}
	
	
	public void saveFeePayment(FeesPaymentTO feesPaymentTOObj){
		FeesPaymentDAOObj.saveFeePayment(feesPaymentTOObj);
	}
	
	public FeesPaymentTO getCollationDetailByUserId(String user_Id,String session_Id,String receipt_no)
	{
	  return FeesPaymentDAOObj.getCollationDetailByUserId(user_Id,session_Id,receipt_no);	
    }
	
	public ArrayList<FeesPaymentTO> getPayedMonthList(String user_Id,String session_Id,String receipt_no)
	{
		return FeesPaymentDAOObj.getPayedMonthList(user_Id,session_Id,receipt_no);
	}
	
	  public String getStudentFeeCollactionDetails(String recordsReturned,String startIndex,String sort,String dir,String SearchKey,String searchBysessionkey,String searchByclasskey) 
	    {
	    	
			int totalRecord = recordsReturned!=null?Integer.parseInt(recordsReturned):0;
			int startInd = startIndex!=null?Integer.parseInt(startIndex):0;
			logger.debug("Parameter for method getStudentDetails recordsReturned==>"+recordsReturned+"startIndex==>"+startIndex+"sort==>"+sort+"dir==>"+dir+"SearchKey"+SearchKey+"searchBysessionkey==>>"+searchBysessionkey+"searchByclasskey==>>"+searchByclasskey);
			ArrayList<FeesPaymentTO> eduStudentTOArrayObj = FeesPaymentDAOObj.getStudentFeeCollactionDetails(totalRecord,startInd,sort,dir,"%"+SearchKey+"%","%"+searchBysessionkey+"%","%"+searchByclasskey+"%");
			String jsonStr ="";
		    JSONArray jsonArray = new JSONArray();
		    JSONObject jsonObj = new JSONObject();
		    
			try {
				     
				  int totalreturnrow = 0;
			      for(Iterator<FeesPaymentTO> iter = eduStudentTOArrayObj.iterator(); iter.hasNext();){
		   	      
			    	  FeesPaymentTO  eduStudentTOObj = iter.next();
				      JSONObject jsonObj1 = new JSONObject();
				      jsonObj1.put("user_id", eduStudentTOObj.getStudentid());
				      jsonObj1.put("first_name", eduStudentTOObj.getFirstname()+" "+eduStudentTOObj.getLastname());
					  jsonObj1.put("Phone", eduStudentTOObj.getPhoneno());
					  jsonObj1.put("Email",eduStudentTOObj.getEmailId());
					  jsonObj1.put("Class",eduStudentTOObj.getClassname());
					  jsonObj1.put("Section",eduStudentTOObj.getSection());
					  jsonObj1.put("Session",eduStudentTOObj.getSessionyear());
					  jsonObj1.put("session_id",eduStudentTOObj.getSessionid());
					  jsonObj1.put("receipt_no",eduStudentTOObj.getReceptNo());
					  // totalreturnrow = eduStudentTOObj.getTotalreturnrow();
				      jsonArray.put(jsonObj1);
				    
				}  
			          jsonObj.put("recordsReturned", ""+totalreturnrow);
					  jsonObj.put("totalRecords", ""+totalRecord);
					  jsonObj.put("startIndex", startIndex);
					  jsonObj.put("sort", sort);
					  jsonObj.put("dir",dir);
					  jsonObj.put("pageSize",recordsReturned);
				      jsonObj.put("records",jsonArray);
			       
		   }catch (JSONException exp) {
			// TODO Auto-generated catch block
			exp.printStackTrace();
			logger.debug("Method getStudentDetails :: ERROR "+exp.getMessage());
		    }
		   	jsonStr = jsonObj.toString();  
		   	logger.debug("Return a JSONObject from getStudentDetails method");
			return jsonStr;
	   }
	  
		public FeesPaymentTO getEduRegistrationDatabyUserId(String user_Id,String session_Id,String reg_status)
		{
		  return FeesPaymentDAOObj.getEduRegistrationDatabyUserId(user_Id,session_Id,reg_status);	
	    }
		
		public void saveRegPayment(FeesPaymentTO FeesPaymentTOObj){
		  
		  FeesPaymentDAOObj.saveRegPayment(FeesPaymentTOObj);
		}

}
