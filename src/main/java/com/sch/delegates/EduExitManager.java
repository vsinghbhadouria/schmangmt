 package com.sch.delegates;
import com.sch.dao.EduExitDAO;
import com.sch.to.*;
public class EduExitManager 
{
    
	EduExitDAO EduExitDAOOBJ; 	
    public EduExitManager() {

    	EduExitDAOOBJ = new EduExitDAO();
    }
   
    public EduExitTO downloadCertificate(EduExitTO eduExitTOObj)
    {
    	return EduExitDAOOBJ.downloadCertificate(eduExitTOObj);
    
    }
   
}
