package com.sch.delegates;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import com.sch.dao.ConnectionUtil;
import com.sch.dao.TopNavigationDAO;
import com.sch.to.TopNavigationTO;
public class TopNavigationManager extends ConnectionUtil {
	
	TopNavigationDAO leftNavigationDAO = new TopNavigationDAO(); 
	private static Logger logger = Logger.getLogger(TopNavigationManager.class);
	ArrayList<TopNavigationTO> fullMenuList=null;
	public ArrayList<TopNavigationTO>  getAllMenu(Integer ug_id)
	{
		try
		{
			
		getConnection();
		logger.debug("Database connection open for Top Navigation from TopNavigationManager");
		fullMenuList = new ArrayList<TopNavigationTO>();
		ArrayList<TopNavigationTO> menuList = leftNavigationDAO.getMenusByParent(0,ug_id,conn); 
		for(TopNavigationTO menu: menuList){
			
			fullMenuList.add(menu);
			int func_ID = menu.getFuncID();
			ArrayList<TopNavigationTO> subMenuList = leftNavigationDAO.getMenusByParent(func_ID,ug_id,conn);
			for(TopNavigationTO submenu:subMenuList){
				logger.debug("ParentManu==>"+menu.funcTITLE+" ChildManu==>"+submenu.funcTITLE);
				fullMenuList.add(submenu);
			}
		}
	}
	catch(Exception exp)
	{
		logger.debug("Exception to getting  topNavigation==>"+exp.getMessage());
		exp.printStackTrace();
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	finally
	{
		try {
			logger.debug("Database connection closed after getting the top Navigation from TopNavigationManager");
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	return fullMenuList;
 }
}
