package com.sch.form;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.sch.to.TopNavigationTO;

public class TopNavigationForm extends ActionForm{

	private static final long serialVersionUID = 1L;

	ArrayList<TopNavigationTO> listfullMenu;

	public ArrayList<TopNavigationTO> getListfullMenu() {
		return listfullMenu;
	}

	public void setListfullMenu(ArrayList<TopNavigationTO> listfullMenu) {
		this.listfullMenu = listfullMenu;
	}
	
}
