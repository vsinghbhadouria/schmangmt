 package com.sch.form;
import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.sch.to.EduStudentReportTO;
public class EduStudentReportForm extends ActionForm 
{
	private static final long serialVersionUID = 1L;	
	protected Integer studentid;
	protected Integer parentid;
	protected String studentname;
	protected String phoneno;
	protected String fathername;
	protected Integer userid;
	protected String classname;
	protected Integer classid;
	protected String dateofbirth;
	protected String departmentname;
	protected Integer dept_id;
	protected String assignsubjected[];
	protected int subjectid;
	protected String subjectname;
	protected Integer sessionid;
	protected String session_year;
	protected Integer subject_count;
	protected String result;
	protected String isrecordExist;
	protected Float percentage;
	protected String division;
	
	protected String educlass;
	protected ArrayList<EduStudentReportTO> classes;
	protected String department;
	protected ArrayList<EduStudentReportTO> departments;
	protected Integer admissionfee;
	protected Integer concessionamt;
	protected String comment;

	
	public Integer getAdmissionfee() {
		return admissionfee;
	}
	public void setAdmissionfee(Integer admissionfee) {
		this.admissionfee = admissionfee;
	}
	public Integer getConcessionamt() {
		return concessionamt;
	}
	public void setConcessionamt(Integer concessionamt) {
		this.concessionamt = concessionamt;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getEduclass() {
		return educlass;
	}
	public void setEduclass(String educlass) {
		this.educlass = educlass;
	}
	public ArrayList<EduStudentReportTO> getClasses() {
		return classes;
	}
	public void setClasses(ArrayList<EduStudentReportTO> classes) {
		this.classes = classes;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public ArrayList<EduStudentReportTO> getDepartments() {
		return departments;
	}
	public void setDepartments(ArrayList<EduStudentReportTO> departments) {
		this.departments = departments;
	}
	public Float getPercentage() {
		return percentage;
	}
	public void setPercentage(Float percentage) {
		this.percentage = percentage;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getIsrecordExist() {
		return isrecordExist;
	}
	public void setIsrecordExist(String isrecordExist) {
		this.isrecordExist = isrecordExist;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}

	public Integer getSubject_count() {
		return subject_count;
	}
	public void setSubject_count(Integer subjectCount) {
		subject_count = subjectCount;
	}
	public String getSession_year() {
		return session_year;
	}
	public void setSession_year(String sessionYear) {
		session_year = sessionYear;
	}
	public Integer getSessionid() {
		return sessionid;
	}
	public void setSessionid(Integer sessionid) {
		this.sessionid = sessionid;
	}
	public int getSubjectid() {
		return subjectid;
	}
	public void setSubjectid(int subjectid) {
		this.subjectid = subjectid;
	}
	public String getSubjectname() {
		return subjectname;
	}
	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}
	public Integer getStudentid() {
		return studentid;
	}
	public void setStudentid(Integer studentid) {
		this.studentid = studentid;
	}
	public Integer getParentid() {
		return parentid;
	}
	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}
	public String getStudentname() {
		return studentname;
	}
	public void setStudentname(String studentname) {
		this.studentname = studentname;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public String getFathername() {
		return fathername;
	}
	public void setFathername(String fathername) {
		this.fathername = fathername;
	}
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	public String getClassname() {
		return classname;
	}
	public void setClassname(String classname) {
		this.classname = classname;
	}
	public Integer getClassid() {
		return classid;
	}
	public void setClassid(Integer classid) {
		this.classid = classid;
	}
	public String getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}
	public String getDepartmentname() {
		return departmentname;
	}
	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}
	public Integer getDept_id() {
		return dept_id;
	}
	public void setDept_id(Integer deptId) {
		dept_id = deptId;
	}
	public String[] getAssignsubjected() {
		return assignsubjected;
	}
	public void setAssignsubjected(String[] assignsubjected) {
		this.assignsubjected = assignsubjected;
	}
		
	
		
}
