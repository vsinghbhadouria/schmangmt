package com.sch.form;
import java.util.ArrayList;
import org.apache.struts.action.ActionForm;
import com.sch.to.LibraryManagementTO;
public class LibraryManagementForm extends ActionForm 
{
	
	private static final long serialVersionUID = 1L;
	protected String bookname;
	protected String booktypename;
	protected int booktypeid;
	protected String bookid;
	protected int bookmasterid;
	protected int rackid;
	protected String rackname;
	protected int numberofcopy;
	protected int availablebookcount;
	protected String userType=null;
	protected String userId=null;
	protected String username=null;
	protected String emailid=null;
	
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getAvailablebookcount() {
		return availablebookcount;
	}
	public void setAvailablebookcount(int availablebookcount) {
		this.availablebookcount = availablebookcount;
	}
	protected ArrayList<LibraryManagementTO> bookTypes;
	
	
	public ArrayList<LibraryManagementTO> getBookTypes() {
		return bookTypes;
	}
	public void setBookTypes(ArrayList<LibraryManagementTO> bookTypes) {
		this.bookTypes = bookTypes;
	}
	public String getBookname() {
		return bookname;
	}
	public void setBookname(String bookname) {
		this.bookname = bookname;
	}
	public String getBooktypename() {
		return booktypename;
	}
	public void setBooktypename(String booktypename) {
		this.booktypename = booktypename;
	}
	public int getBooktypeid() {
		return booktypeid;
	}
	public void setBooktypeid(int booktypeid) {
		this.booktypeid = booktypeid;
	}
	public String getBookid() {
		return bookid;
	}
	public void setBookid(String bookid) {
		this.bookid = bookid;
	}
	public int getBookmasterid() {
		return bookmasterid;
	}
	public void setBookmasterid(int bookmasterid) {
		this.bookmasterid = bookmasterid;
	}
	public int getRackid() {
		return rackid;
	}
	public void setRackid(int rackid) {
		this.rackid = rackid;
	}
	public String getRackname() {
		return rackname;
	}
	public void setRackname(String rackname) {
		this.rackname = rackname;
	}
	public int getNumberofcopy() {
		return numberofcopy;
	}
	public void setNumberofcopy(int numberofcopy) {
		this.numberofcopy = numberofcopy;
	}
	
}
