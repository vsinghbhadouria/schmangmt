package com.sch.form;
import org.apache.struts.action.ActionForm;

public class LoginForm extends ActionForm
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	protected String loginid =null;
	protected String password =null;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	protected String userid = null;
	protected String username = null;
	protected String newpassword =null;
	protected Integer userType;
	protected String confirmpassword = null;
	
	
	public String getConfirmpassword() {
		return confirmpassword;
	}
	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}
	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	public String getNewpassword()
	{
		return newpassword;
	}
	public void setNewpassword(String newpassword)
	{
		this.newpassword = newpassword;
	} 
	
}
