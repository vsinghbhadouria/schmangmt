 package com.sch.form;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;
import com.sch.to.EduStaffTO;
public class EduStaffForm extends ActionForm 
{
	private static final long serialVersionUID = 1L;
	public String getEmpid() {
		return empid;
	}
	public void setEmpid(String empid) {
		this.empid = empid;
	}
	public Integer getParentid() {
		return parentid;
	}
	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public String getSalary() {
		return salary;
	}
	public void setSalary(String salary) {
		this.salary = salary;
	}
	public String getHigheredu() {
		return higheredu;
	}
	public void setHigheredu(String higheredu) {
		this.higheredu = higheredu;
	}
	public String getPreviousexp() {
		return previousexp;
	}
	public void setPreviousexp(String previousexp) {
		this.previousexp = previousexp;
	}
	public String getJoiningdate() {
		return joiningdate;
	}
	public void setJoiningdate(String joiningdate) {
		this.joiningdate = joiningdate;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	

	public ArrayList<EduStaffTO> getLevels() {
		return levels;
	}
	public void setLevels(ArrayList<EduStaffTO> levels) {
		this.levels = levels;
	}


	public Integer getUserid() {
		return userid;
	}
	public void setUserid(int i) {
		this.userid = i;
	}


	public String[] getAssignclassed() {
		return assignclassed;
	}
	public void setAssignclassed(String[] assignclassed) {
		this.assignclassed = assignclassed;
	}

	public Integer getDesignation_id() {
		return designation_id;
	}
	public void setDesignation_id(Integer designation_id) {
		this.designation_id = designation_id;
	}
	
	public Integer getQualification_id() {
		return qualification_id;
	}
	public void setQualification_id(Integer qualification_id) {
		this.qualification_id = qualification_id;
	}

	protected Integer qualification_id;
	protected Integer designation_id;
	protected String address;
	protected String emailid;
	protected String empid;
	protected Integer parentid;
	protected String firstname;
	protected String middlename;
	protected String lastname;
	protected String phoneno;
	protected String salary;
	protected String higheredu;
	protected String previousexp;
	protected String joiningdate;
	protected String level;
	protected Integer userid;
	protected String qualification;
	protected ArrayList<EduStaffTO> levels;
	protected ArrayList<EduStaffTO> qualifications;
	protected String assignclassed[];
	protected String department;
	protected ArrayList<EduStaffTO> departments;
	protected Integer dept_id;
	protected String assignsubjected[];
	protected String password;
	protected String emailIds[];

	public String[] getEmailIds() {
			return emailIds;
		}



		public void setEmailIds(String[] emailIds) {
			this.emailIds = emailIds;
		}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String[] getAssignsubjected() {
		return assignsubjected;
	}
	public void setAssignsubjected(String[] assignsubjected) {
		this.assignsubjected = assignsubjected;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public ArrayList<EduStaffTO> getDepartments() {
		return departments;
	}
	public void setDepartments(ArrayList<EduStaffTO> departments) {
		this.departments = departments;
	}
	public Integer getDept_id() {
		return dept_id;
	}
	public void setDept_id(Integer dept_id) {
		this.dept_id = dept_id;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public ArrayList<EduStaffTO> getQualifications() {
		return qualifications;
	}
	public void setQualifications(ArrayList<EduStaffTO> qualifications) {
		this.qualifications = qualifications;
	}
	
}
