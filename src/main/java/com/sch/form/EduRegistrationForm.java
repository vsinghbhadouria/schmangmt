 package com.sch.form;
import java.util.ArrayList;

import org.apache.struts.action.ActionForm;
import com.sch.to.EduRegistrationTO;
public class EduRegistrationForm extends ActionForm 
{
	private static final long serialVersionUID = 1L;
	protected Integer qualification_id;
	public Integer getQualification_id() {
		return qualification_id;
	}
	public void setQualification_id(Integer qualification_id) {
		this.qualification_id = qualification_id;
	}

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public Integer getStudentid() {
		return studentid;
	}
	public void setStudentid(Integer studentid) {
		this.studentid = studentid;
	}
	public Integer getParentid() {
		return parentid;
	}
	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public String getFathername() {
		return fathername;
	}
	public void setFathername(String fathername) {
		this.fathername = fathername;
	}
	public String getMothername() {
		return mothername;
	}
	public void setMothername(String mothername) {
		this.mothername = mothername;
	}
	public String getParentemailid() {
		return parentemailid;
	}
	public void setParentemailid(String parentemailid) {
		this.parentemailid = parentemailid;
	}
	public String getPreviousinstitute() {
		return previousinstitute;
	}
	public void setPreviousinstitute(String previousinstitute) {
		this.previousinstitute = previousinstitute;
	}
	public String getParentqualification() {
		return parentqualification;
	}
	public void setParentqualification(String parentqualification) {
		this.parentqualification = parentqualification;
	}
	public ArrayList<EduRegistrationTO> getParentqualifications() {
		return parentqualifications;
	}
	public void setParentqualifications(ArrayList<EduRegistrationTO> parentqualifications) {
		this.parentqualifications = parentqualifications;
	}
	public Integer getParentqualification_id() {
		return parentqualification_id;
	}
	public void setParentqualification_id(Integer parentqualification_id) {
		this.parentqualification_id = parentqualification_id;
	}
	public String getStudprequalification() {
		return studprequalification;
	}
	public void setStudprequalification(String studprequalification) {
		this.studprequalification = studprequalification;
	}
	public ArrayList<EduRegistrationTO> getStudprequalifications() {
		return studprequalifications;
	}
	public void setStudprequalifications(
			ArrayList<EduRegistrationTO> studprequalifications) {
		this.studprequalifications = studprequalifications;
	}
	public Integer getStudprequalification_id() {
		return studprequalification_id;
	}
	public void setStudprequalification_id(Integer studprequalification_id) {
		this.studprequalification_id = studprequalification_id;
	}
	public String getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}

	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getEduclass() {
		return educlass;
	}
	public void setEduclass(String educlass) {
		this.educlass = educlass;
	}
	public ArrayList<EduRegistrationTO> getClasses() {
		return classes;
	}
	public void setClasses(ArrayList<EduRegistrationTO> classes) {
		this.classes = classes;
	}

	public Integer getAdmissionfee() {
		return admissionfee;
	}
	public void setAdmissionfee(Integer admissionfee) {
		this.admissionfee = admissionfee;
	}
	public ArrayList<EduRegistrationTO> getAdmissionfees() {
		return admissionfees;
	}
	public void setAdmissionfees(ArrayList<EduRegistrationTO> admissionfees) {
		this.admissionfees = admissionfees;
	}
	public Integer getAdmissionfee_id() {
		return admissionfee_id;
	}
	public void setAdmissionfee_id(Integer admissionfee_id) {
		this.admissionfee_id = admissionfee_id;
	}

	public Integer getClassid() {
		return classid;
	}
	public void setClassid(Integer classid) {
		this.classid = classid;
	}

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getConcessionamt() {
		return concessionamt;
	}
	public void setConcessionamt(Integer concessionamt) {
		this.concessionamt = concessionamt;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	

	public String getRegistrationdate() {
		return registrationdate;
	}
	public void setRegistrationdate(String registrationdate) {
		this.registrationdate = registrationdate;
	}


	protected Integer admissionfee;
	protected ArrayList<EduRegistrationTO> admissionfees;
	protected Integer admissionfee_id;
	protected String address;
	protected String emailid;
	protected Integer studentid;
	protected Integer parentid;
	protected String firstname;
	protected String middlename;
	protected String lastname;
	protected String phoneno;
	protected String fathername;
	protected String mothername;
	protected String parentemailid;
	protected String previousinstitute;
	protected String parentqualification;
	protected ArrayList<EduRegistrationTO> parentqualifications;
	protected Integer parentqualification_id;
	protected String studprequalification;
	protected ArrayList<EduRegistrationTO> studprequalifications;
	protected Integer studprequalification_id;
	protected String dateofbirth;
	protected String registrationdate;
	protected Integer userid;
	protected String educlass;
	protected ArrayList<EduRegistrationTO> classes;
	protected Integer classid;
	protected String gender;
	protected Integer concessionamt;
	protected String comment;
	protected Integer registrationid;
	protected int birthyear;
	protected int birthmonth;
	protected int birthday;
	protected int registrationyear;
	protected int registrationmonth;
	protected int registrationday;
	protected String department;
	protected ArrayList<EduRegistrationTO> departments;
	protected Integer dept_id;
	protected String assignsubjected[];
	protected String password;
	protected String parentpassword;
	protected String studentUser;
	protected String parentUser;
	protected String Parentpassword;
	protected int sessionId;
	protected Integer registrationfee;
	protected String regstatus;
	
	public String getRegstatus() {
		return regstatus;
	}
	public void setRegstatus(String regstatus) {
		this.regstatus = regstatus;
	}
	public Integer getRegistrationfee() {
		return registrationfee;
	}
	public void setRegistrationfee(Integer registrationfee) {
		this.registrationfee = registrationfee;
	}
	public String getStudentUser() {
		return studentUser;
	}
	public void setStudentUser(String studentUser) {
		this.studentUser = studentUser;
	}
	public String getParentUser() {
		return parentUser;
	}
	public void setParentUser(String parentUser) {
		this.parentUser = parentUser;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getParentpassword() {
		return parentpassword;
	}
	public void setParentpassword(String parentpassword) {
		this.parentpassword = parentpassword;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public ArrayList<EduRegistrationTO> getDepartments() {
		return departments;
	}
	public void setDepartments(ArrayList<EduRegistrationTO> departments) {
		this.departments = departments;
	}
	public Integer getDept_id() {
		return dept_id;
	}
	public void setDept_id(Integer dept_id) {
		this.dept_id = dept_id;
	}
	public String[] getAssignsubjected() {
		return assignsubjected;
	}
	public void setAssignsubjected(String[] assignsubjected) {
		this.assignsubjected = assignsubjected;
	}
	public int getRegistrationyear() {
		return registrationyear;
	}
	public void setRegistrationyear(int registrationyear) {
		this.registrationyear = registrationyear;
	}
	public int getRegistrationmonth() {
		return registrationmonth;
	}
	public void setRegistrationmonth(int registrationmonth) {
		this.registrationmonth = registrationmonth;
	}
	public int getRegistrationday() {
		return registrationday;
	}
	public void setRegistrationday(int registrationday) {
		this.registrationday = registrationday;
	}
	public int getBirthyear() {
		return birthyear;
	}
	public void setBirthyear(int birthyear) {
		this.birthyear = birthyear;
	}
	public int getBirthmonth() {
		return birthmonth;
	}
	public void setBirthmonth(int birthmonth) {
		this.birthmonth = birthmonth;
	}
	public int getBirthday() {
		return birthday;
	}
	public void setBirthday(int birthday) {
		this.birthday = birthday;
	}

	public Integer getRegistrationid() {
		return registrationid;
	}
	public void setRegistrationid(Integer registrationid) {
		this.registrationid = registrationid;
	}
	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	
	
	
}
