package com.sch.form;

import org.apache.struts.action.ActionForm;

public class ConcernTrackingForm extends ActionForm 

{
	protected Integer issueid;
	protected String  username;
	protected String  desgnation;
	protected String  issuetitle;
	protected String  issuedesc;
	protected String  category;
	protected String  status;
	protected String  priority;
	protected String  userid;
	protected String  ansdisc;
	protected String  issuesubmiteddate;
	protected Integer  replyid;
	protected String  replydesc;
	protected Integer  stausid;
	
	public Integer getStausid() {
		return stausid;
	}
	public void setStausid(Integer stausid) {
		this.stausid = stausid;
	}
	public String getIssuedesc() {
		return issuedesc;
	}
	public void setIssuedesc(String issuedesc) {
		this.issuedesc = issuedesc;
	}
	public Integer getReplyid() {
		return replyid;
	}
	public void setReplyid(Integer replyid) {
		this.replyid = replyid;
	}
	public String getIssuesubmiteddate() {
		return issuesubmiteddate;
	}
	public void setIssuesubmiteddate(String issuesubmiteddate) {
		this.issuesubmiteddate = issuesubmiteddate;
	}
	public String getIssuerepydate() {
		return issuerepydate;
	}
	public void setIssuerepydate(String issuerepydate) {
		this.issuerepydate = issuerepydate;
	}
	protected String  issuerepydate;
	public String getAnsdisc() {
		return ansdisc;
	}
	public void setAnsdisc(String ansdisc) {
		this.ansdisc = ansdisc;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public Integer getIssueid() {
		return issueid;
	}
	public void setIssueid(Integer issueid) {
		this.issueid = issueid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDesgnation() {
		return desgnation;
	}
	public void setDesgnation(String desgnation) {
		this.desgnation = desgnation;
	}
	public String getIssuetitle() {
		return issuetitle;
	}
	public void setIssuetitle(String issuetitle) {
		this.issuetitle = issuetitle;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getReplydesc() {
		return replydesc;
	}
	public void setReplydesc(String replydesc) {
		this.replydesc = replydesc;
	}

}
