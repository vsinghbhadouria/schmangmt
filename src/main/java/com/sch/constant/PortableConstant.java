package com.sch.constant;
import java.io.InputStream;
import java.util.Properties;
public class PortableConstant {
	public  final static String SCHOOLNAME="schoolName";
	public  final static String PROPNAME="portableMg.properties";
	public  final static String SCHOOLLOCATION="schoolLocation";
	public  final static String CERTIFICATENAME="certificateName";
	public  final static String FOLDERNAME="XC.FolderName";
	public  final static String DATEPICKERSTARTYEAR="datepickerstartyear";
	public  final static String DATEPICKERCURRENTYEAR="datepickercurrentyear";
	public  final static String PARTOFEMPID="partofempid";
	public  final static String PARTOFSTUDENTID="partofstudentid";
	public  final static String PARTOFPARENTID="partofparentid";
	public static final String SMTP_HOST_NAME = "smtp.gmail.com";
	public static final String SMTP_PORT = "465";
	public static final String EMAILMSGTXT = "emailMsgTxt";
	public static final String EMAILSUBJECTTXT = "emailSubjectTxt";
	public static final String EMAILFROMADDRESS = "emailFromAddress";
	public static final String EMAILFROMPASSWORD = "emailFromPassword";
	public static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
	public static final String[] sendTo = {"knp.vinod@gmail.com","vinodbhadouria@yahoo.com"};
	public static final boolean debug = true;
	public static final String REGARDS="regards";
	public static final String NAMESCHOOL="nameSchool";
	public static final String MEETINGSUBJECTTXT = "meetingSubject";
	public static final String EMAILBOOKISSUESUBJECTTXT = "emailBookIssueSubjectTxt";
	public static final String LAIBRARYSERVICES="laibraryServices";
	public static final String EMAILBOOKRETURNSUBJECTTXT="emailBookReturnSubjectTxt";
	public static final String maximumStudentHeadCountAllowed="maximumStudentHeadCountAllowed";
	public static final String STUDENTMAXMSG="studentMaxMsg";
	public static Properties prop=null;
	public void loadProperty()
	{
		 try
		 {
			 if(prop==null){
			 prop = new Properties();
			 InputStream inputStream = 
					    getClass().getClassLoader().getResourceAsStream(PROPNAME);
		  	 prop.load(inputStream);
		  	 //prop.load(new FileInputStream("c:\\properties"+"\\"+PROPNAME));
			 }
		 }
		 catch(Exception exp)
		 {
			 exp.printStackTrace();
		 }
		
	}	
	public  String getProperty(String propKey)
	{
		 String value=prop.getProperty(propKey);
		 return value;
		 
	}

}
