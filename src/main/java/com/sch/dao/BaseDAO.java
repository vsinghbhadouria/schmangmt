package com.sch.dao;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sch.exceptions.DAOException;

public class BaseDAO{
	
	// Logging object and utility method
	private static Log log = LogFactory.getLog(com.sch.dao.BaseDAO.class);
	
	protected static Connection connection = null;

	
	public static Connection getConnection() throws DAOException 
	{
		InitialContext ic;
		try {
			ic = new InitialContext();
			DataSource myDS = (DataSource)ic.lookup("java:comp/env/jdbc/ace");   
			connection = myDS.getConnection();
			//log.info("BASE DAO created connection");
		} 
		catch (NamingException e) {
			throw new DAOException(e);
		}
		catch (SQLException sqe) {
			throw new DAOException(sqe);			
		}
		return connection;
	}
}
