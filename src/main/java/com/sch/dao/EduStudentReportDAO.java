package com.sch.dao;
import java.sql.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import com.sch.form.EduStudentReportForm;
import com.sch.to.*;


public class EduStudentReportDAO extends ConnectionUtil

{
	Query query =null;
    ResultSet rsObj = null; 
    PreparedStatement preStat=null;	
    CallableStatement callStm = null;
	ArrayList<EduStudentReportTO> arraylistobj = new ArrayList<EduStudentReportTO>();
    Logger logger  = Logger.getLogger(EduStudentReportDAO.class);
	public EduStudentReportDAO()
	{
		 query = new Query();  
	}
	
public EduStudentReportTO getEduStudentDatabyUserId(String user_Id,String session_Id)
{
	EduStudentReportTO EduStudentReportTOObj = new EduStudentReportTO();
	try
	{    
		getConnection();
		logger.debug("Method getEduStudentDatabyUserId :: Database Connection opened");
		callStm = conn.prepareCall("{call get_studentDetailByUserSession_sp(?,?)}");
		callStm.setInt(1,Integer.valueOf(user_Id));
		callStm.setString(2,session_Id);
		logger.debug("Method getEduStudentDatabyUserId :: Procedure Executed==>"+callStm);
		rsObj = callStm.executeQuery();
		if(rsObj.next())
		{   
			EduStudentReportTOObj.setStudentid(rsObj.getInt("student_id"));			
			EduStudentReportTOObj.setClassid(rsObj.getInt("class_id"));
			EduStudentReportTOObj.setFathername(rsObj.getString("father_name"));
			EduStudentReportTOObj.setDateofbirth(rsObj.getString("birth_day")+"/"+(Integer.parseInt(rsObj.getString("birth_month"))+1)+"/"+rsObj.getString("birth_year"));
			EduStudentReportTOObj.setClassname(rsObj.getString("class_name"));
			EduStudentReportTOObj.setStudentname(rsObj.getString("first_name")+" "+rsObj.getString("middle_name")+" "+rsObj.getString("last_name"));
			EduStudentReportTOObj.setUserid(rsObj.getString("user_id"));
			EduStudentReportTOObj.setClassid(rsObj.getInt("class_id"));
			EduStudentReportTOObj.setSessionid(rsObj.getInt("session_id"));
			EduStudentReportTOObj.setDept_id(rsObj.getInt("dept_id"));
			EduStudentReportTOObj.setSession_year(rsObj.getString("session_year"));
			EduStudentReportTOObj.setAdmissiondate(rsObj.getString("admission_date"));
			EduStudentReportTOObj.setPhoneno(rsObj.getString("contact_no"));
			EduStudentReportTOObj.setEmailid(rsObj.getString("email_id"));
			EduStudentReportTOObj.setAddress(rsObj.getString("address"));
			EduStudentReportTOObj.setDepartmentname(rsObj.getString("dept_name"));
			
		}
		
	}
	catch(Exception exp){exp.printStackTrace();
	logger.debug("Method getEduStudentDatabyUserId :: ERROR==>"+exp.getMessage());
	}
	finally{
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getEduStudentDatabyUserId :: Database Connection closed");
			}catch(Exception exp){exp.printStackTrace();}}
	return EduStudentReportTOObj;
}
	 public ArrayList<EduStudentReportTO> getSelectedSubject(int user_Id,int classid,String session_Id) {

			ArrayList<EduStudentReportTO> users = new ArrayList<EduStudentReportTO>();
			try{
				getConnection();
				logger.debug("Method getSelectedSubject :: Database Connection closed");
				callStm = conn.prepareCall("{call get_AssignedSubjectForStudentbysessionId(?,?,?)}");
				callStm.setInt(1,user_Id);
				callStm.setInt(2,classid);
				callStm.setString(3,session_Id);
				logger.debug("Method getSelectedSubject :: Procedure Executed==>"+callStm);
				rsObj = callStm.executeQuery();
				while(rsObj.next())
				{
					EduStudentReportTO EduStudentReportTOObj = new EduStudentReportTO();
					EduStudentReportTOObj.setSubjectid(rsObj.getInt("subject_id"));
					EduStudentReportTOObj.setSubjectname(rsObj.getString("subject_name"));
					users.add(EduStudentReportTOObj);
				}
			}catch (Exception exp) {
				exp.printStackTrace();
				logger.debug("Method getSelectedSubject :: ERROR==>"+exp.getMessage());
			}
			finally{
				try {
					closeConnection(conn,callStm,preStat,rsObj);
					logger.debug("Method getSelectedSubject :: Database Connection closed");
					} 
			catch (Exception exp) {exp.printStackTrace();}}
			return users;
		}
	
	 public  void saveEduStudentReportData(ArrayList<EduStudentReportTO> markObj,EduStudentReportTO eduStudentReportTOObj)
		{

			try
			{	
				getConnection();
				logger.debug("Method saveEduStudentReportData :: Database Connection opened");
				conn.setAutoCommit(false);				
				for(int xx=0;xx<markObj.size();xx++){
					preStat = conn.prepareStatement("insert into sch_student_mark_detail values(?,?,?,?,?,?,?)");
					preStat.setInt(1,eduStudentReportTOObj.getStudentid());
					preStat.setInt(2,markObj.get(xx).getSubjectid());
					preStat.setInt(3,eduStudentReportTOObj.getClassid());
					preStat.setInt(4,eduStudentReportTOObj.getSessionid());
					preStat.setInt(5,eduStudentReportTOObj.getDept_id());
					preStat.setInt(6,markObj.get(xx).getOutOf());
					preStat.setFloat(7,markObj.get(xx).getMarkObtained());
					
					preStat.executeUpdate();
				}
				preStat = conn.prepareStatement("insert into sch_student_result_detail values(?,?,?,?,?,?,?,?)");
				preStat.setInt(1,eduStudentReportTOObj.getStudentid());
				preStat.setInt(2,eduStudentReportTOObj.getClassid());
				preStat.setInt(3,eduStudentReportTOObj.getSessionid());
				preStat.setString(4,eduStudentReportTOObj.getResult());
				preStat.setInt(5,eduStudentReportTOObj.getTotalOutOf());
				preStat.setFloat(6,eduStudentReportTOObj.getTotalMarkObtained());
				preStat.setString(7,eduStudentReportTOObj.getDivision());
				preStat.setFloat(8,eduStudentReportTOObj.getPercentage());
				preStat.executeUpdate();
				logger.debug("Method saveEduStudentReportData :: Query Executed==>");
				
			    conn.commit();
			 
			}
			catch(Exception exp)
			{
				exp.printStackTrace();
				logger.debug("Method saveEduStudentReportData :: ERROR==>"+exp.getMessage());
				try {
					conn.rollback();
					logger.debug("Method saveEduStudentReportData :: Transaction rollbacked");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			finally {
				try{
					closeConnection(conn,callStm,preStat,rsObj);
					logger.debug("Method saveEduStudentReportData :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}
			
		}

public boolean isRecordExist(int student_id,int class_id,int session_id,String reqCome){
	System.out.println("student_id>> "+student_id+"class_id>> "+class_id+"session_id>> "+session_id);
	boolean insertRecord=false;
	try
	{	
	getConnection();
	if(reqCome.equals("prepareReportCard")){
	preStat = conn.prepareStatement("select count(*) rcount from sch_student_result_detail where student_id=? and class_id=? and session_id=?");
	preStat.setInt(1,student_id);
	preStat.setInt(2,class_id);
	preStat.setInt(3,session_id);
	}
	else if(reqCome.equals("studNewSessionAdmission")){
	preStat = conn.prepareStatement("select count(*) rcount from sch_student_class_detail where student_id=? and session_id=?");
	preStat.setInt(1,student_id);
	preStat.setInt(2,session_id);
	}

	rsObj=preStat.executeQuery();
	if(rsObj.next() && rsObj.getInt("rcount")>=1){
		insertRecord=true;
	}
	logger.debug("Method isRecordExist :: ERROR==>"+preStat+"    "+rsObj.wasNull()+" insertRecord  "+insertRecord);
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		logger.debug("Method isRecordExist :: ERROR==>"+exp.getMessage());
		try {
			conn.rollback();
			logger.debug("Method isRecordExist :: Transaction rollbacked");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method isRecordExist :: Database Connection closed");
	}catch(Exception exp){
		exp.printStackTrace();
	}}
   return insertRecord;
}

public  ArrayList<EduStudentReportTO> getResultData(String SearchKey,String searchBysessionkey,String searchByclasskey) 
{
   	ArrayList<EduStudentReportTO> eduStudentReportArrayObj = new ArrayList<EduStudentReportTO>();
    try { 
    	getConnection();
    	logger.debug("Method getResultData :: Database Connection opened");
    	callStm = conn.prepareCall("{call sp_student_report_list(?,?,?)}");	
    	callStm.setString(1,SearchKey);
    	callStm.setString(2,searchBysessionkey);
    	callStm.setString(3,searchByclasskey);
    	callStm.execute();
    	rsObj = callStm.getResultSet();
    	logger.debug("Method getResultData :: Procedure Executed==>"+callStm);
		while(rsObj.next())	
    	{
			// ssrd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssrd.status,ssrd.division,scd.class_name,sss.,sdt.
			 
			EduStudentReportTO eduStudentReportTOobj = new EduStudentReportTO();
			eduStudentReportTOobj.setStudentid(rsObj.getInt("student_id"));
			eduStudentReportTOobj.setStudentname(rsObj.getString("first_name")+" "+rsObj.getString("middle_name")+" "+rsObj.getString("last_name"));
			eduStudentReportTOobj.setClassname(rsObj.getString("class_name"));
			eduStudentReportTOobj.setSession_year(rsObj.getString("session_year"));
			eduStudentReportTOobj.setDepartmentname(rsObj.getString("dept_name"));
			eduStudentReportTOobj.setDivision(rsObj.getString("division"));
			eduStudentReportTOobj.setClassname(rsObj.getString("class_name"));
			eduStudentReportTOobj.setResult(rsObj.getString("status"));
			eduStudentReportTOobj.setClassid(rsObj.getInt("class_id"));
			eduStudentReportTOobj.setSessionid(rsObj.getInt("session_id"));
			eduStudentReportTOobj.setDept_id(rsObj.getInt("dept_id"));
			eduStudentReportArrayObj.add(eduStudentReportTOobj);
    	 	
    	}
		  
	}
    catch (Exception exception)
    {
    	exception.printStackTrace();
    	logger.debug("ERROR:: "+exception.getMessage());
    }
    finally 
    {
    	try{	
    		closeConnection(conn,callStm,preStat,rsObj);
    		logger.debug("Method getStudentDetails :: Database Connection closed "+conn);
    	} catch (SQLException e) 
    	{
    		e.printStackTrace();
    	}
    }
	return eduStudentReportArrayObj;
}

public ArrayList<EduStudentReportTO> getSubjectMarkByStudent(String strToken){
	 
	StringTokenizer st = new StringTokenizer(strToken,",");
	int tokenSize = st.countTokens();
	System.out.println(" tokenSize "+tokenSize);
	String student_id = null; 
	String class_id = null; 
	String dept_id = null; 
	String session_id = null; 
	ArrayList<EduStudentReportTO> eduStudentReportArrayObj = new ArrayList<EduStudentReportTO>();
	
    try { 
    	getConnection();
    	logger.debug("Method getSubjectMarkByStudent :: Database Connection opened");
    	while(st.hasMoreTokens()){
		StringTokenizer strow = new StringTokenizer(st.nextToken(),":");
		strow.nextToken();
		student_id = strow.nextToken();
		class_id = strow.nextToken();
		session_id = strow.nextToken();
		dept_id =	strow.nextToken();	
		System.out.println("student_id "+student_id+" class_id "+class_id+" session_id "+session_id+" dept_id "+dept_id);
		
    	callStm = conn.prepareCall("{call sp_student_report_details(?,?,?,?)}");	
    	callStm.setString(1,student_id);
    	callStm.setString(2,class_id);
    	callStm.setString(3,session_id);
    	callStm.setString(4,dept_id);
    	callStm.execute();
    	rsObj = callStm.getResultSet();
    	logger.debug("Method getSubjectMarkByStudent :: Procedure Executed==>"+callStm);
    	EduStudentReportTO eduStudentReportTOobj = new EduStudentReportTO();
    	if(rsObj.next()){
    		eduStudentReportTOobj.setStudentid(rsObj.getInt("student_id"));
			eduStudentReportTOobj.setStudentname(rsObj.getString("first_name")+" "+rsObj.getString("middle_name")+" "+rsObj.getString("last_name"));
			eduStudentReportTOobj.setFathername(rsObj.getString("father_name"));
			eduStudentReportTOobj.setDateofbirth(rsObj.getString("birth_day")+"/"+(Integer.parseInt(rsObj.getString("birth_month"))+1)+"/"+rsObj.getString("birth_year"));
			eduStudentReportTOobj.setClassname(rsObj.getString("class_name"));
			eduStudentReportTOobj.setSession_year(rsObj.getString("session_year"));
			eduStudentReportTOobj.setDepartmentname(rsObj.getString("dept_name"));
			eduStudentReportTOobj.setDivision(rsObj.getString("division"));
			eduStudentReportTOobj.setClassname(rsObj.getString("class_name"));
			eduStudentReportTOobj.setResult(rsObj.getString("status"));
			eduStudentReportTOobj.setClassid(rsObj.getInt("class_id"));
			eduStudentReportTOobj.setSessionid(rsObj.getInt("session_id"));
			eduStudentReportTOobj.setDept_id(rsObj.getInt("dept_id"));
			eduStudentReportTOobj.setPercentage(rsObj.getFloat("percentage"));
			eduStudentReportTOobj.setTotalMarkObtained(rsObj.getFloat("total_mark_obtained"));
			eduStudentReportTOobj.setTotalOutOf(rsObj.getInt("total_mark_outof"));
			
			
    	}
    	
    	callStm = conn.prepareCall("{call sp_student_sub_mark_reports(?,?,?,?)}",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);	
    	callStm.setString(1,student_id);
    	callStm.setString(2,class_id);
    	callStm.setString(3,session_id);
    	callStm.setString(4,dept_id);
    	callStm.execute();
    	rsObj = callStm.getResultSet();
    	logger.debug("Method getSubjectMarkByStudent :: Procedure Executed==>"+callStm);
    	int recount=0;
    	rsObj.last();
    	int rows = rsObj.getRow();
    	System.out.println("total rows========"+rows);
    	String []subject_name = new String[rows];
    	String []markOutOf = new String[rows];
		String []markObtain = new String[rows];
		String []subjectCode = new String[rows];
		rsObj.beforeFirst();
    	while(rsObj.next()){
    		
    		subject_name[recount]=rsObj.getString("subject_name");
    		eduStudentReportTOobj.setSubject(subject_name);
    		markOutOf[recount]=rsObj.getString("mark_outof");
    		eduStudentReportTOobj.setMarkOutOf(markOutOf);
    		markObtain[recount]=rsObj.getString("mark_obtained");
    		eduStudentReportTOobj.setMarkObtain(markObtain);
    		subjectCode[recount]=rsObj.getString("subject_code");
    		eduStudentReportTOobj.setSubjectcode(subjectCode);
    		recount++;
    	}
    	eduStudentReportArrayObj.add(eduStudentReportTOobj);
  
	}

		  
	}
    catch (Exception exception)
    {
    	exception.printStackTrace();
    	logger.debug("ERROR:: "+exception.getMessage());
    }
    finally 
    {
    	try{	
    		closeConnection(conn,callStm,preStat,rsObj);
    		logger.debug("Method getStudentDetails :: Database Connection closed "+conn);
    	} catch (SQLException e) 
    	{
    		e.printStackTrace();
    	}
    }
	return eduStudentReportArrayObj; 
}

public ArrayList<EduStudentReportTO> getSubjectClassByStudent(String student_id){
	 
	ArrayList<EduStudentReportTO> eduStudentReportArrayObj = new ArrayList<EduStudentReportTO>();
	
    try { 
    	getConnection();
    	logger.debug("Method getSubjectClassByStudent :: Database Connection opened");
    	callStm = conn.prepareCall("{call sp_student_session_class_details(?)}");	
    	callStm.setString(1,student_id);
    	callStm.execute();
    	rsObj = callStm.getResultSet();
    	logger.debug("Method getSubjectClassByStudent :: Procedure Executed==>"+callStm);
    	
    	while(rsObj.next()){
    		
    		EduStudentReportTO eduStudentReportTOobj = new EduStudentReportTO();
    		eduStudentReportTOobj.setStudentid(rsObj.getInt("student_id"));
			eduStudentReportTOobj.setClassname(rsObj.getString("class_name"));
			eduStudentReportTOobj.setSession_year(rsObj.getString("session_year"));
			eduStudentReportTOobj.setDepartmentname(rsObj.getString("dept_name"));
			eduStudentReportTOobj.setDivision(rsObj.getString("division")!=null?rsObj.getString("division"):"NA");
			eduStudentReportTOobj.setClassid(rsObj.getInt("class_id"));
			eduStudentReportTOobj.setSessionid(rsObj.getInt("session_id"));
			eduStudentReportTOobj.setDept_id(rsObj.getInt("dept_id"));
			eduStudentReportTOobj.setPercentage(rsObj.getFloat("percentage"));
			eduStudentReportTOobj.setTotalMarkObtained(rsObj.getFloat("total_mark_obtained"));
			eduStudentReportTOobj.setTotalOutOf(rsObj.getInt("total_mark_outof"));
			
			callStm = conn.prepareCall("{call sp_student_subject_class_details(?,?,?)}",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);	
	    	callStm.setInt(1,rsObj.getInt("student_id"));
	    	callStm.setInt(2,rsObj.getInt("class_id"));
	    	callStm.setInt(3,rsObj.getInt("session_id"));
	    	callStm.execute();
	    	ResultSet rssubObj = callStm.getResultSet();
	    	int recount=0;
    		rssubObj.last();
    		int rows = rssubObj.getRow();
    		System.out.println("total rows========"+rows);
    		String []subject_name = new String[rows];
    		rssubObj.beforeFirst();
	    	while(rssubObj.next()){
	    		subject_name[recount]=rssubObj.getString("subject_name");
	    		eduStudentReportTOobj.setSubject(subject_name);
	    		recount++;
	    	}
			eduStudentReportArrayObj.add(eduStudentReportTOobj);
    	}

    }
    catch (Exception exception)
    {
    	exception.printStackTrace();
    	logger.debug("ERROR:: "+exception.getMessage());
    }
    finally 
    {
    	try{	
    		closeConnection(conn,callStm,preStat,rsObj);
    		logger.debug("Method getSubjectClassByStudent :: Database Connection closed "+conn);
    	} catch (SQLException e) 
    	{
    		e.printStackTrace();
    	}
    }
	return eduStudentReportArrayObj; 
}

public  ArrayList<EduStudentReportTO> getStudentProfileList(int totalRecord,int startIndex,String sort,String dir,String SearchKey) 
{
   	ArrayList<EduStudentReportTO> studentreportObj = new ArrayList<EduStudentReportTO>();
    try { 
    	getConnection();
    	logger.debug("Method getStudentDetails :: Database Connection opened");
    	callStm = conn.prepareCall("{call sp_getStudentProfileList(?,?,?,?,?)}");	
    	callStm.setInt(1,totalRecord);
    	callStm.setInt(2,startIndex);
    	callStm.setString(3,sort);
    	callStm.setString(4,dir);
    	callStm.setString(5,SearchKey);
    	callStm.execute();
    	rsObj = callStm.getResultSet();
    	logger.debug("Method getStudentDetails :: Procedure Executed==>"+callStm);
		while(rsObj.next())	
    	{
			
			EduStudentReportTO studentreportTOtemp = new EduStudentReportTO();
			studentreportTOtemp.setStudentid(rsObj.getInt("student_id"));
			studentreportTOtemp.setUserid(rsObj.getString("user_id"));
			studentreportTOtemp.setStudentname(rsObj.getString("first_name")+" "+rsObj.getString("last_name"));
    		studentreportTOtemp.setPhoneno(rsObj.getString("contact_no"));
    	 	studentreportTOtemp.setEmailid(rsObj.getString("email_id"));
    	 	studentreportTOtemp.setAdmissiondate(rsObj.getString("admission_date"));
    	 	studentreportTOtemp.setDateofbirth(rsObj.getString("birth_day")+"/"+(Integer.parseInt(rsObj.getString("birth_month"))+1)+"/"+rsObj.getString("birth_year"));
    	 	studentreportObj.add(studentreportTOtemp);
    	 	
    	}
		  
	}
    catch (Exception exception)
    {
    	exception.printStackTrace();
    	logger.debug("ERROR:: "+exception.getMessage());
    }
    finally 
    {
    	try{	
    		closeConnection(conn,callStm,preStat,rsObj);
    		logger.debug("Method getStudentDetails :: Database Connection closed "+conn);
    	} catch (SQLException e) 
    	{
    		e.printStackTrace();
    	}
    }
	return studentreportObj;
}

public ArrayList<EduStudentReportTO> getSubjectClassByStudent(String student_id,String class_id,String session_id){
	 
	ArrayList<EduStudentReportTO> eduStudentReportArrayObj = new ArrayList<EduStudentReportTO>();
	
    try { 
    	getConnection();
    	logger.debug("Method getSubjectClassByStudent :: Database Connection opened");
    	callStm = conn.prepareCall("{call sp_student_session_class_detailsbyIds(?,?,?)}");	
    	callStm.setString(1,student_id);
    	callStm.setString(2,class_id);
    	callStm.setString(3,session_id);
    	callStm.execute();
    	rsObj = callStm.getResultSet();
    	logger.debug("Method getSubjectClassByStudent :: Procedure Executed==>"+callStm);
    	
    	while(rsObj.next()){
    		
    		EduStudentReportTO eduStudentReportTOobj = new EduStudentReportTO();
    		eduStudentReportTOobj.setStudentid(rsObj.getInt("student_id"));
			eduStudentReportTOobj.setClassname(rsObj.getString("class_name"));
			eduStudentReportTOobj.setSession_year(rsObj.getString("session_year"));
			eduStudentReportTOobj.setDepartmentname(rsObj.getString("dept_name"));
			eduStudentReportTOobj.setDivision(rsObj.getString("division")!=null?rsObj.getString("division"):"NA");
			eduStudentReportTOobj.setClassid(rsObj.getInt("class_id"));
			eduStudentReportTOobj.setSessionid(rsObj.getInt("session_id"));
			eduStudentReportTOobj.setDept_id(rsObj.getInt("dept_id"));
			eduStudentReportTOobj.setPercentage(rsObj.getFloat("percentage"));
			eduStudentReportTOobj.setTotalMarkObtained(rsObj.getFloat("total_mark_obtained"));
			eduStudentReportTOobj.setTotalOutOf(rsObj.getInt("total_mark_outof"));
			
			callStm = conn.prepareCall("{call sp_student_subject_class_details(?,?,?)}",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);	
	    	callStm.setInt(1,rsObj.getInt("student_id"));
	    	callStm.setInt(2,rsObj.getInt("class_id"));
	    	callStm.setInt(3,rsObj.getInt("session_id"));
	    	callStm.execute();
	    	ResultSet rssubObj = callStm.getResultSet();
	    	int recount=0;
    		rssubObj.last();
    		int rows = rssubObj.getRow();
    		System.out.println("total rows========"+rows);
    		String []subject_name = new String[rows];
    		rssubObj.beforeFirst();
	    	while(rssubObj.next()){
	    		subject_name[recount]=rssubObj.getString("subject_name");
	    		eduStudentReportTOobj.setSubject(subject_name);
	    		recount++;
	    	}
			eduStudentReportArrayObj.add(eduStudentReportTOobj);
    	}
    	
    }
    catch (Exception exception)
    {
    	exception.printStackTrace();
    	logger.debug("ERROR:: "+exception.getMessage());
    }
    finally 
    {
    	try{	
    		closeConnection(conn,callStm,preStat,rsObj);
    		logger.debug("Method getSubjectClassByStudent :: Database Connection closed "+conn);
    	} catch (SQLException e) 
    	{
    		e.printStackTrace();
    	}
    }
	return eduStudentReportArrayObj; 
}
public ArrayList<EduStudentReportTO> getClassinfo() {

	ArrayList<EduStudentReportTO> users = new ArrayList<EduStudentReportTO>();
	
	try{
		getConnection();
		logger.debug("Method getClassinfo :: Database Connection opened");
		preStat = conn.prepareStatement(query.getClassInfo);
		logger.debug("Method getClassinfo :: Query Executed==>"+preStat);
		rsObj=preStat.executeQuery();
		while(rsObj.next())
		{
		EduStudentReportTO eduStudentObj = new EduStudentReportTO();
		eduStudentObj.setClassid(rsObj.getInt("class_id"));
		eduStudentObj.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
		users.add(eduStudentObj);
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getClassinfo :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getClassinfo :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}

public  ArrayList<EduStudentReportTO> getDepartment()
{
	 ArrayList<EduStudentReportTO> studentreportObj=new  ArrayList<EduStudentReportTO>();
	try
	{
		getConnection();
		preStat = conn.prepareStatement(query.getDepartment);
		logger.debug("Method getDepartment :: Database Connection opened");
		preStat.setString(1,"Y");
		logger.debug("Method getDepartment :: Query Executed==>"+preStat);
		preStat.execute();	
		rsObj = preStat.getResultSet();
		 while(rsObj.next())
		    {
			EduStudentReportTO EduStudentTOObj =new EduStudentReportTO();
			EduStudentTOObj.setDept_title(rsObj.getString("dept_name"));
			EduStudentTOObj.setDept_id(rsObj.getInt("dept_id"));
			 studentreportObj.add(EduStudentTOObj); 	
		    }
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		logger.debug("Method getDepartment :: ERROR==>"+exp.getMessage());
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getDepartment :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}
	return studentreportObj;
	
}

public EduStudentReportTO getComingSession(Integer sessionid){
	
	EduStudentReportTO eduStudentReportTOObj = new EduStudentReportTO();
	try
	{	
	getConnection();
	preStat = conn.prepareStatement("select session_id,session_year from sch_student_session where session_id=?");
	preStat.setInt(1,sessionid);
	rsObj=preStat.executeQuery();
	if(rsObj.next()){
		eduStudentReportTOObj.setSessionid(rsObj.getInt("session_id"));
		eduStudentReportTOObj.setSession_year(rsObj.getString("session_year"));
	}
	logger.debug("Method getComingSession :: ERROR==>"+preStat);
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		logger.debug("Method getComingSession :: ERROR==>"+exp.getMessage());
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method isRecordExist :: Database Connection closed");
	}catch(Exception exp){
		exp.printStackTrace();
	}}
   return eduStudentReportTOObj;

	
}

public boolean assignedClassSubToNewSession(EduStudentReportTO eduStudentReportToObj,EduStudentReportForm eduStudentReportFormObj){
	
	boolean bool;
	try
	{
		getConnection();
		logger.debug("Method assignedClassSubToNewSession :: Database Connection opened");
		conn.setAutoCommit(false);
	    callStm = conn.prepareCall("{call insert_student_new_session_sp(?,?,?,?,?,?,?)}");
	    callStm.setInt(1, eduStudentReportToObj.getStudentid());
	    callStm.setInt(2, eduStudentReportToObj.getClassid());
	    callStm.setInt(3, eduStudentReportToObj.getSessionid());
	    callStm.setInt(4, eduStudentReportToObj.getDept_id());
	    callStm.setInt(5, eduStudentReportToObj.getAdmissionfee());
	    callStm.setInt(6, eduStudentReportToObj.getConcessionamt());
	    callStm.setString(7, eduStudentReportToObj.getComment());
	    logger.debug("Method assignedClassSubToNewSession :: Procedure Executed==>"+callStm);
		int count  = callStm.executeUpdate();
		if(count==1){
			for(int xx=0;xx<eduStudentReportFormObj.getAssignsubjected().length;xx++)
			{
		
				preStat = conn.prepareStatement(query.insertSubjectStudentid);
			    preStat.setInt(1, eduStudentReportToObj.getStudentid());
			    preStat.setString(2, eduStudentReportFormObj.getAssignsubjected()[xx]);
			    preStat.setInt(3, eduStudentReportToObj.getSessionid());
			    preStat.setInt(4, eduStudentReportToObj.getClassid());
			    logger.debug("Method assignedClassSubToNewSession :: Query Executed for assigned classess==>"+preStat);
			    preStat.executeUpdate();
		    
		    }
			bool=true;
		}
		else{
		bool=false;
		}
		conn.commit();
	
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		bool=false;
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method saveEduStudentData :: Database Connection closed");
	}catch(Exception exp){
		exp.printStackTrace();
	}}
return bool;	
}



public EduStudentReportTO getEduStudentDatabyStudentId(String user_Id,String Stud_status)
{
	EduStudentReportTO EduStudentReportTOObj = new EduStudentReportTO();
	try
	{    
		getConnection();
		logger.debug("Method getEduStudentDatabyUserId :: Database Connection opened");
		callStm = conn.prepareCall("{call get_studentUserByUser_id(?,?)}");
		callStm.setInt(1,Integer.valueOf(user_Id));
		callStm.setString(2, Stud_status);
		logger.debug("Method getEduStudentDatabyUserId :: Procedure Executed==>"+callStm);
		rsObj = callStm.executeQuery();
		if(rsObj.next())
		{   
			EduStudentReportTOObj.setStudentid(rsObj.getInt("student_id"));			
			EduStudentReportTOObj.setFathername(rsObj.getString("father_name"));
			EduStudentReportTOObj.setDateofbirth(rsObj.getString("birth_day")+"/"+(Integer.parseInt(rsObj.getString("birth_month"))+1)+"/"+rsObj.getString("birth_year"));
			EduStudentReportTOObj.setStudentname(rsObj.getString("first_name")+" "+rsObj.getString("middle_name")+" "+rsObj.getString("last_name"));
			EduStudentReportTOObj.setUserid(rsObj.getString("user_id"));
			EduStudentReportTOObj.setAdmissiondate(rsObj.getString("admission_date"));
			EduStudentReportTOObj.setPhoneno(rsObj.getString("contact_no"));
			EduStudentReportTOObj.setEmailid(rsObj.getString("email_id"));
			EduStudentReportTOObj.setAddress(rsObj.getString("address"));
		}
		
	}
	catch(Exception exp){exp.printStackTrace();
	logger.debug("Method getEduStudentDatabyUserId :: ERROR==>"+exp.getMessage());
	}
	finally{
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getEduStudentDatabyUserId :: Database Connection closed");
			}catch(Exception exp){exp.printStackTrace();}}
	return EduStudentReportTOObj;
}

public ArrayList<EduStudentReportTO> getDataForGreenSheet(String classId,String sessionId){
	 
	ArrayList<EduStudentReportTO> eduStudentReportArrayObj = new ArrayList<EduStudentReportTO>();
	
    try { 
    	getConnection();
    	
    	callStm = conn.prepareCall("{call sch_studentDataForGreenSheet(?,?)}",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);	
    	callStm.setString(1,classId);
    	callStm.setString(2,sessionId);
    	callStm.execute();
    	rsObj = callStm.getResultSet();
    	logger.debug("Method getDataForGreenSheet :: Procedure Executed==>"+callStm);
    	rsObj.last();
    	int rowCount=0;
    	rowCount=rsObj.getRow();
    	logger.debug("Method getDataForGreenSheet :: Row Count==>"+rowCount);
    	rsObj.beforeFirst();
    	if(rsObj.getRow()!=rowCount){
        while(rsObj.next()){
        	EduStudentReportTO eduStudentReportTOobj = new EduStudentReportTO();
    		eduStudentReportTOobj.setStudentid(rsObj.getInt("student_id"));
			eduStudentReportTOobj.setStudentname(rsObj.getString("first_name")+" "+rsObj.getString("middle_name")+" "+rsObj.getString("last_name"));
			eduStudentReportTOobj.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
			eduStudentReportTOobj.setSession_year(rsObj.getString("session_year"));
			eduStudentReportTOobj.setDivision(rsObj.getString("division"));
			eduStudentReportTOobj.setResult(rsObj.getString("status"));
			eduStudentReportTOobj.setPercentage(rsObj.getFloat("percentage"));
			eduStudentReportTOobj.setTotalMarkObtained(rsObj.getFloat("total_mark_obtained"));
			eduStudentReportTOobj.setTotalOutOf(rsObj.getInt("total_mark_outof"));
			eduStudentReportTOobj.setSessionid(rsObj.getInt("session_id"));
			eduStudentReportTOobj.setClassid(rsObj.getInt("class_id"));
			callStm = conn.prepareCall("{call sp_student_sub_mark_report_by_session_class(?,?,?)}",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);	
	    	callStm.setInt(1,eduStudentReportTOobj.getStudentid());
	    	callStm.setString(2,classId);
	    	callStm.setString(3,sessionId);
	    	callStm.execute();
	    	ResultSet rssubObj = callStm.getResultSet();
	    	logger.debug("Method getDataForGreenSheet :: Procedure Executed==>"+callStm);
	    	int recount=0;
	    	rssubObj.last();
	    	int rows = rssubObj.getRow();
	    	String []subject_name = new String[rows];
	    	String []markOutOf = new String[rows];
			String []markObtain = new String[rows];
			rssubObj.beforeFirst();
	    	while(rssubObj.next()){
	    		String mark_outof="";
	    		String mark_obtained="";
	    		subject_name[recount]=rssubObj.getString("subject_name");
	    		eduStudentReportTOobj.setSubject(subject_name);
	    		if(rssubObj.getString("mark_outof")!=null){
	    			mark_outof=rssubObj.getString("mark_outof");
	    		}	
	    		markOutOf[recount]=mark_outof;
	    		
	    		eduStudentReportTOobj.setMarkOutOf(markOutOf);
	    		if(rssubObj.getString("mark_obtained")!=null){
	    			mark_obtained=rssubObj.getString("mark_obtained");
	    		}
	    		markObtain[recount]=mark_obtained;
	    		eduStudentReportTOobj.setMarkObtain(markObtain);
	    		recount++;
	    	}
	    	eduStudentReportArrayObj.add(eduStudentReportTOobj);
    	}
    	
    }
    	else{
    		EduStudentReportTO eduStudentReportTOobj = new EduStudentReportTO();
    		eduStudentReportTOobj.setSessionid(Integer.valueOf(sessionId));
    		eduStudentReportTOobj.setClassid(Integer.valueOf(classId));
    		eduStudentReportTOobj.setStudentid(0);
    		eduStudentReportArrayObj.add(eduStudentReportTOobj);
    	}
	
    }
    catch (Exception exception)
    {
    	exception.printStackTrace();
    	logger.debug("ERROR:: "+exception.getMessage());
    }
    finally 
    {
    	try{	
    		closeConnection(conn,callStm,preStat,rsObj);
    		logger.debug("Method getDataForGreenSheet :: Database Connection closed "+conn);
    	} catch (SQLException e) 
    	{
    		e.printStackTrace();
    	}
    }
	return eduStudentReportArrayObj; 
}

}