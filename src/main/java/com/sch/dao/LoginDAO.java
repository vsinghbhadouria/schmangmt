package com.sch.dao;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

import com.sch.to.LoginTO;
//import com.sch.to.OrderTO;
//import com.nal.sse.to.StrudentTO;


public class LoginDAO extends ConnectionUtil{
	
	Query query =null;
    ResultSet rsObj = null; 
    PreparedStatement preStat=null;	
    CallableStatement callStm = null;  
    public static Logger logger = Logger.getLogger(LoginDAO.class);
	public LoginDAO()
	{
		query = new Query();
	}
			public LoginTO checkLogin(String loginid,String password,Integer userType)
			{
				LoginTO objloginto = new LoginTO();
				try
				{
					
					getConnection();
					logger.debug("Database Connection==>"+conn+" userType"+userType+"password"+password+"loginid"+loginid);
					callStm =conn.prepareCall("{call user_login_sp(?,?,?)}");		
					callStm.setString(1,loginid);
					callStm.setString(2,password);
					callStm.setInt(3,userType);
					logger.debug("Query Executed==>"+callStm);
					rsObj = callStm.executeQuery();
					if(rsObj.next())
					{
						
						objloginto.setUserid(rsObj.getString("userId"));
						objloginto.setUsername(rsObj.getString("first_name"));
						objloginto.setUserlastname(rsObj.getString("last_name"));
						objloginto.setEmail(rsObj.getString("email_id"));
						objloginto.setPassword(rsObj.getString("password"));
						objloginto.setUg_title(rsObj.getString("ug_title"));
						objloginto.setUg_id(rsObj.getString("ug_id"));
						objloginto.setWelcome_msg(rsObj.getString("welcome_message"));
						objloginto.setUgCode(rsObj.getString("ug_code"));
						
					}
					
					}
				catch(Exception exp){
					logger.debug("Error"+exp.getMessage());
				}finally{
					try {
						closeConnection(conn,callStm,preStat,rsObj);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				
				return objloginto;
			}
			
			public boolean checkAccess(Integer ug_id,Integer f_id)
			{
				boolean flag=false;
				try
				{
					getConnection(); 
					preStat =conn.prepareStatement(query.checkUgAccess);
					preStat.setInt(1, ug_id);
					preStat.setInt(2, f_id);
					logger.debug("Query Executed with in Method checkAccess ==>"+preStat);
					
					rsObj =	preStat.executeQuery();
					
				    if(rsObj.next())
				    {
				    	flag = true;
				    }
				    else
				    {
				        flag = false;	
				    }
				}catch(Exception exp){
					exp.printStackTrace();

				
			}finally{
				try {
					closeConnection(conn,callStm,preStat,rsObj);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
				return flag;	
}

			public boolean checkPassword(String password, String user_id, String ug_id)
			{
				boolean flag=false;
				try
				{
					getConnection();
					
					callStm =conn.prepareCall("{call check_password_sp(?,?,?)}");	
					callStm.setString(1,user_id);
					callStm.setString(2,password);
					callStm.setString(3,ug_id);
					logger.debug("Query Executed with in Method checkPassword ==>"+callStm);
					rsObj = callStm.executeQuery();
					if(rsObj.next() && rsObj.getInt(1)!=0){
						flag =true;
					}
				}
				catch(Exception exp) {
						exp.printStackTrace();
				}finally {
					try {
						closeConnection(conn,callStm,preStat,rsObj);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				    logger.debug("Password Check ==>"+flag);
					return flag;
			}

			public void savePassword(LoginTO loginToObj)
			{
				try
				{
				  getConnection();
					logger.debug("Database Connection==>"+conn+" New Pass "+loginToObj.getNewpassword()+" Ug Id"+loginToObj.getUg_id()+" loginid "+loginToObj.getUserid());
					callStm =conn.prepareCall("{call change_password_sp(?,?,?,?)}");		
					callStm.setString(1,loginToObj.getUserid());
					callStm.setString(2,loginToObj.getNewpassword());
					callStm.setString(3,loginToObj.getUg_id());
					callStm.setString(4,loginToObj.getPassword());
					logger.debug("Query Executed with in Method savePassword ==>"+callStm);
					callStm.executeUpdate();
				}catch(Exception exp){
						exp.printStackTrace();
				}finally{
					try {
						closeConnection(conn,callStm,preStat,rsObj);
						
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
			  public void loginUserInfo(LoginTO objloginto)
			   {
					try
					{
					  getConnection();
					  preStat = conn.prepareStatement(query.loginUserInfo);
					  preStat.setString(1, objloginto.getUserid());
					  preStat.setString(2, objloginto.getUsername()+" "+objloginto.getUserlastname());
					  preStat.executeUpdate();
					 }catch(Exception exp){
							exp.printStackTrace();
					}finally{
						try {
							closeConnection(conn,callStm,preStat,rsObj);
							
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				  
			   }
}