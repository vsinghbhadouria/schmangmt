package com.sch.dao;
import java.sql.*;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import com.sch.constant.PortableConstant;
import com.sch.form.EduStaffForm;
import com.sch.form.EduStudentForm;
import com.sch.form.LibraryManagementForm;
import com.sch.to.*;

public class LibraryManagementDAO extends ConnectionUtil

{
	Query query =null;
    ResultSet rsObj = null; 
    PreparedStatement preStat=null;	
    CallableStatement callStm = null;
    public static Logger logger = Logger.getLogger(LibraryManagementDAO.class);
	public LibraryManagementDAO()
	{
		 query = new Query();  
	}
	
	public  ArrayList<LibraryManagementTO> getBooktypes()
	{
		 ArrayList<LibraryManagementTO> bookTypeArray=new  ArrayList<LibraryManagementTO>();
		try
		{
			getConnection();
			logger.debug("Method getBooktypes :: Database Connection opened");
			preStat = conn.prepareStatement(query.getBookType);
			logger.debug("Method getBooktypes :: Query Executed==>"+preStat);
			rsObj = preStat.executeQuery();	
			
			 while(rsObj.next())
			    {
				 LibraryManagementTO bookType =new LibraryManagementTO();
				 bookType.setBooktypename(rsObj.getString("book_type_name"));
				 bookType.setBooktypeid(rsObj.getInt("book_type_id"));
				 bookTypeArray.add(bookType); 	
			    }
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method getStaffQualification :: ERROR==>"+exp.getMessage());
		}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method getStaffQualification :: Database Connection closed");
				}catch(Exception exp){
					exp.printStackTrace();
				}}
		return bookTypeArray;
		
	}
	
	public ArrayList<LibraryManagementTO> getBookNames() {
         
		ArrayList<LibraryManagementTO> booknameArray = new ArrayList<LibraryManagementTO>();
		try{
			getConnection();
			logger.debug("Method getRackIds :: Database Connection opened");
			preStat = conn.prepareStatement(query.getBookNames);
			//preStat.setString(1, "%"+bookName+"%");
			logger.debug("Method getRackIds :: Procedure Executed==>"+preStat);
			System.out.println("preStat   "+preStat);
			rsObj=preStat.executeQuery();
			while(rsObj.next())
			{
			LibraryManagementTO bookNameObj = new LibraryManagementTO();
			bookNameObj.setBookname(rsObj.getString("book_name"));
			booknameArray.add(bookNameObj);
			}
		}catch (Exception exp) {
			exp.printStackTrace();
			logger.debug("Method getRackIds :: ERROR==>"+exp.getMessage());
		}
		finally{
			try {
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method getRackIds :: Database Connection closed");
				} 
		catch (Exception exp) {exp.printStackTrace();}}
		return booknameArray;
	}
	

	public String genrateBookIds(String bookName,int numberOfCopy,int bookTypeId){
        
			int maxbookId=0;
			String prefixName=null;
			String bookIds=null;
		try{
			getConnection();
			logger.debug("Method genrateBookIds :: Database Connection opened");
			preStat = conn.prepareStatement(query.getMaxlibrarybookId);
			rsObj=preStat.executeQuery();
			logger.debug("Method genrateBookIds :: Procedure Executed==>"+preStat);
			if(rsObj.next()){
				maxbookId = rsObj.getInt("maxbookId")+1;
			}
			preStat = conn.prepareStatement(query.getPrefixBookId);
			preStat.setInt(1, bookTypeId);
			rsObj=preStat.executeQuery();
			logger.debug("Method genrateBookIds :: Procedure Executed==>"+callStm);
			if(rsObj.next()){
				prefixName = rsObj.getString("prefix_book_id");
			}
			
			for(int xx=0; xx<numberOfCopy;xx++){
				if(bookIds!=null){
				bookIds=bookIds+" "+(prefixName+ ++maxbookId);
				}
				else{
				bookIds=prefixName+maxbookId;
				}
			}
		}catch (Exception exp) {
			exp.printStackTrace();
			logger.debug("Method getRackIds :: ERROR==>"+exp.getMessage());
		}
		finally{
			try {
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method getRackIds :: Database Connection closed");
				} 
		catch (Exception exp) {exp.printStackTrace();}}
		return bookIds;
	}
	
	
	public LibraryManagementTO addLibraryBooks(LibraryManagementTO libraryManagementTO,LibraryManagementForm libraryManagementForm)
	{
		LibraryManagementTO eduStudentTOobj=null;
		try
		{
			
			getConnection();
			int book_master_id=0;
			int number_of_copy=0;
			logger.debug("Method saveEduStudentData :: Database Connection opened");
			conn.setAutoCommit(false);
		    preStat = conn.prepareStatement(query.getBookByName);	
		    preStat.setString(1,libraryManagementTO.getBookname());
		    logger.debug("Method saveEduStudentData :: Procedure Executed==>"+preStat);
		    rsObj = preStat.executeQuery();
		    if(rsObj.next()){
		    	book_master_id=rsObj.getInt("bm_id");
		    	number_of_copy=rsObj.getInt("number_of_copy");
		    }
			if(book_master_id==0){
		    preStat = conn.prepareStatement(query.addBookdetails);	
		    preStat.setString(1,libraryManagementTO.getBookname());
		    preStat.setInt(2,libraryManagementTO.getNumberofcopy());
			preStat.setString(3,libraryManagementTO.getBooktypename());
			preStat.setInt(4,libraryManagementTO.getRackid());
			logger.debug("Method saveEduStudentData :: Procedure Executed==>"+preStat);
			preStat.executeUpdate();
			}
			else{
				
				 preStat = conn.prepareStatement(query.updateBookdetails);	
				 preStat.setInt(1,number_of_copy+libraryManagementTO.getNumberofcopy());
				 preStat.setInt(2,book_master_id);
				 preStat.executeUpdate();
			}
			if(libraryManagementTO.getBookids()!=null && libraryManagementTO.getBookids().length!=0){
				int bm_id=-1;
				if(book_master_id==0){
		    	preStat = conn.prepareStatement(query.getMaxBookId);
				rsObj = preStat.executeQuery();
				
				while(rsObj.next()){
					bm_id =(rsObj.getInt("maxbookId"));	
				}
			}
				else{
					bm_id=book_master_id;
				}
				
					for(int xx=0;xx<libraryManagementTO.getBookids().length;xx++){
					    preStat = conn.prepareStatement(query.insertBookIds);
					    preStat.setInt(1, bm_id);
					    preStat.setString(2, libraryManagementTO.getBookids()[xx]);
					    preStat.setString(3, "N");
					    logger.debug("Method saveEduStudentData :: Query Executed for assigned classess==>"+preStat);
					    preStat.executeUpdate();
				    }
		    }
		    conn.commit();
		 
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method saveEduStudentData :: ERROR==>"+exp.getMessage());
			try {
				conn.rollback();
				logger.debug("Method saveEduStudentData :: Transaction rollbacked");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method saveEduStudentData :: Database Connection closed");
		}catch(Exception exp){
			exp.printStackTrace();
		}}
	return eduStudentTOobj;	
	}
	
	public ArrayList<LibraryManagementTO> getRackIds(int bookTypeId) {
        
		ArrayList<LibraryManagementTO> rackArray = new ArrayList<LibraryManagementTO>();
		try{
			getConnection();
			logger.debug("Method getRackIds :: Database Connection opened");
			preStat = conn.prepareStatement(query.getRackIds);
			preStat.setInt(1, bookTypeId);
			rsObj=preStat.executeQuery();
			logger.debug("Method getRackIds :: Procedure Executed==>"+preStat);
			while(rsObj.next())
			{
			LibraryManagementTO rackDetail = new LibraryManagementTO();
			rackDetail.setRackid(rsObj.getInt("rack_id"));
			rackDetail.setRackname(rsObj.getString("rack_name"));
			rackArray.add(rackDetail);
			}
		}catch (Exception exp) {
			exp.printStackTrace();
			logger.debug("Method getRackIds :: ERROR==>"+exp.getMessage());
		}
		finally{
			try {
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method getRackIds :: Database Connection closed");
				} 
		catch (Exception exp) {exp.printStackTrace();}}
		return rackArray;
	}
	public  ArrayList<LibraryManagementTO> getAllbooks()
	{
		 ArrayList<LibraryManagementTO> bookArray=new  ArrayList<LibraryManagementTO>();
		try
		{
			getConnection();
			logger.debug("Method getBooktypes :: Database Connection opened");
			preStat = conn.prepareStatement(query.getAllbooks);
			logger.debug("Method getBooktypes :: Query Executed==>"+preStat);
			rsObj = preStat.executeQuery();	
			
			 while(rsObj.next())
			    {
				 LibraryManagementTO book =new LibraryManagementTO();
				 book.setBookname(rsObj.getString("book_name"));
				 book.setNumberofcopy(rsObj.getInt("number_of_copy"));
				 book.setBooktypename(rsObj.getString("book_type_name"));
				 book.setRackname(rsObj.getString("rack_name"));
				 book.setAvailablebookcount(rsObj.getInt("available_count"));
				 bookArray.add(book); 	
			    }
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method getStaffQualification :: ERROR==>"+exp.getMessage());
		}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method getStaffQualification :: Database Connection closed");
				}catch(Exception exp){
					exp.printStackTrace();
				}}
		return bookArray;
		
	}
	
	public ArrayList<LibraryManagementTO> getAvailableBookIds(String bookName) {
        
		ArrayList<LibraryManagementTO> rackArray = new ArrayList<LibraryManagementTO>();
		try{
			getConnection();
			logger.debug("Method getRackIds :: Database Connection opened");
			callStm = conn.prepareCall("{call getAvailableBooksBybookName(?)}");
			callStm.setString(1, bookName);
			rsObj=callStm.executeQuery();
			logger.debug("Method getRackIds :: Procedure Executed==>"+callStm);
			while(rsObj.next())
			{
			LibraryManagementTO rackDetail = new LibraryManagementTO();
			rackDetail.setBookcopyid(rsObj.getInt("book_copy_id"));
			rackDetail.setBookid(rsObj.getString("book_id"));
			rackArray.add(rackDetail);
			}
		}catch (Exception exp) {
			exp.printStackTrace();
			logger.debug("Method getRackIds :: ERROR==>"+exp.getMessage());
		}
		finally{
			try {
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method getRackIds :: Database Connection closed");
				} 
		catch (Exception exp) {exp.printStackTrace();}}
		return rackArray;
	}
	
	public ArrayList<LibraryManagementTO> getAvailableBookcountandtype(String bookName) {
        
		ArrayList<LibraryManagementTO> rackArray = new ArrayList<LibraryManagementTO>();
		try{
			getConnection();
			logger.debug("Method getAvailableBookcountandtype :: Database Connection opened");
			callStm = conn.prepareCall("{call getAvailableBookcountandtype_sp(?)}");
			callStm.setString(1, bookName);
			rsObj=callStm.executeQuery();
			logger.debug("Method getAvailableBookcountandtype :: Procedure Executed==>"+callStm);
			while(rsObj.next())
			{
			LibraryManagementTO rackDetail = new LibraryManagementTO();
			rackDetail.setAvailablebookcount(rsObj.getInt("availability_count"));
			rackDetail.setRackname(rsObj.getString("rack_name"));
			rackDetail.setBooktypename(rsObj.getString("book_type_name"));
			rackDetail.setBookmasterid(rsObj.getInt("bm_id"));
			rackArray.add(rackDetail);
			}
		}catch (Exception exp) {
			exp.printStackTrace();
			logger.debug("Method getAvailableBookcountandtype :: ERROR==>"+exp.getMessage());
		}
		finally{
			try {
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method getAvailableBookcountandtype :: Database Connection closed");
				} 
		catch (Exception exp) {exp.printStackTrace();}}
		return rackArray;
	}
	
public ArrayList<EduStudentTO> getStudentDataByUserId(String userId) {
        
		ArrayList<EduStudentTO> rackArray = new ArrayList<EduStudentTO>();
		try{
			getConnection();
			logger.debug("Method getAvailableBookcountandtype :: Database Connection opened");
			callStm = conn.prepareCall("{call getStudentDataByUserId_sp(?,?)}");
			callStm.setString(1, userId);
			callStm.setString(2, "");
			rsObj=callStm.executeQuery();
			logger.debug("Method getAvailableBookcountandtype :: Procedure Executed==>"+callStm);
			while(rsObj.next())
			{

			EduStudentTO rackDetail = new EduStudentTO();
			rackDetail.setFirstname(rsObj.getString("first_name")+" "+rsObj.getString("middle_name")+" "+rsObj.getString("last_name"));
			rackDetail.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
			rackDetail.setEmailid(rsObj.getString("email_id"));
			rackArray.add(rackDetail);
			
			}
		}catch (Exception exp) {
			exp.printStackTrace();
			logger.debug("Method getAvailableBookcountandtype :: ERROR==>"+exp.getMessage());
		}
		finally{
			try {
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method getAvailableBookcountandtype :: Database Connection closed");
				} 
		catch (Exception exp) {exp.printStackTrace();}}
		return rackArray;
	}
	
public ArrayList<EduStaffTO> getTeacherDataByUserId(String userId) {
    
	ArrayList<EduStaffTO> rackArray = new ArrayList<EduStaffTO>();
	try{
		getConnection();
		logger.debug("Method getAvailableBookcountandtype :: Database Connection opened");
		callStm = conn.prepareCall("{call getTeacherDataByUserId_sp(?,?)}");
		callStm.setString(1, userId);
		callStm.setString(2, "");
		rsObj=callStm.executeQuery();
		logger.debug("Method getAvailableBookcountandtype :: Procedure Executed==>"+callStm);
		while(rsObj.next())
		{

		EduStaffTO rackDetail = new EduStaffTO();
		rackDetail.setFirstname(rsObj.getString("first_name")+" "+rsObj.getString("middle_name")+" "+rsObj.getString("last_name"));
		rackDetail.setQualification_title(rsObj.getString("qualification_title"));
		rackDetail.setEmailid(rsObj.getString("email_id"));
		rackDetail.setPhoneno(rsObj.getString("contact_no"));
		rackArray.add(rackDetail);
		
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getAvailableBookcountandtype :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getAvailableBookcountandtype :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return rackArray;
}

	


public ArrayList<LibraryManagementTO> assignBookToUser(LibraryManagementTO libraryManagementTO) {
    
	ArrayList<LibraryManagementTO> rackArray = new ArrayList<LibraryManagementTO>();
	try{
		getConnection();
		logger.debug("Method assignBookToUser :: Database Connection opened");
		callStm = conn.prepareCall("{call assignBookToUser_sp(?,?,?,?)}");
		callStm.setString(1, libraryManagementTO.getUserId());
		callStm.setString(2, libraryManagementTO.getUserType());
		callStm.setString(3, libraryManagementTO.getBookid());
		callStm.setInt(4, libraryManagementTO.getBookmasterid());
		callStm.executeUpdate();
		logger.debug("Method assignBookToUser :: Procedure Executed==>"+callStm);
		
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method assignBookToUser :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method assignBookToUser :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return rackArray;
}
public  ArrayList<LibraryManagementTO> getAssignedbooks()
{
	 ArrayList<LibraryManagementTO> bookArray=new  ArrayList<LibraryManagementTO>();
	try
	{
		getConnection();
		logger.debug("Method getBooktypes :: Database Connection opened");
		callStm = conn.prepareCall("{call getAssignedbooksList_sp()}");
		logger.debug("Method getBooktypes :: Query Executed==>"+callStm);
		rsObj = callStm.executeQuery();	
		
		 while(rsObj.next()) {
			 LibraryManagementTO book =new LibraryManagementTO();
			 book.setUserName(rsObj.getString("first_name")+" "+rsObj.getString("middle_name")+" "+rsObj.getString("last_name"));
			 book.setUserId(rsObj.getString("user_id"));
			 book.setUserType(rsObj.getString("userType"));
			 book.setBookname(rsObj.getString("book_name"));
			 book.setBookid(rsObj.getString("book_id"));
			 book.setIssueDate(rsObj.getString("issue_date"));
			 book.setDueDate(rsObj.getString("due_date"));
			 bookArray.add(book); 	
		    }
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		logger.debug("Method getStaffQualification :: ERROR==>"+exp.getMessage());
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getStaffQualification :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}
	return bookArray;
	
}

public  ArrayList<LibraryManagementTO> getassignedBookByBookId(String bookId)
{
	 ArrayList<LibraryManagementTO> bookArray=new  ArrayList<LibraryManagementTO>();
	try
	{
		getConnection();
		logger.debug("Method getBooktypes :: Database Connection opened");
		callStm = conn.prepareCall("{call getAssignedbookBybookId_sp(?)}");
		callStm.setString(1,bookId);
		logger.debug("Method getBooktypes :: Query Executed==>"+callStm);
		rsObj = callStm.executeQuery();	
		
		 while(rsObj.next()) {
			 LibraryManagementTO book =new LibraryManagementTO();
			 book.setUserName(rsObj.getString("first_name")+" "+rsObj.getString("middle_name")+" "+rsObj.getString("last_name"));
			 book.setUserId(rsObj.getString("user_id"));
			 book.setEmailId(rsObj.getString("email_id"));
			 book.setContact_no(rsObj.getString("contact_no"));
			 book.setBookname(rsObj.getString("book_name"));
			 book.setBookid(rsObj.getString("book_id"));
			 book.setIssueDate(rsObj.getString("issue_date"));
			 book.setDueDate(rsObj.getString("due_date"));
			 book.setRackname(rsObj.getString("rack_name"));
			 book.setUserType(rsObj.getString("userType"));
			 book.setBooktypename(rsObj.getString("book_type_name"));
			 if(rsObj.getString("status")!="D"){
				 book.setStatus("Active");
			 }
			 else{
				 book.setStatus("Deactive");
			 }
			 bookArray.add(book); 	
		    }
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		logger.debug("Method getStaffQualification :: ERROR==>"+exp.getMessage());
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getStaffQualification :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}
	return bookArray;
	
}

public LibraryManagementTO returnBook(LibraryManagementTO libraryManagementTO) {
  
	 LibraryManagementTO book=new  LibraryManagementTO();
	try{
		getConnection();
		logger.debug("Method assignBookToUser :: Database Connection opened");
		callStm = conn.prepareCall("{call returnBookByuser_sp(?)}");
		callStm.setString(1, libraryManagementTO.getBookid());
		callStm.executeUpdate();
		callStm = conn.prepareCall("{call getReturnedbooksListByBookId_sp(?,?)}");
		callStm.setString(1,libraryManagementTO.getBookid());
		callStm.setString(2, libraryManagementTO.getUserId());
		logger.debug("Method assignBookToUser :: Procedure Executed==>"+callStm);
		rsObj = callStm.executeQuery();
		
		 while(rsObj.next()) {
			 //LibraryManagementTO book =new LibraryManagementTO();
			 book.setUserName(rsObj.getString("first_name")+" "+rsObj.getString("middle_name")+" "+rsObj.getString("last_name"));
			 book.setUserId(rsObj.getString("user_id"));
			 book.setUserType(rsObj.getString("userType"));
			 book.setBookname(rsObj.getString("book_name"));
			 book.setBookid(rsObj.getString("book_id"));
			 book.setIssueDate(rsObj.getString("issue_date"));
			 book.setDueDate(rsObj.getString("due_date"));
			 book.setReturnDate(rsObj.getString("return_date"));
			 book.setEmailId(rsObj.getString("email_id"));
			// bookArray.add(book); 	
		    }
		
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method assignBookToUser :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method assignBookToUser :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	
	return book;
	
	
}

public  ArrayList<LibraryManagementTO> getReturnedbooks()
{
	 ArrayList<LibraryManagementTO> bookArray=new  ArrayList<LibraryManagementTO>();
	try
	{
		getConnection();
		logger.debug("Method getBooktypes :: Database Connection opened");
		callStm = conn.prepareCall("{call getReturnedBookList_sp()}");
		logger.debug("Method getBooktypes :: Query Executed==>"+callStm);
		rsObj = callStm.executeQuery();	
		
		 while(rsObj.next()) {
			 LibraryManagementTO book =new LibraryManagementTO();
			 book.setUserName(rsObj.getString("first_name")+" "+rsObj.getString("middle_name")+" "+rsObj.getString("last_name"));
			 book.setUserId(rsObj.getString("user_id"));
			 book.setUserType(rsObj.getString("userType"));
			 book.setBookname(rsObj.getString("book_name"));
			 book.setBookid(rsObj.getString("book_id"));
			 book.setIssueDate(rsObj.getString("issue_date"));
			 book.setDueDate(rsObj.getString("due_date"));
			 book.setReturnDate(rsObj.getString("return_date"));
			 bookArray.add(book); 	
		    }
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		logger.debug("Method getStaffQualification :: ERROR==>"+exp.getMessage());
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getStaffQualification :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}
	return bookArray;
	
}

}