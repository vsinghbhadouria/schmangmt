package com.sch.dao;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

import com.sch.to.*;
public class ConcernTrackingDAO extends ConnectionUtil
{
	Query query =null;
    ResultSet rest = null;
    Connection conn = null ;
    PreparedStatement preStat=null;	
    ConnectionUtil conUtil = null;
    ResultSet rsObj = null; 
    CallableStatement callStm = null;
    Logger logger  = Logger.getLogger(ConcernTrackingDAO.class);
	public ConcernTrackingDAO()
	{
		 query = new Query();        
		 conUtil = new ConnectionUtil();
	}
	public  ArrayList<ConcernTrackingTO> getIssueTrackingDetail(int totalRecord,int startIndex,String sort,String dir,String SearchKey,String searchcategory,String searchpriority,String userid) 
	{
	   	ArrayList<ConcernTrackingTO> IssueTrackingTO = new ArrayList<ConcernTrackingTO>();
        PreparedStatement setRow = null;
	    try { 
	    	
	    	conn =  conUtil.getConnection();
	    	setRow = conn.prepareStatement(query.setrow);
	    	SearchKey = SearchKey!=""?SearchKey:"%%";
	    	searchcategory = searchcategory!=""?searchcategory:"%%";
	    	searchpriority = searchpriority!=""?searchpriority:"%%";
	    	userid = userid!=""?"in("+userid+")":"like "+"'%%'";
	    	System.out.println(userid);
	    	    	//select issue_id,issue_title,issue_desc,category,status,priority,user_first_name,ug_title from (select issue_id,issue_title, issue_desc,category, status,priority,user_first_name,ug_title from nal_issuetracking ni join sch_users nu on ni.user_id = nu.user_id join sch_staff_heirarchy nsh on nsh.user_id = ni.user_id join sch_user_groups nug on nsh.ug_id = nug.ug_id where Upper(status) like Upper('"+SearchKey+"') and Upper(Priority) like Upper('"+searchpriority+"') and Upper(category) like Upper('"+searchcategory+"')  order by status asc ) As derived1
	    	//select ni.issue_id,issue_title,issue_desc,category,nir.status,priority,user_first_name,ug_title from sch_issue ni join sch_issue_reply nir on ni.issue_id = nir.issue_id  join sch_issue_category nc on ni.cat_id = nc.cat_id join sch_users nu on nir.user_id = nu.user_id join sch_staff_heirarchy nsh on nsh.user_id = nir.user_id join sch_user_groups nug on nsh.ug_id = nug.ug_id where Upper(status) like Upper('"+SearchKey+"') and Upper(Priority) like Upper('"+searchpriority+"') and Upper(category) like Upper('"+searchcategory+"')  order by status asc
	    	preStat = conn.prepareStatement("select ni.issue_id,issue_title,issue_desc,category,issue_submited_date,ni.current_status" +
	    									" ,ni.creater_id,priority,user_first_name,user_last_name,ug_title,nu.user_id, (select count(*) from sch_issue_reply r where r.issue_id = ni.issue_id) reply " +
	    									" from sch_issue ni  join sch_issue_category nc on ni.cat_id = nc.cat_id join sch_users nu " +
	    									" on ni.creater_id = nu.user_id join sch_staff_heirarchy nsh " +
	    									" on nsh.user_id = ni.creater_id join sch_user_groups nug " +
	    									" on nsh.ug_id = nug.ug_id where nu.user_id "+userid+" and Upper(current_status) " +
	    									" like Upper('"+SearchKey+"') and Upper(Priority) like Upper('"+searchpriority+"') " +
	    									" and Upper(category) like Upper('"+searchcategory+"')  " +
	    									" order by "+sort+" " +dir+ " limit " +startIndex+ ", " +totalRecord);
	    	
	    	
	    	
			setRow.executeQuery();
			System.out.println("<<<<<<<<<<<<<preStat>>>>>>>>>>>>>"+preStat);
	    	rest = preStat.executeQuery();
	    	while(rest.next())
	    	{
	    		
	    		ConcernTrackingTO IssueTrackingtemp = new ConcernTrackingTO();
	    		IssueTrackingtemp.setIssueid(rest.getInt("issue_id"));
	    		IssueTrackingtemp.setUsername(rest.getString("user_first_name")+" "+rest.getString("user_last_name"));
	    		IssueTrackingtemp.setDesgnation(rest.getString("ug_title"));
	    		IssueTrackingtemp.setIssuetitle(rest.getString("issue_title"));
	    		IssueTrackingtemp.setIssuedesc(rest.getString("issue_desc"));
	    		IssueTrackingtemp.setCategory(rest.getString("category"));
	    		IssueTrackingtemp.setPriority(rest.getString("priority"));
	    		IssueTrackingtemp.setStatus(rest.getString("current_status"));
	    		IssueTrackingtemp.setUserid(rest.getString("user_id"));
	    		IssueTrackingtemp.setReplyid(Integer.parseInt(rest.getString("reply")));
	    		IssueTrackingtemp.setIssuesubmiteddate(rest.getString("issue_submited_date"));
	    		IssueTrackingTO.add(IssueTrackingtemp);
	    			
	    	}
    		  
		}
	    catch (Exception exception) 
	    {
	    	System.out.println(" ERROR: " + exception.getMessage());
		}
	    finally {
	    	try {
	    		conn.close();
				rest.close();
				setRow.close();
		    	preStat.close();
		    	
			} catch (SQLException e) {

				e.printStackTrace();
			}
	    }
		return IssueTrackingTO;
	}
	public  ArrayList<ConcernTrackingTO> getIssueTrackingDetailbyIssue_id(int totalRecord,int startIndex,String sort,String dir,String SearchKey,String searchcategory,String searchpriority,String issue_id) 
	{
	   	ArrayList<ConcernTrackingTO> IssueTrackingTO = new ArrayList<ConcernTrackingTO>();
        PreparedStatement setRow = null;
	    try { 
	    	int countRow=0;
	    	conn =  conUtil.getConnection();
	    	setRow = conn.prepareStatement(query.setrow);
	    	SearchKey = SearchKey!=""?SearchKey:"%%";
	    	searchcategory = searchcategory!=""?searchcategory:"%%";
	    	searchpriority = searchpriority!=""?searchpriority:"%%";
	    	    	//select issue_id,issue_title,issue_desc,category,status,priority,user_first_name,ug_title from (select issue_id,issue_title, issue_desc,category, status,priority,user_first_name,ug_title from sch_issuetracking ni join sch_users nu on ni.user_id = nu.user_id join sch_staff_heirarchy nsh on nsh.user_id = ni.user_id join sch_user_groups nug on nsh.ug_id = nug.ug_id where Upper(status) like Upper('"+SearchKey+"') and Upper(Priority) like Upper('"+searchpriority+"') and Upper(category) like Upper('"+searchcategory+"')  order by status asc ) As derived1
	    	//select ni.issue_id,issue_title,issue_desc,category,nir.status,priority,user_first_name,ug_title from sch_issue ni join sch_issue_reply nir on ni.issue_id = nir.issue_id  join sch_issue_category nc on ni.cat_id = nc.cat_id join sch_users nu on nir.user_id = nu.user_id join sch_staff_heirarchy nsh on nsh.user_id = nir.user_id join sch_user_groups nug on nsh.ug_id = nug.ug_id where Upper(status) like Upper('"+SearchKey+"') and Upper(Priority) like Upper('"+searchpriority+"') and Upper(category) like Upper('"+searchcategory+"')  order by status asc
	    	preStat = conn.prepareStatement("select ni.issue_id,nir.status,user_first_name,user_last_name,ug_title,reply_desc,issue_reply_date,nir.reply_id,nu.user_id from sch_issue ni join sch_issue_reply nir on ni.issue_id = nir.issue_id  join sch_users nu on nir.user_id = nu.user_id join sch_staff_heirarchy nsh on nsh.user_id = nir.user_id join sch_user_groups nug on nsh.ug_id = nug.ug_id where ni.issue_id = ? order by "+sort+"  "+dir+" limit ?, ?");
	    	preStat.setInt(1,Integer.valueOf(issue_id));
	    	preStat.setInt(2, startIndex);
	    	preStat.setInt(3, totalRecord);
	    	setRow.executeQuery();
			System.out.println("<<<<<<<<<<<<<preStat>>>>>>>>>>>>>"+preStat);
	    	rest = preStat.executeQuery();
	    	while(rest.next())
	    	{
	    		
	    		ConcernTrackingTO IssueTrackingtemp = new ConcernTrackingTO();
	    		IssueTrackingtemp.setIssueid(rest.getInt("issue_id"));
	    		IssueTrackingtemp.setUsername(rest.getString("user_first_name")+" "+rest.getString("user_last_name"));
	    		IssueTrackingtemp.setDesgnation(rest.getString("ug_title"));
	    		IssueTrackingtemp.setAnsdisc(rest.getString("reply_desc"));
	    		IssueTrackingtemp.setIssuerepydate(rest.getString("issue_reply_date"));
	    		IssueTrackingtemp.setStatus(rest.getString("status"));
	    		IssueTrackingtemp.setReplyid(rest.getInt("reply_id"));
	    		IssueTrackingtemp.setUserid(rest.getString("user_id"));
	    		IssueTrackingTO.add(IssueTrackingtemp);
	    		
	    	}
    		  
		}
	    catch (Exception exception) 
	    {
	    	System.out.println(" ERROR: " + exception.getMessage());
		}
	    finally {
	    	try {
	    		conn.close();
				rest.close();
				setRow.close();
		    	preStat.close();
		    	
			} catch (SQLException e) {

				e.printStackTrace();
			}
	    }
		return IssueTrackingTO;
	}

	public  int getTotalRecord(String SearchKey,String searchcategory,String searchpriority,String userid)
	{
		int totalRow = 0;
		conn =  conUtil.getConnection(); 
		try
		{
			//query.getTotalRecordfromsch_issuetracking
			userid = userid!=""?"in("+userid+")":"like "+"'%%'";
			SearchKey = SearchKey!=""?SearchKey:"%%";
	    	searchcategory = searchcategory!=""?searchcategory:"%%";
	    	searchpriority = searchpriority!=""?searchpriority:"%%";
			preStat = conn.prepareStatement("select count(*) noofrow" +
	    									" from sch_issue ni  join sch_issue_category nc on ni.cat_id = nc.cat_id join sch_users nu " +
	    									" on ni.creater_id = nu.user_id join sch_staff_heirarchy nsh " +
	    									" on nsh.user_id = ni.creater_id join sch_user_groups nug " +
	    									" on nsh.ug_id = nug.ug_id where nu.user_id "+userid+" and Upper(current_status) " +
	    									" like Upper('"+SearchKey+"') and Upper(Priority) like Upper('"+searchpriority+"') " +
	    									" and Upper(category) like Upper('"+searchcategory+"')  ");
			System.out.println("userid"+preStat);
			rest = preStat.executeQuery();
			if(rest.next())
			{
				totalRow = rest.getInt("noofrow");
			}
		}
		catch(Exception exp){exp.printStackTrace();}
		finally{
			try{
				preStat.close();
				rest.close();
				conn.close();
			}catch(Exception exp){
				exp.printStackTrace();
			}
		}

		return totalRow;
		
	}
	public  int getTotalRecordbyIssueId(String issue_id)
	{
		int totalRow = 0;
		conn =  conUtil.getConnection(); 
		try
		{
			preStat = conn.prepareStatement(query.getTotalRecordfromnal_issue_reply);
			preStat.setInt(1, Integer.valueOf(issue_id));
			rest = preStat.executeQuery();
			if(rest.next())
			{
				totalRow = rest.getInt("noofrow");
			}
		}
		catch(Exception exp){exp.printStackTrace();}
		finally{
			try{
				preStat.close();
				rest.close();
				conn.close();
			}catch(Exception exp){
				exp.printStackTrace();
			}
		}
		return totalRow;
		
	}
	
	public void saveIssue(ConcernTrackingTO objissueto)
	{
		conn =  getConnection(); 
		try
		{
			
			conn.setAutoCommit(false);
			preStat = conn.prepareStatement(query.savenal_issue);
			preStat.setString(1, objissueto.getIssuetitle());
			preStat.setString(2, objissueto.getIssuedesc());
			preStat.setString(3, objissueto.getPriority());
			//preStat.setString(4, objissueto.getIssuesubmiteddate());
			preStat.setInt(4, Integer.valueOf(objissueto.getCategory()));
			preStat.setString(5, objissueto.getUserid());
			preStat.setInt(6,1);
			preStat.executeUpdate();
			logger.debug("saveIssue "+preStat);
			conn.commit();
		
		}
		catch(Exception exp){try{conn.rollback();exp.printStackTrace();}catch(Exception exp1){exp1.printStackTrace();}}
		finally {try{conn.close();preStat.close();objissueto=null;}catch(Exception exp){}}
		
		
		
	}
	  public ConcernTrackingTO getConcernByissueId(String issueid)
	   {
		  ConcernTrackingTO concernToObj = new ConcernTrackingTO();
		  conn =  getConnection(); 
			try
			{
				preStat = conn.prepareStatement(query.getConcernDetailByissueid);
				preStat.setInt(1,Integer.valueOf(issueid));
				rsObj = preStat.executeQuery();
				logger.debug("getConcernByissueId Method "+preStat);
				if(rsObj.next())
				{
					
					concernToObj.setIssueid(rsObj.getInt("issue_id"));
					concernToObj.setIssuetitle(rsObj.getString("issue_title"));
					concernToObj.setIssuedesc(rsObj.getString("issue_desc"));
		    		concernToObj.setIssuesubmiteddate(rsObj.getString("issue_submited_date"));
		    		concernToObj.setPriority(rsObj.getString("priority"));
		    	 	concernToObj.setStatusid(rsObj.getInt("concern_status_id"));
		    	 	concernToObj.setCategory(rsObj.getString("category"));
		    	 	concernToObj.setReplydesc(rsObj.getString("reply_desc"));
		    	 	concernToObj.setStatus(rsObj.getString("concern_status"));
		    	 	concernToObj.setCatid(rsObj.getInt("cat_id"));
				}
			}
			catch(Exception exp){exp.printStackTrace();}
			finally{
				try{
					closeConnection(conn,callStm,preStat,rsObj);
				}catch(Exception exp){
					exp.printStackTrace();
				}
			}
		  return concernToObj;
	   }
	  
	  public void editIssueTrackingByIssueId(ConcernTrackingTO objissuetrackingto)
	  {
		    
		  conn =  conUtil.getConnection(); 
			try
			{
				preStat = conn.prepareStatement(query.updateIssueTrackingDataByissueid);
				preStat.setString(1,objissuetrackingto.getIssuetitle());
				preStat.setString(2,objissuetrackingto.getIssuedesc());
				preStat.setInt(3,Integer.valueOf(objissuetrackingto.getCategory()));
				preStat.setString(4,objissuetrackingto.getPriority());
				preStat.setString(5,objissuetrackingto.getReplydesc());
				preStat.setInt(6,objissuetrackingto.getIssueid());
				preStat.executeUpdate();
			}
			catch(Exception exp){exp.printStackTrace();}
			finally {
				try{
					conn.close();
					preStat.close();
					}
				catch(Exception exp){
					exp.printStackTrace();
				}}
		  
		  
	  }
	  public void deleteIssueTracking(String issueid)
	  {
		    
		  conn =  conUtil.getConnection(); 
			try
			{
				preStat = conn.prepareStatement(query.deleteissuetrackingybyissueid);
				preStat.setInt(1,Integer.valueOf(issueid));
				preStat.executeUpdate();
			}
			catch(Exception exp){exp.printStackTrace();}
			finally{
				try{
					preStat.close();					
					conn.close();
				}catch(Exception exp){
					exp.printStackTrace();
				}
			}
		  
		  
	  }
	  
	  public void deleteIssuereply(String reply_id)
	  {
		  conn =  conUtil.getConnection(); 
			try
			{
				preStat = conn.prepareStatement(query.deleteissuereply);
				preStat.setInt(1,Integer.valueOf(reply_id));
				preStat.executeUpdate();
			}
			catch(Exception exp){exp.printStackTrace();}
			finally{
				try{
					preStat.close();
					conn.close();
				}catch(Exception exp){
					exp.printStackTrace();
				}
			}
		  
		  
	  }
	  public void replyConcernByIssueId(ConcernTrackingTO replyObj)
	  {
		    
		  conn = getConnection(); 
			try
			{
				
				preStat = conn.prepareStatement(query.updateConcernToReply);
				preStat.setString(1,replyObj.getReplydesc());
				preStat.setString(2,replyObj.getStatus());
				preStat.setInt(3, replyObj.getIssueid());
				preStat.executeUpdate();
				logger.debug("replyConcernByIssueId Method "+preStat);
				
			}
			catch(Exception exp){exp.printStackTrace();}
			finally{
				try{
		    		closeConnection(conn,callStm,preStat,rsObj);
		    		logger.debug("Method replyConcernByIssueId :: Database Connection closed "+conn);
	
				}catch(Exception exp){
					exp.printStackTrace();
				}
			}
		  
	  }
	  public ConcernTrackingTO getDataByreplyId(String replyid)
	   {
		  ConcernTrackingTO objissuetrackingto = new ConcernTrackingTO();
		  conn =  conUtil.getConnection(); 
			try
			{
				preStat = conn.prepareStatement(query.getIssuereplyByreplyId);
				preStat.setInt(1,Integer.valueOf(replyid));
				rest = preStat.executeQuery();
				if(rest.next())
				{
					
					objissuetrackingto.setReplyid(rest.getInt("reply_id"));
					objissuetrackingto.setAnsdisc(rest.getString("reply_desc"));
					objissuetrackingto.setStatus(rest.getString("status"));
					objissuetrackingto.setIssueid(rest.getInt("issue_id"));
			
					
				}
			}
			catch(Exception exp){exp.printStackTrace();}
			finally{
				try{
					preStat.close();
					rest.close();
					conn.close();
				}catch(Exception exp){
					exp.printStackTrace();
				}
			}
		  return objissuetrackingto;
	   }
	 
	  public void editIssueReplyByIssueId(ConcernTrackingTO objissue)
	  {
		    
		  conn =  conUtil.getConnection(); 
			try
			{
		
				SimpleDateFormat sf = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss"); 
				String date = sf.format(new java.util.Date());
				preStat = conn.prepareStatement(query.updateissuereply);
				preStat.setString(1,objissue.getAnsdisc());
				preStat.setString(2,objissue.getStatus());
				//preStat.setString(3,date);
				preStat.setInt(3,Integer.valueOf(objissue.getReplyid()));
				preStat.executeUpdate();
				preStat = conn.prepareStatement(query.updatenalissuecurrentstatusbyissueid);
				preStat.setString(1, objissue.getStatus());
				preStat.setInt(2, objissue.getIssueid());
				preStat.executeUpdate();
				
			}
			catch(Exception exp){exp.printStackTrace();}
			finally{
				try{
					preStat.close();
					conn.close();
				}catch(Exception exp){
					exp.printStackTrace();
				}
			}
		  
	  }
	  
	  public String checkForExecutive(String userid)
	  {
		  conn =  conUtil.getConnection(); 
		  String ugid=null;
			try
			{
				preStat = conn.prepareStatement(query.checkForExecutive);
				preStat.setInt(1,Integer.valueOf(userid));
				System.out.println("<<<preStat>>>>"+preStat);
				rest = preStat.executeQuery();
				if(rest.next())
				{
					ugid = ""+rest.getInt("parent_ug_id");
				}
			}
			catch(Exception exp){exp.printStackTrace();}
			finally{
				try{
					preStat.close();
					rest.close();
					conn.close();
				}catch(Exception exp){
					exp.printStackTrace();
				}
			}
		  return ugid;
	   }
	  public ArrayList<ConcernTrackingTO> getAllCategory() 
	  {
		  ArrayList<ConcernTrackingTO> objissuetracking = new ArrayList<ConcernTrackingTO>();
		  conn = conUtil.getConnection();
		  try {
			  preStat = conn.prepareStatement(query.getAllCategory);
			  rest = preStat.executeQuery();
			  while(rest.next())
			  {
				  ConcernTrackingTO objissue = new ConcernTrackingTO();
				  objissue.setCatid(rest.getInt("cat_id"));
				  objissue.setCategory(rest.getString("category"));
				  objissuetracking.add(objissue);
			  }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		finally{
			try{
				preStat.close();
				rest.close();
				conn.close();
			}catch(Exception exp){
				exp.printStackTrace();
			}
		}
		  return objissuetracking;
	  }
	  
		public  ArrayList<ConcernTrackingTO> getConcernDetails(int totalRecord,int startIndex,String sort,String dir,String SearchKey,String searchstatuskey,String searchCategory,String searchprioritykey) 
		{
		   	ArrayList<ConcernTrackingTO> studentObj = new ArrayList<ConcernTrackingTO>();
		    try { 
		    	conn = getConnection();
		    	logger.debug("Method getStudentDetails :: Database Connection opened");
		    	callStm = conn.prepareCall("{call sp_concern_detail_list(?,?,?,?,?,?,?,?)}");	
		    	callStm.setInt(1,totalRecord);
		    	callStm.setInt(2,startIndex);
		    	callStm.setString(3,sort);
		    	callStm.setString(4,dir);
		    	callStm.setString(5,SearchKey);
		    	callStm.setString(6,searchstatuskey);
		    	callStm.setString(7,searchCategory);
		    	callStm.setString(8,searchprioritykey);
		    	callStm.execute();
		    	rsObj = callStm.getResultSet();
		    	logger.debug("Method getStudentDetails :: Procedure Executed==>"+callStm);
	    		while(rsObj.next())	
		    	{
	    			
	    			ConcernTrackingTO studentTOtemp = new ConcernTrackingTO();
	    			studentTOtemp.setIssueid(rsObj.getInt("issue_id"));
		    		studentTOtemp.setIssuetitle(rsObj.getString("issue_title"));
		    		studentTOtemp.setIssuesubmiteddate(rsObj.getString("issue_submited_date"));
		    	 	studentTOtemp.setPriority(rsObj.getString("priority"));
		    	 	studentTOtemp.setStatus(rsObj.getString("concern_status"));
		    	 	studentTOtemp.setCategory(rsObj.getString("category"));
		    	 	studentObj.add(studentTOtemp);
		    	}
	    		  
			}
		    catch (Exception exception)
		    {
		    	exception.printStackTrace();
		    	logger.debug("ERROR:: "+exception.getMessage());
		    }
		    finally 
		    {
		    	try{	
		    		closeConnection(conn,callStm,preStat,rsObj);
		    		logger.debug("Method getStudentDetails :: Database Connection closed "+conn);
		    	} catch (SQLException e) 
		    	{
		    		e.printStackTrace();
		    	}
		    }
			return studentObj;
		}

		public  ArrayList<ConcernTrackingTO> getConcernStatus() 
		{
		 
			  ArrayList<ConcernTrackingTO> arrayConcernObj = new ArrayList<ConcernTrackingTO>();
			  conn =  getConnection(); 
				try
				{
					preStat = conn.prepareStatement(query.getConcernStatus);
					rsObj = preStat.executeQuery();
					logger.debug("getConcernByissueId Method "+preStat);
					while(rsObj.next())
					{
						
						ConcernTrackingTO concernToObj = new ConcernTrackingTO();
						concernToObj.setStatusid(rsObj.getInt("concern_status_id"));
						concernToObj.setStatus(rsObj.getString("concern_status"));
						arrayConcernObj.add(concernToObj);
			    	 						
					}
				}
		    catch (Exception exception)
		    {
		    	exception.printStackTrace();
		    	logger.debug("ERROR:: "+exception.getMessage());
		    }
		    finally 
		    {
		    	try{	
		    		closeConnection(conn,callStm,preStat,rsObj);
		    		logger.debug("Method getStudentDetails :: Database Connection closed "+conn);
		    	} catch (SQLException e) 
		    	{
		    		e.printStackTrace();
		    	}
		    }
			return arrayConcernObj;
		}

}
