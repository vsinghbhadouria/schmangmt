package com.sch.dao;

import java.sql.*;
import java.util.Properties;
import java.io.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
public class ConnectionUtil 
{
	protected Connection conn = null;
	public static Logger logger = Logger.getLogger(ConnectionUtil.class);
   public Connection getConnection()
   {
	   logger.debug("Database Connection Status..........."+conn);
   try
	{
	   if(conn==null){
		
		  logger.debug("Database Connection is in process...........");	
		  Context initContext = new InitialContext();
	      Context envContext = (Context) initContext.lookup("java:/comp/env");
	      DataSource ds = (DataSource) envContext.lookup("jdbc/SCMDB");
	      conn = ds.getConnection();
	      logger.debug("database connection opend");
		
		/*Properties prop = new Properties();
		prop.load(new FileInputStream("F:/Dev/workspace/schmangmt/dbcon.properties"));
		String driver = prop.getProperty("databaseDriver");
		String databaseUrl =  prop.getProperty("databaseUrl");
		String databasePort = prop.getProperty("databasePort");
		String databaseName = prop.getProperty("databaseName");
		String userName = prop.getProperty("userName");
		String Password = prop.getProperty("Password");
		Class.forName(driver);
		logger.debug("Driver loaded.....");
		conn = DriverManager.getConnection(databaseUrl+":"+databasePort+"/"+databaseName,userName, Password);
		*/
		}
		
	}
	catch(Exception exp)
	{
		logger.debug("Error to open database connection..........."+exp.getMessage()); 
		exp.printStackTrace();
	}   
	return conn ;   
	
   }
   
   public void closeConnection(Connection conn,CallableStatement callStm,PreparedStatement preStat,ResultSet rsObj) throws SQLException 
   {
	  logger.debug("Database Connection released..........."); 
	  try
	  {
		  if(conn!=null){
			  conn.close();
		      conn=null;
		      this.conn=null;
		  }
		  if(callStm!=null)
			  callStm.close();
		  if(preStat!=null)
			  preStat.close();
		  if(rsObj!=null)
			  rsObj.close();
		  
	  }
	  catch(SQLException exp)
	  {
		  logger.debug("Error to close database connection..........."+exp.getMessage()); 
		  exp.getMessage();  
		  throw exp;
		
	  }
	   
   }
   
}
