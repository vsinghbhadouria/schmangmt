package com.sch.dao;

import java.sql.*;
import java.util.ArrayList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

import com.sch.delegates.EduExitManager;
import com.sch.form.EduExitForm;
import com.sch.to.EduExitTO;


public class EduExitDAO extends ConnectionUtil

{
	Query query =null;
    ResultSet rsObj = null; 
    PreparedStatement preStat=null;	
    CallableStatement callStm = null;
    Logger logger = Logger.getLogger(EduExitDAO.class);
	public EduExitDAO()
	{
		 query = new Query();  
	}
	

	public  EduExitTO downloadCertificate(EduExitTO eduExitTOObj)
	{
    	EduExitTO EduExitTOOBJ = new EduExitTO();
		try
		{
			getConnection();
			logger.debug("Method downloadCertificate :: Database Connection opened");
			conn.setAutoCommit(false);
			callStm = conn.prepareCall("{call exitformData_sp(?)}");	
			callStm.setString(1,eduExitTOObj.getStudentId());
			logger.debug("Method downloadCertificate :: Procedure Executed==>"+callStm);
	    	callStm.execute();
	    	rsObj = callStm.getResultSet();

	    	if(rsObj.next())
	    	{
	    		EduExitTOOBJ.setFirstname(rsObj.getString("first_name"));
	    		EduExitTOOBJ.setMiddlename(rsObj.getString("middle_name"));
	    		EduExitTOOBJ.setLastname(rsObj.getString("last_name"));
	    		EduExitTOOBJ.setAdmission_date(rsObj.getString("admission_date"));
	    		EduExitTOOBJ.setFathername(rsObj.getString("father_name"));
	    		EduExitTOOBJ.setAddress(rsObj.getString("address"));
	    		EduExitTOOBJ.setClassname(rsObj.getString("class_name"));
	    		EduExitTOOBJ.setStudentId(rsObj.getString("student_id"));
	    	}
			conn.commit();
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method downloadCertificate :: ERROR==>"+exp.getMessage());
			try {
				conn.rollback();
				logger.debug("Method downloadCertificate :: Transaction rollbacked");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method downloadCertificate :: Database Connection closed");
		}catch(Exception exp){
			exp.printStackTrace();
		}}
	
	return EduExitTOOBJ;
}
}