package com.sch.dao;
import java.sql.*;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import com.sch.constant.PortableConstant;
import com.sch.form.EduStudentForm;
import com.sch.to.*;


public class EduStudentDAO extends ConnectionUtil

{
	Query query =null;
    ResultSet rsObj = null; 
    PreparedStatement preStat=null;	
    CallableStatement callStm = null;
	ArrayList<EduStudentTO> arraylistobj = new ArrayList<EduStudentTO>();
    Logger logger  = Logger.getLogger(EduStudentDAO.class);
	public EduStudentDAO()
	{
		 query = new Query();  
	}
	
	public  ArrayList<EduStudentTO> getStudentDetails(int totalRecord,int startIndex,String sort,String dir,String SearchKey,String searchBysessionkey,String searchByclasskey) 
	{
	   	ArrayList<EduStudentTO> studentObj = new ArrayList<EduStudentTO>();
	    try { 
	    	getConnection();
	    	logger.debug("Method getStudentDetails :: Database Connection opened");
	    	callStm = conn.prepareCall("{call sp_student_detail_list(?,?,?,?,?,?,?)}");	
	    	callStm.setInt(1,totalRecord);
	    	callStm.setInt(2,startIndex);
	    	callStm.setString(3,sort);
	    	callStm.setString(4,dir);
	    	callStm.setString(5,SearchKey);
	    	callStm.setString(6,searchBysessionkey);
	    	callStm.setString(7,searchByclasskey);
	    	callStm.execute();
	    	rsObj = callStm.getResultSet();
	    	logger.debug("Method getStudentDetails :: Procedure Executed==>"+callStm);
    		while(rsObj.next())	
	    	{
    			
    			EduStudentTO studentTOtemp = new EduStudentTO();
    			studentTOtemp.setFirstname(rsObj.getString("first_name"));
	    		studentTOtemp.setLastname(rsObj.getString("last_name"));
	    		studentTOtemp.setPhoneno(rsObj.getString("contact_no"));
	    	 	studentTOtemp.setEmailid(rsObj.getString("email_id"));
	    	 	studentTOtemp.setStudentid(rsObj.getInt("student_id"));
	    	 	studentTOtemp.setClassname(rsObj.getString("class_name"));
	    	 	studentTOtemp.setSection(rsObj.getString("section"));
	    	 	studentTOtemp.setSession(rsObj.getString("session_year"));
	    	 	studentTOtemp.setSessionId(rsObj.getInt("session_id"));
	    	 	studentObj.add(studentTOtemp);
	    	 	
	    	}
    		  
		}
	    catch (Exception exception)
	    {
	    	exception.printStackTrace();
	    	logger.debug("ERROR:: "+exception.getMessage());
	    }
	    finally 
	    {
	    	try{	
	    		closeConnection(conn,callStm,preStat,rsObj);
	    		logger.debug("Method getStudentDetails :: Database Connection closed "+conn);
	    	} catch (SQLException e) 
	    	{
	    		e.printStackTrace();
	    	}
	    }
	  //  test();
		return studentObj;
	}

	public  EduStudentTO saveEduStudentData(EduStudentTO EduStudentTOObj,EduStudentForm eduStudentFormObj)
	{
		EduStudentTO eduStudentTOobj=null;
		PortableConstant PortableConstantObj = new PortableConstant();
		String partofstudentId=PortableConstantObj.getProperty(PortableConstant.PARTOFSTUDENTID);
		String partofparentId=PortableConstantObj.getProperty(PortableConstant.PARTOFPARENTID);
		Integer maximumStudentHeadCountAllowed=Integer.valueOf(PortableConstantObj.getProperty(PortableConstant.maximumStudentHeadCountAllowed));
		String studentMaxMsg=PortableConstantObj.getProperty(PortableConstant.STUDENTMAXMSG);
		try
		{
			getConnection();
			logger.debug("Method saveEduStudentData :: Database Connection opened");
			conn.setAutoCommit(false);
		    System.out.println("Before call of insert_student_detail_sp method"+EduStudentTOObj.getBirthday()+" "+EduStudentTOObj.getBirthmonth()+" "+EduStudentTOObj.getBirthyear());
			callStm = conn.prepareCall("{call insert_student_detail_sp(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");	
			callStm.setString(1,EduStudentTOObj.getFirstname());
			callStm.setString(2,EduStudentTOObj.getMiddlename());
			callStm.setString(3,EduStudentTOObj.getLastname());
			callStm.setString(4,EduStudentTOObj.getPassword());
			callStm.setString(5,EduStudentTOObj.getEmailid());
			callStm.setString(6,EduStudentTOObj.getAdmissiondate());
			callStm.setString(7,EduStudentTOObj.getAddress());
			callStm.setString(8,EduStudentTOObj.getPhoneno());
			callStm.setString(9,"A");
			callStm.setString(10,EduStudentTOObj.getPreviousinstitute());
			callStm.setString(11,EduStudentTOObj.getGender());
			callStm.setString(12,EduStudentTOObj.getFathername());
			callStm.setString(13,EduStudentTOObj.getMothername());
			callStm.setString(14,EduStudentTOObj.getParentqualification());
			callStm.setString(15,EduStudentTOObj.getParentemailid());
			callStm.setString(16,EduStudentTOObj.getParentpassword());
			callStm.setInt(17,EduStudentTOObj.getClassid());
			callStm.setString(18,EduStudentTOObj.getStudprequalification());
			callStm.setInt(19,EduStudentTOObj.getAdmissionfee());
			callStm.setInt(20,EduStudentTOObj.getConcessionamt());
			callStm.setString(21,EduStudentTOObj.getComment());
			callStm.setString(22,EduStudentTOObj.getDateofbirth());
			callStm.setInt(23,EduStudentTOObj.getBirthday());
			callStm.setInt(24,EduStudentTOObj.getBirthmonth());
			callStm.setInt(25,EduStudentTOObj.getBirthyear());
			callStm.setInt(26,EduStudentTOObj.getDept_id());
			callStm.setString(27,partofstudentId);
			callStm.setString(28,partofparentId);
			callStm.setInt(29,EduStudentTOObj.getSessionId());
			logger.debug("Method saveEduStudentData :: Procedure Executed==>"+callStm);
			callStm.executeUpdate();
			if(eduStudentFormObj.getAssignsubjected()!=null && eduStudentFormObj.getAssignsubjected().length!=0)
		    {
		    	preStat = conn.prepareStatement("select max(student_id) student_id from sch_student_detail");
				rsObj = preStat.executeQuery();
				int studentid=-1;
				while(rsObj.next()){
					studentid =(rsObj.getInt("student_id"));
					if(studentid>maximumStudentHeadCountAllowed){
						throw new Exception("Student license count exceeded");
					}
					
				}
					for(int xx=0;xx<eduStudentFormObj.getAssignsubjected().length;xx++)
					{
					    preStat = conn.prepareStatement(query.insertSubjectStudentid);
					    preStat.setInt(1, studentid);
					    preStat.setString(2, eduStudentFormObj.getAssignsubjected()[xx]);
					    preStat.setInt(3, EduStudentTOObj.getSessionId());
					    preStat.setInt(4, EduStudentTOObj.getClassid());
					    logger.debug("Method saveEduStudentData :: Query Executed for assigned classess==>"+preStat);
					    preStat.executeUpdate();
				    
				}
		    }
			 preStat = conn.prepareStatement("select ssd.student_id,first_name,middle_name,last_name,ssd.user_id,ssd.password,ssd.email_id,parent_id,father_name,spd.user_id parentuser,spd.password parentpassword,spd.email_id parentemailid from sch_student_detail ssd,sch_parent_detail spd  where ssd.student_id=spd.student_id and ssd.student_id =(select max(student_id) from sch_student_detail)");
			 rsObj = preStat.executeQuery();
			if(rsObj.next()){
				logger.debug("Method saveEduStaffData :: Before getting the Login Info");
				eduStudentTOobj = new EduStudentTO(rsObj.getInt("student_id"),rsObj.getString("first_name"),rsObj.getString("middle_name"),rsObj.getString("last_name"),rsObj.getString("user_id"),rsObj.getString("password"),rsObj.getString("email_id"),rsObj.getInt("parent_id"),rsObj.getString("father_name"),rsObj.getString("parentuser"),rsObj.getString("parentpassword"),rsObj.getString("parentemailid"));
			}
		    conn.commit();
		 
		}
		catch(Exception exp)
		{
			logger.debug("Method saveEduStudentData :: ERROR==>"+exp.getMessage());
			if(exp.getMessage().equals("Student license count exceeded")){
			EduStudentTOObj.setStudentMaxMsg(studentMaxMsg);
			}
			try {
				conn.rollback();
				logger.debug("Method saveEduStudentData :: Transaction rollbacked");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method saveEduStudentData :: Database Connection closed");
		}catch(Exception exp){
			exp.printStackTrace();
		}}
	if(eduStudentTOobj!=null){	
	return eduStudentTOobj;	
	}
	else{
		return EduStudentTOObj;
	}
	}

	public  void updateStudentData(EduStudentTO EduStudentTOObj,EduStudentForm eduStudentFormObj)
	{
		CallableStatement callStm = null;
		try
		{
			
			getConnection();
			logger.debug("Method updateStudentData :: Database Connection opened");
			conn.setAutoCommit(false);
			callStm = conn.prepareCall("{call update_student_detail_sp(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			callStm.setString(1,EduStudentTOObj.getFirstname());
			callStm.setString(2,EduStudentTOObj.getMiddlename());
			callStm.setString(3,EduStudentTOObj.getLastname());
			callStm.setString(4,EduStudentTOObj.getEmailid());
			callStm.setString(5,EduStudentTOObj.getAdmissiondate());
			callStm.setString(6,EduStudentTOObj.getAddress());
			callStm.setString(7,EduStudentTOObj.getPhoneno());
			callStm.setString(8,EduStudentTOObj.getPreviousinstitute());
			callStm.setString(9,EduStudentTOObj.getGender());
			callStm.setString(10,EduStudentTOObj.getFathername());
			callStm.setString(11,EduStudentTOObj.getMothername());
			callStm.setString(12,EduStudentTOObj.getParentqualification());
			callStm.setString(13,EduStudentTOObj.getParentemailid());
			callStm.setInt(14,Integer.valueOf(EduStudentTOObj.getEduclass()));
			callStm.setString(15,EduStudentTOObj.getStudprequalification());
			callStm.setInt(16,EduStudentTOObj.getAdmissionfee());
			callStm.setInt(17,EduStudentTOObj.getConcessionamt());
			callStm.setString(18,EduStudentTOObj.getComment());
			callStm.setString(19,EduStudentTOObj.getDateofbirth());
			callStm.setInt(20,EduStudentTOObj.getBirthday());
			callStm.setInt(21,EduStudentTOObj.getBirthmonth());
			callStm.setInt(22,EduStudentTOObj.getBirthyear());
			callStm.setInt(23,EduStudentTOObj.getStudentid());
			callStm.setInt(24,EduStudentTOObj.getDept_id());
			logger.debug("Method updateStudentData :: Procedure Executed==>"+callStm);
			callStm.executeUpdate();
			 if(eduStudentFormObj.getAssignsubjected()!=null && eduStudentFormObj.getAssignsubjected().length!=0)
			    {
			    	for(int xx=0;xx<eduStudentFormObj.getAssignsubjected().length;xx++)
						{
					    preStat = conn.prepareStatement(query.insertSubjectStudentid);
					    preStat.setInt(1, EduStudentTOObj.getStudentid());
					    preStat.setString(2, eduStudentFormObj.getAssignsubjected()[xx]);
					    preStat.setInt(3, EduStudentTOObj.getSessionId());
					    preStat.setInt(4, EduStudentTOObj.getClassid());
					    preStat.executeUpdate();
					}
			    }
			conn.commit();
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method updateStudentData :: ERROR==>"+exp.getMessage());
			try {
				conn.rollback();
				logger.debug("Method updateStudentData :: Transaction rollbacked");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method updateStudentData :: Database Connection closed");
		}catch(Exception exp){
			exp.printStackTrace();
		}}
		
	}
public  ArrayList<EduStudentTO> getStudentQualification()
{
	 ArrayList<EduStudentTO> eduStudentTOArrayObj=new  ArrayList<EduStudentTO>();
	try
	{
		getConnection();
		logger.debug("Method getStudentQualification :: Database Connection opened");
		preStat = conn.prepareStatement(query.getStudentPrequalification);	   
		logger.debug("Method getStudentQualification :: Query Executed==>"+preStat);
		rsObj = preStat.executeQuery();	
		 while(rsObj.next())
		    {
			 EduStudentTO eduStudentTOObj =new EduStudentTO();
			 eduStudentTOObj.setStudprequalification_title(rsObj.getString("qualification_title"));
			 eduStudentTOObj.setStudprequalification_id(rsObj.getInt("qualification_id"));
			 eduStudentTOArrayObj.add(eduStudentTOObj); 	
		    }
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		logger.debug("Method getStudentQualification :: ERROR==>"+exp.getMessage());
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getStudentQualification :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}
	return eduStudentTOArrayObj;
	
}
public  ArrayList<EduStudentTO> getParentHigherQualification()
{
	 ArrayList<EduStudentTO> eduStudentTOArrayObj=new  ArrayList<EduStudentTO>();
	try
	{
		getConnection();
		logger.debug("Method getParentHigherQualification :: Database Connection opened");
		preStat = conn.prepareStatement(query.getStaffqualification);	 
		logger.debug("Method getParentHigherQualification :: Query Executed==>"+preStat);
		rsObj = preStat.executeQuery();	
		 while(rsObj.next())
		    {
			 EduStudentTO eduStudentTOObj =new EduStudentTO();
			 eduStudentTOObj.setParentqualification_title(rsObj.getString("qualification_title"));
			 eduStudentTOObj.setParentqualification_id(rsObj.getInt("qualification_id"));
		     eduStudentTOArrayObj.add(eduStudentTOObj); 	
		    }
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		logger.debug("Method getParentHigherQualification :: ERROR==>"+exp.getMessage());
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getParentHigherQualification :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}
	return eduStudentTOArrayObj;
	
}


public ArrayList<EduStudentTO> getClassinfo(String userId) {

	ArrayList<EduStudentTO> users = new ArrayList<EduStudentTO>();
	
	try{
		getConnection();
		logger.debug("Method getClassinfo :: Database Connection opened");
		preStat = conn.prepareStatement(query.getClassInfobyUserid);
		preStat.setString(1, userId);
		logger.debug("Method getClassinfo :: Query Executed==>"+preStat);
		rsObj=preStat.executeQuery();
		while(rsObj.next())
		{
		EduStudentTO edustudentObj = new EduStudentTO();
		edustudentObj.setClassid(rsObj.getInt("class_id"));
		edustudentObj.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
		users.add(edustudentObj);
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getClassinfo :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getClassinfo :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}

public ArrayList<EduStudentTO> getClassinfo() {

	ArrayList<EduStudentTO> users = new ArrayList<EduStudentTO>();
	
	try{
		getConnection();
		logger.debug("Method getClassinfo :: Database Connection opened");
		preStat = conn.prepareStatement(query.getClassInfo);
		logger.debug("Method getClassinfo :: Query Executed==>"+preStat);
		rsObj=preStat.executeQuery();
		while(rsObj.next())
		{
		EduStudentTO edustudentObj = new EduStudentTO();
		edustudentObj.setClassid(rsObj.getInt("class_id"));
		edustudentObj.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
		users.add(edustudentObj);
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getClassinfo :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getClassinfo :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}
public ArrayList<EduStudentTO> getAdmissionfees() {

	ArrayList<EduStudentTO> users = new ArrayList<EduStudentTO>();
	
	try{
		getConnection();
		logger.debug("Method getAdmissionfees :: Database Connection opened");
		preStat = conn.prepareStatement(query.getAdmissionfee);
		logger.debug("Method getAdmissionfees :: Query Executed==>"+preStat);
		rsObj=preStat.executeQuery();
		while(rsObj.next())
		{
		EduStudentTO edustudentObj = new EduStudentTO();
		edustudentObj.setClassid(rsObj.getInt("class_id"));
		edustudentObj.setAdmissionfee(rsObj.getInt("admission_fee"));
		users.add(edustudentObj);
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getAdmissionfees :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getAdmissionfees :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}

public Integer getAdmissionfee(Integer classid) {

	Integer fee=null;
	try{
		
		getConnection();
		logger.debug("Method getAdmissionfee :: Database Connection opened");
		preStat = conn.prepareStatement(query.getAdmfee);
		preStat.setInt(1,classid);
		logger.debug("Method getAdmissionfee :: Query Executed==>"+preStat);
		rsObj=preStat.executeQuery();
		while(rsObj.next())
		{
			fee = rsObj.getInt("admission_fee");
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getAdmissionfee :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getAdmissionfee :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return fee;
}

/*
public ArrayList<EduStudentTO> getUnAssoClassInfo(String userid) {

	ArrayList<EduStudentTO> users = new ArrayList<EduStudentTO>();
	try{
		getConnection();
		preStat = conn.prepareStatement(query.getUnAssoclassbyUserid);
		preStat.setString(1, userid);
		rsObj=preStat.executeQuery();
		while(rsObj.next())
		{
		EduStudentTO edustudentObj = new EduStudentTO();
		edustudentObj.setClassid(rsObj.getInt("class_id"));
		edustudentObj.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
		users.add(edustudentObj);
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}
*/
public EduStudentTO getEduStudentDatabyUserId(String user_Id,String session_Id)
{
	EduStudentTO EduStudentTOObj = new EduStudentTO();
	try
	{    
		getConnection();
		logger.debug("Method getEduStudentDatabyUserId :: Database Connection opened");
		callStm = conn.prepareCall("{call get_studentDetailBySession_sp(?,?)}");
		callStm.setInt(1,Integer.valueOf(user_Id));
		callStm.setInt(2,Integer.valueOf(session_Id));
		logger.debug("Method getEduStudentDatabyUserId :: Procedure Executed==>"+callStm);
		rsObj = callStm.executeQuery();
		if(rsObj.next())
		{   
			EduStudentTOObj.setFirstname(rsObj.getString("first_name"));
			EduStudentTOObj.setMiddlename(rsObj.getString("middle_name"));
			EduStudentTOObj.setLastname(rsObj.getString("last_name"));
			EduStudentTOObj.setPhoneno(rsObj.getString("contact_no"));
			EduStudentTOObj.setAddress(rsObj.getString("address"));
			EduStudentTOObj.setStudprequalification_id(rsObj.getInt("preinstclass_id"));
			EduStudentTOObj.setEmailid(rsObj.getString("email_id"));
			EduStudentTOObj.setStudentid(rsObj.getInt("student_id"));
			EduStudentTOObj.setAdmissiondate(rsObj.getString("admission_date"));
			EduStudentTOObj.setPreviousinstitute(rsObj.getString("previous_edu_center"));
			EduStudentTOObj.setGender(rsObj.getString("gender"));
			EduStudentTOObj.setAdmissionfee(rsObj.getInt("admission_fee"));
			EduStudentTOObj.setConcessionamt(rsObj.getInt("concession_amount"));
			EduStudentTOObj.setComment(rsObj.getString("comment"));
			EduStudentTOObj.setClassid(rsObj.getInt("class_id"));
			EduStudentTOObj.setFathername(rsObj.getString("father_name"));
			EduStudentTOObj.setMothername(rsObj.getString("mother_name"));
			EduStudentTOObj.setParentqualification_id(rsObj.getInt("father_higher_qualification_id"));
			EduStudentTOObj.setParentemailid(rsObj.getString("parentemail_id"));
			EduStudentTOObj.setDateofbirth(rsObj.getString("dateofbirth"));
			EduStudentTOObj.setBirthday(rsObj.getInt("birth_day"));
			EduStudentTOObj.setBirthmonth(rsObj.getInt("birth_month"));
			EduStudentTOObj.setBirthyear(rsObj.getInt("birth_year"));
			EduStudentTOObj.setDept_id(rsObj.getInt("dept_id"));
			
		}
		
	}
	catch(Exception exp){exp.printStackTrace();
	logger.debug("Method getEduStudentDatabyUserId :: ERROR==>"+exp.getMessage());
	}
	finally{
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getEduStudentDatabyUserId :: Database Connection closed");
			}catch(Exception exp){exp.printStackTrace();}}
	return EduStudentTOObj;
}

public EduStudentTO viewEduStudentDatabyUserId(String user_Id,String session_Id,String stud_flag)
{
	EduStudentTO EduStudentTOObj = new EduStudentTO();
	try
	{    
		getConnection();
		logger.debug("Method viewEduStudentDatabyUserId :: Database Connection opened");
		callStm = conn.prepareCall("{call view_studentDetailBySession_sp(?,?,?)}");
		callStm.setInt(1,Integer.valueOf(user_Id));
		callStm.setInt(2,Integer.valueOf(session_Id));
		callStm.setString(3,stud_flag);
		logger.debug("Method viewEduStudentDatabyUserId :: Query Executed==>"+callStm);
		rsObj = callStm.executeQuery();
		if(rsObj.next())
		{   
			System.out.println("student"+rsObj.getString("student_prev_qualification")+"parent"+rsObj.getString("parent_higher_qualification")+"class"+rsObj.getString("class_name"));
			EduStudentTOObj.setFirstname(rsObj.getString("first_name"));
			EduStudentTOObj.setMiddlename(rsObj.getString("middle_name"));
			EduStudentTOObj.setLastname(rsObj.getString("last_name"));
			EduStudentTOObj.setPhoneno(rsObj.getString("contact_no"));
			EduStudentTOObj.setAddress(rsObj.getString("address"));
			EduStudentTOObj.setStudprequalification_title(rsObj.getString("student_prev_qualification"));
			EduStudentTOObj.setEmailid(rsObj.getString("email_id"));
			EduStudentTOObj.setStudentid(rsObj.getInt("student_id"));
			EduStudentTOObj.setAdmissiondate(rsObj.getString("admission_date"));
			EduStudentTOObj.setPreviousinstitute(rsObj.getString("previous_edu_center"));
			EduStudentTOObj.setGender(rsObj.getString("gender"));
			EduStudentTOObj.setAdmissionfee(rsObj.getInt("admission_fee"));
			EduStudentTOObj.setConcessionamt(rsObj.getInt("concession_amount"));
			EduStudentTOObj.setComment(rsObj.getString("comment"));
			EduStudentTOObj.setClassname(rsObj.getString("class_name"));
			EduStudentTOObj.setFathername(rsObj.getString("father_name"));
			EduStudentTOObj.setMothername(rsObj.getString("mother_name"));
			EduStudentTOObj.setParentqualification_title(rsObj.getString("parent_higher_qualification"));
			EduStudentTOObj.setParentemailid(rsObj.getString("parentemail_id"));
			EduStudentTOObj.setDateofbirth(rsObj.getString("dateofbirth"));
			EduStudentTOObj.setBirthday(rsObj.getInt("birth_day"));
			EduStudentTOObj.setBirthmonth(rsObj.getInt("birth_month"));
			EduStudentTOObj.setBirthyear(rsObj.getInt("birth_year"));
			
			System.out.println("student"+EduStudentTOObj.getStudprequalification_title()+"parent"+EduStudentTOObj.getParentqualification_title()+"class"+EduStudentTOObj.getClassname());
			
		}
		
	}
	catch(Exception exp){exp.printStackTrace();
	logger.debug("Method viewEduStudentDatabyUserId :: ERROR==>"+exp.getMessage());
	}
	finally{
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method viewEduStudentDatabyUserId :: Database Connection closed");
			}catch(Exception exp){exp.printStackTrace();}}
	return EduStudentTOObj;
}
	public void deleteStudentDetailbyUserId(String student_Id)
	{
		try
		{
			getConnection();
			logger.debug("Method deleteStudentDetailbyUserId :: Database Connection opened");
			preStat = conn.prepareStatement(query.deleteStudentDetailbyUserId);
			preStat.setString(1,student_Id);
			logger.debug("Method deleteStudentDetailbyUserId :: Query Executed==>"+preStat);
			preStat.executeUpdate();
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method deleteStudentDetailbyUserId :: ERROR==>"+exp.getMessage());
		}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method deleteRegistrationDetailbyUserId :: Database Connection closed");
				}catch(Exception exp){exp.printStackTrace();}}
	}
	
	 public ArrayList<String> getSession()
	  {
		 ArrayList<String> sessioninfo = new ArrayList<String>();
		 sessioninfo.add(""+","+"<-----Session----->");
		 try{
		 getConnection();
		 logger.debug("Method getSession :: Database Connection opened");
		 preStat = conn.prepareStatement(query.getSession);
		 logger.debug("Method getSession :: Query Executed==>"+preStat);
		 rsObj = preStat.executeQuery();
			while(rsObj.next()){
				sessioninfo.add(rsObj.getInt("session_id")+","+rsObj.getString("session_year"));
			}
		 }	
			catch(Exception exp)
			{
				exp.printStackTrace();
				logger.debug("Method getSession :: ERROR==>"+exp.getMessage());
			}
			finally {
				try{
					closeConnection(conn,callStm,preStat,rsObj);
					logger.debug("Method getSession :: Database Connection closed");
					}catch(Exception exp){exp.printStackTrace();}}
		 return sessioninfo;
		 
	  }
	 
	 public  ArrayList<EduStudentTO> getDepartment()
	 {
	 	 ArrayList<EduStudentTO> studentObj=new  ArrayList<EduStudentTO>();
	 	try
	 	{
	 		getConnection();
	 		preStat = conn.prepareStatement(query.getDepartment);
	 		logger.debug("Method getDepartment :: Database Connection opened");
	 		preStat.setString(1,"Y");
	 		logger.debug("Method getDepartment :: Query Executed==>"+preStat);
	 		preStat.execute();	
	 		rsObj = preStat.getResultSet();
	 		 while(rsObj.next())
	 		    {
	 			EduStudentTO EduStudentTOObj =new EduStudentTO();
	 			EduStudentTOObj.setDept_title(rsObj.getString("dept_name"));
	 			EduStudentTOObj.setDept_id(rsObj.getInt("dept_id"));
	 			 studentObj.add(EduStudentTOObj); 	
	 		    }
	 	}
	 	catch(Exception exp)
	 	{
	 		exp.printStackTrace();
	 		logger.debug("Method getDepartment :: ERROR==>"+exp.getMessage());
	 	}
	 	finally {
	 		try{
	 			closeConnection(conn,callStm,preStat,rsObj);
	 			logger.debug("Method getDepartment :: Database Connection closed");
	 			}catch(Exception exp){
	 				exp.printStackTrace();
	 			}}
	 	return studentObj;
	 	
	 }
	 
	 public ArrayList<EduStudentTO> getSubjectByClass(String dept_id,String class_id) {

			ArrayList<EduStudentTO> users = new ArrayList<EduStudentTO>();
			    System.out.println("dept_id"+dept_id);
			try{
				getConnection();
				logger.debug("Method getSubjectByClass :: Database Connection closed");
				callStm = conn.prepareCall("{call getSubjectByClass_sp(?,?)}");
				callStm.setString(1, dept_id);
				callStm.setString(2, class_id);
				logger.debug("Method getSubjectByClass :: Procedure Executed==>"+callStm);
				rsObj=callStm.executeQuery();
				while(rsObj.next())
				{
					EduStudentTO EduStudentTOObj = new EduStudentTO();
					EduStudentTOObj.setClassid(rsObj.getInt("subject_id"));
					EduStudentTOObj.setClassname(rsObj.getString("subject_name"));
					users.add(EduStudentTOObj);
				}
			}catch (Exception exp) {
				exp.printStackTrace();
				logger.debug("Method getSubjectByClass :: ERROR==>"+exp.getMessage());
			}
			finally{
				try {
					closeConnection(conn,callStm,preStat,rsObj);
					logger.debug("Method getSubjectByClass :: Database Connection closed");
					} 
			catch (Exception exp) {exp.printStackTrace();}}
			return users;
		}
	 
	 public ArrayList<EduStudentTO> getSelectedSubject(String userid,String classid,String sessionId) {

			ArrayList<EduStudentTO> users = new ArrayList<EduStudentTO>();
			try{
				getConnection();
				logger.debug("Method getSelectedSubject :: Database Connection closed");
				preStat = conn.prepareStatement(query.getSelectedSubjectForStudent);
				preStat.setString(1,classid);
				preStat.setString(2,userid);
				preStat.setString(3,sessionId);
				logger.debug("Method getSelectedSubject :: Query Executed==>"+preStat);
				rsObj=preStat.executeQuery();
				while(rsObj.next())
				{
					EduStudentTO EduStudentTOObj = new EduStudentTO();
					EduStudentTOObj.setSubjectid(rsObj.getInt("subject_id"));
					EduStudentTOObj.setSubjectname(rsObj.getString("subject_name"));
					users.add(EduStudentTOObj);
				}
			}catch (Exception exp) {
				exp.printStackTrace();
				logger.debug("Method getSelectedSubject :: ERROR==>"+exp.getMessage());
			}
			finally{
				try {
					closeConnection(conn,callStm,preStat,rsObj);
					logger.debug("Method getSelectedSubject :: Database Connection closed");
					} 
			catch (Exception exp) {exp.printStackTrace();}}
			return users;
		}
	 
		public  ArrayList<EduStudentTO> getAlumniStudentDetails(int totalRecord,int startIndex,String sort,String dir,String SearchKey,String searchBysessionkey,String searchByclasskey) 
		{
		   	ArrayList<EduStudentTO> alumnistudentObj = new ArrayList<EduStudentTO>();
		    try { 
		    	getConnection();
		    	logger.debug("Method getAlumniStudentDetails :: Database Connection opened");
		    	callStm = conn.prepareCall("{call sp_alumni_student_detail_list(?,?,?,?,?,?,?)}");	
		    	callStm.setInt(1,totalRecord);
		    	callStm.setInt(2,startIndex);
		    	callStm.setString(3,sort);
		    	callStm.setString(4,dir);
		    	callStm.setString(5,SearchKey);
		    	callStm.setString(6,searchBysessionkey);
		    	callStm.setString(7,searchByclasskey);
		    	callStm.execute();
		    	rsObj = callStm.getResultSet();
		    	logger.debug("Method getAlumniStudentDetails :: Procedure Executed==>"+callStm);
	    		while(rsObj.next())	
		    	{
	    			
	    			EduStudentTO studentTOtemp = new EduStudentTO();
	    			studentTOtemp.setFirstname(rsObj.getString("first_name"));
		    		studentTOtemp.setLastname(rsObj.getString("last_name"));
		    		studentTOtemp.setPhoneno(rsObj.getString("contact_no"));
		    	 	studentTOtemp.setEmailid(rsObj.getString("email_id"));
		    	 	studentTOtemp.setStudentid(rsObj.getInt("student_id"));
		    	 	studentTOtemp.setClassname(rsObj.getString("class_name"));
		    	 	studentTOtemp.setSection(rsObj.getString("section"));
		    	 	studentTOtemp.setSession(rsObj.getString("session_year"));
		    	 	studentTOtemp.setSessionId(rsObj.getInt("session_id"));
		    	 	alumnistudentObj.add(studentTOtemp);
		    	 	
		    	}
	    		  
			}
		    catch (Exception exception)
		    {
		    	exception.printStackTrace();
		    	logger.debug("ERROR:: "+exception.getMessage());
		    }
		    finally 
		    {
		    	try{	
		    		closeConnection(conn,callStm,preStat,rsObj);
		    		logger.debug("Method getStudentDetails :: Database Connection closed "+conn);
		    	} catch (SQLException e) 
		    	{
		    		e.printStackTrace();
		    	}
		    }
			return alumnistudentObj;
		}
		
		 public ArrayList<String> getCurrentSession()
		  {
			 ArrayList<String> sessioninfo = new ArrayList<String>();
			 try{
			 getConnection();
			 logger.debug("Method getCurrentSession :: Database Connection opened");
			 callStm = conn.prepareCall("{call sp_getCurrent_session()}");
			 logger.debug("Method getCurrentSession :: Query Executed==>"+callStm);
			 rsObj = callStm.executeQuery();
				while(rsObj.next()){
					sessioninfo.add(rsObj.getInt("session_id")+","+rsObj.getString("session_year"));
				}
			 }	
				catch(Exception exp)
				{
					exp.printStackTrace();
					logger.debug("Method getCurrentSession :: ERROR==>"+exp.getMessage());
				}
				finally {
					try{
						closeConnection(conn,callStm,preStat,rsObj);
						logger.debug("Method getCurrentSession :: Database Connection closed");
						}catch(Exception exp){exp.printStackTrace();}}
			 return sessioninfo;
			 
		  }
		 
		 public void test()
		  {
			 try{
			 getConnection();
			 logger.debug("Method getCurrentSession :: Database Connection opened");
			 callStm = conn.prepareCall("{call sp_test()}");
			 logger.debug("Method getCurrentSession :: Query Executed==>"+callStm);
			 boolean bool = callStm.execute();
			 //boolean bool =true;
			 while(bool){
				 rsObj = callStm.getResultSet();
				while(rsObj.next()){
					
					System.out.println("Test==========="+rsObj.getString(1));
					System.out.println("Test==========="+rsObj.getString(2));
					
				}
				bool = callStm.getMoreResults();
				//rsObj= callStm.getResultSet();
			 }
			}	
				catch(Exception exp)
				{
					exp.printStackTrace();
					logger.debug("Method getCurrentSession :: ERROR==>"+exp.getMessage());
				}
				finally {
					try{
						closeConnection(conn,callStm,preStat,rsObj);
						logger.debug("Method getCurrentSession :: Database Connection closed");
						}catch(Exception exp){exp.printStackTrace();}}
			 
		  }
		 
		 public ArrayList<EduStudentTO> getStudentList(String classId,String selectionFlag) {

				ArrayList<EduStudentTO> users = new ArrayList<EduStudentTO>();
				try{
					getConnection();
					logger.debug("Method getStudentList :: Database Connection opened");
					callStm = conn.prepareCall("{call sp_student_list(?,?)}");	
			    	callStm.setString(1,selectionFlag);
			    	callStm.setString(2,classId);
					rsObj=callStm.executeQuery();
					logger.debug("Method getStudentList :: Query Executed==>"+preStat);
					while(rsObj.next())
					{
					EduStudentTO eduEduStudentTOObj = new EduStudentTO();
					eduEduStudentTOObj.setStudentid(rsObj.getInt("student_id"));
					eduEduStudentTOObj.setEmailid(rsObj.getString("email_id"));
					eduEduStudentTOObj.setFirstname(rsObj.getString("first_name")+" "+rsObj.getString("middle_name")+" "+rsObj.getString("last_name")+"<--->"+rsObj.getString("email_id"));
					users.add(eduEduStudentTOObj);
					}
				}catch (Exception exp) {
					exp.printStackTrace();
					logger.debug("Method getStudentList :: ERROR==>"+exp.getMessage());
				}
				finally{
					try {
						closeConnection(conn,callStm,preStat,rsObj);
						logger.debug("Method getStudentList :: Database Connection closed");
						} 
				catch (Exception exp) {exp.printStackTrace();}}
				return users;
			}

		 public ArrayList<EduStudentTO> getParentList() {

				ArrayList<EduStudentTO> users = new ArrayList<EduStudentTO>();
				try{
					getConnection();
					logger.debug("Method getParentList :: Database Connection opened");
					callStm = conn.prepareCall("{call sp_parent_list()}");	
			    	//callStm.setString(1,selectionFlag);
					rsObj=callStm.executeQuery();
					logger.debug("Method getParentList :: Query Executed==>"+preStat);
					while(rsObj.next())
					{
					EduStudentTO eduEduStudentTOObj = new EduStudentTO();
					eduEduStudentTOObj.setParentid(rsObj.getInt("parent_id"));
					eduEduStudentTOObj.setEmailid(rsObj.getString("email_id"));
					eduEduStudentTOObj.setFathername(rsObj.getString("father_name")+"<--->"+rsObj.getString("email_id"));
					users.add(eduEduStudentTOObj);
					}
				}catch (Exception exp) {
					exp.printStackTrace();
					logger.debug("Method getParentList :: ERROR==>"+exp.getMessage());
				}
				finally{
					try {
						closeConnection(conn,callStm,preStat,rsObj);
						logger.debug("Method getParentList :: Database Connection closed");
						} 
				catch (Exception exp) {exp.printStackTrace();}}
				return users;
			}

}