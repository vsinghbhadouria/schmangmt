package com.sch.dao;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.sch.to.*;

public class TopNavigationDAO extends ConnectionUtil{
	Query query =null;
    ResultSet rsObj = null; 
    PreparedStatement preStat=null;	
    CallableStatement callStm = null; 
    Connection connection=null;
	public TopNavigationDAO() {		
	
	}

	private static Logger logger = Logger.getLogger(TopNavigationDAO.class);
	public ArrayList<TopNavigationTO> getMenusByParent(Integer parent_Id,Integer ug_id,Connection conn) throws SQLException
	{
		Query query = new Query();
		ArrayList<TopNavigationTO> menuSelected = new ArrayList<TopNavigationTO>();
		try{
			
			preStat = conn.prepareStatement(query.ugaccess);
			preStat.setInt(1,parent_Id);
			preStat.setInt(2,ug_id);	
			logger.debug("Execute Query for getting Top Navigation"+preStat);
			rsObj = preStat.executeQuery();	
			while (rsObj.next()){
				TopNavigationTO leftNavigationTOTemp = new TopNavigationTO(rsObj.getInt("F_ID"),rsObj.getString("F_TITLE"),rsObj.getString("F_URL"),rsObj.getInt("PF_ID"));
				menuSelected.add(leftNavigationTOTemp);
			}
			
		}catch(SQLException exp){
			exp.printStackTrace();
			throw exp;
		}
		finally{
			try{
				//closeConnection(connection,callStm,preStat,rsObj);
				logger.debug("Release the database Object(PreparedStatement and ResultSet)");
				if(preStat!=null)
					preStat.close();
				if(rsObj!=null)
					rsObj.close();
			}catch (SQLException sqle) {
				sqle.printStackTrace();				
			}
		}
		return menuSelected;
	}

}
