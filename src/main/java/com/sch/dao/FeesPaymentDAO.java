package com.sch.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.sch.to.EduRegistrationTO;
import com.sch.to.EduStudentTO;
import com.sch.to.FeesPaymentTO;

public class FeesPaymentDAO extends ConnectionUtil
{
	ResultSet rest=null;
	ResultSet rs=null;
	Query query=null;
	ResultSet rsObj = null; 
	PreparedStatement preStat=null;	
	CallableStatement callStm = null;
	Logger logger  = Logger.getLogger(FeesPaymentDAO.class);
	public FeesPaymentDAO()
	{
		//connUtil = new ConnectionUtil();
		query = new Query();
	}

public ArrayList<FeesPaymentTO> getStudentList(String SearchKey,String searchBysessionkey)
	{
		
		ArrayList<FeesPaymentTO> feesPaymentTOArrayObj = new ArrayList<FeesPaymentTO>();
		try
		{
			getConnection();
			logger.debug("Call the procedure sp_student_detail_search............");
			logger.debug("Search Criteria :: "+SearchKey+" :: "+searchBysessionkey);
			callStm = conn.prepareCall("{call sp_student_detail_search(?,?)}");
			callStm.setString(1,"%"+SearchKey+"%");
			callStm.setString(2,"%"+searchBysessionkey+"%");
			rest = callStm.executeQuery();
			System.out.println("dislist"+callStm);
			while(rest.next())
			{
				FeesPaymentTO feesPaymentToObj = new FeesPaymentTO();
				feesPaymentToObj.setStudentid(rest.getInt("student_id"));
				feesPaymentToObj.setFirstname(rest.getString("first_name"));
				feesPaymentToObj.setLastname(rest.getString("last_name"));
				feesPaymentToObj.setFathername(rest.getString("father_name"));
				feesPaymentToObj.setSessionid(rest.getInt("session_id"));
				feesPaymentToObj.setClassid(rest.getInt("class_id"));
				feesPaymentToObj.setClassname(rest.getString("class_name"));
				feesPaymentToObj.setSection(rest.getString("section"));
				feesPaymentToObj.setMonthly_fee(rest.getString("monthly_fee"));
				feesPaymentToObj.setDateofbirth(rest.getInt("birth_month")+1+"/"+rest.getString("birth_day")+"/"+rest.getString("birth_year"));
				feesPaymentToObj.setAddress(rest.getString("address"));
				feesPaymentTOArrayObj.add(feesPaymentToObj);
			}
		}catch(Exception exp){
			exp.printStackTrace();
		}
		 finally
		   {
			   try
			   {
				   callStm.close();
				   rest.close();
				   conn.close();
			   }catch(Exception exp)
			   {
					exp.printStackTrace();   
				   }
		   }
		return feesPaymentTOArrayObj;
	} 
	
	public String getClassBySession(String admissionId,int selectedSessionId)
	{
		String className=null;
		try
		{
			getConnection();
			preStat =conn.prepareStatement(query.getClassNameBySessionId);
			preStat.setString(1, admissionId);
			preStat.setInt(2,selectedSessionId);
			logger.debug("Search Criteria :: "+preStat+" ::Query "+query.getClassNameBySessionId);
			rest =preStat.executeQuery();
			if(rest.next())
			{
				className=rest.getString("class_name")+" "+rest.getString("section")+","+rest.getString("monthly_fee")+","+rest.getString("class_id");
			}
		}
		catch(Exception exp){
			exp.printStackTrace();
		}
		 finally
		   {
			   try
			   {
				   preStat.close();
				   rest.close();
				   conn.close();
			   }catch(Exception exp)
			   {
					exp.printStackTrace();   
				   }
		   }
		return className;
	}

	 public ArrayList<FeesPaymentTO> getMonthList()
	 {
			ArrayList<FeesPaymentTO> feePaymentTOArrayObj = new ArrayList<FeesPaymentTO>();
			
			try
			{
				getConnection();
				logger.debug("Month List :: "+query.getMonthList);
				preStat = conn.prepareStatement(query.getMonthList);
				rs = preStat.executeQuery();
				while(rs.next())
				{
					FeesPaymentTO feePaymentTOObj = new FeesPaymentTO();
					feePaymentTOObj.setMonthid(rs.getInt("month_id"));
					feePaymentTOObj.setMonthname(rs.getString("month_name"));
					feePaymentTOArrayObj.add(feePaymentTOObj);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			 finally
			   {
				   try
				   {
					   preStat.close();
					   rs.close();
					   conn.close();
				   }catch(Exception exp)
				   {
						exp.printStackTrace();   
					   }
			   }
			return feePaymentTOArrayObj;
		 
		 
	 }
	 public void saveFeePayment(FeesPaymentTO feesPaymentTOObj)
	 {
		 System.out.println("Student Id"+feesPaymentTOObj.getStudentid());
		 System.out.println("Fee Amount"+feesPaymentTOObj.getMonthly_fee());
		 System.out.println("Session id"+feesPaymentTOObj.getSessionid());
		 System.out.println("class Id"+feesPaymentTOObj.getClassid());
		 System.out.println("Mode of  payment"+feesPaymentTOObj.getPaymentMode());
		 System.out.println("Number of month"+feesPaymentTOObj.getMonthselection().length);
		 
		 
			try
			{
				getConnection();
				logger.debug("Method saveEduStudentData :: Database Connection opened");
				conn.setAutoCommit(false);
				callStm = conn.prepareCall("{call sch_fee_payment_insertion(?,?,?,?,?,?,?,?,?)}");	
				callStm.setInt(1,feesPaymentTOObj.getStudentid());
				callStm.setInt(2,feesPaymentTOObj.getSessionid());
				callStm.setInt(3,feesPaymentTOObj.getClassid());
				callStm.setString(4,feesPaymentTOObj.getMonthly_fee());
				callStm.setString(5,feesPaymentTOObj.getPaymentDate());
				callStm.setString(6,feesPaymentTOObj.getPaymentMode());
				callStm.setString(7,feesPaymentTOObj.getChequeNo());
				callStm.setString(8,feesPaymentTOObj.getDateoncheque());
				callStm.setString(9,feesPaymentTOObj.getBankname());
				logger.debug("Method saveEduStudentData :: Procedure Executed==>"+callStm);
				callStm.executeUpdate();
				if(feesPaymentTOObj.getMonthselection()!=null && feesPaymentTOObj.getMonthselection().length!=0)
			    {
			    	preStat = conn.prepareStatement("select max(receipt_no) receipt_no from sch_fee_detail");
					rsObj = preStat.executeQuery();
					int receiptno=-1;
					while(rsObj.next()){
						receiptno =(rsObj.getInt("receipt_no"));	
					}
						for(int xx=0;xx<feesPaymentTOObj.getMonthselection().length;xx++)
						{
					    preStat = conn.prepareStatement(query.insertFeePayment);
					    preStat.setInt(1, receiptno);
					    preStat.setString(2, feesPaymentTOObj.getMonthselection()[xx]);
					    logger.debug("Method saveEduStudentData :: Query Executed for assigned classess==>"+preStat);
					    preStat.executeUpdate();
					    
					}
			    }
			    conn.commit();
			 
			}
			catch(Exception exp)
			{
				exp.printStackTrace();
				logger.debug("Method saveEduStudentData :: ERROR==>"+exp.getMessage());
				try {
					conn.rollback();
					logger.debug("Method saveEduStudentData :: Transaction rollbacked");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			finally {
				try{
					closeConnection(conn,callStm,preStat,rsObj);
					logger.debug("Method saveEduStudentData :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}
	        	 
		 
	 }
	 
	 public FeesPaymentTO getCollationDetailByUserId(String user_Id,String session_Id,String receipt_no)
	 {
		 FeesPaymentTO feesPaymentTOObj = new FeesPaymentTO();
	 	try
	 	{    
	 		getConnection();
	 		logger.debug("Method getCollationDetailByUserId :: Database Connection opened");
	 		callStm = conn.prepareCall("{call get_fee_collation_By_userId_sp(?,?,?)}");
	 		callStm.setInt(1,Integer.valueOf(user_Id));
	 		callStm.setInt(2,Integer.valueOf(session_Id));
	 		callStm.setInt(3,Integer.valueOf(receipt_no));
	 		logger.debug("Method getCollationDetailByUserId :: Procedure Executed==>"+callStm);
	 		rsObj = callStm.executeQuery();
	 		if(rsObj.next())
	 		{   
	 			
	 			feesPaymentTOObj.setReceptNo(rsObj.getString("receipt_no"));
	 			feesPaymentTOObj.setStudentid(rsObj.getInt("student_id"));
	 			feesPaymentTOObj.setFirstname(rsObj.getString("first_name"));
	 			feesPaymentTOObj.setMiddlename(rsObj.getString("middle_name"));
	 			feesPaymentTOObj.setLastname(rsObj.getString("last_name"));
	 			feesPaymentTOObj.setMonthly_fee(rsObj.getString("fee_amount"));
	 			feesPaymentTOObj.setDateofbirth(rsObj.getInt("birth_day")+"/"+(rsObj.getInt("birth_month")+1)+"/"+rsObj.getInt("birth_year"));
	 			feesPaymentTOObj.setDept_name(rsObj.getString("dept_name"));
	 			feesPaymentTOObj.setClassname(rsObj.getString("class_name"));
	 			feesPaymentTOObj.setFathername(rsObj.getString("father_name"));
	 			feesPaymentTOObj.setPaymentDate(rsObj.getString("payment_date"));
	 			
	 		}
	 		
	 	}
	 	catch(Exception exp){exp.printStackTrace();
	 	logger.debug("Method getCollationDetailByUserId :: ERROR==>"+exp.getMessage());
	 	}
	 	finally{
	 		try{
	 			closeConnection(conn,callStm,preStat,rsObj);
	 			logger.debug("Method getCollationDetailByUserId :: Database Connection closed");
	 			}catch(Exception exp){exp.printStackTrace();}}
	 	return feesPaymentTOObj;
	 }
	 
	 public ArrayList<FeesPaymentTO> getPayedMonthList(String user_Id,String session_Id,String receipt_no)
	 {
		 
			ArrayList<FeesPaymentTO> feePaymentTOArrayObj = new ArrayList<FeesPaymentTO>();	
			try
			{
				getConnection();
				logger.debug("Month List :: "+query.getMonthList);
				preStat = conn.prepareStatement(query.getPayedMonthList);
				preStat.setString(1, receipt_no);
				preStat.setString(2, user_Id);
				preStat.setString(3, session_Id);
				logger.debug("Method getPayedMonthList :: "+preStat);
				rs = preStat.executeQuery();
				while(rs.next())
				{
					FeesPaymentTO feePaymentTOObj = new FeesPaymentTO();
					feePaymentTOObj.setMonthid(rs.getInt("month_id"));
					feePaymentTOObj.setMonthname(rs.getString("month_name"));
					feePaymentTOArrayObj.add(feePaymentTOObj);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			 finally
			   {
				   try
				   {
					   preStat.close();
					   rs.close();
					   conn.close();
				   }catch(Exception exp)
				   {
						exp.printStackTrace();   
					   }
			   }
			return feePaymentTOArrayObj;
		 
	 }
	 
	 public  ArrayList<FeesPaymentTO> getStudentFeeCollactionDetails(int totalRecord,int startIndex,String sort,String dir,String SearchKey,String searchBysessionkey,String searchByclasskey) 
		{
		   	ArrayList<FeesPaymentTO> feesPaymentTOObj = new ArrayList<FeesPaymentTO>();
		    try { 
		    	getConnection();
		    	logger.debug("Method getStudentFeeCollactionDetails :: Database Connection opened");
		    	callStm = conn.prepareCall("{call sp_student_feeCollaction_detail(?,?,?,?,?,?,?)}");	
		    	callStm.setInt(1,totalRecord);
		    	callStm.setInt(2,startIndex);
		    	callStm.setString(3,sort);
		    	callStm.setString(4,dir);
		    	callStm.setString(5,SearchKey);
		    	callStm.setString(6,searchBysessionkey);
		    	callStm.setString(7,searchByclasskey);
		    	callStm.execute();
		    	rsObj = callStm.getResultSet();
		    	logger.debug("Method getStudentFeeCollactionDetails :: Procedure Executed==>"+callStm);
	    		while(rsObj.next())	
		    	{
	    			
	    			FeesPaymentTO feesPaymentTOtemp = new FeesPaymentTO();
	    			feesPaymentTOtemp.setFirstname(rsObj.getString("first_name"));
	    			feesPaymentTOtemp.setLastname(rsObj.getString("last_name"));
	    			feesPaymentTOtemp.setEmailId(rsObj.getString("email_id"));
	    			feesPaymentTOtemp.setStudentid(rsObj.getInt("student_id"));
	    			feesPaymentTOtemp.setClassname(rsObj.getString("class_name"));
	    			feesPaymentTOtemp.setSection(rsObj.getString("section"));
	    			feesPaymentTOtemp.setSessionyear(rsObj.getString("session_year"));
	    			feesPaymentTOtemp.setSessionid(rsObj.getInt("session_id"));
	    			feesPaymentTOtemp.setReceptNo(rsObj.getString("receipt_no"));
	    			feesPaymentTOtemp.setPhoneno(rsObj.getString("contact_no"));
	    			feesPaymentTOObj.add(feesPaymentTOtemp);
		    	 	
		    	}
	    		  
			}
		    catch (Exception exception)
		    {
		    	exception.printStackTrace();
		    	logger.debug("ERROR:: "+exception.getMessage());
		    }
		    finally 
		    {
		    	try{	
		    		closeConnection(conn,callStm,preStat,rsObj);
		    		logger.debug("Method getStudentFeeCollactionDetails :: Database Connection closed "+conn);
		    	} catch (SQLException e) 
		    	{
		    		e.printStackTrace();
		    	}
		    }
			return feesPaymentTOObj;
		}
	 
	 public FeesPaymentTO getEduRegistrationDatabyUserId(String user_Id,String session_Id,String reg_status)
	 {
		 FeesPaymentTO feesPaymentTOObj = new FeesPaymentTO();
			
	 	try
	 	{    
	 		getConnection();
	 		logger.debug("Method getEduRegistrationDatabyUserId :: Database Connection opened");
	 		callStm = conn.prepareCall("{call get_registrationDetailBySession_sp(?,?,?)}");
	 		callStm.setInt(1,Integer.valueOf(user_Id));
	 		callStm.setInt(2,Integer.valueOf(session_Id));
	 		callStm.setString(3,reg_status);
	 		logger.debug("Method getEduRegistrationDatabyUserId :: Procedure Executed==>"+callStm);
	 		rsObj = callStm.executeQuery();
	 		if(rsObj.next())
	 		{
	 			
	 			feesPaymentTOObj.setFirstname(rsObj.getString("first_name"));
	 			feesPaymentTOObj.setMiddlename(rsObj.getString("middle_name"));
	 			feesPaymentTOObj.setLastname(rsObj.getString("last_name"));
	 			feesPaymentTOObj.setPhoneno(rsObj.getString("contact_no"));
	 			feesPaymentTOObj.setAddress(rsObj.getString("address"));
	 			feesPaymentTOObj.setEmailId(rsObj.getString("email_id"));
	 			feesPaymentTOObj.setStudentid(rsObj.getInt("registration_no"));
	 			//feesPaymentTOObj.setRegistrationdate(rsObj.getString("registration_date"));
	 			//feesPaymentTOObj.setGender(rsObj.getString("gender"));
	 			feesPaymentTOObj.setClassid(rsObj.getInt("class_id"));
	 			feesPaymentTOObj.setFathername(rsObj.getString("father_name"));
	 			feesPaymentTOObj.setDateofbirth(rsObj.getString("dateofbirth"));
	 			//feesPaymentTOObj.setStudprequalification_id(rsObj.getInt("preinstclass_id"));
	 			//feesPaymentTOObj.setPreviousinstitute(rsObj.getString("previous_edu_center"));
	 			feesPaymentTOObj.setFathername(rsObj.getString("father_name"));
	 			//feesPaymentTOObj.setMothername(rsObj.getString("mother_name"));
	 			//feesPaymentTOObj.setParentqualification_id(rsObj.getInt("father_higher_qualification_id"));
	 			//feesPaymentTOObj.setParentemailid(rsObj.getString("parentemail_id"));
	 			feesPaymentTOObj.setDept_id(rsObj.getInt("dept_id"));
	 			feesPaymentTOObj.setClassname(rsObj.getString("class_name"));
	 			feesPaymentTOObj.setDept_name(rsObj.getString("dept_name"));
	 			feesPaymentTOObj.setSessionyear(rsObj.getString("session_year"));
	 			//feesPaymentTOObj.setStudprequalification_title(rsObj.getString("stud_pre_class"));
	 			//feesPaymentTOObj.setParentqualification_title(rsObj.getString("father_qualification"));
	 			feesPaymentTOObj.setRegistrationfee(rsObj.getInt("regfees"));
	 			//feesPaymentTOObj.setConcessionamt(rsObj.getInt("concessionamt"));
	 			//feesPaymentTOObj.setComment(rsObj.getString("comments"));
	 			
	 		}
	 		
	 	}
	 	catch(Exception exp){exp.printStackTrace();
	 	logger.debug("Method getEduRegistrationDatabyUserId :: ERROR==>"+exp.getMessage());
	 	}
	 	finally{
	 		try{
	 			closeConnection(conn,callStm,preStat,rsObj);
	 			logger.debug("Method getEduRegistrationDatabyUserId :: Database Connection closed");
	 			}catch(Exception exp){exp.printStackTrace();}}
	 	return feesPaymentTOObj;
	 }

	 public void saveRegPayment(FeesPaymentTO FeesPaymentTOObj){
		  
			try
			{
				getConnection();
				logger.debug("Method saveRegPayment :: Database Connection opened");
				callStm = conn.prepareCall("{call sch_regfee_collection(?,?,?,?)}");	
				callStm.setInt(1,FeesPaymentTOObj.getRegistrationfee());
				callStm.setInt(2,FeesPaymentTOObj.getConcessionamt());
				callStm.setString(3,FeesPaymentTOObj.getComment());
				callStm.setInt(4,FeesPaymentTOObj.getStudentid());
				logger.debug("Method saveRegPayment :: Procedure Executed==>"+callStm);
				callStm.executeUpdate();
				
			}
			catch(Exception exp)
			{
				exp.printStackTrace();
				logger.debug("Method saveRegPayment :: ERROR==>"+exp.getMessage());
				try {
					conn.rollback();
					logger.debug("Method saveRegPayment :: Transaction rollbacked");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			finally {
				try{
					closeConnection(conn,callStm,preStat,rsObj);
					logger.debug("Method saveRegPayment :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}

		}


}
