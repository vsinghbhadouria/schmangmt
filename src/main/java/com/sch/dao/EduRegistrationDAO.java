package com.sch.dao;

import java.sql.*;
import java.util.ArrayList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

import com.sch.constant.PortableConstant;
import com.sch.delegates.EduStaffManager;
import com.sch.form.EduRegistrationForm;
import com.sch.form.EduStaffForm;
import com.sch.form.EduStudentForm;
import com.sch.to.*;


public class EduRegistrationDAO extends ConnectionUtil

{
	Query query =null;
    ResultSet rsObj = null; 
    PreparedStatement preStat=null;	
    CallableStatement callStm = null;
	ArrayList<EduRegistrationTO> arraylistobj = new ArrayList<EduRegistrationTO>();
	Logger logger  = Logger.getLogger(EduRegistrationDAO.class);
	public EduRegistrationDAO()
	{
		 query = new Query();  
	}
	
	public  ArrayList<EduRegistrationTO> getRegistrationDetails(int totalRecord,int startIndex,String sort,String dir,String SearchKey,String searchBysessionkey,String searchByclasskey,String regFlag) 
	{
	   	ArrayList<EduRegistrationTO> registrationObj = new ArrayList<EduRegistrationTO>();
	    try { 
	    	getConnection();
	    	logger.debug("Method getRegistrationDetails :: Database Connection opened");
	    	callStm = conn.prepareCall("{call sp_registration_detail_list(?,?,?,?,?,?,?,?)}");	
	    	callStm.setInt(1,totalRecord);
	    	callStm.setInt(2,startIndex);
	    	callStm.setString(3,sort);
	    	callStm.setString(4,dir);
	    	callStm.setString(5,SearchKey);
	    	callStm.setString(6,searchBysessionkey);
	    	callStm.setString(7,searchByclasskey);
	    	callStm.setString(8,regFlag);
	    	callStm.execute();
	    	rsObj = callStm.getResultSet();
	    	logger.debug("Method getRegistrationDetails :: Procedure Executed==>"+callStm);
    		while(rsObj.next())	
	    	{
    			EduRegistrationTO eduRegistrationTOtemp = new EduRegistrationTO();
    			eduRegistrationTOtemp.setFirstname(rsObj.getString("first_name"));
    			eduRegistrationTOtemp.setLastname(rsObj.getString("last_name"));
    			eduRegistrationTOtemp.setPhoneno(rsObj.getString("contact_no"));
    			eduRegistrationTOtemp.setEmailid(rsObj.getString("email_id"));
    			eduRegistrationTOtemp.setRegistrationid(rsObj.getInt("registration_no"));
    			eduRegistrationTOtemp.setClassname(rsObj.getString("class_name"));
    			eduRegistrationTOtemp.setSession(rsObj.getString("session_year"));
    			eduRegistrationTOtemp.setSessionId(rsObj.getInt("session_id"));
    			
	    	 	registrationObj.add(eduRegistrationTOtemp);
	    	}
    		  
		}
	    catch (Exception exception)
	    {
	    	exception.printStackTrace();
	    	logger.debug("ERROR:: "+exception.getMessage());
	    }
	    finally 
	    {
	    	try{	
	    		closeConnection(conn,callStm,preStat,rsObj);
	    		logger.debug("Method getRegistrationDetails :: Database Connection closed "+conn);
	    	} catch (SQLException e) 
	    	{
	    		e.printStackTrace();
	    	}
	    }
		return registrationObj;
	}
	
	public  ArrayList<EduStudentTO> getRegistrationDetails1(int totalRecord,int startIndex,String sort,String dir,String SearchKey,String searchBysessionkey,String searchByclasskey) 
	{
	   	ArrayList<EduStudentTO> registrationObj = new ArrayList<EduStudentTO>();
	    try { 
	    	getConnection();
	    	callStm = conn.prepareCall("{call sp_student_detail_list(?,?,?,?,?,?,?)}");	
	    	callStm.setInt(1,totalRecord);
	    	callStm.setInt(2,startIndex);
	    	callStm.setString(3,sort);
	    	callStm.setString(4,dir);
	    	callStm.setString(5,SearchKey);
	    	callStm.setString(6,searchBysessionkey);
	    	callStm.setString(7,searchByclasskey);
	    	callStm.execute();
	    	rsObj = callStm.getResultSet();
	    	System.out.println("rsObj====="+rsObj+" callStm"+callStm);
    		while(rsObj.next())	
	    	{
    			
    			EduStudentTO registrationTOtemp = new EduStudentTO();
    			registrationTOtemp.setFirstname(rsObj.getString("first_name"));
	    		registrationTOtemp.setLastname(rsObj.getString("last_name"));
	    		registrationTOtemp.setPhoneno(rsObj.getString("contact_no"));
	    	 	registrationTOtemp.setEmailid(rsObj.getString("email_id"));
	    	 	registrationTOtemp.setStudentid(rsObj.getInt("student_id"));
	    	 	registrationTOtemp.setClassname(rsObj.getString("class_name"));
	    	 	registrationTOtemp.setSection(rsObj.getString("section"));
	    	 	registrationTOtemp.setSession(rsObj.getString("session_year"));
	    	 	registrationTOtemp.setSessionId(rsObj.getInt("session_id"));
	    	 	registrationObj.add(registrationTOtemp);
	    	 	
	    	}
    		  
		}
	    catch (Exception exception)
	    {
	    	exception.printStackTrace();
	    	System.out.println(" ERROR: " + exception.getMessage());
	    }
	    finally 
	    {
	    	try{	
	    		closeConnection(conn,callStm,preStat,rsObj);
	    	} catch (SQLException e) 
	    	{
	    		e.printStackTrace();
	    	}
	    }
		return registrationObj;
	}

	public  void saveEduRegistrationData(EduRegistrationTO EduRegistrationTOObj)
	{
		try
		{
			getConnection();
			logger.debug("Method saveEduRegistrationData :: Database Connection opened");
			conn.setAutoCommit(false);
		   	callStm = conn.prepareCall("{call insert_registration_detail_sp(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");	
			callStm.setString(1,EduRegistrationTOObj.getFirstname());
			callStm.setString(2,EduRegistrationTOObj.getMiddlename());
			callStm.setString(3,EduRegistrationTOObj.getLastname());
			callStm.setString(4,EduRegistrationTOObj.getFathername());
			callStm.setString(5,EduRegistrationTOObj.getEmailid());
			callStm.setString(6,EduRegistrationTOObj.getRegistrationmonth()+"/"+EduRegistrationTOObj.getRegistrationday()+"/"+EduRegistrationTOObj.getRegistrationyear());
			callStm.setString(7,EduRegistrationTOObj.getAddress());
			callStm.setString(8,EduRegistrationTOObj.getPhoneno());
			callStm.setString(9,EduRegistrationTOObj.getRegstatus());
			callStm.setString(10,EduRegistrationTOObj.getGender());
			callStm.setInt(11,EduRegistrationTOObj.getClassid());
			callStm.setString(12,EduRegistrationTOObj.getBirthmonth()+"/"+EduRegistrationTOObj.getBirthday()+"/"+EduRegistrationTOObj.getBirthyear());
			callStm.setString(13,EduRegistrationTOObj.getMothername());
			callStm.setString(14,EduRegistrationTOObj.getParentqualification());
			callStm.setString(15,EduRegistrationTOObj.getParentemailid());
			callStm.setString(16,EduRegistrationTOObj.getParentemailid());
			callStm.setString(17,EduRegistrationTOObj.getStudprequalification());
			callStm.setString(18,EduRegistrationTOObj.getPreviousinstitute());
			callStm.setInt(19,EduRegistrationTOObj.getRegistrationfee());
			callStm.setInt(20,EduRegistrationTOObj.getConcessionamt());
			callStm.setString(21,EduRegistrationTOObj.getComment());
			callStm.setInt(22,EduRegistrationTOObj.getDept_id());
			logger.debug("Method saveEduRegistrationData :: Procedure Executed==>"+callStm);
			callStm.executeUpdate();
			conn.commit();
		 
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method saveEduRegistrationData :: ERROR==>"+exp.getMessage());
			try {
				conn.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method saveEduRegistrationData :: Database Connection closed");
		}catch(Exception exp){
			exp.printStackTrace();
		}}
		
	}

	public  void updateRegistrationData(EduRegistrationTO EduRegistrationTOObj)
	{
		CallableStatement callStm = null;
		try
		{
			logger.debug("Method updateRegistrationData :: Database Connection opened");
			getConnection();
			conn.setAutoCommit(false);
			callStm = conn.prepareCall("{call update_registration_detail_sp(?,?,?,?,?,?,?,?,?,?,?,?)}");
			callStm.setString(1,EduRegistrationTOObj.getFirstname());
			callStm.setString(2,EduRegistrationTOObj.getMiddlename());
			callStm.setString(3,EduRegistrationTOObj.getLastname());
			callStm.setString(4,EduRegistrationTOObj.getEmailid());
			callStm.setString(5,EduRegistrationTOObj.getRegistrationdate());
			callStm.setString(6,EduRegistrationTOObj.getAddress());
			callStm.setString(7,EduRegistrationTOObj.getPhoneno());
			callStm.setString(8,EduRegistrationTOObj.getGender());
			callStm.setString(9,EduRegistrationTOObj.getFathername());
			callStm.setInt(10,Integer.valueOf(EduRegistrationTOObj.getEduclass()));
			callStm.setString(11,EduRegistrationTOObj.getDateofbirth());
			callStm.setInt(12,EduRegistrationTOObj.getRegistrationid());
			logger.debug("Method updateRegistrationData :: Procedure Executed==>"+callStm);
			callStm.executeUpdate();
			conn.commit();
			
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method updateRegistrationData :: ERROR==>"+exp.getMessage());
			try {
				conn.rollback();
				logger.debug("Method updateRegistrationData :: Transaction rollbacked");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method updateRegistrationData :: Database Connection closed");
		}catch(Exception exp){
			exp.printStackTrace();
		}}
		
	}
public  ArrayList<EduRegistrationTO> getRegistrationQualification()
{
	 ArrayList<EduRegistrationTO> registrationObj=new  ArrayList<EduRegistrationTO>();
	try
	{
		getConnection();
		logger.debug("Method getRegistrationQualification :: Database Connection opened");
		preStat = conn.prepareStatement(query.getStudentPrequalification);	  
		logger.debug("Method getRegistrationQualification :: Query Executed==>"+preStat);
		rsObj = preStat.executeQuery();	
		 while(rsObj.next())
		    {
			 EduRegistrationTO eduRegistrationTOObj =new EduRegistrationTO();
			 eduRegistrationTOObj.setStudprequalification_title(rsObj.getString("qualification_title"));
		     eduRegistrationTOObj.setStudprequalification_id(rsObj.getInt("qualification_id"));
			 registrationObj.add(eduRegistrationTOObj); 	
		    }
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		logger.debug("Method getRegistrationQualification :: ERROR==>"+exp.getMessage());
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getRegistrationQualification :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}
	return registrationObj;
	
}
public  ArrayList<EduRegistrationTO> getParentHigherQualification()
{
	 ArrayList<EduRegistrationTO> eduRegistrationTOArrayObj = new  ArrayList<EduRegistrationTO>();
	try
	{
		getConnection();
		logger.debug("Method getParentHigherQualification :: Database Connection opened");
		preStat = conn.prepareStatement(query.getStaffqualification);
		logger.debug("Method getParentHigherQualification :: Query Executed==>"+preStat);
		rsObj = preStat.executeQuery();	
		 while(rsObj.next())
		    {
			 EduRegistrationTO eduRegistrationTOObj =new EduRegistrationTO();
			 eduRegistrationTOObj.setParentqualification_title(rsObj.getString("qualification_title"));
			 eduRegistrationTOObj.setParentqualification_id(rsObj.getInt("qualification_id"));
		     eduRegistrationTOArrayObj.add(eduRegistrationTOObj); 	
		    }
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		logger.debug("Method getParentHigherQualification :: ERROR==>"+exp.getMessage());
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getParentHigherQualification :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}
	return eduRegistrationTOArrayObj;
	
}


public ArrayList<EduRegistrationTO> getClassinfo() {

	ArrayList<EduRegistrationTO> users = new ArrayList<EduRegistrationTO>();
	
	try{
		getConnection();
		logger.debug("Method getClassinfo :: Database Connection opened");
		preStat = conn.prepareStatement(query.getClassInfo);
		logger.debug("Method getClassinfo :: Query Executed==>"+preStat);
		rsObj=preStat.executeQuery();
		while(rsObj.next())
		{
		EduRegistrationTO eduregistrationObj = new EduRegistrationTO();
		eduregistrationObj.setClassid(rsObj.getInt("class_id"));
		eduregistrationObj.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
		users.add(eduregistrationObj);
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getClassinfo :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getClassinfo :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}
public ArrayList<EduRegistrationTO> getAdmissionfees() {

	ArrayList<EduRegistrationTO> users = new ArrayList<EduRegistrationTO>();
	
	try{
		getConnection();
		logger.debug("Method getAdmissionfees :: Database Connection opened");
		preStat = conn.prepareStatement(query.getAdmissionfee);
		logger.debug("Method getAdmissionfees :: Query Executed==>"+preStat);
		rsObj=preStat.executeQuery();
		while(rsObj.next())
		{
		EduRegistrationTO eduregistrationObj = new EduRegistrationTO();
		eduregistrationObj.setClassid(rsObj.getInt("class_id"));
		eduregistrationObj.setRegistrationfee(rsObj.getInt("registration_fee"));
		users.add(eduregistrationObj);
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getAdmissionfees :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getAdmissionfees :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}

public Integer getRegistrationfee(Integer classid) {

	Integer fee=null;
	try{
		
		getConnection();
		logger.debug("Method getRegistrationfee :: Database Connection opened");
		preStat = conn.prepareStatement(query.getAdmfee);
		preStat.setInt(1,classid);
		logger.debug("Method getRegistrationfee :: Query Executed==>"+preStat);
		rsObj=preStat.executeQuery();
		while(rsObj.next())
		{
			fee = rsObj.getInt("registration_fee");
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getRegistrationfee :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getRegistrationfee :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return fee;
}

/*
public ArrayList<EduRegistrationTO> getUnAssoClassInfo(String userid) {

	ArrayList<EduRegistrationTO> users = new ArrayList<EduRegistrationTO>();
	try{
		getConnection();
		preStat = conn.prepareStatement(query.getUnAssoclassbyUserid);
		preStat.setString(1, userid);
		rsObj=preStat.executeQuery();
		while(rsObj.next())
		{
		EduRegistrationTO eduregistrationObj = new EduRegistrationTO();
		eduregistrationObj.setClassid(rsObj.getInt("class_id"));
		eduregistrationObj.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
		users.add(eduregistrationObj);
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}
*/
public EduRegistrationTO getEduRegistrationDatabyUserId(String user_Id,String session_Id,String reg_status)
{
	EduRegistrationTO EduRegistrationTOObj = new EduRegistrationTO();
	try
	{    
		getConnection();
		logger.debug("Method getEduRegistrationDatabyUserId :: Database Connection opened");
		callStm = conn.prepareCall("{call get_registrationDetailBySession_sp(?,?,?)}");
		callStm.setInt(1,Integer.valueOf(user_Id));
		callStm.setInt(2,Integer.valueOf(session_Id));
		callStm.setString(3,reg_status);
		logger.debug("Method getEduRegistrationDatabyUserId :: Procedure Executed==>"+callStm);
		rsObj = callStm.executeQuery();
		if(rsObj.next())
		{
			
			EduRegistrationTOObj.setFirstname(rsObj.getString("first_name"));
			EduRegistrationTOObj.setMiddlename(rsObj.getString("middle_name"));
			EduRegistrationTOObj.setLastname(rsObj.getString("last_name"));
			EduRegistrationTOObj.setPhoneno(rsObj.getString("contact_no"));
			EduRegistrationTOObj.setAddress(rsObj.getString("address"));
			EduRegistrationTOObj.setEmailid(rsObj.getString("email_id"));
			EduRegistrationTOObj.setRegistrationid(rsObj.getInt("registration_no"));
			EduRegistrationTOObj.setRegistrationdate(rsObj.getString("registration_date"));
			EduRegistrationTOObj.setGender(rsObj.getString("gender"));
			EduRegistrationTOObj.setClassid(rsObj.getInt("class_id"));
			EduRegistrationTOObj.setFathername(rsObj.getString("father_name"));
			EduRegistrationTOObj.setDateofbirth(rsObj.getString("dateofbirth"));
			EduRegistrationTOObj.setStudprequalification_id(rsObj.getInt("preinstclass_id"));
			EduRegistrationTOObj.setPreviousinstitute(rsObj.getString("previous_edu_center"));
			EduRegistrationTOObj.setFathername(rsObj.getString("father_name"));
			EduRegistrationTOObj.setMothername(rsObj.getString("mother_name"));
			EduRegistrationTOObj.setParentqualification_id(rsObj.getInt("father_higher_qualification_id"));
			EduRegistrationTOObj.setParentemailid(rsObj.getString("parentemail_id"));
			EduRegistrationTOObj.setDept_id(rsObj.getInt("dept_id"));
			EduRegistrationTOObj.setClassname(rsObj.getString("class_name"));
			EduRegistrationTOObj.setStudprequalification_title(rsObj.getString("stud_pre_class"));
			EduRegistrationTOObj.setParentqualification_title(rsObj.getString("father_qualification"));
			EduRegistrationTOObj.setRegistrationfee(rsObj.getInt("registration_fee"));
			EduRegistrationTOObj.setConcessionamt(rsObj.getInt("concessionamt"));
			EduRegistrationTOObj.setComment(rsObj.getString("comments"));
			
		}
		
	}
	catch(Exception exp){exp.printStackTrace();
	logger.debug("Method getEduRegistrationDatabyUserId :: ERROR==>"+exp.getMessage());
	}
	finally{
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getEduRegistrationDatabyUserId :: Database Connection closed");
			}catch(Exception exp){exp.printStackTrace();}}
	return EduRegistrationTOObj;
}
	public void deleteRegistrationDetailbyUserId(String student_Id,String Session_id)
	{
		try
		{
			getConnection();
			logger.debug("Method deleteRegistrationDetailbyUserId :: Database Connection opened");
			preStat = conn.prepareStatement(query.deleteRegistrationDetailbyUserId);
			preStat.setString(1,student_Id);
			preStat.setString(2,Session_id);
			logger.debug("Method deleteRegistrationDetailbyUserId :: Query Executed==>"+preStat);
			preStat.executeUpdate();
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method deleteRegistrationDetailbyUserId :: ERROR==>"+exp.getMessage());
			
			}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method deleteRegistrationDetailbyUserId :: Database Connection closed");
				}catch(Exception exp){exp.printStackTrace();}}
		}
	
	
	public  EduRegistrationTO saveEduStudentData(EduRegistrationTO EduRegistrationTOObj,EduRegistrationForm eduRegistrationFormObj)
	{
		EduRegistrationTO EduRegistrationToObj=null;
		try
		{
			PortableConstant PortableConstantObj = new PortableConstant();
			String partofstudentId=PortableConstantObj.getProperty(PortableConstant.PARTOFSTUDENTID);
			String partofparentId=PortableConstantObj.getProperty(PortableConstant.PARTOFPARENTID);

			getConnection();
			logger.debug("Method saveEduStudentData :: Database Connection opened");
			conn.setAutoCommit(false);
		    System.out.println("Before call of insert_student_detail_sp method"+EduRegistrationTOObj.getBirthday()+" "+EduRegistrationTOObj.getBirthmonth()+" "+EduRegistrationTOObj.getBirthyear());
			callStm = conn.prepareCall("{call insert_student_detail_sp(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");	
			callStm.setString(1,EduRegistrationTOObj.getFirstname());
			callStm.setString(2,EduRegistrationTOObj.getMiddlename());
			callStm.setString(3,EduRegistrationTOObj.getLastname());
			callStm.setString(4,EduRegistrationTOObj.getPassword());
			callStm.setString(5,EduRegistrationTOObj.getEmailid());
			callStm.setString(6,EduRegistrationTOObj.getRegistrationdate());
			callStm.setString(7,EduRegistrationTOObj.getAddress());
			callStm.setString(8,EduRegistrationTOObj.getPhoneno());
			callStm.setString(9,"A");
			callStm.setString(10,EduRegistrationTOObj.getPreviousinstitute());
			callStm.setString(11,EduRegistrationTOObj.getGender());
			callStm.setString(12,EduRegistrationTOObj.getFathername());
			callStm.setString(13,EduRegistrationTOObj.getMothername());
			callStm.setString(14,EduRegistrationTOObj.getParentqualification());
			callStm.setString(15,EduRegistrationTOObj.getParentemailid());
			callStm.setString(16,EduRegistrationTOObj.getParentpassword());
			callStm.setInt(17,EduRegistrationTOObj.getClassid());
			callStm.setString(18,EduRegistrationTOObj.getStudprequalification());
			callStm.setInt(19,EduRegistrationTOObj.getAdmissionfee());
			callStm.setInt(20,EduRegistrationTOObj.getConcessionamt());
			callStm.setString(21,EduRegistrationTOObj.getComment());
			callStm.setString(22,null);
			callStm.setInt(23,EduRegistrationTOObj.getBirthday());
			callStm.setInt(24,EduRegistrationTOObj.getBirthmonth());
			callStm.setInt(25,EduRegistrationTOObj.getBirthyear());
			callStm.setInt(26,EduRegistrationTOObj.getDept_id());
			callStm.setString(27,partofstudentId);
			callStm.setString(28,partofparentId);
			callStm.setInt(29,EduRegistrationTOObj.getSessionId());
			logger.debug("Method saveEduStudentData :: Procedure Executed==>"+callStm);
			callStm.executeUpdate();
			  if(eduRegistrationFormObj.getAssignsubjected()!=null && eduRegistrationFormObj.getAssignsubjected().length!=0)
			    {
			    	preStat = conn.prepareStatement("select max(student_id) student_id from sch_student_detail");
					rsObj = preStat.executeQuery();
					int studentid=-1;
					while(rsObj.next()){
						studentid =(rsObj.getInt("student_id"));	
					}
						for(int xx=0;xx<eduRegistrationFormObj.getAssignsubjected().length;xx++)
						{
						System.out.println("Registration data"+eduRegistrationFormObj.getAssignsubjected()[xx]);	
					    preStat = conn.prepareStatement(query.insertSubjectStudentid);
					    preStat.setInt(1, studentid);
					    preStat.setString(2, eduRegistrationFormObj.getAssignsubjected()[xx]);
					    preStat.setInt(3, EduRegistrationTOObj.getSessionId());
					    preStat.setInt(4, EduRegistrationTOObj.getClassid());
					    logger.debug("Method saveEduStudentData :: Query Executed for assigned classess==>"+preStat);
					    preStat.executeUpdate();
					}
						preStat = conn.prepareStatement(query.updateRegistrationstatus);
						preStat.setString(1, "S");
						preStat.setInt(2, EduRegistrationTOObj.getRegistrationid());
						preStat.setInt(3, EduRegistrationTOObj.getSessionId());
						preStat.executeUpdate();
			    }
			  
				 preStat = conn.prepareStatement("select ssd.student_id,first_name,middle_name,last_name,ssd.user_id,ssd.password,ssd.email_id,parent_id,father_name,spd.user_id parentuser,spd.password parentpassword,spd.email_id parentemailid from sch_student_detail ssd,sch_parent_detail spd  where ssd.student_id=spd.student_id and ssd.student_id =(select max(student_id) from sch_student_detail)");
				 rsObj = preStat.executeQuery();
				if(rsObj.next()){
					logger.debug("Method saveEduStaffData :: Before getting the Login Info");
					EduRegistrationToObj = new EduRegistrationTO(rsObj.getInt("student_id"),rsObj.getString("first_name"),rsObj.getString("middle_name"),rsObj.getString("last_name"),rsObj.getString("user_id"),rsObj.getString("password"),rsObj.getString("email_id"),rsObj.getInt("parent_id"),rsObj.getString("father_name"),rsObj.getString("parentuser"),rsObj.getString("parentpassword"),rsObj.getString("parentemailid"));
				}
		
			conn.commit();
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method saveEduStudentData :: ERROR==>"+exp.getMessage());
			try {
				conn.rollback();
				logger.debug("Method saveEduStudentData :: Transaction rollbacked");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method saveEduStudentData :: Database Connection closed");
		}catch(Exception exp){
			exp.printStackTrace();
		}}
		return EduRegistrationToObj;
	}

	
	 public  ArrayList<EduRegistrationTO> getDepartment()
	 {
	 	 ArrayList<EduRegistrationTO> eduRegistrationTObj=new  ArrayList<EduRegistrationTO>();
	 	try
	 	{
	 		getConnection();
	 		preStat = conn.prepareStatement(query.getDepartment);
	 		logger.debug("Method getDepartment :: Database Connection opened");
	 		preStat.setString(1,"Y");
	 		logger.debug("Method getDepartment :: Query Executed==>"+preStat);
	 		preStat.execute();	
	 		rsObj = preStat.getResultSet();
	 		 while(rsObj.next())
	 		    {
	 			EduRegistrationTO eduRegistrationTOObj =new EduRegistrationTO();
	 			eduRegistrationTOObj.setDept_title(rsObj.getString("dept_name"));
	 			eduRegistrationTOObj.setDept_id(rsObj.getInt("dept_id"));
	 			eduRegistrationTObj.add(eduRegistrationTOObj); 	
	 		    }
	 	}
	 	catch(Exception exp)
	 	{
	 		exp.printStackTrace();
	 		logger.debug("Method getDepartment :: ERROR==>"+exp.getMessage());
	 	}
	 	finally {
	 		try{
	 			closeConnection(conn,callStm,preStat,rsObj);
	 			logger.debug("Method getDepartment :: Database Connection closed");
	 			}catch(Exception exp){
	 				exp.printStackTrace();
	 			}}
	 	return eduRegistrationTObj;
	 	
	 }
	 

	 public ArrayList<EduRegistrationTO> getClassinfo(String userId) {

	 	ArrayList<EduRegistrationTO> users = new ArrayList<EduRegistrationTO>();
	 	
	 	try{
	 		getConnection();
	 		logger.debug("Method getClassinfo :: Database Connection opened");
	 		preStat = conn.prepareStatement(query.getSelectedSubjectForRegistration);
	 		preStat.setString(1, userId);
	 		logger.debug("Method getClassinfo :: Query Executed==>"+preStat);
	 		rsObj=preStat.executeQuery();
	 		while(rsObj.next())
	 		{
	 		EduRegistrationTO eduRegistrationTOObj = new EduRegistrationTO();
	 		eduRegistrationTOObj.setClassid(rsObj.getInt("class_id"));
	 		eduRegistrationTOObj.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
	 		users.add(eduRegistrationTOObj);
	 		}
	 	}catch (Exception exp) {
	 		exp.printStackTrace();
	 		logger.debug("Method getClassinfo :: ERROR==>"+exp.getMessage());
	 	}
	 	finally{
	 		try {
	 			closeConnection(conn,callStm,preStat,rsObj);
	 			logger.debug("Method getClassinfo :: Database Connection closed");
	 			} 
	 	catch (Exception exp) {exp.printStackTrace();}}
	 	return users;
	 }


}



