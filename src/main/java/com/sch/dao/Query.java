package com.sch.dao;

public class Query 

{
	public String checkUgAccess="select *  from sch_ug_access where  ug_id=?  and f_id=?";
	public String getTotalRecordfromnal_issue_reply="select count(*) noofrow from sch_issue ni join nal_issue_reply nir on ni.issue_id = nir.issue_id  join sch_users nu on nir.user_id = nu.user_id join sch_staff_heirarchy nsh on nsh.user_id = nir.user_id join sch_user_groups nug on nsh.ug_id = nug.ug_id where ni.issue_id = ? ";
	public String savenal_issue = "insert into sch_issue(issue_title,issue_desc,Priority,issue_submited_date,cat_id,creater_id,concern_status_id) values(?,?,?,(select now() from dual),?,?,?)";
	public String getConcernDetailByissueid = "select ss.issue_id,ss.issue_title,ss.issue_submited_date,ss.priority,ss.concern_status_id ,scc.category,ss.issue_desc,ss.reply_desc,scs.concern_status,ss.cat_id from sch_issue ss ,sch_concern_category scc,sch_concern_status scs where ss.cat_id=scc.cat_id and ss.concern_status_id =scs.concern_status_id and ss.issue_id=?";
	public String getAllCategory = "select cat_id,category from sch_concern_category";

	public String updateIssueTrackingDataByissueid = "update sch_issue set issue_title=?,issue_desc=?,cat_id=?,Priority=?,reply_desc=? where issue_id=?";
	public String deleteissuetrackingybyissueid = "delete from sch_issue where issue_id=?";
	public String replyIssueTrackingDataByissueid ="insert into sch_issue_reply (issue_id,user_id,reply_desc,issue_reply_date,status)values(?,?,?,(select now() from dual),?)";
	public String updatenalissuecurrentstatusbyissueid="update sch_issue set current_status=? where issue_id=?";
	public String deleteissuereply="delete from sch_issue_reply where reply_id=?";
	public String getIssuereplyByreplyId = "select reply_desc,status,reply_id,issue_id from sch_issue_reply  where reply_id=?";
	public String updateissuereply = "update sch_issue_reply set reply_desc=?,status=?,issue_reply_date=(select now() from dual) where reply_id=?";
	public String checkForExecutive = "select parent_ug_id from sch_staff_heirarchy nsh where nsh.user_id = ?";

	public String ugaccess = "Select af.F_ID,af.F_TITLE,af.PF_ID,af.F_URL from sch_functionalities af,sch_ug_access nua where af.F_DELETED = 0 and af.PF_ID =? and nua.f_id = af.f_id and nua.ug_id =? order by F_ID";
	public String setrow = "set @row =0;"; 
	public String loginUserInfo="insert into sch_login_info (user_id,user_name,update_timestamp) values(?,?,(select now() from dual))";
	public String getClassInfo="select class_id,class_name,section  from sch_class_detail";
	public String insertClassempid = "insert into sch_emp_class_detail(emp_id,class_id) values(?,?)";
	public String getStaffqualification = "select qualification_title,qualification_id  from sch_qualification_detail";
	public String getStaffLevel = "select ug_id,ug_title from sch_user_groups where ug_id not in(6) order by ug_pid ";
	public String getStaffDetailbyUserId="select first_name,last_name,middle_name,ssd.designation,salary,email_id,address,contact_no,ssd.qualification_id,previous_exp,joining_date,emp_id,dept_id,user_id,qualification_title,staff_designation_title from sch_staff_detail ssd,sch_qualification_detail sqd,sch_staff_designation ssdd where ssd.qualification_id=sqd.qualification_id and ssd.designation=ssdd.staff_designation_id and emp_id=? and emp_status=?";
	public String getSelectedClass="select scd.class_id,class_name,section  from sch_class_detail scd,sch_emp_class_detail secd ,sch_staff_detail ssd where scd.class_id=secd.class_id and secd.emp_id = ssd.emp_id and ssd.dept_id=? and secd.emp_id =?";
	public String getUnAssoclassbyUserid="select scd.class_id,class_name,section  from sch_class_detail scd where scd.class_id not in(select class_id from sch_emp_class_detail secd where secd.emp_id =?)";
	public String deleteStaffDetailbyUserId="update sch_staff_detail set emp_status='D' where emp_id=?";
	public String checkLoginUser="select ssd.emp_id,first_name,last_name,email_id,password,ug.ug_title,ug.ug_id,ug.welcome_message,ug.ug_code  from sch_staff_detail ssd,sch_user_groups ug ,sch_staff_heirarchy sh where email_id=? and password =? and ssd.emp_id = sh.user_id and ug.ug_id = sh.ug_id";
	public String getAdmissionfee="select class_id,class_name,section,admission_fee,monthly_fee,registration_fee  from sch_class_detail";
	public String getAdmfee="select class_id,class_name,section,admission_fee,monthly_fee,registration_fee  from sch_class_detail where class_id=?";
	public String getStudentDetailbyUserId="select ssd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.email_id,ssd.admission_date,ssd.contact_no,ssd.address,ssd.dateofbirth,ssd.previous_edu_center,ssd.gender,admission_fee,concession_amount,comment,class_id,preinstclass_id,spd.father_name,spd.mother_name,spd.father_higher_qualification_id,spd.email_id parentemail_id,ssd.birth_day,ssd.birth_month,ssd.birth_year from sch_student_detail ssd ,sch_student_class_detail sscd,sch_student_admissionfee_collection ssac,sch_parent_detail spd  where ssd.student_id=sscd.student_id and ssd.student_id=ssac.student_id and ssd.student_id=spd.student_id and ssd.student_id = ? and ssd.status='A' ";
	public String deleteStudentDetailbyUserId="update sch_student_detail set status='D' where student_id=?";
	public String getRegistrationDetailbyUserId=" select registration_no,first_name,last_name,contact_no,email_id,class_name,srd.class_id,father_name,middle_name,gender,address,dateofbirth,registration_date from sch_registration_detail srd ,sch_class_detail scd where srd.class_id=scd.class_id and srd.reg_status='R' and registration_no=? and session_id=?";
	public String deleteRegistrationDetailbyUserId="update sch_registration_detail set reg_status='R' where registration_no=? and session_id=?";
	public String getStudentPrequalification = "select qualification_title,qualification_id  from sch_prev_student_detail";
	public String getSession = "select session_id,session_year from  sch_student_session";
	public String viewStudentDetailbyUserId="select ssd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.email_id,ssd.admission_date,ssd.contact_no,ssd.address,ssd.dateofbirth,ssd.previous_edu_center,ssd.gender,scd.admission_fee,concession_amount,comment,scd.class_name,spsd.qualification_title student_prev_qualification,sqd.qualification_title parent_higher_qualification,spd.father_name,spd.mother_name,spd.email_id parentemail_id,ssd.birth_day,ssd.birth_month,ssd.birth_year from sch_student_detail ssd ,sch_student_class_detail sscd,sch_student_admissionfee_collection ssac,sch_parent_detail spd,sch_class_detail scd,sch_qualification_detail sqd ,sch_prev_student_detail spsd where ssd.student_id=sscd.student_id and ssd.student_id=ssac.student_id and ssd.student_id=spd.student_id  and scd.class_id = sscd.class_id and sqd.qualification_id = spd.father_higher_qualification_id and spsd.qualification_id = preinstclass_id and ssd.status='A' and ssd.student_id=?";
	public String getDepartment = "select dept_id,dept_name  from sch_department_detail where dept_status=?";
	public String insertSubjectEmpid="insert into sch_emp_subject_detail(emp_id,subject_id) values(?,?)";
	public String getSelectedSubject ="select sesd.subject_id,ssd.subject_name  from sch_subject_detail ssd,sch_emp_subject_detail sesd,sch_staff_detail ssfd where ssd.subject_id = sesd.subject_id and sesd.emp_id = ssfd.emp_id and ssfd.dept_id=? and sesd.emp_id =?";
	public String insertSubjectStudentid="insert into sch_student_subject_detail(student_id,subject_id,session_id,class_id) values(?,?,?,?)";
	public String getClassInfobyUserid="select sdd.dept_id,scd.class_id,sdd.dept_name,scd.class_name,section from sch_department_detail sdd, sch_dept_class_relation sdcr,sch_class_detail scd where  sdd.dept_id=sdcr.dept_id and sdcr.class_id=scd.class_id and sdd.dept_id=? order by scd.class_id";
	public String getSelectedSubjectForStudent ="select distinct sssd.subject_id,ssd.subject_name  from sch_subject_detail ssd,sch_student_subject_detail sssd,sch_student_detail stud,sch_student_class_detail sscd where ssd.subject_id = sssd.subject_id and stud.student_id = sssd.student_id and sscd.session_id=sssd.session_id and sscd.student_id = stud.student_id and sscd.class_id=? and stud.student_id=? and sscd.session_id=? order by sssd.subject_id";
	public String getSelectedSubjectForRegistration="select  distinct sdd.dept_id,scd.class_id,sdd.dept_name,scd.class_name,section from sch_department_detail sdd, sch_dept_class_relation sdcr,sch_class_detail scd ,sch_registration_detail srd where  sdd.dept_id=sdcr.dept_id  and sdcr.class_id = scd.class_id and sdcr.dept_id=srd.dept_id and srd.registration_no=?";
	public String getClassNameBySessionId= "select  scd.class_name,scd.section,scd.monthly_fee,scd.class_id  from sch_student_class_detail sscd,sch_class_detail scd where sscd.student_id=? and sscd.session_id=? and sscd.class_id=scd.class_id";
	public String getMonthList = "select month_id,month_name from sch_month_detail";
	public String insertFeePayment="insert into sch_fee_collection(receipt_no,month_id) values(?,?)";
	public String getPayedMonthList = "select sfc. month_id,month_name from sch_month_detail smd,sch_fee_collection sfc,sch_fee_detail sfd where smd.month_id=sfc.month_id and sfd.receipt_no=sfc.receipt_no and sfc.receipt_no=? and sfd.admission_no=? and session_id=? order by smd. month_id";
	public String updateRegistrationstatus="update sch_registration_detail set reg_status=? where registration_no=? and session_id=?";
	public String updateConcernToReply="update sch_issue set reply_desc=?,concern_status_id=?,issue_reply_date=(select now() from dual) where issue_id=?";
	public String getConcernStatus="select concern_status_id,concern_status from sch_concern_status";
	public String getTeacherList="select ssd.emp_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.email_id from sch_staff_detail ssd ,sch_staff_designation ssde where ssd.designation=ssde.staff_designation_id and ssd.designation=3 order by ssd.first_name";
	public String getClassnameByclass_id="select class_name,section,class_id from sch_class_detail where class_id=?";
	public String getBookType = "select book_type_id,book_type_name from library_book_type";
	public String getRackIds="select lri.rack_id,lri.rack_name from library_book_type lbt,library_rack_book_type_relation lrbbtr,library_rack_info lri where lbt.book_type_id = lrbbtr.book_type_id and lrbbtr.rack_id=lri.rack_id and  lrbbtr.book_type_id=?";
	public String getMaxBookId="select (max(bm_id)) maxbookId  from library_book_master";
	public String getPrefixBookId="select prefix_book_id from library_book_type where book_type_id=?";
	public String addBookdetails="insert into library_book_master(book_name,number_of_copy,book_type,rack_id) values(?,?,?,?)";
	public String insertBookIds="insert into library_copy_of_books(bm_id,book_id,assigned_status) values(?,?,?)";
	public String getBookByName="select bm_id,number_of_copy from library_book_master where book_name=?";
	public String updateBookdetails="update library_book_master set number_of_copy=? where bm_id=?";
	public String getMaxlibrarybookId="select (max(book_copy_id)) maxbookId  from library_copy_of_books";
	public String getBookNames="select book_name from library_book_master";
	public String getAllbooks="select book_name,number_of_copy,book_type_name,rack_name,(number_of_copy- IFNULL(assigned_count,0)) available_count  from library_book_master lbm ,library_book_type lbt,library_rack_info lri  where lbm.book_type =  lbt.book_type_id and lri.rack_id = lbm.rack_id";
	public String getAssignedbooks="select ssd.first_name,ssd.middle_name,ssd.last_name, lbm.book_name,lbi.book_id,lbi.issue_date,lbi.due_date from library_book_issue lbi,library_copy_of_books lcob,library_book_master lbm,sch_student_detail ssd where lbi.return_status='N' and lcob.book_id=lbi.book_id and lcob.bm_id=lbm.bm_id and user_type='S' and lbi.user_id = ssd.user_id";
}