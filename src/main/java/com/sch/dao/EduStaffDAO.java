package com.sch.dao;
import java.sql.*;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import com.sch.constant.PortableConstant;
import com.sch.form.EduStaffForm;
import com.sch.to.*;

public class EduStaffDAO extends ConnectionUtil

{
	Query query =null;
    ResultSet rsObj = null; 
    PreparedStatement preStat=null;	
    CallableStatement callStm = null;
    public static Logger logger = Logger.getLogger(EduStaffDAO.class);
	ArrayList<EduStaffTO> arraylistobj = new ArrayList<EduStaffTO>();
	public EduStaffDAO()
	{
		 query = new Query();  
	}
	
	public  ArrayList<EduStaffTO> getStaffDetails(int totalRecord,int startIndex,String sort,String dir,String SearchKey) 
	{
	   	ArrayList<EduStaffTO> eduStaffTOArrayObj = new ArrayList<EduStaffTO>();
	    try { 
	    	getConnection();
	    	logger.debug("Method getStaffDetails :: Database Connection opened");
	    	callStm = conn.prepareCall("{call sp_staff_detail_list(?,?,?,?,?)}");	
	    	callStm.setInt(1,totalRecord);
	    	callStm.setInt(2,startIndex);
	    	callStm.setString(3,sort);
	    	callStm.setString(4,dir);
	    	callStm.setString(5,SearchKey);
	    	callStm.execute();
	    	rsObj = callStm.getResultSet();
	    	logger.debug("Method getStaffDetails :: Procedure Executed==>"+callStm);
    		while(rsObj.next())	
	    	{
    			EduStaffTO eduStaffTOObj = new EduStaffTO();
    			eduStaffTOObj.setFirstname(rsObj.getString("first_name"));
    			eduStaffTOObj.setLastname(rsObj.getString("last_name"));
    			eduStaffTOObj.setPhoneno(rsObj.getString("contact_no"));
    			eduStaffTOObj.setLevel(rsObj.getString("staff_designation_title"));
    			eduStaffTOObj.setEmailid(rsObj.getString("email_id"));
    			eduStaffTOObj.setEmpid(rsObj.getInt("emp_id"));
	    	 	eduStaffTOArrayObj.add(eduStaffTOObj);
	    	}
    		  
		}
	    catch (Exception exception)
	    {
	    	exception.printStackTrace();
	    	logger.debug("ERROR:: "+exception.getMessage());
	 	}
	    finally 
	    {
	    	try{	
	    		closeConnection(conn,callStm,preStat,rsObj);
	    		logger.debug("Method getStaffDetails :: Database Connection closed "+conn);
	    	} catch (SQLException e) {
	    		e.printStackTrace();	
	    	}
	    }
		return eduStaffTOArrayObj;
	}
	public  ArrayList<EduStaffTO> getAlumniStaffDetails(int totalRecord,int startIndex,String sort,String dir,String SearchKey) 
	{
	   	ArrayList<EduStaffTO> eduAlumniStaffTOArrayObj = new ArrayList<EduStaffTO>();
	    try { 
	    	getConnection();
	    	logger.debug("Method getAlumniStaffDetails :: Database Connection opened");
	    	callStm = conn.prepareCall("{call sp_alumni_staff_detail_list(?,?,?,?,?)}");	
	    	callStm.setInt(1,totalRecord);
	    	callStm.setInt(2,startIndex);
	    	callStm.setString(3,sort);
	    	callStm.setString(4,dir);
	    	callStm.setString(5,SearchKey);
	    	callStm.execute();
	    	rsObj = callStm.getResultSet();
	    	logger.debug("Method getAlumniStaffDetails :: Procedure Executed==>"+callStm);
    		while(rsObj.next())	
	    	{
    			EduStaffTO eduStaffTOObj = new EduStaffTO();
    			eduStaffTOObj.setFirstname(rsObj.getString("first_name"));
    			eduStaffTOObj.setLastname(rsObj.getString("last_name"));
    			eduStaffTOObj.setPhoneno(rsObj.getString("contact_no"));
    			eduStaffTOObj.setLevel(rsObj.getString("staff_designation_title"));
    			eduStaffTOObj.setEmailid(rsObj.getString("email_id"));
    			eduStaffTOObj.setEmpid(rsObj.getInt("emp_id"));
    			eduAlumniStaffTOArrayObj.add(eduStaffTOObj);
	    	}
    		  
		}
	    catch (Exception exception)
	    {
	    	exception.printStackTrace();
	    	logger.debug("ERROR:: "+exception.getMessage());
	 	}
	    finally 
	    {
	    	try{	
	    		closeConnection(conn,callStm,preStat,rsObj);
	    		logger.debug("Method getAlumniStaffDetails :: Database Connection closed "+conn);
	    	} catch (SQLException e) {
	    		e.printStackTrace();	
	    	}
	    }
		return eduAlumniStaffTOArrayObj;
	}
	public  EduStaffTO saveEduStaffData(EduStaffTO eduStaffTOObj,EduStaffForm eduStaffFormObj)
	{
		EduStaffTO eduStaffTOobj = null;
		try
		{
			PortableConstant PortableConstantObj = new PortableConstant();
			String partofEmpId=PortableConstantObj.getProperty(PortableConstant.PARTOFEMPID);
			 
			getConnection();
			logger.debug("Method saveEduStaffData :: Database Connection opened");
			conn.setAutoCommit(false);
			callStm = conn.prepareCall("{call insert_staff_detail_sp(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");	
			callStm.setString(1,eduStaffTOObj.getFirstname());
			callStm.setString(2,eduStaffTOObj.getMiddlename());
			callStm.setString(3,eduStaffTOObj.getLastname());
			callStm.setString(4,eduStaffTOObj.getEmailid());
			callStm.setString(5,eduStaffTOObj.getPassword());
			callStm.setString(6,eduStaffTOObj.getSalary());
			callStm.setString(7,eduStaffTOObj.getAddress());
			callStm.setString(8,eduStaffTOObj.getPhoneno());
			callStm.setString(9,eduStaffTOObj.getQualification());
			callStm.setString(10,"A");
			callStm.setString(11,eduStaffTOObj.getPreviousexp());
			callStm.setString(12,eduStaffTOObj.getJoiningdate());
			callStm.setString(13,eduStaffTOObj.getLevel());
			callStm.setInt(14,eduStaffTOObj.getDept_id());
			callStm.setString(15,partofEmpId);
			callStm.executeUpdate();
			logger.debug("Method saveEduStaffData :: Procedure Executed==>"+callStm);
		    if(eduStaffFormObj.getAssignclassed()!=null && eduStaffFormObj.getAssignclassed().length!=0 && eduStaffTOObj.getLevel().equalsIgnoreCase("3"))
		    {
		    	preStat = conn.prepareStatement("select max(emp_id)emp_id from sch_staff_detail");
				rsObj = preStat.executeQuery();
				int empid=-1;
				while(rsObj.next()){
					empid =(rsObj.getInt("emp_id"));	
				}
					for(int xx=0;xx<eduStaffFormObj.getAssignclassed().length;xx++)
					{
				    preStat = conn.prepareStatement(query.insertClassempid);
				    preStat.setInt(1, empid);
				    preStat.setString(2, eduStaffFormObj.getAssignclassed()[xx]);
				    logger.debug("Method saveEduStaffData :: Query Executed for assigned classess==>"+preStat);
				    preStat.executeUpdate();
					
				}
		    }
		    
		    if(eduStaffFormObj.getAssignsubjected()!=null && eduStaffFormObj.getAssignsubjected().length!=0 && eduStaffTOObj.getLevel().equalsIgnoreCase("3"))
		    {
		    	preStat = conn.prepareStatement("select max(emp_id)emp_id from sch_staff_detail");
				rsObj = preStat.executeQuery();
				int empid=-1;
				while(rsObj.next()){
					empid =(rsObj.getInt("emp_id"));	
				}
					for(int xx=0;xx<eduStaffFormObj.getAssignsubjected().length;xx++)
					{
				    preStat = conn.prepareStatement(query.insertSubjectEmpid);
				    preStat.setInt(1, empid);
				    preStat.setString(2, eduStaffFormObj.getAssignsubjected()[xx]);
				    preStat.executeUpdate();
				    logger.debug("Method saveEduStaffData :: Query Executed for assigned subjects==>"+preStat);
				}
		    }
		    
		    preStat = conn.prepareStatement("select emp_id,first_name,middle_name,last_name,user_id,password,staff_designation_title,email_id from sch_staff_detail ssd,sch_staff_designation sstd where (emp_id=(select max(emp_id) from sch_staff_detail )) and sstd.staff_designation_id=ssd.designation");
			rsObj = preStat.executeQuery();
			logger.debug("Method saveEduStaffData :: Query Executed for assigned subjects==>"+preStat);
			if(rsObj.next()){
				logger.debug("Method saveEduStaffData :: Before getting the Login Info");
				eduStaffTOobj = new EduStaffTO(rsObj.getInt("emp_id"),rsObj.getString("first_name"),rsObj.getString("middle_name"),rsObj.getString("last_name"),rsObj.getString("user_id"),rsObj.getString("staff_designation_title"),rsObj.getString("email_id"));
			}

		   conn.commit();
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method saveEduStaffData :: ERROR==>"+exp.getMessage());
			try {
				conn.rollback();
				logger.debug("Method saveEduStaffData :: Transaction rollbacked");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method saveEduStaffData :: Database Connection closed");
		}catch(Exception exp){
			exp.printStackTrace();
		}}
		
		return eduStaffTOobj;
	}

	public  void updateStaffData(EduStaffTO eduStaffTOObj,EduStaffForm edustaffFormObj)
	{
		CallableStatement callStm = null;
		try
		{
			getConnection();
			logger.debug("Method updateStaffData :: Database Connection opened");
			conn.setAutoCommit(false);
			callStm = conn.prepareCall("{call update_StaffDetail_sp(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			callStm.setString(1,eduStaffTOObj.getFirstname());
			callStm.setString(2,eduStaffTOObj.getMiddlename());
			callStm.setString(3,eduStaffTOObj.getLastname());
			callStm.setString(4,eduStaffTOObj.getEmailid());
			callStm.setString(5,eduStaffTOObj.getSalary());
			callStm.setString(6,eduStaffTOObj.getAddress());
			callStm.setString(7,eduStaffTOObj.getPhoneno());
			callStm.setString(8,eduStaffTOObj.getQualification());
			callStm.setString(9,eduStaffTOObj.getPreviousexp());
			callStm.setString(10,eduStaffTOObj.getJoiningdate());
			callStm.setString(11,eduStaffTOObj.getLevel());
			callStm.setInt(12,eduStaffTOObj.getUserid());
			callStm.setInt(13,eduStaffTOObj.getDept_id());
			logger.debug("Method updateStaffData :: Procedure Executed==>"+callStm);
			callStm.executeUpdate();
			if(edustaffFormObj.getAssignclassed()!=null && edustaffFormObj.getAssignclassed().length!=0 && eduStaffTOObj.getLevel().equalsIgnoreCase("3") )
			    {
					  for(int xx=0;xx<edustaffFormObj.getAssignclassed().length;xx++)
						{
						  preStat = conn.prepareStatement(query.insertClassempid);
						  preStat.setInt(1, eduStaffTOObj.getUserid());
						  preStat.setString(2, edustaffFormObj.getAssignclassed()[xx]);
						  preStat.executeUpdate();
						  logger.debug("Method updateStaffData :: Query Executed for assigned classes==>"+preStat);

						}
			    }
			 
			   if(edustaffFormObj.getAssignsubjected()!=null && edustaffFormObj.getAssignsubjected().length!=0 && eduStaffTOObj.getLevel().equalsIgnoreCase("3"))
			    {
				   	  for(int xx=0;xx<edustaffFormObj.getAssignsubjected().length;xx++)
						{
					    preStat = conn.prepareStatement(query.insertSubjectEmpid);
					    preStat.setInt(1, eduStaffTOObj.getUserid());
					    preStat.setString(2, edustaffFormObj.getAssignsubjected()[xx]);
					    preStat.executeUpdate();
					    logger.debug("Method updateStaffData :: Query Executed for assigned subjects==>"+preStat);

					}
			    }
		    conn.commit();
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method updateStaffData :: ERROR==>"+exp.getMessage());
			try {
				conn.rollback();
				logger.debug("Method updateStaffData :: Transaction rollbacked");
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method updateStaffData :: Database Connection closed");
		}catch(Exception exp){
			exp.printStackTrace();
		}}
		
	}



public  ArrayList<EduStaffTO> getStaffQualification()
{
	 ArrayList<EduStaffTO> eduStaffTOArrayObj=new  ArrayList<EduStaffTO>();
	try
	{
		getConnection();
		logger.debug("Method getStaffQualification :: Database Connection opened");
		preStat = conn.prepareStatement(query.getStaffqualification);
		logger.debug("Method getStaffQualification :: Query Executed==>"+preStat);
		rsObj = preStat.executeQuery();	
		
		 while(rsObj.next())
		    {
			 EduStaffTO eduStaffTOObj =new EduStaffTO();
			 eduStaffTOObj.setQualification_title(rsObj.getString("qualification_title"));
			 eduStaffTOObj.setQualification_id(rsObj.getInt("qualification_id"));
		     eduStaffTOArrayObj.add(eduStaffTOObj); 	
		    }
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		logger.debug("Method getStaffQualification :: ERROR==>"+exp.getMessage());
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getStaffQualification :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}
	return eduStaffTOArrayObj;
	
}

public  ArrayList<EduStaffTO> getDepartment()
{
	 ArrayList<EduStaffTO> eduStaffTOArrayObj=new  ArrayList<EduStaffTO>();
	try
	{
		getConnection();
		logger.debug("Method getDepartment :: Database Connection opened");
		preStat = conn.prepareStatement(query.getDepartment);	 
		preStat.setString(1,"Y");
		logger.debug("Method getDepartment :: Query Executed==>"+preStat);
		preStat.execute();	
		rsObj = preStat.getResultSet();
		 while(rsObj.next())
		    {
			 EduStaffTO eduStaffTOObj =new EduStaffTO();
			 eduStaffTOObj.setDept_title(rsObj.getString("dept_name"));
			 eduStaffTOObj.setDept_id(rsObj.getInt("dept_id"));
		     eduStaffTOArrayObj.add(eduStaffTOObj); 	
		    }
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		logger.debug("Method getDepartment :: ERROR==>"+exp.getMessage());
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getDepartment :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}
	return eduStaffTOArrayObj;
	
}

public  ArrayList<EduStaffTO> getStaffLevel()
{
	 ArrayList<EduStaffTO> eduStaffTOArrayObj=new  ArrayList<EduStaffTO>();
	try
	{
		getConnection();
		logger.debug("Method getStaffLevel :: Database Connection opened");
		preStat = conn.prepareStatement(query.getStaffLevel);	
		logger.debug("Method getStaffLevel :: Query Executed==>"+preStat);
		rsObj = preStat.executeQuery();
		
		while(rsObj.next())
		    {
			 EduStaffTO eduStaffTOObj =new EduStaffTO();
			 eduStaffTOObj.setDesignation_title(rsObj.getString("ug_title"));
			 eduStaffTOObj.setDesignation_id(rsObj.getInt("ug_id"));
		     eduStaffTOArrayObj.add(eduStaffTOObj); 	
		    }
	}
	catch(Exception exp)
	{
		exp.printStackTrace();
		logger.debug("Method getStaffLevel :: ERROR==>"+exp.getMessage());
	}
	finally {
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getStaffLevel :: Database Connection closed");
			}catch(Exception exp){
				exp.printStackTrace();
			}}
	return eduStaffTOArrayObj;
	
}
public ArrayList<EduStaffTO> getSelectedClass(String userid,String deptid) {

	ArrayList<EduStaffTO> users = new ArrayList<EduStaffTO>();
	try{
		getConnection();
		logger.debug("Method getSelectedClass :: Database Connection opened");
		preStat = conn.prepareStatement(query.getSelectedClass);
		preStat.setString(1,deptid);
		preStat.setString(2,userid);
		logger.debug("Method getSelectedClass :: Query Executed==>"+preStat);
		rsObj=preStat.executeQuery();
		
		while(rsObj.next())
		{
		EduStaffTO edustaffObj = new EduStaffTO();
		edustaffObj.setClassid(rsObj.getInt("class_id"));
		edustaffObj.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
		users.add(edustaffObj);
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getSelectedClass :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getSelectedClass :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}

public ArrayList<EduStaffTO> getSelectedSubject(String userid,String deptid) {

	ArrayList<EduStaffTO> users = new ArrayList<EduStaffTO>();
	try{
		getConnection();
		logger.debug("Method getSelectedSubject :: Database Connection opened");
		preStat = conn.prepareStatement(query.getSelectedSubject);
		preStat.setString(1,deptid);
		preStat.setString(2,userid);
		rsObj=preStat.executeQuery();
		logger.debug("Method getSelectedSubject :: Query Executed==>"+preStat);
		while(rsObj.next())
		{
		EduStaffTO edustaffObj = new EduStaffTO();
		edustaffObj.setSubjectid(rsObj.getInt("subject_id"));
		edustaffObj.setSubjectname(rsObj.getString("subject_name"));
		users.add(edustaffObj);
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getSelectedSubject :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getSelectedSubject :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}

public ArrayList<EduStaffTO> getClassinfo() {

	ArrayList<EduStaffTO> users = new ArrayList<EduStaffTO>();
	
	try{
		getConnection();
		logger.debug("Method getClassinfo :: Database Connection opened");
		preStat = conn.prepareStatement(query.getClassInfo);
		rsObj=preStat.executeQuery();
		logger.debug("Method getClassinfo :: Query Executed==>"+preStat);
		while(rsObj.next())
		{
		EduStaffTO edustaffObj = new EduStaffTO();
		edustaffObj.setClassid(rsObj.getInt("class_id"));
		edustaffObj.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
		users.add(edustaffObj);
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getClassinfo :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getClassinfo :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}
public ArrayList<EduStaffTO> getClassinfoByDepartment(String dept_id) {

	ArrayList<EduStaffTO> users = new ArrayList<EduStaffTO>();
	try{
		getConnection();
		logger.debug("Method getClassinfoByDepartment :: Database Connection opened");
		callStm = conn.prepareCall("{call getClass_Detail_By_dept_id_sp(?)}");
		callStm.setString(1, dept_id);
		rsObj=callStm.executeQuery();
		logger.debug("Method getClassinfoByDepartment :: Procedure Executed==>"+callStm);
		while(rsObj.next())
		{
		EduStaffTO edustaffObj = new EduStaffTO();
		edustaffObj.setClassid(rsObj.getInt("class_id"));
		edustaffObj.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
		users.add(edustaffObj);
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getClassinfoByDepartment :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getClassinfoByDepartment :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}
public ArrayList<EduStaffTO> getSubjectByDept(String dept_id) {

	ArrayList<EduStaffTO> users = new ArrayList<EduStaffTO>();
	    System.out.println("dept_id"+dept_id);
	try{
		getConnection();
		logger.debug("Method getSubjectByDept :: Database Connection opened");
		callStm = conn.prepareCall("{call getSubjectByDepartment_sp(?)}");
		callStm.setString(1, dept_id);
		logger.debug("Method getSubjectByDept :: Procedure Executed==>"+callStm);
		rsObj=callStm.executeQuery();
		while(rsObj.next())
		{
		EduStaffTO edustaffObj = new EduStaffTO();
		edustaffObj.setClassid(rsObj.getInt("subject_id"));
		edustaffObj.setClassname(rsObj.getString("subject_name"));
		users.add(edustaffObj);
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getSubjectByDept :: ERROR==>"+exp.getMessage());
	}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getSubjectByDept :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}
public ArrayList<EduStaffTO> getUnAssoClassInfo(String userid) {

	ArrayList<EduStaffTO> users = new ArrayList<EduStaffTO>();
	try{
		getConnection();
		logger.debug("Method getUnAssoClassInfo :: Database Connection opened");
		preStat = conn.prepareStatement(query.getUnAssoclassbyUserid);
		preStat.setString(1, userid);
		rsObj=preStat.executeQuery();
		logger.debug("Method getUnAssoClassInfo :: Query Executed==>"+preStat);
		while(rsObj.next())
		{
		EduStaffTO edustaffObj = new EduStaffTO();
		edustaffObj.setClassid(rsObj.getInt("class_id"));
		edustaffObj.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
		users.add(edustaffObj);
		}
	}catch (Exception exp) {
		exp.printStackTrace();
		logger.debug("Method getUnAssoClassInfo :: ERROR==>"+exp.getMessage());
		}
	finally{
		try {
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getUnAssoClassInfo :: Database Connection closed");
			} 
	catch (Exception exp) {exp.printStackTrace();}}
	return users;
}

public EduStaffTO getEduStaffDatabyUserId(String user_Id,String emp_status)
{
	EduStaffTO EduStaffTOObj = new EduStaffTO();
	try
	{    
		getConnection();
		logger.debug("Method getEduStaffDatabyUserId :: Database Connection opened");
		preStat = conn.prepareStatement(query.getStaffDetailbyUserId);
		preStat.setInt(1,Integer.valueOf(user_Id));
		preStat.setString(2,emp_status);
		rsObj = preStat.executeQuery();
		logger.debug("Method getEduStaffDatabyUserId :: Query Executed==>"+preStat);
		if(rsObj.next())
		{   
			EduStaffTOObj.setFirstname(rsObj.getString("first_name"));
			EduStaffTOObj.setMiddlename(rsObj.getString("middle_name"));
			EduStaffTOObj.setLastname(rsObj.getString("last_name"));
			EduStaffTOObj.setDesignation_id(rsObj.getInt("designation"));
			EduStaffTOObj.setPhoneno(rsObj.getString("contact_no"));
			EduStaffTOObj.setAddress(rsObj.getString("address"));
			EduStaffTOObj.setSalary(rsObj.getString("salary"));
			EduStaffTOObj.setPreviousexp(rsObj.getString("previous_exp"));
			EduStaffTOObj.setQualification_id(rsObj.getInt("qualification_id"));
			EduStaffTOObj.setEmailid(rsObj.getString("email_id"));
			EduStaffTOObj.setJoiningdate(rsObj.getString("joining_date"));
			EduStaffTOObj.setUserid(rsObj.getInt("emp_id"));
			EduStaffTOObj.setDept_id(rsObj.getInt("dept_id"));
			EduStaffTOObj.setLoginuser(rsObj.getString("user_id"));
			EduStaffTOObj.setDesignation_title(rsObj.getString("staff_designation_title"));
			EduStaffTOObj.setQualification_title(rsObj.getString("qualification_title"));
		}
		
	}
	catch(Exception exp){
		exp.printStackTrace();
		logger.debug("Method getEduStaffDatabyUserId :: ERROR==>"+exp.getMessage());
		}
	finally{
		try{
			closeConnection(conn,callStm,preStat,rsObj);
			logger.debug("Method getEduStaffDatabyUserId :: Database Connection closed");
			}catch(Exception exp){exp.printStackTrace();}}
	return EduStaffTOObj;
}
	public void deleteStaffDetailbyUserId(String user_Id)
	{
		try
		{
			getConnection();
			logger.debug("Method deleteStaffDetailbyUserId :: Database Connection opened");
			preStat = conn.prepareStatement(query.deleteStaffDetailbyUserId);
			preStat.setString(1,user_Id);
			preStat.executeUpdate();
			logger.debug("Method deleteStaffDetailbyUserId :: Query Executed==>"+preStat);
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
			logger.debug("Method deleteStaffDetailbyUserId :: ERROR==>"+exp.getMessage());
		}
		finally {
			try{
				closeConnection(conn,callStm,preStat,rsObj);
				logger.debug("Method deleteStaffDetailbyUserId :: Database Connection closed");
				}catch(Exception exp){exp.printStackTrace();}}
	}
	
	 public ArrayList<EduStudentReportTO> getSubjectClassByUser(String user_Id){
		 
			ArrayList<EduStudentReportTO> eduStudentReportArrayObj = new ArrayList<EduStudentReportTO>();
			
		    try { 
		    	getConnection();
		    	logger.debug("Method getSubjectClassByStudent :: Database Connection opened");
		    	callStm = conn.prepareCall("{call sp_staff_class_session_details(?)}");	
		    	callStm.setString(1,user_Id);
		    	callStm.execute();
		    	rsObj = callStm.getResultSet();
		    	logger.debug("Method getSubjectClassByStudent :: Procedure Executed==>"+callStm);
		    	String class_name="";
		    	EduStudentReportTO eduStudentReportTOobj = new EduStudentReportTO();
		    	while(rsObj.next()){
		    		
		    		class_name = class_name+" "+rsObj.getString("class_name")+" "+rsObj.getString("section");
		    		eduStudentReportTOobj.setStudentid(rsObj.getInt("emp_id"));
					eduStudentReportTOobj.setClassname(class_name);
					eduStudentReportTOobj.setDepartmentname(rsObj.getString("dept_name"));
					
				}
		    	
		    	callStm = conn.prepareCall("{call sp_staff_subject_class_details(?)}",ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);	
		    	callStm.setString(1,user_Id);
		    	callStm.execute();
		    	ResultSet rssubObj = callStm.getResultSet();
		    	int recount=0;
	    		rssubObj.last();
	    		int rows = rssubObj.getRow();
	    		System.out.println("total rows========"+rows);
	    		String []subject_name = new String[rows];
	    		rssubObj.beforeFirst(); 	
		    		while(rssubObj.next()){
			    		subject_name[recount]=rssubObj.getString("subject_name");
			    		eduStudentReportTOobj.setSubject(subject_name);
			    		recount++;
			    	}
				eduStudentReportArrayObj.add(eduStudentReportTOobj);
		    	
		    }
		    catch (Exception exception)
		    {
		    	exception.printStackTrace();
		    	logger.debug("ERROR:: "+exception.getMessage());
		    }
		    finally 
		    {
		    	try{	
		    		closeConnection(conn,callStm,preStat,rsObj);
		    		logger.debug("Method getSubjectClassByStudent :: Database Connection closed "+conn);
		    	} catch (SQLException e) 
		    	{
		    		e.printStackTrace();
		    	}
		    }
			return eduStudentReportArrayObj; 
		}
	 
	 public ArrayList<EduStaffTO> getTeacherList(String selectionFlag) {

			ArrayList<EduStaffTO> users = new ArrayList<EduStaffTO>();
			try{
				getConnection();
				logger.debug("Method getTeacherList :: Database Connection opened");
				callStm = conn.prepareCall("{call sp_teacher_list(?)}");	
		    	callStm.setString(1,selectionFlag);
				rsObj=callStm.executeQuery();
				logger.debug("Method getTeacherList :: Query Executed==>"+preStat);
				while(rsObj.next())
				{
				EduStaffTO edustaffObj = new EduStaffTO();
				edustaffObj.setEmpid(rsObj.getInt("emp_id"));
				edustaffObj.setEmailid(rsObj.getString("email_id"));
				edustaffObj.setFirstname(rsObj.getString("first_name")+" "+rsObj.getString("middle_name")+" "+rsObj.getString("last_name")+"<--->"+rsObj.getString("email_id"));
				users.add(edustaffObj);
				}
			}catch (Exception exp) {
				exp.printStackTrace();
				logger.debug("Method getTeacherList :: ERROR==>"+exp.getMessage());
			}
			finally{
				try {
					closeConnection(conn,callStm,preStat,rsObj);
					logger.debug("Method getTeacherList :: Database Connection closed");
					} 
			catch (Exception exp) {exp.printStackTrace();}}
			return users;
		}
	 
	 public EduStaffTO getClassname(String class_id){
		 
		 EduStaffTO edustaffObj = new EduStaffTO();
		 try{
				getConnection();
				logger.debug("Method getClassname :: Database Connection opened");
				preStat = conn.prepareStatement(query.getClassnameByclass_id);
				preStat.setString(1, class_id);
				rsObj=preStat.executeQuery();
				logger.debug("Method getClassname :: Query Executed==>"+preStat);
				if(rsObj.next())
				{
				
				edustaffObj.setClassid(rsObj.getInt("class_id"));
				edustaffObj.setClassname(rsObj.getString("class_name")+" "+rsObj.getString("section"));
				}
			}catch (Exception exp) {
				exp.printStackTrace();
				logger.debug("Method getClassname :: ERROR==>"+exp.getMessage());
				}
			finally{
				try {
					closeConnection(conn,callStm,preStat,rsObj);
					logger.debug("Method getClassname :: Database Connection closed");
					} 
			catch (Exception exp) {exp.printStackTrace();}}
			return edustaffObj;

	 }
	 
	 
}