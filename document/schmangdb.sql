CREATE DATABASE  IF NOT EXISTS `schmangdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `schmangdb`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: schmangdb
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ajax`
--

DROP TABLE IF EXISTS `ajax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ajax` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `engine` varchar(255) NOT NULL DEFAULT '',
  `browser` varchar(255) NOT NULL DEFAULT '',
  `platform` varchar(255) NOT NULL DEFAULT '',
  `version` float NOT NULL DEFAULT '0',
  `grade` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ajax`
--

LOCK TABLES `ajax` WRITE;
/*!40000 ALTER TABLE `ajax` DISABLE KEYS */;
INSERT INTO `ajax` VALUES (1,'Trident','Internet Explorer 4.0','Win 95+',4,'X'),(2,'Trident','Internet Explorer 5.0','Win 95+',5,'C'),(3,'Trident','Internet Explorer 5.5','Win 95+',5.5,'A'),(4,'Trident','Internet Explorer 6','Win 98+',6,'A'),(5,'Trident','Internet Explorer 7','Win XP SP2+',7,'A'),(6,'Trident','AOL browser (AOL desktop)','Win XP',6,'A'),(7,'Gecko','Firefox 1.0','Win 98+ / OSX.2+',1.7,'A'),(8,'Gecko','Firefox 1.5','Win 98+ / OSX.2+',1.8,'A'),(9,'Gecko','Firefox 2.0','Win 98+ / OSX.2+',1.8,'A'),(10,'Gecko','Firefox 3.0','Win 2k+ / OSX.3+',1.9,'A'),(11,'Gecko','Camino 1.0','OSX.2+',1.8,'A'),(12,'Gecko','Camino 1.5','OSX.3+',1.8,'A'),(13,'Gecko','Netscape 7.2','Win 95+ / Mac OS 8.6-9.2',1.7,'A'),(14,'Gecko','Netscape Browser 8','Win 98SE+',1.7,'A'),(15,'Gecko','Netscape Navigator 9','Win 98+ / OSX.2+',1.8,'A'),(16,'Gecko','Mozilla 1.0','Win 95+ / OSX.1+',1,'A'),(17,'Gecko','Mozilla 1.1','Win 95+ / OSX.1+',1.1,'A'),(18,'Gecko','Mozilla 1.2','Win 95+ / OSX.1+',1.2,'A'),(19,'Gecko','Mozilla 1.3','Win 95+ / OSX.1+',1.3,'A'),(20,'Gecko','Mozilla 1.4','Win 95+ / OSX.1+',1.4,'A'),(21,'Gecko','Mozilla 1.5','Win 95+ / OSX.1+',1.5,'A'),(22,'Gecko','Mozilla 1.6','Win 95+ / OSX.1+',1.6,'A'),(23,'Gecko','Mozilla 1.7','Win 98+ / OSX.1+',1.7,'A'),(24,'Gecko','Mozilla 1.8','Win 98+ / OSX.1+',1.8,'A'),(25,'Gecko','Seamonkey 1.1','Win 98+ / OSX.2+',1.8,'A'),(26,'Gecko','Epiphany 2.20','Gnome',1.8,'A'),(27,'Webkit','Safari 1.2','OSX.3',125.5,'A'),(28,'Webkit','Safari 1.3','OSX.3',312.8,'A'),(29,'Webkit','Safari 2.0','OSX.4+',419.3,'A'),(30,'Webkit','Safari 3.0','OSX.4+',522.1,'A'),(31,'Webkit','OmniWeb 5.5','OSX.4+',420,'A'),(32,'Webkit','iPod Touch / iPhone','iPod',420.1,'A'),(33,'Webkit','S60','S60',413,'A'),(34,'Trident','Internet Explorer 4.0','Win 95+',4,'X'),(35,'Trident','Internet Explorer 5.0','Win 95+',5,'C'),(36,'Trident','Internet Explorer 5.5','Win 95+',5.5,'A'),(37,'Trident','Internet Explorer 6','Win 98+',6,'A'),(38,'Trident','Internet Explorer 7','Win XP SP2+',7,'A'),(39,'Trident','AOL browser (AOL desktop)','Win XP',6,'A'),(40,'Gecko','Firefox 1.0','Win 98+ / OSX.2+',1.7,'A'),(41,'Gecko','Firefox 1.5','Win 98+ / OSX.2+',1.8,'A'),(42,'Gecko','Firefox 2.0','Win 98+ / OSX.2+',1.8,'A'),(43,'Gecko','Firefox 3.0','Win 2k+ / OSX.3+',1.9,'A'),(44,'Gecko','Camino 1.0','OSX.2+',1.8,'A'),(45,'Gecko','Camino 1.5','OSX.3+',1.8,'A'),(46,'Gecko','Netscape 7.2','Win 95+ / Mac OS 8.6-9.2',1.7,'A'),(47,'Gecko','Netscape Browser 8','Win 98SE+',1.7,'A'),(48,'Gecko','Netscape Navigator 9','Win 98+ / OSX.2+',1.8,'A'),(49,'Gecko','Mozilla 1.0','Win 95+ / OSX.1+',1,'A'),(50,'Gecko','Mozilla 1.1','Win 95+ / OSX.1+',1.1,'A'),(51,'Gecko','Mozilla 1.2','Win 95+ / OSX.1+',1.2,'A'),(52,'Gecko','Mozilla 1.3','Win 95+ / OSX.1+',1.3,'A'),(53,'Gecko','Mozilla 1.4','Win 95+ / OSX.1+',1.4,'A'),(54,'Gecko','Mozilla 1.5','Win 95+ / OSX.1+',1.5,'A'),(55,'Gecko','Mozilla 1.6','Win 95+ / OSX.1+',1.6,'A'),(56,'Gecko','Mozilla 1.7','Win 98+ / OSX.1+',1.7,'A'),(57,'Gecko','Mozilla 1.8','Win 98+ / OSX.1+',1.8,'A'),(58,'Gecko','Seamonkey 1.1','Win 98+ / OSX.2+',1.8,'A'),(59,'Gecko','Epiphany 2.20','Gnome',1.8,'A'),(60,'Webkit','Safari 1.2','OSX.3',125.5,'A'),(61,'Webkit','Safari 1.3','OSX.3',312.8,'A'),(62,'Webkit','Safari 2.0','OSX.4+',419.3,'A'),(63,'Webkit','Safari 3.0','OSX.4+',522.1,'A'),(64,'Webkit','OmniWeb 5.5','OSX.4+',420,'A'),(65,'Webkit','iPod Touch / iPhone','iPod',420.1,'A'),(66,'Webkit','S60','S60',413,'A'),(67,'Trident','Internet Explorer 4.0','Win 95+',4,'X'),(68,'Trident','Internet Explorer 5.0','Win 95+',5,'C'),(69,'Trident','Internet Explorer 5.5','Win 95+',5.5,'A'),(70,'Trident','Internet Explorer 6','Win 98+',6,'A'),(71,'Trident','Internet Explorer 7','Win XP SP2+',7,'A'),(72,'Trident','AOL browser (AOL desktop)','Win XP',6,'A'),(73,'Gecko','Firefox 1.0','Win 98+ / OSX.2+',1.7,'A'),(74,'Gecko','Firefox 1.5','Win 98+ / OSX.2+',1.8,'A'),(75,'Gecko','Firefox 2.0','Win 98+ / OSX.2+',1.8,'A'),(76,'Gecko','Firefox 3.0','Win 2k+ / OSX.3+',1.9,'A'),(77,'Gecko','Camino 1.0','OSX.2+',1.8,'A'),(78,'Gecko','Camino 1.5','OSX.3+',1.8,'A'),(79,'Gecko','Netscape 7.2','Win 95+ / Mac OS 8.6-9.2',1.7,'A'),(80,'Gecko','Netscape Browser 8','Win 98SE+',1.7,'A'),(81,'Gecko','Netscape Navigator 9','Win 98+ / OSX.2+',1.8,'A'),(82,'Gecko','Mozilla 1.0','Win 95+ / OSX.1+',1,'A'),(83,'Gecko','Mozilla 1.1','Win 95+ / OSX.1+',1.1,'A'),(84,'Gecko','Mozilla 1.2','Win 95+ / OSX.1+',1.2,'A'),(85,'Gecko','Mozilla 1.3','Win 95+ / OSX.1+',1.3,'A'),(86,'Gecko','Mozilla 1.4','Win 95+ / OSX.1+',1.4,'A'),(87,'Gecko','Mozilla 1.5','Win 95+ / OSX.1+',1.5,'A'),(88,'Gecko','Mozilla 1.6','Win 95+ / OSX.1+',1.6,'A'),(89,'Gecko','Mozilla 1.7','Win 98+ / OSX.1+',1.7,'A'),(90,'Gecko','Mozilla 1.8','Win 98+ / OSX.1+',1.8,'A'),(91,'Gecko','Seamonkey 1.1','Win 98+ / OSX.2+',1.8,'A'),(92,'Gecko','Epiphany 2.20','Gnome',1.8,'A'),(93,'Webkit','Safari 1.2','OSX.3',125.5,'A'),(94,'Webkit','Safari 1.3','OSX.3',312.8,'A'),(95,'Webkit','Safari 2.0','OSX.4+',419.3,'A'),(96,'Webkit','Safari 3.0','OSX.4+',522.1,'A'),(97,'Webkit','OmniWeb 5.5','OSX.4+',420,'A'),(98,'Webkit','iPod Touch / iPhone','iPod',420.1,'A'),(99,'Webkit','S60','S60',413,'A'),(100,'Presto','Nintendo DS browser','Nintendo DS',8.5,'C/A<sup>1</sup>'),(101,'KHTML','Konqureror 3.1','KDE 3.1',3.1,'C'),(102,'KHTML','Konqureror 3.3','KDE 3.3',3.3,'A'),(103,'KHTML','Konqureror 3.5','KDE 3.5',3.5,'A'),(104,'Tasman','Internet Explorer 5.1','Mac OS 7.6-9',1,'C'),(105,'Tasman','Internet Explorer 5.2','Mac OS 8-X',1,'C');
/*!40000 ALTER TABLE `ajax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_book_issue`
--

DROP TABLE IF EXISTS `library_book_issue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_book_issue` (
  `book_id` varchar(20) DEFAULT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `return_date` date DEFAULT NULL,
  `return_status` char(1) DEFAULT NULL,
  `user_type` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_book_issue`
--

LOCK TABLES `library_book_issue` WRITE;
/*!40000 ALTER TABLE `library_book_issue` DISABLE KEYS */;
INSERT INTO `library_book_issue` VALUES ('BHS/SE/55','BHSS0007','2013-12-29','2014-01-13','2014-01-02','Y','S'),('BHS/SE/57','BHSE0039','2013-12-29','2014-01-13','2014-01-01','Y','T'),('BHS/SE/58','BHSE0038','2013-12-29','2014-01-13',NULL,'N','T'),('BHS/IT/1','BHSS0001','2013-12-29','2014-01-13','2014-01-01','Y','S'),('BHS/IT/1','BHSS0001','2014-01-01','2014-01-16','2014-01-01','Y','S'),('BHS/IT/1','BHSS0007','2014-01-01','2014-01-16',NULL,'N','S'),('BHS/IT/2','BHSE0023','2014-01-01','2014-01-16',NULL,'N','T'),('BHS/IT/38','BHSE0035','2014-01-01','2014-01-16','2014-01-01','Y','T'),('BHS/IT/3','BHSS0001','2014-01-01','2014-01-16',NULL,'N','S'),('BHS/SE/56','BHSE0023','2014-01-02','2014-01-17',NULL,'N','T'),('BHS/IT/40','BHSE0033','2014-01-05','2014-01-20','2014-01-05','Y','T'),('BHS/MT/49','','2016-08-31','2016-09-15',NULL,'N','S'),('BHS/SE/53','','2016-08-31','2016-09-15',NULL,'N','S'),('BHS/IT/60','BHSE0049','2016-08-31','2016-09-15',NULL,'N','T');
/*!40000 ALTER TABLE `library_book_issue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_book_master`
--

DROP TABLE IF EXISTS `library_book_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_book_master` (
  `bm_id` int(10) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(50) DEFAULT NULL,
  `number_of_copy` int(10) DEFAULT NULL,
  `book_type` varchar(50) DEFAULT NULL,
  `rack_id` varchar(10) DEFAULT NULL,
  `assigned_count` int(10) DEFAULT NULL,
  PRIMARY KEY (`bm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_book_master`
--

LOCK TABLES `library_book_master` WRITE;
/*!40000 ALTER TABLE `library_book_master` DISABLE KEYS */;
INSERT INTO `library_book_master` VALUES (1,'Java',24,'1','1',3),(2,'Advance Jsp',7,'1','2',NULL),(3,'java J2ee',6,'1','1',NULL),(4,'DS',2,'1','1',NULL),(6,'C++ Opps',5,'1','1',0),(7,'English Grammer',4,'6','11',NULL),(8,'Math-I',2,'3','5',NULL),(9,'Math-II',2,'3','5',1),(10,'Science-I',2,'2','4',NULL),(11,'Science-II',2,'2','3',1),(12,'chemistry-I',2,'2','3',1),(13,'chemistry-II',3,'2','3',1),(15,'Xml Tutorial',10,'2','3',1),(16,'New Book',4,'2','4',NULL),(17,'Test',2,'1','1',NULL),(19,'Test-2',2,'1','1',NULL);
/*!40000 ALTER TABLE `library_book_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_book_type`
--

DROP TABLE IF EXISTS `library_book_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_book_type` (
  `book_type_id` int(10) DEFAULT NULL,
  `book_type_name` varchar(50) DEFAULT NULL,
  `prefix_book_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_book_type`
--

LOCK TABLES `library_book_type` WRITE;
/*!40000 ALTER TABLE `library_book_type` DISABLE KEYS */;
INSERT INTO `library_book_type` VALUES (1,'Information Technology','BHS/IT/'),(2,'Science','BHS/SE/'),(3,'Math','BHS/MT/'),(4,'Art','BHS/AT/'),(5,'History','BHS/HT/'),(6,'Communication Skill','BHS/CS/');
/*!40000 ALTER TABLE `library_book_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_copy_of_books`
--

DROP TABLE IF EXISTS `library_copy_of_books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_copy_of_books` (
  `book_copy_id` int(10) NOT NULL AUTO_INCREMENT,
  `bm_id` int(10) DEFAULT NULL,
  `book_id` varchar(10) NOT NULL,
  `assigned_status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`book_copy_id`,`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_copy_of_books`
--

LOCK TABLES `library_copy_of_books` WRITE;
/*!40000 ALTER TABLE `library_copy_of_books` DISABLE KEYS */;
INSERT INTO `library_copy_of_books` VALUES (1,1,'BHS/IT/1','Y'),(2,1,'BHS/IT/2','Y'),(3,1,'BHS/IT/3','Y'),(4,1,'BHS/IT/4','N'),(5,1,'BHS/IT/5','N'),(6,1,'BHS/IT/6','N'),(7,1,'BHS/IT/7','N'),(8,1,'BHS/IT/8','N'),(9,1,'BHS/IT/9','N'),(10,1,'BHS/IT/10','N'),(11,2,'BHS/IT/11','N'),(12,2,'BHS/IT/12','N'),(13,2,'BHS/IT/13','N'),(14,2,'BHS/IT/14','N'),(15,2,'BHS/IT/15','N'),(16,3,'BHS/IT/16','N'),(17,3,'BHS/IT/17','N'),(18,3,'BHS/IT/18','N'),(19,3,'BHS/IT/19','N'),(20,3,'BHS/IT/20','N'),(21,3,'BHS/IT/21','N'),(22,1,'BHS/IT/22','N'),(23,1,'BHS/IT/23','N'),(24,1,'BHS/IT/24','N'),(25,1,'BHS/IT/25','N'),(26,1,'BHS/IT/26','N'),(27,1,'BHS/IT/27','N'),(28,1,'BHS/IT/28','N'),(29,2,'BHS/IT/29','N'),(30,2,'BHS/IT/30','N'),(31,4,'BHS/IT/31','N'),(33,1,'BHS/IT/33','N'),(34,1,'BHS/IT/34','N'),(38,6,'BHS/IT/38','N'),(39,6,'BHS/IT/39','N'),(40,6,'BHS/IT/40','N'),(41,6,'BHS/IT/41','N'),(42,6,'BHS/IT/42','N'),(43,7,'BHS/CS/43','N'),(44,7,'BHS/CS/44','N'),(45,7,'BHS/CS/45','N'),(46,7,'BHS/CS/46','N'),(47,8,'BHS/MT/47','N'),(48,8,'BHS/MT/48','N'),(49,9,'BHS/MT/49','Y'),(50,9,'BHS/MT/50','N'),(51,10,'BHS/SE/51','N'),(52,10,'BHS/SE/52','N'),(53,11,'BHS/SE/53','Y'),(54,11,'BHS/SE/54','N'),(55,12,'BHS/SE/55','N'),(56,12,'BHS/SE/56','Y'),(57,13,'BHS/SE/57','N'),(58,13,'BHS/SE/58','Y'),(59,13,'BHS/SE/59','N'),(60,15,'BHS/IT/60','Y'),(61,15,'BHS/IT/61','N'),(62,15,'BHS/IT/62','N'),(63,15,'BHS/IT/63','N'),(64,15,'BHS/IT/64','N'),(65,15,'BHS/IT/65','N'),(66,15,'BHS/IT/66','N'),(67,15,'BHS/IT/67','N'),(68,15,'BHS/IT/68','N'),(69,15,'BHS/IT/69','N'),(70,16,'BHS/SE/70','N'),(71,16,'BHS/SE/71','N'),(72,16,'BHS/SE/72','N'),(73,16,'BHS/SE/73','N'),(74,17,'BHS/IT/74','N'),(75,17,'BHS/IT/75','N'),(76,19,'BHS/IT/76','N'),(77,19,'BHS/IT/77','N');
/*!40000 ALTER TABLE `library_copy_of_books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_rack_book_type_relation`
--

DROP TABLE IF EXISTS `library_rack_book_type_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_rack_book_type_relation` (
  `book_rack_id` int(10) DEFAULT NULL,
  `book_type_id` int(10) DEFAULT NULL,
  `rack_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_rack_book_type_relation`
--

LOCK TABLES `library_rack_book_type_relation` WRITE;
/*!40000 ALTER TABLE `library_rack_book_type_relation` DISABLE KEYS */;
INSERT INTO `library_rack_book_type_relation` VALUES (1,1,1),(2,1,2),(3,2,3),(4,2,4),(5,3,5),(6,3,6),(7,4,7),(8,4,8),(9,5,9),(10,5,10),(11,6,11),(12,6,12);
/*!40000 ALTER TABLE `library_rack_book_type_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_rack_info`
--

DROP TABLE IF EXISTS `library_rack_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_rack_info` (
  `rack_id` int(10) DEFAULT NULL,
  `rack_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_rack_info`
--

LOCK TABLES `library_rack_info` WRITE;
/*!40000 ALTER TABLE `library_rack_info` DISABLE KEYS */;
INSERT INTO `library_rack_info` VALUES (1,'BHSIT01'),(2,'BHSIT02'),(3,'BHSSC03'),(4,'BHSSC04'),(5,'BHSMT05'),(6,'BHSMT06'),(7,'BHSAT07'),(8,'BHSAT08'),(9,'BHSHT09'),(10,'BHSHT10'),(11,'BHSCS11'),(12,'BHSCS12');
/*!40000 ALTER TABLE `library_rack_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_assignment_detail`
--

DROP TABLE IF EXISTS `sch_assignment_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_assignment_detail` (
  `assignment_id` int(11) NOT NULL AUTO_INCREMENT,
  `assignment_name` varchar(50) DEFAULT NULL,
  `assignment_desc` varchar(100) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  PRIMARY KEY (`assignment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_assignment_detail`
--

LOCK TABLES `sch_assignment_detail` WRITE;
/*!40000 ALTER TABLE `sch_assignment_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `sch_assignment_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_branch_class_relation`
--

DROP TABLE IF EXISTS `sch_branch_class_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_branch_class_relation` (
  `branchclass_id` int(10) NOT NULL AUTO_INCREMENT,
  `branch_id` int(10) DEFAULT NULL,
  `class_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`branchclass_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_branch_class_relation`
--

LOCK TABLES `sch_branch_class_relation` WRITE;
/*!40000 ALTER TABLE `sch_branch_class_relation` DISABLE KEYS */;
INSERT INTO `sch_branch_class_relation` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,1,7),(8,1,8),(9,1,9),(10,1,10);
/*!40000 ALTER TABLE `sch_branch_class_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_branch_detail`
--

DROP TABLE IF EXISTS `sch_branch_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_branch_detail` (
  `branch_id` int(10) NOT NULL DEFAULT '0',
  `branch_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_branch_detail`
--

LOCK TABLES `sch_branch_detail` WRITE;
/*!40000 ALTER TABLE `sch_branch_detail` DISABLE KEYS */;
INSERT INTO `sch_branch_detail` VALUES (1,'Primary'),(2,'Science');
/*!40000 ALTER TABLE `sch_branch_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_class_detail`
--

DROP TABLE IF EXISTS `sch_class_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_class_detail` (
  `class_id` int(3) NOT NULL,
  `class_name` varchar(20) DEFAULT NULL,
  `section` char(1) DEFAULT NULL,
  `admission_fee` decimal(10,0) DEFAULT NULL,
  `monthly_fee` decimal(10,0) DEFAULT NULL,
  `registration_fee` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_class_detail`
--

LOCK TABLES `sch_class_detail` WRITE;
/*!40000 ALTER TABLE `sch_class_detail` DISABLE KEYS */;
INSERT INTO `sch_class_detail` VALUES (1,'First-I','A',2000,120,100),(2,'Second-II','A',2000,120,100),(3,'Third-III','A',2400,120,100),(4,'Third-III','B',2500,200,100),(5,'Fourth-IV','A',3000,200,100),(6,'Fifth-V','A',3500,300,100),(7,'Fifth-V','B',3500,300,100),(8,'Sixth-VI','A',4000,350,100),(9,'Seventh-VII','A',4000,350,100),(10,'Eighth-VIII','A',4000,350,100),(11,'Nineth-IX','A',4500,400,500),(12,'Nineth-IX','B',4500,400,500),(13,'Tenth-X(High School)','A',4500,400,500),(14,'Tenth-X(High School)','B',4500,400,500),(15,'Eleventh-XI','A',3000,500,500),(16,'Eleventh-XI','B',3000,500,500),(17,'Twelth-XII','A',3000,500,500),(18,'Twelth-XII','B',3000,500,500);
/*!40000 ALTER TABLE `sch_class_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_class_subject_relation`
--

DROP TABLE IF EXISTS `sch_class_subject_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_class_subject_relation` (
  `class_subject_id` int(10) NOT NULL AUTO_INCREMENT,
  `class_id` int(10) DEFAULT NULL,
  `subject_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`class_subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_class_subject_relation`
--

LOCK TABLES `sch_class_subject_relation` WRITE;
/*!40000 ALTER TABLE `sch_class_subject_relation` DISABLE KEYS */;
INSERT INTO `sch_class_subject_relation` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,2,5),(6,1,6),(7,2,1),(8,2,3),(9,3,1),(10,3,2),(11,3,7),(12,2,7),(13,1,7),(14,1,8),(15,3,8),(16,2,4),(17,4,1),(18,4,2),(19,4,3),(20,4,4),(21,5,5),(22,5,6),(23,5,7),(24,5,8),(25,6,3),(26,6,4),(27,6,5),(28,6,6),(29,6,7),(30,7,1),(32,7,2),(33,7,3),(34,8,4),(35,8,5),(36,8,7),(37,9,6),(38,9,2),(39,9,1),(40,10,5),(41,10,6),(42,10,7),(43,10,8),(44,11,3),(45,11,4),(46,11,5),(47,11,6),(48,12,7),(49,12,1),(50,12,2),(51,12,5),(52,13,1),(53,13,2),(54,13,3),(55,14,4),(56,14,5),(57,14,6),(58,14,7),(59,15,8),(60,15,7),(61,15,5),(62,15,4),(63,16,6),(64,17,1),(65,18,2),(66,18,5),(67,NULL,NULL);
/*!40000 ALTER TABLE `sch_class_subject_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_concern_category`
--

DROP TABLE IF EXISTS `sch_concern_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_concern_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_concern_category`
--

LOCK TABLES `sch_concern_category` WRITE;
/*!40000 ALTER TABLE `sch_concern_category` DISABLE KEYS */;
INSERT INTO `sch_concern_category` VALUES (1,'Fees Concern'),(2,'Facility Concern'),(3,'Student Concern'),(4,'Suggestion'),(5,'Other');
/*!40000 ALTER TABLE `sch_concern_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_concern_status`
--

DROP TABLE IF EXISTS `sch_concern_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_concern_status` (
  `concern_status_id` int(10) DEFAULT NULL,
  `concern_status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_concern_status`
--

LOCK TABLES `sch_concern_status` WRITE;
/*!40000 ALTER TABLE `sch_concern_status` DISABLE KEYS */;
INSERT INTO `sch_concern_status` VALUES (1,'Pending'),(2,'Resolved'),(3,'Unresolved');
/*!40000 ALTER TABLE `sch_concern_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_department_detail`
--

DROP TABLE IF EXISTS `sch_department_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_department_detail` (
  `dept_id` int(10) NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(50) DEFAULT NULL,
  `dept_status` char(1) DEFAULT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_department_detail`
--

LOCK TABLES `sch_department_detail` WRITE;
/*!40000 ALTER TABLE `sch_department_detail` DISABLE KEYS */;
INSERT INTO `sch_department_detail` VALUES (1,'Primary','Y'),(2,'Science','Y'),(3,'Art','N'),(4,'Commerce','Y');
/*!40000 ALTER TABLE `sch_department_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_dept_branch_relation`
--

DROP TABLE IF EXISTS `sch_dept_branch_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_dept_branch_relation` (
  `deptbranch_id` int(10) NOT NULL AUTO_INCREMENT,
  `dept_id` int(10) DEFAULT NULL,
  `branch_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`deptbranch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_dept_branch_relation`
--

LOCK TABLES `sch_dept_branch_relation` WRITE;
/*!40000 ALTER TABLE `sch_dept_branch_relation` DISABLE KEYS */;
INSERT INTO `sch_dept_branch_relation` VALUES (1,1,1);
/*!40000 ALTER TABLE `sch_dept_branch_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_dept_class_relation`
--

DROP TABLE IF EXISTS `sch_dept_class_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_dept_class_relation` (
  `deptclass_id` int(10) NOT NULL AUTO_INCREMENT,
  `dept_id` int(10) DEFAULT NULL,
  `class_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`deptclass_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_dept_class_relation`
--

LOCK TABLES `sch_dept_class_relation` WRITE;
/*!40000 ALTER TABLE `sch_dept_class_relation` DISABLE KEYS */;
INSERT INTO `sch_dept_class_relation` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,1,7),(8,1,8),(9,1,9),(10,1,10),(11,2,11),(12,2,12),(13,2,13),(14,2,14),(15,4,15),(16,4,16),(17,4,17),(18,4,18);
/*!40000 ALTER TABLE `sch_dept_class_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_emp_class_detail`
--

DROP TABLE IF EXISTS `sch_emp_class_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_emp_class_detail` (
  `emp_class_relation_id` int(10) NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(20) NOT NULL,
  `class_id` varchar(20) NOT NULL,
  PRIMARY KEY (`emp_class_relation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_emp_class_detail`
--

LOCK TABLES `sch_emp_class_detail` WRITE;
/*!40000 ALTER TABLE `sch_emp_class_detail` DISABLE KEYS */;
INSERT INTO `sch_emp_class_detail` VALUES (41,'12','5'),(42,'12','3'),(52,'13','4'),(53,'13','6'),(58,'14','3'),(59,'15','6'),(60,'15','7'),(61,'15','9'),(62,'16','2'),(63,'16','5'),(87,'19','8'),(88,'19','7'),(89,'19','6'),(90,'20','6'),(91,'20','5'),(92,'20','4'),(93,'20','3'),(94,'20','2'),(95,'20','1'),(99,'18','1'),(100,'18','3'),(101,'18','7'),(102,'33','10'),(103,'33','9'),(104,'33','8'),(110,'11','8'),(111,'11','4'),(121,'44','10'),(122,'44','9'),(123,'44','5'),(126,'45','4'),(127,'45','3'),(137,'46','3'),(138,'46','2'),(139,'46','1'),(140,'46','7'),(142,'9','2'),(143,'9','1'),(144,'49','4'),(145,'49','3');
/*!40000 ALTER TABLE `sch_emp_class_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_emp_subject_detail`
--

DROP TABLE IF EXISTS `sch_emp_subject_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_emp_subject_detail` (
  `emp_subject_relation_id` int(10) NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(20) NOT NULL,
  `subject_id` varchar(20) NOT NULL,
  PRIMARY KEY (`emp_subject_relation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_emp_subject_detail`
--

LOCK TABLES `sch_emp_subject_detail` WRITE;
/*!40000 ALTER TABLE `sch_emp_subject_detail` DISABLE KEYS */;
INSERT INTO `sch_emp_subject_detail` VALUES (41,'12','5'),(42,'12','3'),(52,'13','4'),(53,'13','6'),(58,'14','3'),(59,'15','6'),(60,'15','7'),(61,'15','9'),(62,'16','2'),(63,'16','5'),(75,'19','3'),(76,'19','2'),(77,'19','1'),(78,'20','2'),(79,'20','7'),(80,'20','5'),(81,'20','1'),(82,'20','2'),(83,'20','3'),(84,'20','2'),(85,'20','1'),(86,'33','3'),(87,'33','2'),(88,'33','1'),(89,'41','6'),(90,'41','1'),(99,'44','1'),(100,'44','3'),(101,'44','6'),(102,'44','8'),(105,'45','4'),(106,'45','3'),(110,'48','6'),(111,'48','6'),(112,'48','3'),(113,'48','2'),(114,'48','1'),(115,'48','7'),(116,'48','3'),(117,'48','2'),(118,'48','1'),(119,'48','2'),(120,'9','2'),(121,'9','1'),(122,'49','4'),(123,'49','3');
/*!40000 ALTER TABLE `sch_emp_subject_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_fee_collection`
--

DROP TABLE IF EXISTS `sch_fee_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_fee_collection` (
  `receipt_no` int(10) DEFAULT NULL,
  `month_id` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_fee_collection`
--

LOCK TABLES `sch_fee_collection` WRITE;
/*!40000 ALTER TABLE `sch_fee_collection` DISABLE KEYS */;
INSERT INTO `sch_fee_collection` VALUES (1,1),(1,2),(1,3),(1,4),(2,5),(2,6),(2,7),(2,8),(3,1),(3,2),(3,3),(3,4),(3,5),(3,6),(3,7),(3,8),(3,9),(3,10),(3,11),(3,12),(4,1),(4,2),(4,3),(4,4),(4,5),(4,6);
/*!40000 ALTER TABLE `sch_fee_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_fee_detail`
--

DROP TABLE IF EXISTS `sch_fee_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_fee_detail` (
  `receipt_no` int(10) NOT NULL AUTO_INCREMENT,
  `admission_no` int(10) DEFAULT NULL,
  `session_id` int(4) DEFAULT NULL,
  `class_id` int(10) DEFAULT NULL,
  `fee_amount` int(10) DEFAULT NULL,
  `concession_amt` int(10) DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_mode` varchar(10) DEFAULT NULL,
  `cheque_no` varchar(9) DEFAULT NULL,
  `cheque_date` datetime DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`receipt_no`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_fee_detail`
--

LOCK TABLES `sch_fee_detail` WRITE;
/*!40000 ALTER TABLE `sch_fee_detail` DISABLE KEYS */;
INSERT INTO `sch_fee_detail` VALUES (1,1,9,2,480,NULL,'2012-10-30 00:00:00','Cash','','1900-00-00 00:00:00',''),(2,1,9,2,480,NULL,'2012-10-30 00:00:00','Cheque','400300100','2012-10-30 00:00:00','ICICI'),(3,6,10,1,1440,NULL,'2013-10-06 00:00:00','Cash','','1900-00-00 00:00:00',''),(4,1,10,3,720,NULL,'2013-10-11 00:00:00','Cash','','1900-00-00 00:00:00','');
/*!40000 ALTER TABLE `sch_fee_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_functionalities`
--

DROP TABLE IF EXISTS `sch_functionalities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_functionalities` (
  `F_ID` int(11) NOT NULL,
  `F_TITLE` varchar(50) DEFAULT NULL,
  `UPDATED_TIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `F_DELETED` int(11) DEFAULT NULL,
  `PF_ID` int(11) DEFAULT NULL,
  `F_URL` varchar(100) DEFAULT NULL,
  `F_ORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`F_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_functionalities`
--

LOCK TABLES `sch_functionalities` WRITE;
/*!40000 ALTER TABLE `sch_functionalities` DISABLE KEYS */;
INSERT INTO `sch_functionalities` VALUES (1,'SCH Master Setup','2011-03-23 15:51:47',0,0,0,'#',1),(2,'Admission','2011-03-23 15:54:20',NULL,0,0,'#',NULL),(3,'Reports','2011-03-23 06:38:13',NULL,0,0,'#',7),(4,'Password','2011-03-23 16:19:10',NULL,0,0,'#',8),(5,'Fees','2011-03-23 16:02:59',NULL,0,0,'#',NULL),(6,'Library','2011-03-23 16:04:31',NULL,0,0,'#',NULL),(7,'Assignment','2016-09-01 10:04:17',NULL,0,0,'#',NULL),(8,'Concern Tracker','2013-03-09 03:59:02',NULL,0,0,'#',5),(9,'Alumni','2012-06-03 09:51:41',NULL,0,0,'#',NULL),(10,'One More Top Nav','2012-06-03 09:52:29',NULL,0,0,'#',NULL),(11,'Home','2012-06-03 09:51:18',NULL,0,0,'/schmangmt/LoginAction.do?operation=home',0),(12,'Employee Master','2012-06-03 09:51:07',NULL,0,1,'/schmangmt/EduStaffAction.do?operation=search',NULL),(13,'Concern Listing','2013-03-10 04:38:32',NULL,0,8,'/schmangmt/ConcernTrackingAction.do?operation=search',NULL),(14,'Change Password','2012-11-03 17:41:19',NULL,0,4,'/schmangmt/LoginAction.do?operation=changePassword',NULL),(15,'Admission','2012-06-03 09:50:12',NULL,0,2,'/schmangmt/EduStudentAction.do?operation=search',NULL),(16,'Registration','2012-06-03 09:50:04',NULL,0,2,'/schmangmt/EduRegistrationAction.do?operation=search',NULL),(17,'School Leaving Certificate','2012-06-03 09:49:55',NULL,0,2,'/schmangmt/EduExitAction.do?operation=search',NULL),(18,'Report Card','2012-08-31 17:49:41',NULL,0,3,'/schmangmt/EduStudentReportAction.do?operation=reportList',NULL),(19,'Green Sheet','2013-04-06 06:23:03',NULL,0,3,'/schmangmt/EduStudentReportAction.do?operation=greenSheet',NULL),(20,'Fees Payments','2012-06-03 09:49:39',NULL,0,5,'/schmangmt/FeesPaymentAction.do?operation=createSetup',NULL),(21,'Book Master','2013-10-12 10:43:02',NULL,0,6,'/schmangmt/LibraryManagementAction.do?operation=createSetup',NULL),(22,'Book Issue','2016-09-19 11:49:17',NULL,0,6,'/schmangmt/LibraryManagementAction.do?operation=issueSetup',NULL),(23,'Book Return','2016-09-19 11:49:17',NULL,0,6,'/schmangmt/LibraryManagementAction.do?operation=returnSetup',NULL),(24,'Issued Books','2016-09-19 11:49:17',NULL,0,6,'/schmangmt/LibraryManagementAction.do?operation=assignedBooklist',NULL),(25,'Fine Collection','2012-06-03 09:49:06',NULL,0,6,'#',NULL),(26,'Task','2016-09-01 10:04:17',NULL,0,7,'#',NULL),(27,'Fee Receipt','2012-06-03 09:48:57',NULL,0,5,'/schmangmt/FeesPaymentAction.do?operation=receiptSetup',NULL),(29,'Former Employees','2012-08-02 06:20:07',NULL,0,9,'/schmangmt/EduStaffAction.do?operation=searchAlumni',NULL),(30,'Alumni Students','2012-08-02 06:19:41',NULL,0,9,'/schmangmt/EduStudentAction.do?operation=searchAlumni',NULL),(31,'Employees Profile','2012-11-11 11:00:15',NULL,0,1,'/schmangmt/EduStaffAction.do?operation=empProfile',NULL),(32,'Pending Fees','2012-06-03 09:48:11',NULL,0,5,'#',NULL),(33,'Fees Collection','2012-06-03 09:47:48',NULL,0,5,'#',NULL),(34,'Students Profile','2012-09-12 15:43:40',NULL,0,2,'/schmangmt/EduStudentReportAction.do?operation=studentProfile',NULL),(35,'Prepare Report Card','2012-08-17 18:21:24',NULL,0,3,'/schmangmt/EduStudentReportAction.do?operation=search',NULL),(36,'New Session','2012-09-20 15:16:13',NULL,0,2,'/schmangmt/EduStudentReportAction.do?operation=newSession',NULL),(37,'Registration Fees','2012-10-27 08:28:50',NULL,0,5,'/schmangmt/FeesPaymentAction.do?operation=registrationfeeSetup',NULL),(38,'Meeting','2013-09-15 17:04:12',NULL,0,1,'/schmangmt/EduStaffAction.do?operation=meetingSetup',NULL),(39,'Books Availability','2016-09-19 11:49:17',NULL,0,6,'/schmangmt/LibraryManagementAction.do?operation=bookList',NULL),(40,'Returned Books','2016-09-19 11:49:17',NULL,0,6,'/schmangmt/LibraryManagementAction.do?operation=returnedBooklist',NULL),(41,'Assignment','2016-09-01 10:17:20',NULL,0,7,'#',NULL),(42,'Allocation','2016-09-01 02:36:11',NULL,0,7,'#',NULL);
/*!40000 ALTER TABLE `sch_functionalities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_issue`
--

DROP TABLE IF EXISTS `sch_issue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_issue` (
  `issue_id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_title` varchar(255) DEFAULT NULL,
  `issue_desc` varchar(255) DEFAULT NULL,
  `issue_submited_date` datetime DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `creater_id` int(11) DEFAULT NULL,
  `concern_status_id` int(11) DEFAULT NULL,
  `reply_desc` varchar(255) DEFAULT NULL,
  `issue_reply_date` datetime DEFAULT NULL,
  PRIMARY KEY (`issue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_issue`
--

LOCK TABLES `sch_issue` WRITE;
/*!40000 ALTER TABLE `sch_issue` DISABLE KEYS */;
INSERT INTO `sch_issue` VALUES (2,'Holiday','School should declare the holiday list before start session','2009-11-02 00:54:25','High',4,23,1,'',NULL),(3,'anxiety about admission','My son want math so please let me know the date when admission will be open','2009-11-02 02:27:10','High',5,1,1,'',NULL),(8,'Not able to submit fee for may month','We have financial problem and not able to submit fee for month May,Request you give me time so I can submit the fee in Jun month along with may month fee\r\n','2013-03-16 09:56:16','Medium',1,43,2,'Finally your request has been approved by school management you can submit fee in next month','2013-03-17 10:17:54'),(10,'Sport activity','School should organized sport activity on every Saturday and every two month should have competition this will help to motivate the student and also they get time to fun\r\n','2013-03-17 11:50:33','High',4,43,1,NULL,NULL);
/*!40000 ALTER TABLE `sch_issue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_login_info`
--

DROP TABLE IF EXISTS `sch_login_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_login_info` (
  `user_name` varchar(255) DEFAULT NULL,
  `update_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_login_info`
--

LOCK TABLES `sch_login_info` WRITE;
/*!40000 ALTER TABLE `sch_login_info` DISABLE KEYS */;
INSERT INTO `sch_login_info` VALUES ('vinod bhadouria','2011-03-23 17:25:09',14),('vinod bhadouria','2011-03-23 17:27:12',14),('vinod bhadouria','2011-03-23 18:00:20',14),('vinod bhadouria','2011-03-23 19:02:58',14),('vinod bhadouria','2011-03-23 19:13:45',14),('vinod bhadouria','2011-03-23 19:16:59',14),('vinod bhadouria','2011-03-24 02:35:58',14),('vinod bhadouria','2011-03-24 02:45:42',14),('vinod bhadouria','2011-03-24 03:00:22',14),('vinod bhadouria','2011-03-24 03:20:14',14),('vinod bhadouria','2011-03-24 12:02:12',14),('vinod bhadouria','2011-03-25 15:54:06',14),('vinod bhadouria','2011-03-25 16:16:20',14),('vinod bhadouria','2011-03-25 16:26:27',14),('vinod bhadouria','2011-03-25 18:22:25',14),('vinod bhadouria','2011-03-26 05:09:32',14),('vinod bhadouria','2011-03-26 05:35:20',14),('vinod bhadouria','2011-03-26 06:34:29',14),('vinod bhadouria','2011-03-26 06:41:58',14),('vinod bhadouria','2011-03-26 07:40:23',14),('vinod bhadouria','2011-03-26 07:53:26',14),('vinod bhadouria','2011-03-26 14:26:51',14),('vinod bhadouria','2011-03-26 14:57:06',14),('vinod bhadouria','2011-03-26 15:18:42',14),('vinod bhadouria','2011-03-26 18:06:52',14),('vinod bhadouria','2011-03-26 18:16:20',14),('vinod bhadouria','2011-03-26 18:22:33',14),('vinod bhadouria','2011-03-26 18:46:57',14),('vinod bhadouria','2011-03-26 18:58:56',14),('vinod bhadouria','2011-03-26 19:05:00',14),('vinod bhadouria','2011-03-26 19:39:45',14),('vinod bhadouria','2011-03-26 20:35:12',14),('vinod bhadouria','2011-03-26 20:53:52',14),('vinod bhadouria','2011-03-26 21:00:03',14),('vinod bhadouria','2011-03-26 21:49:47',14),('vinod bhadouria','2011-03-26 22:03:43',14),('vinod bhadouria','2011-03-27 07:56:27',14),('vinod bhadouria','2011-03-27 08:49:09',14),('vinod bhadouria','2011-03-27 09:13:44',14),('vinod bhadouria','2011-03-27 14:58:11',14),('vinod bhadouria','2011-03-27 15:43:01',14),('vinod bhadouria','2011-03-27 15:50:59',14),('vinod bhadouria','2011-03-27 17:15:14',14),('vinod bhadouria','2011-03-27 17:55:45',14),('vinod bhadouria','2011-03-27 18:15:07',14),('vinod bhadouria','2011-03-27 18:26:42',14),('vinod bhadouria','2011-03-27 18:50:12',14),('vinod bhadouria','2011-03-27 19:03:54',14),('vinod bhadouria','2011-03-28 15:54:41',14),('vinod bhadouria','2011-03-28 16:49:50',14),('vinod bhadouria','2011-03-28 17:02:01',14),('vinod bhadouria','2011-03-31 18:20:19',14),('vinod bhadouria','2011-04-02 11:16:47',14),('vinod bhadouria','2011-04-02 11:23:59',14),('vinod bhadouria','2011-04-02 11:41:41',14),('vinod bhadouria','2011-04-02 11:52:14',14),('vinod bhadouria','2011-04-02 11:58:43',14),('vinod bhadouria','2011-04-02 12:00:40',14),('vinod bhadouria','2011-04-03 05:18:33',14),('vinod bhadouria','2011-04-03 06:32:52',14),('vinod bhadouria','2011-04-03 07:14:53',14),('vinod bhadouria','2011-04-03 07:18:29',14),('vinod bhadouria','2011-04-03 15:50:18',14),('vinod bhadouria','2011-04-03 16:48:46',14),('vinod bhadouria','2011-04-03 17:05:34',14),('vinod bhadouria','2011-04-03 17:21:49',14),('vinod bhadouria','2011-04-03 17:22:25',14),('vinod bhadouria','2011-04-03 18:19:01',14),('vinod bhadouria','2011-04-03 18:21:35',14),('vinod bhadouria','2011-04-03 18:28:10',14),('vinod bhadouria','2011-04-03 19:20:48',14),('vinod bhadouria','2011-04-07 16:30:08',14),('vinod bhadouria','2011-04-07 17:52:51',14),('vinod bhadouria','2011-04-08 16:09:04',14),('vinod bhadouria','2011-04-08 17:33:29',14),('vinod bhadouria','2011-04-08 18:42:24',14),('vinod bhadouria','2011-04-08 20:13:00',14),('vinod bhadouria','2011-04-09 07:14:51',14),('vinod bhadouria','2011-04-10 05:04:35',14),('vinod bhadouria','2011-04-10 05:10:13',14),('vinod bhadouria','2011-04-10 05:59:25',14),('vinod bhadouria','2011-04-10 06:20:24',14),('vinod bhadouria','2011-04-10 07:36:16',14),('vinod bhadouria','2011-04-10 07:39:18',14),('vinod bhadouria','2011-04-10 15:11:44',14),('vinod bhadouria','2011-04-10 15:26:34',14),('vinod bhadouria','2011-04-10 15:33:06',14),('vinod bhadouria','2011-04-10 15:52:45',14),('vinod bhadouria','2011-04-10 15:57:14',14),('vinod bhadouria','2011-04-10 19:09:32',14),('vinod bhadouria','2011-04-12 17:34:00',14),('vinod bhadouria','2011-04-12 18:07:55',14),('vinod bhadouria','2011-04-12 18:37:07',14),('vinod bhadouria','2011-04-13 17:46:31',14),('vinod bhadouria','2011-04-13 18:48:56',14),('vinod bhadouria','2011-04-15 16:24:52',14),('vinod bhadouria','2011-04-17 12:10:36',14),('vinod bhadouria','2011-04-17 16:56:06',14),('vinod bhadouria','2011-04-17 17:07:37',14),('vinod bhadouria','2011-04-17 17:13:25',14),('vinod bhadouria','2011-04-17 17:21:41',14),('vinod bhadouria','2011-04-17 17:40:56',14),('vinod bhadouria','2011-04-17 18:46:32',14),('vinod bhadouria','2011-04-17 18:56:00',14),('vinod bhadouria','2011-04-17 18:59:36',14),('vinod bhadouria','2011-04-17 19:04:03',14),('vinod bhadouria','2011-04-17 19:10:36',14),('vinod bhadouria','2011-04-21 13:31:31',14),('vinod bhadouria','2011-04-21 15:21:58',14),('vinod bhadouria','2011-04-21 16:27:44',14),('vinod bhadouria','2011-04-21 16:54:49',14),('vinod bhadouria','2011-04-22 02:13:05',14),('vinod bhadouria','2011-04-22 04:39:23',14),('vinod bhadouria','2011-04-22 05:41:09',14),('vinod bhadouria','2011-04-22 09:49:30',14),('vinod bhadouria','2011-04-22 10:20:38',14),('vinod bhadouria','2011-04-24 04:40:36',14),('vinod bhadouria','2011-04-24 14:03:04',14),('vinod bhadouria','2011-04-25 02:54:34',14),('vinod bhadouria','2011-04-25 13:42:19',14),('vinod bhadouria','2011-04-25 13:49:58',14),('vinod bhadouria','2011-04-25 14:02:53',14),('vinod bhadouria','2011-04-29 18:46:05',14),('vinod bhadouria','2011-04-29 18:54:03',14),('vinod bhadouria','2011-04-30 04:20:53',14),('vinod bhadouria','2011-04-30 17:07:47',14),('vinod bhadouria','2011-04-30 17:24:55',14),('vinod bhadouria','2011-04-30 17:36:39',14),('vinod bhadouria','2011-04-30 17:45:13',14),('vinod bhadouria','2011-04-30 17:48:10',14),('vinod bhadouria','2011-04-30 17:53:18',14),('vinod bhadouria','2011-04-30 17:54:36',14),('vinod bhadouria','2011-04-30 17:59:18',14),('vinod bhadouria','2011-04-30 18:03:41',14),('vinod bhadouria','2011-04-30 18:36:53',14),('vinod bhadouria','2011-04-30 18:39:03',14),('vinod bhadouria','2011-04-30 18:43:25',14),('vinod bhadouria','2011-04-30 18:46:52',14),('vinod bhadouria','2011-04-30 18:57:22',14),('vinod bhadouria','2011-04-30 19:16:16',14),('vinod bhadouria','2011-05-01 04:23:19',14),('vinod bhadouria','2011-05-01 05:00:37',14),('vinod bhadouria','2011-05-01 06:19:15',14),('vinod bhadouria','2011-05-01 06:28:34',14),('vinod bhadouria','2011-05-01 06:47:56',14),('vinod bhadouria','2011-05-01 14:29:39',14),('vinod bhadouria','2011-05-01 15:23:44',14),('vinod bhadouria','2011-05-01 15:32:04',14),('vinod bhadouria','2011-05-01 15:56:17',14),('vinod bhadouria','2011-05-01 17:14:28',14),('vinod bhadouria','2011-05-01 17:26:23',14),('vinod bhadouria','2011-05-01 17:44:16',14),('vinod bhadouria','2011-05-01 17:54:57',14),('vinod bhadouria','2011-05-01 18:16:48',14),('vinod bhadouria','2011-05-01 19:28:52',14),('vinod bhadouria','2011-05-01 19:37:47',14),('vinod bhadouria','2011-05-04 01:46:49',14),('vinod bhadouria','2011-05-07 09:23:32',14),('vinod bhadouria','2011-05-07 09:29:15',14),('vinod bhadouria','2011-05-07 09:42:49',14),('vinod bhadouria','2011-05-07 10:00:21',14),('vinod bhadouria','2011-05-07 10:22:02',14),('vinod bhadouria','2011-05-07 12:07:16',14),('vinod bhadouria','2011-05-07 12:42:41',14),('vinod bhadouria','2011-05-07 12:45:11',14),('vinod bhadouria','2011-05-07 12:50:04',14),('vinod bhadouria','2011-05-07 13:14:17',14),('vinod bhadouria','2011-05-07 13:18:18',14),('vinod bhadouria','2011-05-07 15:07:46',14),('vinod bhadouria','2011-05-07 15:23:23',14),('vinod bhadouria','2011-05-07 17:54:47',14),('vinod bhadouria','2011-05-07 18:04:40',14),('vinod bhadouria','2011-05-07 18:28:58',14),('vinod bhadouria','2011-05-07 18:39:30',14),('vinod bhadouria','2011-05-07 18:51:37',14),('vinod bhadouria','2011-05-07 18:55:12',14),('vinod bhadouria','2011-05-07 19:03:18',14),('vinod bhadouria','2011-05-07 19:05:39',14),('vinod bhadouria','2011-05-07 19:16:04',14),('vinod bhadouria','2011-05-07 19:18:17',14),('vinod bhadouria','2011-05-07 19:20:29',14),('vinod bhadouria','2011-05-07 19:28:31',14),('vinod bhadouria','2011-05-07 19:31:16',14),('vinod bhadouria','2011-05-07 19:45:01',14),('vinod bhadouria','2011-05-07 19:46:57',14),('vinod bhadouria','2011-05-07 19:58:56',14),('vinod bhadouria','2011-05-07 19:59:54',14),('vinod bhadouria','2011-05-07 20:17:02',14),('vinod bhadouria','2011-05-08 06:49:28',14),('vinod bhadouria','2011-05-08 06:54:59',14),('vinod bhadouria','2011-05-08 08:06:03',14),('vinod bhadouria','2011-05-08 08:19:51',14),('vinod bhadouria','2011-05-08 08:37:00',14),('vinod bhadouria','2011-05-08 10:55:29',14),('vinod bhadouria','2011-05-08 11:58:50',14),('vinod bhadouria','2011-05-08 11:59:55',14),('vinod bhadouria','2011-05-08 12:03:29',14),('vinod bhadouria','2011-05-08 12:30:49',14),('vinod bhadouria','2011-05-08 12:44:56',14),('vinod bhadouria','2011-05-08 12:52:57',14),('vinod bhadouria','2011-05-08 13:35:23',14),('vinod bhadouria','2011-05-08 13:40:43',14),('vinod bhadouria','2011-05-08 13:44:21',14),('vinod bhadouria','2011-05-08 13:49:48',14),('vinod bhadouria','2011-05-08 14:56:52',14),('vinod bhadouria','2011-05-08 15:31:09',14),('vinod bhadouria','2011-05-08 15:33:05',14),('vinod bhadouria','2011-05-08 15:37:57',14),('vinod bhadouria','2011-05-08 15:55:31',14),('vinod bhadouria','2011-05-08 16:25:57',14),('vinod bhadouria','2011-05-09 17:40:56',14),('vinod bhadouria','2011-05-09 17:53:50',14),('vinod bhadouria','2011-05-09 18:02:21',14),('vinod bhadouria','2011-05-09 18:14:50',14),('vinod bhadouria','2011-05-09 18:54:09',14),('vinod bhadouria','2011-05-10 18:08:20',14),('vinod bhadouria','2011-05-10 18:10:01',14),('vinod bhadouria','2011-05-10 18:26:35',14),('vinod bhadouria','2011-05-10 18:41:06',14),('vinod bhadouria','2011-05-10 18:44:03',14),('vinod bhadouria','2011-05-10 18:49:56',14),('vinod bhadouria','2011-05-10 18:55:02',14),('vinod bhadouria','2011-05-11 17:12:11',14),('vinod bhadouria','2011-05-11 18:40:51',14),('vinod bhadouria','2011-05-11 18:58:55',14),('vinod bhadouria','2011-05-11 19:16:39',14),('vinod bhadouria','2011-05-11 19:27:53',14),('vinod bhadouria','2011-05-16 03:25:44',14),('vinod bhadouria','2011-05-16 19:07:48',14),('vinod bhadouria','2011-05-16 19:15:18',14),('vinod bhadouria','2011-05-16 19:22:14',14),('vinod bhadouria','2011-05-18 16:19:33',14),('vinod bhadouria','2011-05-18 16:37:31',14),('vinod bhadouria','2011-05-19 16:24:06',14),('vinod bhadouria','2011-05-21 06:36:03',14),('vinod bhadouria','2011-05-21 07:47:13',14),('vinod bhadouria','2011-05-21 08:25:51',14),('vinod bhadouria','2011-05-21 11:46:37',14),('vinod bhadouria','2011-05-21 19:24:13',14),('vinod bhadouria','2011-05-21 19:43:14',14),('vinod bhadouria','2011-05-21 20:01:33',14),('vinod bhadouria','2011-05-21 20:12:46',14),('vinod bhadouria','2011-05-21 20:17:21',14),('vinod bhadouria','2011-05-21 20:23:57',14),('vinod bhadouria','2011-05-22 02:11:53',14),('vinod bhadouria','2011-05-22 02:49:34',14),('vinod bhadouria','2011-05-22 04:09:06',14),('vinod bhadouria','2011-05-22 05:50:45',14),('vinod bhadouria','2011-05-22 06:12:17',14),('vinod bhadouria','2011-05-22 06:45:19',14),('vinod bhadouria','2011-05-22 07:02:24',14),('vinod bhadouria','2011-05-22 07:50:34',14),('vinod bhadouria','2011-05-27 16:38:04',14),('vinod bhadouria','2011-05-27 16:49:42',14),('vinod bhadouria','2011-05-27 18:00:28',14),('vinod bhadouria','2011-05-27 19:41:28',14),('vinod bhadouria','2011-05-28 02:25:41',14),('vinod bhadouria','2011-05-28 05:07:01',14),('vinod bhadouria','2011-05-28 05:57:05',14),('vinod bhadouria','2011-05-28 06:02:12',14),('vinod bhadouria','2011-05-28 06:06:55',14),('vinod bhadouria','2011-05-28 06:18:18',14),('vinod bhadouria','2011-05-28 07:33:58',14),('vinod bhadouria','2011-05-28 08:36:49',14),('vinod bhadouria','2011-05-28 10:48:32',14),('vinod bhadouria','2011-05-28 14:46:26',14),('vinod bhadouria','2011-05-28 15:18:00',14),('vinod bhadouria','2011-05-28 15:30:42',14),('vinod bhadouria','2011-05-28 15:33:21',14),('vinod bhadouria','2011-05-28 15:36:03',14),('vinod bhadouria','2011-05-28 15:57:28',14),('vinod bhadouria','2011-05-28 16:52:08',14),('vinod bhadouria','2011-05-28 17:19:40',14),('vinod bhadouria','2011-05-28 17:26:29',14),('vinod bhadouria','2011-05-28 18:03:19',14),('vinod bhadouria','2011-05-28 18:13:23',14),('vinod bhadouria','2011-05-29 03:36:29',14),('vinod bhadouria','2011-05-29 03:48:46',14),('vinod bhadouria','2011-05-29 03:53:27',14),('vinod bhadouria','2011-05-29 05:09:01',14),('vinod bhadouria','2011-05-29 05:17:07',14),('vinod bhadouria','2011-05-29 05:37:44',14),('vinod bhadouria','2011-05-29 06:21:04',14),('vinod bhadouria','2011-05-29 06:23:13',14),('vinod bhadouria','2011-05-29 06:34:53',14),('vinod bhadouria','2011-05-29 07:06:52',14),('vinod bhadouria','2011-05-29 09:11:50',14),('vinod bhadouria','2011-05-29 11:22:48',14),('vinod bhadouria','2011-05-29 13:27:41',14),('vinod bhadouria','2011-05-29 13:45:58',14),('vinod bhadouria','2011-05-29 14:32:28',14),('vinod bhadouria','2011-05-29 14:49:55',14),('vinod bhadouria','2011-05-29 16:01:19',14),('vinod bhadouria','2011-05-29 16:04:01',14),('vinod bhadouria','2011-05-29 16:44:19',14),('vinod bhadouria','2011-05-29 16:56:59',14),('vinod bhadouria','2011-06-01 15:33:59',14),('vinod bhadouria','2011-06-01 17:59:55',14),('vinod bhadouria','2011-06-02 17:06:10',14),('vinod bhadouria','2011-06-02 17:45:53',14),('vinod bhadouria','2011-06-02 17:56:47',14),('vinod bhadouria','2011-06-02 18:35:58',14),('vinod bhadouria','2011-06-03 18:21:04',14),('vinod bhadouria','2011-06-03 19:27:24',14),('vinod bhadouria','2011-06-03 20:19:21',14),('vinod bhadouria','2011-06-04 09:31:17',14),('vinod bhadouria','2011-06-04 09:45:44',14),('vinod bhadouria','2011-06-04 10:18:36',14),('vinod bhadouria','2011-06-04 13:32:53',14),('vinod bhadouria','2011-06-04 14:49:35',14),('vinod bhadouria','2011-06-04 16:02:52',14),('vinod bhadouria','2011-06-04 18:01:49',14),('vinod bhadouria','2011-06-04 18:30:16',14),('vinod bhadouria','2011-06-04 18:40:03',14),('vinod bhadouria','2011-06-04 18:58:10',14),('vinod bhadouria','2011-06-04 19:02:01',14),('vinod bhadouria','2011-06-04 19:07:41',14),('vinod bhadouria','2011-06-04 19:14:59',14),('vinod bhadouria','2011-06-04 20:30:42',14),('vinod bhadouria','2011-06-04 20:39:03',14),('vinod bhadouria','2011-06-05 03:38:36',14),('vinod bhadouria','2011-06-05 06:04:27',14),('Arvindra Rathore','2011-06-05 06:15:16',15),('vinod bhadouria','2011-06-12 05:16:02',14),('vinod bhadouria','2011-06-12 08:57:37',14),('vinod bhadouria','2011-06-12 10:22:23',14),('vinod bhadouria','2011-06-12 10:33:26',14),('vinod bhadouria','2011-06-12 10:38:26',14),('vinod bhadouria','2011-06-12 10:56:13',14),('vinod bhadouria','2011-06-12 11:33:34',14),('vinod bhadouria','2011-06-12 11:56:27',14),('vinod bhadouria','2011-06-12 12:13:39',14),('vinod bhadouria','2011-06-12 12:21:30',14),('vinod bhadouria','2011-06-12 12:33:36',14),('vinod bhadouria','2011-06-12 12:36:05',14),('vinod bhadouria','2011-06-12 12:41:34',14),('vinod bhadouria','2011-06-12 12:43:33',14),('vinod bhadouria','2011-06-12 12:47:10',14),('vinod bhadouria','2011-06-12 12:52:01',14),('vinod bhadouria','2011-06-12 13:01:29',14),('vinod bhadouria','2011-06-12 13:03:47',14),('vinod bhadouria','2011-06-12 13:59:45',14),('vinod bhadouria','2011-06-16 17:02:04',14),('vinod bhadouria','2011-06-16 18:08:04',14),('vinod bhadouria','2011-06-16 18:28:26',14),('vinod bhadouria','2011-06-17 17:35:43',14),('vinod bhadouria','2011-06-18 05:39:06',14),('vinod bhadouria','2011-06-18 06:40:35',14),('vinod bhadouria','2011-06-18 07:14:00',14),('vinod bhadouria','2011-06-18 11:13:19',14),('vinod bhadouria','2011-06-18 12:10:10',14),('vinod bhadouria','2011-06-18 17:30:46',14),('vinod bhadouria','2011-06-18 18:33:04',14),('vinod bhadouria','2011-06-18 18:47:20',14),('vinod bhadouria','2011-06-18 18:59:54',14),('vinod bhadouria','2011-06-19 02:47:39',14),('vinod bhadouria','2011-06-19 03:06:34',14),('vinod bhadouria','2011-06-19 03:56:02',14),('vinod bhadouria','2011-06-19 03:59:58',14),('vinod bhadouria','2011-06-19 05:48:51',14),('vinod bhadouria','2011-06-19 05:59:06',14),('vinod bhadouria','2011-06-19 06:15:19',14),('vinod bhadouria','2011-06-19 06:39:35',14),('vinod bhadouria','2011-06-19 06:49:04',14),('vinod bhadouria','2011-06-19 07:47:51',14),('vinod bhadouria','2011-06-19 14:51:01',14),('vinod bhadouria','2011-06-19 15:12:39',14),('vinod bhadouria','2011-06-19 15:21:20',14),('vinod bhadouria','2011-06-19 16:10:32',14),('vinod bhadouria','2011-06-19 17:32:29',14),('vinod bhadouria','2011-06-19 20:26:41',14),('vinod bhadouria','2011-06-19 20:31:18',14),('vinod bhadouria','2011-06-19 21:02:24',14),('vinod bhadouria','2011-06-24 16:48:57',14),('vinod bhadouria','2011-06-24 17:33:06',14),('vinod bhadouria','2011-06-24 18:39:39',14),('vinod bhadouria','2011-06-24 18:52:18',14),('vinod bhadouria','2011-06-24 19:45:51',14),('vinod bhadouria','2011-06-25 06:25:51',14),('vinod bhadouria','2011-06-25 06:38:28',14),('vinod bhadouria','2011-06-25 06:41:09',14),('vinod bhadouria','2011-06-25 08:39:05',14),('vinod bhadouria','2011-06-25 08:42:39',14),('vinod bhadouria','2011-06-25 08:47:39',14),('vinod bhadouria','2011-06-25 09:32:12',14),('vinod bhadouria','2011-06-25 09:38:01',14),('vinod bhadouria','2011-06-25 09:54:34',14),('vinod bhadouria','2011-06-25 09:56:04',14),('vinod bhadouria','2011-06-25 10:06:53',14),('vinod bhadouria','2011-06-25 17:05:20',14),('vinod bhadouria','2011-06-25 17:22:32',14),('vinod bhadouria','2011-06-26 17:24:53',14),('vinod bhadouria','2011-06-26 17:33:14',14),('vinod bhadouria','2011-06-26 17:41:56',14),('vinod bhadouria','2011-06-26 17:49:18',14),('vinod bhadouria','2011-06-27 18:01:04',14),('vinod bhadouria','2011-07-02 04:13:35',14),('vinod bhadouria','2011-07-03 01:50:22',14),('vinod bhadouria','2011-07-03 02:02:51',14),('vinod bhadouria','2011-07-03 02:05:38',14),('vinod bhadouria','2011-07-08 03:55:07',14),('vinod bhadouria','2011-07-08 06:09:05',14),('vinod bhadouria','2011-07-08 07:33:43',14),('vinod bhadouria','2011-07-09 05:42:03',14),('vinod bhadouria','2011-07-09 06:01:30',14),('vinod bhadouria','2011-07-14 17:07:17',14),('vinod bhadouria','2011-07-16 05:26:54',14),('vinod bhadouria','2011-07-16 16:05:22',14),('vinod bhadouria','2011-07-23 11:19:30',14),('vinod bhadouria','2011-07-23 11:44:55',14),('vinod bhadouria','2011-07-23 12:16:50',14),('vinod bhadouria','2011-07-23 14:34:18',14),('vinod bhadouria','2011-07-23 16:20:45',14),('vinod bhadouria','2011-07-23 16:25:29',14),('vinod bhadouria','2011-07-23 16:47:24',14),('vinod bhadouria','2011-07-23 16:53:08',14),('vinod bhadouria','2011-07-23 17:17:56',14),('vinod bhadouria','2011-07-23 18:36:35',14),('vinod bhadouria','2011-07-23 19:21:07',14),('vinod bhadouria','2011-07-24 07:53:59',14),('vinod bhadouria','2011-07-24 08:10:48',14),('vinod bhadouria','2011-07-24 08:27:18',14),('vinod bhadouria','2011-07-24 09:44:52',14),('vinod bhadouria','2011-07-24 09:51:33',14),('vinod bhadouria','2011-07-24 10:23:27',14),('vinod bhadouria','2011-07-24 10:45:26',14),('vinod bhadouria','2011-07-24 10:51:04',14),('vinod bhadouria','2011-07-24 11:22:26',14),('vinod bhadouria','2011-07-24 11:34:44',14),('vinod bhadouria','2011-07-24 11:42:51',14),('vinod bhadouria','2011-07-24 11:54:25',14),('vinod bhadouria','2011-07-24 12:03:52',14),('vinod bhadouria','2011-07-30 17:08:19',14),('vinod bhadouria','2011-07-30 18:15:13',14),('vinod bhadouria','2011-07-30 19:10:52',14),('vinod bhadouria','2011-07-30 19:13:00',14),('vinod bhadouria','2011-07-30 20:23:41',14),('vinod bhadouria','2011-07-30 20:30:52',14),('vinod bhadouria','2011-07-31 08:02:37',14),('vinod bhadouria','2011-07-31 11:36:57',14),('vinod bhadouria','2011-07-31 12:06:31',14),('vinod bhadouria','2011-07-31 12:24:07',14),('vinod bhadouria','2011-07-31 14:13:34',14),('vinod bhadouria','2011-07-31 17:02:18',14),('vinod bhadouria','2011-07-31 17:49:14',14),('vinod bhadouria','2011-07-31 17:54:30',14),('vinod bhadouria','2011-08-03 16:55:35',14),('vinod bhadouria','2011-08-06 06:29:14',14),('vinod bhadouria','2011-08-06 19:19:40',14),('vinod bhadouria','2011-08-07 04:09:19',14),('vinod bhadouria','2011-08-07 06:42:39',14),('vinod bhadouria','2011-08-07 06:52:46',14),('vinod bhadouria','2011-08-07 07:00:48',14),('vinod bhadouria','2011-08-07 08:14:54',14),('vinod bhadouria','2011-08-07 09:59:01',14),('vinod bhadouria','2011-08-07 10:48:17',14),('vinod bhadouria','2011-08-07 11:14:40',14),('vinod bhadouria','2011-08-07 11:42:18',14),('vinod bhadouria','2011-08-07 18:28:02',14),('vinod bhadouria','2011-08-07 18:33:05',14),('vinod bhadouria','2011-08-07 18:40:16',14),('vinod bhadouria','2011-08-07 18:53:10',14),('vinod bhadouria','2011-08-07 18:58:09',14),('vinod bhadouria','2011-08-07 19:02:17',14),('vinod bhadouria','2011-08-08 05:28:16',14),('vinod bhadouria','2011-08-08 06:36:29',14),('vinod bhadouria','2011-08-08 06:58:00',14),('vinod bhadouria','2011-08-08 14:25:49',14),('vinod bhadouria','2011-08-08 15:35:04',14),('vinod bhadouria','2011-08-08 17:53:01',14),('vinod bhadouria','2011-08-09 16:55:09',14),('vinod bhadouria','2011-08-09 17:08:42',14),('vinod bhadouria','2011-08-09 17:11:28',14),('vinod bhadouria','2011-08-09 17:52:50',14),('vinod bhadouria','2011-08-10 16:19:53',14),('vinod bhadouria','2011-08-10 17:43:19',14),('vinod bhadouria','2011-08-10 17:51:47',14),('vinod bhadouria','2011-08-11 16:18:30',14),('vinod bhadouria','2011-08-11 17:13:24',14),('vinod bhadouria','2011-08-11 18:44:06',14),('vinod bhadouria','2011-08-11 18:48:01',14),('vinod bhadouria','2011-08-11 18:53:37',14),('vinod bhadouria','2011-08-11 18:55:37',14),('vinod bhadouria','2011-08-13 05:32:42',14),('vinod bhadouria','2011-08-13 07:02:34',14),('vinod bhadouria','2011-08-13 07:05:34',14),('vinod bhadouria','2011-08-13 07:13:17',14),('vinod bhadouria','2011-08-13 08:37:18',14),('vinod bhadouria','2011-08-14 06:24:08',14),('vinod bhadouria','2011-08-14 07:59:53',14),('vinod bhadouria','2011-08-14 08:41:10',14),('vinod bhadouria','2011-08-14 09:15:35',14),('vinod bhadouria','2011-08-14 09:30:37',14),('vinod bhadouria','2011-08-14 11:34:12',14),('vinod bhadouria','2011-08-14 12:10:07',14),('vinod bhadouria','2011-08-14 14:07:13',14),('vinod bhadouria','2011-08-14 16:23:25',14),('vinod bhadouria','2011-08-15 07:22:03',14),('vinod bhadouria','2011-08-15 08:22:27',14),('vinod bhadouria','2011-08-15 11:32:36',14),('vinod bhadouria','2011-08-15 12:37:37',14),('vinod bhadouria','2011-08-15 12:58:41',14),('vinod bhadouria','2011-08-15 13:26:26',14),('vinod bhadouria','2011-08-15 13:38:15',14),('vinod bhadouria','2011-08-15 14:43:12',14),('vinod bhadouria','2011-08-15 16:36:32',14),('vinod bhadouria','2011-08-16 16:06:45',14),('vinod bhadouria','2011-08-16 16:44:48',14),('vinod bhadouria','2011-08-16 17:57:13',14),('vinod bhadouria','2011-08-16 18:31:40',14),('vinod bhadouria','2011-08-16 19:10:16',14),('vinod bhadouria','2011-08-17 18:42:02',14),('vinod bhadouria','2011-08-18 18:14:50',14),('vinod bhadouria','2011-08-18 19:24:16',14),('vinod bhadouria','2011-08-18 20:17:35',14),('vinod bhadouria','2011-08-18 20:42:08',14),('vinod bhadouria','2011-08-18 20:44:57',14),('vinod bhadouria','2011-08-19 04:55:10',14),('vinod bhadouria','2011-08-19 05:44:04',14),('vinod bhadouria','2011-08-19 06:20:16',14),('vinod bhadouria','2011-08-19 06:33:40',14),('vinod bhadouria','2011-08-19 06:36:31',14),('vinod bhadouria','2011-08-19 07:27:30',14),('vinod bhadouria','2011-08-19 08:13:28',14),('vinod bhadouria','2011-08-19 11:22:47',14),('vinod bhadouria','2011-08-20 05:56:05',14),('vinod bhadouria','2011-08-20 06:03:35',14),('vinod bhadouria','2011-08-20 06:06:33',14),('vinod bhadouria','2011-08-20 07:05:02',14),('vinod bhadouria','2011-08-20 10:42:56',14),('vinod bhadouria','2011-08-20 11:25:15',14),('vinod bhadouria','2011-08-20 14:31:29',14),('vinod bhadouria','2011-08-20 19:05:49',14),('vinod bhadouria','2011-08-21 10:17:12',14),('vinod bhadouria','2011-08-21 11:29:04',14),('vinod bhadouria','2011-08-21 11:49:37',14),('vinod bhadouria','2011-08-21 14:07:16',14),('vinod bhadouria','2011-08-21 14:10:28',14),('vinod bhadouria','2011-08-21 14:19:57',14),('vinod bhadouria','2011-08-21 14:25:38',14),('vinod bhadouria','2011-08-21 14:28:23',14),('vinod bhadouria','2011-08-21 14:31:18',14),('vinod bhadouria','2011-08-21 14:47:52',14),('vinod bhadouria','2011-08-21 15:08:04',14),('vinod bhadouria','2011-08-21 15:40:04',14),('vinod bhadouria','2011-08-22 02:30:04',14),('vinod bhadouria','2011-08-22 02:49:53',14),('vinod bhadouria','2011-08-22 16:59:16',14),('vinod bhadouria','2011-08-22 17:09:41',14),('vinod bhadouria','2011-08-22 17:22:00',14),('vinod bhadouria','2011-08-22 17:26:03',14),('vinod bhadouria','2011-08-22 18:09:26',14),('vinod bhadouria','2011-08-22 19:09:31',14),('vinod bhadouria','2011-08-22 19:33:14',14),('vinod bhadouria','2011-08-23 18:26:30',14),('vinod bhadouria','2011-08-26 03:43:34',14),('vinod bhadouria','2011-08-26 15:03:51',14),('vinod bhadouria','2011-08-26 17:13:13',14),('vinod bhadouria','2011-08-26 17:16:33',14),('vinod bhadouria','2011-08-26 17:31:36',14),('vinod bhadouria','2011-08-26 18:58:55',14),('vinod bhadouria','2011-08-26 19:05:54',14),('vinod bhadouria','2011-08-26 19:07:33',14),('vinod bhadouria','2011-08-27 03:08:35',14),('vinod bhadouria','2011-08-27 04:21:01',14),('vinod bhadouria','2011-08-27 06:39:40',14),('vinod bhadouria','2011-08-27 06:46:28',14),('vinod bhadouria','2011-08-27 06:50:14',14),('vinod bhadouria','2011-08-27 06:50:52',14),('vinod bhadouria','2011-08-27 06:57:46',14),('vinod bhadouria','2011-08-27 07:05:23',14),('vinod bhadouria','2011-08-27 07:08:58',14),('vinod bhadouria','2011-08-27 07:09:20',14),('vinod bhadouria','2011-08-27 07:09:32',14),('vinod bhadouria','2011-08-27 07:10:18',14),('vinod bhadouria','2011-08-27 07:11:02',14),('vinod bhadouria','2011-08-27 07:11:29',14),('vinod bhadouria','2011-08-27 07:12:14',14),('vinod bhadouria','2011-08-27 07:12:24',14),('vinod bhadouria','2011-08-27 07:12:56',14),('vinod bhadouria','2011-08-27 07:13:32',14),('vinod bhadouria','2011-08-27 07:14:07',14),('vinod bhadouria','2011-08-27 07:14:30',14),('vinod bhadouria','2011-08-27 07:15:07',14),('vinod bhadouria','2011-08-27 07:15:22',14),('vinod bhadouria','2011-08-27 07:15:39',14),('vinod bhadouria','2011-08-27 07:15:48',14),('vinod bhadouria','2011-08-27 07:15:56',14),('vinod bhadouria','2011-08-27 07:16:10',14),('vinod bhadouria','2011-08-27 07:17:03',14),('vinod bhadouria','2011-08-27 07:17:13',14),('vinod bhadouria','2011-08-27 07:17:57',14),('vinod bhadouria','2011-08-27 07:18:00',14),('vinod bhadouria','2011-08-27 07:18:16',14),('vinod bhadouria','2011-08-27 07:18:19',14),('vinod bhadouria','2011-08-27 07:18:33',14),('vinod bhadouria','2011-08-27 07:18:46',14),('vinod bhadouria','2011-08-27 07:19:02',14),('vinod bhadouria','2011-08-27 07:19:53',14),('vinod bhadouria','2011-08-27 08:21:31',14),('vinod bhadouria','2011-08-27 08:47:14',14),('vinod bhadouria','2011-08-27 09:00:31',14),('vinod bhadouria','2011-08-27 09:03:15',14),('vinod bhadouria','2011-08-27 12:46:01',14),('vinod bhadouria','2011-08-27 16:07:27',14),('vinod bhadouria','2011-08-27 16:12:23',14),('vinod bhadouria','2011-08-27 16:15:45',14),('vinod bhadouria','2011-08-27 16:45:30',14),('vinod bhadouria','2011-08-27 16:49:07',14),('vinod bhadouria','2011-08-27 16:50:00',14),('vinod bhadouria','2011-08-27 17:09:19',14),('vinod bhadouria','2011-08-27 17:15:06',14),('vinod bhadouria','2011-08-27 17:16:29',14),('vinod bhadouria','2011-08-27 17:19:24',14),('vinod bhadouria','2011-08-27 17:24:11',14),('vinod bhadouria','2011-08-27 17:32:46',14),('vinod bhadouria','2011-08-27 17:34:19',14),('vinod bhadouria','2011-08-27 18:00:39',14),('vinod bhadouria','2011-08-27 18:18:04',14),('vinod bhadouria','2011-08-27 18:40:22',14),('vinod bhadouria','2011-08-27 18:52:06',14),('vinod bhadouria','2011-08-27 19:14:52',14),('vinod bhadouria','2011-08-28 01:59:41',14),('vinod bhadouria','2011-08-28 02:47:13',14),('vinod bhadouria','2011-08-28 06:06:05',14),('vinod bhadouria','2011-08-28 06:32:43',14),('vinod bhadouria','2011-08-28 07:03:11',14),('vinod bhadouria','2011-08-28 07:33:51',14),('vinod bhadouria','2011-08-28 07:58:46',14),('vinod bhadouria','2011-08-28 10:59:32',14),('vinod bhadouria','2011-08-28 11:14:48',14),('vinod bhadouria','2011-08-28 11:42:34',14),('vinod bhadouria','2011-08-28 11:47:09',14),('vinod bhadouria','2011-08-28 11:58:00',14),('vinod bhadouria','2011-08-28 14:18:40',14),('vinod bhadouria','2011-08-28 14:25:07',14),('vinod bhadouria','2011-08-28 15:04:46',14),('vinod bhadouria','2011-08-28 17:34:38',14),('vinod bhadouria','2011-08-29 02:05:54',14),('vinod bhadouria','2011-08-29 04:15:43',14),('vinod bhadouria','2011-08-29 05:04:18',14),('vinod bhadouria','2011-08-29 07:25:02',14),('vinod bhadouria','2011-08-29 08:00:57',14),('vinod bhadouria','2011-08-29 11:24:47',14),('vinod bhadouria','2011-08-29 16:36:44',14),('vinod bhadouria','2011-08-29 17:55:38',14),('vinod bhadouria','2011-08-31 15:28:55',14),('vinod bhadouria','2011-08-31 17:34:51',14),('vinod bhadouria','2011-08-31 18:35:39',14),('vinod bhadouria','2011-09-01 04:15:59',14),('vinod bhadouria','2011-09-01 16:37:34',14),('vinod bhadouria','2011-09-02 17:47:45',14),('vinod bhadouria','2011-09-02 18:31:33',14),('vinod bhadouria','2011-09-03 05:04:12',14),('vinod bhadouria','2011-09-03 05:26:19',14),('vinod bhadouria','2011-09-03 05:53:48',14),('vinod bhadouria','2011-09-03 06:20:07',14),('vinod bhadouria','2011-09-03 06:39:49',14),('vinod bhadouria','2011-09-03 07:16:28',14),('vinod bhadouria','2011-09-03 08:31:20',14),('vinod bhadouria','2011-09-03 13:39:48',14),('vinod bhadouria','2011-09-03 15:24:43',14),('vinod bhadouria','2011-09-03 19:35:45',14),('vinod bhadouria','2011-09-03 20:01:48',14),('vinod bhadouria','2011-09-03 20:24:14',14),('vinod bhadouria','2011-09-04 03:58:03',14),('Milan Chauhan','2011-09-06 17:02:11',43),('Anuj Bhadouria','2011-09-06 17:24:25',35),('Milan Chauhan','2011-09-06 17:35:11',43),('Milan Chauhan','2011-09-06 17:53:15',43),('Milan Chauhan','2011-09-06 18:14:21',43),('Milan Chauhan','2011-09-07 17:12:19',43),('Anuj Bhadouria','2011-09-10 15:18:23',35),('Milan Chauhan','2011-09-10 15:19:16',43),('Milan Chauhan','2011-09-17 06:35:05',43),('Milan Chauhan','2011-10-27 03:52:57',43),('Milan Chauhan','2011-10-31 04:11:07',43),('Milan Chauhan','2011-11-24 16:47:50',43),('Milan Chauhan','2011-11-24 16:57:24',43),('first_name  last_name','2012-06-02 08:26:32',30),('Vikash Rajput','2012-06-02 08:32:57',44),('Vikash Rajput','2012-06-02 08:40:01',44),('Vikash Rajput','2012-06-02 08:44:46',44),('Milan Chauhan','2012-06-02 09:10:14',43),('Milan Chauhan','2012-06-02 11:31:09',43),('Milan Chauhan','2012-06-02 11:51:49',43),('Milan Chauhan','2012-06-02 12:05:34',43),('Milan Chauhan','2012-06-02 12:30:45',43),('Milan Chauhan','2012-06-02 12:45:09',43),('Milan Chauhan','2012-06-02 13:04:20',43),('Milan Chauhan','2012-06-02 13:18:19',43),('Milan Chauhan','2012-06-02 14:03:25',43),('Milan Chauhan','2012-06-03 03:16:24',43),('Milan Chauhan','2012-06-03 06:35:50',43),('Milan Chauhan','2012-06-03 06:36:40',43),('Milan Chauhan','2012-06-03 08:58:12',43),('Milan Chauhan','2012-06-03 09:27:48',43),('Milan Chauhan','2012-06-03 09:28:54',43),('Milan Chauhan','2012-06-03 09:59:19',43),('Milan Chauhan','2012-06-03 10:02:14',43),('Milan Chauhan','2012-06-03 10:04:45',43),('Milan Chauhan','2012-06-03 10:10:06',43),('Milan Chauhan','2012-06-03 15:21:15',43),('Milan Chauhan','2012-06-03 17:14:44',43),('Milan Chauhan','2012-06-03 17:18:44',43),('Milan Chauhan','2012-06-03 17:27:56',43),('Ram Sharma','2012-06-03 18:02:06',40),('Ram Sharma','2012-06-03 18:04:53',40),('Ram Sharma','2012-06-03 18:06:46',40),('Ram Sharma','2012-06-03 18:10:31',40),('Ram Sharma','2012-06-03 18:12:54',40),('Milan Chauhan','2012-06-23 02:16:13',43),('Milan Chauhan','2012-06-23 02:22:49',43),('Milan Chauhan','2012-06-23 02:41:51',43),('Milan Chauhan','2012-07-09 01:58:36',43),('Milan Chauhan','2012-07-09 02:05:04',43),('Milan Chauhan','2012-07-29 08:19:44',43),('Milan Chauhan','2012-07-29 13:52:16',43),('Milan Chauhan','2012-07-29 14:05:55',43),('Milan Chauhan','2012-07-29 14:36:52',43),('Milan Chauhan','2012-07-29 15:30:04',43),('Milan Chauhan','2012-07-29 17:51:36',43),('Milan Chauhan','2012-07-30 16:26:25',43),('Milan Chauhan','2012-07-31 01:55:47',43),('Milan Chauhan','2012-07-31 01:57:24',43),('Milan Chauhan','2012-07-31 02:16:30',43),('Milan Chauhan','2012-08-02 05:36:43',43),('Milan Chauhan','2012-08-02 06:53:22',43),('Milan Chauhan','2012-08-02 08:26:58',43),('Milan Chauhan','2012-08-02 10:55:18',43),('Milan Chauhan','2012-08-02 11:47:45',43),('Milan Chauhan','2012-08-02 12:13:57',43),('Milan Chauhan','2012-08-02 15:12:19',43),('Milan Chauhan','2012-08-02 16:55:12',43),('Milan Chauhan','2012-08-02 17:57:10',43),('Milan Chauhan','2012-08-02 18:19:15',43),('Milan Chauhan','2012-08-02 18:28:56',43),('Milan Chauhan','2012-08-02 19:41:04',43),('Milan Chauhan','2012-08-02 20:14:19',43),('Milan Chauhan','2012-08-02 21:48:31',43),('Milan Chauhan','2012-08-03 18:37:48',43),('Milan Chauhan','2012-08-03 19:29:35',43),('Milan Chauhan','2012-08-03 19:42:00',43),('Milan Chauhan','2012-08-03 19:52:04',43),('Milan Chauhan','2012-08-04 10:53:31',43),('Milan Chauhan','2012-08-04 11:32:01',43),('Milan Chauhan','2012-08-04 11:59:08',43),('Milan Chauhan','2012-08-04 12:04:18',43),('Milan Chauhan','2012-08-04 18:28:17',43),('Milan Chauhan','2012-08-04 18:49:40',43),('Milan Chauhan','2012-08-05 04:30:39',43),('Milan Chauhan','2012-08-05 02:08:56',43),('Milan Chauhan','2012-08-05 02:18:21',43),('Milan Chauhan','2012-08-05 03:02:57',43),('Milan Chauhan','2012-08-05 04:05:38',43),('Milan Chauhan','2012-08-05 14:56:14',43),('Milan Chauhan','2012-08-05 16:01:34',43),('Milan Chauhan','2012-08-06 18:34:15',43),('Milan Chauhan','2012-08-06 19:23:15',43),('Milan Chauhan','2012-08-06 19:23:42',43),('Milan Chauhan','2012-08-06 20:51:18',43),('Milan Chauhan','2012-08-06 21:00:09',43),('Milan Chauhan','2012-08-07 16:41:32',43),('Milan Chauhan','2012-08-07 16:42:03',43),('Milan Chauhan','2012-08-07 17:17:52',43),('Milan Chauhan','2012-08-07 17:49:30',43),('Milan Chauhan','2012-08-07 18:05:14',43),('Milan Chauhan','2012-08-07 18:29:13',43),('Milan Chauhan','2012-08-07 18:38:03',43),('Milan Chauhan','2012-08-07 18:48:13',43),('Milan Chauhan','2012-08-07 19:05:14',43),('Milan Chauhan','2012-08-07 19:21:22',43),('Milan Chauhan','2012-08-07 19:47:48',43),('Milan Chauhan','2012-08-07 19:58:21',43),('Milan Chauhan','2012-08-07 20:16:37',43),('Milan Chauhan','2012-08-07 21:17:43',43),('Milan Chauhan','2012-08-08 16:37:50',43),('Milan Chauhan','2012-08-08 17:23:21',43),('Milan Chauhan','2012-08-08 17:38:24',43),('Milan Chauhan','2012-08-08 18:08:31',43),('Milan Chauhan','2012-08-08 18:51:46',43),('Milan Chauhan','2012-08-08 19:49:28',43),('Milan Chauhan','2012-08-08 19:59:32',43),('Milan Chauhan','2012-08-08 20:14:22',43),('Milan Chauhan','2012-08-15 06:51:17',43),('Milan Chauhan','2012-08-15 12:29:57',43),('Milan Chauhan','2012-08-17 16:28:36',43),('Milan Chauhan','2012-08-17 17:31:37',43),('Milan Chauhan','2012-08-17 18:24:42',43),('Milan Chauhan','2012-08-18 04:28:19',43),('Milan Chauhan','2012-08-18 06:04:37',43),('Milan Chauhan','2012-08-18 07:13:19',43),('Milan Chauhan','2012-08-18 11:25:26',43),('Milan Chauhan','2012-08-18 17:38:39',43),('Milan Chauhan','2012-08-18 17:46:34',43),('Milan Chauhan','2012-08-18 18:18:17',43),('Milan Chauhan','2012-08-18 18:37:20',43),('Milan Chauhan','2012-08-18 18:53:39',43),('Milan Chauhan','2012-08-18 18:54:32',43),('Milan Chauhan','2012-08-18 18:58:25',43),('Milan Chauhan','2012-08-18 19:05:53',43),('Milan Chauhan','2012-08-18 19:30:30',43),('Milan Chauhan','2012-08-18 19:35:53',43),('Milan Chauhan','2012-08-19 04:04:55',43),('Milan Chauhan','2012-08-19 04:13:56',43),('Milan Chauhan','2012-08-19 04:19:18',43),('Milan Chauhan','2012-08-19 04:35:50',43),('Milan Chauhan','2012-08-19 05:07:58',43),('Milan Chauhan','2012-08-19 07:21:42',43),('Milan Chauhan','2012-08-19 07:49:56',43),('Milan Chauhan','2012-08-19 07:57:38',43),('Milan Chauhan','2012-08-19 08:04:35',43),('Milan Chauhan','2012-08-19 08:18:40',43),('Milan Chauhan','2012-08-19 08:52:54',43),('Milan Chauhan','2012-08-19 09:02:49',43),('Milan Chauhan','2012-08-19 09:08:44',43),('Milan Chauhan','2012-08-19 09:46:19',43),('Milan Chauhan','2012-08-19 12:24:49',43),('Milan Chauhan','2012-08-19 15:04:14',43),('Milan Chauhan','2012-08-19 19:33:55',43),('Milan Chauhan','2012-08-20 03:39:08',43),('Milan Chauhan','2012-08-20 05:28:35',43),('Milan Chauhan','2012-08-20 05:36:00',43),('Milan Chauhan','2012-08-20 05:52:30',43),('Milan Chauhan','2012-08-20 09:30:49',43),('Milan Chauhan','2012-08-20 09:34:46',43),('Milan Chauhan','2012-08-20 09:46:35',43),('Milan Chauhan','2012-08-20 09:57:23',43),('Milan Chauhan','2012-08-20 12:57:04',43),('Milan Chauhan','2012-08-20 15:01:14',43),('Milan Chauhan','2012-08-20 16:13:12',43),('Milan Chauhan','2012-08-20 16:14:29',43),('Milan Chauhan','2012-08-20 17:32:48',43),('Milan Chauhan','2012-08-21 15:28:54',43),('Milan Chauhan','2012-08-21 15:36:01',43),('Milan Chauhan','2012-08-22 14:49:54',43),('Milan Chauhan','2012-08-22 14:56:54',43),('Milan Chauhan','2012-08-22 15:57:21',43),('Milan Chauhan','2012-08-22 16:16:07',43),('Milan Chauhan','2012-08-22 17:09:25',43),('Milan Chauhan','2012-08-23 14:05:23',43),('Milan Chauhan','2012-08-23 15:35:38',43),('Milan Chauhan','2012-08-23 15:47:50',43),('Milan Chauhan','2012-08-23 16:07:02',43),('Milan Chauhan','2012-08-23 16:52:27',43),('Milan Chauhan','2012-08-23 16:59:24',43),('Milan Chauhan','2012-08-24 15:07:39',43),('Milan Chauhan','2012-08-24 16:28:40',43),('Milan Chauhan','2012-08-24 16:30:52',43),('Milan Chauhan','2012-08-25 06:26:23',43),('Milan Chauhan','2012-08-25 06:32:18',43),('Milan Chauhan','2012-08-25 07:03:21',43),('Milan Chauhan','2012-08-25 07:51:41',43),('Milan Chauhan','2012-08-25 08:41:05',43),('Milan Chauhan','2012-08-25 08:48:10',43),('Milan Chauhan','2012-08-25 08:52:27',43),('Milan Chauhan','2012-08-25 12:37:54',43),('Milan Chauhan','2012-08-25 13:19:56',43),('Milan Chauhan','2012-08-26 14:12:59',43),('Milan Chauhan','2012-08-26 14:54:26',43),('Milan Chauhan','2012-08-26 15:45:46',43),('Milan Chauhan','2012-08-26 18:30:04',43),('Milan Chauhan','2012-08-26 18:40:32',43),('Milan Chauhan','2012-08-26 18:45:14',43),('Milan Chauhan','2012-08-27 15:20:58',43),('Milan Chauhan','2012-08-27 15:55:53',43),('Milan Chauhan','2012-08-27 15:58:07',43),('Milan Chauhan','2012-08-27 16:27:25',43),('Milan Chauhan','2012-08-28 14:09:25',43),('Milan Chauhan','2012-08-28 15:56:18',43),('Milan Chauhan','2012-08-28 16:20:30',43),('Milan Chauhan','2012-08-29 02:40:01',43),('Milan Chauhan','2012-08-30 16:54:42',43),('Milan Chauhan','2012-08-31 02:06:01',43),('Milan Chauhan','2012-08-31 17:00:05',43),('Milan Chauhan','2012-08-31 17:53:21',43),('Milan Chauhan','2012-08-31 18:14:15',43),('Milan Chauhan','2012-08-31 18:15:35',43),('Milan Chauhan','2012-08-31 19:07:30',43),('Milan Chauhan','2012-08-31 19:20:03',43),('Milan Chauhan','2012-08-31 19:31:46',43),('Milan Chauhan','2012-09-01 03:08:40',43),('Milan Chauhan','2012-09-01 03:27:22',43),('Milan Chauhan','2012-09-01 03:36:59',43),('Milan Chauhan','2012-09-01 03:45:02',43),('Milan Chauhan','2012-09-01 04:06:32',43),('Milan Chauhan','2012-09-01 04:44:28',43),('Milan Chauhan','2012-09-01 04:48:25',43),('Milan Chauhan','2012-09-01 10:43:07',43),('Milan Chauhan','2012-09-01 12:20:05',43),('Milan Chauhan','2012-09-01 14:25:37',43),('Milan Chauhan','2012-09-01 16:26:25',43),('Milan Chauhan','2012-09-01 17:25:37',43),('Milan Chauhan','2012-09-01 19:02:35',43),('Milan Chauhan','2012-09-01 19:08:26',43),('Milan Chauhan','2012-09-02 02:13:01',43),('Milan Chauhan','2012-09-02 02:13:32',43),('Milan Chauhan','2012-09-02 03:06:43',43),('Milan Chauhan','2012-09-02 03:10:41',43),('Milan Chauhan','2012-09-02 06:36:19',43),('Milan Chauhan','2012-09-02 06:42:15',43),('Milan Chauhan','2012-09-02 09:29:37',43),('Milan Chauhan','2012-09-02 11:46:14',43),('Milan Chauhan','2012-09-02 12:19:17',43),('Milan Chauhan','2012-09-02 12:19:51',43),('Milan Chauhan','2012-09-02 12:36:23',43),('Milan Chauhan','2012-09-02 12:46:33',43),('Milan Chauhan','2012-09-02 14:20:50',43),('Milan Chauhan','2012-09-02 16:41:33',43),('Milan Chauhan','2012-09-02 16:54:33',43),('Milan Chauhan','2012-09-02 16:59:33',43),('Milan Chauhan','2012-09-02 17:15:26',43),('Milan Chauhan','2012-09-03 16:12:03',43),('Milan Chauhan','2012-09-03 16:31:08',43),('Milan Chauhan','2012-09-03 17:02:36',43),('Milan Chauhan','2012-09-03 17:15:42',43),('Milan Chauhan','2012-09-03 17:24:35',43),('Milan Chauhan','2012-09-05 02:14:27',43),('Milan Chauhan','2012-09-05 02:50:57',43),('Milan Chauhan','2012-09-05 14:27:47',43),('Milan Chauhan','2012-09-05 15:52:02',43),('Milan Chauhan','2012-09-05 15:52:33',43),('Milan Chauhan','2012-09-05 16:32:49',43),('Milan Chauhan','2012-09-05 16:45:36',43),('Milan Chauhan','2012-09-05 17:17:23',43),('Milan Chauhan','2012-09-06 01:59:06',43),('Milan Chauhan','2012-09-06 12:15:19',43),('Milan Chauhan','2012-09-06 14:44:22',43),('Milan Chauhan','2012-09-06 16:33:03',43),('Milan Chauhan','2012-09-06 16:55:28',43),('Milan Chauhan','2012-09-07 01:17:53',43),('Milan Chauhan','2012-09-07 02:18:09',43),('Milan Chauhan','2012-09-07 02:30:07',43),('Milan Chauhan','2012-09-07 17:41:27',43),('Milan Chauhan','2012-09-07 17:48:26',43),('Milan Chauhan','2012-09-07 17:53:24',43),('Milan Chauhan','2012-09-07 18:05:51',43),('Milan Chauhan','2012-09-07 18:07:06',43),('Milan Chauhan','2012-09-08 01:55:56',43),('Milan Chauhan','2012-09-08 01:58:30',43),('Milan Chauhan','2012-09-08 03:54:00',43),('Milan Chauhan','2012-09-08 07:41:44',43),('Milan Chauhan','2012-09-08 09:11:35',43),('Milan Chauhan','2012-09-08 11:56:59',43),('Milan Chauhan','2012-09-08 13:11:29',43),('Milan Chauhan','2012-09-08 14:23:43',43),('Milan Chauhan','2012-09-08 15:18:06',43),('Milan Chauhan','2012-09-08 16:17:02',43),('Milan Chauhan','2012-09-08 16:32:33',43),('Milan Chauhan','2012-09-08 16:45:09',43),('Milan Chauhan','2012-09-08 17:24:25',43),('Milan Chauhan','2012-09-08 17:31:11',43),('Milan Chauhan','2012-09-09 02:00:13',43),('Milan Chauhan','2012-09-09 02:09:23',43),('Milan Chauhan','2012-09-09 02:23:54',43),('Milan Chauhan','2012-09-09 02:37:05',43),('Milan Chauhan','2012-09-09 02:43:31',43),('Milan Chauhan','2012-09-09 03:30:22',43),('Milan Chauhan','2012-09-09 04:31:15',43),('Milan Chauhan','2012-09-09 04:42:09',43),('Milan Chauhan','2012-09-09 06:28:31',43),('Milan Chauhan','2012-09-09 07:20:41',43),('Milan Chauhan','2012-09-09 07:35:13',43),('Pratima Rathore','2012-09-09 07:43:33',45),('vinod ','2012-09-09 07:44:55',43),('Raman Bhadouria','2012-09-09 07:45:42',42),('Milan Chauhan','2012-09-09 08:33:50',43),('Milan Chauhan','2012-09-09 11:14:18',43),('Milan Chauhan','2012-09-09 12:27:34',43),('Raman Bhadouria','2012-09-09 12:51:10',42),('Raman Bhadouria','2012-09-09 13:04:29',42),('Raman Bhadouria','2012-09-09 13:11:51',42),('Raman Bhadouria','2012-09-09 13:12:39',42),('Raman Bhadouria','2012-09-09 13:12:45',42),('Raman Bhadouria','2012-09-09 13:17:32',42),('Raman Bhadouria','2012-09-09 13:17:50',42),('Raman Bhadouria','2012-09-09 13:21:32',42),('vinod ','2012-09-09 13:48:43',43),('vinod ','2012-09-09 13:50:40',43),('vinod ','2012-09-09 13:50:48',43),('vinod ','2012-09-09 13:52:50',43),('vinod ','2012-09-09 13:59:34',43),('vinod ','2012-09-09 14:01:31',43),('vinod ','2012-09-09 14:02:34',43),('vinod ','2012-09-09 14:03:20',43),('vinod ','2012-09-09 14:10:16',43),('vinod ','2012-09-09 14:10:55',43),('vinod ','2012-09-09 14:10:59',43),('vinod ','2012-09-09 14:11:00',43),('vinod ','2012-09-09 14:12:28',43),('vinod ','2012-09-09 14:15:35',43),('vinod ','2012-09-09 14:20:42',43),('vinod ','2012-09-09 14:21:34',43),('Milan Chauhan','2012-09-09 15:10:16',43),('Raman Bhadouria','2012-09-09 16:11:52',42),('Raman Bhadouria','2012-09-09 16:19:12',42),('Raman Bhadouria','2012-09-09 16:25:21',42),('Raman Bhadouria','2012-09-09 16:51:42',42),('Raman Bhadouria','2012-09-09 16:56:11',42),('vinod ','2012-09-09 17:16:39',43),('Pratima Rathore','2012-09-10 16:33:09',45),('Pratima Rathore','2012-09-10 16:40:37',45),('Milan Chauhan','2012-09-10 17:12:10',43),('Milan Chauhan','2012-09-11 01:25:27',43),('Milan Chauhan','2012-09-11 17:12:23',43),('Milan Chauhan','2012-09-12 15:15:45',43),('Raman Bhadouria','2012-09-12 16:20:32',42),('Raman Bhadouria','2012-09-12 16:40:41',42),('Milan Chauhan','2012-09-12 16:41:53',43),('Raman Bhadouria','2012-09-12 16:51:47',42),('Raman Bhadouria','2012-09-12 16:57:04',42),('Raman Bhadouria','2012-09-12 17:16:34',42),('Raman Bhadouria','2012-09-14 15:23:19',42),('Raman Bhadouria','2012-09-15 07:33:57',42),('Raman Bhadouria','2012-09-15 08:44:10',42),('Raman Bhadouria','2012-09-15 11:58:20',42),('Raman Bhadouria','2012-09-15 15:21:49',42),('Raman Bhadouria','2012-09-15 15:52:33',42),('Milan Chauhan','2012-09-15 16:42:13',43),('Milan Chauhan','2012-09-16 02:02:33',43),('Raman Bhadouria','2012-03-23 07:35:19',42),('Raman Bhadouria','2012-03-23 08:14:48',42),('Dushyant Bhadouria','2012-03-23 08:37:28',39),('Dushyant Bhadouria','2012-03-23 08:50:10',39),('Dushyant Bhadouria','2012-03-23 16:49:40',39),('Dushyant Bhadouria','2012-03-24 14:55:19',39),('Dushyant Bhadouria','2012-03-24 14:55:45',39),('Dushyant Bhadouria','2012-09-17 15:25:08',39),('Dushyant Bhadouria','2012-09-17 16:01:19',39),('Dushyant Bhadouria','2012-09-17 17:24:22',39),('Dushyant Bhadouria','2012-09-18 02:11:08',39),('Dushyant Bhadouria','2012-09-18 02:57:06',39),('Dushyant Bhadouria','2012-09-18 03:03:26',39),('Dushyant Bhadouria','2012-09-18 04:28:21',39),('Dushyant Bhadouria','2012-09-18 04:43:02',39),('Dushyant Bhadouria','2012-09-18 04:49:37',39),('Dushyant Bhadouria','2012-09-18 07:55:38',39),('Dushyant Bhadouria','2012-09-18 08:01:30',39),('Dushyant Bhadouria','2012-09-18 08:08:33',39),('Dushyant Bhadouria','2012-09-18 08:09:00',39),('Dushyant Bhadouria','2012-09-18 09:01:45',39),('Dushyant Bhadouria','2012-09-18 13:12:43',39),('Dushyant Bhadouria','2012-09-18 14:04:14',39),('Milan Chauhan','2012-09-18 15:03:32',43),('Milan Chauhan','2012-09-18 15:39:05',43),('Milan Chauhan','2012-09-18 15:52:37',43),('Milan Chauhan','2012-09-18 16:52:48',43),('Milan Chauhan','2012-09-18 18:14:05',43),('Milan Chauhan','2012-09-19 03:39:32',43),('Dushyant Bhadouria','2012-09-19 04:13:29',39),('Dushyant Bhadouria','2012-09-19 04:33:57',39),('Dushyant Bhadouria','2012-09-19 05:15:39',39),('Milan Chauhan','2012-09-19 05:17:42',43),('Dushyant Bhadouria','2012-09-19 05:31:13',39),('Milan Chauhan','2012-09-19 05:32:01',43),('Milan Chauhan','2012-09-19 08:18:51',43),('Milan Chauhan','2012-09-19 09:16:13',43),('Milan Chauhan','2012-09-19 10:01:22',43),('Milan Chauhan','2012-09-19 10:32:21',43),('Milan Chauhan','2012-09-19 11:04:30',43),('Milan Chauhan','2012-09-19 11:18:34',43),('Milan Chauhan','2012-09-19 14:48:48',43),('Milan Chauhan','2012-09-19 17:19:27',43),('Milan Chauhan','2012-09-20 14:50:23',43),('Milan Chauhan','2012-09-20 16:21:24',43),('Milan Chauhan','2012-09-20 16:30:16',43),('Milan Chauhan','2012-09-20 17:50:20',43),('Milan Chauhan','2012-09-20 18:36:02',43),('Milan Chauhan','2012-09-20 18:43:29',43),('Milan Chauhan','2012-09-21 14:47:29',43),('Milan Chauhan','2012-09-21 16:27:22',43),('Milan Chauhan','2012-09-22 05:43:33',43),('Milan Chauhan','2012-09-22 06:48:19',43),('Milan Chauhan','2012-09-22 07:27:09',43),('Milan Chauhan','2012-09-22 08:28:04',43),('Milan Chauhan','2012-09-22 09:31:41',43),('Milan Chauhan','2012-09-22 10:36:39',43),('Milan Chauhan','2012-09-22 14:25:32',43),('Milan Chauhan','2012-09-22 18:01:42',43),('Milan Chauhan','2012-09-23 02:24:53',43),('Milan Chauhan','2012-09-23 05:39:45',43),('Milan Chauhan','2012-09-23 10:46:54',43),('Milan Chauhan','2012-09-23 10:59:17',43),('Milan Chauhan','2012-09-23 14:12:38',43),('Milan Chauhan','2012-09-23 14:33:48',43),('Milan Chauhan','2012-09-23 15:03:53',43),('Milan Chauhan','2012-09-23 15:48:08',43),('Milan Chauhan','2012-09-23 15:59:04',43),('Milan Chauhan','2012-10-19 15:30:55',43),('Milan Chauhan','2012-10-20 00:38:13',43),('Milan Chauhan','2012-10-20 02:58:20',43),('Milan Chauhan','2012-10-20 18:36:08',43),('Milan Chauhan','2012-10-21 07:09:31',43),('Milan Chauhan','2012-10-21 08:11:16',43),('Milan Chauhan','2012-10-21 08:29:36',43),('Milan Chauhan','2012-10-21 11:24:11',43),('Milan Chauhan','2012-10-21 14:27:51',43),('Milan Chauhan','2012-10-21 15:44:01',43),('Milan Chauhan','2012-10-21 15:52:26',43),('Milan Chauhan','2012-10-21 17:00:08',43),('Milan Chauhan','2012-10-21 17:34:35',43),('Milan Chauhan','2012-10-22 15:34:22',43),('Milan Chauhan','2012-10-22 16:19:41',43),('Milan Chauhan','2012-10-23 14:50:17',43),('Milan Chauhan','2012-10-23 15:17:44',43),('Milan Chauhan','2012-10-23 16:25:24',43),('Milan Chauhan','2012-10-23 16:51:56',43),('Milan Chauhan','2012-10-23 16:59:56',43),('Milan Chauhan','2012-10-23 17:04:40',43),('Milan Chauhan','2012-10-23 17:13:36',43),('Milan Chauhan','2012-10-23 17:13:54',43),('Milan Chauhan','2012-10-23 17:19:55',43),('Milan Chauhan','2012-10-23 17:24:37',43),('Milan Chauhan','2012-10-24 03:04:07',43),('Milan Chauhan','2012-10-24 04:41:30',43),('Milan Chauhan','2012-10-24 08:22:49',43),('Milan Chauhan','2012-10-24 08:34:25',43),('Milan Chauhan','2012-10-24 09:09:50',43),('Milan Chauhan','2012-10-24 09:46:59',43),('Milan Chauhan','2012-10-24 09:58:53',43),('Milan Chauhan','2012-10-24 11:39:28',43),('Milan Chauhan','2012-10-24 16:58:56',43),('Milan Chauhan','2012-10-24 17:10:13',43),('Milan Chauhan','2012-10-24 17:19:02',43),('Milan Chauhan','2012-10-24 17:36:52',43),('Milan Chauhan','2012-10-24 18:06:17',43),('Milan Chauhan','2012-10-24 18:55:59',43),('Milan Chauhan','2012-10-25 16:55:07',43),('Milan Chauhan','2012-10-25 16:55:27',43),('Milan Chauhan','2012-10-25 17:21:11',43),('Milan Chauhan','2012-10-25 17:37:03',43),('Milan Chauhan','2012-10-25 17:41:24',43),('Milan Chauhan','2012-10-25 17:54:50',43),('Milan Chauhan','2012-10-26 04:11:50',43),('Milan Chauhan','2012-10-26 15:34:14',43),('Milan Chauhan','2012-10-26 15:53:44',43),('Milan Chauhan','2012-10-26 16:09:57',43),('Milan Chauhan','2012-10-27 07:44:27',43),('Milan Chauhan','2012-10-27 09:25:52',43),('Milan Chauhan','2012-10-27 10:33:27',43),('Milan Chauhan','2012-10-27 11:13:36',43),('Milan Chauhan','2012-10-27 13:51:03',43),('Milan Chauhan','2012-10-27 16:24:44',43),('Milan Chauhan','2012-10-27 16:30:32',43),('Milan Chauhan','2012-10-27 17:49:31',43),('Milan Chauhan','2012-10-27 18:22:11',43),('Milan Chauhan','2012-10-27 19:00:27',43),('Milan Chauhan','2012-10-28 02:21:43',43),('Milan Chauhan','2012-10-28 02:48:50',43),('Milan Chauhan','2012-10-28 03:04:24',43),('Milan Chauhan','2012-10-28 05:24:18',43),('Ram Bhadouria','2012-10-28 05:30:45',52),('Arvindra ','2012-10-28 05:32:32',53),('Milan Chauhan','2012-10-28 05:43:46',43),('Milan Chauhan','2012-10-28 05:51:50',43),('Milan Chauhan','2012-10-28 06:04:15',43),('Milan Chauhan','2012-10-28 06:24:08',43),('Milan Chauhan','2012-10-28 06:31:41',43),('Milan Chauhan','2012-10-28 06:54:58',43),('Anuj Bhadouria','2012-10-28 07:01:11',59),('Milan Chauhan','2012-10-28 07:04:13',43),('Milan Chauhan','2012-10-28 07:09:08',43),('Milan Chauhan','2012-10-28 07:45:39',43),('Milan Chauhan','2012-10-28 16:22:41',43),('Milan Chauhan','2012-10-28 17:13:14',43),('Milan Chauhan','2012-10-28 18:30:26',43),('Milan Chauhan','2012-10-28 19:11:53',43),('Milan Chauhan','2012-10-28 19:39:46',43),('Milan Chauhan','2012-10-28 19:59:31',43),('Vipul Bhadouria','2012-10-28 20:15:10',4),('Arvindra ','2012-10-28 20:15:54',4),('Milan Chauhan','2012-10-29 02:26:23',43),('Milan Chauhan','2012-10-29 14:35:13',43),('Milan Chauhan','2012-10-29 14:51:35',43),('Milan Chauhan','2012-10-29 15:01:01',43),('Milan Chauhan','2012-10-29 15:36:23',43),('Milan Chauhan','2012-10-30 15:45:25',43),('Milan Chauhan','2012-10-30 16:55:32',43),('Milan Chauhan','2012-10-30 17:18:14',43),('Milan Chauhan','2012-10-31 02:59:11',43),('Milan Chauhan','2012-10-31 03:24:35',43),('Milan Chauhan','2012-10-31 03:44:39',43),('Milan Chauhan','2012-10-31 03:49:07',43),('Milan Chauhan','2012-10-31 03:55:52',43),('Milan Chauhan','2012-10-31 10:38:45',43),('Milan Chauhan','2012-10-31 11:23:13',43),('Milan Chauhan','2012-10-31 13:36:52',43),('Milan Chauhan','2012-10-31 13:43:04',43),('Milan Chauhan','2012-10-31 15:10:48',43),('Milan Chauhan','2012-10-31 15:51:11',43),('Milan Chauhan','2012-10-31 16:13:28',43),('Milan Chauhan','2012-11-01 03:34:22',43),('Milan Chauhan','2012-11-01 16:20:55',43),('Milan Chauhan','2012-11-01 17:20:53',43),('Milan Chauhan','2012-11-01 17:23:44',43),('Milan Chauhan','2012-11-01 17:30:00',43),('Milan Chauhan','2012-11-01 17:32:44',43),('Milan Chauhan','2012-11-01 17:36:20',43),('Milan Chauhan','2012-11-01 17:37:41',43),('Milan Chauhan','2012-11-01 17:42:03',43),('Milan Chauhan','2012-11-01 17:43:07',43),('Milan Chauhan','2012-11-01 17:54:19',43),('Milan Chauhan','2012-11-01 17:56:58',43),('Milan Chauhan','2012-11-02 01:28:08',43),('Milan Chauhan','2012-11-02 01:30:05',43),('Milan Chauhan','2012-11-02 02:07:03',43),('Milan Chauhan','2012-11-02 02:13:33',43),('Milan Chauhan','2012-11-02 02:17:20',43),('Milan Chauhan','2012-11-02 02:17:38',43),('Milan Chauhan','2012-11-02 02:17:58',43),('Milan Chauhan','2012-11-02 02:17:59',43),('Milan Chauhan','2012-11-02 02:18:00',43),('Milan Chauhan','2012-11-02 02:18:01',43),('Milan Chauhan','2012-11-02 02:18:02',43),('Milan Chauhan','2012-11-02 02:26:25',43),('Milan Chauhan','2012-11-02 02:30:32',43),('Milan Chauhan','2012-11-02 02:31:41',43),('Milan Chauhan','2012-11-02 02:31:45',43),('Milan Chauhan','2012-11-03 00:35:59',43),('Milan Chauhan','2012-11-03 01:10:04',43),('Milan Chauhan','2012-11-03 01:10:36',43),('Milan Chauhan','2012-11-03 01:13:15',43),('Milan Chauhan','2012-11-03 01:13:53',43),('Milan Chauhan','2012-11-03 01:14:25',43),('Milan Chauhan','2012-11-03 02:03:25',43),('Milan Chauhan','2012-11-03 02:47:18',43),('Milan Chauhan','2012-11-03 03:01:06',43),('Milan Chauhan','2012-11-03 03:12:37',43),('Milan Chauhan','2012-11-03 06:34:59',43),('Milan Chauhan','2012-11-03 06:47:57',43),('Milan Chauhan','2012-11-03 06:57:00',43),('Milan Chauhan','2012-11-03 07:20:43',43),('Milan Chauhan','2012-11-03 07:33:39',43),('Milan Chauhan','2012-11-03 08:24:10',43),('Milan Chauhan','2012-11-03 08:36:16',43),('Milan Chauhan','2012-11-03 08:45:08',43),('Milan Chauhan','2012-11-03 10:20:45',43),('Manish Vishnoi','2012-11-03 10:39:21',48),('Milan Chauhan','2012-11-03 10:43:45',43),('Milan Chauhan','2012-11-03 10:47:38',43),('Manish Vishnoi','2012-11-03 10:49:08',48),('Milan Chauhan','2012-11-03 13:11:11',43),('Manish Vishnoi','2012-11-03 17:38:16',48),('Manish Vishnoi','2012-11-03 17:42:29',48),('Manish Vishnoi','2012-11-03 17:55:04',48),('Manish Vishnoi','2012-11-03 18:10:01',48),('Manish Vishnoi','2012-11-03 18:15:30',48),('Manish Vishnoi','2012-11-03 18:28:00',48),('Milan Chauhan','2012-11-03 18:36:36',43),('Milan Chauhan','2012-11-04 18:28:39',43),('Milan Chauhan','2012-11-04 18:37:44',43),('Milan Chauhan','2012-11-04 18:41:34',43),('Milan Chauhan','2012-11-04 19:00:25',43),('Milan Chauhan','2012-11-05 01:16:08',43),('Milan Chauhan','2012-11-05 01:51:47',43),('Milan Chauhan','2012-11-05 02:16:05',43),('Milan Chauhan','2012-11-05 13:17:11',43),('Manish Vishnoi','2012-11-05 13:37:13',48),('Manish Vishnoi','2012-11-05 14:35:51',48),('Manish Vishnoi','2012-11-05 15:03:00',48),('Manish Vishnoi','2012-11-05 15:06:58',48),('Manish Vishnoi','2012-11-05 15:43:50',48),('Manish Vishnoi','2012-11-05 15:52:27',48),('Manish Vishnoi','2012-11-05 15:59:47',48),('Manish Vishnoi','2012-11-05 16:01:26',48),('Milan Chauhan','2012-11-10 02:01:15',43),('Manish Vishnoi','2012-11-10 02:04:49',48),('Manish Vishnoi','2012-11-10 02:28:12',48),('Manish Vishnoi','2012-11-10 03:00:57',48),('Manish Vishnoi','2012-11-10 03:14:15',48),('Manish Vishnoi','2012-11-10 03:18:24',48),('Manish Vishnoi','2012-11-10 03:21:32',48),('Manish Vishnoi','2012-11-10 03:36:01',48),('Manish Vishnoi','2012-11-10 03:53:37',48),('Manish Vishnoi','2012-11-10 04:03:08',48),('Milan Chauhan','2012-11-10 04:03:57',43),('Milan Chauhan','2012-11-10 05:29:16',43),('Manish Vishnoi','2012-11-10 07:23:40',48),('Manish Vishnoi','2012-11-10 09:27:22',48),('Manish Vishnoi','2012-11-10 11:59:16',48),('Milan Chauhan','2012-11-10 12:08:14',43),('Manish Vishnoi','2012-11-10 15:54:19',48),('Milan Chauhan','2012-11-10 16:22:12',43),('Manish Vishnoi','2012-11-10 16:40:59',48),('Milan Chauhan','2012-11-10 16:43:06',43),('Milan Chauhan','2012-11-10 16:47:00',43),('Anoop Chaturvidi','2012-11-10 17:00:46',1),('Anoop Chaturvidi','2012-11-10 17:04:40',1),('Anoop Chaturvidi','2012-11-10 17:13:00',1),('Milan Chauhan','2012-11-11 06:46:31',43),('Milan Chauhan','2012-11-11 07:53:20',43),('Milan Chauhan','2012-11-11 10:49:38',43),('Milan Chauhan','2012-11-11 11:08:51',43),('Milan Chauhan','2012-11-11 11:14:19',43),('Milan Chauhan','2012-11-11 12:51:12',43),('Milan Chauhan','2012-11-11 23:59:54',43),('Milan Chauhan','2012-11-12 00:11:25',43),('Milan Chauhan','2012-11-13 00:14:24',43),('Milan Chauhan','2012-11-17 03:39:07',43),('Milan Chauhan','2012-11-17 03:43:33',43),('Milan Chauhan','2012-11-17 03:56:56',43),('Milan Chauhan','2012-11-17 03:59:43',43),('Milan Chauhan','2012-11-17 07:38:06',43),('Milan Chauhan','2012-11-19 14:21:22',43),('Milan Chauhan','2012-11-23 16:19:41',43),('Milan Chauhan','2012-11-24 04:12:38',43),('Milan Chauhan','2012-11-24 15:15:43',43),('Milan Chauhan','2012-11-30 16:08:56',43),('Milan Chauhan','2012-11-30 16:15:39',43),('Milan Chauhan','2012-12-01 00:51:26',43),('Milan Chauhan','2012-12-02 15:13:43',43),('Milan Chauhan','2012-12-08 02:42:44',43),('Milan Chauhan','2012-12-08 02:52:29',43),('Milan Chauhan','2012-12-16 06:29:48',43),('Milan Chauhan','2012-12-31 17:59:14',43),('Milan Chauhan','2012-12-31 18:34:01',43),('Milan Chauhan','2013-01-01 16:52:22',43),('Milan Chauhan','2013-01-01 17:51:43',43),('Milan Chauhan','2013-01-02 15:54:51',43),('Milan Chauhan','2013-01-03 15:58:31',43),('Milan Chauhan','2013-01-04 16:40:40',43),('Milan Chauhan','2013-01-04 17:56:57',43),('Milan Chauhan','2013-01-06 06:41:16',43),('Milan Chauhan','2013-01-08 15:35:19',43),('Milan Chauhan','2013-01-11 16:14:24',43),('Milan Chauhan','2013-01-12 17:31:51',43),('Milan Chauhan','2013-01-13 02:13:21',43),('Milan Chauhan','2013-01-15 16:10:42',43),('Milan Chauhan','2013-01-15 16:38:29',43),('Milan Chauhan','2013-01-15 16:40:02',43),('Milan Chauhan','2013-01-15 16:43:49',43),('Milan Chauhan','2013-01-15 16:44:37',43),('Milan Chauhan','2013-01-15 17:40:31',43),('Milan Chauhan','2013-01-16 16:39:12',43),('Milan Chauhan','2013-01-16 17:41:29',43),('Milan Chauhan','2013-01-17 13:54:52',43),('Milan Chauhan','2013-01-17 14:49:09',43),('Milan Chauhan','2013-01-20 13:00:41',43),('Milan Chauhan','2013-01-31 15:57:44',43),('Milan Chauhan','2013-02-02 06:07:06',43),('Milan Chauhan','2013-02-02 06:48:18',43),('Milan Chauhan','2013-02-02 06:52:33',43),('Milan Chauhan','2013-02-02 07:08:40',43),('Milan Chauhan','2013-02-02 15:59:41',43),('Milan Chauhan','2013-02-02 16:54:52',43),('Milan Chauhan','2013-02-02 17:47:03',43),('Milan Chauhan','2013-02-25 16:54:12',43),('Milan Chauhan','2013-02-25 16:56:35',43),('Milan Chauhan','2013-02-25 17:44:24',43),('Milan Chauhan','2013-02-25 17:46:18',43),('Milan Chauhan','2013-03-02 16:29:52',43),('Milan Chauhan','2013-03-06 17:12:24',43),('Milan Chauhan','2013-03-07 15:48:56',43),('Milan Chauhan','2013-03-07 17:26:11',43),('Milan Chauhan','2013-03-09 02:44:46',43),('Milan Chauhan','2013-03-09 03:50:34',43),('Milan Chauhan','2013-03-09 05:15:31',43),('Milan Chauhan','2013-03-09 05:53:51',43),('Milan Chauhan','2013-03-09 07:11:47',43),('Milan Chauhan','2013-03-09 07:35:11',43),('Milan Chauhan','2013-03-09 07:38:31',43),('Milan Chauhan','2013-03-09 12:13:48',43),('Milan Chauhan','2013-03-09 12:21:15',43),('Milan Chauhan','2013-03-09 12:51:03',43),('Milan Chauhan','2013-03-09 12:57:00',43),('Milan Chauhan','2013-03-09 12:59:02',43),('Milan Chauhan','2013-03-09 13:07:21',43),('Milan Chauhan','2013-03-09 14:02:01',43),('Milan Chauhan','2013-03-09 14:07:33',43),('Milan Chauhan','2013-03-09 15:27:42',43),('Milan Chauhan','2013-03-09 16:24:30',43),('Milan Chauhan','2013-03-09 18:10:57',43),('Milan Chauhan','2013-03-09 18:54:44',43),('Milan Chauhan','2013-03-09 19:35:09',43),('Milan Chauhan','2013-03-09 19:38:26',43),('Milan Chauhan','2013-03-10 02:19:13',43),('Milan Chauhan','2013-03-10 03:06:51',43),('Milan Chauhan','2013-03-10 04:47:55',43),('Milan Chauhan','2013-03-10 04:52:27',43),('Milan Chauhan','2013-03-10 08:27:44',43),('Milan Chauhan','2013-03-10 08:34:55',43),('Milan Chauhan','2013-03-10 09:52:40',43),('Milan Chauhan','2013-03-11 03:40:00',43),('Milan Chauhan','2013-03-11 03:52:38',43),('Milan Chauhan','2013-03-11 04:13:33',43),('Milan Chauhan','2013-03-11 04:17:43',43),('Milan Chauhan','2013-03-11 04:20:20',43),('Milan Chauhan','2013-03-11 16:19:09',43),('Milan Chauhan','2013-03-12 01:52:17',43),('Milan Chauhan','2013-03-12 16:33:03',43),('Milan Chauhan','2013-03-13 16:22:21',43),('Milan Chauhan','2013-03-13 17:40:15',43),('Milan Chauhan','2013-03-13 17:53:31',43),('Milan Chauhan','2013-03-13 17:56:26',43),('Milan Chauhan','2013-03-14 17:52:54',43),('Milan Chauhan','2013-03-15 01:58:59',43),('Milan Chauhan','2013-03-15 02:08:03',43),('Milan Chauhan','2013-03-15 16:11:41',43),('Milan Chauhan','2013-03-15 17:23:02',43),('Milan Chauhan','2013-03-15 17:45:00',43),('Milan Chauhan','2013-03-15 17:57:13',43),('Milan Chauhan','2013-03-15 18:05:06',43),('Milan Chauhan','2013-03-15 18:23:03',43),('Milan Chauhan','2013-03-15 18:27:35',43),('Milan Chauhan','2013-03-16 02:21:34',43),('Milan Chauhan','2013-03-16 03:35:53',43),('Milan Chauhan','2013-03-16 03:48:54',43),('Milan Chauhan','2013-03-16 03:52:20',43),('Milan Chauhan','2013-03-16 05:09:41',43),('Milan Chauhan','2013-03-16 05:58:30',43),('Milan Chauhan','2013-03-16 06:04:45',43),('Milan Chauhan','2013-03-16 06:11:01',43),('Milan Chauhan','2013-03-16 07:30:45',43),('Milan Chauhan','2013-03-16 07:41:33',43),('Milan Chauhan','2013-03-16 07:56:20',43),('Milan Chauhan','2013-03-16 08:16:41',43),('Milan Chauhan','2013-03-16 08:21:59',43),('Milan Chauhan','2013-03-16 12:06:13',43),('Milan Chauhan','2013-03-16 15:28:19',43),('Milan Chauhan','2013-03-16 16:01:40',43),('Milan Chauhan','2013-03-16 16:11:52',43),('Milan Chauhan','2013-03-16 16:42:08',43),('Milan Chauhan','2013-03-16 17:58:22',43),('Milan Chauhan','2013-03-16 18:06:05',43),('Milan Chauhan','2013-03-16 18:09:31',43),('Milan Chauhan','2013-03-16 18:13:51',43),('Milan Chauhan','2013-03-16 18:26:11',43),('Milan Chauhan','2013-03-16 18:55:52',43),('Milan Chauhan','2013-03-16 18:59:37',43),('Milan Chauhan','2013-03-16 19:13:07',43),('Milan Chauhan','2013-03-16 19:20:13',43),('Milan Chauhan','2013-03-16 19:34:38',43),('Milan Chauhan','2013-03-16 19:38:17',43),('Milan Chauhan','2013-03-17 02:50:36',43),('Milan Chauhan','2013-03-17 04:22:43',43),('Milan Chauhan','2013-03-17 04:32:18',43),('Milan Chauhan','2013-03-17 04:45:49',43),('Milan Chauhan','2013-03-17 05:23:20',43),('Milan Chauhan','2013-03-17 05:25:31',43),('Milan Chauhan','2013-03-17 05:29:32',43),('Milan Chauhan','2013-03-17 05:39:43',43),('Milan Chauhan','2013-03-17 05:53:20',43),('Milan Chauhan','2013-03-17 05:59:55',43),('Milan Chauhan','2013-03-17 06:04:46',43),('Milan Chauhan','2013-03-17 06:15:11',43),('Milan Chauhan','2013-03-17 08:25:32',43),('Milan Chauhan','2013-03-17 08:43:37',43),('Milan Chauhan','2013-03-17 09:28:20',43),('Milan Chauhan','2013-03-17 09:56:18',43),('Milan Chauhan','2013-03-17 10:19:02',43),('Milan Chauhan','2013-03-17 10:26:28',43),('Milan Chauhan','2013-03-17 15:33:57',43),('Milan Chauhan','2013-03-17 15:52:43',43),('Milan Chauhan','2013-03-17 17:02:45',43),('Milan Chauhan','2013-03-17 17:57:46',43),('Milan Chauhan','2013-03-17 18:24:41',43),('Milan Chauhan','2013-03-18 15:20:57',43),('Milan Chauhan','2013-03-22 18:17:22',43),('Milan Chauhan','2013-03-26 03:50:53',43),('Milan Chauhan','2013-03-26 04:22:05',43),('Milan Chauhan','2013-03-26 04:52:47',43),('Milan Chauhan','2013-03-26 04:56:51',43),('Milan Chauhan','2013-03-29 11:52:57',43),('Milan Chauhan','2013-03-30 03:39:57',43),('Milan Chauhan','2013-03-31 09:15:24',43),('Milan Chauhan','2013-04-06 05:43:39',43),('Milan Chauhan','2013-04-06 06:16:20',43),('Milan Chauhan','2013-04-06 06:57:47',43),('Milan Chauhan','2013-04-06 08:02:21',43),('Milan Chauhan','2013-04-06 08:03:43',43),('Milan Chauhan','2013-04-07 03:19:20',43),('Milan Chauhan','2013-04-07 04:43:34',43),('Milan Chauhan','2013-09-07 13:20:10',43),('Milan Chauhan','2013-09-07 13:43:24',43),('Milan Chauhan','2013-09-07 14:23:01',43),('Milan Chauhan','2013-09-07 15:58:15',43),('Milan Chauhan','2013-09-07 16:34:30',43),('Milan Chauhan','2013-09-08 13:30:31',43),('Milan Chauhan','2013-09-08 14:36:49',43),('Milan Chauhan','2013-09-08 15:01:20',43),('Milan Chauhan','2013-09-08 15:28:56',43),('Milan Chauhan','2013-09-09 01:14:33',43),('Milan Chauhan','2013-09-09 01:54:02',43),('Milan Chauhan','2013-09-09 04:04:29',43),('Milan Chauhan','2013-09-09 04:26:41',43),('Milan Chauhan','2013-09-09 04:45:27',43),('Milan Chauhan','2013-09-09 10:04:42',43),('Milan Chauhan','2013-09-09 11:44:19',43),('Milan Chauhan','2013-09-09 12:41:18',43),('Milan Chauhan','2013-09-09 12:49:19',43),('Milan Chauhan','2013-09-09 12:52:26',43),('Milan Chauhan','2013-09-09 13:28:06',43),('Milan Chauhan','2013-09-09 14:42:41',43),('Milan Chauhan','2013-09-09 16:47:03',43),('Milan Chauhan','2013-09-09 16:59:51',43),('Milan Chauhan','2013-09-09 17:06:43',43),('Milan Chauhan','2013-09-09 17:13:44',43),('Milan Chauhan','2013-09-09 17:18:11',43),('Milan Chauhan','2013-09-10 13:46:06',43),('Milan Chauhan','2013-09-10 15:08:37',43),('Milan Chauhan','2013-09-10 15:30:35',43),('Milan Chauhan','2013-09-10 15:45:27',43),('Milan Chauhan','2013-09-11 01:35:19',43),('Milan Chauhan','2013-09-11 02:55:01',43),('Milan Chauhan','2013-09-11 16:03:03',43),('Milan Chauhan','2013-09-12 14:46:39',43),('Milan Chauhan','2013-09-12 15:32:58',43),('Milan Chauhan','2013-09-12 16:50:13',43),('Milan Chauhan','2013-09-12 17:24:18',43),('Milan Chauhan','2013-09-13 01:41:17',43),('Milan Chauhan','2013-09-13 02:17:27',43),('Milan Chauhan','2013-09-13 02:53:42',43),('Milan Chauhan','2013-09-13 16:11:45',43),('Milan Chauhan','2013-09-13 16:24:57',43),('Milan Chauhan','2013-09-13 16:55:47',43),('Milan Chauhan','2013-09-14 01:24:46',43),('Milan Chauhan','2013-09-14 07:10:25',43),('Milan Chauhan','2013-09-14 07:55:42',43),('Milan Chauhan','2013-09-14 08:37:48',43),('Milan Chauhan','2013-09-14 10:54:29',43),('Milan Chauhan','2013-09-14 11:33:04',43),('Milan Chauhan','2013-09-14 12:06:49',43),('Milan Chauhan','2013-09-14 12:33:40',43),('Milan Chauhan','2013-09-14 12:39:50',43),('Milan Chauhan','2013-09-14 13:18:09',43),('Milan Chauhan','2013-09-14 13:20:35',43),('Milan Chauhan','2013-09-14 13:21:58',43),('Milan Chauhan','2013-09-14 14:25:11',43),('Milan Chauhan','2013-09-14 16:31:52',43),('Milan Chauhan','2013-09-14 17:33:23',43),('Milan Chauhan','2013-09-14 17:46:21',43),('Milan Chauhan','2013-09-15 01:59:45',43),('Milan Chauhan','2013-09-15 02:31:05',43),('Milan Chauhan','2013-09-15 02:51:06',43),('Milan Chauhan','2013-09-15 02:56:50',43),('Milan Chauhan','2013-09-15 03:08:29',43),('Milan Chauhan','2013-09-15 03:09:58',43),('Milan Chauhan','2013-09-15 03:27:59',43),('Milan Chauhan','2013-09-15 03:34:46',43),('Milan Chauhan','2013-09-15 03:38:44',43),('Milan Chauhan','2013-09-15 03:50:47',43),('Milan Chauhan','2013-09-15 04:04:03',43),('Milan Chauhan','2013-09-15 04:16:08',43),('Milan Chauhan','2013-09-15 05:24:15',43),('Milan Chauhan','2013-09-15 05:29:29',43),('Milan Chauhan','2013-09-15 05:35:52',43),('Milan Chauhan','2013-09-15 06:04:21',43),('Milan Chauhan','2013-09-15 12:21:55',43),('Milan Chauhan','2013-09-15 13:47:51',43),('Milan Chauhan','2013-09-15 15:19:55',43),('Milan Chauhan','2013-09-15 16:03:12',43),('Milan Chauhan','2013-09-15 16:56:30',43),('Milan Chauhan','2013-09-15 17:01:43',43),('Milan Chauhan','2013-09-15 17:02:57',43),('Milan Chauhan','2013-09-15 17:26:10',43),('Milan Chauhan','2013-09-15 18:26:43',43),('Milan Chauhan','2013-09-16 01:01:29',43),('Milan Chauhan','2013-09-16 02:47:10',43),('Milan Chauhan','2013-09-16 13:40:34',43),('Milan Chauhan','2013-09-16 14:27:47',43),('Milan Chauhan','2013-09-16 14:42:59',43),('Milan Chauhan','2013-09-16 15:52:22',43),('Milan Chauhan','2013-09-16 16:43:12',43),('Milan Chauhan','2013-09-17 02:05:51',43),('Milan Chauhan','2013-09-17 02:08:28',43),('Milan Chauhan','2013-09-17 02:12:44',43),('Milan Chauhan','2013-09-17 03:13:59',43),('Milan Chauhan','2013-09-17 15:06:29',43),('Milan Chauhan','2013-09-17 15:49:52',43),('Milan Chauhan','2013-09-17 16:48:11',43),('Milan Chauhan','2013-09-17 16:56:39',43),('Milan Chauhan','2013-09-17 17:20:32',43),('Milan Chauhan','2013-09-18 02:53:56',43),('Milan Chauhan','2013-09-18 13:33:36',43),('Milan Chauhan','2013-09-18 13:40:14',43),('Milan Chauhan','2013-09-18 13:53:54',43),('Milan Chauhan','2013-09-18 14:00:11',43),('Milan Chauhan','2013-09-18 14:23:34',43),('Milan Chauhan','2013-09-18 14:29:14',43),('Milan Chauhan','2013-09-18 16:19:43',43),('Milan Chauhan','2013-09-18 16:39:04',43),('Milan Chauhan','2013-09-18 17:03:39',43),('Milan Chauhan','2013-09-19 02:26:21',43),('Milan Chauhan','2013-09-19 02:38:33',43),('Milan Chauhan','2013-09-19 14:02:07',43),('Milan Chauhan','2013-09-19 16:11:15',43),('Milan Chauhan','2013-09-19 16:14:34',43),('Milan Chauhan','2013-09-20 01:52:39',43),('Milan Chauhan','2013-09-20 02:07:55',43),('Milan Chauhan','2013-09-20 02:28:28',43),('Milan Chauhan','2013-09-20 02:35:42',43),('Milan Chauhan','2013-09-20 02:38:48',43),('Milan Chauhan','2013-09-20 16:13:10',43),('Milan Chauhan','2013-09-21 17:58:48',43),('Milan Chauhan','2013-09-21 18:04:20',43),('Milan Chauhan','2013-09-22 10:43:25',43),('Milan Chauhan','2013-09-22 11:59:59',43),('Milan Chauhan','2013-09-22 17:07:45',43),('Milan Chauhan','2013-09-23 01:42:28',43),('Milan Chauhan','2013-09-23 01:57:39',43),('Milan Chauhan','2013-09-23 02:28:45',43),('Milan Chauhan','2013-09-23 02:35:04',43),('Milan Chauhan','2013-09-23 02:44:58',43),('Milan Chauhan','2013-09-23 03:02:44',43),('Milan Chauhan','2013-09-23 17:02:04',43),('Milan Chauhan','2013-09-24 02:28:01',43),('Milan Chauhan','2013-09-24 02:56:48',43),('Milan Chauhan','2013-09-24 03:11:55',43),('Milan Chauhan','2013-09-24 03:35:53',43),('Milan Chauhan','2013-09-24 15:40:06',43),('Milan Chauhan','2013-09-24 17:39:41',43),('Milan Chauhan','2013-09-24 17:40:31',43),('Milan Chauhan','2013-09-24 17:43:34',43),('Milan Chauhan','2013-09-24 17:43:41',43),('Milan Chauhan','2013-09-25 02:31:09',43),('Milan Chauhan','2013-09-25 02:49:05',43),('Milan Chauhan','2013-09-25 14:59:22',43),('Milan Chauhan','2013-09-25 16:42:58',43),('Milan Chauhan','2013-09-25 16:45:04',43),('Milan Chauhan','2013-09-26 13:50:08',43),('Milan Chauhan','2013-09-26 14:23:43',43),('Milan Chauhan','2013-09-26 16:23:03',43),('Milan Chauhan','2013-09-27 14:33:30',43),('Milan Chauhan','2013-09-27 16:37:27',43),('Milan Chauhan','2013-09-27 17:04:36',43),('Milan Chauhan','2013-09-27 18:08:33',43),('Milan Chauhan','2013-09-28 03:02:21',43),('Milan Chauhan','2013-09-28 03:19:11',43),('Milan Chauhan','2013-09-28 03:57:32',43),('Milan Chauhan','2013-09-28 04:00:49',43),('Milan Chauhan','2013-09-28 06:31:41',43),('Milan Chauhan','2013-09-28 07:35:40',43),('Milan Chauhan','2013-09-28 11:08:00',43),('Milan Chauhan','2013-09-28 11:13:20',43),('Milan Chauhan','2013-09-28 11:27:22',43),('Milan Chauhan','2013-09-28 11:29:04',43),('Milan Chauhan','2013-09-28 12:03:36',43),('Milan Chauhan','2013-09-28 16:48:40',43),('Milan Chauhan','2013-09-28 17:03:04',43),('Milan Chauhan','2013-09-28 17:21:26',43),('Milan Chauhan','2013-09-28 17:38:07',43),('Milan Chauhan','2013-09-28 17:41:57',43),('Milan Chauhan','2013-09-28 17:53:47',43),('Milan Chauhan','2013-09-28 17:56:36',43),('Milan Chauhan','2013-09-28 18:09:20',43),('Milan Chauhan','2013-09-29 01:26:18',43),('Milan Chauhan','2013-09-29 01:39:56',43),('Milan Chauhan','2013-09-29 02:13:07',43),('Milan Chauhan','2013-09-29 02:23:23',43),('Milan Chauhan','2013-09-29 02:27:02',43),('Milan Chauhan','2013-09-29 02:47:11',43),('Milan Chauhan','2013-09-29 03:28:17',43),('Milan Chauhan','2013-09-29 05:14:19',43),('Milan Chauhan','2013-09-29 08:49:48',43),('Milan Chauhan','2013-09-29 15:02:41',43),('Milan Chauhan','2013-09-29 16:16:42',43),('Milan Chauhan','2013-09-29 16:55:18',43),('Milan Chauhan','2013-09-30 14:21:22',43),('Anoop Chaturvidi','2013-09-30 14:26:22',1),('Milan Chauhan','2013-09-30 14:54:03',43),('Milan Chauhan','2013-09-30 15:25:03',43),('Milan Chauhan','2013-09-30 17:26:22',43),('Milan Chauhan','2013-10-01 17:27:00',43),('Milan Chauhan','2013-10-02 03:09:16',43),('Milan Chauhan','2013-10-02 04:19:56',43),('Milan Chauhan','2013-10-02 04:26:23',43),('Milan Chauhan','2013-10-02 04:53:37',43),('Milan Chauhan','2013-10-02 05:32:07',43),('Milan Chauhan','2013-10-02 06:45:32',43),('Milan Chauhan','2013-10-02 06:52:10',43),('Milan Chauhan','2013-10-02 09:49:43',43),('Milan Chauhan','2013-10-02 10:13:28',43),('Milan Chauhan','2013-10-02 10:42:09',43),('Milan Chauhan','2013-10-02 10:54:35',43),('Milan Chauhan','2013-10-02 11:02:20',43),('Milan Chauhan','2013-10-02 11:06:45',43),('Milan Chauhan','2013-10-02 11:10:38',43),('Milan Chauhan','2013-10-02 11:22:45',43),('Milan Chauhan','2013-10-02 12:00:40',43),('Milan Chauhan','2013-10-02 14:12:32',43),('Milan Chauhan','2013-10-02 16:27:41',43),('Milan Chauhan','2013-10-02 17:29:02',43),('Milan Chauhan','2013-10-02 18:05:16',43),('Milan Chauhan','2013-10-02 18:21:12',43),('Milan Chauhan','2013-10-02 18:44:01',43),('Milan Chauhan','2013-10-03 17:40:59',43),('Milan Chauhan','2013-10-03 17:48:51',43),('Milan Chauhan','2013-10-03 17:58:20',43),('Milan Chauhan','2013-10-03 18:24:03',43),('Milan Chauhan','2013-10-04 14:13:26',43),('Milan Chauhan','2013-10-04 17:15:03',43),('Milan Chauhan','2013-10-05 03:00:57',43),('Milan Chauhan','2013-10-06 02:55:49',43),('Milan Chauhan','2013-10-06 03:14:40',43),('Milan Chauhan','2013-10-06 03:24:19',43),('Milan Chauhan','2013-10-06 03:41:56',43),('Milan Chauhan','2013-10-06 03:49:25',43),('Milan Chauhan','2013-10-06 04:57:06',43),('Milan Chauhan','2013-10-06 05:27:00',43),('Sudeep Mishra','2013-10-06 05:41:50',6),('Milan Chauhan','2013-10-09 15:17:22',43),('Milan Chauhan','2013-10-09 15:27:22',43),('Milan Chauhan','2013-10-09 15:28:56',43),('Milan Chauhan','2013-10-09 16:09:11',43),('Milan Chauhan','2013-10-09 17:01:33',43),('Milan Chauhan','2013-10-09 17:27:53',43),('Milan Chauhan','2013-10-09 17:41:42',43),('Milan Chauhan','2013-10-09 17:43:37',43),('Milan Chauhan','2013-10-09 17:45:39',43),('Milan Chauhan','2013-10-09 17:57:53',43),('Milan Chauhan','2013-10-09 18:01:35',43),('Milan Chauhan','2013-10-09 18:11:10',43),('Milan Chauhan','2013-10-09 18:17:42',43),('Milan Chauhan','2013-10-09 18:46:29',43),('Milan Chauhan','2013-10-10 03:20:25',43),('Milan Chauhan','2013-10-10 14:10:57',43),('Milan Chauhan','2013-10-10 14:16:53',43),('Milan Chauhan','2013-10-10 14:25:55',43),('Milan Chauhan','2013-10-10 17:17:21',43),('Milan Chauhan','2013-10-10 18:28:45',43),('Milan Chauhan','2013-10-10 18:36:15',43),('Milan Chauhan','2013-10-10 18:51:29',43),('Milan Chauhan','2013-10-11 03:01:59',43),('Milan Chauhan','2013-10-11 03:05:12',43),('Milan Chauhan','2013-10-11 14:16:35',43),('Milan Chauhan','2013-10-11 15:10:39',43),('Milan Chauhan','2013-10-11 15:21:19',43),('Milan Chauhan','2013-10-11 16:25:05',43),('Milan Chauhan','2013-10-11 16:44:15',43),('Milan Chauhan','2013-10-12 02:46:20',43),('Milan Chauhan','2013-10-12 02:55:56',43),('Milan Chauhan','2013-10-12 02:57:34',43),('Milan Chauhan','2013-10-12 03:03:06',43),('Milan Chauhan','2013-10-12 03:59:31',43),('Milan Chauhan','2013-10-12 05:03:23',43),('Milan Chauhan','2013-10-12 08:01:43',43),('Milan Chauhan','2013-10-12 09:42:09',43),('Milan Chauhan','2013-10-12 10:44:40',43),('Milan Chauhan','2013-10-12 11:06:28',43),('Milan Chauhan','2013-10-12 11:12:43',43),('Milan Chauhan','2013-10-12 12:43:28',43),('Milan Chauhan','2013-10-12 12:45:29',43),('Milan Chauhan','2013-10-12 12:47:25',43),('Milan Chauhan','2013-10-12 12:49:35',43),('Milan Chauhan','2013-10-12 12:52:04',43),('Milan Chauhan','2013-10-12 14:38:38',43),('Milan Chauhan','2013-10-12 16:24:39',43),('Milan Chauhan','2013-10-12 16:32:04',43),('Milan Chauhan','2013-10-12 16:34:57',43),('Milan Chauhan','2013-10-12 17:47:27',43),('Milan Chauhan','2013-10-12 18:20:35',43),('Milan Chauhan','2013-10-12 18:47:41',43),('Milan Chauhan','2013-10-12 19:02:30',43),('Milan Chauhan','2013-10-13 07:03:09',43),('Milan Chauhan','2013-10-13 09:13:25',43),('Milan Chauhan','2013-10-13 09:15:00',43),('Milan Chauhan','2013-10-13 09:28:04',43),('Milan Chauhan','2013-10-13 10:27:24',43),('Milan Chauhan','2013-10-13 13:36:36',43),('Milan Chauhan','2013-10-13 17:31:10',43),('Milan Chauhan','2013-10-13 18:16:46',43),('Milan Chauhan','2013-10-13 18:41:16',43),('Milan Chauhan','2013-10-13 18:56:34',43),('Milan Chauhan','2013-10-14 02:23:51',43),('Milan Chauhan','2013-10-14 02:43:34',43),('Milan Chauhan','2013-10-14 04:03:59',43),('Milan Chauhan','2013-10-14 04:05:34',43),('Milan Chauhan','2013-10-14 04:07:01',43),('Milan Chauhan','2013-10-14 04:15:36',43),('Milan Chauhan','2013-10-14 04:40:25',43),('Milan Chauhan','2013-10-14 05:14:33',43),('Milan Chauhan','2013-10-14 05:40:29',43),('Milan Chauhan','2013-10-14 09:14:22',43),('Milan Chauhan','2013-10-14 09:36:39',43),('Milan Chauhan','2013-10-14 10:42:49',43),('Milan Chauhan','2013-10-14 10:51:49',43),('Milan Chauhan','2013-10-14 15:35:05',43),('Milan Chauhan','2013-10-14 16:38:20',43),('Milan Chauhan','2013-10-14 16:44:03',43),('Milan Chauhan','2013-10-14 18:03:18',43),('Milan Chauhan','2013-10-14 18:34:13',43),('Milan Chauhan','2013-10-14 18:43:32',43),('Milan Chauhan','2013-10-14 18:53:13',43),('Milan Chauhan','2013-10-14 19:02:56',43),('Milan Chauhan','2013-10-15 02:46:52',43),('Milan Chauhan','2013-10-15 15:39:53',43),('Milan Chauhan','2013-10-16 13:48:44',43),('Milan Chauhan','2013-10-16 15:05:34',43),('Milan Chauhan','2013-10-16 15:38:07',43),('Milan Chauhan','2013-10-16 16:03:15',43),('Milan Chauhan','2013-10-16 16:11:05',43),('Milan Chauhan','2013-10-16 16:19:32',43),('Milan Chauhan','2013-10-16 16:23:54',43),('Milan Chauhan','2013-10-16 16:48:55',43),('Milan Chauhan','2013-10-16 16:51:50',43),('Milan Chauhan','2013-10-17 02:32:35',43),('Milan Chauhan','2013-10-17 18:01:03',43),('Milan Chauhan','2013-10-17 18:14:19',43),('Milan Chauhan','2013-10-18 02:15:00',43),('Milan Chauhan','2013-10-18 14:47:36',43),('Milan Chauhan','2013-10-19 03:10:23',43),('Milan Chauhan','2013-10-19 03:23:02',43),('Milan Chauhan','2013-10-19 03:43:49',43),('Milan Chauhan','2013-10-19 05:53:41',43),('Milan Chauhan','2013-10-19 09:50:37',43),('Milan Chauhan','2013-10-19 11:50:36',43),('Milan Chauhan','2013-10-19 12:17:38',43),('Milan Chauhan','2013-10-19 14:03:39',43),('Milan Chauhan','2013-10-19 14:10:13',43),('Milan Chauhan','2013-10-19 14:34:13',43),('Milan Chauhan','2013-10-19 14:41:20',43),('Milan Chauhan','2013-10-19 16:12:36',43),('Milan Chauhan','2013-10-19 16:15:17',43),('Milan Chauhan','2013-10-19 16:25:06',43),('Milan Chauhan','2013-10-20 01:17:21',43),('Milan Chauhan','2013-10-20 02:47:25',43),('Milan Chauhan','2013-10-20 06:27:19',43),('Milan Chauhan','2013-10-20 06:59:08',43),('Milan Chauhan','2013-10-20 11:20:32',43),('Milan Chauhan','2013-10-20 15:04:12',43),('Milan Chauhan','2013-10-21 16:11:51',43),('Milan Chauhan','2013-10-21 16:53:30',43),('Milan Chauhan','2013-10-21 17:00:07',43),('Milan Chauhan','2013-10-21 17:00:41',43),('Milan Chauhan','2013-10-21 17:13:50',43),('Milan Chauhan','2013-10-22 02:04:45',43),('Milan Chauhan','2013-10-23 02:25:10',43),('Milan Chauhan','2013-10-25 16:08:29',43),('Milan Chauhan','2013-12-19 16:25:24',43),('Milan Chauhan','2013-12-19 16:29:46',43),('Milan Chauhan','2013-12-21 06:36:09',43),('Milan Chauhan','2013-12-25 04:59:44',43),('Milan Chauhan','2013-12-25 06:23:00',43),('Milan Chauhan','2013-12-25 06:37:30',43),('Milan Chauhan','2013-12-25 06:45:57',43),('Milan Chauhan','2013-12-25 06:57:13',43),('Milan Chauhan','2013-12-25 08:22:11',43),('Milan Chauhan','2013-12-25 11:19:15',43),('Milan Chauhan','2013-12-25 12:45:20',43),('Milan Chauhan','2013-12-25 14:44:48',43),('Milan Chauhan','2013-12-25 15:02:03',43),('Milan Chauhan','2013-12-26 13:55:39',43),('Milan Chauhan','2013-12-26 16:30:48',43),('Milan Chauhan','2013-12-27 13:49:40',43),('Milan Chauhan','2013-12-28 02:29:13',43),('Milan Chauhan','2013-12-28 13:10:36',43),('Milan Chauhan','2013-12-28 13:43:15',43),('Milan Chauhan','2013-12-28 14:16:51',43),('Milan Chauhan','2013-12-28 18:00:23',43),('Milan Chauhan','2013-12-29 03:17:47',43),('Milan Chauhan','2013-12-29 05:25:27',43),('Milan Chauhan','2013-12-29 06:07:38',43),('Milan Chauhan','2013-12-29 12:05:00',43),('Milan Chauhan','2013-12-29 14:47:02',43),('Milan Chauhan','2013-12-29 16:23:14',43),('Milan Chauhan','2013-12-29 16:42:43',43),('Milan Chauhan','2013-12-29 17:17:41',43),('Milan Chauhan','2013-12-31 11:38:25',43),('Milan Chauhan','2013-12-31 11:59:38',43),('Milan Chauhan','2013-12-31 13:08:02',43),('Milan Chauhan','2013-12-31 15:04:27',43),('Milan Chauhan','2013-12-31 23:46:43',43),('Milan Chauhan','2014-01-01 00:31:26',43),('Milan Chauhan','2014-01-01 02:28:25',43),('Milan Chauhan','2014-01-01 02:44:50',43),('Milan Chauhan','2014-01-01 13:38:33',43),('Milan Chauhan','2014-01-01 13:46:19',43),('Milan Chauhan','2014-01-01 13:53:22',43),('Milan Chauhan','2014-01-01 13:59:31',43),('Milan Chauhan','2014-01-01 14:06:19',43),('Milan Chauhan','2014-01-01 14:07:37',43),('Milan Chauhan','2014-01-02 14:29:45',43),('Milan Chauhan','2014-01-02 14:44:12',43),('Milan Chauhan','2014-01-05 15:58:35',43),('Milan Chauhan','2014-01-07 04:40:37',43),('Milan Chauhan','2014-01-07 04:47:16',43),('Milan Chauhan','2014-01-07 04:51:37',43),('Anoop Chaturvidi','2014-01-07 05:06:40',1),('Milan Chauhan','2014-01-07 11:39:13',43),('Milan Chauhan','2016-08-31 05:58:14',43),('Milan Chauhan','2016-08-31 06:18:24',43),('Milan Chauhan','2016-08-31 07:24:48',43),('Milan Chauhan','2016-09-01 05:02:29',43),('Milan Chauhan','2016-09-01 05:58:28',43),('Milan Chauhan','2016-09-01 06:03:22',43),('Milan Chauhan','2016-09-01 06:05:31',43),('Milan Chauhan','2016-09-01 06:07:38',43),('Milan Chauhan','2016-09-01 06:12:43',43),('Milan Chauhan','2016-09-01 06:46:03',43),('Milan Chauhan','2016-09-01 09:39:14',43),('Milan Chauhan','2016-09-02 05:35:09',43),('Milan Chauhan','2016-09-02 05:47:18',43),('Milan Chauhan','2016-09-19 06:08:52',43),('Milan Chauhan','2016-09-19 06:49:33',43),('Milan Chauhan','2016-09-19 07:22:58',43),('Milan Chauhan','2016-09-19 07:58:40',43),('Milan Chauhan','2016-09-19 10:29:23',43),('Milan Chauhan','2016-09-19 11:07:20',43),('Milan Chauhan','2016-09-19 11:26:22',43),('Milan Chauhan','2016-09-19 11:41:26',43),('Milan Chauhan','2016-09-19 12:04:46',43),('Milan Chauhan','2016-09-19 12:12:37',43),('Milan Chauhan','2016-09-21 09:45:27',43),('Milan Chauhan','2016-09-21 10:46:35',43),('Milan Chauhan','2016-09-21 12:02:06',43),('Milan Chauhan','2016-09-21 12:17:50',43),('Milan Chauhan','2016-09-22 06:35:45',43),('Milan Chauhan','2016-09-22 06:52:42',43),('Milan Chauhan','2016-09-22 08:19:06',43),('Milan Chauhan','2016-09-22 08:41:36',43),('Milan Chauhan','2016-09-22 09:08:55',43),('Milan Chauhan','2016-09-22 09:42:02',43),('Milan Chauhan','2016-09-22 10:20:39',43),('Milan Chauhan','2016-09-22 11:04:34',43),('Milan Chauhan','2016-09-22 11:22:29',43),('Milan Chauhan','2016-09-22 15:39:36',43),('Milan Chauhan','2016-09-22 16:22:54',43),('Milan Chauhan','2016-09-22 16:49:37',43),('Milan Chauhan','2016-09-22 16:56:47',43),('Milan Chauhan','2016-09-23 15:45:37',43),('Milan Chauhan','2016-09-23 16:53:11',43),('Milan Chauhan','2016-09-23 17:26:06',43),('Milan Chauhan','2016-09-23 17:59:21',43),('Milan Chauhan','2016-09-24 02:25:00',43),('Milan Chauhan','2016-09-24 04:34:47',43),('Milan Chauhan','2016-09-24 04:34:54',43),('Milan Chauhan','2016-09-24 04:40:50',43),('Milan Chauhan','2016-09-24 04:43:47',43),('Milan Chauhan','2016-09-24 04:52:01',43),('Milan Chauhan','2016-09-24 05:01:12',43),('Milan Chauhan','2016-09-24 15:07:36',43),('Milan Chauhan','2016-09-24 15:23:31',43),('Milan Chauhan','2016-09-24 15:28:53',43),('Milan Chauhan','2016-09-24 18:20:34',43),('Milan Chauhan','2016-09-24 18:23:10',43),('Milan Chauhan','2016-09-24 18:26:18',43),('Milan Chauhan','2016-09-25 03:26:50',43),('Milan Chauhan','2016-09-25 03:51:28',43),('Milan Chauhan','2016-09-25 03:57:16',43),('Milan Chauhan','2016-09-25 04:27:46',43),('Milan Chauhan','2016-09-25 04:37:13',43),('Milan Chauhan','2016-09-25 04:43:17',43),('Milan Chauhan','2016-09-25 05:00:08',43),('Milan Chauhan','2016-09-25 05:18:23',43),('Milan Chauhan','2016-09-25 05:18:41',43),('Milan Chauhan','2016-09-25 05:19:45',43),('Milan Chauhan','2016-09-25 05:20:26',43),('Milan Chauhan','2016-09-25 05:20:33',43),('Milan Chauhan','2016-09-25 05:22:52',43),('Milan Chauhan','2016-09-25 05:23:12',43),('Milan Chauhan','2016-09-25 05:24:19',43),('Milan Chauhan','2016-09-25 05:24:44',43),('Milan Chauhan','2016-09-25 05:24:57',43),('Milan Chauhan','2016-09-25 05:25:01',43),('Milan Chauhan','2016-09-25 05:30:53',43),('Milan Chauhan','2016-09-25 05:31:10',43),('Milan Chauhan','2016-09-25 05:32:36',43),('Milan Chauhan','2016-09-25 05:35:00',43),('Milan Chauhan','2016-09-25 05:35:16',43),('Milan Chauhan','2016-09-25 05:37:15',43),('Milan Chauhan','2016-09-25 05:38:54',43),('Milan Chauhan','2016-09-25 06:19:20',43),('Milan Chauhan','2016-09-25 06:20:08',43),('Milan Chauhan','2016-09-25 07:19:16',43),('Milan Chauhan','2016-09-25 07:21:48',43),('Milan Chauhan','2016-09-25 07:34:24',43),('Milan Chauhan','2016-09-25 13:12:32',43),('Milan Chauhan','2016-09-25 13:27:58',43),('Milan Chauhan','2016-09-25 16:58:51',43),('Milan Chauhan','2016-09-25 17:01:11',43);
/*!40000 ALTER TABLE `sch_login_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_month_detail`
--

DROP TABLE IF EXISTS `sch_month_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_month_detail` (
  `month_id` int(10) NOT NULL AUTO_INCREMENT,
  `month_name` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`month_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_month_detail`
--

LOCK TABLES `sch_month_detail` WRITE;
/*!40000 ALTER TABLE `sch_month_detail` DISABLE KEYS */;
INSERT INTO `sch_month_detail` VALUES (1,'January'),(2,'February'),(3,'March'),(4,'April'),(5,'May'),(6,'June'),(7,'July'),(8,'August'),(9,'September'),(10,'October'),(11,'November'),(12,'December');
/*!40000 ALTER TABLE `sch_month_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_parent_detail`
--

DROP TABLE IF EXISTS `sch_parent_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_parent_detail` (
  `parent_id` int(10) NOT NULL AUTO_INCREMENT,
  `student_id` int(10) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `mother_name` varchar(50) DEFAULT NULL,
  `father_higher_qualification_id` int(10) DEFAULT NULL,
  `mather_higher_qualification_id` int(10) DEFAULT NULL,
  `email_id` varchar(30) DEFAULT NULL,
  `user_id` varchar(8) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_parent_detail`
--

LOCK TABLES `sch_parent_detail` WRITE;
/*!40000 ALTER TABLE `sch_parent_detail` DISABLE KEYS */;
INSERT INTO `sch_parent_detail` VALUES (1,1,'Arvindra','',-1,NULL,'knp.vinod@gmail.com','BHSP0001','b6ec67ab222d7326968bd5669a8abb01'),(2,2,'Arvindra','babli',4,NULL,'knp.vinod@gmail.com','BHSP0002','b6ec67ab222d7326968bd5669a8abb01'),(3,3,'Arvindra','babli',4,NULL,'knp.vinod@gmail.com','BHSP0003','9c3a3b6c396f9a5f3bd0938ddbf08c01'),(4,4,'Arvindra','Babli',4,NULL,'knp.vinod@gmail.com','BHSP0004','9c3a3b6c396f9a5f3bd0938ddbf08c01'),(5,5,'Vinod Singh','Mamta',4,NULL,'knp.vinod@gmail.com','BHSP0005',''),(6,6,'Ramesh','Vimla',5,NULL,'vinodsingh.knp@gmail.com','BHSP0006','18bcacc1ad1a164d2656e9ae665fd5cf'),(7,7,'Vinod Singh','Pratima Singh',5,NULL,'vinodbhadouria@gmail.com','BHSP0007',''),(8,8,'Vinod Singh','Pratima Singh',5,NULL,'vinodbhadouria@gmail.com','BHSP0008',''),(9,9,'Vinod','Pratima',5,NULL,'vinodsingh.knp@gmail.com','BHSP0009','44d70e970ad6d9cab55de2ab25922d1e'),(30,10,'Mulayam singh','TEST',2,NULL,'vinodsingh.knp@gmail.com','BHSP0010','4df643f4551f04ce693c0f59be2cdaf1');
/*!40000 ALTER TABLE `sch_parent_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_parent_heirarchy`
--

DROP TABLE IF EXISTS `sch_parent_heirarchy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_parent_heirarchy` (
  `ug_id` int(11) DEFAULT NULL,
  `parent_ug_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `parent_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_parent_heirarchy`
--

LOCK TABLES `sch_parent_heirarchy` WRITE;
/*!40000 ALTER TABLE `sch_parent_heirarchy` DISABLE KEYS */;
INSERT INTO `sch_parent_heirarchy` VALUES (7,NULL,1,NULL),(7,NULL,2,NULL),(7,NULL,3,NULL),(7,NULL,4,NULL),(7,NULL,5,NULL),(7,NULL,6,NULL),(7,NULL,7,NULL),(7,NULL,8,NULL),(7,NULL,9,NULL),(7,NULL,10,NULL);
/*!40000 ALTER TABLE `sch_parent_heirarchy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_prev_student_detail`
--

DROP TABLE IF EXISTS `sch_prev_student_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_prev_student_detail` (
  `qualification_id` int(3) NOT NULL,
  `qualification_title` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`qualification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_prev_student_detail`
--

LOCK TABLES `sch_prev_student_detail` WRITE;
/*!40000 ALTER TABLE `sch_prev_student_detail` DISABLE KEYS */;
INSERT INTO `sch_prev_student_detail` VALUES (-1,'None'),(1,'First-I'),(2,'Second-II'),(3,'Third-III'),(4,'Fourth-IV'),(5,'Fifth-V'),(6,'Sixth-VI'),(7,'Seventh-VII'),(8,'Eighth-VIII'),(9,'Nineth-IX'),(10,'Tenth-X(High School)');
/*!40000 ALTER TABLE `sch_prev_student_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_qualification_detail`
--

DROP TABLE IF EXISTS `sch_qualification_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_qualification_detail` (
  `qualification_id` int(10) NOT NULL AUTO_INCREMENT,
  `qualification_title` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`qualification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_qualification_detail`
--

LOCK TABLES `sch_qualification_detail` WRITE;
/*!40000 ALTER TABLE `sch_qualification_detail` DISABLE KEYS */;
INSERT INTO `sch_qualification_detail` VALUES (-1,'None'),(1,'5th'),(2,'High School(10th)'),(3,'Enter(12th)'),(4,'Graguate'),(5,'Post Graguate');
/*!40000 ALTER TABLE `sch_qualification_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_registration_detail`
--

DROP TABLE IF EXISTS `sch_registration_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_registration_detail` (
  `registration_no` int(20) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `middle_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `father_name` varchar(50) DEFAULT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  `registration_date` varchar(10) DEFAULT NULL,
  `address` varchar(1000) NOT NULL,
  `contact_no` varchar(10) DEFAULT NULL,
  `reg_status` char(1) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `dateofbirth` varchar(10) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `reg_entrance_status` char(1) DEFAULT NULL,
  `preinstclass_id` int(10) DEFAULT NULL,
  `previous_edu_center` varchar(100) DEFAULT NULL,
  `dept_id` int(10) DEFAULT NULL,
  `registration_fee` int(10) DEFAULT NULL,
  `concessionamt` int(10) DEFAULT NULL,
  `comments` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`registration_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_registration_detail`
--

LOCK TABLES `sch_registration_detail` WRITE;
/*!40000 ALTER TABLE `sch_registration_detail` DISABLE KEYS */;
INSERT INTO `sch_registration_detail` VALUES (1,'Rameshwar','Singh','Bhadouria','Arvindra','knp.vinod@gmail.com','9/29/2012','flat no 201,kanpur','9867891771','A','Male','9/29/2004',2,9,NULL,1,'mnct',1,100,10,'Poor Family Background'),(2,'Rohit','Singh','Bhadouria','Arvindra','knp.vinod@gmail.com','9/29/2012','kanpur','9867891771','A','Male','9/29/1996',6,9,NULL,4,'mnct',1,100,20,'staff relationship'),(3,'Mukesh','Singh','Rajput','Arvindra','knp.vinod@gmail.com','9/29/2012','Eman chuck kanpur','9867891771','S','Male','9/29/2000',8,9,NULL,5,'cjpt',1,100,20,'Staff relief'),(4,'Vipul','Singh','Bhadouria','Arvindra','knp.vinod@gmail.com','9/29/2012','kanpur','9867891771','S','Male','9/29/1999',8,9,NULL,5,'mcdt',1,100,20,'Staff discount'),(5,'Vikranth','Singh','Rajput','Vinod Singh','knp.vinod@gmail.com','9/29/2012','plot no.38, Surendra Nagar rawatpur kanpur','9867891771','A','Male','9/29/1998',8,9,NULL,5,'dps',1,100,20,'poor family'),(6,'Madhav','Gopal','Krishna','Vinod Singh','knp.vinod@gmail.com','9/29/2012','rawatpur kanpur','9867891771','S','Male','9/29/1999',9,9,NULL,6,'mnct',1,100,10,'Poor family'),(7,'Vishal','Singh','Chauhan','Arvindra','vinodsingh.knp@gmail.com','0/18/2012','Flat no 201 Matrachhya Sadan Airoli','9867891771','P','Male','0/16/1983',3,9,NULL,2,'',1,0,0,''),(8,'Viraj','Singh','Bhadouria','Vinod Singh','vinodbhadouria@gmail.com','8/30/2013','flat no 201, Matrachhya sadan airoli navi mumbai-400708','9867891771','A','Male','3/18/2011',1,10,NULL,1,'N/A',1,100,10,'Poor Family'),(9,'Rohit','Kumar','rawat','Vishal','vinodbhadouria@gmail.com','9/6/2013','flat no 203,\r\nMumbai\r\n','9867891771','A','Male','9/6/2006',13,10,NULL,9,'N/A',2,500,100,'student of staff member'),(10,'ashok','kumar','Singh','mohit','vinodbhadouria@gmail.com','9/6/2013','kanpur utter pradesh','9867891771','P','Male','9/6/1997',15,10,NULL,10,'ram lala inter college',4,0,0,''),(11,'rohit','kumar','singh','Umesh Singh','vinodsingh.knp@gmail.com','8/1/2016','kanpur utter pradesh','9867891771','P','Male','5/11/2013',2,NULL,NULL,1,'test',1,0,0,''),(12,'Vinod','Singh','Bhadouria','Surendra Singh','vinodsingh.knp@gmail.com','8/1/2016','kanpur Utter pradesh','9867891771','A','Male','8/1/1996',13,13,NULL,9,'LNCT',2,500,200,'He belongs to poor family');
/*!40000 ALTER TABLE `sch_registration_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_registration_parent_detail`
--

DROP TABLE IF EXISTS `sch_registration_parent_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_registration_parent_detail` (
  `parent_id` int(10) NOT NULL AUTO_INCREMENT,
  `registration_no` int(10) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `mother_name` varchar(50) DEFAULT NULL,
  `father_higher_qualification_id` int(10) DEFAULT NULL,
  `mather_higher_qualification_id` int(10) DEFAULT NULL,
  `email_id` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_registration_parent_detail`
--

LOCK TABLES `sch_registration_parent_detail` WRITE;
/*!40000 ALTER TABLE `sch_registration_parent_detail` DISABLE KEYS */;
INSERT INTO `sch_registration_parent_detail` VALUES (1,1,'Arvindra','babli',4,4,'knp.vinod@gmail.com','knp.vinod@gmail.com'),(2,2,'Arvindra','babli',4,4,'knp.vinod@gmail.com','knp.vinod@gmail.com'),(3,3,'Arvindra','babli',4,4,'knp.vinod@gmail.com','knp.vinod@gmail.com'),(4,4,'Arvindra','Babli',4,4,'knp.vinod@gmail.com','knp.vinod@gmail.com'),(5,5,'Vinod Singh','Pratima Singh',4,4,'knp.vinod@gmail.com','knp.vinod@gmail.com'),(6,6,'Vinod Singh','Mamta',4,4,'knp.vinod@gmail.com','knp.vinod@gmail.com'),(7,7,'Arvindra','Babli',5,5,'knp.vinod@gmail.com','knp.vinod@gmail.com'),(8,8,'Vinod Singh','Pratima Singh',5,5,'vinodbhadouria@gmail.com','vinodbhadouria@gmail.com'),(9,9,'Vishal','eisha',5,5,'vinodsingh.knp@gmail.com','vinodsingh.knp@gmail.com'),(10,10,'mohit','mahima',5,5,'vinodsingh.knp@gmail.com','vinodsingh.knp@gmail.com'),(11,11,'Umesh Singh','Kavita',1,1,'vinodsingh.knp@gmail.com','vinodsingh.knp@gmail.com'),(12,12,'Surendra Singh','Shushila Devi',4,4,'vinodsingh.knp@gmail.com','vinodsingh.knp@gmail.com');
/*!40000 ALTER TABLE `sch_registration_parent_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_staff_designation`
--

DROP TABLE IF EXISTS `sch_staff_designation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_staff_designation` (
  `staff_designation_id` int(10) NOT NULL DEFAULT '0',
  `staff_designation_title` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`staff_designation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_staff_designation`
--

LOCK TABLES `sch_staff_designation` WRITE;
/*!40000 ALTER TABLE `sch_staff_designation` DISABLE KEYS */;
INSERT INTO `sch_staff_designation` VALUES (1,'Director'),(2,'Principal'),(3,'Teacher'),(4,'Accountant'),(5,'Librarian'),(6,'Student');
/*!40000 ALTER TABLE `sch_staff_designation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_staff_detail`
--

DROP TABLE IF EXISTS `sch_staff_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_staff_detail` (
  `emp_id` int(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `designation` int(1) NOT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  `user_id` varchar(8) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `salary` decimal(10,0) DEFAULT NULL,
  `address` varchar(100) NOT NULL,
  `contact_no` varchar(10) DEFAULT NULL,
  `qualification_id` int(10) NOT NULL,
  `emp_status` char(1) DEFAULT NULL,
  `previous_exp` varchar(100) NOT NULL,
  `joining_date` datetime DEFAULT NULL,
  `release_date` datetime DEFAULT NULL,
  `dept_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_staff_detail`
--

LOCK TABLES `sch_staff_detail` WRITE;
/*!40000 ALTER TABLE `sch_staff_detail` DISABLE KEYS */;
INSERT INTO `sch_staff_detail` VALUES (8,'Akash','singh','bhadouria',2,'akash@com.com',NULL,'5f4dcc3b5aa765d61d8327deb882cf99',3000,'kanpur','9868888',2,'A','2 year teaching exp.','2011-02-17 00:00:00',NULL,NULL),(9,'Ram','singh','Dixit',3,'ram@gmail.com',NULL,'5f4dcc3b5aa765d61d8327deb882cf99',3500,'kanpur,Ghatampur','9867891771',2,'D','Fresh','2011-02-18 00:00:00',NULL,1),(10,'vinod raj','singh','bhadouria',4,'change@com.com',NULL,'5f4dcc3b5aa765d61d8327deb882cf99',3000,'kanpur','9868888',3,'A','Fresh','2011-02-19 00:00:00',NULL,NULL),(11,'rohit','singh','Pujari',3,'pujari@gmail.com',NULL,'5f4dcc3b5aa765d61d8327deb882cf99',43000,'pune','986754321',2,'A','Fresher','2011-02-05 00:00:00',NULL,1),(12,'Abhishek','b','bhachhan',3,'abhi@gmail.com',NULL,'5f4dcc3b5aa765d61d8327deb882cf99',50000,'mumbai','97564329',3,'A','4 year exp in film ind.','2011-02-12 00:00:00',NULL,NULL),(13,'Test your App ','App','Edu',3,'test@gmail.com',NULL,'5f4dcc3b5aa765d61d8327deb882cf99',404323,'Rawatpur,kanpur','98765432',2,'D','NO ECX','2011-02-26 00:00:00',NULL,NULL),(14,'vinod','singh','bhadouria',1,'vinod@gmail.com',NULL,'5f4dcc3b5aa765d61d8327deb882cf99',20000,'kanpur','9867891771',2,'A','2 year in ct','2011-03-02 00:00:00',NULL,NULL),(15,'Arvindra','Singh','Rathore',3,'rathore@gmail.com',NULL,'5f4dcc3b5aa765d61d8327deb882cf99',2300,'kanpur rawatpur','9867891771',3,'A','2 year teaching exp. in xyz school','2011-06-03 00:00:00',NULL,NULL),(16,'Rajat','Sinngh','Rajput',3,'rajatcall@gmail.com',NULL,'password',4000,'Plot no 38,Surendra Nagar Rawatpur knapur','9867891771',4,'A','dfsafadsf','2011-06-12 00:00:00',NULL,1),(17,'Mitesh','singh','talwar',2,'mitesh@gmail.com',NULL,'password',5000,'gm road pune near by karve hospital','98675462',4,'A','2 year teaching exp. in administrator','2011-06-07 00:00:00',NULL,-1),(18,'panda deb','Test','product',3,'deatail@gmail.com',NULL,'password',4000,'plot no 38 surendra nagar rawatpur kanpue','9867891771',4,'A','none','2011-06-16 00:00:00',NULL,1),(19,'Ramesh','Singh','Sachan',3,'sachan@gmail.com',NULL,'password',15000,'Adarsh nagar ram lala trust kanpur','8765678121',4,'A','2 year teaching exp. in administrator','2011-06-25 00:00:00',NULL,1),(20,'Ram','Singh','Raja',3,'ramsin@gmail.com',NULL,'password',7000,'kanpur 208019','9867891771',4,'A','2 year teaching exp.','2011-07-03 00:00:00',NULL,1),(21,'first_name','middle_name',' last_name',2,'email_id','BHSE0021','strpassword',2011,'address','contact_no',2,'D','previous_exp','2011-03-08 00:00:00',NULL,2),(22,'first_name','middle_name',' last_name',2,'email_id','BHSE0022','strpassword',2011,'address','contact_no',2,'A','previous_exp','2011-03-08 00:00:00',NULL,2),(23,'first_name','middle_name',' last_name',2,'email_id','BHSE0023','strpassword',2011,'address','contact_no',2,'A','previous_exp','2011-03-08 00:00:00',NULL,2),(24,'first_name','middle_name',' last_name',2,'email_id',NULL,'strpassword',2011,'address','contact_no',2,'A','previous_exp','2011-03-08 00:00:00',NULL,2),(25,'first_name','middle_name',' last_name',2,'email_id','24','strpassword',2011,'address','contact_no',2,'A','previous_exp','2011-03-08 00:00:00',NULL,2),(26,'first_name','middle_name',' last_name',2,'email_id','26','strpassword',2011,'address','contact_no',2,'A','previous_exp','2011-03-08 00:00:00',NULL,2),(27,'first_name','middle_name',' last_name',2,'email_id','27','strpassword',2011,'address','contact_no',2,'A','previous_exp','2011-03-08 00:00:00',NULL,2),(28,'first_name','middle_name',' last_name',2,'email_id','1','strpassword',2011,'address','contact_no',2,'A','previous_exp','2011-03-08 00:00:00',NULL,2),(29,'first_name','middle_name',' last_name',2,'email_id','1','strpassword',2011,'address','contact_no',2,'A','previous_exp','2011-03-08 00:00:00',NULL,2),(30,'first_name','middle_name',' last_name',2,'email_id','BHSE0030','5f4dcc3b5aa765d61d8327deb882cf99',2011,'address','contact_no',2,'A','previous_exp','2011-03-08 00:00:00',NULL,2),(31,'first_name','middle_name',' last_name',2,'email_id','BHSE31','strpassword',2011,'address','contact_no',2,'A','previous_exp','2011-03-08 00:00:00',NULL,2),(32,'Vimal','Singh','Upadhya',1,'vimal@gmail.com','BHSE0032','2011829',23000,'kanpur,rawatpur','9867891771',4,'A','10 yaers in Education management','2011-08-29 00:00:00',NULL,-1),(33,'Remya ','Rajan','ji',3,'remy@gmail.com','BHSE0033','2011829',12000,'Kanpur,Utter Pradesh','9867891771',4,'A','2 year exp.','2011-08-29 00:00:00',NULL,1),(34,'Akash','singh','rajput',2,'email@gmail.com','BHSE0034','201192',4000,'knapur','9867891771',4,'A','none','2011-09-02 00:00:00',NULL,-1),(35,'Dhermendra','singh','panjabi',4,'gmail@gmail.com','BHSE0035','201193',4000,'kapur','43254565',4,'A','2 year exp.','2011-09-03 00:00:00',NULL,-1),(36,'Mahesh','Singh','Parihar',1,'knp.vinod@gmail.com','BHSE0036','201193',23000,'Kanpur Nagar','986745321',4,'A','10 yaers in Education management','2011-09-03 00:00:00',NULL,-1),(37,'Ramesh','pandit','pathak',1,'knp.vinod@gmail.com','BHSE0037','201193',12000,'Ujjain','984532891',4,'A','2 year exp.','2011-09-03 00:00:00',NULL,-1),(38,'Mithali','dutt','chetarji',2,'knp.vinod@gmail.com','BHSE0038','2011910',134521,'West Bangal,Hawada ','98675431',4,'A','10 yaers in Education management','2011-09-10 00:00:00',NULL,-1),(39,'Himesh','Singh','Raja',1,'knp.vinod@gmail.com','BHSE0039','20111020',34000,'Delhi','98675412',4,'A','10 yaers in Education management','2011-10-20 00:00:00',NULL,-1),(40,'Ram','Singh','Sharma',2,'knp.vinod@gmail.com','BHSE0040','5f4dcc3b5aa765d61d8327deb882cf99',15000,'Bhopal','987654321',4,'A','10 yaers in Education management','2011-10-28 00:00:00',NULL,-1),(41,'Mitan','Singh','Solanki',4,'knp.vinod@gmail.com','BHSE0041','5f4dcc3b5aa765d61d8327deb882cf99',23000,'Kanpur Rawatpur Utter Pradesh','9867892341',4,'A','10 yaers in Education management','2011-09-29 00:00:00',NULL,-1),(42,'TEst','terdst','twert',1,'knp.vinod@gmail.com','BHSE0042','',2311233,'cxghd','5675546',2,'A','2','2011-09-03 00:00:00',NULL,-1),(43,'Milan','Singh','Chauhan',1,'knp.vinod@gmail.com','BHSE0043','5f4dcc3b5aa765d61d8327deb882cf99',23000,'Rawatpur,Kanpur','986790121',4,'A','11 year exp.','2011-12-23 00:00:00',NULL,-1),(44,'Vikash','Singh','Rajput',3,'knp.vinod@gmail.com','BHSE0044','5f4dcc3b5aa765d61d8327deb882cf99',15000,'Ram Nagar rawatpur kanpur','9867891771',4,'A','3 year teaching exp.','2011-09-17 00:00:00',NULL,1),(45,'Pratima','Singh','Rathore',3,'knp.vinod@gmail.com','BHSE0045','96c99e06fdbdb47f489f8f904aa5dccf',7000,'Mainpuri flat no. 01,U.P','9867891771',4,'A','12 month teachin exp.','2012-09-10 00:00:00',NULL,1),(46,'Mohit','Singh','Sharma',3,'knp.vinod@gmail.com','BHSE0046','3a2b1ccd4c4e1827b036a2962d6e771a',4000,'Flat no. 38 Surendra Nagar Rawatpur Kanpur','9867891771',4,'A','2 year exp. in lnct','2012-11-01 00:00:00',NULL,1),(47,'Vishwanath','Pratap','Singh',1,'knp.vino@gmail.com','BHSE0047','3a2b1ccd4c4e1827b036a2962d6e771a',500000,'Pali hill Mumbai','9867891771',4,'A','10 year teaching exp.','2012-11-01 00:00:00',NULL,-1),(48,'Manish','Singh','Vishnoi',4,'knp.vinod@gmail.com','BHSE0048','5f4dcc3b5aa765d61d8327deb882cf99',60000,'23/3 B, Madan Nagar Kanpur','9867891771',4,'A','3 year teaching exp.','2012-11-03 00:00:00',NULL,-1),(49,'Rohit','Singh','Raja',3,'vinodsingh.knp@gmail.com','BHSE0049','4965c7f8fdbdd30f2f0a284113c892e3',4500,'Plot no 38,Surendra Nagar','9867891771',5,'A','2 year teaching exp','2016-08-31 00:00:00',NULL,1);
/*!40000 ALTER TABLE `sch_staff_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_staff_heirarchy`
--

DROP TABLE IF EXISTS `sch_staff_heirarchy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_staff_heirarchy` (
  `ug_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_ug_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `parent_user_id` int(11) DEFAULT NULL,
  KEY `ug_id` (`ug_id`),
  KEY `nsh_fk02` (`parent_ug_id`),
  CONSTRAINT `nsh_fk02` FOREIGN KEY (`parent_ug_id`) REFERENCES `sch_user_groups` (`ug_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_staff_heirarchy`
--

LOCK TABLES `sch_staff_heirarchy` WRITE;
/*!40000 ALTER TABLE `sch_staff_heirarchy` DISABLE KEYS */;
INSERT INTO `sch_staff_heirarchy` VALUES (1,NULL,1,NULL),(1,NULL,2,NULL),(1,NULL,3,NULL),(2,NULL,4,NULL),(2,1,5,2),(4,3,6,7),(3,1,7,2),(4,2,8,4),(5,2,10,4),(6,5,12,9),(6,5,13,10),(2,1,32,1),(9,NULL,50,NULL),(1,NULL,51,NULL),(1,NULL,14,NULL),(3,NULL,15,NULL),(3,NULL,16,NULL),(2,NULL,17,NULL),(3,NULL,19,NULL),(3,NULL,18,NULL),(2,NULL,20,NULL),(2,NULL,21,NULL),(2,NULL,22,NULL),(2,NULL,23,NULL),(2,NULL,24,NULL),(2,NULL,25,NULL),(2,NULL,26,NULL),(2,NULL,27,NULL),(2,NULL,28,NULL),(2,NULL,29,NULL),(2,NULL,30,NULL),(1,NULL,31,NULL),(3,NULL,33,NULL),(2,NULL,34,NULL),(4,NULL,35,NULL),(1,NULL,36,NULL),(1,NULL,37,NULL),(2,NULL,38,NULL),(1,NULL,39,NULL),(2,NULL,40,NULL),(4,NULL,41,NULL),(1,NULL,42,NULL),(1,NULL,43,NULL),(3,NULL,11,NULL),(3,NULL,44,NULL),(3,NULL,45,NULL),(1,NULL,47,NULL),(4,NULL,48,NULL),(1,NULL,43,NULL),(3,NULL,46,NULL),(3,NULL,9,NULL),(3,NULL,49,NULL);
/*!40000 ALTER TABLE `sch_staff_heirarchy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_student_admissionfee_collection`
--

DROP TABLE IF EXISTS `sch_student_admissionfee_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_student_admissionfee_collection` (
  `student_id` int(10) NOT NULL,
  `session_id` int(10) NOT NULL,
  `admission_fee` int(10) DEFAULT NULL,
  `concession_amount` int(10) DEFAULT NULL,
  `comment` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`student_id`,`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_student_admissionfee_collection`
--

LOCK TABLES `sch_student_admissionfee_collection` WRITE;
/*!40000 ALTER TABLE `sch_student_admissionfee_collection` DISABLE KEYS */;
INSERT INTO `sch_student_admissionfee_collection` VALUES (1,9,2000,500,NULL),(1,10,2400,400,'Poor family Background'),(1,11,3000,0,''),(2,9,3000,1000,'Poor Family Background'),(3,9,4000,1000,NULL),(4,9,4000,1000,NULL),(5,9,4000,0,NULL),(6,10,2000,100,'Staff Member'),(6,11,2000,0,''),(7,10,0,0,''),(8,10,2000,0,NULL),(9,13,2000,0,''),(10,13,2400,0,'');
/*!40000 ALTER TABLE `sch_student_admissionfee_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_student_class_detail`
--

DROP TABLE IF EXISTS `sch_student_class_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_student_class_detail` (
  `student_id` varchar(20) NOT NULL,
  `class_id` varchar(20) NOT NULL,
  `preinstclass_id` varchar(20) DEFAULT NULL,
  `session_id` int(10) NOT NULL,
  `dept_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`student_id`,`class_id`,`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_student_class_detail`
--

LOCK TABLES `sch_student_class_detail` WRITE;
/*!40000 ALTER TABLE `sch_student_class_detail` DISABLE KEYS */;
INSERT INTO `sch_student_class_detail` VALUES ('1','2','1',9,1),('1','3','2',10,1),('1','5','3',11,1),('10','3','2',13,1),('2','5','3',9,1),('3','8','5',9,1),('4','2','5',9,1),('5','9','6',9,1),('6','1','-1',10,1),('6','2','1',11,1),('7','1','1',10,1),('8','1','1',10,1),('9','2','1',13,1);
/*!40000 ALTER TABLE `sch_student_class_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_student_detail`
--

DROP TABLE IF EXISTS `sch_student_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_student_detail` (
  `student_id` int(20) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `middle_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `user_id` varchar(8) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  `admission_date` varchar(255) DEFAULT NULL,
  `address` varchar(500) NOT NULL,
  `contact_no` varchar(10) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `previous_edu_center` varchar(50) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `dateofbirth` varchar(20) DEFAULT NULL,
  `birth_day` int(2) DEFAULT NULL,
  `birth_month` int(2) DEFAULT NULL,
  `birth_year` int(4) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_student_detail`
--

LOCK TABLES `sch_student_detail` WRITE;
/*!40000 ALTER TABLE `sch_student_detail` DISABLE KEYS */;
INSERT INTO `sch_student_detail` VALUES (1,'Anoop','Singh','Chaturvidi','BHSS0001','5f4dcc3b5aa765d61d8327deb882cf99','vinodsingh.knp@gmail.com','9/28/2012','flat no.201,kanpur','9867891771','A','mnct','M',NULL,28,9,2002),(2,'Vimal','Singh','Bhadouria','BHSS0002','1659a3ed426c0afc0778973da4d9cef8','knp.vinod@gmail.com','9/29/2012','ramawadi kanpur','9867891771','A','dctc','M',NULL,29,9,1999),(3,'Mukesh','Singh','Rajput','BHSS0003','f10e34ee7a02e6f056a70b386186995f','knp.vinod@gmail.com','9/29/2012','Eman chuck kanpur','9867891771','D','cjpt','Male',NULL,29,9,2000),(4,'Vipul','Singh','Bhadouria','BHSS0004','1659a3ed426c0afc0778973da4d9cef8','knp.vinod@gmail.com','9/29/2012','kanpur','9867891771','D','mcdt','Male',NULL,29,9,1999),(5,'Madhav','Gopal','Krishna','BHSS0005','','vinodbhadouria@gmail.com','11/6/2012','rawatpur kanpur','9867891771','A','mnct','Male',NULL,29,9,1999),(6,'Sudeep','Kumar ','Mishra','BHSS0006','23956d2b3507a6a80cfe4c14abfce647','vinodbhadouria@gmail.com','8/30/2013','flat no 201,Matrachhya sadan airoli navi mumbai-400708','9867891771','D','N/A','M',NULL,21,5,2011),(7,'Master Viraj','Singh','Bhadouria','BHSS0007','','vinodbhadouria@gmail.com','8/30/2013','flat no 201, Matrachhya sadan airoli navi mumbai-400708','9867891771','A','N/A','Male',NULL,18,3,2011),(8,'Viraj','Singh','Bhadouria','BHSS0008','','vinodbhadouria@gmail.com','8/30/2013','flat no 201, Matrachhya sadan airoli navi mumbai-400708','9867891771','A','N/A','Male',NULL,18,3,2011),(9,'Raj','Singh','Raja','BHSS0009','d01e7046da1722c44f368a1733f92c6c','knp.vinod@gmail.com','8/21/2016','kanpur utter pradesh','9867891771','D','Tree House','M',NULL,21,8,2011),(10,'Akhilesh','Mulayam','Yadav','BHSS0010','b550b27f8eabc581c0ac9716f82e354c','vinodsingh.knp@gmail.com','8/22/2016','SAPAI','9867891771','A','pree school','M',NULL,22,8,2012);
/*!40000 ALTER TABLE `sch_student_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_student_heirarchy`
--

DROP TABLE IF EXISTS `sch_student_heirarchy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_student_heirarchy` (
  `ug_id` int(11) DEFAULT NULL,
  `parent_ug_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `parent_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_student_heirarchy`
--

LOCK TABLES `sch_student_heirarchy` WRITE;
/*!40000 ALTER TABLE `sch_student_heirarchy` DISABLE KEYS */;
INSERT INTO `sch_student_heirarchy` VALUES (6,NULL,1,NULL),(6,NULL,2,NULL),(6,NULL,3,NULL),(6,NULL,4,NULL),(6,NULL,5,NULL),(6,NULL,6,NULL),(6,NULL,7,NULL),(6,NULL,8,NULL),(6,NULL,9,NULL),(6,NULL,10,NULL);
/*!40000 ALTER TABLE `sch_student_heirarchy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_student_image_detail`
--

DROP TABLE IF EXISTS `sch_student_image_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_student_image_detail` (
  `student_id` int(10) DEFAULT NULL,
  `photo_id` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_student_image_detail`
--

LOCK TABLES `sch_student_image_detail` WRITE;
/*!40000 ALTER TABLE `sch_student_image_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `sch_student_image_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_student_mark_detail`
--

DROP TABLE IF EXISTS `sch_student_mark_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_student_mark_detail` (
  `student_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `dept_id` int(10) DEFAULT NULL,
  `mark_outof` int(11) DEFAULT NULL,
  `mark_obtained` float(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_student_mark_detail`
--

LOCK TABLES `sch_student_mark_detail` WRITE;
/*!40000 ALTER TABLE `sch_student_mark_detail` DISABLE KEYS */;
INSERT INTO `sch_student_mark_detail` VALUES (1,4,2,9,1,100,75),(1,7,2,9,1,100,67),(1,3,2,9,1,100,89),(1,1,2,9,1,100,56),(1,5,2,9,1,100,77),(4,7,2,9,1,100,34),(4,5,2,9,1,100,67),(4,4,2,9,1,100,59),(5,1,9,9,1,100,78),(5,2,9,9,1,100,67),(5,6,9,9,1,100,90),(1,1,3,10,1,100,90),(1,2,3,10,1,100,90),(1,7,3,10,1,100,90),(1,8,3,10,1,100,90),(6,1,1,10,1,100,80),(6,2,1,10,1,100,90),(6,3,1,10,1,100,99),(6,4,1,10,1,100,88),(6,8,1,10,1,100,68),(9,1,2,13,1,100,33),(9,3,2,13,1,100,40),(9,4,2,13,1,100,50),(9,5,2,13,1,100,60),(9,7,2,13,1,100,70);
/*!40000 ALTER TABLE `sch_student_mark_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_student_result_detail`
--

DROP TABLE IF EXISTS `sch_student_result_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_student_result_detail` (
  `student_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `total_mark_outof` int(11) DEFAULT NULL,
  `total_mark_obtained` float(11,0) DEFAULT NULL,
  `Division` varchar(20) DEFAULT NULL,
  `percentage` float(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_student_result_detail`
--

LOCK TABLES `sch_student_result_detail` WRITE;
/*!40000 ALTER TABLE `sch_student_result_detail` DISABLE KEYS */;
INSERT INTO `sch_student_result_detail` VALUES (1,2,9,'Pass',500,364,'First',73),(4,2,9,'Pass',300,160,'Second',53),(5,9,9,'Pass',300,235,'First',78),(1,3,10,'Pass',400,360,'First',90),(6,1,10,'Pass',500,425,'First',85),(9,2,13,'Pass',500,253,'Second',51);
/*!40000 ALTER TABLE `sch_student_result_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_student_session`
--

DROP TABLE IF EXISTS `sch_student_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_student_session` (
  `session_id` int(10) NOT NULL AUTO_INCREMENT,
  `session_start` date DEFAULT NULL,
  `session_end` date DEFAULT NULL,
  `session_year` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_student_session`
--

LOCK TABLES `sch_student_session` WRITE;
/*!40000 ALTER TABLE `sch_student_session` DISABLE KEYS */;
INSERT INTO `sch_student_session` VALUES (1,'2005-07-01','2006-06-30','2005-2006'),(2,'2006-07-01','2007-06-30','2006-2007'),(3,'2007-07-01','2008-06-30','2007-2008'),(5,'2008-07-01','2009-06-30','2008-2009'),(6,'2009-07-01','2010-06-30','2009-2010'),(7,'2010-07-01','2011-06-30','2010-2011'),(8,'2011-07-01','2012-06-30','2011-2012'),(9,'2012-07-01','2013-06-30','2012-2013'),(10,'2013-07-01','2014-06-30','2013-2014'),(11,'2014-07-01','2015-06-30','2014-2015'),(12,'2015-07-01','2016-06-30','2015-2016'),(13,'2016-07-01','2017-06-30','2016-2017');
/*!40000 ALTER TABLE `sch_student_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_student_session_detail`
--

DROP TABLE IF EXISTS `sch_student_session_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_student_session_detail` (
  `student_id` int(10) DEFAULT NULL,
  `session_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_student_session_detail`
--

LOCK TABLES `sch_student_session_detail` WRITE;
/*!40000 ALTER TABLE `sch_student_session_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `sch_student_session_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_student_subject_detail`
--

DROP TABLE IF EXISTS `sch_student_subject_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_student_subject_detail` (
  `student_subject_relation_id` int(10) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(20) NOT NULL,
  `subject_id` varchar(20) NOT NULL,
  `session_id` int(10) DEFAULT NULL,
  `class_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`student_subject_relation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_student_subject_detail`
--

LOCK TABLES `sch_student_subject_detail` WRITE;
/*!40000 ALTER TABLE `sch_student_subject_detail` DISABLE KEYS */;
INSERT INTO `sch_student_subject_detail` VALUES (115,'1','4',9,2),(116,'1','7',9,2),(117,'1','3',9,2),(118,'1','1',9,2),(119,'1','5',9,2),(120,'2','8',9,5),(121,'2','7',9,5),(122,'2','6',9,5),(123,'2','5',9,5),(124,'3','7',9,8),(125,'3','5',9,8),(126,'3','4',9,8),(127,'4','7',9,8),(128,'4','5',9,8),(129,'4','4',9,8),(130,'1','8',10,3),(131,'1','7',10,3),(132,'1','2',10,3),(133,'1','1',10,3),(134,'5','1',9,9),(135,'5','2',9,9),(136,'5','6',9,9),(137,'6','8',10,1),(138,'6','4',10,1),(139,'6','3',10,1),(140,'6','2',10,1),(141,'6','1',10,1),(142,'1','8',11,5),(143,'1','7',11,5),(144,'1','6',11,5),(145,'1','5',11,5),(156,'6','4',11,2),(157,'6','7',11,2),(158,'6','3',11,2),(159,'6','1',11,2),(160,'6','5',11,2),(164,'7','3',10,1),(165,'7','7',10,1),(166,'9','4',13,2),(167,'9','7',13,2),(168,'9','3',13,2),(169,'9','1',13,2),(170,'9','5',13,2),(171,'10','8',13,3),(172,'10','7',13,3),(173,'10','2',13,3),(174,'10','1',13,3);
/*!40000 ALTER TABLE `sch_student_subject_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_subject_detail`
--

DROP TABLE IF EXISTS `sch_subject_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_subject_detail` (
  `subject_id` int(10) NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(50) DEFAULT NULL,
  `subject_code` int(10) DEFAULT NULL,
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_subject_detail`
--

LOCK TABLES `sch_subject_detail` WRITE;
/*!40000 ALTER TABLE `sch_subject_detail` DISABLE KEYS */;
INSERT INTO `sch_subject_detail` VALUES (1,'Hindi',100),(2,'English',101),(3,'math',103),(4,'Science',104),(5,'Social Science',105),(6,'Biology',106),(7,'History',107),(8,'Physics',108);
/*!40000 ALTER TABLE `sch_subject_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_task_detail`
--

DROP TABLE IF EXISTS `sch_task_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_task_detail` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(50) NOT NULL,
  `task_desc` varchar(100) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_task_detail`
--

LOCK TABLES `sch_task_detail` WRITE;
/*!40000 ALTER TABLE `sch_task_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `sch_task_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_ug_access`
--

DROP TABLE IF EXISTS `sch_ug_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_ug_access` (
  `f_id` int(11) DEFAULT NULL,
  `ug_id` int(11) DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  KEY `f_id` (`f_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_ug_access`
--

LOCK TABLES `sch_ug_access` WRITE;
/*!40000 ALTER TABLE `sch_ug_access` DISABLE KEYS */;
INSERT INTO `sch_ug_access` VALUES (2,1,'2011-03-23 16:27:00',NULL),(3,1,'2011-03-23 16:27:33',NULL),(5,1,'2011-03-23 16:27:56',NULL),(6,1,'2011-03-23 16:28:17',NULL),(7,1,'2011-03-23 16:28:39',NULL),(8,1,'2011-03-23 16:29:07',NULL),(9,1,'2011-03-23 16:28:39',NULL),(19,1,'2012-06-03 10:09:32',NULL),(11,1,'2011-03-23 16:30:59',NULL),(12,1,'2011-03-23 16:31:00',NULL),(13,1,'2011-03-23 16:31:02',NULL),(15,1,'2012-06-03 09:57:41',NULL),(16,1,'2012-06-03 09:57:33',NULL),(17,1,'2012-06-03 09:57:17',NULL),(18,1,'2012-06-03 09:57:12',NULL),(21,1,'2012-06-03 09:56:56',NULL),(20,1,'2012-06-03 09:56:51',NULL),(22,1,'2012-06-03 09:55:59',NULL),(23,1,'2012-06-03 09:55:54',NULL),(24,1,'2012-06-03 09:55:50',NULL),(25,1,'2012-06-03 09:55:44',NULL),(26,1,'2012-06-03 09:55:39',NULL),(27,1,'2012-06-03 09:55:12',NULL),(29,1,'2012-06-03 09:55:06',NULL),(30,1,'2012-06-03 09:55:02',NULL),(31,1,'2012-06-03 09:54:57',NULL),(32,1,'2012-06-03 09:54:51',NULL),(33,1,'2012-06-03 09:54:44',NULL),(14,1,'2012-06-03 09:58:02',NULL),(1,2,'2012-09-09 11:34:31',NULL),(2,2,'2012-09-09 11:34:30',NULL),(3,2,'2012-09-09 11:34:29',NULL),(4,2,'2012-09-09 11:34:28',NULL),(5,2,'2012-09-09 11:34:27',NULL),(6,2,'2012-09-09 11:34:27',NULL),(7,2,'2012-09-09 11:34:26',NULL),(8,2,'2012-09-09 11:34:25',NULL),(9,2,'2012-09-09 11:34:25',NULL),(11,2,'2012-09-09 11:34:25',NULL),(13,2,'2012-09-09 11:34:25',NULL),(14,2,'2012-09-09 11:34:24',NULL),(15,2,'2012-09-09 11:34:24',NULL),(16,2,'2012-09-09 11:34:24',NULL),(17,2,'2012-09-09 11:34:23',NULL),(18,2,'2012-09-09 11:34:23',NULL),(19,2,'2012-09-09 11:34:23',NULL),(20,2,'2012-09-09 11:34:22',NULL),(21,2,'2012-09-09 11:34:22',NULL),(22,2,'2012-09-09 11:34:22',NULL),(23,2,'2012-09-09 11:34:22',NULL),(24,2,'2012-09-09 11:34:21',NULL),(25,2,'2012-09-09 11:34:21',NULL),(26,2,'2012-09-09 11:34:21',NULL),(27,2,'2012-09-09 11:34:20',NULL),(28,2,'2012-09-09 11:34:20',NULL),(29,2,'2012-09-09 11:34:20',NULL),(30,2,'2012-09-09 11:34:19',NULL),(31,2,'2012-09-09 11:34:18',NULL),(32,2,'2012-09-09 11:34:17',NULL),(33,2,'2012-09-09 11:34:17',NULL),(34,2,'2012-09-09 11:34:17',NULL),(35,2,'2012-09-09 11:34:17',NULL),(31,2,'2012-09-09 08:52:51',NULL),(32,2,'2012-09-09 08:52:50',NULL),(33,2,'2012-09-09 08:52:49',NULL),(34,2,'2012-09-09 11:45:19',NULL),(1,2,'2012-06-03 17:52:37',NULL),(2,2,'2012-06-03 18:08:29',NULL),(1,3,'2012-09-09 08:52:28',NULL),(2,3,'2012-09-09 08:53:21',NULL),(3,3,'2012-09-09 08:53:21',NULL),(4,3,'2012-09-09 08:53:20',NULL),(5,3,'2012-09-09 08:53:20',NULL),(6,3,'2012-09-09 08:53:19',NULL),(7,3,'2012-09-09 08:53:18',NULL),(8,3,'2012-09-09 08:53:18',NULL),(9,3,'2012-09-09 08:53:17',NULL),(11,3,'2012-09-09 08:53:17',NULL),(13,3,'2012-09-09 08:53:00',NULL),(14,3,'2012-09-09 08:53:00',NULL),(15,3,'2012-09-09 08:52:58',NULL),(16,3,'2012-09-09 08:52:58',NULL),(17,3,'2012-09-09 08:52:57',NULL),(18,3,'2012-09-09 08:52:57',NULL),(19,3,'2012-09-09 08:52:56',NULL),(20,3,'2012-09-09 08:52:56',NULL),(21,3,'2012-09-09 08:52:56',NULL),(22,3,'2012-09-09 08:52:55',NULL),(23,3,'2012-09-09 08:52:55',NULL),(24,3,'2012-09-09 08:52:54',NULL),(25,3,'2012-09-09 08:52:54',NULL),(26,3,'2012-09-09 08:52:54',NULL),(27,3,'2012-09-09 08:52:53',NULL),(28,3,'2012-09-09 08:52:53',NULL),(29,3,'2012-09-09 08:52:52',NULL),(30,3,'2012-09-09 08:52:52',NULL),(31,3,'2012-09-09 08:52:51',NULL),(32,3,'2012-09-09 08:52:50',NULL),(33,3,'2012-09-09 08:52:49',NULL),(34,3,'2012-09-09 11:06:36',NULL),(35,3,'2012-09-09 11:06:39',NULL),(34,2,'2012-09-09 11:34:16',NULL),(35,2,'2012-09-09 11:34:16',NULL),(36,2,'2012-09-09 11:34:15',NULL),(36,2,'2012-09-09 11:39:54',NULL),(36,3,'2012-09-09 11:06:40',NULL),(34,1,'2012-09-09 11:38:17',NULL),(35,1,'2012-09-09 11:38:54',NULL),(36,1,'2012-09-09 11:39:14',NULL),(1,4,'2012-09-09 11:42:56',NULL),(2,4,'2012-09-09 12:37:29',NULL),(3,4,'2012-09-09 12:37:29',NULL),(4,4,'2012-09-09 12:37:30',NULL),(5,4,'2012-09-09 12:37:30',NULL),(6,4,'2012-09-09 12:37:30',NULL),(7,4,'2012-09-09 12:37:31',NULL),(8,4,'2012-09-09 12:37:31',NULL),(9,4,'2012-09-09 12:37:31',NULL),(11,4,'2012-09-09 12:37:32',NULL),(12,4,'2012-09-09 12:37:32',NULL),(13,4,'2012-09-09 12:37:33',NULL),(14,4,'2012-09-09 12:37:33',NULL),(15,4,'2012-09-09 12:37:33',NULL),(16,4,'2012-09-09 12:37:34',NULL),(17,4,'2012-09-09 12:37:34',NULL),(18,4,'2012-09-09 12:37:34',NULL),(19,4,'2012-09-09 12:37:35',NULL),(20,4,'2012-09-09 12:37:35',NULL),(21,4,'2012-09-09 12:37:36',NULL),(22,4,'2012-09-09 12:37:40',NULL),(23,4,'2012-09-09 12:37:40',NULL),(24,4,'2012-09-09 12:37:41',NULL),(25,4,'2012-09-09 12:37:42',NULL),(26,4,'2012-09-09 12:37:42',NULL),(27,4,'2012-09-09 12:37:43',NULL),(28,4,'2012-09-09 12:37:44',NULL),(29,4,'2012-09-09 12:37:45',NULL),(30,4,'2012-09-09 12:37:45',NULL),(31,4,'2012-09-09 12:37:46',NULL),(32,4,'2012-09-09 12:37:46',NULL),(33,4,'2012-09-09 12:37:47',NULL),(34,4,'2012-09-09 12:37:48',NULL),(35,4,'2012-09-09 12:37:49',NULL),(36,4,'2012-09-09 12:37:50',NULL),(1,1,'2012-09-09 12:42:57',NULL),(3,5,'2012-09-09 12:38:12',NULL),(4,5,'2012-09-09 12:38:13',NULL),(5,5,'2012-09-09 12:38:14',NULL),(6,5,'2012-09-09 12:38:14',NULL),(7,5,'2012-09-09 12:38:15',NULL),(8,5,'2012-09-09 12:38:16',NULL),(9,5,'2012-09-09 12:38:17',NULL),(11,5,'2012-09-09 12:38:19',NULL),(12,5,'2012-09-09 12:38:22',NULL),(13,5,'2012-09-09 12:38:24',NULL),(14,5,'2012-09-09 12:38:29',NULL),(15,5,'2012-09-09 12:38:35',NULL),(16,5,'2012-09-09 12:38:38',NULL),(17,5,'2012-09-09 12:38:38',NULL),(18,5,'2012-09-09 12:38:39',NULL),(19,5,'2012-09-09 12:38:40',NULL),(20,5,'2012-09-09 12:38:40',NULL),(21,5,'2012-09-09 12:38:41',NULL),(22,5,'2012-09-09 12:38:41',NULL),(23,6,'2012-09-09 12:37:05',NULL),(24,6,'2012-09-09 12:37:05',NULL),(25,6,'2012-09-09 12:37:04',NULL),(26,6,'2012-09-09 12:37:01',NULL),(27,6,'2012-09-09 12:37:01',NULL),(28,6,'2012-09-09 12:36:59',NULL),(29,6,'2012-09-09 12:36:59',NULL),(30,6,'2012-09-09 12:36:58',NULL),(31,6,'2012-09-09 13:11:36',NULL),(32,6,'2012-09-09 12:36:58',NULL),(34,6,'2012-09-09 12:36:57',NULL),(2,6,'2012-09-09 12:43:49',NULL),(3,6,'2012-09-09 12:43:50',NULL),(19,6,'2012-09-09 12:43:50',NULL),(5,6,'2012-09-09 12:43:50',NULL),(7,6,'2012-09-09 12:43:50',NULL),(8,6,'2012-09-09 12:43:50',NULL),(13,6,'2012-09-09 12:43:50',NULL),(9,6,'2012-09-09 12:43:50',NULL),(11,6,'2012-09-09 13:24:27',NULL),(24,7,'2012-09-09 14:08:52',NULL),(25,7,'2012-09-09 14:08:52',NULL),(26,7,'2012-09-09 14:08:52',NULL),(27,7,'2012-09-09 14:08:52',NULL),(28,7,'2012-09-09 14:08:52',NULL),(29,7,'2012-09-09 14:08:52',NULL),(30,7,'2012-09-09 14:08:52',NULL),(31,7,'2012-09-09 14:08:52',NULL),(32,7,'2012-09-09 14:08:52',NULL),(34,7,'2012-09-09 14:08:52',NULL),(2,7,'2012-09-09 14:08:52',NULL),(3,7,'2012-09-09 14:08:52',NULL),(19,7,'2012-09-09 14:08:52',NULL),(5,7,'2012-09-09 14:08:52',NULL),(8,7,'2012-09-09 14:21:15',NULL),(1,7,'2012-09-09 14:02:23',NULL),(11,7,'2012-09-09 14:11:57',NULL),(1,6,'2012-09-09 14:12:08',NULL),(13,7,'2012-09-09 14:21:23',NULL),(37,1,'2012-10-23 16:30:06',NULL),(37,2,'2012-10-23 16:30:34',NULL),(37,4,'2012-10-23 16:32:07',NULL),(38,1,'2012-10-25 16:52:23',NULL),(38,2,'2012-10-25 16:53:03',NULL),(38,4,'2012-10-25 16:53:18',NULL),(4,1,'2012-11-03 10:47:32',NULL),(39,1,'2013-10-12 02:57:19',NULL),(39,2,'2013-10-12 02:58:45',NULL),(40,1,'2014-01-01 02:32:01',NULL),(40,2,'2014-01-01 02:32:27',NULL),(41,1,'2016-09-01 10:13:17',NULL),(41,2,'2016-09-01 10:13:34',NULL),(41,3,'2016-09-01 10:13:42',NULL),(41,4,'2016-09-01 10:13:48',NULL),(41,5,'2016-09-01 10:13:55',NULL),(41,6,'2016-09-01 10:14:14',NULL),(41,7,'2016-09-01 10:14:22',NULL),(42,7,'2016-09-01 10:17:59',NULL),(42,6,'2016-09-01 10:18:15',NULL),(42,4,'2016-09-01 10:18:22',NULL),(42,1,'2016-09-01 10:18:27',NULL),(42,2,'2016-09-01 10:18:33',NULL),(42,3,'2016-09-01 10:18:41',NULL);
/*!40000 ALTER TABLE `sch_ug_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_user_groups`
--

DROP TABLE IF EXISTS `sch_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_user_groups` (
  `ug_id` int(11) NOT NULL AUTO_INCREMENT,
  `ug_title` varchar(255) DEFAULT NULL,
  `ug_pid` int(11) DEFAULT NULL,
  `welcome_message` text,
  `ug_code` varchar(20) DEFAULT NULL,
  `ug_domain` int(3) DEFAULT NULL,
  PRIMARY KEY (`ug_id`),
  KEY `ug_domain` (`ug_domain`),
  CONSTRAINT `sch_user_groups_ibfk_1` FOREIGN KEY (`ug_domain`) REFERENCES `nal_domains` (`domain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_user_groups`
--

LOCK TABLES `sch_user_groups` WRITE;
/*!40000 ALTER TABLE `sch_user_groups` DISABLE KEYS */;
INSERT INTO `sch_user_groups` VALUES (1,'Director',0,'<p>Hello Users,</p><p>Welcome to the Introduction page of School Management System [SMS] for Bonbeach High School.</p><p>The current working versions of SMS application includes the following basic features:<ul><li>View Add a Staff Member \n <a href=\"/schmangmt/EduStaffAction.do?operation=search\">Employees</a> and \n <a href=\"/schmangmt/EduStudentAction.do?operation=search\">Students</a>.\n </li><li><a href=\"/schmangmt/EduStudentReportAction.do?operation=studentProfile\">View</a> Student Profile.</li><li>\n Fees <a href=\"/schmangmt/FeesPaymentAction.do?operation=createSetup\">Payments</a> and \n <a href=\"/schmangmt/FeesPaymentAction.do?operation=receiptSetup\">Receipt.</a>\n </li><li>\n View Previous/Alumni \n <a href=\"/schmangmt/EduStaffAction.do?operation=searchAlumni\">Employees</a> and \n <a href=\"/schmangmt/EduStudentAction.do?operation=searchAlumni\">Students</a>.\n </li>\n <li>\n Marksheet/Greensheet Generation \n <a href=\"/schmangmt/EduStudentReportAction.do?operation=reportList\">Report Card</a> and \n <a href=\"/schmangmt/EduStudentReportAction.do?operation=greenSheet\">Green Sheet</a>.\n </li>\n <li>\n Library Managment System \n <a href=\"/schmangmt/LibraryManagementAction.do?operation=issueSetup\">Book Issue</a> and \n <a href=\"/schmangmt/LibraryManagementAction.do?operation=bookList\">Books Availability</a>.\n </li>\n </ul></p>','EX',1),(2,'Principal',1,'<p>Hello Users,</p><p>Welcome to the Introduction page of School Management System [SMS] for Bonbeach High School.</p><p>The current working versions of SMS application includes the following basic features:<ul><li>View Add a Staff Member \r\n<a href=\"/schmangmt/EduStaffAction.do?operation=search\">Employees</a> or \r\n<a href=\"/schmangmt/EduStudentAction.do?operation=search\">Students</a>.\r\n</li><li><a href=\"/schmangmt/EduStudentReportAction.do?operation=studentProfile\">View</a> Student Profile.</li><li>\r\nFees <a href=\"/schmangmt/FeesPaymentAction.do?operation=createSetup\">Payments</a> and \r\n<a href=\"/schmangmt/FeesPaymentAction.do?operation=receiptSetup\">Receipt.</a>\r\n</li><li>\r\nView Previous/Alumni \r\n<a href=\"/schmangmt/EduStaffAction.do?operation=searchAlumni\">Employees</a> or \r\n<a href=\"/schmangmt/EduStudentAction.do?operation=searchAlumni\">Students</a>.\r\n</li></ul></p>\r\n','RM',1),(3,'Teacher',2,'<p>Hello Users,</p><p>Welcome to the Introduction page of School Management System [SMS] for Bonbeach High School.</p><p>The current working versions of SMS application includes the following basic features:<ul><li>View Add a Staff Member \r\n<a href=\"/schmangmt/EduStaffAction.do?operation=search\">Employees</a> or \r\n<a href=\"/schmangmt/EduStudentAction.do?operation=search\">Students</a>.\r\n</li><li><a href=\"/schmangmt/EduStudentReportAction.do?operation=studentProfile\">View</a> Student Profile.</li><li>\r\nFees <a href=\"/schmangmt/FeesPaymentAction.do?operation=createSetup\">Payments</a> and \r\n<a href=\"/schmangmt/FeesPaymentAction.do?operation=receiptSetup\">Receipt.</a>\r\n</li><li>\r\nView Previous/Alumni \r\n<a href=\"/schmangmt/EduStaffAction.do?operation=searchAlumni\">Employees</a> or \r\n<a href=\"/schmangmt/EduStudentAction.do?operation=searchAlumni\">Students</a>.\r\n</li></ul></p>\r\n','BM',1),(4,'Accountant',3,'<p>Hello Users,</p><p>Welcome to the Introduction page of School Management System [SMS] for Bonbeach High School.</p><p>The current working versions of SMS application includes the following basic features:<ul><li>View Add a Staff Member \r\n<a href=\"/schmangmt/EduStaffAction.do?operation=search\">Employees</a> or \r\n<a href=\"/schmangmt/EduStudentAction.do?operation=search\">Students</a>.\r\n</li><li><a href=\"/schmangmt/EduStudentReportAction.do?operation=studentProfile\">View</a> Student Profile.</li><li>\r\nFees <a href=\"/schmangmt/FeesPaymentAction.do?operation=createSetup\">Payments</a> and \r\n<a href=\"/schmangmt/FeesPaymentAction.do?operation=receiptSetup\">Receipt.</a>\r\n</li><li>\r\nView Previous/Alumni \r\n<a href=\"/schmangmt/EduStaffAction.do?operation=searchAlumni\">Employees</a> or \r\n<a href=\"/schmangmt/EduStudentAction.do?operation=searchAlumni\">Students</a>.\r\n</li></ul></p>\r\n','ASM',1),(5,'Librarian',4,'<p>Hello Users,</p><p>Welcome to the Introduction page of School Management System [SMS] for Bonbeach High School.</p><p>The current working versions of SMS application includes the following basic features:<ul><li>View Add a Staff Member \r\n<a href=\"/schmangmt/EduStaffAction.do?operation=search\">Employees</a> or \r\n<a href=\"/schmangmt/EduStudentAction.do?operation=search\">Students</a>.\r\n</li><li><a href=\"/schmangmt/EduStudentReportAction.do?operation=studentProfile\">View</a> Student Profile.</li><li>\r\nFees <a href=\"/schmangmt/FeesPaymentAction.do?operation=createSetup\">Payments</a> and \r\n<a href=\"/schmangmt/FeesPaymentAction.do?operation=receiptSetup\">Receipt.</a>\r\n</li><li>\r\nView Previous/Alumni \r\n<a href=\"/schmangmt/EduStaffAction.do?operation=searchAlumni\">Employees</a> or \r\n<a href=\"/schmangmt/EduStudentAction.do?operation=searchAlumni\">Students</a>.\r\n</li></ul></p>\r\n','TM',1),(6,'Student',5,'<p>Hello Users,</p><p>Welcome to the Introduction page of School Management System [SMS] for Bonbeach High School.</p><p>The current working versions of SMS application includes the following basic features:<ul><li>View Teacher Details \r\n<a href=\"#\">Teachers</a>.\r\n</li><li><a href=\"/schmangmt/EduStudentReportAction.do?operation=studentProfile\">View</a> Student Reports.</li><li>\r\n<a href=\"/schmangmt/FeesPaymentAction.do?operation=receiptSetup\">Fee Reciept</a> Generation Reports.</li><li>\r\nView Previous/Alumni \r\n<a href=\"/schmangmt/EduStaffAction.do?operation=searchAlumni\">Employees</a> or \r\n<a href=\"/schmangmt/EduStudentAction.do?operation=searchAlumni\">Students</a>.\r\n</li>\r\n<li><a href=\"/schmangmt/EduStudentReportAction.do?operation=reportList\">Report Card</a> Generation.</li>\r\n</ul></p>',NULL,NULL),(7,'Parent',6,'<p>Hello Users,</p><p>Welcome to the Introduction page of School Management System [SMS] for Bonbeach High School.</p><p>The current working versions of SMS application includes the following basic features:<ul><li>View Teacher Details \r\n<a href=\"#\">Teachers</a>.\r\n</li><li><a href=\"#\">View</a> Student Reports.</li><li>\r\n<a href=\"/schmangmt/FeesPaymentAction.do?operation=receiptSetup\">Fee Reciept</a> Generation Reports.</li><li>\r\nView Previous/Alumni \r\n<a href=\"/schmangmt/EduStaffAction.do?operation=searchAlumni\">Employees</a> or \r\n<a href=\"/schmangmt/EduStudentAction.do?operation=searchAlumni\">Students</a>.\r\n</li>\r\n<li><a href=\"#\">Report Card</a> Generation.</li>\r\n</ul></p>',NULL,NULL);
/*!40000 ALTER TABLE `sch_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sch_users`
--

DROP TABLE IF EXISTS `sch_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sch_users` (
  `User_id` int(20) NOT NULL AUTO_INCREMENT,
  `User_first_name` varchar(255) NOT NULL,
  `middle_initial` varchar(255) DEFAULT NULL,
  `User_last_name` varchar(255) NOT NULL,
  `User_phone` varchar(255) DEFAULT NULL,
  `Company` varchar(255) NOT NULL,
  `Fax` varchar(255) DEFAULT NULL,
  `email` varchar(1000) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `user_deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`User_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sch_users`
--

LOCK TABLES `sch_users` WRITE;
/*!40000 ALTER TABLE `sch_users` DISABLE KEYS */;
INSERT INTO `sch_users` VALUES (1,'System','U','Administrator','12345678','Nikhil Adhesives','12345678','sa@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(2,'Jayesh','','Raythatha','09320233356','Nikhil Adhesives ','','vinod.singh@zekisolutions.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(3,'Vaishali','','Sanghavi','09320691974','Nikhil Adhesives ','','vaishali.sanghavi@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(4,'Amar','',' Kataria','09352225050','Nikhil Adhesives ','','knp.vinod@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(5,'Neeraj','','Mishra','09368521254','Nikhil Adhesives ','','satish.vishwakarama@zekisolutions.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(6,'Arup','','Singh','09337033256','Nikhil Adhesives ','','arup.singh@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(7,'C','K','Sinha','09330051256','Nikhil Adhesives ','','c.k.sinha@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(8,'Dharmesh ','','Pandya','09327069425','Nikhil Adhesives ','','dharmesh.pandya@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(9,'Rajesh','','Sharma','09314308256','Nikhil Adhesives ','','rajesh.sharma@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(10,'Siddharth','','Sharma','09314305256','Nikhil Adhesives ','','siddharth.sharma@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(11,'Lalit ','','Jain','09314314256','Nikhil Adhesives ','','lalit.jain@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(12,'Jugal ','K','Gupta','09314311256','Nikhil Adhesives ','','jugal.gupta@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(13,'Mohsin','','Khan','09314315256','Nikhil Adhesives ','','mohsin.khan@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(14,'Vinay ','S','Yadav','09314313256','Nikhil Adhesives ','','vinay.yadav@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(15,'Vishal','','Sharma','09389551253','Nikhil Adhesives ','','vishal.sharma@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(16,'Suresh ','','Awasthi','09389445125','Nikhil Adhesives ','','suresh.awasthi@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(17,'Vijay ','','Shukla','09311304524','Nikhil Adhesives ','','vijay.shukla@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(18,'Atul','K','Tiwari','09319971254','Nikhil Adhesives ','','atul.tiwari@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',1),(19,'Sonu','','Trivedi','09319122351','Nikhil Adhesives ','','sonu.trivedi@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(20,'Shailendra ','K','Tripathi','09389423125','Nikhil Adhesives ','','shailendra.tripathi@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(21,'Deepak','','Mishra','09336497209','Nikhil Adhesives ','','deepak.mishra@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(22,'Arun ','K','Tripathi','09389420125','Nikhil Adhesives ','','arun.tripathi@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(23,'Sachin','','Jaiswal','09389418125','Nikhil Adhesives ','','sachin.jaiswal@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(24,'Ghanshyam ','','Sharma','09327106256','Nikhil Adhesives ','','ghanshyam.sharma@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(25,'Kunjan ','','Rupareliya','09327139256','Nikhil Adhesives ','','kunjan.rupareliya@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(26,'Ritesh','','Bhagat','09386251315','Nikhil Adhesives ','','ritesh.bhagat@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(27,'Bijoy','','Paul','09435107473','Nikhil Adhesives ','','bijoy.paul@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(28,'Mukesh ','','Bisen','09302776125','Nikhil Adhesives ','','mukesh.bisen@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(29,'Kapil ','','Gogna','09316261254','Nikhil Adhesives ','','kapil.gogna@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(30,'Rajkumar','K','Chakraborty','09330042256','Nikhil Adhesives ','','rajkumar.chakraborty@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(31,'Self Arun','','Tripathi','09389420125','Nikhil Adhesives','','selfarun.tripathi@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(32,'test','a','tst','1212','aa','1212','test123@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',1),(33,'Self Dharmesh','','Pandya','09327069425','Nikhil Adhesives','','selfdharmesh.pandya@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(34,'Self Siddharth','','Sharma','09314305256','Nikhil Adhesives','','selfsiddharth.sharma@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(35,'Self Rajesh','','Sharma','09314308256','Nikhil Adhesives','','selfrajesh.sharma@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(36,'Self Suresh','','Awasthi','09389445125','Nikhil Adhesives','','selfsuresh.awasthi@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(37,'Self Vishal','','Sharma','09389551253','Nikhil Adhesives','','selfvishal.sharma@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(38,'Rajendra','','Mishra','09337030256','Nikhil Adhesives','','rajendra.mishra@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(39,'Self arup','','Singh','09337033256','Nikhil Adhesives','','selfarup.singh@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(40,'Sanjay ','','Deshmukh','09370256062','Nikhil Adhesives','','sanjay.deshmukh@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(41,'Self sanjay','','Deshmukh','09370256062','Nikhil Adhesives','','selfsanjay.deshmukh@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(42,'Sanjay ','','Rathore','09302774425','Nikhil Adhesives','','sanjay.rathore@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(43,'Shivkumar','','Patidar','09407337574','Nikhil Adhesives','','shinkumar.patidar@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(44,'Jayendra','','Dubey','09425403528','Nikhil Adhesives','','jayendra.dubey@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(45,'Selfsanjay ','','Rathore','09302774425','Nikhil Adhesives','','selfsanjay.rathore@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(46,'Rashid','','Mohammed','09318531254','Nikhil Adhesives','','rashid.mohammed@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(47,'vinod','s','bhadouria','9867891771','Zeki Solutions ltd.','787656788','vinodbhadouria@yahoo.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(48,'satish','k','vishwakarama','9769505010','Zeki Solutions ltd.','9867891771','vishwakarama.satish@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(49,'testexe','','exe','1212','nal','','testexe@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(50,'tanaji','','s','000','nal','','tanaji@nikhiladhesives.com','5f4dcc3b5aa765d61d8327deb882cf99',0),(51,'Vinod ','Singh','Bhadouria','9867891771','Accenture','','vinod.bhadouria@accenture.com','9ee930a81e59a99a5dfa12c1cd252f49',0);
/*!40000 ALTER TABLE `sch_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'schmangdb'
--
/*!50003 DROP PROCEDURE IF EXISTS `assignBookToUser_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `assignBookToUser_sp`(IN userId varchar(50), IN userType varchar(1),IN bookId varchar(20),IN bmId int)
begin
insert into library_book_issue(book_id,user_id,issue_date,due_date,user_type,return_status) values(bookId,userId,now(),date_add(now(),interval 15 day),userType,'N');
update library_book_master set assigned_count= (IFNULL(assigned_count,0)+1)  where bm_id= bmId;
update library_copy_of_books set assigned_status= 'Y'  where book_id= bookId;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `backup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `backup`(IN class_id varchar(50),IN  searchBysessionkey varchar(50))
begin
declare session_key varchar(50);
IF(searchBysessionkey<>'%%' )
Then
set @session_key=searchBysessionkey ;
 ELSE
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;
END IF;
 select  stud.student_id,stud.first_name,stud.middle_name,stud.last_name,ssd.subject_id,ssd.subject_name, ssmd.mark_outof, ssmd.mark_obtained,ssrd.status,ssrd.total_mark_outof,ssrd.total_mark_obtained,ssrd.division,ssrd.percentage,scd.class_name from
sch_student_mark_detail ssmd,sch_subject_detail ssd,sch_student_detail stud,sch_student_result_detail ssrd,sch_class_detail scd
where ssmd.subject_id = ssd.subject_id
and stud.student_id=ssmd.student_id
and ssrd.student_id =ssmd.student_id
and  ssmd.session_id=ssrd.session_id
and ssmd.class_id=ssrd.class_id
and  ssmd.class_id = class_id
and scd.class_id=ssmd.class_id
and  ssmd.session_id = @session_key ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `change_password_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `change_password_sp`(userid varchar(50),strnewpassword varchar(100),ug_id int,strpassword varchar(100))
BEGIN
if(ug_id = 1 or ug_id = 2  or ug_id = 3 or ug_id = 4 or ug_id = 5 )
THEN
update sch_staff_detail set password = strnewpassword where emp_id = userid and password=strpassword;
ELSE if(ug_id=6 )
then
update sch_student_detail set password = strnewpassword where student_id = userid and password=strpassword;
ELSE
update sch_parent_detail set password = strnewpassword where parent_id = userid and password=strpassword;
END IF;
END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `check_password_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `check_password_sp`(userid varchar(50),strpassword varchar(100),ug_id int)
BEGIN
if(ug_id = 1 or ug_id = 2  or ug_id = 3 or ug_id = 4 or ug_id = 5 )
THEN
select count(*) from sch_staff_detail where emp_id = userid and password=strpassword;
ELSE if(ug_id=6 )
then
select count(*) from sch_student_detail where student_id = userid and password=strpassword;
ELSE
select count(*) from sch_parent_detail where parent_id = userid and password=strpassword;
END IF;
END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `emp_temp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `emp_temp`(IN a int,IN b int)
begin
set @a=a; set @b=b;
IF (a is not null)
then
set @a=2;
end if;
PREPARE STMT FROM 'select * from sch_staff_designation limit ? ,?';
EXECUTE STMT USING @a, @b;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `exitformData_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `exitformData_sp`(IN userId varchar(11))
begin
select first_name,middle_name,last_name,admission_date,father_name,address,class_name,ssd.student_id from sch_student_detail ssd,sch_parent_detail spd,sch_student_class_detail sscd,sch_class_detail scd 
where ssd.student_id = spd.student_id and ssd.student_id = sscd.student_id and sscd.class_id = scd.class_id and ssd.user_id =userId and ssd.status='D';
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAssignedbookBybookId_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAssignedbookBybookId_sp`(IN bookId varchar(20))
begin
select ssd.first_name,ssd.middle_name,ssd.last_name,ssd.user_id,ssd.email_id,ssd.contact_no,ssd.status status, lbm.book_name,lbt.book_type_name,lbi.book_id,lbi.issue_date,lbi.due_date,rack_name,'Student' userType
from library_book_issue lbi,library_copy_of_books lcob,library_book_master lbm,sch_student_detail ssd,library_rack_info lri,library_book_type lbt
where lbi.return_status='N' and lcob.book_id=lbi.book_id and lcob.bm_id=lbm.bm_id and lbm.rack_id = lri.rack_id and lbm.book_type=lbt.book_type_id and user_type='S' and lbi.user_id = ssd.user_id and lbi.book_id=bookId
union all
select ssd.first_name,ssd.middle_name,ssd.last_name,ssd.user_id,ssd.email_id,ssd.contact_no,ssd.emp_status status, lbm.book_name,lbt.book_type_name,lbi.book_id,lbi.issue_date,lbi.due_date,rack_name,'Teacher' userType
from library_book_issue lbi,library_copy_of_books lcob,library_book_master lbm,sch_staff_detail ssd,library_rack_info lri,library_book_type lbt
where lbi.return_status='N' and lcob.book_id=lbi.book_id and lcob.bm_id=lbm.bm_id and lbm.rack_id = lri.rack_id and lbm.book_type=lbt.book_type_id and user_type='T' and lbi.user_id = ssd.user_id  and lbi.book_id=bookId;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAssignedbooksList_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAssignedbooksList_sp`()
begin
select ssd.first_name,ssd.middle_name,ssd.last_name,ssd.user_id, lbm.book_name,lbi.book_id,lbi.issue_date,lbi.due_date,'Student' userType
from library_book_issue lbi,library_copy_of_books lcob,library_book_master lbm,sch_student_detail ssd
where lbi.return_status='N' and lcob.book_id=lbi.book_id and lcob.bm_id=lbm.bm_id and user_type='S' and lbi.user_id = ssd.user_id
union all
select ssd.first_name,ssd.middle_name,ssd.last_name,ssd.user_id, lbm.book_name,lbi.book_id,lbi.issue_date,lbi.due_date,'Teacher' userType
from library_book_issue lbi,library_copy_of_books lcob,library_book_master lbm,sch_staff_detail ssd
where lbi.return_status='N' and lcob.book_id=lbi.book_id and lcob.bm_id=lbm.bm_id and user_type='T' and lbi.user_id = ssd.user_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAvailableBookcountandtype_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAvailableBookcountandtype_sp`(IN bookName varchar(50))
begin
select (lbm.number_of_copy-IFNULL(assigned_count,0)) availability_count,lri.rack_name,lbt.book_type_name,lbm.bm_id from library_book_master lbm ,library_book_type lbt,library_rack_info lri where lbm.rack_id=lri.rack_id and lbm.book_type=lbt.book_type_id and book_name=bookName;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAvailableBooksBybookName` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAvailableBooksBybookName`(IN bookName varchar(50))
begin
select book_copy_id,book_id from library_copy_of_books lcob,library_book_master lbm where lcob.bm_id=lbm.bm_id and lbm.book_name=bookName and lcob.assigned_status='N';
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getClass_Detail_By_dept_id_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getClass_Detail_By_dept_id_sp`(IN deptId int)
BEGIN
select sdd.dept_id,scd.class_id,sdd.dept_name,scd.class_name,section from
sch_department_detail sdd, sch_dept_class_relation sdcr,sch_class_detail scd where
sdd.dept_id=sdcr.dept_id and sdcr.class_id = scd.class_id and sdd.dept_id=deptId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getReturnedBookList_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getReturnedBookList_sp`()
begin
select ssd.first_name,ssd.middle_name,ssd.last_name,ssd.user_id, lbm.book_name,lbi.book_id,lbi.issue_date,lbi.due_date,lbi.return_date,'Student' userType
from library_book_issue lbi,library_copy_of_books lcob,library_book_master lbm,sch_student_detail ssd
where lbi.return_status='Y' and lcob.book_id=lbi.book_id and lcob.bm_id=lbm.bm_id and user_type='S' and lbi.user_id = ssd.user_id
union all
select ssd.first_name,ssd.middle_name,ssd.last_name,ssd.user_id, lbm.book_name,lbi.book_id,lbi.issue_date,lbi.due_date,lbi.return_date,'Teacher' userType
from library_book_issue lbi,library_copy_of_books lcob,library_book_master lbm,sch_staff_detail ssd
where lbi.return_status='Y' and lcob.book_id=lbi.book_id and lcob.bm_id=lbm.bm_id and user_type='T' and lbi.user_id = ssd.user_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getStudentDataByUserId_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getStudentDataByUserId_sp`(IN userId varchar(20),IN searchBysessionkey varchar(1))
begin
declare session_key varchar(50);
IF(searchBysessionkey<>'' )
Then
	set @session_key=searchBysessionkey ;
 ELSE
select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 	set @session_key=session_key ;
END IF;

select ssd.first_name,ssd.middle_name,ssd.last_name,ssd.email_id,scd.class_name,scd.section,ssd.status
from sch_student_detail ssd,sch_student_class_detail sscd,sch_class_detail scd
where ssd.student_id=sscd.student_id
and sscd.class_id=scd.class_id
and ssd.status='A'
and sscd.session_id =@session_key
and ssd.user_id = userId;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getSubjectByClass_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getSubjectByClass_sp`(IN deptId varchar(10),IN classId int)
BEGIN
declare deptid int;
IF(deptId<>'' )
Then
	set @deptid=deptId;
 ELSE
	 select dept_id into deptid from  sch_dept_class_relation where class_id=classId;
 	set @deptid=deptid ;
END IF;


select  distinct scsr.subject_id ,ssd.subject_name from
sch_department_detail sdd, sch_dept_class_relation sdcr,sch_class_detail scd,sch_class_subject_relation scsr,sch_subject_detail ssd where
sdd.dept_id=sdcr.dept_id and sdcr.class_id = scd.class_id and sdcr.class_id =  scsr.class_id and ssd.subject_id = scsr.subject_id and sdcr.class_id=classId  and sdd.dept_id=@deptid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getSubjectByDepartment_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getSubjectByDepartment_sp`(IN deptId int)
BEGIN
select  distinct scsr.subject_id ,ssd.subject_name from
sch_department_detail sdd, sch_dept_class_relation sdcr,sch_class_detail scd,sch_class_subject_relation scsr,sch_subject_detail ssd where
sdd.dept_id=sdcr.dept_id and sdcr.class_id = scd.class_id and sdcr.class_id =  scsr.class_id and ssd.subject_id = scsr.subject_id and sdd.dept_id=deptId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getTeacherDataByUserId_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getTeacherDataByUserId_sp`(IN userId varchar(50),IN searchBysessionkey varchar(1))
begin
declare session_key varchar(50);
IF(searchBysessionkey<>'' )
Then
	set @session_key=searchBysessionkey ;
 ELSE
select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 	set @session_key=session_key ;
END IF;

select ssd.first_name,ssd.middle_name,ssd.last_name,ssd.email_id,sqd.qualification_title,ssd.contact_no
from sch_staff_detail ssd,sch_qualification_detail sqd
where ssd.qualification_id=sqd.qualification_id
and ssd.user_id = userId;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_AssignedSubjectForStudentbysessionId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_AssignedSubjectForStudentbysessionId`(IN studentId int,IN class_Id int,IN searchBysessionkey varchar(50))
BEGIN
declare session_key varchar(50);
IF(searchBysessionkey<>'' )
Then
	set @session_key=searchBysessionkey ;
 ELSE
	 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 	set @session_key=session_key ;
END IF;

select distinct sssd.subject_id,ssd.subject_name
from sch_subject_detail ssd,sch_student_subject_detail sssd,sch_student_detail stud,sch_student_class_detail sscd,sch_class_detail scd
where ssd.subject_id = sssd.subject_id and stud.student_id = sssd.student_id and sscd.student_id = stud.student_id and scd.class_id=sscd.class_id  and sscd.class_id=class_Id and  sssd.class_id=sscd.class_id and stud.student_id=studentId and sscd.session_id like @session_key order by ssd.subject_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_fee_collation_By_userId_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_fee_collation_By_userId_sp`(IN studentId int,IN searchBysessionkey varchar(50),IN receipt_no int)
BEGIN
declare session_key varchar(50);
IF(searchBysessionkey<>'' )
Then
	set @session_key=searchBysessionkey ;
 ELSE
	 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 	set @session_key=session_key ;
END IF;
select sfd.receipt_no, ssd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.contact_no,sfd.fee_amount,ssd.address,ssd.dateofbirth,sfd.class_id,scd.class_name,scd.section,sfd.payment_date,sdd.dept_name,spd.father_name,ssd.birth_day,ssd.birth_month,ssd.birth_year
from sch_student_detail ssd ,sch_fee_detail sfd,sch_parent_detail spd,sch_class_detail scd,sch_dept_class_relation sdcr,sch_department_detail sdd
where ssd.student_id=sfd.admission_no and ssd.student_id=spd.student_id and sfd.class_id=scd.class_id and sfd.class_id=sdcr.class_id  and sdcr.dept_id=sdd.dept_id and ssd.status='A' and ssd.student_id = studentId and sfd.receipt_no=receipt_no and sfd.session_id like @session_key;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_registrationDetailBySession_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_registrationDetailBySession_sp`(IN registration_no int,IN searchBysessionkey varchar(50),IN reg_status varchar(1))
BEGIN
declare session_key varchar(50);
IF(searchBysessionkey<>'' )
Then
	set @session_key=searchBysessionkey ;
 ELSE
	 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 	set @session_key=session_key ;
END IF;
select ssd.registration_no,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.email_id,ssd.registration_date,ssd.contact_no,
ssd.address,ssd.dateofbirth,ssd.previous_edu_center,ssd.gender,
class_name,spsd.qualification_title stud_pre_class,spd.father_name,spd.mother_name,sqd.qualification_title father_qualification,
spd.email_id parentemail_id,ssd.class_id,ssd.preinstclass_id,spd.father_higher_qualification_id,ssd.registration_fee,concessionamt,comments,ssd.dept_id,sdd.dept_name,sss.session_year,scd.registration_fee regfees
from sch_registration_detail ssd ,sch_registration_parent_detail spd,sch_class_detail scd,sch_prev_student_detail spsd,sch_qualification_detail sqd,sch_department_detail sdd,sch_student_session sss
where  ssd.registration_no=spd.registration_no and ssd.class_id=scd.class_id   and spsd.qualification_id = ssd.preinstclass_id and sqd.qualification_id=spd.father_higher_qualification_id and ssd.dept_id = sdd.dept_id and sss.session_id=ssd.session_id
and ssd.reg_status=reg_status and ssd.registration_no = registration_no  and ssd.session_id = @session_key;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_studentDetailBySession_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_studentDetailBySession_sp`(IN studentId int,IN searchBysessionkey varchar(50))
BEGIN
declare session_key varchar(50);
IF(searchBysessionkey<>'' )
Then
	set @session_key=searchBysessionkey ;
 ELSE
	 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 	set @session_key=session_key ;
END IF;
select ssd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.email_id,ssd.admission_date,ssd.contact_no,ssd.address,ssd.dateofbirth,ssd.previous_edu_center,ssd.gender,admission_fee,
concession_amount,comment,class_id,preinstclass_id,spd.father_name,spd.mother_name,spd.father_higher_qualification_id,spd.email_id parentemail_id,ssd.birth_day,ssd.birth_month,ssd.birth_year,sscd.dept_id
from sch_student_detail ssd ,sch_student_class_detail sscd,sch_student_admissionfee_collection ssac,sch_parent_detail spd
where ssd.student_id=sscd.student_id and ssd.student_id=ssac.student_id and ssd.student_id=spd.student_id  and ssd.status='A' and ssd.student_id = studentId and sscd.session_id like @session_key;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_studentDetailByUserSession_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_studentDetailByUserSession_sp`(IN studentId int,IN searchBysessionkey varchar(50))
BEGIN
declare session_key varchar(50);
IF(searchBysessionkey<>'' )
Then
	set @session_key=searchBysessionkey ;
 ELSE
	 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 	set @session_key=session_key ;
END IF;
select ssd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.dateofbirth,sscd.class_id,spd.father_name,ssd.birth_day,ssd.birth_month,ssd.birth_year,sscd.dept_id,class_name,sdt.dept_name,sscd.session_id,sss.session_year,ssd.user_id,ssd.admission_date,ssd.contact_no,ssd.email_id,address
from sch_student_detail ssd ,sch_student_class_detail sscd,sch_parent_detail spd,sch_class_detail scd,sch_department_detail sdt,sch_student_session sss
where ssd.student_id=sscd.student_id  and ssd.student_id=spd.student_id  and ssd.status='A' and scd.class_id=sscd.class_id and sdt.dept_id=sscd.dept_id and sss.session_id=sscd.session_id and ssd.student_id = studentId and sscd.session_id like @session_key;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_studentUserByUser_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_studentUserByUser_id`(IN studentId int,IN Stud_status varchar(1))
BEGIN
select ssd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.dateofbirth,spd.father_name,ssd.birth_day,ssd.birth_month,ssd.birth_year,ssd.user_id,ssd.admission_date,ssd.contact_no,ssd.email_id,address
from sch_student_detail ssd ,sch_parent_detail spd
where ssd.student_id=spd.student_id  and ssd.status=Stud_status and  ssd.student_id = studentId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_registration_detail_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_registration_detail_sp`(IN first_name varchar(50),IN middle_name varchar(50),IN last_name varchar(50),IN father_name varchar(50),IN email_id varchar(50),IN registration_date varchar(10),IN address varchar(1000),IN contact_no varchar(10),IN reg_status varchar(1),IN gender varchar(10),IN classid INT,IN dateofbirth varchar(10),IN mother_name varchar(50),IN parent_qualificationId int,IN parentEmailId varchar(50),IN parentpassword varchar(50),IN stud_pre_classid varchar(10),IN preInstitudeName varchar(50),IN registration_fee int,IN concessionamt int,IN comment varchar(100),IN deptId int)
BEGIN
declare maxregistration_no  int;
declare sessionId int;
select session_id into sessionId from  sch_student_session where  CURDATE()  between session_start and session_end;
select  IFNULL(max(registration_no),0 ) into maxregistration_no from sch_registration_detail;
set @maxregistration_no=(maxregistration_no+1);
insert into sch_registration_detail(registration_no,first_name,middle_name,last_name,father_name,email_id,registration_date,address,contact_no,reg_status,gender,dateofbirth,class_id,session_id,preinstclass_id,previous_edu_center,registration_fee,concessionamt,comments,dept_id)
values(@maxregistration_no,first_name,middle_name,last_name,father_name,email_id,registration_date,address,contact_no,reg_status,gender,dateofbirth,classid,sessionId,stud_pre_classid ,preInstitudeName,registration_fee,concessionamt ,comment,deptId);
insert into sch_registration_parent_detail(registration_no,father_name,mother_name,father_higher_qualification_id,mather_higher_qualification_id,email_id,password )
values(@maxregistration_no,father_name,mother_name,parent_qualificationId,parent_qualificationId,parentEmailId ,parentpassword );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_staff_detail_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_staff_detail_sp`(IN first_name varchar(50),IN middle_name varchar(50),IN last_name varchar(50),IN email_id varchar(50),IN strpassword varchar(50),IN salary varchar(10),IN address varchar(50),IN contact_no varchar(10),IN strqualification_id INT,IN emp_status varchar(1),IN previous_exp varchar(50),IN joining_date datetime,IN designation_id INT,IN deptId int,IN userid varchar(8))
BEGIN
declare maxemp_id  int;
declare loginid  varchar(8);
select max(emp_id) into maxemp_id from sch_staff_detail;
set @maxemp_id=(maxemp_id+1);
IF(@maxemp_id>=1 and @maxemp_id<=9)
then
SET @loginid=concat(userid,'000',@maxemp_id);
ELSEIF (@maxemp_id>=10 and @maxemp_id<=99)
then
SET @loginid=concat(userid,'00',@maxemp_id);
ELSEIF ( @maxemp_id>=100 and @maxemp_id<=999)
then
SET @loginid=concat(userid,'0',@maxemp_id);
else
SET @loginid=concat(userid,'',@maxemp_id);
end if;
insert into sch_staff_detail(first_name,middle_name,last_name,email_id,password,salary,address,contact_no,qualification_id,emp_status,previous_exp,joining_date,designation,dept_id,user_id )
values(first_name,middle_name,last_name,email_id,strpassword,salary,address,contact_no,strqualification_id,emp_status,previous_exp,joining_date,designation_id,deptId,@loginid);
delete from sch_staff_heirarchy where user_id =@maxemp_id;
insert into sch_staff_heirarchy(user_id,ug_id) values(@maxemp_id,designation_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_student_detail_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_student_detail_sp`(IN first_name varchar(50),IN middle_name varchar(50),IN last_name varchar(50),IN strpassword varchar(50),IN email_id varchar(50),IN admission_date varchar(10),IN address varchar(1000),IN contact_no varchar(10),IN stud_status varchar(1),IN previous_edu_center varchar(50),IN gender varchar(6),IN father_name varchar(50),IN mother_name varchar(50),IN father_higher_qualification_id varchar(50),IN parent_email_id varchar(50),IN parent_password varchar(50),IN classid INT,IN preinstclass_id varchar(10),IN admissionfee INT,IN concessionamt INT,IN comment varchar(100),IN dateofbirth datetime,IN birth_day varchar(2),IN birth_month varchar(2),IN birth_year varchar(4),IN deptId int,IN userid varchar(8),IN parentuser varchar(8),IN sessionId varchar(10))
BEGIN
declare maxstudent_id  int;
declare loginid  varchar(8);
declare maxparent_id  int;
declare parentloginid  varchar(8);
declare session_key varchar(10);
IF(sessionId<>'' )
Then
	set @session_key=sessionId;
 ELSE
	 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 	set @session_key=session_key ;
END IF;
select IFNULL(max(student_id),0 ) into maxstudent_id from sch_student_detail;
set @maxstudent_id=(maxstudent_id+1);
IF(@maxstudent_id>=1 and @maxstudent_id<=9)
then
SET @loginid=concat(userid,'000',@maxstudent_id);
ELSEIF (@maxstudent_id>=10 and @maxstudent_id<=99)
then
SET @loginid=concat(userid,'00',@maxstudent_id);
ELSEIF ( @maxstudent_id>=100 and @maxstudent_id<=999)
then
SET @loginid=concat(userid,'0',@maxstudent_id );
else
SET @loginid=concat(userid,'',@maxstudent_id );
end if;
insert into sch_student_detail(student_id,first_name,middle_name,last_name,email_id,password,admission_date,address,contact_no,status,previous_edu_center,gender,dateofbirth,birth_day,birth_month,birth_year,user_id )
values(@maxstudent_id,first_name,middle_name,last_name,email_id,strpassword,admission_date,address,contact_no,stud_status,previous_edu_center,gender,dateofbirth,birth_day,birth_month,birth_year,@loginid);
insert into sch_student_class_detail(student_id,class_id,preinstclass_id,session_id,dept_id) values(@maxstudent_id,classid,preinstclass_id,@session_key,deptId);
insert into sch_student_admissionfee_collection (student_id,session_id,admission_fee,concession_amount,comment) values(@maxstudent_id,@session_key,admissionfee ,concessionamt ,comment ) ;
delete from sch_student_heirarchy where user_id = @maxstudent_id;
insert into sch_student_heirarchy(user_id,ug_id) values( @maxstudent_id,6);
select IFNULL(max(parent_id),0 ) into maxparent_id from sch_parent_detail;
set @maxparent_id=(maxparent_id+1);
IF(@maxparent_id>=1 and @maxparent_id<=9)
then
SET @parentloginid=concat(parentuser,'000',@maxparent_id);
ELSEIF (@maxparent_id>=10 and @maxparent_id<=99)
then
SET @parentloginid=concat(parentuser,'00',@maxparent_id);
ELSEIF ( @maxparent_id>=100 and @maxparent_id<=999)
then
SET @parentloginid=concat(parentuser,'0',@maxparent_id );
else
SET @parentloginid=concat(parentuser,'',@maxparent_id );
end if;
insert into sch_parent_detail(student_id,father_name,mother_name,father_higher_qualification_id,email_id,password,user_id)
values(@maxstudent_id,father_name,mother_name,father_higher_qualification_id,parent_email_id,parent_password,@parentloginid);
delete from sch_parent_heirarchy where user_id = @maxparent_id;
insert into sch_parent_heirarchy(user_id,ug_id) values( @maxparent_id,7);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insert_student_new_session_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_student_new_session_sp`(IN studentId INT,IN classId INT,IN sessionId INT,IN deptId INT, IN admission_fee int,IN concession_amount int,IN commentMsg varchar(100))
BEGIN
declare tbcount INT;
declare preclassId INT;
select count(1) into tbcount from sch_student_class_detail where  student_id=studentId and class_id=classId and session_id=sessionId;
select class_id into preclassId from sch_student_class_detail where  student_id=studentId and session_id=(sessionId-1);
if(tbcount=0 )
then
insert into sch_student_class_detail (student_id,class_id,preinstclass_id,session_id,dept_id) values(studentId,classId,preclassId,sessionId,deptId);
insert into sch_student_admissionfee_collection(student_id,session_id,admission_fee,concession_amount,comment) values (studentId,sessionId,admission_fee,concession_amount,commentMsg);
end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `returnBookByuser_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `returnBookByuser_sp`(IN bookId varchar(20))
begin
	update library_book_issue set return_date=now(),return_status='Y' where book_id=bookId;
	update library_copy_of_books set assigned_status='N' where book_id=bookId;
        update library_book_master set assigned_count=(assigned_count-1) where bm_id = (select bm_id from library_copy_of_books where book_id=bookId);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sch_fee_payment_insertion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sch_fee_payment_insertion`(IN admission_no int,IN session_id int,IN class_id int,IN fee_amount int,IN payment_date datetime,IN payment_mode varchar(10),IN cheque_no varchar(10) ,IN cheque_date datetime,IN bank  varchar(40))
BEGIN
insert into sch_fee_detail (admission_no,session_id,class_id,fee_amount,payment_date,payment_mode,cheque_no,cheque_date,bank)
values(admission_no,session_id,class_id,fee_amount,payment_date,payment_mode,cheque_no,cheque_date,bank);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sch_green_sheet` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sch_green_sheet`(IN class_id varchar(50),IN  searchBysessionkey varchar(50))
begin
declare session_key varchar(50);
IF(searchBysessionkey<>'%%' )
Then
set @session_key=searchBysessionkey ;
 ELSE
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;
END IF;
 select  stud.student_id,stud.first_name,stud.middle_name,stud.last_name,ssd.subject_id,ssd.subject_name, ssmd.mark_outof, ssmd.mark_obtained,ssrd.status,ssrd.total_mark_outof,ssrd.total_mark_obtained,ssrd.division,ssrd.percentage,scd.class_name from
sch_student_mark_detail ssmd,sch_subject_detail ssd,sch_student_detail stud,sch_student_result_detail ssrd,sch_class_detail scd
where ssmd.subject_id = ssd.subject_id
and stud.student_id=ssmd.student_id
and ssrd.student_id =ssmd.student_id
and  ssmd.session_id=ssrd.session_id
and ssmd.class_id=ssrd.class_id
and  ssmd.class_id = class_id
and scd.class_id=ssmd.class_id
and  ssmd.session_id = @session_key ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sch_regfee_collection` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sch_regfee_collection`(IN registration_fee int, IN concessionamt int,IN comment varchar(100), IN registrationId int)
BEGIN

update sch_registration_detail set registration_fee=registration_fee,concessionamt=concessionamt,comments=comment, reg_status='A' where registration_no = registrationId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sch_studentDataForGreenSheet` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sch_studentDataForGreenSheet`(IN class_id varchar(50),IN  searchBysessionkey varchar(50))
begin
declare session_key varchar(50);
IF(searchBysessionkey<>'%%' )
Then
set @session_key=searchBysessionkey ;
 ELSE
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;
END IF;
select  stud.student_id,stud.first_name,stud.middle_name,stud.last_name,ssrd.status,ssrd.total_mark_outof,ssrd.total_mark_obtained,ssrd.division,ssrd.percentage,scd.class_name,sss.session_year,sss.session_id,scd.class_id,scd.section from
sch_student_detail stud,sch_student_result_detail ssrd,sch_class_detail scd,sch_student_session sss
where  stud.student_id=ssrd.student_id
and  sss.session_id=ssrd.session_id
and  ssrd.class_id = class_id
and scd.class_id=ssrd.class_id
and  ssrd.session_id = @session_key ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sch_user_login_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sch_user_login_sp`(IN userid varchar(8),IN strpassword varchar(100),IN usergroup int)
begin
if(usergroup>=1 and usergroup<=5)
Then
select ssd.emp_id,first_name,last_name,email_id,password,ug.ug_title,ug.ug_id,ug.welcome_message,ug.ug_code  from sch_staff_detail ssd,sch_user_groups ug ,sch_staff_heirarchy sh
where ssd.user_id=userid
and ssd.password =strpassword
and ssd.emp_id = sh.user_id and ug.ug_id = sh.ug_id ;
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_alumni_staff_detail_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_alumni_staff_detail_list`(IN recordsReturned int,IN startIndex int,IN sort varchar(40),IN dir varchar(4),IN SearchKey varchar(50))
begin
declare totalRecord int;
declare noofrow int;
declare status_flag varchar(1);
 select count(*)  into noofrow  from sch_staff_detail ssd ,sch_qualification_detail ssq ,sch_staff_designation ssed
 where ssd.designation=ssed.staff_designation_id and ssd.qualification_id=ssq.qualification_id and ssd.emp_status='D' and
(Upper(ssed.staff_designation_title) like Upper(searchKey) or Upper(ssd.first_name) like Upper(searchKey) or Upper(ssd.last_name) like Upper(searchKey));

IF (recordsReturned is not null)
Then
set @totalRecord=recordsReturned;
ELSE
set @totalRecord=0;
end IF;
		IF @totalRecord > noofrow
		Then
			set @totalRecord = noofrow;
		END IF;
set @a=searchKey ; set @b=sort;set @c=startIndex ;set @d=@totalRecord;set @z=dir;set @status_flag='D';set @z =dir;
PREPARE STMT FROM 'select emp_id, first_name,last_name,contact_no,ssed.staff_designation_title,ssq.qualification_title,ssd.email_id from sch_staff_detail ssd ,sch_qualification_detail  ssq ,sch_staff_designation ssed
	    			where ssd.designation=ssed.staff_designation_id and ssd.qualification_id=ssq.qualification_id  and ssd.emp_status=?
 and (Upper(ssed.staff_designation_title) like Upper(?) or Upper(ssd.first_name) like Upper(?) or Upper(ssd.last_name) like Upper(?)) ORDER BY ?  limit ?, ? ';
EXECUTE STMT USING @status_flag,@a,@a,@a, @b,@c,@d;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_alumni_student_detail_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_alumni_student_detail_list`(IN recordsReturned int,IN startIndex int,IN sort varchar(40),IN dir varchar(4),IN searchKey varchar(50),IN searchBysessionkey varchar(50),IN searchByclasskey varchar(50))
begin
declare totalRecord int;
declare noofrow int;
declare status_flag varchar(1);
declare session_key varchar(50);
IF(searchBysessionkey<>'%%' )
Then
set @session_key=searchBysessionkey ;
 ELSE
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;
END IF;

 select count(*) into  noofrow from sch_student_detail ssd ,sch_student_class_detail sscd,sch_class_detail scd
 where ssd.student_id=sscd.student_id and sscd.class_id=scd.class_id and ssd.status='D' and
(Upper(scd.class_name) like Upper(searchKey) or Upper(ssd.first_name) like Upper(searchKey) or Upper(ssd.last_name) like Upper(searchKey))
and  Upper(sscd.session_id) like Upper(@session_key)  and Upper(sscd.class_id) like Upper(searchByclasskey) ;

IF (recordsReturned is not null)
Then
set @totalRecord=recordsReturned;
ELSE
set @totalRecord=0;
end IF;
		IF @totalRecord > noofrow
		Then
			set @totalRecord = noofrow;
		END IF;
set @a=searchKey ; set @b=sort;set @c=startIndex ;set @d=10;set @z=dir;set @status_flag='D';set @z =dir;set @p=session_key; set @q=searchByclasskey;
PREPARE STMT FROM 'select ssd.student_id,first_name,ssd.last_name,ssd.email_id,scd.class_name,ssd.admission_date,ssd.contact_no,scd.section,ssd.dateofbirth,session_year,sscd.session_id
from sch_student_detail ssd ,sch_student_class_detail sscd,sch_class_detail scd,sch_student_session sss
 where ssd.student_id=sscd.student_id and sscd.class_id=scd.class_id and sss.session_id=sscd.session_id and ssd.status=?
 and (Upper(scd.class_name) like Upper(?) or Upper(ssd.first_name) like Upper(?) or Upper(ssd.last_name) like Upper(?))
and  Upper(sscd.session_id) like Upper(?)  and Upper(sscd.class_id) like Upper(?) ORDER BY ?  limit ?, ? ';
EXECUTE STMT USING @status_flag,@a,@a,@a,@session_key,@q,@b,@c,@d;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_concern_detail_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_concern_detail_list`(IN recordsReturned int,IN startIndex int,IN sort varchar(40),IN dir varchar(4),IN searchKey varchar(50),IN searchstatuskey varchar(50),IN searchCategoryKey varchar(50),IN searchprioritykey varchar(50))
begin

set @a=searchKey ; set @b=sort;set @c=startIndex ;set @d=recordsReturned   ;set @z=dir;set @p=searchprioritykey; set @q=searchCategoryKey; set @r=searchstatuskey;

PREPARE STMT FROM 'select ss.issue_id,ss.issue_title,ss.issue_submited_date,ss.priority,scs.concern_status,scc.category from sch_issue ss ,sch_concern_category scc,sch_concern_status scs
 where ss.cat_id=scc.cat_id and ss.concern_status_id=scs.concern_status_id and
(Upper(ss.issue_title) like Upper(?) or Upper(ss.priority) like Upper(?) or Upper(ss.concern_status_id) like Upper(?))
and  Upper(ss.priority) like Upper(?)  and Upper(ss.cat_id) like Upper(?)  and Upper(ss.concern_status_id) like Upper(?)   ORDER BY ?  limit ?,?

';

EXECUTE STMT USING @a,@a,@a,@p,@q,@r,@b,@c,@d;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_emps_in_dept` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_emps_in_dept`()
Begin
select qualification_title,qualification_id  from sch_staff_qualification;
End ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_emp_count` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_emp_count`(IN searchKey varchar(30))
begin
 select count(*) noofrow  from sch_staff_detail ssd ,sch_staff_qualification ssq ,sch_staff_designation ssed
 where ssd.designation=ssed.staff_designation_id and ssd.qualification_id=ssq.qualification_id and
(Upper(ssed.staff_designation_title) like Upper(searchKey) or Upper(ssd.first_name) like Upper(searchKey) or Upper(ssd.last_name) like Upper(searchKey));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getCurrent_session` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getCurrent_session`()
BEGIN
 declare session_key varchar(50);
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;

select session_id,session_year from sch_student_session where session_id in(@session_key,@session_key+1) order by session_id asc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getStudentProfileList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getStudentProfileList`(IN recordsReturned int,IN startIndex int,IN sort varchar(40),IN dir varchar(4),IN searchKey varchar(50))
begin
declare totalRecord int;
declare noofrow int;
declare status_flag varchar(1);
declare session_key varchar(50);
select count(*) into  noofrow from sch_student_detail ssd
 where ssd.status='A' and
( Upper(ssd.first_name) like Upper(searchKey) or Upper(ssd.last_name) like Upper(searchKey)) ;

IF (recordsReturned is not null)
Then
set @totalRecord=recordsReturned;
ELSE
set @totalRecord=0;
end IF;
		IF @totalRecord > noofrow
		Then
			set @totalRecord = noofrow;
		END IF;
set @a=searchKey ; set @b=sort;set @c=startIndex ;set @d=100;set @z=dir;set @status_flag='A';set @z =dir;
PREPARE STMT FROM 'select ssd.student_id,ssd.user_id,first_name,ssd.last_name,ssd.email_id,ssd.admission_date,ssd.contact_no,ssd.birth_day,ssd.birth_month,ssd.birth_year
from sch_student_detail ssd
 where ssd.status=?
 and (Upper(ssd.first_name) like Upper(?) or Upper(ssd.last_name) like Upper(?))
 ORDER BY ?  limit ?, ? ';
EXECUTE STMT USING @status_flag,@a,@a,@b,@c,@d;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_parent_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_parent_list`()
begin
select parent_id,father_name,email_id from sch_parent_detail;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_registration_detail_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_registration_detail_list`(IN recordsReturned int,IN startIndex int,IN sort varchar(40),IN dir varchar(4),IN SearchKey varchar(50),IN searchBysessionkey varchar(50),IN searchByclasskey varchar(50),regFlag varchar(5))
begin
declare totalRecord int;
declare noofrow int;
declare status_flag varchar(1);
declare session_key varchar(50);

IF(searchBysessionkey<>'%%' )
Then
set @session_key=searchBysessionkey ;
 ELSE
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;
END IF;

IF(regFlag='true')
Then
set @status_flag = 'A';
ELSE
set @status_flag = 'P';
end IF;
 select count(*)  into  noofrow  from sch_registration_detail srd ,sch_class_detail scd where srd.class_id=scd.class_id and srd.reg_status=@status_flag and
(Upper(srd.registration_date) like Upper(searchKey) or Upper(srd.first_name) like Upper(searchKey) or Upper(srd.last_name) like Upper(searchKey))
and  Upper(srd.session_id) like Upper(@session_key)  and Upper(srd.class_id) like Upper(searchByclasskey) ;

IF (recordsReturned is not null)
Then
set @totalRecord=recordsReturned;
ELSE
set @totalRecord=0;
end IF;
		IF @totalRecord > noofrow
		Then
			set @totalRecord = noofrow;
		END IF;
set @a=searchKey ; set @b=sort;set @c=startIndex ;set @d=@totalRecord;set @z=dir;set @z =dir;set @p=session_key; set @q=searchByclasskey;
PREPARE STMT FROM ' select registration_no,first_name,last_name,contact_no,email_id,class_name,sss.session_year,srd.session_id from
 sch_registration_detail srd ,sch_class_detail scd,sch_student_session sss where srd.class_id=scd.class_id and sss.session_id = srd.session_id and srd.reg_status=?
 and (Upper(srd.registration_no) like Upper(?) or Upper(srd.first_name) like Upper(?) or Upper(srd.last_name) like Upper(?))
 and  Upper(srd.session_id) like Upper(?)  and Upper(srd.class_id)  like Upper(?) ORDER BY ?  limit ?, ? ';
EXECUTE STMT USING @status_flag,@a,@a,@a,@session_key,@q,@b,@c,@d;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_staff_class_session_details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_staff_class_session_details`(IN emp_id varchar(50))
begin

select  ssd.emp_id,scd.class_name,section,sdd.dept_name from
sch_staff_detail ssd,sch_emp_class_detail secd,sch_class_detail scd,sch_department_detail sdd
where  ssd.dept_id=sdd.dept_id and ssd.emp_id=secd.emp_id  and secd.class_id=scd.class_id and ssd.designation=3 and ssd.emp_id = emp_id ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_staff_detail_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_staff_detail_list`(IN recordsReturned int,IN startIndex int,IN sort varchar(40),IN dir varchar(4),IN SearchKey varchar(50))
begin
declare totalRecord int;
declare noofrow int;
declare status_flag varchar(1);
 select count(*)  into noofrow  from sch_staff_detail ssd ,sch_qualification_detail ssq ,sch_staff_designation ssed
 where ssd.designation=ssed.staff_designation_id and ssd.qualification_id=ssq.qualification_id and ssd.emp_status='A' and
(Upper(ssed.staff_designation_title) like Upper(searchKey) or Upper(ssd.first_name) like Upper(searchKey) or Upper(ssd.last_name) like Upper(searchKey));

IF (recordsReturned is not null)
Then
set @totalRecord=recordsReturned;
ELSE
set @totalRecord=0;
end IF;
		IF @totalRecord > noofrow
		Then
			set @totalRecord = noofrow;
		END IF;
set @a=searchKey ; set @b=sort;set @c=startIndex ;set @d=@totalRecord;set @z=dir;set @status_flag='A';set @z =dir;
PREPARE STMT FROM 'select emp_id, first_name,last_name,contact_no,ssed.staff_designation_title,ssq.qualification_title,ssd.email_id from sch_staff_detail ssd ,sch_qualification_detail  ssq ,sch_staff_designation ssed
	    			where ssd.designation=ssed.staff_designation_id and ssd.qualification_id=ssq.qualification_id  and ssd.emp_status=?
 and (Upper(ssed.staff_designation_title) like Upper(?) or Upper(ssd.first_name) like Upper(?) or Upper(ssd.last_name) like Upper(?)) ORDER BY ?  limit ?, ? ';
EXECUTE STMT USING @status_flag,@a,@a,@a, @b,@c,@d;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_staff_detail_list_copy` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_staff_detail_list_copy`(IN recordsReturned int,IN startIndex int,IN sort varchar(40),IN dir varchar(4),IN SearchKey varchar(50))
begin
declare totalRecord int;
declare noofrow int;
declare status_flag varchar(1);
 select count(*)  into noofrow  from sch_staff_detail ssd ,sch_qualification_detail ssq ,sch_staff_designation ssed
 where ssd.designation=ssed.staff_designation_id and ssd.qualification_id=ssq.qualification_id and ssd.emp_status='A' and
(Upper(ssed.staff_designation_title) like Upper(searchKey) or Upper(ssd.first_name) like Upper(searchKey) or Upper(ssd.last_name) like Upper(searchKey));

IF (recordsReturned is not null)
Then
set @totalRecord=recordsReturned;
ELSE
set @totalRecord=0;
end IF;
		IF @totalRecord > noofrow
		Then
			set @totalRecord = noofrow;
		END IF;
set @a=searchKey ; set @b=sort;set @c=startIndex ;set @d=@totalRecord;set @z=dir;set @status_flag='A';set @z =dir;
PREPARE STMT FROM 'select emp_id, first_name,last_name,contact_no,ssed.staff_designation_title,ssq.qualification_title,ssd.email_id from sch_staff_detail ssd ,sch_qualification_detail  ssq ,sch_staff_designation ssed
	    			where ssd.designation=ssed.staff_designation_id and ssd.qualification_id=ssq.qualification_id  and ssd.emp_status=?
 and (Upper(ssed.staff_designation_title) like Upper(?) or Upper(ssd.first_name) like Upper(?) or Upper(ssd.last_name) like Upper(?)) ORDER BY ?  limit ?, ? ';
EXECUTE STMT USING @status_flag,@a,@a,@a, @b,@c,@d;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_staff_detail_list_copy1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_staff_detail_list_copy1`(IN recordsReturned int,IN startIndex int,IN sort varchar(40),IN dir varchar(4),IN SearchKey varchar(50))
begin
declare totalRecord int;
declare noofrow int;
declare status_flag varchar(1);
 select count(*)  into noofrow  from sch_staff_detail ssd ,sch_qualification_detail ssq ,sch_staff_designation ssed
 where ssd.designation=ssed.staff_designation_id and ssd.qualification_id=ssq.qualification_id and ssd.emp_status='A' and
(Upper(ssed.staff_designation_title) like Upper(searchKey) or Upper(ssd.first_name) like Upper(searchKey) or Upper(ssd.last_name) like Upper(searchKey));

IF (recordsReturned is not null)
Then
set @totalRecord=recordsReturned;
ELSE
set @totalRecord=0;
end IF;
		IF @totalRecord > noofrow
		Then
			set @totalRecord = noofrow;
		END IF;
set @a=searchKey ; set @b=sort;set @c=startIndex ;set @d=@totalRecord;set @z=dir;set @status_flag='A';set @z =dir;
PREPARE STMT FROM 'select emp_id, first_name,last_name,contact_no,ssed.staff_designation_title,ssq.qualification_title,ssd.email_id from sch_staff_detail ssd ,sch_qualification_detail  ssq ,sch_staff_designation ssed
	    			where ssd.designation=ssed.staff_designation_id and ssd.qualification_id=ssq.qualification_id  and ssd.emp_status=?
 and (Upper(ssed.staff_designation_title) like Upper(?) or Upper(ssd.first_name) like Upper(?) or Upper(ssd.last_name) like Upper(?)) ORDER BY ?  limit ?, ? ';
EXECUTE STMT USING @status_flag,@a,@a,@a, @b,@c,@d;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_staff_subject_class_details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_staff_subject_class_details`(IN emp_id int)
begin
 select sssd.subject_id,ssd.subject_name
from sch_emp_subject_detail sssd,sch_subject_detail ssd,sch_staff_detail stde
where sssd.subject_id=ssd.subject_id and sssd.emp_id=stde.emp_id and stde.designation=3 and stde.emp_id=emp_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_student_detail_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_student_detail_list`(IN recordsReturned int,IN startIndex int,IN sort varchar(40),IN dir varchar(4),IN searchKey varchar(50),IN searchBysessionkey varchar(50),IN searchByclasskey varchar(50))
begin
declare totalRecord int;
declare noofrow int;
declare status_flag varchar(1);
declare session_key varchar(50);
IF(searchBysessionkey<>'%%' )
Then
set @session_key=searchBysessionkey ;
 ELSE
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;
END IF;


 select count(*) into  noofrow from sch_student_detail ssd ,sch_student_class_detail sscd,sch_class_detail scd
 where ssd.student_id=sscd.student_id and sscd.class_id=scd.class_id and ssd.status='A' and
(Upper(scd.class_name) like Upper(searchKey) or Upper(ssd.first_name) like Upper(searchKey) or Upper(ssd.last_name) like Upper(searchKey))
and  Upper(sscd.session_id) like Upper(@session_key)  and Upper(sscd.class_id) like Upper(searchByclasskey) ;

IF (recordsReturned is not null)
Then
set @totalRecord=recordsReturned;
ELSE
set @totalRecord=0;
end IF;
		IF @totalRecord > noofrow
		Then
			set @totalRecord = noofrow;
		END IF;
set @a=searchKey ; set @b=sort;set @c=startIndex ;set @d=@totalRecord;set @z=dir;set @z =dir;set @status_flag = 'A';set @p=session_key; set @q=searchByclasskey;
PREPARE STMT FROM 'select ssd.student_id,first_name,ssd.last_name,ssd.email_id,scd.class_name,ssd.admission_date,ssd.contact_no,scd.section,ssd.dateofbirth,session_year,sscd.session_id
from sch_student_detail ssd ,sch_student_class_detail sscd,sch_class_detail scd,sch_student_session sss
 where ssd.student_id=sscd.student_id and sscd.class_id=scd.class_id and sss.session_id=sscd.session_id and ssd.status=?
 and (Upper(scd.class_name) like Upper(?) or Upper(ssd.first_name) like Upper(?) or Upper(ssd.last_name) like Upper(?))
and  Upper(sscd.session_id) like Upper(?)  and Upper(sscd.class_id) like Upper(?) ORDER BY ?  limit ?,? ';
EXECUTE STMT USING @status_flag,@a,@a,@a,@session_key,@q,@b,@c,@d;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_student_detail_search` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_student_detail_search`(IN searchKey varchar(50),IN searchBysessionkey varchar(50))
begin
declare session_key varchar(50);
IF(searchBysessionkey<>'%%' )
Then
set @session_key=searchBysessionkey ;
 ELSE
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;
END IF;
 select ssd.student_id,first_name,ssd.last_name,ssd.email_id,scd.class_name,ssd.admission_date,ssd.contact_no,scd.section,ssd.birth_day,ssd.birth_month,ssd.birth_year,session_year,sscd.session_id,spd.father_name,sscd.class_id,ssd.address,scd.monthly_fee
 from sch_student_detail ssd ,sch_student_class_detail sscd,sch_class_detail scd,sch_student_session sss,sch_parent_detail spd
 where ssd.student_id=sscd.student_id and sscd.class_id=scd.class_id and sss.session_id=sscd.session_id and ssd.student_id=spd.student_id and ssd.status='A' and
(Upper(scd.class_name) like Upper(searchKey) or Upper(ssd.first_name) like Upper(searchKey) or Upper(ssd.last_name) like Upper(searchKey) or Upper(ssd.student_id) like Upper(searchKey))
and  Upper(sscd.session_id) like Upper(@session_key) ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_student_feeCollaction_detail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_student_feeCollaction_detail`(IN recordsReturned int,IN startIndex int,IN sort varchar(40),IN dir varchar(4),IN searchKey varchar(50),IN searchBysessionkey varchar(50),IN searchByclasskey varchar(50))
begin
declare totalRecord int;
declare noofrow int;
declare status_flag varchar(1);
declare session_key varchar(50);
IF(searchBysessionkey<>'%%' )
Then
set @session_key=searchBysessionkey ;
 ELSE
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;
END IF;


 select  count(*) into  noofrow  from sch_fee_detail sfd,sch_class_detail scd,sch_student_detail ssd
 where
sfd.admission_no=ssd.student_id and
sfd.class_id=scd.class_id
and ssd.status='A'
and(Upper(scd.class_name) like Upper(searchKey) or Upper(ssd.first_name) like Upper(searchKey) or Upper(ssd.last_name) like Upper(searchKey))
and  Upper(sfd.session_id) like Upper(@session_key)  and Upper(sfd.class_id) like Upper(searchByclasskey) ;

IF (recordsReturned is not null)
Then
set @totalRecord=recordsReturned;
ELSE
set @totalRecord=0;
end IF;
		IF @totalRecord > noofrow
		Then
			set @totalRecord = noofrow;
		END IF;
set @a=searchKey ; set @b=sort;set @c=startIndex ;set @d=10;set @z=dir;set @status_flag='A';set @z =dir;set @p=session_key; set @q=searchByclasskey;
PREPARE STMT FROM 'select ssd.student_id,first_name,ssd.last_name,ssd.email_id,scd.class_name,ssd.admission_date,ssd.contact_no,scd.section,ssd.dateofbirth,session_year,sfd.session_id,sfd.receipt_no
from sch_student_detail ssd ,sch_fee_detail sfd,sch_class_detail scd,sch_student_session sss
 where ssd.student_id=sfd.admission_no and sfd.class_id=scd.class_id and sss.session_id=sfd.session_id and ssd.status=?
 and (Upper(scd.class_name) like Upper(?) or Upper(ssd.first_name) like Upper(?) or Upper(ssd.last_name) like Upper(?))
and  Upper(sfd.session_id) like Upper(?)  and Upper(sfd.class_id) like Upper(?) ORDER BY ?  limit ?, ? ';
EXECUTE STMT USING @status_flag,@a,@a,@a,@session_key,@q,@b,@c,@d;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_student_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_student_list`(IN selectionFlag varchar(50),IN classId varchar(50))
begin
declare session_key varchar(50);
if(selectionFlag='active' && classId='' )
then
select ssd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.email_id from
sch_student_detail ssd where ssd.status='A';
end if;
if(selectionFlag='active' &&  classId!='' )
then
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;

select ssd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.email_id from
sch_student_detail ssd,sch_student_class_detail sscd where ssd.student_id =sscd.student_id and sscd.class_id = classId and session_id= session_key and ssd.status='A';
end if;
if(selectionFlag!='active' &&  classId='' )
then
select ssd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.email_id from
sch_student_detail ssd where ssd.status='D';
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_student_new_session_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_student_new_session_list`(IN searchKey varchar(50),IN searchBysessionkey varchar(50),IN searchByclasskey varchar(50))
begin
declare totalRecord int;
declare noofrow int;
declare status_flag varchar(1);
declare session_key varchar(50);
IF(searchBysessionkey<>'%%' )
Then
set @session_key=searchBysessionkey ;
 ELSE
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;
END IF;
 select ssrd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssrd.status,ssrd.division,scd.class_name,sss.session_year,sdt.dept_name ,ssrd.session_id,ssrd.class_id,sscd.dept_id
from sch_student_result_detail ssrd ,sch_class_detail scd,sch_student_detail ssd,sch_student_session sss,sch_student_class_detail sscd,sch_department_detail sdt
 where ssrd.class_id=scd.class_id  and ssd.student_id=ssrd.student_id and sss.session_id = ssrd.session_id and sscd.student_id= ssrd.student_id and sscd.class_id=ssrd.class_id and sscd.session_id=ssrd.session_id  and sdt.dept_id=sscd.dept_id and
(Upper(ssrd.student_id) like Upper(searchKey) or Upper(ssd.first_name) like Upper(searchKey) or Upper(ssd.last_name) like Upper(searchKey))
and  Upper(ssrd.session_id) like Upper(@session_key)  and Upper(ssrd.class_id) like Upper(searchByclasskey) ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_student_report_details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_student_report_details`(IN student_id varchar(50),IN class_id varchar(50),IN  searchBysessionkey varchar(50),IN dept_id varchar(50))
begin
declare session_key varchar(50);
IF(searchBysessionkey<>'%%' )
Then
set @session_key=searchBysessionkey ;
 ELSE
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;
END IF;
 select ssrd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssrd.status,ssrd.division,ssrd.percentage,scd.class_name,
sss.session_year,sdt.dept_name ,ssrd.session_id,ssrd.class_id,sscd.dept_id,spd.father_name,ssd.birth_day,ssd.birth_month,ssd.birth_year,ssrd.total_mark_outof,ssrd.total_mark_obtained
from sch_student_result_detail ssrd ,sch_class_detail scd,sch_student_detail ssd,sch_student_session sss,sch_student_class_detail sscd,sch_department_detail sdt,sch_parent_detail spd
where ssrd.class_id=scd.class_id
and ssd.student_id=ssrd.student_id
and sss.session_id = ssrd.session_id
and sscd.student_id= ssrd.student_id
and sscd.class_id=ssrd.class_id
and sscd.session_id=ssrd.session_id
and ssd.student_id=spd.student_id
and sdt.dept_id=sscd.dept_id
and ssrd.student_id=student_id
and  ssrd.class_id = class_id
and  ssrd.session_id = @session_key
and sscd.dept_id = dept_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_student_report_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_student_report_list`(IN searchKey varchar(50),IN searchBysessionkey varchar(50),IN searchByclasskey varchar(50))
begin
declare totalRecord int;
declare noofrow int;
declare status_flag varchar(1);
declare session_key varchar(50);
IF(searchBysessionkey<>'%%' )
Then
set @session_key=searchBysessionkey ;
 ELSE
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;
END IF;
 select ssrd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssrd.status,ssrd.division,scd.class_name,sss.session_year,sdt.dept_name ,ssrd.session_id,ssrd.class_id,sscd.dept_id
from sch_student_result_detail ssrd ,sch_class_detail scd,sch_student_detail ssd,sch_student_session sss,sch_student_class_detail sscd,sch_department_detail sdt
 where ssrd.class_id=scd.class_id  and ssd.student_id=ssrd.student_id and sss.session_id = ssrd.session_id and sscd.student_id= ssrd.student_id and sscd.class_id=ssrd.class_id and sscd.session_id=ssrd.session_id  and sdt.dept_id=sscd.dept_id and
(Upper(ssrd.student_id) like Upper(searchKey) or Upper(ssd.first_name) like Upper(searchKey) or Upper(ssd.last_name) like Upper(searchKey))
and  Upper(ssrd.session_id) like Upper(@session_key)  and Upper(ssrd.class_id) like Upper(searchByclasskey) and ssd.status='A' ;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_student_session_class_details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_student_session_class_details`(IN student_id varchar(50))
begin

select  subtable.student_id,subtable.class_id,subtable.dept_id,subtable.session_id,sss.session_year,scd.class_name,scd.section,dept_name,subtable.division,subtable.percentage,subtable.total_mark_outof,subtable.total_mark_obtained from
(select sscd.student_id,sscd.class_id,sscd.dept_id,ssrd.division,ssrd.percentage,ssrd.total_mark_outof,ssrd.total_mark_obtained,sscd.session_id
from sch_student_class_detail sscd left outer join sch_student_result_detail ssrd on sscd.student_id = ssrd.student_id and sscd.class_id = ssrd.class_id and   sscd.session_id = ssrd.session_id having sscd.student_id=student_id ) subtable,
sch_student_session sss ,sch_class_detail scd, sch_department_detail sdt
where  sss.session_id= subtable.session_id and subtable.class_id=scd.class_id and subtable.dept_id=sdt.dept_id order by subtable.session_id ;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_student_session_class_detailsbyIds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_student_session_class_detailsbyIds`(IN student_id varchar(50),IN class_id varchar(50),IN session_id varchar(50))
begin

select  subtable.student_id,subtable.class_id,subtable.dept_id,subtable.session_id,sss.session_year,scd.class_name,scd.section,dept_name,subtable.division,subtable.percentage,subtable.total_mark_outof,subtable.total_mark_obtained from
(select sscd.student_id,sscd.class_id,sscd.dept_id,ssrd.division,ssrd.percentage,ssrd.total_mark_outof,ssrd.total_mark_obtained,sscd.session_id
from sch_student_class_detail sscd left outer join sch_student_result_detail ssrd on sscd.student_id = ssrd.student_id and sscd.class_id = ssrd.class_id and  
 sscd.session_id = ssrd.session_id having sscd.student_id=student_id  and sscd.class_id=class_id and sscd.session_id=session_id ) subtable,
sch_student_session sss ,sch_class_detail scd, sch_department_detail sdt
where  sss.session_id= subtable.session_id and subtable.class_id=scd.class_id and subtable.dept_id=sdt.dept_id order by subtable.session_id ;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_student_subject_class_details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_student_subject_class_details`(IN student_id int,IN class_id int,IN session_id int)
begin
 select sssd.subject_id,sssd.session_id,sssd.class_id,ssd.subject_name
from sch_student_subject_detail sssd,sch_subject_detail ssd
where sssd.subject_id=ssd.subject_id and sssd.student_id=student_id and sssd.class_id=class_id and sssd.session_id=session_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_student_sub_mark_reports` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_student_sub_mark_reports`(IN student_id varchar(50),IN class_id varchar(50),IN  searchBysessionkey varchar(50),IN dept_id varchar(50))
begin
declare session_key varchar(50);
IF(searchBysessionkey<>'%%' )
Then
set @session_key=searchBysessionkey ;
 ELSE
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;
END IF;
 select ssd.subject_name, ssmd.mark_outof, ssmd.mark_obtained,ssd.subject_code from sch_student_mark_detail ssmd,sch_subject_detail ssd
where ssmd.subject_id = ssd.subject_id and ssmd.student_id=student_id  and  ssmd.class_id = class_id and  ssmd.session_id = @session_key and ssmd.dept_id = dept_id order by ssd.subject_code;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_student_sub_mark_report_by_session_class` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_student_sub_mark_report_by_session_class`(IN student_id varchar(50),IN class_id varchar(50),IN  searchBysessionkey varchar(50))
begin
declare session_key varchar(50);
IF(searchBysessionkey<>'%%' )
Then
set @session_key=searchBysessionkey ;
 ELSE
 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 set @session_key=session_key ;
END IF;

select leftt.subject_name, leftt.subject_id,leftt.class_id,rightt.mark_outof,rightt.mark_obtained from
(select ssd.subject_name,scsr.subject_id,class_id from sch_class_subject_relation scsr,sch_subject_detail ssd where scsr.class_id=class_id and scsr.subject_id=ssd.subject_id ) leftt
left outer join
(select subject_id,class_id,mark_outof,mark_obtained from sch_student_mark_detail ssmd
where  ssmd.student_id=student_id  and  ssmd.class_id = class_id and  ssmd.session_id = @session_key) rightt
on leftt.class_id=rightt.class_id  and leftt.subject_id=rightt.subject_id order by leftt.subject_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_teacher_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_teacher_list`(IN selectionFlag varchar(50))
begin
if(selectionFlag='active' )
then
select ssd.emp_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.email_id from
sch_staff_detail ssd ,sch_staff_designation ssde
where ssd.designation=ssde.staff_designation_id
and ssd.designation=3
and emp_status='A'
order by ssd.first_name;
else
select ssd.emp_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.email_id from
sch_staff_detail ssd ,sch_staff_designation ssde
where ssd.designation=ssde.staff_designation_id
and ssd.designation=3
and emp_status='D'
order by ssd.first_name;
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `test` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `test`(OUT param1 varchar(10))
begin
 select count(*) noofrow  from sch_staff_detail ssd ,sch_staff_qualification ssq ,sch_staff_designation ssed
 where ssd.designation=ssed.staff_designation_id and ssd.qualification_id=ssq.qualification_id and
(Upper(ssed.staff_designation_title) like Upper('%%') or Upper(ssd.first_name) like Upper('%%') or Upper(ssd.last_name) like Upper('%%'));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TestProc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `TestProc`()
select now() ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `test_login_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `test_login_sp`(emailId varchar(50),pwd varchar(100),ug_id varchar(10))
BEGIN
select ssd.student_id emp_id ,first_name,last_name,email_id,password,ug.ug_title,ug.ug_id,ug.welcome_message,ug.ug_code
from sch_student_detail ssd,sch_user_groups ug ,sch_student_heirarchy ssh where email_id= emailId   and
password =  pwd and ssd.student_id = ssh.user_id and ug.ug_id = ssh.ug_id ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_registration_detail_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_registration_detail_sp`(IN first_name varchar(50),IN middle_name varchar(50),IN last_name varchar(50),IN email_id varchar(50),IN registration_date datetime,IN address varchar(1000),IN contact_no varchar(10),IN gender varchar(10),IN father_name varchar(50),IN classid INT,IN dateofbirth datetime,IN registrationno INT)
BEGIN
update sch_registration_detail set first_name = first_name,middle_name = middle_name,last_name = last_name ,email_id = email_id,registration_date = registration_date ,father_name =  father_name,class_id = classid,
address = address ,contact_no = contact_no ,gender=gender,dateofbirth=dateofbirth where trim(registration_no)= trim(registrationno );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_StaffDetail_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_StaffDetail_sp`(IN first_name varchar(50),IN middle_name varchar(50),IN last_name varchar(50),IN email_id varchar(50),IN salary varchar(10),IN address varchar(50),IN contact_no varchar(10),IN strqualification_id INT,IN previous_exp varchar(50),IN joining_date datetime,IN designation_id INT,userid INT,deptid INT)
BEGIN
update  sch_staff_detail  set first_name=first_name,middle_name=middle_name,last_name=last_name,email_id=email_id,salary=salary,address=address,
contact_no=contact_no,qualification_id=strqualification_id ,previous_exp=previous_exp,joining_date=joining_date,designation=designation_id,dept_id=deptid  where trim(emp_id)=trim(userid) ;
delete from sch_staff_heirarchy where  trim(user_id)=trim(userid);
insert into sch_staff_heirarchy(user_id,ug_id) values(userid,designation_id);
delete from sch_emp_class_detail where trim(emp_id) = trim(userid) ;
delete from sch_emp_subject_detail where trim(emp_id) = trim(userid) ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_student_detail_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_student_detail_sp`(IN first_name varchar(50),IN middle_name varchar(50),IN last_name varchar(50),IN email_id varchar(50),IN admission_date varchar(10),IN address varchar(500),IN contact_no varchar(10),IN previous_edu_center varchar(50),IN gender varchar(10),IN father_name varchar(50),IN mother_name varchar(50),IN father_higher_qualification_id varchar(50),IN parent_email_id varchar(50),IN classid INT,IN preinstclassid varchar(10),IN admissionfee INT,IN concessionamt INT,IN comment varchar(100),IN dateofbirth datetime,IN birth_day varchar(2),IN birth_month varchar(2),IN birth_year varchar(4),IN studentid INT,IN deptId int)
BEGIN
update sch_student_detail set first_name = first_name,middle_name = middle_name,last_name = last_name ,email_id = email_id,admission_date = admission_date ,
address = address ,contact_no = contact_no ,previous_edu_center=previous_edu_center,gender=gender,dateofbirth=dateofbirth,birth_day=birth_day,birth_month=birth_month,birth_year=birth_year where trim(student_id)= trim(studentid);
update sch_parent_detail set father_name =  father_name ,mother_name = mother_name,father_higher_qualification_id = father_higher_qualification_id ,email_id=parent_email_id where trim(student_id)= trim(studentid);
update  sch_student_class_detail set class_id = classid ,preinstclass_id = preinstclassid ,dept_id =deptId where trim(student_id)= trim(studentid);
update sch_student_admissionfee_collection set admission_fee = admissionfee,concession_amount = concessionamt ,comment = comment where trim(student_id)= trim(studentid);
delete from sch_student_subject_detail where trim(student_id) = trim(studentid ) ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_login_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_login_sp`(userid varchar(50),strpassword varchar(100),ug_id int)
BEGIN
if(ug_id = 1 or ug_id = 2  or ug_id = 3 or ug_id = 4 or ug_id = 5 )
THEN
select ssd.emp_id userId ,first_name,last_name,email_id,password,ug.ug_title,ug.ug_id,ug.welcome_message,ug.ug_code
from sch_staff_detail ssd,sch_user_groups ug ,sch_staff_heirarchy sh
where ssd.user_id=userid and ssd.password =strpassword and ssd.emp_id = sh.user_id and ug.ug_id = sh.ug_id ;
ELSE if(ug_id=6 )
then
select ssd.student_id userId ,first_name,last_name,email_id,password,ug.ug_title,ug.ug_id,ug.welcome_message,ug.ug_code
from sch_student_detail ssd,sch_user_groups ug ,sch_student_heirarchy ssh where ssd.user_id= userid   and
password =  strpassword and ssd.student_id = ssh.user_id and ug.ug_id = ssh.ug_id ;
ELSE
select spd.parent_id userId ,father_name first_name,"" last_name,email_id,password,ug.ug_title,ug.ug_id,ug.welcome_message,ug.ug_code
from sch_parent_detail spd,sch_user_groups ug ,sch_parent_heirarchy sph where spd.user_id= userid   and
password =  strpassword and spd.parent_id = sph.user_id and ug.ug_id = sph.ug_id ;
END IF;
END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `view_studentDetailBySession_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `view_studentDetailBySession_sp`(IN studentId int,IN searchBysessionkey varchar(50),IN stud_status varchar(1))
BEGIN
declare session_key varchar(50);
IF(searchBysessionkey<>'' )
Then
	set @session_key=searchBysessionkey ;
 ELSE
	 select session_id into session_key from  sch_student_session where  CURDATE()  between session_start and session_end;
 	set @session_key=session_key ;
END IF;
select ssd.student_id,ssd.first_name,ssd.middle_name,ssd.last_name,ssd.email_id,ssd.admission_date,ssd.contact_no,ssd.address,ssd.dateofbirth,ssd.previous_edu_center,ssd.gender,scd.admission_fee,concession_amount,comment,scd.class_name
,spsd.qualification_title student_prev_qualification,sqd.qualification_title parent_higher_qualification,spd.father_name,spd.mother_name,spd.email_id parentemail_id,ssd.birth_day,ssd.birth_month,ssd.birth_year,sscd.session_id
from
 sch_student_detail ssd ,
sch_student_class_detail sscd,
sch_student_admissionfee_collection ssac,
sch_parent_detail spd,
sch_class_detail scd,sch_qualification_detail sqd ,
sch_prev_student_detail spsd
where ssd.student_id=sscd.student_id
and ssd.student_id=ssac.student_id
and ssd.student_id=spd.student_id
and scd.class_id = sscd.class_id
and sqd.qualification_id = spd.father_higher_qualification_id
and spsd.qualification_id = sscd.preinstclass_id
and ssd.status=stud_status and ssd.student_id=studentId
and sscd.session_id= @session_key;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-26  9:00:37
